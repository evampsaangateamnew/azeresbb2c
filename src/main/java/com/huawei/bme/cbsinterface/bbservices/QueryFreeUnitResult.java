package com.huawei.bme.cbsinterface.bbservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * 2015.02.06 modified by
 * yuewnfei\uff1aFreeUnitItemDetail\u8282\u70b9\u4e0b\u65b0\u589eInitialOrigin;
 * FreeUnitItemDetail/FreeUnitOrigin\u8282\u70b9\u4e0b\u65b0\u589eFreeUnitInstanceID\u548cFileID
 * 
 * <p>
 * Java class for QueryFreeUnitResult complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QueryFreeUnitResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FreeUnitItem" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TotalInitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="TotalUnusedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="FreeUnitItemDetail" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="InitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="CurrentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FreeUnitOrigin" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OriginType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;choice>
 *                                         &lt;element name="OfferingKey">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                                   &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                         &lt;element name="FileID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                         &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;/choice>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="InitialOrigin" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="InitialType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;choice>
 *                                         &lt;element name="OfferingKey">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                                   &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;/choice>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="RollOverFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastRollOveredTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="MemberUsageList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ShareUsageList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SharedPrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OfferingKey" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryFreeUnitResult", propOrder = { "freeUnitItem", "shareUsageList" })
public class QueryFreeUnitResult {
	@XmlElement(name = "FreeUnitItem")
	protected List<QueryFreeUnitResult.FreeUnitItem> freeUnitItem;
	@XmlElement(name = "ShareUsageList")
	protected List<QueryFreeUnitResult.ShareUsageList> shareUsageList;

	/**
	 * Gets the value of the freeUnitItem property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the freeUnitItem property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getFreeUnitItem().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link QueryFreeUnitResult.FreeUnitItem }
	 * 
	 * 
	 */
	public List<QueryFreeUnitResult.FreeUnitItem> getFreeUnitItem() {
		if (freeUnitItem == null) {
			freeUnitItem = new ArrayList<QueryFreeUnitResult.FreeUnitItem>();
		}
		return this.freeUnitItem;
	}

	/**
	 * Gets the value of the shareUsageList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the shareUsageList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getShareUsageList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link QueryFreeUnitResult.ShareUsageList }
	 * 
	 * 
	 */
	public List<QueryFreeUnitResult.ShareUsageList> getShareUsageList() {
		if (shareUsageList == null) {
			shareUsageList = new ArrayList<QueryFreeUnitResult.ShareUsageList>();
		}
		return this.shareUsageList;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="TotalInitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="TotalUnusedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="FreeUnitItemDetail" maxOccurs="unbounded">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                   &lt;element name="InitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                   &lt;element name="CurrentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="FreeUnitOrigin" minOccurs="0">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="OriginType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;choice>
	 *                               &lt;element name="OfferingKey">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                                         &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                               &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                               &lt;element name="FileID" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                               &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                             &lt;/choice>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                   &lt;element name="InitialOrigin" minOccurs="0">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="InitialType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;choice>
	 *                               &lt;element name="OfferingKey">
	 *                                 &lt;complexType>
	 *                                   &lt;complexContent>
	 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                       &lt;sequence>
	 *                                         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                                         &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                                       &lt;/sequence>
	 *                                     &lt;/restriction>
	 *                                   &lt;/complexContent>
	 *                                 &lt;/complexType>
	 *                               &lt;/element>
	 *                               &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                             &lt;/choice>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                   &lt;element name="RollOverFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="LastRollOveredTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="MemberUsageList" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "freeUnitType", "freeUnitTypeName", "measureUnit", "measureUnitName",
			"totalInitialAmount", "totalUnusedAmount", "freeUnitItemDetail", "memberUsageList" })
	public static class FreeUnitItem {
		@XmlElement(name = "FreeUnitType", required = true)
		protected String freeUnitType;
		@XmlElement(name = "FreeUnitTypeName", required = true)
		protected String freeUnitTypeName;
		@XmlElement(name = "MeasureUnit", required = true)
		protected String measureUnit;
		@XmlElement(name = "MeasureUnitName", required = true)
		protected String measureUnitName;
		@XmlElement(name = "TotalInitialAmount")
		protected long totalInitialAmount;
		@XmlElement(name = "TotalUnusedAmount")
		protected long totalUnusedAmount;
		@XmlElement(name = "FreeUnitItemDetail", required = true)
		protected List<QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail> freeUnitItemDetail;
		@XmlElement(name = "MemberUsageList")
		protected List<QueryFreeUnitResult.FreeUnitItem.MemberUsageList> memberUsageList;

		/**
		 * Gets the value of the freeUnitType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getFreeUnitType() {
			return freeUnitType;
		}

		/**
		 * Sets the value of the freeUnitType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setFreeUnitType(String value) {
			this.freeUnitType = value;
		}

		/**
		 * Gets the value of the freeUnitTypeName property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getFreeUnitTypeName() {
			return freeUnitTypeName;
		}

		/**
		 * Sets the value of the freeUnitTypeName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setFreeUnitTypeName(String value) {
			this.freeUnitTypeName = value;
		}

		/**
		 * Gets the value of the measureUnit property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMeasureUnit() {
			return measureUnit;
		}

		/**
		 * Sets the value of the measureUnit property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setMeasureUnit(String value) {
			this.measureUnit = value;
		}

		/**
		 * Gets the value of the measureUnitName property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMeasureUnitName() {
			return measureUnitName;
		}

		/**
		 * Sets the value of the measureUnitName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setMeasureUnitName(String value) {
			this.measureUnitName = value;
		}

		/**
		 * Gets the value of the totalInitialAmount property.
		 * 
		 */
		public long getTotalInitialAmount() {
			return totalInitialAmount;
		}

		/**
		 * Sets the value of the totalInitialAmount property.
		 * 
		 */
		public void setTotalInitialAmount(long value) {
			this.totalInitialAmount = value;
		}

		/**
		 * Gets the value of the totalUnusedAmount property.
		 * 
		 */
		public long getTotalUnusedAmount() {
			return totalUnusedAmount;
		}

		/**
		 * Sets the value of the totalUnusedAmount property.
		 * 
		 */
		public void setTotalUnusedAmount(long value) {
			this.totalUnusedAmount = value;
		}

		/**
		 * Gets the value of the freeUnitItemDetail property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the freeUnitItemDetail property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getFreeUnitItemDetail().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail }
		 * 
		 * 
		 */
		public List<QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail> getFreeUnitItemDetail() {
			if (freeUnitItemDetail == null) {
				freeUnitItemDetail = new ArrayList<QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail>();
			}
			return this.freeUnitItemDetail;
		}

		/**
		 * Gets the value of the memberUsageList property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the memberUsageList property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getMemberUsageList().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link QueryFreeUnitResult.FreeUnitItem.MemberUsageList }
		 * 
		 * 
		 */
		public List<QueryFreeUnitResult.FreeUnitItem.MemberUsageList> getMemberUsageList() {
			if (memberUsageList == null) {
				memberUsageList = new ArrayList<QueryFreeUnitResult.FreeUnitItem.MemberUsageList>();
			}
			return this.memberUsageList;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *         &lt;element name="InitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *         &lt;element name="CurrentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="FreeUnitOrigin" minOccurs="0">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="OriginType" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;choice>
		 *                     &lt;element name="OfferingKey">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *                               &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                     &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *                     &lt;element name="FileID" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *                     &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *                   &lt;/choice>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *         &lt;element name="InitialOrigin" minOccurs="0">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="InitialType" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;choice>
		 *                     &lt;element name="OfferingKey">
		 *                       &lt;complexType>
		 *                         &lt;complexContent>
		 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                             &lt;sequence>
		 *                               &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *                               &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                             &lt;/sequence>
		 *                           &lt;/restriction>
		 *                         &lt;/complexContent>
		 *                       &lt;/complexType>
		 *                     &lt;/element>
		 *                     &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *                   &lt;/choice>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *         &lt;element name="RollOverFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="LastRollOveredTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "freeUnitInstanceID", "initialAmount", "currentAmount", "effectiveTime",
				"expireTime", "freeUnitOrigin", "initialOrigin", "rollOverFlag", "lastRollOveredTime" })
		public static class FreeUnitItemDetail {
			@XmlElement(name = "FreeUnitInstanceID")
			protected long freeUnitInstanceID;
			@XmlElement(name = "InitialAmount")
			protected long initialAmount;
			@XmlElement(name = "CurrentAmount")
			protected long currentAmount;
			@XmlElement(name = "EffectiveTime", required = true)
			protected String effectiveTime;
			@XmlElement(name = "ExpireTime", required = true)
			protected String expireTime;
			@XmlElement(name = "FreeUnitOrigin")
			protected QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin freeUnitOrigin;
			@XmlElement(name = "InitialOrigin")
			protected QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin initialOrigin;
			@XmlElement(name = "RollOverFlag", required = true)
			protected String rollOverFlag;
			@XmlElement(name = "LastRollOveredTime")
			protected String lastRollOveredTime;

			/**
			 * Gets the value of the freeUnitInstanceID property.
			 * 
			 */
			public long getFreeUnitInstanceID() {
				return freeUnitInstanceID;
			}

			/**
			 * Sets the value of the freeUnitInstanceID property.
			 * 
			 */
			public void setFreeUnitInstanceID(long value) {
				this.freeUnitInstanceID = value;
			}

			/**
			 * Gets the value of the initialAmount property.
			 * 
			 */
			public long getInitialAmount() {
				return initialAmount;
			}

			/**
			 * Sets the value of the initialAmount property.
			 * 
			 */
			public void setInitialAmount(long value) {
				this.initialAmount = value;
			}

			/**
			 * Gets the value of the currentAmount property.
			 * 
			 */
			public long getCurrentAmount() {
				return currentAmount;
			}

			/**
			 * Sets the value of the currentAmount property.
			 * 
			 */
			public void setCurrentAmount(long value) {
				this.currentAmount = value;
			}

			/**
			 * Gets the value of the effectiveTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getEffectiveTime() {
				return effectiveTime;
			}

			/**
			 * Sets the value of the effectiveTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setEffectiveTime(String value) {
				this.effectiveTime = value;
			}

			/**
			 * Gets the value of the expireTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getExpireTime() {
				return expireTime;
			}

			/**
			 * Sets the value of the expireTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setExpireTime(String value) {
				this.expireTime = value;
			}

			/**
			 * Gets the value of the freeUnitOrigin property.
			 * 
			 * @return possible object is
			 *         {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin }
			 * 
			 */
			public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin getFreeUnitOrigin() {
				return freeUnitOrigin;
			}

			/**
			 * Sets the value of the freeUnitOrigin property.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin }
			 * 
			 */
			public void setFreeUnitOrigin(QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin value) {
				this.freeUnitOrigin = value;
			}

			/**
			 * Gets the value of the initialOrigin property.
			 * 
			 * @return possible object is
			 *         {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin }
			 * 
			 */
			public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin getInitialOrigin() {
				return initialOrigin;
			}

			/**
			 * Sets the value of the initialOrigin property.
			 * 
			 * @param value
			 *            allowed object is
			 *            {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin }
			 * 
			 */
			public void setInitialOrigin(QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin value) {
				this.initialOrigin = value;
			}

			/**
			 * Gets the value of the rollOverFlag property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getRollOverFlag() {
				return rollOverFlag;
			}

			/**
			 * Sets the value of the rollOverFlag property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setRollOverFlag(String value) {
				this.rollOverFlag = value;
			}

			/**
			 * Gets the value of the lastRollOveredTime property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getLastRollOveredTime() {
				return lastRollOveredTime;
			}

			/**
			 * Sets the value of the lastRollOveredTime property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setLastRollOveredTime(String value) {
				this.lastRollOveredTime = value;
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * 
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="OriginType" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;choice>
			 *           &lt;element name="OfferingKey">
			 *             &lt;complexType>
			 *               &lt;complexContent>
			 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                   &lt;sequence>
			 *                     &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
			 *                     &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *                   &lt;/sequence>
			 *                 &lt;/restriction>
			 *               &lt;/complexContent>
			 *             &lt;/complexType>
			 *           &lt;/element>
			 *           &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}long"/>
			 *           &lt;element name="FileID" type="{http://www.w3.org/2001/XMLSchema}long"/>
			 *           &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
			 *         &lt;/choice>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "originType", "offeringKey", "planID", "fileID", "freeUnitInstanceID" })
			public static class FreeUnitOrigin {
				@XmlElement(name = "OriginType", required = true)
				protected String originType;
				@XmlElement(name = "OfferingKey")
				protected QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey offeringKey;
				@XmlElementRef(name = "PlanID", namespace = "http://www.huawei.com/bme/cbsinterface/bbservices", type = JAXBElement.class, required = false)
				protected JAXBElement<Long> planID;
				@XmlElementRef(name = "FileID", namespace = "http://www.huawei.com/bme/cbsinterface/bbservices", type = JAXBElement.class, required = false)
				protected JAXBElement<Long> fileID;
				@XmlElementRef(name = "FreeUnitInstanceID", namespace = "http://www.huawei.com/bme/cbsinterface/bbservices", type = JAXBElement.class, required = false)
				protected JAXBElement<Long> freeUnitInstanceID;

				/**
				 * Gets the value of the originType property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getOriginType() {
					return originType;
				}

				/**
				 * Sets the value of the originType property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setOriginType(String value) {
					this.originType = value;
				}

				/**
				 * Gets the value of the offeringKey property.
				 * 
				 * @return possible object is
				 *         {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey }
				 * 
				 */
				public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey getOfferingKey() {
					return offeringKey;
				}

				/**
				 * Sets the value of the offeringKey property.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey }
				 * 
				 */
				public void setOfferingKey(
						QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey value) {
					this.offeringKey = value;
				}

				/**
				 * Gets the value of the planID property.
				 * 
				 * @return possible object is {@link JAXBElement
				 *         }{@code <}{@link Long }{@code >}
				 * 
				 */
				public JAXBElement<Long> getPlanID() {
					return planID;
				}

				/**
				 * Sets the value of the planID property.
				 * 
				 * @param value
				 *            allowed object is {@link JAXBElement
				 *            }{@code <}{@link Long }{@code >}
				 * 
				 */
				public void setPlanID(JAXBElement<Long> value) {
					this.planID = value;
				}

				/**
				 * Gets the value of the fileID property.
				 * 
				 * @return possible object is {@link JAXBElement
				 *         }{@code <}{@link Long }{@code >}
				 * 
				 */
				public JAXBElement<Long> getFileID() {
					return fileID;
				}

				/**
				 * Sets the value of the fileID property.
				 * 
				 * @param value
				 *            allowed object is {@link JAXBElement
				 *            }{@code <}{@link Long }{@code >}
				 * 
				 */
				public void setFileID(JAXBElement<Long> value) {
					this.fileID = value;
				}

				/**
				 * Gets the value of the freeUnitInstanceID property.
				 * 
				 * @return possible object is {@link JAXBElement
				 *         }{@code <}{@link Long }{@code >}
				 * 
				 */
				public JAXBElement<Long> getFreeUnitInstanceID() {
					return freeUnitInstanceID;
				}

				/**
				 * Sets the value of the freeUnitInstanceID property.
				 * 
				 * @param value
				 *            allowed object is {@link JAXBElement
				 *            }{@code <}{@link Long }{@code >}
				 * 
				 */
				public void setFreeUnitInstanceID(JAXBElement<Long> value) {
					this.freeUnitInstanceID = value;
				}

				/**
				 * <p>
				 * Java class for anonymous complex type.
				 * 
				 * <p>
				 * The following schema fragment specifies the expected content
				 * contained within this class.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
				 *         &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "offeringID", "purchaseSeq" })
				public static class OfferingKey {
					@XmlElement(name = "OfferingID", required = true, nillable = true)
					protected BigInteger offeringID;
					@XmlElement(name = "PurchaseSeq")
					protected String purchaseSeq;

					/**
					 * Gets the value of the offeringID property.
					 * 
					 * @return possible object is {@link BigInteger }
					 * 
					 */
					public BigInteger getOfferingID() {
						return offeringID;
					}

					/**
					 * Sets the value of the offeringID property.
					 * 
					 * @param value
					 *            allowed object is {@link BigInteger }
					 * 
					 */
					public void setOfferingID(BigInteger value) {
						this.offeringID = value;
					}

					/**
					 * Gets the value of the purchaseSeq property.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getPurchaseSeq() {
						return purchaseSeq;
					}

					/**
					 * Sets the value of the purchaseSeq property.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setPurchaseSeq(String value) {
						this.purchaseSeq = value;
					}
				}
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * 
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="InitialType" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;choice>
			 *           &lt;element name="OfferingKey">
			 *             &lt;complexType>
			 *               &lt;complexContent>
			 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                   &lt;sequence>
			 *                     &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
			 *                     &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *                   &lt;/sequence>
			 *                 &lt;/restriction>
			 *               &lt;/complexContent>
			 *             &lt;/complexType>
			 *           &lt;/element>
			 *           &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}long"/>
			 *         &lt;/choice>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "initialType", "offeringKey", "planID" })
			public static class InitialOrigin {
				@XmlElement(name = "InitialType", required = true)
				protected String initialType;
				@XmlElement(name = "OfferingKey")
				protected QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.OfferingKey offeringKey;
				@XmlElementRef(name = "PlanID", namespace = "http://www.huawei.com/bme/cbsinterface/bbservices", type = JAXBElement.class, required = false)
				protected JAXBElement<Long> planID;

				/**
				 * Gets the value of the initialType property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getInitialType() {
					return initialType;
				}

				/**
				 * Sets the value of the initialType property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setInitialType(String value) {
					this.initialType = value;
				}

				/**
				 * Gets the value of the offeringKey property.
				 * 
				 * @return possible object is
				 *         {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.OfferingKey }
				 * 
				 */
				public QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.OfferingKey getOfferingKey() {
					return offeringKey;
				}

				/**
				 * Sets the value of the offeringKey property.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.OfferingKey }
				 * 
				 */
				public void setOfferingKey(
						QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.InitialOrigin.OfferingKey value) {
					this.offeringKey = value;
				}

				/**
				 * Gets the value of the planID property.
				 * 
				 * @return possible object is {@link JAXBElement
				 *         }{@code <}{@link Long }{@code >}
				 * 
				 */
				public JAXBElement<Long> getPlanID() {
					return planID;
				}

				/**
				 * Sets the value of the planID property.
				 * 
				 * @param value
				 *            allowed object is {@link JAXBElement
				 *            }{@code <}{@link Long }{@code >}
				 * 
				 */
				public void setPlanID(JAXBElement<Long> value) {
					this.planID = value;
				}

				/**
				 * <p>
				 * Java class for anonymous complex type.
				 * 
				 * <p>
				 * The following schema fragment specifies the expected content
				 * contained within this class.
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
				 *         &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
				 *       &lt;/sequence>
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 * 
				 * 
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "offeringID", "purchaseSeq" })
				public static class OfferingKey {
					@XmlElement(name = "OfferingID", required = true, nillable = true)
					protected BigInteger offeringID;
					@XmlElement(name = "PurchaseSeq")
					protected String purchaseSeq;

					/**
					 * Gets the value of the offeringID property.
					 * 
					 * @return possible object is {@link BigInteger }
					 * 
					 */
					public BigInteger getOfferingID() {
						return offeringID;
					}

					/**
					 * Sets the value of the offeringID property.
					 * 
					 * @param value
					 *            allowed object is {@link BigInteger }
					 * 
					 */
					public void setOfferingID(BigInteger value) {
						this.offeringID = value;
					}

					/**
					 * Gets the value of the purchaseSeq property.
					 * 
					 * @return possible object is {@link String }
					 * 
					 */
					public String getPurchaseSeq() {
						return purchaseSeq;
					}

					/**
					 * Sets the value of the purchaseSeq property.
					 * 
					 * @param value
					 *            allowed object is {@link String }
					 * 
					 */
					public void setPurchaseSeq(String value) {
						this.purchaseSeq = value;
					}
				}
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "primaryIdentity", "subscriberKey", "usedAmount" })
		public static class MemberUsageList {
			@XmlElement(name = "PrimaryIdentity", required = true)
			protected String primaryIdentity;
			@XmlElement(name = "SubscriberKey", required = true)
			protected String subscriberKey;
			@XmlElement(name = "UsedAmount")
			protected long usedAmount;

			/**
			 * Gets the value of the primaryIdentity property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPrimaryIdentity() {
				return primaryIdentity;
			}

			/**
			 * Sets the value of the primaryIdentity property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPrimaryIdentity(String value) {
				this.primaryIdentity = value;
			}

			/**
			 * Gets the value of the subscriberKey property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getSubscriberKey() {
				return subscriberKey;
			}

			/**
			 * Sets the value of the subscriberKey property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setSubscriberKey(String value) {
				this.subscriberKey = value;
			}

			/**
			 * Gets the value of the usedAmount property.
			 * 
			 */
			public long getUsedAmount() {
				return usedAmount;
			}

			/**
			 * Sets the value of the usedAmount property.
			 * 
			 */
			public void setUsedAmount(long value) {
				this.usedAmount = value;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="SharedPrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="OfferingKey" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "sharedPrimaryIdentity", "offeringKey", "usedAmount", "freeUnitType",
			"measureUnit" })
	public static class ShareUsageList {
		@XmlElement(name = "SharedPrimaryIdentity", required = true)
		protected String sharedPrimaryIdentity;
		@XmlElement(name = "OfferingKey")
		protected QueryFreeUnitResult.ShareUsageList.OfferingKey offeringKey;
		@XmlElement(name = "UsedAmount")
		protected long usedAmount;
		@XmlElement(name = "FreeUnitType", required = true)
		protected String freeUnitType;
		@XmlElement(name = "MeasureUnit", required = true)
		protected String measureUnit;

		/**
		 * Gets the value of the sharedPrimaryIdentity property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSharedPrimaryIdentity() {
			return sharedPrimaryIdentity;
		}

		/**
		 * Sets the value of the sharedPrimaryIdentity property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSharedPrimaryIdentity(String value) {
			this.sharedPrimaryIdentity = value;
		}

		/**
		 * Gets the value of the offeringKey property.
		 * 
		 * @return possible object is
		 *         {@link QueryFreeUnitResult.ShareUsageList.OfferingKey }
		 * 
		 */
		public QueryFreeUnitResult.ShareUsageList.OfferingKey getOfferingKey() {
			return offeringKey;
		}

		/**
		 * Sets the value of the offeringKey property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link QueryFreeUnitResult.ShareUsageList.OfferingKey }
		 * 
		 */
		public void setOfferingKey(QueryFreeUnitResult.ShareUsageList.OfferingKey value) {
			this.offeringKey = value;
		}

		/**
		 * Gets the value of the usedAmount property.
		 * 
		 */
		public long getUsedAmount() {
			return usedAmount;
		}

		/**
		 * Sets the value of the usedAmount property.
		 * 
		 */
		public void setUsedAmount(long value) {
			this.usedAmount = value;
		}

		/**
		 * Gets the value of the freeUnitType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getFreeUnitType() {
			return freeUnitType;
		}

		/**
		 * Sets the value of the freeUnitType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setFreeUnitType(String value) {
			this.freeUnitType = value;
		}

		/**
		 * Gets the value of the measureUnit property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMeasureUnit() {
			return measureUnit;
		}

		/**
		 * Sets the value of the measureUnit property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setMeasureUnit(String value) {
			this.measureUnit = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="PurchaseSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "offeringID", "purchaseSeq" })
		public static class OfferingKey {
			@XmlElement(name = "OfferingID", required = true, nillable = true)
			protected BigInteger offeringID;
			@XmlElement(name = "PurchaseSeq")
			protected String purchaseSeq;

			/**
			 * Gets the value of the offeringID property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getOfferingID() {
				return offeringID;
			}

			/**
			 * Sets the value of the offeringID property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			public void setOfferingID(BigInteger value) {
				this.offeringID = value;
			}

			/**
			 * Gets the value of the purchaseSeq property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPurchaseSeq() {
				return purchaseSeq;
			}

			/**
			 * Sets the value of the purchaseSeq property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPurchaseSeq(String value) {
				this.purchaseSeq = value;
			}
		}
	}
}
