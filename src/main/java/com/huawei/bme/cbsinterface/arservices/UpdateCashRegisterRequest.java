
package com.huawei.bme.cbsinterface.arservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;


/**
 * <p>Java class for UpdateCashRegisterRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateCashRegisterRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BatchNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionList" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PaymentMethodID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="TransactionAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateCashRegisterRequest", propOrder = {
    "batchNo",
    "transactionID",
    "transactionTime",
    "currencyID",
    "transactionList",
    "operID"
})
public class UpdateCashRegisterRequest {

    @XmlElement(name = "BatchNo")
    protected long batchNo;
    @XmlElement(name = "TransactionID")
    protected String transactionID;
    @XmlElement(name = "TransactionTime", required = true)
    protected String transactionTime;
    @XmlElement(name = "CurrencyID")
    protected String currencyID;
    @XmlElement(name = "TransactionList", required = true)
    protected List<UpdateCashRegisterRequest.TransactionList> transactionList;
    @XmlElement(name = "OperID")
    protected long operID;

    /**
     * Gets the value of the batchNo property.
     * 
     */
    public long getBatchNo() {
        return batchNo;
    }

    /**
     * Sets the value of the batchNo property.
     * 
     */
    public void setBatchNo(long value) {
        this.batchNo = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the transactionTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionTime() {
        return transactionTime;
    }

    /**
     * Sets the value of the transactionTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionTime(String value) {
        this.transactionTime = value;
    }

    /**
     * Gets the value of the currencyID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyID() {
        return currencyID;
    }

    /**
     * Sets the value of the currencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyID(String value) {
        this.currencyID = value;
    }

    /**
     * Gets the value of the transactionList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateCashRegisterRequest.TransactionList }
     * 
     * 
     */
    public List<UpdateCashRegisterRequest.TransactionList> getTransactionList() {
        if (transactionList == null) {
            transactionList = new ArrayList<UpdateCashRegisterRequest.TransactionList>();
        }
        return this.transactionList;
    }

    /**
     * Gets the value of the operID property.
     * 
     */
    public long getOperID() {
        return operID;
    }

    /**
     * Sets the value of the operID property.
     * 
     */
    public void setOperID(long value) {
        this.operID = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PaymentMethodID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="TransactionAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paymentMethodID",
        "transactionAmount",
        "additionalProperty"
    })
    public static class TransactionList {

        @XmlElement(name = "PaymentMethodID")
        protected long paymentMethodID;
        @XmlElement(name = "TransactionAmount")
        protected long transactionAmount;
        @XmlElement(name = "AdditionalProperty")
        protected SimpleProperty additionalProperty;

        /**
         * Gets the value of the paymentMethodID property.
         * 
         */
        public long getPaymentMethodID() {
            return paymentMethodID;
        }

        /**
         * Sets the value of the paymentMethodID property.
         * 
         */
        public void setPaymentMethodID(long value) {
            this.paymentMethodID = value;
        }

        /**
         * Gets the value of the transactionAmount property.
         * 
         */
        public long getTransactionAmount() {
            return transactionAmount;
        }

        /**
         * Sets the value of the transactionAmount property.
         * 
         */
        public void setTransactionAmount(long value) {
            this.transactionAmount = value;
        }

        /**
         * Gets the value of the additionalProperty property.
         * 
         * @return
         *     possible object is
         *     {@link SimpleProperty }
         *     
         */
        public SimpleProperty getAdditionalProperty() {
            return additionalProperty;
        }

        /**
         * Sets the value of the additionalProperty property.
         * 
         * @param value
         *     allowed object is
         *     {@link SimpleProperty }
         *     
         */
        public void setAdditionalProperty(SimpleProperty value) {
            this.additionalProperty = value;
        }

    }

}
