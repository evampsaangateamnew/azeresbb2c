
package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryInvoiceResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryInvoiceResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoiceInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="InvoiceNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BillCycleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BillCycleBeginTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BillCycleEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InvoiceAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="OpenAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="DisputeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="InvoiceDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SettleDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="InvoiceDetail" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="ServiceCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ChargeAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="OpenAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="DisputeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryInvoiceResult", propOrder = {
    "invoiceInfo"
})
public class QueryInvoiceResult {

    @XmlElement(name = "InvoiceInfo")
    protected List<QueryInvoiceResult.InvoiceInfo> invoiceInfo;

    /**
     * Gets the value of the invoiceInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the invoiceInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvoiceInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryInvoiceResult.InvoiceInfo }
     * 
     * 
     */
    public List<QueryInvoiceResult.InvoiceInfo> getInvoiceInfo() {
        if (invoiceInfo == null) {
            invoiceInfo = new ArrayList<QueryInvoiceResult.InvoiceInfo>();
        }
        return this.invoiceInfo;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="InvoiceNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BillCycleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BillCycleBeginTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BillCycleEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InvoiceAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="OpenAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="DisputeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="InvoiceDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SettleDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="InvoiceDetail" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="ServiceCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ChargeAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="OpenAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="DisputeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "custKey",
        "subKey",
        "primaryIdentity",
        "transType",
        "invoiceID",
        "invoiceNo",
        "billCycleID",
        "billCycleBeginTime",
        "billCycleEndTime",
        "invoiceAmount",
        "openAmount",
        "disputeAmount",
        "currencyID",
        "invoiceDate",
        "dueDate",
        "settleDate",
        "status",
        "invoiceDetail"
    })
    public static class InvoiceInfo {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "CustKey", required = true)
        protected String custKey;
        @XmlElement(name = "SubKey")
        protected String subKey;
        @XmlElement(name = "PrimaryIdentity")
        protected String primaryIdentity;
        @XmlElement(name = "TransType", required = true)
        protected String transType;
        @XmlElement(name = "InvoiceID")
        protected long invoiceID;
        @XmlElement(name = "InvoiceNo", required = true)
        protected String invoiceNo;
        @XmlElement(name = "BillCycleID")
        protected String billCycleID;
        @XmlElement(name = "BillCycleBeginTime")
        protected String billCycleBeginTime;
        @XmlElement(name = "BillCycleEndTime")
        protected String billCycleEndTime;
        @XmlElement(name = "InvoiceAmount")
        protected long invoiceAmount;
        @XmlElement(name = "OpenAmount")
        protected long openAmount;
        @XmlElement(name = "DisputeAmount")
        protected Long disputeAmount;
        @XmlElement(name = "CurrencyID", required = true)
        protected BigInteger currencyID;
        @XmlElement(name = "InvoiceDate", required = true)
        protected String invoiceDate;
        @XmlElement(name = "DueDate", required = true)
        protected String dueDate;
        @XmlElement(name = "SettleDate")
        protected String settleDate;
        @XmlElement(name = "Status", required = true)
        protected String status;
        @XmlElement(name = "InvoiceDetail", required = true)
        protected List<QueryInvoiceResult.InvoiceInfo.InvoiceDetail> invoiceDetail;

        /**
         * Gets the value of the acctKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Sets the value of the acctKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Gets the value of the custKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustKey() {
            return custKey;
        }

        /**
         * Sets the value of the custKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustKey(String value) {
            this.custKey = value;
        }

        /**
         * Gets the value of the subKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubKey() {
            return subKey;
        }

        /**
         * Sets the value of the subKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubKey(String value) {
            this.subKey = value;
        }

        /**
         * Gets the value of the primaryIdentity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryIdentity() {
            return primaryIdentity;
        }

        /**
         * Sets the value of the primaryIdentity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryIdentity(String value) {
            this.primaryIdentity = value;
        }

        /**
         * Gets the value of the transType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransType() {
            return transType;
        }

        /**
         * Sets the value of the transType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransType(String value) {
            this.transType = value;
        }

        /**
         * Gets the value of the invoiceID property.
         * 
         */
        public long getInvoiceID() {
            return invoiceID;
        }

        /**
         * Sets the value of the invoiceID property.
         * 
         */
        public void setInvoiceID(long value) {
            this.invoiceID = value;
        }

        /**
         * Gets the value of the invoiceNo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvoiceNo() {
            return invoiceNo;
        }

        /**
         * Sets the value of the invoiceNo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvoiceNo(String value) {
            this.invoiceNo = value;
        }

        /**
         * Gets the value of the billCycleID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillCycleID() {
            return billCycleID;
        }

        /**
         * Sets the value of the billCycleID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillCycleID(String value) {
            this.billCycleID = value;
        }

        /**
         * Gets the value of the billCycleBeginTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillCycleBeginTime() {
            return billCycleBeginTime;
        }

        /**
         * Sets the value of the billCycleBeginTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillCycleBeginTime(String value) {
            this.billCycleBeginTime = value;
        }

        /**
         * Gets the value of the billCycleEndTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillCycleEndTime() {
            return billCycleEndTime;
        }

        /**
         * Sets the value of the billCycleEndTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillCycleEndTime(String value) {
            this.billCycleEndTime = value;
        }

        /**
         * Gets the value of the invoiceAmount property.
         * 
         */
        public long getInvoiceAmount() {
            return invoiceAmount;
        }

        /**
         * Sets the value of the invoiceAmount property.
         * 
         */
        public void setInvoiceAmount(long value) {
            this.invoiceAmount = value;
        }

        /**
         * Gets the value of the openAmount property.
         * 
         */
        public long getOpenAmount() {
            return openAmount;
        }

        /**
         * Sets the value of the openAmount property.
         * 
         */
        public void setOpenAmount(long value) {
            this.openAmount = value;
        }

        /**
         * Gets the value of the disputeAmount property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getDisputeAmount() {
            return disputeAmount;
        }

        /**
         * Sets the value of the disputeAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setDisputeAmount(Long value) {
            this.disputeAmount = value;
        }

        /**
         * Gets the value of the currencyID property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Sets the value of the currencyID property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Gets the value of the invoiceDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvoiceDate() {
            return invoiceDate;
        }

        /**
         * Sets the value of the invoiceDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvoiceDate(String value) {
            this.invoiceDate = value;
        }

        /**
         * Gets the value of the dueDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDueDate() {
            return dueDate;
        }

        /**
         * Sets the value of the dueDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDueDate(String value) {
            this.dueDate = value;
        }

        /**
         * Gets the value of the settleDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSettleDate() {
            return settleDate;
        }

        /**
         * Sets the value of the settleDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSettleDate(String value) {
            this.settleDate = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the invoiceDetail property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the invoiceDetail property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInvoiceDetail().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryInvoiceResult.InvoiceInfo.InvoiceDetail }
         * 
         * 
         */
        public List<QueryInvoiceResult.InvoiceInfo.InvoiceDetail> getInvoiceDetail() {
            if (invoiceDetail == null) {
                invoiceDetail = new ArrayList<QueryInvoiceResult.InvoiceInfo.InvoiceDetail>();
            }
            return this.invoiceDetail;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="ServiceCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ChargeAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="OpenAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="DisputeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "invoiceID",
            "serviceCategory",
            "chargeCode",
            "chargeAmount",
            "openAmount",
            "disputeAmount",
            "currencyID",
            "status"
        })
        public static class InvoiceDetail {

            @XmlElement(name = "InvoiceID")
            protected long invoiceID;
            @XmlElement(name = "ServiceCategory", required = true)
            protected String serviceCategory;
            @XmlElement(name = "ChargeCode")
            protected String chargeCode;
            @XmlElement(name = "ChargeAmount")
            protected long chargeAmount;
            @XmlElement(name = "OpenAmount")
            protected long openAmount;
            @XmlElement(name = "DisputeAmount")
            protected Long disputeAmount;
            @XmlElement(name = "CurrencyID", required = true)
            protected BigInteger currencyID;
            @XmlElement(name = "Status", required = true)
            protected String status;

            /**
             * Gets the value of the invoiceID property.
             * 
             */
            public long getInvoiceID() {
                return invoiceID;
            }

            /**
             * Sets the value of the invoiceID property.
             * 
             */
            public void setInvoiceID(long value) {
                this.invoiceID = value;
            }

            /**
             * Gets the value of the serviceCategory property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getServiceCategory() {
                return serviceCategory;
            }

            /**
             * Sets the value of the serviceCategory property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setServiceCategory(String value) {
                this.serviceCategory = value;
            }

            /**
             * Gets the value of the chargeCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChargeCode() {
                return chargeCode;
            }

            /**
             * Sets the value of the chargeCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChargeCode(String value) {
                this.chargeCode = value;
            }

            /**
             * Gets the value of the chargeAmount property.
             * 
             */
            public long getChargeAmount() {
                return chargeAmount;
            }

            /**
             * Sets the value of the chargeAmount property.
             * 
             */
            public void setChargeAmount(long value) {
                this.chargeAmount = value;
            }

            /**
             * Gets the value of the openAmount property.
             * 
             */
            public long getOpenAmount() {
                return openAmount;
            }

            /**
             * Sets the value of the openAmount property.
             * 
             */
            public void setOpenAmount(long value) {
                this.openAmount = value;
            }

            /**
             * Gets the value of the disputeAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getDisputeAmount() {
                return disputeAmount;
            }

            /**
             * Sets the value of the disputeAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setDisputeAmount(Long value) {
                this.disputeAmount = value;
            }

            /**
             * Gets the value of the currencyID property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCurrencyID() {
                return currencyID;
            }

            /**
             * Sets the value of the currencyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCurrencyID(BigInteger value) {
                this.currencyID = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

        }

    }

}
