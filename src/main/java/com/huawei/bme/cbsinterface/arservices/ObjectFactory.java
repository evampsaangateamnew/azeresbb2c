
package com.huawei.bme.cbsinterface.arservices;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.bme.cbsinterface.arservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.bme.cbsinterface.arservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QueryInvoicePaymentResult }
     * 
     */
    public QueryInvoicePaymentResult createQueryInvoicePaymentResult() {
        return new QueryInvoicePaymentResult();
    }

    /**
     * Create an instance of {@link QueryInvoicePaymentResult.InvoicePaymentInfo }
     * 
     */
    public QueryInvoicePaymentResult.InvoicePaymentInfo createQueryInvoicePaymentResultInvoicePaymentInfo() {
        return new QueryInvoicePaymentResult.InvoicePaymentInfo();
    }

    /**
     * Create an instance of {@link Refund2ARRequest }
     * 
     */
    public Refund2ARRequest createRefund2ARRequest() {
        return new Refund2ARRequest();
    }

    /**
     * Create an instance of {@link Refund2ARRequest.RefundObj }
     * 
     */
    public Refund2ARRequest.RefundObj createRefund2ARRequestRefundObj() {
        return new Refund2ARRequest.RefundObj();
    }

    /**
     * Create an instance of {@link RechargeResult }
     * 
     */
    public RechargeResult createRechargeResult() {
        return new RechargeResult();
    }

    /**
     * Create an instance of {@link RechargeResult.LifeCycleChgInfo }
     * 
     */
    public RechargeResult.LifeCycleChgInfo createRechargeResultLifeCycleChgInfo() {
        return new RechargeResult.LifeCycleChgInfo();
    }

    /**
     * Create an instance of {@link RechargeResult.RechargeBonus }
     * 
     */
    public RechargeResult.RechargeBonus createRechargeResultRechargeBonus() {
        return new RechargeResult.RechargeBonus();
    }

    /**
     * Create an instance of {@link QueryInvoiceResult }
     * 
     */
    public QueryInvoiceResult createQueryInvoiceResult() {
        return new QueryInvoiceResult();
    }

    /**
     * Create an instance of {@link QueryInvoiceResult.InvoiceInfo }
     * 
     */
    public QueryInvoiceResult.InvoiceInfo createQueryInvoiceResultInvoiceInfo() {
        return new QueryInvoiceResult.InvoiceInfo();
    }

    /**
     * Create an instance of {@link TransferBalanceRequest }
     * 
     */
    public TransferBalanceRequest createTransferBalanceRequest() {
        return new TransferBalanceRequest();
    }

    /**
     * Create an instance of {@link CheckCashRegisterRequest }
     * 
     */
    public CheckCashRegisterRequest createCheckCashRegisterRequest() {
        return new CheckCashRegisterRequest();
    }

    /**
     * Create an instance of {@link QueryBalanceResult }
     * 
     */
    public QueryBalanceResult createQueryBalanceResult() {
        return new QueryBalanceResult();
    }

    /**
     * Create an instance of {@link QueryBalanceResult.AcctList }
     * 
     */
    public QueryBalanceResult.AcctList createQueryBalanceResultAcctList() {
        return new QueryBalanceResult.AcctList();
    }

    /**
     * Create an instance of {@link QueryBalanceResult.AcctList.AccountCredit }
     * 
     */
    public QueryBalanceResult.AcctList.AccountCredit createQueryBalanceResultAcctListAccountCredit() {
        return new QueryBalanceResult.AcctList.AccountCredit();
    }

    /**
     * Create an instance of {@link QueryBalanceResult.AcctList.OutStandingList }
     * 
     */
    public QueryBalanceResult.AcctList.OutStandingList createQueryBalanceResultAcctListOutStandingList() {
        return new QueryBalanceResult.AcctList.OutStandingList();
    }

    /**
     * Create an instance of {@link QueryTransferLogResult }
     * 
     */
    public QueryTransferLogResult createQueryTransferLogResult() {
        return new QueryTransferLogResult();
    }

    /**
     * Create an instance of {@link Payment2ARRequest }
     * 
     */
    public Payment2ARRequest createPayment2ARRequest() {
        return new Payment2ARRequest();
    }

    /**
     * Create an instance of {@link Payment2ARRequest.PaymentObj }
     * 
     */
    public Payment2ARRequest.PaymentObj createPayment2ARRequestPaymentObj() {
        return new Payment2ARRequest.PaymentObj();
    }

    /**
     * Create an instance of {@link QueryBalanceRequest }
     * 
     */
    public QueryBalanceRequest createQueryBalanceRequest() {
        return new QueryBalanceRequest();
    }

    /**
     * Create an instance of {@link QueryBalanceRequest.QueryObj }
     * 
     */
    public QueryBalanceRequest.QueryObj createQueryBalanceRequestQueryObj() {
        return new QueryBalanceRequest.QueryObj();
    }

    /**
     * Create an instance of {@link Payment2ARRollBackRequest }
     * 
     */
    public Payment2ARRollBackRequest createPayment2ARRollBackRequest() {
        return new Payment2ARRollBackRequest();
    }

    /**
     * Create an instance of {@link Payment2ARRollBackRequest.PaymentObj }
     * 
     */
    public Payment2ARRollBackRequest.PaymentObj createPayment2ARRollBackRequestPaymentObj() {
        return new Payment2ARRollBackRequest.PaymentObj();
    }

    /**
     * Create an instance of {@link QueryRechargeLogRequest }
     * 
     */
    public QueryRechargeLogRequest createQueryRechargeLogRequest() {
        return new QueryRechargeLogRequest();
    }

    /**
     * Create an instance of {@link QueryRechargeLogRequest.QueryObj }
     * 
     */
    public QueryRechargeLogRequest.QueryObj createQueryRechargeLogRequestQueryObj() {
        return new QueryRechargeLogRequest.QueryObj();
    }

    /**
     * Create an instance of {@link QueryInvoiceRequest }
     * 
     */
    public QueryInvoiceRequest createQueryInvoiceRequest() {
        return new QueryInvoiceRequest();
    }

    /**
     * Create an instance of {@link QueryAdjustLogResult }
     * 
     */
    public QueryAdjustLogResult createQueryAdjustLogResult() {
        return new QueryAdjustLogResult();
    }

    /**
     * Create an instance of {@link QueryAdjustLogResult.AdjustInfo }
     * 
     */
    public QueryAdjustLogResult.AdjustInfo createQueryAdjustLogResultAdjustInfo() {
        return new QueryAdjustLogResult.AdjustInfo();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult }
     * 
     */
    public QueryRechargeLogResult createQueryRechargeLogResult() {
        return new QueryRechargeLogResult();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult.RechargeInfo }
     * 
     */
    public QueryRechargeLogResult.RechargeInfo createQueryRechargeLogResultRechargeInfo() {
        return new QueryRechargeLogResult.RechargeInfo();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus }
     * 
     */
    public QueryRechargeLogResult.RechargeInfo.RechargeBonus createQueryRechargeLogResultRechargeInfoRechargeBonus() {
        return new QueryRechargeLogResult.RechargeInfo.RechargeBonus();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo }
     * 
     */
    public QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo createQueryRechargeLogResultRechargeInfoLifeCycleChgInfo() {
        return new QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo();
    }

    /**
     * Create an instance of {@link QueryAdjustLogRequest }
     * 
     */
    public QueryAdjustLogRequest createQueryAdjustLogRequest() {
        return new QueryAdjustLogRequest();
    }

    /**
     * Create an instance of {@link QueryAdjustLogRequest.QueryDetailInfo }
     * 
     */
    public QueryAdjustLogRequest.QueryDetailInfo createQueryAdjustLogRequestQueryDetailInfo() {
        return new QueryAdjustLogRequest.QueryDetailInfo();
    }

    /**
     * Create an instance of {@link QueryAdjustLogRequest.QueryObj }
     * 
     */
    public QueryAdjustLogRequest.QueryObj createQueryAdjustLogRequestQueryObj() {
        return new QueryAdjustLogRequest.QueryObj();
    }

    /**
     * Create an instance of {@link RechargeRequest }
     * 
     */
    public RechargeRequest createRechargeRequest() {
        return new RechargeRequest();
    }

    /**
     * Create an instance of {@link RechargeRequest.RechargeInfo }
     * 
     */
    public RechargeRequest.RechargeInfo createRechargeRequestRechargeInfo() {
        return new RechargeRequest.RechargeInfo();
    }

    /**
     * Create an instance of {@link RechargeRequest.RechargeObj }
     * 
     */
    public RechargeRequest.RechargeObj createRechargeRequestRechargeObj() {
        return new RechargeRequest.RechargeObj();
    }

    /**
     * Create an instance of {@link QueryInvoicePaymentRequest }
     * 
     */
    public QueryInvoicePaymentRequest createQueryInvoicePaymentRequest() {
        return new QueryInvoicePaymentRequest();
    }

    /**
     * Create an instance of {@link TransferBalanceResult }
     * 
     */
    public TransferBalanceResult createTransferBalanceResult() {
        return new TransferBalanceResult();
    }

    /**
     * Create an instance of {@link TransferBalanceResult.Transferee }
     * 
     */
    public TransferBalanceResult.Transferee createTransferBalanceResultTransferee() {
        return new TransferBalanceResult.Transferee();
    }

    /**
     * Create an instance of {@link TransferBalanceResult.Transferee.LifeCycleChgInfo }
     * 
     */
    public TransferBalanceResult.Transferee.LifeCycleChgInfo createTransferBalanceResultTransfereeLifeCycleChgInfo() {
        return new TransferBalanceResult.Transferee.LifeCycleChgInfo();
    }

    /**
     * Create an instance of {@link TransferBalanceResult.Transferor }
     * 
     */
    public TransferBalanceResult.Transferor createTransferBalanceResultTransferor() {
        return new TransferBalanceResult.Transferor();
    }

    /**
     * Create an instance of {@link TransferBalanceResult.Transferor.LifeCycleChgInfo }
     * 
     */
    public TransferBalanceResult.Transferor.LifeCycleChgInfo createTransferBalanceResultTransferorLifeCycleChgInfo() {
        return new TransferBalanceResult.Transferor.LifeCycleChgInfo();
    }

    /**
     * Create an instance of {@link PaymentResult }
     * 
     */
    public PaymentResult createPaymentResult() {
        return new PaymentResult();
    }

    /**
     * Create an instance of {@link PaymentResult.OutStandingList }
     * 
     */
    public PaymentResult.OutStandingList createPaymentResultOutStandingList() {
        return new PaymentResult.OutStandingList();
    }

    /**
     * Create an instance of {@link PaymentResult.PaymentBonus }
     * 
     */
    public PaymentResult.PaymentBonus createPaymentResultPaymentBonus() {
        return new PaymentResult.PaymentBonus();
    }

    /**
     * Create an instance of {@link QueryPaymentLogRequest }
     * 
     */
    public QueryPaymentLogRequest createQueryPaymentLogRequest() {
        return new QueryPaymentLogRequest();
    }

    /**
     * Create an instance of {@link AdjustmentRequest }
     * 
     */
    public AdjustmentRequest createAdjustmentRequest() {
        return new AdjustmentRequest();
    }

    /**
     * Create an instance of {@link AdjustmentRequest.AdjustmentObj }
     * 
     */
    public AdjustmentRequest.AdjustmentObj createAdjustmentRequestAdjustmentObj() {
        return new AdjustmentRequest.AdjustmentObj();
    }

    /**
     * Create an instance of {@link QueryTransferLogRequest }
     * 
     */
    public QueryTransferLogRequest createQueryTransferLogRequest() {
        return new QueryTransferLogRequest();
    }

    /**
     * Create an instance of {@link QueryTransferLogRequest.QueryObj }
     * 
     */
    public QueryTransferLogRequest.QueryObj createQueryTransferLogRequestQueryObj() {
        return new QueryTransferLogRequest.QueryObj();
    }

    /**
     * Create an instance of {@link RefundRequest }
     * 
     */
    public RefundRequest createRefundRequest() {
        return new RefundRequest();
    }

    /**
     * Create an instance of {@link RefundRequest.RefundObj }
     * 
     */
    public RefundRequest.RefundObj createRefundRequestRefundObj() {
        return new RefundRequest.RefundObj();
    }

    /**
     * Create an instance of {@link RechargeRollBackRequest }
     * 
     */
    public RechargeRollBackRequest createRechargeRollBackRequest() {
        return new RechargeRollBackRequest();
    }

    /**
     * Create an instance of {@link RechargeRollBackRequest.RechargeObj }
     * 
     */
    public RechargeRollBackRequest.RechargeObj createRechargeRollBackRequestRechargeObj() {
        return new RechargeRollBackRequest.RechargeObj();
    }

    /**
     * Create an instance of {@link PaymentRollBackRequest }
     * 
     */
    public PaymentRollBackRequest createPaymentRollBackRequest() {
        return new PaymentRollBackRequest();
    }

    /**
     * Create an instance of {@link PaymentRollBackRequest.PaymentObj }
     * 
     */
    public PaymentRollBackRequest.PaymentObj createPaymentRollBackRequestPaymentObj() {
        return new PaymentRollBackRequest.PaymentObj();
    }

    /**
     * Create an instance of {@link PaymentRequest }
     * 
     */
    public PaymentRequest createPaymentRequest() {
        return new PaymentRequest();
    }

    /**
     * Create an instance of {@link PaymentRequest.PaymentInfo }
     * 
     */
    public PaymentRequest.PaymentInfo createPaymentRequestPaymentInfo() {
        return new PaymentRequest.PaymentInfo();
    }

    /**
     * Create an instance of {@link PaymentRequest.PaymentInfo.CashPayment }
     * 
     */
    public PaymentRequest.PaymentInfo.CashPayment createPaymentRequestPaymentInfoCashPayment() {
        return new PaymentRequest.PaymentInfo.CashPayment();
    }

    /**
     * Create an instance of {@link PaymentRequest.PaymentObj }
     * 
     */
    public PaymentRequest.PaymentObj createPaymentRequestPaymentObj() {
        return new PaymentRequest.PaymentObj();
    }

    /**
     * Create an instance of {@link UpdateCashRegisterRequest }
     * 
     */
    public UpdateCashRegisterRequest createUpdateCashRegisterRequest() {
        return new UpdateCashRegisterRequest();
    }

    /**
     * Create an instance of {@link RechargeRollBackResult }
     * 
     */
    public RechargeRollBackResult createRechargeRollBackResult() {
        return new RechargeRollBackResult();
    }

    /**
     * Create an instance of {@link RechargeRollBackResult.LifeCycleRollBack }
     * 
     */
    public RechargeRollBackResult.LifeCycleRollBack createRechargeRollBackResultLifeCycleRollBack() {
        return new RechargeRollBackResult.LifeCycleRollBack();
    }

    /**
     * Create an instance of {@link RechargeRollBackResult.BonusRollBack }
     * 
     */
    public RechargeRollBackResult.BonusRollBack createRechargeRollBackResultBonusRollBack() {
        return new RechargeRollBackResult.BonusRollBack();
    }

    /**
     * Create an instance of {@link QueryPaymentLogResult }
     * 
     */
    public QueryPaymentLogResult createQueryPaymentLogResult() {
        return new QueryPaymentLogResult();
    }

    /**
     * Create an instance of {@link QueryPaymentLogResult.PaymentInfo }
     * 
     */
    public QueryPaymentLogResult.PaymentInfo createQueryPaymentLogResultPaymentInfo() {
        return new QueryPaymentLogResult.PaymentInfo();
    }

    /**
     * Create an instance of {@link QueryPaymentLogResultMsg }
     * 
     */
    public QueryPaymentLogResultMsg createQueryPaymentLogResultMsg() {
        return new QueryPaymentLogResultMsg();
    }

    /**
     * Create an instance of {@link RechargeRollBackResultMsg }
     * 
     */
    public RechargeRollBackResultMsg createRechargeRollBackResultMsg() {
        return new RechargeRollBackResultMsg();
    }

    /**
     * Create an instance of {@link Payment2ARResultMsg }
     * 
     */
    public Payment2ARResultMsg createPayment2ARResultMsg() {
        return new Payment2ARResultMsg();
    }

    /**
     * Create an instance of {@link UpdateCashRegisterRequestMsg }
     * 
     */
    public UpdateCashRegisterRequestMsg createUpdateCashRegisterRequestMsg() {
        return new UpdateCashRegisterRequestMsg();
    }

    /**
     * Create an instance of {@link RefundResultMsg }
     * 
     */
    public RefundResultMsg createRefundResultMsg() {
        return new RefundResultMsg();
    }

    /**
     * Create an instance of {@link CheckCashRegisterResultMsg }
     * 
     */
    public CheckCashRegisterResultMsg createCheckCashRegisterResultMsg() {
        return new CheckCashRegisterResultMsg();
    }

    /**
     * Create an instance of {@link CheckCashRegisterResult }
     * 
     */
    public CheckCashRegisterResult createCheckCashRegisterResult() {
        return new CheckCashRegisterResult();
    }

    /**
     * Create an instance of {@link PaymentRequestMsg }
     * 
     */
    public PaymentRequestMsg createPaymentRequestMsg() {
        return new PaymentRequestMsg();
    }

    /**
     * Create an instance of {@link PaymentRollBackRequestMsg }
     * 
     */
    public PaymentRollBackRequestMsg createPaymentRollBackRequestMsg() {
        return new PaymentRollBackRequestMsg();
    }

    /**
     * Create an instance of {@link RechargeRollBackRequestMsg }
     * 
     */
    public RechargeRollBackRequestMsg createRechargeRollBackRequestMsg() {
        return new RechargeRollBackRequestMsg();
    }

    /**
     * Create an instance of {@link RefundRequestMsg }
     * 
     */
    public RefundRequestMsg createRefundRequestMsg() {
        return new RefundRequestMsg();
    }

    /**
     * Create an instance of {@link QueryTransferLogRequestMsg }
     * 
     */
    public QueryTransferLogRequestMsg createQueryTransferLogRequestMsg() {
        return new QueryTransferLogRequestMsg();
    }

    /**
     * Create an instance of {@link Adv2DepositResultMsg }
     * 
     */
    public Adv2DepositResultMsg createAdv2DepositResultMsg() {
        return new Adv2DepositResultMsg();
    }

    /**
     * Create an instance of {@link Adv2DepositResult }
     * 
     */
    public Adv2DepositResult createAdv2DepositResult() {
        return new Adv2DepositResult();
    }

    /**
     * Create an instance of {@link Payment2ARRollBackResultMsg }
     * 
     */
    public Payment2ARRollBackResultMsg createPayment2ARRollBackResultMsg() {
        return new Payment2ARRollBackResultMsg();
    }

    /**
     * Create an instance of {@link AdjustmentRequestMsg }
     * 
     */
    public AdjustmentRequestMsg createAdjustmentRequestMsg() {
        return new AdjustmentRequestMsg();
    }

    /**
     * Create an instance of {@link QueryPaymentLogRequestMsg }
     * 
     */
    public QueryPaymentLogRequestMsg createQueryPaymentLogRequestMsg() {
        return new QueryPaymentLogRequestMsg();
    }

    /**
     * Create an instance of {@link PaymentResultMsg }
     * 
     */
    public PaymentResultMsg createPaymentResultMsg() {
        return new PaymentResultMsg();
    }

    /**
     * Create an instance of {@link TransferBalanceResultMsg }
     * 
     */
    public TransferBalanceResultMsg createTransferBalanceResultMsg() {
        return new TransferBalanceResultMsg();
    }

    /**
     * Create an instance of {@link QueryInvoicePaymentRequestMsg }
     * 
     */
    public QueryInvoicePaymentRequestMsg createQueryInvoicePaymentRequestMsg() {
        return new QueryInvoicePaymentRequestMsg();
    }

    /**
     * Create an instance of {@link AdjustmentResultMsg }
     * 
     */
    public AdjustmentResultMsg createAdjustmentResultMsg() {
        return new AdjustmentResultMsg();
    }

    /**
     * Create an instance of {@link AdjustmentResult }
     * 
     */
    public AdjustmentResult createAdjustmentResult() {
        return new AdjustmentResult();
    }

    /**
     * Create an instance of {@link PaymentRollBackResultMsg }
     * 
     */
    public PaymentRollBackResultMsg createPaymentRollBackResultMsg() {
        return new PaymentRollBackResultMsg();
    }

    /**
     * Create an instance of {@link RechargeRequestMsg }
     * 
     */
    public RechargeRequestMsg createRechargeRequestMsg() {
        return new RechargeRequestMsg();
    }

    /**
     * Create an instance of {@link QueryAdjustLogRequestMsg }
     * 
     */
    public QueryAdjustLogRequestMsg createQueryAdjustLogRequestMsg() {
        return new QueryAdjustLogRequestMsg();
    }

    /**
     * Create an instance of {@link UpdateCashRegisterResultMsg }
     * 
     */
    public UpdateCashRegisterResultMsg createUpdateCashRegisterResultMsg() {
        return new UpdateCashRegisterResultMsg();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResultMsg }
     * 
     */
    public QueryRechargeLogResultMsg createQueryRechargeLogResultMsg() {
        return new QueryRechargeLogResultMsg();
    }

    /**
     * Create an instance of {@link QueryAdjustLogResultMsg }
     * 
     */
    public QueryAdjustLogResultMsg createQueryAdjustLogResultMsg() {
        return new QueryAdjustLogResultMsg();
    }

    /**
     * Create an instance of {@link QueryInvoiceRequestMsg }
     * 
     */
    public QueryInvoiceRequestMsg createQueryInvoiceRequestMsg() {
        return new QueryInvoiceRequestMsg();
    }

    /**
     * Create an instance of {@link QueryRechargeLogRequestMsg }
     * 
     */
    public QueryRechargeLogRequestMsg createQueryRechargeLogRequestMsg() {
        return new QueryRechargeLogRequestMsg();
    }

    /**
     * Create an instance of {@link Payment2ARRollBackRequestMsg }
     * 
     */
    public Payment2ARRollBackRequestMsg createPayment2ARRollBackRequestMsg() {
        return new Payment2ARRollBackRequestMsg();
    }

    /**
     * Create an instance of {@link QueryBalanceRequestMsg }
     * 
     */
    public QueryBalanceRequestMsg createQueryBalanceRequestMsg() {
        return new QueryBalanceRequestMsg();
    }

    /**
     * Create an instance of {@link Payment2ARRequestMsg }
     * 
     */
    public Payment2ARRequestMsg createPayment2ARRequestMsg() {
        return new Payment2ARRequestMsg();
    }

    /**
     * Create an instance of {@link QueryTransferLogResultMsg }
     * 
     */
    public QueryTransferLogResultMsg createQueryTransferLogResultMsg() {
        return new QueryTransferLogResultMsg();
    }

    /**
     * Create an instance of {@link QueryBalanceResultMsg }
     * 
     */
    public QueryBalanceResultMsg createQueryBalanceResultMsg() {
        return new QueryBalanceResultMsg();
    }

    /**
     * Create an instance of {@link CheckCashRegisterRequestMsg }
     * 
     */
    public CheckCashRegisterRequestMsg createCheckCashRegisterRequestMsg() {
        return new CheckCashRegisterRequestMsg();
    }

    /**
     * Create an instance of {@link Adv2DepositRequestMsg }
     * 
     */
    public Adv2DepositRequestMsg createAdv2DepositRequestMsg() {
        return new Adv2DepositRequestMsg();
    }

    /**
     * Create an instance of {@link Adv2DepositRequest }
     * 
     */
    public Adv2DepositRequest createAdv2DepositRequest() {
        return new Adv2DepositRequest();
    }

    /**
     * Create an instance of {@link TransferBalanceRequestMsg }
     * 
     */
    public TransferBalanceRequestMsg createTransferBalanceRequestMsg() {
        return new TransferBalanceRequestMsg();
    }

    /**
     * Create an instance of {@link QueryInvoiceResultMsg }
     * 
     */
    public QueryInvoiceResultMsg createQueryInvoiceResultMsg() {
        return new QueryInvoiceResultMsg();
    }

    /**
     * Create an instance of {@link RechargeResultMsg }
     * 
     */
    public RechargeResultMsg createRechargeResultMsg() {
        return new RechargeResultMsg();
    }

    /**
     * Create an instance of {@link Refund2ARRequestMsg }
     * 
     */
    public Refund2ARRequestMsg createRefund2ARRequestMsg() {
        return new Refund2ARRequestMsg();
    }

    /**
     * Create an instance of {@link Refund2ARResultMsg }
     * 
     */
    public Refund2ARResultMsg createRefund2ARResultMsg() {
        return new Refund2ARResultMsg();
    }

    /**
     * Create an instance of {@link QueryInvoicePaymentResultMsg }
     * 
     */
    public QueryInvoicePaymentResultMsg createQueryInvoicePaymentResultMsg() {
        return new QueryInvoicePaymentResultMsg();
    }

    /**
     * Create an instance of {@link UpdateCashRegisterResult }
     * 
     */
    public UpdateCashRegisterResult createUpdateCashRegisterResult() {
        return new UpdateCashRegisterResult();
    }

    /**
     * Create an instance of {@link QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo }
     * 
     */
    public QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo createQueryInvoicePaymentResultInvoicePaymentInfoCardInfo() {
        return new QueryInvoicePaymentResult.InvoicePaymentInfo.CardInfo();
    }

    /**
     * Create an instance of {@link Refund2ARRequest.RefundInfo }
     * 
     */
    public Refund2ARRequest.RefundInfo createRefund2ARRequestRefundInfo() {
        return new Refund2ARRequest.RefundInfo();
    }

    /**
     * Create an instance of {@link Refund2ARRequest.RefundChannel }
     * 
     */
    public Refund2ARRequest.RefundChannel createRefund2ARRequestRefundChannel() {
        return new Refund2ARRequest.RefundChannel();
    }

    /**
     * Create an instance of {@link Refund2ARRequest.RefundObj.AcctAccessCode }
     * 
     */
    public Refund2ARRequest.RefundObj.AcctAccessCode createRefund2ARRequestRefundObjAcctAccessCode() {
        return new Refund2ARRequest.RefundObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus }
     * 
     */
    public RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus createRechargeResultLifeCycleChgInfoOldLifeCycleStatus() {
        return new RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus();
    }

    /**
     * Create an instance of {@link RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus }
     * 
     */
    public RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus createRechargeResultLifeCycleChgInfoNewLifeCycleStatus() {
        return new RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus();
    }

    /**
     * Create an instance of {@link RechargeResult.RechargeBonus.FreeUnitItemList }
     * 
     */
    public RechargeResult.RechargeBonus.FreeUnitItemList createRechargeResultRechargeBonusFreeUnitItemList() {
        return new RechargeResult.RechargeBonus.FreeUnitItemList();
    }

    /**
     * Create an instance of {@link RechargeResult.RechargeBonus.BalanceList }
     * 
     */
    public RechargeResult.RechargeBonus.BalanceList createRechargeResultRechargeBonusBalanceList() {
        return new RechargeResult.RechargeBonus.BalanceList();
    }

    /**
     * Create an instance of {@link QueryInvoiceResult.InvoiceInfo.InvoiceDetail }
     * 
     */
    public QueryInvoiceResult.InvoiceInfo.InvoiceDetail createQueryInvoiceResultInvoiceInfoInvoiceDetail() {
        return new QueryInvoiceResult.InvoiceInfo.InvoiceDetail();
    }

    /**
     * Create an instance of {@link TransferBalanceRequest.TransferorObj }
     * 
     */
 /*   public TransferBalanceRequest.TransferorObj createTransferBalanceRequestTransferorObj() {
        return new TransferBalanceRequest.TransferorObj();
    }

    *//**
     * Create an instance of {@link TransferBalanceRequest.TransfereeObj }
     * 
     *//*
    public TransferBalanceRequest.TransfereeObj createTransferBalanceRequestTransfereeObj() {
        return new TransferBalanceRequest.TransfereeObj();
    }
*/
    /**
     * Create an instance of {@link TransferBalanceRequest.TransferorAcct }
     * 
     */
    public TransferBalanceRequest.TransferorAcct createTransferBalanceRequestTransferorAcct() {
        return new TransferBalanceRequest.TransferorAcct();
    }

    /**
     * Create an instance of {@link TransferBalanceRequest.TransfereeAcct }
     * 
     */
    public TransferBalanceRequest.TransfereeAcct createTransferBalanceRequestTransfereeAcct() {
        return new TransferBalanceRequest.TransfereeAcct();
    }

    /**
     * Create an instance of {@link TransferBalanceRequest.FreeUnitTransferorInfo }
     * 
     */
 /*   public TransferBalanceRequest.FreeUnitTransferorInfo createTransferBalanceRequestFreeUnitTransferorInfo() {
        return new TransferBalanceRequest.FreeUnitTransferorInfo();
    }

    *//**
     * Create an instance of {@link TransferBalanceRequest.CreditTransferorInfo }
     * 
     *//*
    public TransferBalanceRequest.CreditTransferorInfo createTransferBalanceRequestCreditTransferorInfo() {
        return new TransferBalanceRequest.CreditTransferorInfo();
    }*/

    /**
     * Create an instance of {@link CheckCashRegisterRequest.TransactionList }
     * 
     */
    public CheckCashRegisterRequest.TransactionList createCheckCashRegisterRequestTransactionList() {
        return new CheckCashRegisterRequest.TransactionList();
    }

    /**
     * Create an instance of {@link QueryBalanceResult.AcctList.PaymentLimitUsage }
     * 
     */
  /*  public QueryBalanceResult.AcctList.PaymentLimitUsage createQueryBalanceResultAcctListPaymentLimitUsage() {
        return new QueryBalanceResult.AcctList.PaymentLimitUsage();
    }*/

    /**
     * Create an instance of {@link QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo }
     * 
     */
    public QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo createQueryBalanceResultAcctListAccountCreditCreditAmountInfo() {
        return new QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo();
    }

    /**
     * Create an instance of {@link QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail }
     * 
     */
    public QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail createQueryBalanceResultAcctListOutStandingListOutStandingDetail() {
        return new QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail();
    }

    /**
     * Create an instance of {@link QueryTransferLogResult.TransferInfo }
     * 
     */
    public QueryTransferLogResult.TransferInfo createQueryTransferLogResultTransferInfo() {
        return new QueryTransferLogResult.TransferInfo();
    }

    /**
     * Create an instance of {@link Payment2ARRequest.PaymentObj.AcctAccessCode }
     * 
     */
    public Payment2ARRequest.PaymentObj.AcctAccessCode createPayment2ARRequestPaymentObjAcctAccessCode() {
        return new Payment2ARRequest.PaymentObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryBalanceRequest.QueryObj.AcctAccessCode }
     * 
     */
    public QueryBalanceRequest.QueryObj.AcctAccessCode createQueryBalanceRequestQueryObjAcctAccessCode() {
        return new QueryBalanceRequest.QueryObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link Payment2ARRollBackRequest.RollBackFeeValues }
     * 
     */
    public Payment2ARRollBackRequest.RollBackFeeValues createPayment2ARRollBackRequestRollBackFeeValues() {
        return new Payment2ARRollBackRequest.RollBackFeeValues();
    }

    /**
     * Create an instance of {@link Payment2ARRollBackRequest.PaymentObj.AcctAccessCode }
     * 
     */
    public Payment2ARRollBackRequest.PaymentObj.AcctAccessCode createPayment2ARRollBackRequestPaymentObjAcctAccessCode() {
        return new Payment2ARRollBackRequest.PaymentObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryRechargeLogRequest.QueryObj.AcctAccessCode }
     * 
     */
    public QueryRechargeLogRequest.QueryObj.AcctAccessCode createQueryRechargeLogRequestQueryObjAcctAccessCode() {
        return new QueryRechargeLogRequest.QueryObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryInvoiceRequest.AcctAccessCode }
     * 
     */
    public QueryInvoiceRequest.AcctAccessCode createQueryInvoiceRequestAcctAccessCode() {
        return new QueryInvoiceRequest.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryInvoiceRequest.TimePeriod }
     * 
     */
    public QueryInvoiceRequest.TimePeriod createQueryInvoiceRequestTimePeriod() {
        return new QueryInvoiceRequest.TimePeriod();
    }

    /**
     * Create an instance of {@link QueryAdjustLogResult.AdjustInfo.FreeUnitAdjustmentInfo }
     * 
     */
    public QueryAdjustLogResult.AdjustInfo.FreeUnitAdjustmentInfo createQueryAdjustLogResultAdjustInfoFreeUnitAdjustmentInfo() {
        return new QueryAdjustLogResult.AdjustInfo.FreeUnitAdjustmentInfo();
    }

    /**
     * Create an instance of {@link QueryAdjustLogResult.AdjustInfo.BalanceAdjustmentInfo }
     * 
     */
    public QueryAdjustLogResult.AdjustInfo.BalanceAdjustmentInfo createQueryAdjustLogResultAdjustInfoBalanceAdjustmentInfo() {
        return new QueryAdjustLogResult.AdjustInfo.BalanceAdjustmentInfo();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult.RechargeInfo.CardInfo }
     * 
     */
    public QueryRechargeLogResult.RechargeInfo.CardInfo createQueryRechargeLogResultRechargeInfoCardInfo() {
        return new QueryRechargeLogResult.RechargeInfo.CardInfo();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList }
     * 
     */
    public QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList createQueryRechargeLogResultRechargeInfoRechargeBonusFreeUnitItemList() {
        return new QueryRechargeLogResult.RechargeInfo.RechargeBonus.FreeUnitItemList();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList }
     * 
     */
    public QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList createQueryRechargeLogResultRechargeInfoRechargeBonusBalanceList() {
        return new QueryRechargeLogResult.RechargeInfo.RechargeBonus.BalanceList();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus }
     * 
     */
    public QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus createQueryRechargeLogResultRechargeInfoLifeCycleChgInfoOldLifeCycleStatus() {
        return new QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.OldLifeCycleStatus();
    }

    /**
     * Create an instance of {@link QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus }
     * 
     */
    public QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus createQueryRechargeLogResultRechargeInfoLifeCycleChgInfoNewLifeCycleStatus() {
        return new QueryRechargeLogResult.RechargeInfo.LifeCycleChgInfo.NewLifeCycleStatus();
    }

    /**
     * Create an instance of {@link QueryAdjustLogRequest.QueryDetailInfo.FreeUnitInfo }
     * 
     */
    public QueryAdjustLogRequest.QueryDetailInfo.FreeUnitInfo createQueryAdjustLogRequestQueryDetailInfoFreeUnitInfo() {
        return new QueryAdjustLogRequest.QueryDetailInfo.FreeUnitInfo();
    }

    /**
     * Create an instance of {@link QueryAdjustLogRequest.QueryDetailInfo.BalanceInfo }
     * 
     */
    public QueryAdjustLogRequest.QueryDetailInfo.BalanceInfo createQueryAdjustLogRequestQueryDetailInfoBalanceInfo() {
        return new QueryAdjustLogRequest.QueryDetailInfo.BalanceInfo();
    }

    /**
     * Create an instance of {@link QueryAdjustLogRequest.QueryObj.AcctAccessCode }
     * 
     */
    public QueryAdjustLogRequest.QueryObj.AcctAccessCode createQueryAdjustLogRequestQueryObjAcctAccessCode() {
        return new QueryAdjustLogRequest.QueryObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link RechargeRequest.RechargeInfo.CardPayment }
     * 
     */
    public RechargeRequest.RechargeInfo.CardPayment createRechargeRequestRechargeInfoCardPayment() {
        return new RechargeRequest.RechargeInfo.CardPayment();
    }

    /**
     * Create an instance of {@link RechargeRequest.RechargeInfo.CashPayment }
     * 
     */
    public RechargeRequest.RechargeInfo.CashPayment createRechargeRequestRechargeInfoCashPayment() {
        return new RechargeRequest.RechargeInfo.CashPayment();
    }

    /**
     * Create an instance of {@link RechargeRequest.RechargeObj.AcctAccessCode }
     * 
     */
    public RechargeRequest.RechargeObj.AcctAccessCode createRechargeRequestRechargeObjAcctAccessCode() {
        return new RechargeRequest.RechargeObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryInvoicePaymentRequest.AcctAccessCode }
     * 
     */
    public QueryInvoicePaymentRequest.AcctAccessCode createQueryInvoicePaymentRequestAcctAccessCode() {
        return new QueryInvoicePaymentRequest.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryInvoicePaymentRequest.TimePeriod }
     * 
     */
    public QueryInvoicePaymentRequest.TimePeriod createQueryInvoicePaymentRequestTimePeriod() {
        return new QueryInvoicePaymentRequest.TimePeriod();
    }

    /**
     * Create an instance of {@link TransferBalanceResult.Transferee.LifeCycleChgInfo.OldLifeCycleStatus }
     * 
     */
    public TransferBalanceResult.Transferee.LifeCycleChgInfo.OldLifeCycleStatus createTransferBalanceResultTransfereeLifeCycleChgInfoOldLifeCycleStatus() {
        return new TransferBalanceResult.Transferee.LifeCycleChgInfo.OldLifeCycleStatus();
    }

    /**
     * Create an instance of {@link TransferBalanceResult.Transferee.LifeCycleChgInfo.NewLifeCycleStatus }
     * 
     */
    public TransferBalanceResult.Transferee.LifeCycleChgInfo.NewLifeCycleStatus createTransferBalanceResultTransfereeLifeCycleChgInfoNewLifeCycleStatus() {
        return new TransferBalanceResult.Transferee.LifeCycleChgInfo.NewLifeCycleStatus();
    }

    /**
     * Create an instance of {@link TransferBalanceResult.Transferor.LifeCycleChgInfo.OldLifeCycleStatus }
     * 
     */
    public TransferBalanceResult.Transferor.LifeCycleChgInfo.OldLifeCycleStatus createTransferBalanceResultTransferorLifeCycleChgInfoOldLifeCycleStatus() {
        return new TransferBalanceResult.Transferor.LifeCycleChgInfo.OldLifeCycleStatus();
    }

    /**
     * Create an instance of {@link TransferBalanceResult.Transferor.LifeCycleChgInfo.NewLifeCycleStatus }
     * 
     */
    public TransferBalanceResult.Transferor.LifeCycleChgInfo.NewLifeCycleStatus createTransferBalanceResultTransferorLifeCycleChgInfoNewLifeCycleStatus() {
        return new TransferBalanceResult.Transferor.LifeCycleChgInfo.NewLifeCycleStatus();
    }

    /**
     * Create an instance of {@link PaymentResult.OutStandingList.OutStandingDetail }
     * 
     */
    public PaymentResult.OutStandingList.OutStandingDetail createPaymentResultOutStandingListOutStandingDetail() {
        return new PaymentResult.OutStandingList.OutStandingDetail();
    }

    /**
     * Create an instance of {@link PaymentResult.PaymentBonus.FreeUnitItemList }
     * 
     */
    public PaymentResult.PaymentBonus.FreeUnitItemList createPaymentResultPaymentBonusFreeUnitItemList() {
        return new PaymentResult.PaymentBonus.FreeUnitItemList();
    }

    /**
     * Create an instance of {@link PaymentResult.PaymentBonus.BalanceList }
     * 
     */
    public PaymentResult.PaymentBonus.BalanceList createPaymentResultPaymentBonusBalanceList() {
        return new PaymentResult.PaymentBonus.BalanceList();
    }

    /**
     * Create an instance of {@link QueryPaymentLogRequest.AcctAccessCode }
     * 
     */
    public QueryPaymentLogRequest.AcctAccessCode createQueryPaymentLogRequestAcctAccessCode() {
        return new QueryPaymentLogRequest.AcctAccessCode();
    }

    /**
     * Create an instance of {@link AdjustmentRequest.FreeUnitAdjustmentInfo }
     * 
     */
    public AdjustmentRequest.FreeUnitAdjustmentInfo createAdjustmentRequestFreeUnitAdjustmentInfo() {
        return new AdjustmentRequest.FreeUnitAdjustmentInfo();
    }

    /**
     * Create an instance of {@link AdjustmentRequest.AdjustmentObj.AcctAccessCode }
     * 
     */
    public AdjustmentRequest.AdjustmentObj.AcctAccessCode createAdjustmentRequestAdjustmentObjAcctAccessCode() {
        return new AdjustmentRequest.AdjustmentObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryTransferLogRequest.QueryObj.AcctAccessCode }
     * 
     */
    public QueryTransferLogRequest.QueryObj.AcctAccessCode createQueryTransferLogRequestQueryObjAcctAccessCode() {
        return new QueryTransferLogRequest.QueryObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link RefundRequest.RefundInfo }
     * 
     */
    public RefundRequest.RefundInfo createRefundRequestRefundInfo() {
        return new RefundRequest.RefundInfo();
    }

    /**
     * Create an instance of {@link RefundRequest.RefundChannel }
     * 
     */
    public RefundRequest.RefundChannel createRefundRequestRefundChannel() {
        return new RefundRequest.RefundChannel();
    }

    /**
     * Create an instance of {@link RefundRequest.RefundObj.AcctAccessCode }
     * 
     */
    public RefundRequest.RefundObj.AcctAccessCode createRefundRequestRefundObjAcctAccessCode() {
        return new RefundRequest.RefundObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link RechargeRollBackRequest.RechargeObj.AcctAccessCode }
     * 
     */
    public RechargeRollBackRequest.RechargeObj.AcctAccessCode createRechargeRollBackRequestRechargeObjAcctAccessCode() {
        return new RechargeRollBackRequest.RechargeObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link PaymentRollBackRequest.PaymentObj.AcctAccessCode }
     * 
     */
    public PaymentRollBackRequest.PaymentObj.AcctAccessCode createPaymentRollBackRequestPaymentObjAcctAccessCode() {
        return new PaymentRollBackRequest.PaymentObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link PaymentRequest.PaymentInfo.CardPayment }
     * 
     */
    public PaymentRequest.PaymentInfo.CardPayment createPaymentRequestPaymentInfoCardPayment() {
        return new PaymentRequest.PaymentInfo.CardPayment();
    }

    /**
     * Create an instance of {@link PaymentRequest.PaymentInfo.CashPayment.ApplyList }
     * 
     */
    public PaymentRequest.PaymentInfo.CashPayment.ApplyList createPaymentRequestPaymentInfoCashPaymentApplyList() {
        return new PaymentRequest.PaymentInfo.CashPayment.ApplyList();
    }

    /**
     * Create an instance of {@link PaymentRequest.PaymentObj.AcctAccessCode }
     * 
     */
    public PaymentRequest.PaymentObj.AcctAccessCode createPaymentRequestPaymentObjAcctAccessCode() {
        return new PaymentRequest.PaymentObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link UpdateCashRegisterRequest.TransactionList }
     * 
     */
    public UpdateCashRegisterRequest.TransactionList createUpdateCashRegisterRequestTransactionList() {
        return new UpdateCashRegisterRequest.TransactionList();
    }

    /**
     * Create an instance of {@link RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus }
     * 
     */
    public RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus createRechargeRollBackResultLifeCycleRollBackOldLifeCycleStatus() {
        return new RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus();
    }

    /**
     * Create an instance of {@link RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus }
     * 
     */
    public RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus createRechargeRollBackResultLifeCycleRollBackNewLifeCycleStatus() {
        return new RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus();
    }

    /**
     * Create an instance of {@link RechargeRollBackResult.BonusRollBack.FreeUnitItemList }
     * 
     */
    public RechargeRollBackResult.BonusRollBack.FreeUnitItemList createRechargeRollBackResultBonusRollBackFreeUnitItemList() {
        return new RechargeRollBackResult.BonusRollBack.FreeUnitItemList();
    }

    /**
     * Create an instance of {@link RechargeRollBackResult.BonusRollBack.BalanceList }
     * 
     */
    public RechargeRollBackResult.BonusRollBack.BalanceList createRechargeRollBackResultBonusRollBackBalanceList() {
        return new RechargeRollBackResult.BonusRollBack.BalanceList();
    }

    /**
     * Create an instance of {@link QueryPaymentLogResult.PaymentInfo.PaymentDetail }
     * 
     */
    public QueryPaymentLogResult.PaymentInfo.PaymentDetail createQueryPaymentLogResultPaymentInfoPaymentDetail() {
        return new QueryPaymentLogResult.PaymentInfo.PaymentDetail();
    }

    /**
     * Create an instance of {@link QueryPaymentLogResult.PaymentInfo.CardInfo }
     * 
     */
    public QueryPaymentLogResult.PaymentInfo.CardInfo createQueryPaymentLogResultPaymentInfoCardInfo() {
        return new QueryPaymentLogResult.PaymentInfo.CardInfo();
    }

}
