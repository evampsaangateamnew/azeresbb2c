
package com.huawei.bme.cbsinterface.arservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader"/>
 *         &lt;element name="Refund2ARRequest" type="{http://www.huawei.com/bme/cbsinterface/arservices}Refund2ARRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "refund2ARRequest"
})
@XmlRootElement(name = "Refund2ARRequestMsg")
public class Refund2ARRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "Refund2ARRequest", namespace = "", required = true)
    protected Refund2ARRequest refund2ARRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the refund2ARRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Refund2ARRequest }
     *     
     */
    public Refund2ARRequest getRefund2ARRequest() {
        return refund2ARRequest;
    }

    /**
     * Sets the value of the refund2ARRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Refund2ARRequest }
     *     
     */
    public void setRefund2ARRequest(Refund2ARRequest value) {
        this.refund2ARRequest = value;
    }

}
