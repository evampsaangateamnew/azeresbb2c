
package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.AcctBalance;


/**
 * <p>Java class for QueryBalanceResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryBalanceResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctList" maxOccurs="2">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BalanceResult" type="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctBalance" maxOccurs="unbounded"/>
 *                   &lt;element name="ReservedBalanceDetail" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BalanceInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="ReservedAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PaymentLimitUsage" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CorPayRelaList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CorPayRelaAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="PayRelaType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CorPayRelaInstanID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="IndvUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="CorpUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="IndvReserAmountList" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IndvReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CorpReserAmountList" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CorpReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PreMonIndvUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurMonIndvUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="PayByIndvPayOffAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="PayByCorpPayOffAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="OtherFeeAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OutStandingList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BillCycleID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BillCycleBeginTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BillCycleEndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OutStandingDetail" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OutStandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AccountCredit" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="CreditLimitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="TotalCreditAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="TotalUsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="TotalRemainAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="CreditAmountInfo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;element name="LimitClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryBalanceResult", propOrder = {
    "acctList"
})
public class QueryBalanceResult {

    @XmlElement(name = "AcctList", required = true)
    protected List<QueryBalanceResult.AcctList> acctList;

    /**
     * Gets the value of the acctList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acctList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcctList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryBalanceResult.AcctList }
     * 
     * 
     */
    public List<QueryBalanceResult.AcctList> getAcctList() {
        if (acctList == null) {
            acctList = new ArrayList<QueryBalanceResult.AcctList>();
        }
        return this.acctList;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BalanceResult" type="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctBalance" maxOccurs="unbounded"/>
     *         &lt;element name="ReservedBalanceDetail" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="BalanceInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="ReservedAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PaymentLimitUsage" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CorPayRelaList" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CorPayRelaAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="PayRelaType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CorPayRelaInstanID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="IndvUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="CorpUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="IndvReserAmountList" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IndvReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CorpReserAmountList" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CorpReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PreMonIndvUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurMonIndvUnbilledAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="PayByIndvPayOffAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="PayByCorpPayOffAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="OtherFeeAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OutStandingList" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="BillCycleID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BillCycleBeginTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BillCycleEndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OutStandingDetail" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OutStandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AccountCredit" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="CreditLimitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="TotalCreditAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="TotalUsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="TotalRemainAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="CreditAmountInfo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                             &lt;element name="LimitClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "balanceResult",
        "reservedBalanceDetail",
        "paymentLimitUsage",
        "corPayRelaList",
        "indvUnbilledAmount",
        "corpUnbilledAmount",
        "indvReserAmountList",
        "corpReserAmountList",
        "preMonIndvUnbilledAmount",
        "curMonIndvUnbilledAmount",
        "payByIndvPayOffAmount",
        "payByCorpPayOffAmount",
        "otherFeeAmount",
        "statusDetail",
        "outStandingList",
        "accountCredit"
    })
    public static class AcctList {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "BalanceResult", required = true)
        protected List<AcctBalance> balanceResult;
        @XmlElement(name = "ReservedBalanceDetail")
        protected List<QueryBalanceResult.AcctList.ReservedBalanceDetail> reservedBalanceDetail;
        @XmlElement(name = "PaymentLimitUsage")
        protected List<QueryBalanceResult.AcctList.PaymentLimitUsage> paymentLimitUsage;
        @XmlElement(name = "CorPayRelaList")
        protected List<QueryBalanceResult.AcctList.CorPayRelaList> corPayRelaList;
        @XmlElement(name = "IndvUnbilledAmount")
        protected Long indvUnbilledAmount;
        @XmlElement(name = "CorpUnbilledAmount")
        protected long corpUnbilledAmount;
        @XmlElement(name = "IndvReserAmountList", required = true)
        protected List<QueryBalanceResult.AcctList.IndvReserAmountList> indvReserAmountList;
        @XmlElement(name = "CorpReserAmountList", required = true)
        protected List<QueryBalanceResult.AcctList.CorpReserAmountList> corpReserAmountList;
        @XmlElement(name = "PreMonIndvUnbilledAmount")
        protected long preMonIndvUnbilledAmount;
        @XmlElement(name = "CurMonIndvUnbilledAmount")
        protected long curMonIndvUnbilledAmount;
        @XmlElement(name = "PayByIndvPayOffAmount")
        protected long payByIndvPayOffAmount;
        @XmlElement(name = "PayByCorpPayOffAmount")
        protected long payByCorpPayOffAmount;
        @XmlElement(name = "OtherFeeAmount")
        protected long otherFeeAmount;
        @XmlElement(name = "StatusDetail", required = true)
        protected String statusDetail;
        @XmlElement(name = "OutStandingList")
        protected List<QueryBalanceResult.AcctList.OutStandingList> outStandingList;
        @XmlElement(name = "AccountCredit")
        protected List<QueryBalanceResult.AcctList.AccountCredit> accountCredit;

        /**
         * Gets the value of the acctKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Sets the value of the acctKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Gets the value of the balanceResult property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceResult property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceResult().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcctBalance }
         * 
         * 
         */
        public List<AcctBalance> getBalanceResult() {
            if (balanceResult == null) {
                balanceResult = new ArrayList<AcctBalance>();
            }
            return this.balanceResult;
        }

        /**
         * Gets the value of the reservedBalanceDetail property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the reservedBalanceDetail property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReservedBalanceDetail().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryBalanceResult.AcctList.ReservedBalanceDetail }
         * 
         * 
         */
        public List<QueryBalanceResult.AcctList.ReservedBalanceDetail> getReservedBalanceDetail() {
            if (reservedBalanceDetail == null) {
                reservedBalanceDetail = new ArrayList<QueryBalanceResult.AcctList.ReservedBalanceDetail>();
            }
            return this.reservedBalanceDetail;
        }

        /**
         * Gets the value of the paymentLimitUsage property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the paymentLimitUsage property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPaymentLimitUsage().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryBalanceResult.AcctList.PaymentLimitUsage }
         * 
         * 
         */
        public List<QueryBalanceResult.AcctList.PaymentLimitUsage> getPaymentLimitUsage() {
            if (paymentLimitUsage == null) {
                paymentLimitUsage = new ArrayList<QueryBalanceResult.AcctList.PaymentLimitUsage>();
            }
            return this.paymentLimitUsage;
        }

        /**
         * Gets the value of the corPayRelaList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the corPayRelaList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCorPayRelaList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryBalanceResult.AcctList.CorPayRelaList }
         * 
         * 
         */
        public List<QueryBalanceResult.AcctList.CorPayRelaList> getCorPayRelaList() {
            if (corPayRelaList == null) {
                corPayRelaList = new ArrayList<QueryBalanceResult.AcctList.CorPayRelaList>();
            }
            return this.corPayRelaList;
        }

        /**
         * Gets the value of the indvUnbilledAmount property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getIndvUnbilledAmount() {
            return indvUnbilledAmount;
        }

        /**
         * Sets the value of the indvUnbilledAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setIndvUnbilledAmount(Long value) {
            this.indvUnbilledAmount = value;
        }

        /**
         * Gets the value of the corpUnbilledAmount property.
         * 
         */
        public long getCorpUnbilledAmount() {
            return corpUnbilledAmount;
        }

        /**
         * Sets the value of the corpUnbilledAmount property.
         * 
         */
        public void setCorpUnbilledAmount(long value) {
            this.corpUnbilledAmount = value;
        }

        /**
         * Gets the value of the indvReserAmountList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the indvReserAmountList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getIndvReserAmountList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryBalanceResult.AcctList.IndvReserAmountList }
         * 
         * 
         */
        public List<QueryBalanceResult.AcctList.IndvReserAmountList> getIndvReserAmountList() {
            if (indvReserAmountList == null) {
                indvReserAmountList = new ArrayList<QueryBalanceResult.AcctList.IndvReserAmountList>();
            }
            return this.indvReserAmountList;
        }

        /**
         * Gets the value of the corpReserAmountList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the corpReserAmountList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCorpReserAmountList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryBalanceResult.AcctList.CorpReserAmountList }
         * 
         * 
         */
        public List<QueryBalanceResult.AcctList.CorpReserAmountList> getCorpReserAmountList() {
            if (corpReserAmountList == null) {
                corpReserAmountList = new ArrayList<QueryBalanceResult.AcctList.CorpReserAmountList>();
            }
            return this.corpReserAmountList;
        }

        /**
         * Gets the value of the preMonIndvUnbilledAmount property.
         * 
         */
        public long getPreMonIndvUnbilledAmount() {
            return preMonIndvUnbilledAmount;
        }

        /**
         * Sets the value of the preMonIndvUnbilledAmount property.
         * 
         */
        public void setPreMonIndvUnbilledAmount(long value) {
            this.preMonIndvUnbilledAmount = value;
        }

        /**
         * Gets the value of the curMonIndvUnbilledAmount property.
         * 
         */
        public long getCurMonIndvUnbilledAmount() {
            return curMonIndvUnbilledAmount;
        }

        /**
         * Sets the value of the curMonIndvUnbilledAmount property.
         * 
         */
        public void setCurMonIndvUnbilledAmount(long value) {
            this.curMonIndvUnbilledAmount = value;
        }

        /**
         * Gets the value of the payByIndvPayOffAmount property.
         * 
         */
        public long getPayByIndvPayOffAmount() {
            return payByIndvPayOffAmount;
        }

        /**
         * Sets the value of the payByIndvPayOffAmount property.
         * 
         */
        public void setPayByIndvPayOffAmount(long value) {
            this.payByIndvPayOffAmount = value;
        }

        /**
         * Gets the value of the payByCorpPayOffAmount property.
         * 
         */
        public long getPayByCorpPayOffAmount() {
            return payByCorpPayOffAmount;
        }

        /**
         * Sets the value of the payByCorpPayOffAmount property.
         * 
         */
        public void setPayByCorpPayOffAmount(long value) {
            this.payByCorpPayOffAmount = value;
        }

        /**
         * Gets the value of the otherFeeAmount property.
         * 
         */
        public long getOtherFeeAmount() {
            return otherFeeAmount;
        }

        /**
         * Sets the value of the otherFeeAmount property.
         * 
         */
        public void setOtherFeeAmount(long value) {
            this.otherFeeAmount = value;
        }

        /**
         * Gets the value of the statusDetail property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatusDetail() {
            return statusDetail;
        }

        /**
         * Sets the value of the statusDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatusDetail(String value) {
            this.statusDetail = value;
        }

        /**
         * Gets the value of the outStandingList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the outStandingList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOutStandingList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryBalanceResult.AcctList.OutStandingList }
         * 
         * 
         */
        public List<QueryBalanceResult.AcctList.OutStandingList> getOutStandingList() {
            if (outStandingList == null) {
                outStandingList = new ArrayList<QueryBalanceResult.AcctList.OutStandingList>();
            }
            return this.outStandingList;
        }

        /**
         * Gets the value of the accountCredit property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accountCredit property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAccountCredit().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryBalanceResult.AcctList.AccountCredit }
         * 
         * 
         */
        public List<QueryBalanceResult.AcctList.AccountCredit> getAccountCredit() {
            if (accountCredit == null) {
                accountCredit = new ArrayList<QueryBalanceResult.AcctList.AccountCredit>();
            }
            return this.accountCredit;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="CreditLimitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="TotalCreditAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="TotalUsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="TotalRemainAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="CreditAmountInfo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                   &lt;element name="LimitClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "creditLimitType",
            "creditLimitTypeName",
            "totalCreditAmount",
            "totalUsageAmount",
            "totalRemainAmount",
            "currencyID",
            "creditAmountInfo"
        })
        public static class AccountCredit {

            @XmlElement(name = "CreditLimitType")
            protected String creditLimitType;
            @XmlElement(name = "CreditLimitTypeName")
            protected String creditLimitTypeName;
            @XmlElement(name = "TotalCreditAmount")
            protected long totalCreditAmount;
            @XmlElement(name = "TotalUsageAmount")
            protected long totalUsageAmount;
            @XmlElement(name = "TotalRemainAmount")
            protected long totalRemainAmount;
            @XmlElement(name = "CurrencyID", required = true)
            protected BigInteger currencyID;
            @XmlElement(name = "CreditAmountInfo")
            protected List<QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo> creditAmountInfo;

            /**
             * Gets the value of the creditLimitType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreditLimitType() {
                return creditLimitType;
            }

            /**
             * Sets the value of the creditLimitType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreditLimitType(String value) {
                this.creditLimitType = value;
            }

            /**
             * Gets the value of the creditLimitTypeName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreditLimitTypeName() {
                return creditLimitTypeName;
            }

            /**
             * Sets the value of the creditLimitTypeName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreditLimitTypeName(String value) {
                this.creditLimitTypeName = value;
            }

            /**
             * Gets the value of the totalCreditAmount property.
             * 
             */
            public long getTotalCreditAmount() {
                return totalCreditAmount;
            }

            /**
             * Sets the value of the totalCreditAmount property.
             * 
             */
            public void setTotalCreditAmount(long value) {
                this.totalCreditAmount = value;
            }

            /**
             * Gets the value of the totalUsageAmount property.
             * 
             */
            public long getTotalUsageAmount() {
                return totalUsageAmount;
            }

            /**
             * Sets the value of the totalUsageAmount property.
             * 
             */
            public void setTotalUsageAmount(long value) {
                this.totalUsageAmount = value;
            }

            /**
             * Gets the value of the totalRemainAmount property.
             * 
             */
            public long getTotalRemainAmount() {
                return totalRemainAmount;
            }

            /**
             * Sets the value of the totalRemainAmount property.
             * 
             */
            public void setTotalRemainAmount(long value) {
                this.totalRemainAmount = value;
            }

            /**
             * Gets the value of the currencyID property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCurrencyID() {
                return currencyID;
            }

            /**
             * Sets the value of the currencyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCurrencyID(BigInteger value) {
                this.currencyID = value;
            }

            /**
             * Gets the value of the creditAmountInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the creditAmountInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCreditAmountInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo }
             * 
             * 
             */
            public List<QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo> getCreditAmountInfo() {
                if (creditAmountInfo == null) {
                    creditAmountInfo = new ArrayList<QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo>();
                }
                return this.creditAmountInfo;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *         &lt;element name="LimitClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "creditInstID",
                "limitClass",
                "amount",
                "effectiveTime",
                "expireTime"
            })
            public static class CreditAmountInfo {

                @XmlElement(name = "CreditInstID")
                protected long creditInstID;
                @XmlElement(name = "LimitClass", required = true)
                protected String limitClass;
                @XmlElement(name = "Amount")
                protected long amount;
                @XmlElement(name = "EffectiveTime", required = true)
                protected String effectiveTime;
                @XmlElement(name = "ExpireTime", required = true)
                protected String expireTime;

                /**
                 * Gets the value of the creditInstID property.
                 * 
                 */
                public long getCreditInstID() {
                    return creditInstID;
                }

                /**
                 * Sets the value of the creditInstID property.
                 * 
                 */
                public void setCreditInstID(long value) {
                    this.creditInstID = value;
                }

                /**
                 * Gets the value of the limitClass property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLimitClass() {
                    return limitClass;
                }

                /**
                 * Sets the value of the limitClass property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLimitClass(String value) {
                    this.limitClass = value;
                }

                /**
                 * Gets the value of the amount property.
                 * 
                 */
                public long getAmount() {
                    return amount;
                }

                /**
                 * Sets the value of the amount property.
                 * 
                 */
                public void setAmount(long value) {
                    this.amount = value;
                }

                /**
                 * Gets the value of the effectiveTime property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEffectiveTime() {
                    return effectiveTime;
                }

                /**
                 * Sets the value of the effectiveTime property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEffectiveTime(String value) {
                    this.effectiveTime = value;
                }

                /**
                 * Gets the value of the expireTime property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExpireTime() {
                    return expireTime;
                }

                /**
                 * Sets the value of the expireTime property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExpireTime(String value) {
                    this.expireTime = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CorPayRelaAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="PayRelaType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CorPayRelaInstanID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "corPayRelaAmount",
            "payRelaType",
            "corPayRelaInstanID"
        })
        public static class CorPayRelaList {

            @XmlElement(name = "CorPayRelaAmount")
            protected long corPayRelaAmount;
            @XmlElement(name = "PayRelaType", required = true)
            protected String payRelaType;
            @XmlElement(name = "CorPayRelaInstanID")
            protected String corPayRelaInstanID;

            /**
             * Gets the value of the corPayRelaAmount property.
             * 
             */
            public long getCorPayRelaAmount() {
                return corPayRelaAmount;
            }

            /**
             * Sets the value of the corPayRelaAmount property.
             * 
             */
            public void setCorPayRelaAmount(long value) {
                this.corPayRelaAmount = value;
            }

            /**
             * Gets the value of the payRelaType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayRelaType() {
                return payRelaType;
            }

            /**
             * Sets the value of the payRelaType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayRelaType(String value) {
                this.payRelaType = value;
            }

            /**
             * Gets the value of the corPayRelaInstanID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorPayRelaInstanID() {
                return corPayRelaInstanID;
            }

            /**
             * Sets the value of the corPayRelaInstanID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorPayRelaInstanID(String value) {
                this.corPayRelaInstanID = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CorpReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "corpReserAmount"
        })
        public static class CorpReserAmountList {

            @XmlElement(name = "CorpReserAmount")
            protected long corpReserAmount;

            /**
             * Gets the value of the corpReserAmount property.
             * 
             */
            public long getCorpReserAmount() {
                return corpReserAmount;
            }

            /**
             * Sets the value of the corpReserAmount property.
             * 
             */
            public void setCorpReserAmount(long value) {
                this.corpReserAmount = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IndvReserAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "indvReserAmount"
        })
        public static class IndvReserAmountList {

            @XmlElement(name = "IndvReserAmount")
            protected long indvReserAmount;

            /**
             * Gets the value of the indvReserAmount property.
             * 
             */
            public long getIndvReserAmount() {
                return indvReserAmount;
            }

            /**
             * Sets the value of the indvReserAmount property.
             * 
             */
            public void setIndvReserAmount(long value) {
                this.indvReserAmount = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="BillCycleID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BillCycleBeginTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BillCycleEndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OutStandingDetail" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OutStandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "billCycleID",
            "billCycleBeginTime",
            "billCycleEndTime",
            "billCycleType",
            "dueDate",
            "outStandingDetail"
        })
        public static class OutStandingList {

            @XmlElement(name = "BillCycleID", required = true)
            protected String billCycleID;
            @XmlElement(name = "BillCycleBeginTime", required = true)
            protected String billCycleBeginTime;
            @XmlElement(name = "BillCycleEndTime", required = true)
            protected String billCycleEndTime;
            @XmlElement(name = "BillCycleType", required = true)
            protected String billCycleType;
            @XmlElement(name = "DueDate", required = true)
            protected String dueDate;
            @XmlElement(name = "OutStandingDetail", required = true)
            protected List<QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail> outStandingDetail;

            /**
             * Gets the value of the billCycleID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleID() {
                return billCycleID;
            }

            /**
             * Sets the value of the billCycleID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleID(String value) {
                this.billCycleID = value;
            }

            /**
             * Gets the value of the billCycleBeginTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleBeginTime() {
                return billCycleBeginTime;
            }

            /**
             * Sets the value of the billCycleBeginTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleBeginTime(String value) {
                this.billCycleBeginTime = value;
            }

            /**
             * Gets the value of the billCycleEndTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleEndTime() {
                return billCycleEndTime;
            }

            /**
             * Sets the value of the billCycleEndTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleEndTime(String value) {
                this.billCycleEndTime = value;
            }

            /**
             * Gets the value of the billCycleType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleType() {
                return billCycleType;
            }

            /**
             * Sets the value of the billCycleType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleType(String value) {
                this.billCycleType = value;
            }

            /**
             * Gets the value of the dueDate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDueDate() {
                return dueDate;
            }

            /**
             * Sets the value of the dueDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDueDate(String value) {
                this.dueDate = value;
            }

            /**
             * Gets the value of the outStandingDetail property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the outStandingDetail property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOutStandingDetail().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail }
             * 
             * 
             */
            public List<QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail> getOutStandingDetail() {
                if (outStandingDetail == null) {
                    outStandingDetail = new ArrayList<QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail>();
                }
                return this.outStandingDetail;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OutStandingAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "outStandingAmount",
                "currencyID"
            })
            public static class OutStandingDetail {

                @XmlElement(name = "OutStandingAmount")
                protected long outStandingAmount;
                @XmlElement(name = "CurrencyID", required = true)
                protected BigInteger currencyID;

                /**
                 * Gets the value of the outStandingAmount property.
                 * 
                 */
                public long getOutStandingAmount() {
                    return outStandingAmount;
                }

                /**
                 * Sets the value of the outStandingAmount property.
                 * 
                 */
                public void setOutStandingAmount(long value) {
                    this.outStandingAmount = value;
                }

                /**
                 * Gets the value of the currencyID property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCurrencyID() {
                    return currencyID;
                }

                /**
                 * Sets the value of the currencyID property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCurrencyID(BigInteger value) {
                    this.currencyID = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "amount",
            "usedAmount",
            "limitType"
        })
        public static class PaymentLimitUsage {

            @XmlElement(name = "Amount")
            protected long amount;
            @XmlElement(name = "UsedAmount", required = true)
            protected String usedAmount;
            @XmlElement(name = "LimitType")
            protected String limitType;

            /**
             * Gets the value of the amount property.
             * 
             */
            public long getAmount() {
                return amount;
            }

            /**
             * Sets the value of the amount property.
             * 
             */
            public void setAmount(long value) {
                this.amount = value;
            }

            /**
             * Gets the value of the usedAmount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUsedAmount() {
                return usedAmount;
            }

            /**
             * Sets the value of the usedAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUsedAmount(String value) {
                this.usedAmount = value;
            }

            /**
             * Gets the value of the limitType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLimitType() {
                return limitType;
            }

            /**
             * Sets the value of the limitType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLimitType(String value) {
                this.limitType = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="BalanceInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="ReservedAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "balanceInstanceID",
            "reservedAmount"
        })
        public static class ReservedBalanceDetail {

            @XmlElement(name = "BalanceInstanceID")
            protected long balanceInstanceID;
            @XmlElement(name = "ReservedAmount", required = true)
            protected String reservedAmount;

            /**
             * Gets the value of the balanceInstanceID property.
             * 
             */
            public long getBalanceInstanceID() {
                return balanceInstanceID;
            }

            /**
             * Sets the value of the balanceInstanceID property.
             * 
             */
            public void setBalanceInstanceID(long value) {
                this.balanceInstanceID = value;
            }

            /**
             * Gets the value of the reservedAmount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReservedAmount() {
                return reservedAmount;
            }

            /**
             * Sets the value of the reservedAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReservedAmount(String value) {
                this.reservedAmount = value;
            }

        }

    }

}
