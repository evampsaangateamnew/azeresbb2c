
package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.BalanceChgInfo;
import com.huawei.cbs.ar.wsservice.arcommon.FreeUnitChgInfo;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;


/**
 * <p>Java class for QueryTransferLogResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryTransferLogResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TradeTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TransferChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="ExtTransID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TransferAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="OppositePrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OppositeAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="ResultCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BalanceChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BalanceChgInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="FreeUnitChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}FreeUnitChgInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryTransferLogResult", propOrder = {
    "transferInfo",
    "totalRowNum",
    "beginRowNum",
    "fetchRowNum"
})
public class QueryTransferLogResult {

    @XmlElement(name = "TransferInfo")
    protected List<QueryTransferLogResult.TransferInfo> transferInfo;
    @XmlElement(name = "TotalRowNum")
    protected long totalRowNum;
    @XmlElement(name = "BeginRowNum")
    protected long beginRowNum;
    @XmlElement(name = "FetchRowNum")
    protected long fetchRowNum;

    /**
     * Gets the value of the transferInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transferInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransferInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryTransferLogResult.TransferInfo }
     * 
     * 
     */
    public List<QueryTransferLogResult.TransferInfo> getTransferInfo() {
        if (transferInfo == null) {
            transferInfo = new ArrayList<QueryTransferLogResult.TransferInfo>();
        }
        return this.transferInfo;
    }

    /**
     * Gets the value of the totalRowNum property.
     * 
     */
    public long getTotalRowNum() {
        return totalRowNum;
    }

    /**
     * Sets the value of the totalRowNum property.
     * 
     */
    public void setTotalRowNum(long value) {
        this.totalRowNum = value;
    }

    /**
     * Gets the value of the beginRowNum property.
     * 
     */
    public long getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Sets the value of the beginRowNum property.
     * 
     */
    public void setBeginRowNum(long value) {
        this.beginRowNum = value;
    }

    /**
     * Gets the value of the fetchRowNum property.
     * 
     */
    public long getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Sets the value of the fetchRowNum property.
     * 
     */
    public void setFetchRowNum(long value) {
        this.fetchRowNum = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TradeTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TransferChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="ExtTransID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TransferAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="OppositePrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OppositeAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="ResultCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BalanceChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BalanceChgInfo" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="FreeUnitChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}FreeUnitChgInfo" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tradeTime",
        "acctKey",
        "subKey",
        "primaryIdentity",
        "transferChannelID",
        "transID",
        "extTransID",
        "transferAmount",
        "oppositePrimaryIdentity",
        "oppositeAcctKey",
        "currencyID",
        "resultCode",
        "balanceChgInfo",
        "freeUnitChgInfo",
        "additionalProperty"
    })
    public static class TransferInfo {

        @XmlElement(name = "TradeTime", required = true)
        protected String tradeTime;
        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "SubKey")
        protected String subKey;
        @XmlElement(name = "PrimaryIdentity")
        protected String primaryIdentity;
        @XmlElement(name = "TransferChannelID")
        protected String transferChannelID;
        @XmlElement(name = "TransID")
        protected long transID;
        @XmlElement(name = "ExtTransID")
        protected String extTransID;
        @XmlElement(name = "TransferAmount")
        protected long transferAmount;
        @XmlElement(name = "OppositePrimaryIdentity")
        protected String oppositePrimaryIdentity;
        @XmlElement(name = "OppositeAcctKey")
        protected String oppositeAcctKey;
        @XmlElement(name = "CurrencyID", required = true)
        protected BigInteger currencyID;
        @XmlElement(name = "ResultCode", required = true)
        protected String resultCode;
        @XmlElement(name = "BalanceChgInfo")
        protected List<BalanceChgInfo> balanceChgInfo;
        @XmlElement(name = "FreeUnitChgInfo")
        protected List<FreeUnitChgInfo> freeUnitChgInfo;
        @XmlElement(name = "AdditionalProperty")
        protected List<SimpleProperty> additionalProperty;

        /**
         * Gets the value of the tradeTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTradeTime() {
            return tradeTime;
        }

        /**
         * Sets the value of the tradeTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTradeTime(String value) {
            this.tradeTime = value;
        }

        /**
         * Gets the value of the acctKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Sets the value of the acctKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Gets the value of the subKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubKey() {
            return subKey;
        }

        /**
         * Sets the value of the subKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubKey(String value) {
            this.subKey = value;
        }

        /**
         * Gets the value of the primaryIdentity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryIdentity() {
            return primaryIdentity;
        }

        /**
         * Sets the value of the primaryIdentity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryIdentity(String value) {
            this.primaryIdentity = value;
        }

        /**
         * Gets the value of the transferChannelID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransferChannelID() {
            return transferChannelID;
        }

        /**
         * Sets the value of the transferChannelID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransferChannelID(String value) {
            this.transferChannelID = value;
        }

        /**
         * Gets the value of the transID property.
         * 
         */
        public long getTransID() {
            return transID;
        }

        /**
         * Sets the value of the transID property.
         * 
         */
        public void setTransID(long value) {
            this.transID = value;
        }

        /**
         * Gets the value of the extTransID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtTransID() {
            return extTransID;
        }

        /**
         * Sets the value of the extTransID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtTransID(String value) {
            this.extTransID = value;
        }

        /**
         * Gets the value of the transferAmount property.
         * 
         */
        public long getTransferAmount() {
            return transferAmount;
        }

        /**
         * Sets the value of the transferAmount property.
         * 
         */
        public void setTransferAmount(long value) {
            this.transferAmount = value;
        }

        /**
         * Gets the value of the oppositePrimaryIdentity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOppositePrimaryIdentity() {
            return oppositePrimaryIdentity;
        }

        /**
         * Sets the value of the oppositePrimaryIdentity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOppositePrimaryIdentity(String value) {
            this.oppositePrimaryIdentity = value;
        }

        /**
         * Gets the value of the oppositeAcctKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOppositeAcctKey() {
            return oppositeAcctKey;
        }

        /**
         * Sets the value of the oppositeAcctKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOppositeAcctKey(String value) {
            this.oppositeAcctKey = value;
        }

        /**
         * Gets the value of the currencyID property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Sets the value of the currencyID property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Gets the value of the resultCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResultCode() {
            return resultCode;
        }

        /**
         * Sets the value of the resultCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResultCode(String value) {
            this.resultCode = value;
        }

        /**
         * Gets the value of the balanceChgInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceChgInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceChgInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BalanceChgInfo }
         * 
         * 
         */
        public List<BalanceChgInfo> getBalanceChgInfo() {
            if (balanceChgInfo == null) {
                balanceChgInfo = new ArrayList<BalanceChgInfo>();
            }
            return this.balanceChgInfo;
        }

        /**
         * Gets the value of the freeUnitChgInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the freeUnitChgInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFreeUnitChgInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FreeUnitChgInfo }
         * 
         * 
         */
        public List<FreeUnitChgInfo> getFreeUnitChgInfo() {
            if (freeUnitChgInfo == null) {
                freeUnitChgInfo = new ArrayList<FreeUnitChgInfo>();
            }
            return this.freeUnitChgInfo;
        }

        /**
         * Gets the value of the additionalProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdditionalProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getAdditionalProperty() {
            if (additionalProperty == null) {
                additionalProperty = new ArrayList<SimpleProperty>();
            }
            return this.additionalProperty;
        }

    }

}
