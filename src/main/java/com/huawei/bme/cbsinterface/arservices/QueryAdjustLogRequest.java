
package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode;


/**
 * <p>Java class for QueryAdjustLogRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryAdjustLogRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryObj">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="SubAccessCode" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SubAccessCode"/>
 *                   &lt;element name="AcctAccessCode">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
 *                           &lt;sequence>
 *                             &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="QueryDetailInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="FreeUnitInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice>
 *                             &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BalanceInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice>
 *                             &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryAdjustLogRequest", propOrder = {
    "queryObj",
    "queryDetailInfo",
    "totalRowNum",
    "beginRowNum",
    "fetchRowNum",
    "startTime",
    "endTime"
})
public class QueryAdjustLogRequest {

    @XmlElement(name = "QueryObj", required = true)
    protected QueryAdjustLogRequest.QueryObj queryObj;
    @XmlElement(name = "QueryDetailInfo")
    protected QueryAdjustLogRequest.QueryDetailInfo queryDetailInfo;
    @XmlElement(name = "TotalRowNum", required = true)
    protected BigInteger totalRowNum;
    @XmlElement(name = "BeginRowNum", required = true)
    protected BigInteger beginRowNum;
    @XmlElement(name = "FetchRowNum", required = true)
    protected BigInteger fetchRowNum;
    @XmlElement(name = "StartTime", required = true)
    protected String startTime;
    @XmlElement(name = "EndTime", required = true)
    protected String endTime;

    /**
     * Gets the value of the queryObj property.
     * 
     * @return
     *     possible object is
     *     {@link QueryAdjustLogRequest.QueryObj }
     *     
     */
    public QueryAdjustLogRequest.QueryObj getQueryObj() {
        return queryObj;
    }

    /**
     * Sets the value of the queryObj property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryAdjustLogRequest.QueryObj }
     *     
     */
    public void setQueryObj(QueryAdjustLogRequest.QueryObj value) {
        this.queryObj = value;
    }

    /**
     * Gets the value of the queryDetailInfo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryAdjustLogRequest.QueryDetailInfo }
     *     
     */
    public QueryAdjustLogRequest.QueryDetailInfo getQueryDetailInfo() {
        return queryDetailInfo;
    }

    /**
     * Sets the value of the queryDetailInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryAdjustLogRequest.QueryDetailInfo }
     *     
     */
    public void setQueryDetailInfo(QueryAdjustLogRequest.QueryDetailInfo value) {
        this.queryDetailInfo = value;
    }

    /**
     * Gets the value of the totalRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalRowNum() {
        return totalRowNum;
    }

    /**
     * Sets the value of the totalRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalRowNum(BigInteger value) {
        this.totalRowNum = value;
    }

    /**
     * Gets the value of the beginRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Sets the value of the beginRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBeginRowNum(BigInteger value) {
        this.beginRowNum = value;
    }

    /**
     * Gets the value of the fetchRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Sets the value of the fetchRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFetchRowNum(BigInteger value) {
        this.fetchRowNum = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartTime(String value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the endTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * Sets the value of the endTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndTime(String value) {
        this.endTime = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="FreeUnitInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice>
     *                   &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BalanceInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice>
     *                   &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "freeUnitInfo",
        "balanceInfo"
    })
    public static class QueryDetailInfo {

        @XmlElement(name = "FreeUnitInfo")
        protected QueryAdjustLogRequest.QueryDetailInfo.FreeUnitInfo freeUnitInfo;
        @XmlElement(name = "BalanceInfo")
        protected QueryAdjustLogRequest.QueryDetailInfo.BalanceInfo balanceInfo;

        /**
         * Gets the value of the freeUnitInfo property.
         * 
         * @return
         *     possible object is
         *     {@link QueryAdjustLogRequest.QueryDetailInfo.FreeUnitInfo }
         *     
         */
        public QueryAdjustLogRequest.QueryDetailInfo.FreeUnitInfo getFreeUnitInfo() {
            return freeUnitInfo;
        }

        /**
         * Sets the value of the freeUnitInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryAdjustLogRequest.QueryDetailInfo.FreeUnitInfo }
         *     
         */
        public void setFreeUnitInfo(QueryAdjustLogRequest.QueryDetailInfo.FreeUnitInfo value) {
            this.freeUnitInfo = value;
        }

        /**
         * Gets the value of the balanceInfo property.
         * 
         * @return
         *     possible object is
         *     {@link QueryAdjustLogRequest.QueryDetailInfo.BalanceInfo }
         *     
         */
        public QueryAdjustLogRequest.QueryDetailInfo.BalanceInfo getBalanceInfo() {
            return balanceInfo;
        }

        /**
         * Sets the value of the balanceInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryAdjustLogRequest.QueryDetailInfo.BalanceInfo }
         *     
         */
        public void setBalanceInfo(QueryAdjustLogRequest.QueryDetailInfo.BalanceInfo value) {
            this.balanceInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice>
         *         &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "balanceID",
            "balanceType"
        })
        public static class BalanceInfo {

            @XmlElement(name = "BalanceID")
            protected Long balanceID;
            @XmlElement(name = "BalanceType")
            protected String balanceType;

            /**
             * Gets the value of the balanceID property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getBalanceID() {
                return balanceID;
            }

            /**
             * Sets the value of the balanceID property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setBalanceID(Long value) {
                this.balanceID = value;
            }

            /**
             * Gets the value of the balanceType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalanceType() {
                return balanceType;
            }

            /**
             * Sets the value of the balanceType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalanceType(String value) {
                this.balanceType = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice>
         *         &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "freeUnitInstanceID",
            "freeUnitType"
        })
        public static class FreeUnitInfo {

            @XmlElement(name = "FreeUnitInstanceID")
            protected Long freeUnitInstanceID;
            @XmlElement(name = "FreeUnitType")
            protected String freeUnitType;

            /**
             * Gets the value of the freeUnitInstanceID property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getFreeUnitInstanceID() {
                return freeUnitInstanceID;
            }

            /**
             * Sets the value of the freeUnitInstanceID property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setFreeUnitInstanceID(Long value) {
                this.freeUnitInstanceID = value;
            }

            /**
             * Gets the value of the freeUnitType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeUnitType() {
                return freeUnitType;
            }

            /**
             * Sets the value of the freeUnitType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeUnitType(String value) {
                this.freeUnitType = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="SubAccessCode" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SubAccessCode"/>
     *         &lt;element name="AcctAccessCode">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
     *                 &lt;sequence>
     *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subAccessCode",
        "acctAccessCode"
    })
    public static class QueryObj {

        @XmlElement(name = "SubAccessCode")
        protected SubAccessCode subAccessCode;
        @XmlElement(name = "AcctAccessCode")
        protected QueryAdjustLogRequest.QueryObj.AcctAccessCode acctAccessCode;

        /**
         * Gets the value of the subAccessCode property.
         * 
         * @return
         *     possible object is
         *     {@link SubAccessCode }
         *     
         */
        public SubAccessCode getSubAccessCode() {
            return subAccessCode;
        }

        /**
         * Sets the value of the subAccessCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link SubAccessCode }
         *     
         */
        public void setSubAccessCode(SubAccessCode value) {
            this.subAccessCode = value;
        }

        /**
         * Gets the value of the acctAccessCode property.
         * 
         * @return
         *     possible object is
         *     {@link QueryAdjustLogRequest.QueryObj.AcctAccessCode }
         *     
         */
        public QueryAdjustLogRequest.QueryObj.AcctAccessCode getAcctAccessCode() {
            return acctAccessCode;
        }

        /**
         * Sets the value of the acctAccessCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryAdjustLogRequest.QueryObj.AcctAccessCode }
         *     
         */
        public void setAcctAccessCode(QueryAdjustLogRequest.QueryObj.AcctAccessCode value) {
            this.acctAccessCode = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://cbs.huawei.com/ar/wsservice/arcommon}AcctAccessCode">
         *       &lt;sequence>
         *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "payType"
        })
        public static class AcctAccessCode
            extends com.huawei.cbs.ar.wsservice.arcommon.AcctAccessCode
        {

            @XmlElement(name = "PayType")
            protected Object payType;

            /**
             * Gets the value of the payType property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPayType() {
                return payType;
            }

            /**
             * Sets the value of the payType property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPayType(Object value) {
                this.payType = value;
            }

        }

    }

}
