
package com.huawei.bme.cbsinterface.arservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
 *         &lt;element name="QueryAdjustLogResult" type="{http://www.huawei.com/bme/cbsinterface/arservices}QueryAdjustLogResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "queryAdjustLogResult"
})
@XmlRootElement(name = "QueryAdjustLogResultMsg")
public class QueryAdjustLogResultMsg {

    @XmlElement(name = "ResultHeader", namespace = "", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "QueryAdjustLogResult", namespace = "", required = true)
    protected QueryAdjustLogResult queryAdjustLogResult;

    /**
     * Gets the value of the resultHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Sets the value of the resultHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Gets the value of the queryAdjustLogResult property.
     * 
     * @return
     *     possible object is
     *     {@link QueryAdjustLogResult }
     *     
     */
    public QueryAdjustLogResult getQueryAdjustLogResult() {
        return queryAdjustLogResult;
    }

    /**
     * Sets the value of the queryAdjustLogResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryAdjustLogResult }
     *     
     */
    public void setQueryAdjustLogResult(QueryAdjustLogResult value) {
        this.queryAdjustLogResult = value;
    }

}
