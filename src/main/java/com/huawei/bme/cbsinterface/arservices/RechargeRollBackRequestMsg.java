
package com.huawei.bme.cbsinterface.arservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader"/>
 *         &lt;element name="RechargeRollBackRequest" type="{http://www.huawei.com/bme/cbsinterface/arservices}RechargeRollBackRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "rechargeRollBackRequest"
})
@XmlRootElement(name = "RechargeRollBackRequestMsg")
public class RechargeRollBackRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "RechargeRollBackRequest", namespace = "", required = true)
    protected RechargeRollBackRequest rechargeRollBackRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the rechargeRollBackRequest property.
     * 
     * @return
     *     possible object is
     *     {@link RechargeRollBackRequest }
     *     
     */
    public RechargeRollBackRequest getRechargeRollBackRequest() {
        return rechargeRollBackRequest;
    }

    /**
     * Sets the value of the rechargeRollBackRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link RechargeRollBackRequest }
     *     
     */
    public void setRechargeRollBackRequest(RechargeRollBackRequest value) {
        this.rechargeRollBackRequest = value;
    }

}
