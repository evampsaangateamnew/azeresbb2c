
package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.BalanceChgInfo;
import com.huawei.cbs.ar.wsservice.arcommon.LoanChgInfo;


/**
 * <p>Java class for RechargeResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RechargeResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RechargeSerialNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BalanceChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BalanceChgInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LoanChgInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}LoanChgInfo" minOccurs="0"/>
 *         &lt;element name="RechargeBonus" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LifeCycleChgInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OldLifeCycleStatus" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="NewLifeCycleStatus" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AddValidity" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RechargeResult", propOrder = {
    "rechargeSerialNo",
    "balanceChgInfo",
    "loanChgInfo",
    "rechargeBonus",
    "lifeCycleChgInfo"
})
public class RechargeResult {

    @XmlElement(name = "RechargeSerialNo", required = true)
    protected String rechargeSerialNo;
    @XmlElement(name = "BalanceChgInfo")
    protected List<BalanceChgInfo> balanceChgInfo;
    @XmlElement(name = "LoanChgInfo")
    protected LoanChgInfo loanChgInfo;
    @XmlElement(name = "RechargeBonus")
    protected RechargeResult.RechargeBonus rechargeBonus;
    @XmlElement(name = "LifeCycleChgInfo")
    protected RechargeResult.LifeCycleChgInfo lifeCycleChgInfo;

    /**
     * Gets the value of the rechargeSerialNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRechargeSerialNo() {
        return rechargeSerialNo;
    }

    /**
     * Sets the value of the rechargeSerialNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRechargeSerialNo(String value) {
        this.rechargeSerialNo = value;
    }

    /**
     * Gets the value of the balanceChgInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the balanceChgInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBalanceChgInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BalanceChgInfo }
     * 
     * 
     */
    public List<BalanceChgInfo> getBalanceChgInfo() {
        if (balanceChgInfo == null) {
            balanceChgInfo = new ArrayList<BalanceChgInfo>();
        }
        return this.balanceChgInfo;
    }

    /**
     * Gets the value of the loanChgInfo property.
     * 
     * @return
     *     possible object is
     *     {@link LoanChgInfo }
     *     
     */
    public LoanChgInfo getLoanChgInfo() {
        return loanChgInfo;
    }

    /**
     * Sets the value of the loanChgInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanChgInfo }
     *     
     */
    public void setLoanChgInfo(LoanChgInfo value) {
        this.loanChgInfo = value;
    }

    /**
     * Gets the value of the rechargeBonus property.
     * 
     * @return
     *     possible object is
     *     {@link RechargeResult.RechargeBonus }
     *     
     */
    public RechargeResult.RechargeBonus getRechargeBonus() {
        return rechargeBonus;
    }

    /**
     * Sets the value of the rechargeBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link RechargeResult.RechargeBonus }
     *     
     */
    public void setRechargeBonus(RechargeResult.RechargeBonus value) {
        this.rechargeBonus = value;
    }

    /**
     * Gets the value of the lifeCycleChgInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RechargeResult.LifeCycleChgInfo }
     *     
     */
    public RechargeResult.LifeCycleChgInfo getLifeCycleChgInfo() {
        return lifeCycleChgInfo;
    }

    /**
     * Sets the value of the lifeCycleChgInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RechargeResult.LifeCycleChgInfo }
     *     
     */
    public void setLifeCycleChgInfo(RechargeResult.LifeCycleChgInfo value) {
        this.lifeCycleChgInfo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OldLifeCycleStatus" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="NewLifeCycleStatus" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AddValidity" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "oldLifeCycleStatus",
        "newLifeCycleStatus",
        "addValidity"
    })
    public static class LifeCycleChgInfo {

        @XmlElement(name = "OldLifeCycleStatus", required = true)
        protected List<RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus> oldLifeCycleStatus;
        @XmlElement(name = "NewLifeCycleStatus", required = true)
        protected List<RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus> newLifeCycleStatus;
        @XmlElement(name = "AddValidity", required = true)
        protected BigInteger addValidity;

        /**
         * Gets the value of the oldLifeCycleStatus property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the oldLifeCycleStatus property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOldLifeCycleStatus().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus }
         * 
         * 
         */
        public List<RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus> getOldLifeCycleStatus() {
            if (oldLifeCycleStatus == null) {
                oldLifeCycleStatus = new ArrayList<RechargeResult.LifeCycleChgInfo.OldLifeCycleStatus>();
            }
            return this.oldLifeCycleStatus;
        }

        /**
         * Gets the value of the newLifeCycleStatus property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the newLifeCycleStatus property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNewLifeCycleStatus().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus }
         * 
         * 
         */
        public List<RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus> getNewLifeCycleStatus() {
            if (newLifeCycleStatus == null) {
                newLifeCycleStatus = new ArrayList<RechargeResult.LifeCycleChgInfo.NewLifeCycleStatus>();
            }
            return this.newLifeCycleStatus;
        }

        /**
         * Gets the value of the addValidity property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getAddValidity() {
            return addValidity;
        }

        /**
         * Sets the value of the addValidity property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setAddValidity(BigInteger value) {
            this.addValidity = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "statusName",
            "statusExpireTime",
            "statusIndex"
        })
        public static class NewLifeCycleStatus {

            @XmlElement(name = "StatusName", required = true)
            protected String statusName;
            @XmlElement(name = "StatusExpireTime", required = true)
            protected String statusExpireTime;
            @XmlElement(name = "StatusIndex", required = true)
            protected String statusIndex;

            /**
             * Gets the value of the statusName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusName() {
                return statusName;
            }

            /**
             * Sets the value of the statusName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusName(String value) {
                this.statusName = value;
            }

            /**
             * Gets the value of the statusExpireTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusExpireTime() {
                return statusExpireTime;
            }

            /**
             * Sets the value of the statusExpireTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusExpireTime(String value) {
                this.statusExpireTime = value;
            }

            /**
             * Gets the value of the statusIndex property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusIndex() {
                return statusIndex;
            }

            /**
             * Sets the value of the statusIndex property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusIndex(String value) {
                this.statusIndex = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "statusName",
            "statusExpireTime",
            "statusIndex"
        })
        public static class OldLifeCycleStatus {

            @XmlElement(name = "StatusName", required = true)
            protected String statusName;
            @XmlElement(name = "StatusExpireTime", required = true)
            protected String statusExpireTime;
            @XmlElement(name = "StatusIndex", required = true)
            protected String statusIndex;

            /**
             * Gets the value of the statusName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusName() {
                return statusName;
            }

            /**
             * Sets the value of the statusName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusName(String value) {
                this.statusName = value;
            }

            /**
             * Gets the value of the statusExpireTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusExpireTime() {
                return statusExpireTime;
            }

            /**
             * Sets the value of the statusExpireTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusExpireTime(String value) {
                this.statusExpireTime = value;
            }

            /**
             * Gets the value of the statusIndex property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusIndex() {
                return statusIndex;
            }

            /**
             * Sets the value of the statusIndex property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusIndex(String value) {
                this.statusIndex = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "freeUnitItemList",
        "balanceList"
    })
    public static class RechargeBonus {

        @XmlElement(name = "FreeUnitItemList")
        protected List<RechargeResult.RechargeBonus.FreeUnitItemList> freeUnitItemList;
        @XmlElement(name = "BalanceList")
        protected List<RechargeResult.RechargeBonus.BalanceList> balanceList;

        /**
         * Gets the value of the freeUnitItemList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the freeUnitItemList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFreeUnitItemList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RechargeResult.RechargeBonus.FreeUnitItemList }
         * 
         * 
         */
        public List<RechargeResult.RechargeBonus.FreeUnitItemList> getFreeUnitItemList() {
            if (freeUnitItemList == null) {
                freeUnitItemList = new ArrayList<RechargeResult.RechargeBonus.FreeUnitItemList>();
            }
            return this.freeUnitItemList;
        }

        /**
         * Gets the value of the balanceList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RechargeResult.RechargeBonus.BalanceList }
         * 
         * 
         */
        public List<RechargeResult.RechargeBonus.BalanceList> getBalanceList() {
            if (balanceList == null) {
                balanceList = new ArrayList<RechargeResult.RechargeBonus.BalanceList>();
            }
            return this.balanceList;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "balanceType",
            "balanceID",
            "balanceTypeName",
            "bonusAmt",
            "currencyID",
            "effectiveTime",
            "expireTime"
        })
        public static class BalanceList {

            @XmlElement(name = "BalanceType", required = true)
            protected String balanceType;
            @XmlElement(name = "BalanceID")
            protected String balanceID;
            @XmlElement(name = "BalanceTypeName", required = true)
            protected String balanceTypeName;
            @XmlElement(name = "BonusAmt")
            protected long bonusAmt;
            @XmlElement(name = "CurrencyID", required = true)
            protected BigInteger currencyID;
            @XmlElement(name = "EffectiveTime", required = true)
            protected String effectiveTime;
            @XmlElement(name = "ExpireTime", required = true)
            protected String expireTime;

            /**
             * Gets the value of the balanceType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalanceType() {
                return balanceType;
            }

            /**
             * Sets the value of the balanceType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalanceType(String value) {
                this.balanceType = value;
            }

            /**
             * Gets the value of the balanceID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalanceID() {
                return balanceID;
            }

            /**
             * Sets the value of the balanceID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalanceID(String value) {
                this.balanceID = value;
            }

            /**
             * Gets the value of the balanceTypeName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalanceTypeName() {
                return balanceTypeName;
            }

            /**
             * Sets the value of the balanceTypeName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalanceTypeName(String value) {
                this.balanceTypeName = value;
            }

            /**
             * Gets the value of the bonusAmt property.
             * 
             */
            public long getBonusAmt() {
                return bonusAmt;
            }

            /**
             * Sets the value of the bonusAmt property.
             * 
             */
            public void setBonusAmt(long value) {
                this.bonusAmt = value;
            }

            /**
             * Gets the value of the currencyID property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCurrencyID() {
                return currencyID;
            }

            /**
             * Sets the value of the currencyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCurrencyID(BigInteger value) {
                this.currencyID = value;
            }

            /**
             * Gets the value of the effectiveTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Sets the value of the effectiveTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Gets the value of the expireTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpireTime() {
                return expireTime;
            }

            /**
             * Sets the value of the expireTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpireTime(String value) {
                this.expireTime = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "freeUnitID",
            "freeUnitType",
            "freeUnitTypeName",
            "measureUnit",
            "measureUnitName",
            "bonusAmt",
            "effectiveTime",
            "expireTime"
        })
        public static class FreeUnitItemList {

            @XmlElement(name = "FreeUnitID", required = true)
            protected String freeUnitID;
            @XmlElement(name = "FreeUnitType", required = true)
            protected String freeUnitType;
            @XmlElement(name = "FreeUnitTypeName")
            protected String freeUnitTypeName;
            @XmlElement(name = "MeasureUnit", required = true)
            protected String measureUnit;
            @XmlElement(name = "MeasureUnitName")
            protected String measureUnitName;
            @XmlElement(name = "BonusAmt", required = true, type = Long.class, nillable = true)
            protected Long bonusAmt;
            @XmlElement(name = "EffectiveTime", required = true)
            protected String effectiveTime;
            @XmlElement(name = "ExpireTime", required = true)
            protected String expireTime;

            /**
             * Gets the value of the freeUnitID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeUnitID() {
                return freeUnitID;
            }

            /**
             * Sets the value of the freeUnitID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeUnitID(String value) {
                this.freeUnitID = value;
            }

            /**
             * Gets the value of the freeUnitType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeUnitType() {
                return freeUnitType;
            }

            /**
             * Sets the value of the freeUnitType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeUnitType(String value) {
                this.freeUnitType = value;
            }

            /**
             * Gets the value of the freeUnitTypeName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeUnitTypeName() {
                return freeUnitTypeName;
            }

            /**
             * Sets the value of the freeUnitTypeName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeUnitTypeName(String value) {
                this.freeUnitTypeName = value;
            }

            /**
             * Gets the value of the measureUnit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMeasureUnit() {
                return measureUnit;
            }

            /**
             * Sets the value of the measureUnit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMeasureUnit(String value) {
                this.measureUnit = value;
            }

            /**
             * Gets the value of the measureUnitName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMeasureUnitName() {
                return measureUnitName;
            }

            /**
             * Sets the value of the measureUnitName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMeasureUnitName(String value) {
                this.measureUnitName = value;
            }

            /**
             * Gets the value of the bonusAmt property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getBonusAmt() {
                return bonusAmt;
            }

            /**
             * Sets the value of the bonusAmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setBonusAmt(Long value) {
                this.bonusAmt = value;
            }

            /**
             * Gets the value of the effectiveTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Sets the value of the effectiveTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Gets the value of the expireTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpireTime() {
                return expireTime;
            }

            /**
             * Sets the value of the expireTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpireTime(String value) {
                this.expireTime = value;
            }

        }

    }

}
