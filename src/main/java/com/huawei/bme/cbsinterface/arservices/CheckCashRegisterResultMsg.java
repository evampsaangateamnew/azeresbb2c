
package com.huawei.bme.cbsinterface.arservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
 *         &lt;element name="CheckCashRegisterResult" type="{http://www.huawei.com/bme/cbsinterface/arservices}CheckCashRegisterResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "checkCashRegisterResult"
})
@XmlRootElement(name = "CheckCashRegisterResultMsg")
public class CheckCashRegisterResultMsg {

    @XmlElement(name = "ResultHeader", namespace = "", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "CheckCashRegisterResult", namespace = "", required = true)
    protected CheckCashRegisterResult checkCashRegisterResult;

    /**
     * Gets the value of the resultHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Sets the value of the resultHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Gets the value of the checkCashRegisterResult property.
     * 
     * @return
     *     possible object is
     *     {@link CheckCashRegisterResult }
     *     
     */
    public CheckCashRegisterResult getCheckCashRegisterResult() {
        return checkCashRegisterResult;
    }

    /**
     * Sets the value of the checkCashRegisterResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckCashRegisterResult }
     *     
     */
    public void setCheckCashRegisterResult(CheckCashRegisterResult value) {
        this.checkCashRegisterResult = value;
    }

}
