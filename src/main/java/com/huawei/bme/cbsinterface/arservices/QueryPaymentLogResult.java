
package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.BankInfo;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;
import com.huawei.cbs.ar.wsservice.arcommon.Tax;


/**
 * <p>Java class for QueryPaymentLogResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryPaymentLogResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExtTransID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExtPayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PaymentTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="OriAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="OriCurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="CurrencyRate" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *                   &lt;element name="PaymentDetail" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ApplyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ApplyAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="InvoiceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="InvoiceDetailID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ChargeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="TaxList" type="{http://cbs.huawei.com/ar/wsservice/arcommon}Tax" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;element name="DiscountAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CardInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" minOccurs="0"/>
 *                   &lt;element name="PayChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AccessMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="DeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="CurAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="TaxAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryPaymentLogResult", propOrder = {
    "paymentInfo",
    "totalRowNum",
    "beginRowNum",
    "fetchRowNum"
})
public class QueryPaymentLogResult {

    @XmlElement(name = "PaymentInfo")
    protected List<QueryPaymentLogResult.PaymentInfo> paymentInfo;
    @XmlElement(name = "TotalRowNum")
    protected long totalRowNum;
    @XmlElement(name = "BeginRowNum")
    protected long beginRowNum;
    @XmlElement(name = "FetchRowNum")
    protected long fetchRowNum;

    /**
     * Gets the value of the paymentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryPaymentLogResult.PaymentInfo }
     * 
     * 
     */
    public List<QueryPaymentLogResult.PaymentInfo> getPaymentInfo() {
        if (paymentInfo == null) {
            paymentInfo = new ArrayList<QueryPaymentLogResult.PaymentInfo>();
        }
        return this.paymentInfo;
    }

    /**
     * Gets the value of the totalRowNum property.
     * 
     */
    public long getTotalRowNum() {
        return totalRowNum;
    }

    /**
     * Sets the value of the totalRowNum property.
     * 
     */
    public void setTotalRowNum(long value) {
        this.totalRowNum = value;
    }

    /**
     * Gets the value of the beginRowNum property.
     * 
     */
    public long getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Sets the value of the beginRowNum property.
     * 
     */
    public void setBeginRowNum(long value) {
        this.beginRowNum = value;
    }

    /**
     * Gets the value of the fetchRowNum property.
     * 
     */
    public long getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Sets the value of the fetchRowNum property.
     * 
     */
    public void setFetchRowNum(long value) {
        this.fetchRowNum = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExtTransID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExtPayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PaymentTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="OriAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="OriCurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="CurrencyRate" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
     *         &lt;element name="PaymentDetail" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ApplyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ApplyAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="InvoiceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="InvoiceDetailID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ChargeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="TaxList" type="{http://cbs.huawei.com/ar/wsservice/arcommon}Tax" maxOccurs="unbounded" minOccurs="0"/>
     *                   &lt;element name="DiscountAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CardInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BankInfo" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BankInfo" minOccurs="0"/>
     *         &lt;element name="PayChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AccessMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="DeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="CurAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="TaxAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "custKey",
        "subKey",
        "primaryIdentity",
        "transID",
        "transType",
        "extTransID",
        "extPayType",
        "paymentTime",
        "amount",
        "currencyID",
        "oriAmount",
        "oriCurrencyID",
        "currencyRate",
        "paymentDetail",
        "paymentMethod",
        "cardInfo",
        "bankInfo",
        "payChannelID",
        "accessMode",
        "status",
        "operID",
        "deptID",
        "curAmount",
        "taxAmount",
        "additionalProperty"
    })
    public static class PaymentInfo {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "CustKey", required = true)
        protected String custKey;
        @XmlElement(name = "SubKey")
        protected String subKey;
        @XmlElement(name = "PrimaryIdentity")
        protected String primaryIdentity;
        @XmlElement(name = "TransID", required = true)
        protected String transID;
        @XmlElement(name = "TransType")
        protected String transType;
        @XmlElement(name = "ExtTransID")
        protected String extTransID;
        @XmlElement(name = "ExtPayType")
        protected String extPayType;
        @XmlElement(name = "PaymentTime", required = true)
        protected String paymentTime;
        @XmlElement(name = "Amount")
        protected long amount;
        @XmlElement(name = "CurrencyID", required = true)
        protected BigInteger currencyID;
        @XmlElement(name = "OriAmount")
        protected Long oriAmount;
        @XmlElement(name = "OriCurrencyID")
        protected BigInteger oriCurrencyID;
        @XmlElement(name = "CurrencyRate")
        protected Float currencyRate;
        @XmlElement(name = "PaymentDetail", required = true)
        protected List<QueryPaymentLogResult.PaymentInfo.PaymentDetail> paymentDetail;
        @XmlElement(name = "PaymentMethod")
        protected String paymentMethod;
        @XmlElement(name = "CardInfo")
        protected QueryPaymentLogResult.PaymentInfo.CardInfo cardInfo;
        @XmlElement(name = "BankInfo")
        protected BankInfo bankInfo;
        @XmlElement(name = "PayChannelID")
        protected String payChannelID;
        @XmlElement(name = "AccessMode")
        protected String accessMode;
        @XmlElement(name = "Status", required = true)
        protected String status;
        @XmlElement(name = "OperID")
        protected Long operID;
        @XmlElement(name = "DeptID")
        protected Long deptID;
        @XmlElement(name = "CurAmount")
        protected Long curAmount;
        @XmlElement(name = "TaxAmount")
        protected Long taxAmount;
        @XmlElement(name = "AdditionalProperty")
        protected List<SimpleProperty> additionalProperty;

        /**
         * Gets the value of the acctKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Sets the value of the acctKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Gets the value of the custKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustKey() {
            return custKey;
        }

        /**
         * Sets the value of the custKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustKey(String value) {
            this.custKey = value;
        }

        /**
         * Gets the value of the subKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubKey() {
            return subKey;
        }

        /**
         * Sets the value of the subKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubKey(String value) {
            this.subKey = value;
        }

        /**
         * Gets the value of the primaryIdentity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryIdentity() {
            return primaryIdentity;
        }

        /**
         * Sets the value of the primaryIdentity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryIdentity(String value) {
            this.primaryIdentity = value;
        }

        /**
         * Gets the value of the transID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransID() {
            return transID;
        }

        /**
         * Sets the value of the transID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransID(String value) {
            this.transID = value;
        }

        /**
         * Gets the value of the transType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransType() {
            return transType;
        }

        /**
         * Sets the value of the transType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransType(String value) {
            this.transType = value;
        }

        /**
         * Gets the value of the extTransID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtTransID() {
            return extTransID;
        }

        /**
         * Sets the value of the extTransID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtTransID(String value) {
            this.extTransID = value;
        }

        /**
         * Gets the value of the extPayType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtPayType() {
            return extPayType;
        }

        /**
         * Sets the value of the extPayType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtPayType(String value) {
            this.extPayType = value;
        }

        /**
         * Gets the value of the paymentTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaymentTime() {
            return paymentTime;
        }

        /**
         * Sets the value of the paymentTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaymentTime(String value) {
            this.paymentTime = value;
        }

        /**
         * Gets the value of the amount property.
         * 
         */
        public long getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         */
        public void setAmount(long value) {
            this.amount = value;
        }

        /**
         * Gets the value of the currencyID property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Sets the value of the currencyID property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Gets the value of the oriAmount property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getOriAmount() {
            return oriAmount;
        }

        /**
         * Sets the value of the oriAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setOriAmount(Long value) {
            this.oriAmount = value;
        }

        /**
         * Gets the value of the oriCurrencyID property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getOriCurrencyID() {
            return oriCurrencyID;
        }

        /**
         * Sets the value of the oriCurrencyID property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setOriCurrencyID(BigInteger value) {
            this.oriCurrencyID = value;
        }

        /**
         * Gets the value of the currencyRate property.
         * 
         * @return
         *     possible object is
         *     {@link Float }
         *     
         */
        public Float getCurrencyRate() {
            return currencyRate;
        }

        /**
         * Sets the value of the currencyRate property.
         * 
         * @param value
         *     allowed object is
         *     {@link Float }
         *     
         */
        public void setCurrencyRate(Float value) {
            this.currencyRate = value;
        }

        /**
         * Gets the value of the paymentDetail property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the paymentDetail property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPaymentDetail().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryPaymentLogResult.PaymentInfo.PaymentDetail }
         * 
         * 
         */
        public List<QueryPaymentLogResult.PaymentInfo.PaymentDetail> getPaymentDetail() {
            if (paymentDetail == null) {
                paymentDetail = new ArrayList<QueryPaymentLogResult.PaymentInfo.PaymentDetail>();
            }
            return this.paymentDetail;
        }

        /**
         * Gets the value of the paymentMethod property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaymentMethod() {
            return paymentMethod;
        }

        /**
         * Sets the value of the paymentMethod property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaymentMethod(String value) {
            this.paymentMethod = value;
        }

        /**
         * Gets the value of the cardInfo property.
         * 
         * @return
         *     possible object is
         *     {@link QueryPaymentLogResult.PaymentInfo.CardInfo }
         *     
         */
        public QueryPaymentLogResult.PaymentInfo.CardInfo getCardInfo() {
            return cardInfo;
        }

        /**
         * Sets the value of the cardInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryPaymentLogResult.PaymentInfo.CardInfo }
         *     
         */
        public void setCardInfo(QueryPaymentLogResult.PaymentInfo.CardInfo value) {
            this.cardInfo = value;
        }

        /**
         * Gets the value of the bankInfo property.
         * 
         * @return
         *     possible object is
         *     {@link BankInfo }
         *     
         */
        public BankInfo getBankInfo() {
            return bankInfo;
        }

        /**
         * Sets the value of the bankInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link BankInfo }
         *     
         */
        public void setBankInfo(BankInfo value) {
            this.bankInfo = value;
        }

        /**
         * Gets the value of the payChannelID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayChannelID() {
            return payChannelID;
        }

        /**
         * Sets the value of the payChannelID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayChannelID(String value) {
            this.payChannelID = value;
        }

        /**
         * Gets the value of the accessMode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccessMode() {
            return accessMode;
        }

        /**
         * Sets the value of the accessMode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccessMode(String value) {
            this.accessMode = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the operID property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getOperID() {
            return operID;
        }

        /**
         * Sets the value of the operID property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setOperID(Long value) {
            this.operID = value;
        }

        /**
         * Gets the value of the deptID property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getDeptID() {
            return deptID;
        }

        /**
         * Sets the value of the deptID property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setDeptID(Long value) {
            this.deptID = value;
        }

        /**
         * Gets the value of the curAmount property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getCurAmount() {
            return curAmount;
        }

        /**
         * Sets the value of the curAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setCurAmount(Long value) {
            this.curAmount = value;
        }

        /**
         * Gets the value of the taxAmount property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getTaxAmount() {
            return taxAmount;
        }

        /**
         * Sets the value of the taxAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setTaxAmount(Long value) {
            this.taxAmount = value;
        }

        /**
         * Gets the value of the additionalProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdditionalProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getAdditionalProperty() {
            if (additionalProperty == null) {
                additionalProperty = new ArrayList<SimpleProperty>();
            }
            return this.additionalProperty;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CardPinNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="CardSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cardPinNumber",
            "cardSequence"
        })
        public static class CardInfo {

            @XmlElement(name = "CardPinNumber")
            protected String cardPinNumber;
            @XmlElement(name = "CardSequence")
            protected String cardSequence;

            /**
             * Gets the value of the cardPinNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCardPinNumber() {
                return cardPinNumber;
            }

            /**
             * Sets the value of the cardPinNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCardPinNumber(String value) {
                this.cardPinNumber = value;
            }

            /**
             * Gets the value of the cardSequence property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCardSequence() {
                return cardSequence;
            }

            /**
             * Sets the value of the cardSequence property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCardSequence(String value) {
                this.cardSequence = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ApplyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ApplyAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="InvoiceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="InvoiceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="InvoiceDetailID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ChargeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="TaxList" type="{http://cbs.huawei.com/ar/wsservice/arcommon}Tax" maxOccurs="unbounded" minOccurs="0"/>
         *         &lt;element name="DiscountAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "applyType",
            "applyAmount",
            "invoiceNo",
            "invoiceID",
            "invoiceDetailID",
            "chargeCode",
            "chargeAmount",
            "taxList",
            "discountAmount"
        })
        public static class PaymentDetail {

            @XmlElement(name = "ApplyType", required = true)
            protected String applyType;
            @XmlElement(name = "ApplyAmount")
            protected long applyAmount;
            @XmlElement(name = "InvoiceNo")
            protected String invoiceNo;
            @XmlElement(name = "InvoiceID")
            protected Long invoiceID;
            @XmlElement(name = "InvoiceDetailID")
            protected Long invoiceDetailID;
            @XmlElement(name = "ChargeCode")
            protected String chargeCode;
            @XmlElement(name = "ChargeAmount")
            protected Long chargeAmount;
            @XmlElement(name = "TaxList")
            protected List<Tax> taxList;
            @XmlElement(name = "DiscountAmount")
            protected Long discountAmount;

            /**
             * Gets the value of the applyType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getApplyType() {
                return applyType;
            }

            /**
             * Sets the value of the applyType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setApplyType(String value) {
                this.applyType = value;
            }

            /**
             * Gets the value of the applyAmount property.
             * 
             */
            public long getApplyAmount() {
                return applyAmount;
            }

            /**
             * Sets the value of the applyAmount property.
             * 
             */
            public void setApplyAmount(long value) {
                this.applyAmount = value;
            }

            /**
             * Gets the value of the invoiceNo property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvoiceNo() {
                return invoiceNo;
            }

            /**
             * Sets the value of the invoiceNo property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvoiceNo(String value) {
                this.invoiceNo = value;
            }

            /**
             * Gets the value of the invoiceID property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getInvoiceID() {
                return invoiceID;
            }

            /**
             * Sets the value of the invoiceID property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setInvoiceID(Long value) {
                this.invoiceID = value;
            }

            /**
             * Gets the value of the invoiceDetailID property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getInvoiceDetailID() {
                return invoiceDetailID;
            }

            /**
             * Sets the value of the invoiceDetailID property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setInvoiceDetailID(Long value) {
                this.invoiceDetailID = value;
            }

            /**
             * Gets the value of the chargeCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChargeCode() {
                return chargeCode;
            }

            /**
             * Sets the value of the chargeCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChargeCode(String value) {
                this.chargeCode = value;
            }

            /**
             * Gets the value of the chargeAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getChargeAmount() {
                return chargeAmount;
            }

            /**
             * Sets the value of the chargeAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setChargeAmount(Long value) {
                this.chargeAmount = value;
            }

            /**
             * Gets the value of the taxList property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the taxList property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTaxList().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Tax }
             * 
             * 
             */
            public List<Tax> getTaxList() {
                if (taxList == null) {
                    taxList = new ArrayList<Tax>();
                }
                return this.taxList;
            }

            /**
             * Gets the value of the discountAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getDiscountAmount() {
                return discountAmount;
            }

            /**
             * Sets the value of the discountAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setDiscountAmount(Long value) {
                this.discountAmount = value;
            }

        }

    }

}
