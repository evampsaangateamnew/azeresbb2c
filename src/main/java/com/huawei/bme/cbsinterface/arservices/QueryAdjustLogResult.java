
package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;


/**
 * <p>Java class for QueryAdjustLogResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryAdjustLogResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdjustInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TradeTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="ExtTransID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FreeUnitAdjustmentInfo" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BalanceAdjustmentInfo" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="DeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="Remark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryAdjustLogResult", propOrder = {
    "adjustInfo",
    "totalRowNum",
    "beginRowNum",
    "fetchRowNum"
})
public class QueryAdjustLogResult {

    @XmlElement(name = "AdjustInfo")
    protected List<QueryAdjustLogResult.AdjustInfo> adjustInfo;
    @XmlElement(name = "TotalRowNum", required = true)
    protected BigInteger totalRowNum;
    @XmlElement(name = "BeginRowNum", required = true)
    protected BigInteger beginRowNum;
    @XmlElement(name = "FetchRowNum", required = true)
    protected BigInteger fetchRowNum;

    /**
     * Gets the value of the adjustInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjustInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjustInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryAdjustLogResult.AdjustInfo }
     * 
     * 
     */
    public List<QueryAdjustLogResult.AdjustInfo> getAdjustInfo() {
        if (adjustInfo == null) {
            adjustInfo = new ArrayList<QueryAdjustLogResult.AdjustInfo>();
        }
        return this.adjustInfo;
    }

    /**
     * Gets the value of the totalRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalRowNum() {
        return totalRowNum;
    }

    /**
     * Sets the value of the totalRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalRowNum(BigInteger value) {
        this.totalRowNum = value;
    }

    /**
     * Gets the value of the beginRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Sets the value of the beginRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBeginRowNum(BigInteger value) {
        this.beginRowNum = value;
    }

    /**
     * Gets the value of the fetchRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Sets the value of the fetchRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFetchRowNum(BigInteger value) {
        this.fetchRowNum = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TradeTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TransID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="ExtTransID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FreeUnitAdjustmentInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BalanceAdjustmentInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="OperID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="DeptID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="Remark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AdditionalProperty" type="{http://cbs.huawei.com/ar/wsservice/arcommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tradeTime",
        "acctKey",
        "subKey",
        "primaryIdentity",
        "channelID",
        "transID",
        "extTransID",
        "freeUnitAdjustmentInfo",
        "balanceAdjustmentInfo",
        "operID",
        "deptID",
        "remark",
        "reason",
        "additionalProperty"
    })
    public static class AdjustInfo {

        @XmlElement(name = "TradeTime", required = true)
        protected String tradeTime;
        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "SubKey")
        protected String subKey;
        @XmlElement(name = "PrimaryIdentity")
        protected String primaryIdentity;
        @XmlElement(name = "ChannelID")
        protected String channelID;
        @XmlElement(name = "TransID")
        protected long transID;
        @XmlElement(name = "ExtTransID")
        protected String extTransID;
        @XmlElement(name = "FreeUnitAdjustmentInfo")
        protected List<QueryAdjustLogResult.AdjustInfo.FreeUnitAdjustmentInfo> freeUnitAdjustmentInfo;
        @XmlElement(name = "BalanceAdjustmentInfo")
        protected List<QueryAdjustLogResult.AdjustInfo.BalanceAdjustmentInfo> balanceAdjustmentInfo;
        @XmlElement(name = "OperID")
        protected Long operID;
        @XmlElement(name = "DeptID")
        protected Long deptID;
        @XmlElement(name = "Remark")
        protected String remark;
        @XmlElement(name = "Reason", required = true)
        protected String reason;
        @XmlElement(name = "AdditionalProperty")
        protected List<SimpleProperty> additionalProperty;

        /**
         * Gets the value of the tradeTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTradeTime() {
            return tradeTime;
        }

        /**
         * Sets the value of the tradeTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTradeTime(String value) {
            this.tradeTime = value;
        }

        /**
         * Gets the value of the acctKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Sets the value of the acctKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Gets the value of the subKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubKey() {
            return subKey;
        }

        /**
         * Sets the value of the subKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubKey(String value) {
            this.subKey = value;
        }

        /**
         * Gets the value of the primaryIdentity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryIdentity() {
            return primaryIdentity;
        }

        /**
         * Sets the value of the primaryIdentity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryIdentity(String value) {
            this.primaryIdentity = value;
        }

        /**
         * Gets the value of the channelID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannelID() {
            return channelID;
        }

        /**
         * Sets the value of the channelID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannelID(String value) {
            this.channelID = value;
        }

        /**
         * Gets the value of the transID property.
         * 
         */
        public long getTransID() {
            return transID;
        }

        /**
         * Sets the value of the transID property.
         * 
         */
        public void setTransID(long value) {
            this.transID = value;
        }

        /**
         * Gets the value of the extTransID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtTransID() {
            return extTransID;
        }

        /**
         * Sets the value of the extTransID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtTransID(String value) {
            this.extTransID = value;
        }

        /**
         * Gets the value of the freeUnitAdjustmentInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the freeUnitAdjustmentInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFreeUnitAdjustmentInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryAdjustLogResult.AdjustInfo.FreeUnitAdjustmentInfo }
         * 
         * 
         */
        public List<QueryAdjustLogResult.AdjustInfo.FreeUnitAdjustmentInfo> getFreeUnitAdjustmentInfo() {
            if (freeUnitAdjustmentInfo == null) {
                freeUnitAdjustmentInfo = new ArrayList<QueryAdjustLogResult.AdjustInfo.FreeUnitAdjustmentInfo>();
            }
            return this.freeUnitAdjustmentInfo;
        }

        /**
         * Gets the value of the balanceAdjustmentInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceAdjustmentInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceAdjustmentInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryAdjustLogResult.AdjustInfo.BalanceAdjustmentInfo }
         * 
         * 
         */
        public List<QueryAdjustLogResult.AdjustInfo.BalanceAdjustmentInfo> getBalanceAdjustmentInfo() {
            if (balanceAdjustmentInfo == null) {
                balanceAdjustmentInfo = new ArrayList<QueryAdjustLogResult.AdjustInfo.BalanceAdjustmentInfo>();
            }
            return this.balanceAdjustmentInfo;
        }

        /**
         * Gets the value of the operID property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getOperID() {
            return operID;
        }

        /**
         * Sets the value of the operID property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setOperID(Long value) {
            this.operID = value;
        }

        /**
         * Gets the value of the deptID property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getDeptID() {
            return deptID;
        }

        /**
         * Sets the value of the deptID property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setDeptID(Long value) {
            this.deptID = value;
        }

        /**
         * Gets the value of the remark property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemark() {
            return remark;
        }

        /**
         * Sets the value of the remark property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemark(String value) {
            this.remark = value;
        }

        /**
         * Gets the value of the reason property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReason() {
            return reason;
        }

        /**
         * Sets the value of the reason property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReason(String value) {
            this.reason = value;
        }

        /**
         * Gets the value of the additionalProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdditionalProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getAdditionalProperty() {
            if (additionalProperty == null) {
                additionalProperty = new ArrayList<SimpleProperty>();
            }
            return this.additionalProperty;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "balanceID",
            "balanceType",
            "adjustmentType",
            "adjustmentAmt",
            "currencyID",
            "effectiveTime",
            "expireTime"
        })
        public static class BalanceAdjustmentInfo {

            @XmlElement(name = "BalanceID")
            protected Long balanceID;
            @XmlElement(name = "BalanceType")
            protected String balanceType;
            @XmlElement(name = "AdjustmentType")
            protected String adjustmentType;
            @XmlElement(name = "AdjustmentAmt")
            protected Long adjustmentAmt;
            @XmlElement(name = "CurrencyID")
            protected BigInteger currencyID;
            @XmlElement(name = "EffectiveTime")
            protected String effectiveTime;
            @XmlElement(name = "ExpireTime")
            protected String expireTime;

            /**
             * Gets the value of the balanceID property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getBalanceID() {
                return balanceID;
            }

            /**
             * Sets the value of the balanceID property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setBalanceID(Long value) {
                this.balanceID = value;
            }

            /**
             * Gets the value of the balanceType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalanceType() {
                return balanceType;
            }

            /**
             * Sets the value of the balanceType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalanceType(String value) {
                this.balanceType = value;
            }

            /**
             * Gets the value of the adjustmentType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAdjustmentType() {
                return adjustmentType;
            }

            /**
             * Sets the value of the adjustmentType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAdjustmentType(String value) {
                this.adjustmentType = value;
            }

            /**
             * Gets the value of the adjustmentAmt property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getAdjustmentAmt() {
                return adjustmentAmt;
            }

            /**
             * Sets the value of the adjustmentAmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setAdjustmentAmt(Long value) {
                this.adjustmentAmt = value;
            }

            /**
             * Gets the value of the currencyID property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCurrencyID() {
                return currencyID;
            }

            /**
             * Sets the value of the currencyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCurrencyID(BigInteger value) {
                this.currencyID = value;
            }

            /**
             * Gets the value of the effectiveTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Sets the value of the effectiveTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Gets the value of the expireTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpireTime() {
                return expireTime;
            }

            /**
             * Sets the value of the expireTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpireTime(String value) {
                this.expireTime = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "freeUnitInstanceID",
            "freeUnitType",
            "adjustmentType",
            "adjustmentAmt",
            "measureUnit",
            "effectiveTime",
            "expireTime"
        })
        public static class FreeUnitAdjustmentInfo {

            @XmlElement(name = "FreeUnitInstanceID")
            protected Long freeUnitInstanceID;
            @XmlElement(name = "FreeUnitType")
            protected String freeUnitType;
            @XmlElement(name = "AdjustmentType")
            protected String adjustmentType;
            @XmlElement(name = "AdjustmentAmt")
            protected Long adjustmentAmt;
            @XmlElement(name = "MeasureUnit")
            protected BigInteger measureUnit;
            @XmlElement(name = "EffectiveTime")
            protected String effectiveTime;
            @XmlElement(name = "ExpireTime")
            protected String expireTime;

            /**
             * Gets the value of the freeUnitInstanceID property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getFreeUnitInstanceID() {
                return freeUnitInstanceID;
            }

            /**
             * Sets the value of the freeUnitInstanceID property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setFreeUnitInstanceID(Long value) {
                this.freeUnitInstanceID = value;
            }

            /**
             * Gets the value of the freeUnitType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeUnitType() {
                return freeUnitType;
            }

            /**
             * Sets the value of the freeUnitType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeUnitType(String value) {
                this.freeUnitType = value;
            }

            /**
             * Gets the value of the adjustmentType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAdjustmentType() {
                return adjustmentType;
            }

            /**
             * Sets the value of the adjustmentType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAdjustmentType(String value) {
                this.adjustmentType = value;
            }

            /**
             * Gets the value of the adjustmentAmt property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getAdjustmentAmt() {
                return adjustmentAmt;
            }

            /**
             * Sets the value of the adjustmentAmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setAdjustmentAmt(Long value) {
                this.adjustmentAmt = value;
            }

            /**
             * Gets the value of the measureUnit property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMeasureUnit() {
                return measureUnit;
            }

            /**
             * Sets the value of the measureUnit property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMeasureUnit(BigInteger value) {
                this.measureUnit = value;
            }

            /**
             * Gets the value of the effectiveTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Sets the value of the effectiveTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Gets the value of the expireTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpireTime() {
                return expireTime;
            }

            /**
             * Sets the value of the expireTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpireTime(String value) {
                this.expireTime = value;
            }

        }

    }

}
