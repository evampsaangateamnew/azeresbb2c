package com.huawei.bme.cbsinterface.arservices;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;
import com.evampsaanga.configs.Config;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.2.4-b01 Generated
 * source version: 2.2
 * 
 */
@WebServiceClient(name = "ArServices", targetNamespace = "http://www.huawei.com/bme/cbsinterface/arservices", wsdlLocation = "file:/D:/Projects/Adeel/Java%20work%20space/Delete%20me/WebContent/CBSInterface_AR_Services_ECare.wsdl")
public class ArServices_Service extends Service {
	private final static URL ARSERVICES_WSDL_LOCATION;
	private final static WebServiceException ARSERVICES_EXCEPTION;
	private final static QName ARSERVICES_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/arservices",
			"ArServices");
	static {
		URL url = null;
		WebServiceException e = null;
		try {
			url = new URL(Config.CBS_AR_WSDL_PATH);
		} catch (MalformedURLException ex) {
			e = new WebServiceException(ex);
		}
		ARSERVICES_WSDL_LOCATION = url;
		ARSERVICES_EXCEPTION = e;
	}

	public ArServices_Service() {
		super(__getWsdlLocation(), ARSERVICES_QNAME);
	}

	public ArServices_Service(WebServiceFeature... features) {
		super(__getWsdlLocation(), ARSERVICES_QNAME, features);
	}

	public ArServices_Service(URL wsdlLocation) {
		super(wsdlLocation, ARSERVICES_QNAME);
	}

	public ArServices_Service(URL wsdlLocation, WebServiceFeature... features) {
		super(wsdlLocation, ARSERVICES_QNAME, features);
	}

	public ArServices_Service(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public ArServices_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	/**
	 * 
	 * @return returns ArServices
	 */
	@WebEndpoint(name = "ArServicesPort")
	public ArServices getArServicesPort() {
		return super.getPort(new QName("http://www.huawei.com/bme/cbsinterface/arservices", "ArServicesPort"),
				ArServices.class);
	}

	/**
	 * 
	 * @param features
	 *            A list of {@link javax.xml.ws.WebServiceFeature} to configure
	 *            on the proxy. Supported features not in the
	 *            <code>features</code> parameter will have their default
	 *            values.
	 * @return returns ArServices
	 */
	@WebEndpoint(name = "ArServicesPort")
	public ArServices getArServicesPort(WebServiceFeature... features) {
		return super.getPort(new QName("http://www.huawei.com/bme/cbsinterface/arservices", "ArServicesPort"),
				ArServices.class, features);
	}

	private static URL __getWsdlLocation() {
		if (ARSERVICES_EXCEPTION != null) {
			throw ARSERVICES_EXCEPTION;
		}
		return ARSERVICES_WSDL_LOCATION;
	}
}
