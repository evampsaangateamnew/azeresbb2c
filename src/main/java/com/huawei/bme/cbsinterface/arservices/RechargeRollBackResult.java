
package com.huawei.bme.cbsinterface.arservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.cbs.ar.wsservice.arcommon.BalanceChgInfo;
import com.huawei.cbs.ar.wsservice.arcommon.LoanChgInfo;


/**
 * <p>Java class for RechargeRollBackResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RechargeRollBackResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RechargeSerialNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BalanceRollBack" type="{http://cbs.huawei.com/ar/wsservice/arcommon}BalanceChgInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LoanRollBack" type="{http://cbs.huawei.com/ar/wsservice/arcommon}LoanChgInfo" minOccurs="0"/>
 *         &lt;element name="BonusRollBack" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="RollBonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RollBonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LifeCycleRollBack" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OldLifeCycleStatus" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="NewLifeCycleStatus" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RollBackValidity" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RechargeRollBackResult", propOrder = {
    "rechargeSerialNo",
    "balanceRollBack",
    "loanRollBack",
    "bonusRollBack",
    "lifeCycleRollBack"
})
public class RechargeRollBackResult {

    @XmlElement(name = "RechargeSerialNo", required = true)
    protected String rechargeSerialNo;
    @XmlElement(name = "BalanceRollBack")
    protected List<BalanceChgInfo> balanceRollBack;
    @XmlElement(name = "LoanRollBack")
    protected LoanChgInfo loanRollBack;
    @XmlElement(name = "BonusRollBack")
    protected RechargeRollBackResult.BonusRollBack bonusRollBack;
    @XmlElement(name = "LifeCycleRollBack")
    protected RechargeRollBackResult.LifeCycleRollBack lifeCycleRollBack;

    /**
     * Gets the value of the rechargeSerialNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRechargeSerialNo() {
        return rechargeSerialNo;
    }

    /**
     * Sets the value of the rechargeSerialNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRechargeSerialNo(String value) {
        this.rechargeSerialNo = value;
    }

    /**
     * Gets the value of the balanceRollBack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the balanceRollBack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBalanceRollBack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BalanceChgInfo }
     * 
     * 
     */
    public List<BalanceChgInfo> getBalanceRollBack() {
        if (balanceRollBack == null) {
            balanceRollBack = new ArrayList<BalanceChgInfo>();
        }
        return this.balanceRollBack;
    }

    /**
     * Gets the value of the loanRollBack property.
     * 
     * @return
     *     possible object is
     *     {@link LoanChgInfo }
     *     
     */
    public LoanChgInfo getLoanRollBack() {
        return loanRollBack;
    }

    /**
     * Sets the value of the loanRollBack property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanChgInfo }
     *     
     */
    public void setLoanRollBack(LoanChgInfo value) {
        this.loanRollBack = value;
    }

    /**
     * Gets the value of the bonusRollBack property.
     * 
     * @return
     *     possible object is
     *     {@link RechargeRollBackResult.BonusRollBack }
     *     
     */
    public RechargeRollBackResult.BonusRollBack getBonusRollBack() {
        return bonusRollBack;
    }

    /**
     * Sets the value of the bonusRollBack property.
     * 
     * @param value
     *     allowed object is
     *     {@link RechargeRollBackResult.BonusRollBack }
     *     
     */
    public void setBonusRollBack(RechargeRollBackResult.BonusRollBack value) {
        this.bonusRollBack = value;
    }

    /**
     * Gets the value of the lifeCycleRollBack property.
     * 
     * @return
     *     possible object is
     *     {@link RechargeRollBackResult.LifeCycleRollBack }
     *     
     */
    public RechargeRollBackResult.LifeCycleRollBack getLifeCycleRollBack() {
        return lifeCycleRollBack;
    }

    /**
     * Sets the value of the lifeCycleRollBack property.
     * 
     * @param value
     *     allowed object is
     *     {@link RechargeRollBackResult.LifeCycleRollBack }
     *     
     */
    public void setLifeCycleRollBack(RechargeRollBackResult.LifeCycleRollBack value) {
        this.lifeCycleRollBack = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FreeUnitItemList" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="RollBonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BalanceList" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RollBonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "freeUnitItemList",
        "balanceList"
    })
    public static class BonusRollBack {

        @XmlElement(name = "FreeUnitItemList")
        protected List<RechargeRollBackResult.BonusRollBack.FreeUnitItemList> freeUnitItemList;
        @XmlElement(name = "BalanceList")
        protected List<RechargeRollBackResult.BonusRollBack.BalanceList> balanceList;

        /**
         * Gets the value of the freeUnitItemList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the freeUnitItemList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFreeUnitItemList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RechargeRollBackResult.BonusRollBack.FreeUnitItemList }
         * 
         * 
         */
        public List<RechargeRollBackResult.BonusRollBack.FreeUnitItemList> getFreeUnitItemList() {
            if (freeUnitItemList == null) {
                freeUnitItemList = new ArrayList<RechargeRollBackResult.BonusRollBack.FreeUnitItemList>();
            }
            return this.freeUnitItemList;
        }

        /**
         * Gets the value of the balanceList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RechargeRollBackResult.BonusRollBack.BalanceList }
         * 
         * 
         */
        public List<RechargeRollBackResult.BonusRollBack.BalanceList> getBalanceList() {
            if (balanceList == null) {
                balanceList = new ArrayList<RechargeRollBackResult.BonusRollBack.BalanceList>();
            }
            return this.balanceList;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RollBonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "balanceType",
            "balanceID",
            "balanceTypeName",
            "rollBonusAmt",
            "currencyID"
        })
        public static class BalanceList {

            @XmlElement(name = "BalanceType", required = true)
            protected String balanceType;
            @XmlElement(name = "BalanceID")
            protected String balanceID;
            @XmlElement(name = "BalanceTypeName", required = true)
            protected String balanceTypeName;
            @XmlElement(name = "RollBonusAmt")
            protected long rollBonusAmt;
            @XmlElement(name = "CurrencyID", required = true)
            protected BigInteger currencyID;

            /**
             * Gets the value of the balanceType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalanceType() {
                return balanceType;
            }

            /**
             * Sets the value of the balanceType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalanceType(String value) {
                this.balanceType = value;
            }

            /**
             * Gets the value of the balanceID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalanceID() {
                return balanceID;
            }

            /**
             * Sets the value of the balanceID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalanceID(String value) {
                this.balanceID = value;
            }

            /**
             * Gets the value of the balanceTypeName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalanceTypeName() {
                return balanceTypeName;
            }

            /**
             * Sets the value of the balanceTypeName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalanceTypeName(String value) {
                this.balanceTypeName = value;
            }

            /**
             * Gets the value of the rollBonusAmt property.
             * 
             */
            public long getRollBonusAmt() {
                return rollBonusAmt;
            }

            /**
             * Sets the value of the rollBonusAmt property.
             * 
             */
            public void setRollBonusAmt(long value) {
                this.rollBonusAmt = value;
            }

            /**
             * Gets the value of the currencyID property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCurrencyID() {
                return currencyID;
            }

            /**
             * Sets the value of the currencyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCurrencyID(BigInteger value) {
                this.currencyID = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="FreeUnitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="RollBonusAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "freeUnitID",
            "freeUnitType",
            "freeUnitTypeName",
            "measureUnit",
            "measureUnitName",
            "rollBonusAmt"
        })
        public static class FreeUnitItemList {

            @XmlElement(name = "FreeUnitID", required = true)
            protected String freeUnitID;
            @XmlElement(name = "FreeUnitType", required = true)
            protected String freeUnitType;
            @XmlElement(name = "FreeUnitTypeName")
            protected String freeUnitTypeName;
            @XmlElement(name = "MeasureUnit", required = true)
            protected String measureUnit;
            @XmlElement(name = "MeasureUnitName")
            protected String measureUnitName;
            @XmlElement(name = "RollBonusAmt", required = true, type = Long.class, nillable = true)
            protected Long rollBonusAmt;

            /**
             * Gets the value of the freeUnitID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeUnitID() {
                return freeUnitID;
            }

            /**
             * Sets the value of the freeUnitID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeUnitID(String value) {
                this.freeUnitID = value;
            }

            /**
             * Gets the value of the freeUnitType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeUnitType() {
                return freeUnitType;
            }

            /**
             * Sets the value of the freeUnitType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeUnitType(String value) {
                this.freeUnitType = value;
            }

            /**
             * Gets the value of the freeUnitTypeName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeUnitTypeName() {
                return freeUnitTypeName;
            }

            /**
             * Sets the value of the freeUnitTypeName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeUnitTypeName(String value) {
                this.freeUnitTypeName = value;
            }

            /**
             * Gets the value of the measureUnit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMeasureUnit() {
                return measureUnit;
            }

            /**
             * Sets the value of the measureUnit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMeasureUnit(String value) {
                this.measureUnit = value;
            }

            /**
             * Gets the value of the measureUnitName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMeasureUnitName() {
                return measureUnitName;
            }

            /**
             * Sets the value of the measureUnitName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMeasureUnitName(String value) {
                this.measureUnitName = value;
            }

            /**
             * Gets the value of the rollBonusAmt property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getRollBonusAmt() {
                return rollBonusAmt;
            }

            /**
             * Sets the value of the rollBonusAmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setRollBonusAmt(Long value) {
                this.rollBonusAmt = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OldLifeCycleStatus" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="NewLifeCycleStatus" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RollBackValidity" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "oldLifeCycleStatus",
        "newLifeCycleStatus",
        "rollBackValidity"
    })
    public static class LifeCycleRollBack {

        @XmlElement(name = "OldLifeCycleStatus", required = true)
        protected List<RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus> oldLifeCycleStatus;
        @XmlElement(name = "NewLifeCycleStatus", required = true)
        protected List<RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus> newLifeCycleStatus;
        @XmlElement(name = "RollBackValidity", required = true)
        protected BigInteger rollBackValidity;

        /**
         * Gets the value of the oldLifeCycleStatus property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the oldLifeCycleStatus property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOldLifeCycleStatus().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus }
         * 
         * 
         */
        public List<RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus> getOldLifeCycleStatus() {
            if (oldLifeCycleStatus == null) {
                oldLifeCycleStatus = new ArrayList<RechargeRollBackResult.LifeCycleRollBack.OldLifeCycleStatus>();
            }
            return this.oldLifeCycleStatus;
        }

        /**
         * Gets the value of the newLifeCycleStatus property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the newLifeCycleStatus property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNewLifeCycleStatus().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus }
         * 
         * 
         */
        public List<RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus> getNewLifeCycleStatus() {
            if (newLifeCycleStatus == null) {
                newLifeCycleStatus = new ArrayList<RechargeRollBackResult.LifeCycleRollBack.NewLifeCycleStatus>();
            }
            return this.newLifeCycleStatus;
        }

        /**
         * Gets the value of the rollBackValidity property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRollBackValidity() {
            return rollBackValidity;
        }

        /**
         * Sets the value of the rollBackValidity property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRollBackValidity(BigInteger value) {
            this.rollBackValidity = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "statusName",
            "statusExpireTime",
            "statusIndex"
        })
        public static class NewLifeCycleStatus {

            @XmlElement(name = "StatusName", required = true)
            protected String statusName;
            @XmlElement(name = "StatusExpireTime", required = true)
            protected String statusExpireTime;
            @XmlElement(name = "StatusIndex", required = true)
            protected String statusIndex;

            /**
             * Gets the value of the statusName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusName() {
                return statusName;
            }

            /**
             * Sets the value of the statusName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusName(String value) {
                this.statusName = value;
            }

            /**
             * Gets the value of the statusExpireTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusExpireTime() {
                return statusExpireTime;
            }

            /**
             * Sets the value of the statusExpireTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusExpireTime(String value) {
                this.statusExpireTime = value;
            }

            /**
             * Gets the value of the statusIndex property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusIndex() {
                return statusIndex;
            }

            /**
             * Sets the value of the statusIndex property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusIndex(String value) {
                this.statusIndex = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "statusName",
            "statusExpireTime",
            "statusIndex"
        })
        public static class OldLifeCycleStatus {

            @XmlElement(name = "StatusName", required = true)
            protected String statusName;
            @XmlElement(name = "StatusExpireTime", required = true)
            protected String statusExpireTime;
            @XmlElement(name = "StatusIndex", required = true)
            protected String statusIndex;

            /**
             * Gets the value of the statusName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusName() {
                return statusName;
            }

            /**
             * Sets the value of the statusName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusName(String value) {
                this.statusName = value;
            }

            /**
             * Gets the value of the statusExpireTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusExpireTime() {
                return statusExpireTime;
            }

            /**
             * Sets the value of the statusExpireTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusExpireTime(String value) {
                this.statusExpireTime = value;
            }

            /**
             * Gets the value of the statusIndex property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusIndex() {
                return statusIndex;
            }

            /**
             * Sets the value of the statusIndex property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusIndex(String value) {
                this.statusIndex = value;
            }

        }

    }

}
