package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for QueryCorporateInstallmentRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QueryCorporateInstallmentRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GroupMemberList" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubAccessCode">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode">
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="FirstPaymentList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FirstPaymentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryCorporateInstallmentRequest", propOrder = { "groupCode", "groupMemberList" })
public class QueryCorporateInstallmentRequest {
	@XmlElement(name = "GroupCode", required = true)
	protected String groupCode;
	@XmlElement(name = "GroupMemberList", required = true)
	protected List<QueryCorporateInstallmentRequest.GroupMemberList> groupMemberList;

	/**
	 * Gets the value of the groupCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGroupCode() {
		return groupCode;
	}

	/**
	 * Sets the value of the groupCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGroupCode(String value) {
		this.groupCode = value;
	}

	/**
	 * Gets the value of the groupMemberList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the groupMemberList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGroupMemberList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link QueryCorporateInstallmentRequest.GroupMemberList }
	 * 
	 * 
	 */
	public List<QueryCorporateInstallmentRequest.GroupMemberList> getGroupMemberList() {
		if (groupMemberList == null) {
			groupMemberList = new ArrayList<QueryCorporateInstallmentRequest.GroupMemberList>();
		}
		return this.groupMemberList;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="SubAccessCode">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode">
	 *               &lt;/extension>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="FirstPaymentList" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="FirstPaymentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "subAccessCode", "firstPaymentList" })
	public static class GroupMemberList {
		@XmlElement(name = "SubAccessCode", required = true)
		protected QueryCorporateInstallmentRequest.GroupMemberList.SubAccessCode subAccessCode;
		@XmlElement(name = "FirstPaymentList")
		protected List<QueryCorporateInstallmentRequest.GroupMemberList.FirstPaymentList> firstPaymentList;

		/**
		 * Gets the value of the subAccessCode property.
		 * 
		 * @return possible object is
		 *         {@link QueryCorporateInstallmentRequest.GroupMemberList.SubAccessCode }
		 * 
		 */
		public QueryCorporateInstallmentRequest.GroupMemberList.SubAccessCode getSubAccessCode() {
			return subAccessCode;
		}

		/**
		 * Sets the value of the subAccessCode property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link QueryCorporateInstallmentRequest.GroupMemberList.SubAccessCode }
		 * 
		 */
		public void setSubAccessCode(QueryCorporateInstallmentRequest.GroupMemberList.SubAccessCode value) {
			this.subAccessCode = value;
		}

		/**
		 * Gets the value of the firstPaymentList property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the firstPaymentList property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getFirstPaymentList().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link QueryCorporateInstallmentRequest.GroupMemberList.FirstPaymentList }
		 * 
		 * 
		 */
		public List<QueryCorporateInstallmentRequest.GroupMemberList.FirstPaymentList> getFirstPaymentList() {
			if (firstPaymentList == null) {
				firstPaymentList = new ArrayList<QueryCorporateInstallmentRequest.GroupMemberList.FirstPaymentList>();
			}
			return this.firstPaymentList;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="FirstPaymentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "contractID", "firstPaymentAmount" })
		public static class FirstPaymentList {
			@XmlElement(name = "ContractID", required = true)
			protected String contractID;
			@XmlElement(name = "FirstPaymentAmount")
			protected long firstPaymentAmount;

			/**
			 * Gets the value of the contractID property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getContractID() {
				return contractID;
			}

			/**
			 * Sets the value of the contractID property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setContractID(String value) {
				this.contractID = value;
			}

			/**
			 * Gets the value of the firstPaymentAmount property.
			 * 
			 */
			public long getFirstPaymentAmount() {
				return firstPaymentAmount;
			}

			/**
			 * Sets the value of the firstPaymentAmount property.
			 * 
			 */
			public void setFirstPaymentAmount(long value) {
				this.firstPaymentAmount = value;
			}
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode">
		 *     &lt;/extension>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class SubAccessCode extends com.huawei.bme.cbsinterface.bccommon.SubAccessCode {
		}
	}
}
