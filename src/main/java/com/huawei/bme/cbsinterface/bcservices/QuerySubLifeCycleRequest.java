package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;

/**
 * <p>
 * Java class for QuerySubLifeCycleRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QuerySubLifeCycleRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubLifeCycleRequest", propOrder = { "subAccessCode" })
public class QuerySubLifeCycleRequest {
	@XmlElement(name = "SubAccessCode", required = true)
	protected SubAccessCode subAccessCode;

	/**
	 * Gets the value of the subAccessCode property.
	 * 
	 * @return possible object is {@link SubAccessCode }
	 * 
	 */
	public SubAccessCode getSubAccessCode() {
		return subAccessCode;
	}

	/**
	 * Sets the value of the subAccessCode property.
	 * 
	 * @param value
	 *            allowed object is {@link SubAccessCode }
	 * 
	 */
	public void setSubAccessCode(SubAccessCode value) {
		this.subAccessCode = value;
	}
}
