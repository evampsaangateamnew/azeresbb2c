
package com.huawei.bme.cbsinterface.bcservices.querypayment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.bme.cbsinterface.cbscommon.querypayment.RequestHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader"/>
 *         &lt;element name="QuerySubLifeCycleRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QuerySubLifeCycleRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "querySubLifeCycleRequest"
})
@XmlRootElement(name = "QuerySubLifeCycleRequestMsg")
public class QuerySubLifeCycleRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "QuerySubLifeCycleRequest", namespace = "", required = true)
    protected QuerySubLifeCycleRequest querySubLifeCycleRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the querySubLifeCycleRequest property.
     * 
     * @return
     *     possible object is
     *     {@link QuerySubLifeCycleRequest }
     *     
     */
    public QuerySubLifeCycleRequest getQuerySubLifeCycleRequest() {
        return querySubLifeCycleRequest;
    }

    /**
     * Sets the value of the querySubLifeCycleRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuerySubLifeCycleRequest }
     *     
     */
    public void setQuerySubLifeCycleRequest(QuerySubLifeCycleRequest value) {
        this.querySubLifeCycleRequest = value;
    }

}
