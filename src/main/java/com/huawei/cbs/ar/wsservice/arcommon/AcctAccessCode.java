
package com.huawei.cbs.ar.wsservice.arcommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcctAccessCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AcctAccessCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctAccessCode", propOrder = {
    "primaryIdentity",
    "accountKey",
    "accountCode"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.PaymentRollBackRequest.PaymentObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.RechargeRollBackRequest.RechargeObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.RefundRequest.RefundObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.QueryTransferLogRequest.QueryObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.AdjustmentRequest.AdjustmentObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.QueryPaymentLogRequest.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.QueryInvoicePaymentRequest.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.QueryAdjustLogRequest.QueryObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.QueryRechargeLogRequest.QueryObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.Payment2ARRollBackRequest.PaymentObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.Payment2ARRequest.PaymentObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.arservices.TransferBalanceRequest.TransferorAcct.class,
    com.huawei.bme.cbsinterface.arservices.TransferBalanceRequest.TransfereeAcct.class,
    com.huawei.bme.cbsinterface.arservices.Refund2ARRequest.RefundObj.AcctAccessCode.class
})
public class AcctAccessCode {

    @XmlElement(name = "PrimaryIdentity")
    protected String primaryIdentity;
    @XmlElement(name = "AccountKey")
    protected String accountKey;
    @XmlElement(name = "AccountCode")
    protected String accountCode;

    /**
     * Gets the value of the primaryIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryIdentity() {
        return primaryIdentity;
    }

    /**
     * Sets the value of the primaryIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryIdentity(String value) {
        this.primaryIdentity = value;
    }

    /**
     * Gets the value of the accountKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountKey() {
        return accountKey;
    }

    /**
     * Sets the value of the accountKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountKey(String value) {
        this.accountKey = value;
    }

    /**
     * Gets the value of the accountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Sets the value of the accountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

}
