package com.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetHistoryBillFileList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetHistoryBillFileList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetHistoryBillFileList" type="{http://crm.huawei.com/query/}GetHistoryBillFileInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetHistoryBillFileList", propOrder = { "getHistoryBillFileList" })
public class GetHistoryBillFileList {
	@XmlElement(name = "GetHistoryBillFileList")
	protected List<GetHistoryBillFileInfo> getHistoryBillFileList;

	/**
	 * Gets the value of the getHistoryBillFileList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the getHistoryBillFileList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGetHistoryBillFileList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link GetHistoryBillFileInfo }
	 * 
	 * 
	 */
	public List<GetHistoryBillFileInfo> getGetHistoryBillFileList() {
		if (getHistoryBillFileList == null) {
			getHistoryBillFileList = new ArrayList<GetHistoryBillFileInfo>();
		}
		return this.getHistoryBillFileList;
	}
}
