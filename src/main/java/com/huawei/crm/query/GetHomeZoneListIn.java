package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetHomeZoneListIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetHomeZoneListIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetHomeZoneListIn", propOrder = { "serviceNumber", "subscriberId" })
public class GetHomeZoneListIn {
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "SubscriberId")
	protected Long subscriberId;

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the subscriberId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Sets the value of the subscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setSubscriberId(Long value) {
		this.subscriberId = value;
	}
}
