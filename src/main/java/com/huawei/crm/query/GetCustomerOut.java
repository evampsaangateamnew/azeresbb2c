package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.AddressList;
import com.huawei.crm.basetype.ContactList;
import com.huawei.crm.basetype.DNESettingList;
import com.huawei.crm.basetype.DPAInfo;
import com.huawei.crm.basetype.ExtParameterList;
import com.huawei.crm.basetype.ManagerList;

/**
 * <p>
 * Java class for GetCustomerOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomerOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="ParentCustId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerCode" type="{http://crm.huawei.com/basetype/}CustomerCode" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://crm.huawei.com/basetype/}Title" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://crm.huawei.com/basetype/}FirstName" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://crm.huawei.com/basetype/}MiddleName" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://crm.huawei.com/basetype/}LastName" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://crm.huawei.com/basetype/}Nationality" minOccurs="0"/>
 *         &lt;element name="CustomerType" type="{http://crm.huawei.com/basetype/}CustomerType" minOccurs="0"/>
 *         &lt;element name="CustomerLevel" type="{http://crm.huawei.com/basetype/}CustLevel" minOccurs="0"/>
 *         &lt;element name="CustomerLanguage" type="{http://crm.huawei.com/basetype/}CustomerLanguage" minOccurs="0"/>
 *         &lt;element name="Gender" type="{http://crm.huawei.com/basetype/}Gender" minOccurs="0"/>
 *         &lt;element name="CertificateType" type="{http://crm.huawei.com/basetype/}CertificateType" minOccurs="0"/>
 *         &lt;element name="CertificateNumber" type="{http://crm.huawei.com/basetype/}CertificateNumber" minOccurs="0"/>
 *         &lt;element name="CustomerSegment" type="{http://crm.huawei.com/basetype/}CustomerSegment" minOccurs="0"/>
 *         &lt;element name="TenantId" type="{http://crm.huawei.com/basetype/}TenantId" minOccurs="0"/>
 *         &lt;element name="AddressList" type="{http://crm.huawei.com/basetype/}AddressList" minOccurs="0"/>
 *         &lt;element name="ContactList" type="{http://crm.huawei.com/basetype/}ContactList" minOccurs="0"/>
 *         &lt;element name="ManagerList" type="{http://crm.huawei.com/basetype/}ManagerList" minOccurs="0"/>
 *         &lt;element name="DateOfBirth" type="{http://crm.huawei.com/basetype/}DateOfBirth" minOccurs="0"/>
 *         &lt;element name="PlaceOfBirth" type="{http://crm.huawei.com/basetype/}PlaceOfBirth" minOccurs="0"/>
 *         &lt;element name="Occupation" type="{http://crm.huawei.com/basetype/}Occupation" minOccurs="0"/>
 *         &lt;element name="Religion" type="{http://crm.huawei.com/basetype/}Religion" minOccurs="0"/>
 *         &lt;element name="Income" type="{http://crm.huawei.com/basetype/}Income" minOccurs="0"/>
 *         &lt;element name="Education" type="{http://crm.huawei.com/basetype/}Education" minOccurs="0"/>
 *         &lt;element name="DealerId" type="{http://crm.huawei.com/basetype/}DealerId" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}CustStatus" minOccurs="0"/>
 *         &lt;element name="DNESettingsList" type="{http://crm.huawei.com/basetype/}DNESettingList" minOccurs="0"/>
 *         &lt;element name="DPAInfo" type="{http://crm.huawei.com/basetype/}DPAInfo" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomerOut", propOrder = {})
public class GetCustomerOut {
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "ParentCustId")
	protected Long parentCustId;
	@XmlElement(name = "CustomerCode")
	protected String customerCode;
	@XmlElement(name = "Title")
	protected String title;
	@XmlElement(name = "FirstName")
	protected String firstName;
	@XmlElement(name = "MiddleName")
	protected String middleName;
	@XmlElement(name = "LastName")
	protected String lastName;
	@XmlElement(name = "Nationality")
	protected String nationality;
	@XmlElement(name = "CustomerType")
	protected String customerType;
	@XmlElement(name = "CustomerLevel")
	protected String customerLevel;
	@XmlElement(name = "CustomerLanguage")
	protected String customerLanguage;
	@XmlElement(name = "Gender")
	protected String gender;
	@XmlElement(name = "CertificateType")
	protected String certificateType;
	@XmlElement(name = "CertificateNumber")
	protected String certificateNumber;
	@XmlElement(name = "CustomerSegment")
	protected String customerSegment;
	@XmlElement(name = "TenantId")
	protected String tenantId;
	@XmlElement(name = "AddressList")
	protected AddressList addressList;
	@XmlElement(name = "ContactList")
	protected ContactList contactList;
	@XmlElement(name = "ManagerList")
	protected ManagerList managerList;
	@XmlElement(name = "DateOfBirth")
	protected String dateOfBirth;
	@XmlElement(name = "PlaceOfBirth")
	protected String placeOfBirth;
	@XmlElement(name = "Occupation")
	protected String occupation;
	@XmlElement(name = "Religion")
	protected String religion;
	@XmlElement(name = "Income")
	protected String income;
	@XmlElement(name = "Education")
	protected String education;
	@XmlElement(name = "DealerId")
	protected String dealerId;
	@XmlElement(name = "Status")
	protected String status;
	@XmlElement(name = "DNESettingsList")
	protected DNESettingList dneSettingsList;
	@XmlElement(name = "DPAInfo")
	protected DPAInfo dpaInfo;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the parentCustId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getParentCustId() {
		return parentCustId;
	}

	/**
	 * Sets the value of the parentCustId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setParentCustId(Long value) {
		this.parentCustId = value;
	}

	/**
	 * Gets the value of the customerCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * Sets the value of the customerCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerCode(String value) {
		this.customerCode = value;
	}

	/**
	 * Gets the value of the title property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTitle(String value) {
		this.title = value;
	}

	/**
	 * Gets the value of the firstName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the value of the firstName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFirstName(String value) {
		this.firstName = value;
	}

	/**
	 * Gets the value of the middleName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * Sets the value of the middleName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMiddleName(String value) {
		this.middleName = value;
	}

	/**
	 * Gets the value of the lastName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the value of the lastName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLastName(String value) {
		this.lastName = value;
	}

	/**
	 * Gets the value of the nationality property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * Sets the value of the nationality property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNationality(String value) {
		this.nationality = value;
	}

	/**
	 * Gets the value of the customerType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * Sets the value of the customerType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerType(String value) {
		this.customerType = value;
	}

	/**
	 * Gets the value of the customerLevel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerLevel() {
		return customerLevel;
	}

	/**
	 * Sets the value of the customerLevel property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerLevel(String value) {
		this.customerLevel = value;
	}

	/**
	 * Gets the value of the customerLanguage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerLanguage() {
		return customerLanguage;
	}

	/**
	 * Sets the value of the customerLanguage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerLanguage(String value) {
		this.customerLanguage = value;
	}

	/**
	 * Gets the value of the gender property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Sets the value of the gender property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGender(String value) {
		this.gender = value;
	}

	/**
	 * Gets the value of the certificateType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCertificateType() {
		return certificateType;
	}

	/**
	 * Sets the value of the certificateType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCertificateType(String value) {
		this.certificateType = value;
	}

	/**
	 * Gets the value of the certificateNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * Sets the value of the certificateNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCertificateNumber(String value) {
		this.certificateNumber = value;
	}

	/**
	 * Gets the value of the customerSegment property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerSegment() {
		return customerSegment;
	}

	/**
	 * Sets the value of the customerSegment property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerSegment(String value) {
		this.customerSegment = value;
	}

	/**
	 * Gets the value of the tenantId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * Sets the value of the tenantId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTenantId(String value) {
		this.tenantId = value;
	}

	/**
	 * Gets the value of the addressList property.
	 * 
	 * @return possible object is {@link AddressList }
	 * 
	 */
	public AddressList getAddressList() {
		return addressList;
	}

	/**
	 * Sets the value of the addressList property.
	 * 
	 * @param value
	 *            allowed object is {@link AddressList }
	 * 
	 */
	public void setAddressList(AddressList value) {
		this.addressList = value;
	}

	/**
	 * Gets the value of the contactList property.
	 * 
	 * @return possible object is {@link ContactList }
	 * 
	 */
	public ContactList getContactList() {
		return contactList;
	}

	/**
	 * Sets the value of the contactList property.
	 * 
	 * @param value
	 *            allowed object is {@link ContactList }
	 * 
	 */
	public void setContactList(ContactList value) {
		this.contactList = value;
	}

	/**
	 * Gets the value of the managerList property.
	 * 
	 * @return possible object is {@link ManagerList }
	 * 
	 */
	public ManagerList getManagerList() {
		return managerList;
	}

	/**
	 * Sets the value of the managerList property.
	 * 
	 * @param value
	 *            allowed object is {@link ManagerList }
	 * 
	 */
	public void setManagerList(ManagerList value) {
		this.managerList = value;
	}

	/**
	 * Gets the value of the dateOfBirth property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Sets the value of the dateOfBirth property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDateOfBirth(String value) {
		this.dateOfBirth = value;
	}

	/**
	 * Gets the value of the placeOfBirth property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	/**
	 * Sets the value of the placeOfBirth property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPlaceOfBirth(String value) {
		this.placeOfBirth = value;
	}

	/**
	 * Gets the value of the occupation property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOccupation() {
		return occupation;
	}

	/**
	 * Sets the value of the occupation property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOccupation(String value) {
		this.occupation = value;
	}

	/**
	 * Gets the value of the religion property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReligion() {
		return religion;
	}

	/**
	 * Sets the value of the religion property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReligion(String value) {
		this.religion = value;
	}

	/**
	 * Gets the value of the income property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIncome() {
		return income;
	}

	/**
	 * Sets the value of the income property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIncome(String value) {
		this.income = value;
	}

	/**
	 * Gets the value of the education property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEducation() {
		return education;
	}

	/**
	 * Sets the value of the education property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEducation(String value) {
		this.education = value;
	}

	/**
	 * Gets the value of the dealerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDealerId() {
		return dealerId;
	}

	/**
	 * Sets the value of the dealerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDealerId(String value) {
		this.dealerId = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the dneSettingsList property.
	 * 
	 * @return possible object is {@link DNESettingList }
	 * 
	 */
	public DNESettingList getDNESettingsList() {
		return dneSettingsList;
	}

	/**
	 * Sets the value of the dneSettingsList property.
	 * 
	 * @param value
	 *            allowed object is {@link DNESettingList }
	 * 
	 */
	public void setDNESettingsList(DNESettingList value) {
		this.dneSettingsList = value;
	}

	/**
	 * Gets the value of the dpaInfo property.
	 * 
	 * @return possible object is {@link DPAInfo }
	 * 
	 */
	public DPAInfo getDPAInfo() {
		return dpaInfo;
	}

	/**
	 * Sets the value of the dpaInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link DPAInfo }
	 * 
	 */
	public void setDPAInfo(DPAInfo value) {
		this.dpaInfo = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
