package com.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.CalcOneOffFeeInfoForQuery;

/**
 * <p>
 * Java class for CalcOneOffFeeList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CalcOneOffFeeList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CalcOneOffFeeList" type="{http://crm.huawei.com/basetype/}CalcOneOffFeeInfoForQuery" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalcOneOffFeeList", propOrder = { "calcOneOffFeeList" })
public class CalcOneOffFeeList {
	@XmlElement(name = "CalcOneOffFeeList")
	protected List<CalcOneOffFeeInfoForQuery> calcOneOffFeeList;

	/**
	 * Gets the value of the calcOneOffFeeList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the calcOneOffFeeList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCalcOneOffFeeList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CalcOneOffFeeInfoForQuery }
	 * 
	 * 
	 */
	public List<CalcOneOffFeeInfoForQuery> getCalcOneOffFeeList() {
		if (calcOneOffFeeList == null) {
			calcOneOffFeeList = new ArrayList<CalcOneOffFeeInfoForQuery>();
		}
		return this.calcOneOffFeeList;
	}
}
