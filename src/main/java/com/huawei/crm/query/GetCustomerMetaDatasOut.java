package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetCustomerMetaDatasOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomerMetaDatasOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}CustStatus"/>
 *         &lt;element name="BEID" type="{http://crm.huawei.com/basetype/}BeID" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomerMetaDatasOut", propOrder = {})
public class GetCustomerMetaDatasOut {
	@XmlElement(name = "CustomerId")
	protected long customerId;
	@XmlElement(name = "Status", required = true)
	protected String status;
	@XmlElement(name = "BEID")
	protected String beid;

	/**
	 * Gets the value of the customerId property.
	 * 
	 */
	public long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 */
	public void setCustomerId(long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the beid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBEID() {
		return beid;
	}

	/**
	 * Sets the value of the beid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBEID(String value) {
		this.beid = value;
	}
}
