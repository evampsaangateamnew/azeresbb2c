package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.huawei.crm.query package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: com.huawei.crm.query
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link GetNetworkSettingDataResponse }
	 * 
	 */
	public GetNetworkSettingDataResponse createGetNetworkSettingDataResponse() {
		return new GetNetworkSettingDataResponse();
	}

	/**
	 * Create an instance of {@link GetTimeSchemaOut }
	 * 
	 */
	public GetTimeSchemaOut createGetTimeSchemaOut() {
		return new GetTimeSchemaOut();
	}

	/**
	 * Create an instance of {@link GetSubscriberHistoryIn }
	 * 
	 */
	public GetSubscriberHistoryIn createGetSubscriberHistoryIn() {
		return new GetSubscriberHistoryIn();
	}

	/**
	 * Create an instance of {@link GetAccountMetaDataResponse }
	 * 
	 */
	public GetAccountMetaDataResponse createGetAccountMetaDataResponse() {
		return new GetAccountMetaDataResponse();
	}

	/**
	 * Create an instance of {@link GetSubscriberMetaDataOut }
	 * 
	 */
	public GetSubscriberMetaDataOut createGetSubscriberMetaDataOut() {
		return new GetSubscriberMetaDataOut();
	}

	/**
	 * Create an instance of {@link FnFGroupList }
	 * 
	 */
	public FnFGroupList createFnFGroupList() {
		return new FnFGroupList();
	}

	/**
	 * Create an instance of {@link GetCustomerMetaDataRequest }
	 * 
	 */
	public GetCustomerMetaDataRequest createGetCustomerMetaDataRequest() {
		return new GetCustomerMetaDataRequest();
	}

	/**
	 * Create an instance of {@link SubTeleResourceInfo }
	 * 
	 */
	public SubTeleResourceInfo createSubTeleResourceInfo() {
		return new SubTeleResourceInfo();
	}

	/**
	 * Create an instance of {@link GetCustSOfferingIn }
	 * 
	 */
	public GetCustSOfferingIn createGetCustSOfferingIn() {
		return new GetCustSOfferingIn();
	}

	/**
	 * Create an instance of {@link GetSubscriberMetaDatasOut }
	 * 
	 */
	public GetSubscriberMetaDatasOut createGetSubscriberMetaDatasOut() {
		return new GetSubscriberMetaDatasOut();
	}

	/**
	 * Create an instance of {@link ContactRequestInfo }
	 * 
	 */
	public ContactRequestInfo createContactRequestInfo() {
		return new ContactRequestInfo();
	}

	/**
	 * Create an instance of {@link GetOfferingSpecResponse }
	 * 
	 */
	public GetOfferingSpecResponse createGetOfferingSpecResponse() {
		return new GetOfferingSpecResponse();
	}

	/**
	 * Create an instance of {@link GetAccountMetaDataIn }
	 * 
	 */
	public GetAccountMetaDataIn createGetAccountMetaDataIn() {
		return new GetAccountMetaDataIn();
	}

	/**
	 * Create an instance of {@link GetSubscriberMetaDatasIn }
	 * 
	 */
	public GetSubscriberMetaDatasIn createGetSubscriberMetaDatasIn() {
		return new GetSubscriberMetaDatasIn();
	}

	/**
	 * Create an instance of {@link GetOfferingSpecIn }
	 * 
	 */
	public GetOfferingSpecIn createGetOfferingSpecIn() {
		return new GetOfferingSpecIn();
	}

	/**
	 * Create an instance of {@link MemberSubscriberList }
	 * 
	 */
	public MemberSubscriberList createMemberSubscriberList() {
		return new MemberSubscriberList();
	}

	/**
	 * Create an instance of {@link CalcOneOffFeeIn }
	 * 
	 */
	public CalcOneOffFeeIn createCalcOneOffFeeIn() {
		return new CalcOneOffFeeIn();
	}

	/**
	 * Create an instance of {@link GetCustomerResponse }
	 * 
	 */
	public GetCustomerResponse createGetCustomerResponse() {
		return new GetCustomerResponse();
	}

	/**
	 * Create an instance of {@link GetGroupOfMemberDataInfo }
	 * 
	 */
	public GetGroupOfMemberDataInfo createGetGroupOfMemberDataInfo() {
		return new GetGroupOfMemberDataInfo();
	}

	/**
	 * Create an instance of {@link GetAccountOut }
	 * 
	 */
	public GetAccountOut createGetAccountOut() {
		return new GetAccountOut();
	}

	/**
	 * Create an instance of {@link GetAccountMetaDataRequest }
	 * 
	 */
	public GetAccountMetaDataRequest createGetAccountMetaDataRequest() {
		return new GetAccountMetaDataRequest();
	}

	/**
	 * Create an instance of {@link GetCustPOfferingRequest }
	 * 
	 */
	public GetCustPOfferingRequest createGetCustPOfferingRequest() {
		return new GetCustPOfferingRequest();
	}

	/**
	 * Create an instance of {@link GetAccountHistoryRequest }
	 * 
	 */
	public GetAccountHistoryRequest createGetAccountHistoryRequest() {
		return new GetAccountHistoryRequest();
	}

	/**
	 * Create an instance of {@link GetHomeZoneListResponse }
	 * 
	 */
	public GetHomeZoneListResponse createGetHomeZoneListResponse() {
		return new GetHomeZoneListResponse();
	}

	/**
	 * Create an instance of {@link GetFnFDataIn }
	 * 
	 */
	public GetFnFDataIn createGetFnFDataIn() {
		return new GetFnFDataIn();
	}

	/**
	 * Create an instance of {@link QueryContactLogOut }
	 * 
	 */
	public QueryContactLogOut createQueryContactLogOut() {
		return new QueryContactLogOut();
	}

	/**
	 * Create an instance of {@link ProductInstInfo }
	 * 
	 */
	public ProductInstInfo createProductInstInfo() {
		return new ProductInstInfo();
	}

	/**
	 * Create an instance of {@link GetCustomerHistoryIn }
	 * 
	 */
	public GetCustomerHistoryIn createGetCustomerHistoryIn() {
		return new GetCustomerHistoryIn();
	}

	/**
	 * Create an instance of {@link GetAccountListIn }
	 * 
	 */
	public GetAccountListIn createGetAccountListIn() {
		return new GetAccountListIn();
	}

	/**
	 * Create an instance of {@link SubTeleResourceList }
	 * 
	 */
	public SubTeleResourceList createSubTeleResourceList() {
		return new SubTeleResourceList();
	}

	/**
	 * Create an instance of {@link ValidateOfferingRelationIn }
	 * 
	 */
	public ValidateOfferingRelationIn createValidateOfferingRelationIn() {
		return new ValidateOfferingRelationIn();
	}

	/**
	 * Create an instance of {@link CallScreenNumberInfo }
	 * 
	 */
	public CallScreenNumberInfo createCallScreenNumberInfo() {
		return new CallScreenNumberInfo();
	}

	/**
	 * Create an instance of {@link GetGroupDataIn }
	 * 
	 */
	public GetGroupDataIn createGetGroupDataIn() {
		return new GetGroupDataIn();
	}

	/**
	 * Create an instance of {@link GetSubResourceDataResponse }
	 * 
	 */
	public GetSubResourceDataResponse createGetSubResourceDataResponse() {
		return new GetSubResourceDataResponse();
	}

	/**
	 * Create an instance of {@link GetOfferSalesFeeRequest }
	 * 
	 */
	public GetOfferSalesFeeRequest createGetOfferSalesFeeRequest() {
		return new GetOfferSalesFeeRequest();
	}

	/**
	 * Create an instance of {@link AttributeInfo }
	 * 
	 */
	public AttributeInfo createAttributeInfo() {
		return new AttributeInfo();
	}

	/**
	 * Create an instance of {@link SkuInfo }
	 * 
	 */
	public SkuInfo createSkuInfo() {
		return new SkuInfo();
	}

	/**
	 * Create an instance of {@link OfferingInfo }
	 * 
	 */
	public OfferingInfo createOfferingInfo() {
		return new OfferingInfo();
	}

	/**
	 * Create an instance of {@link CheckPasswordResponse }
	 * 
	 */
	public CheckPasswordResponse createCheckPasswordResponse() {
		return new CheckPasswordResponse();
	}

	/**
	 * Create an instance of
	 * {@link GetSpecialOfferingResponse.GeSpecialOfferingBody }
	 * 
	 */
	public GetSpecialOfferingResponse.GeSpecialOfferingBody createGetSpecialOfferingResponseGeSpecialOfferingBody() {
		return new GetSpecialOfferingResponse.GeSpecialOfferingBody();
	}

	/**
	 * Create an instance of {@link GetFnFDataResponse }
	 * 
	 */
	public GetFnFDataResponse createGetFnFDataResponse() {
		return new GetFnFDataResponse();
	}

	/**
	 * Create an instance of {@link GetCustomerMetaDatasOut }
	 * 
	 */
	public GetCustomerMetaDatasOut createGetCustomerMetaDatasOut() {
		return new GetCustomerMetaDatasOut();
	}

	/**
	 * Create an instance of {@link GetTimeSchemaResponse }
	 * 
	 */
	public GetTimeSchemaResponse createGetTimeSchemaResponse() {
		return new GetTimeSchemaResponse();
	}

	/**
	 * Create an instance of {@link GetCustomerMetaDataResponse }
	 * 
	 */
	public GetCustomerMetaDataResponse createGetCustomerMetaDataResponse() {
		return new GetCustomerMetaDataResponse();
	}

	/**
	 * Create an instance of {@link GetOfferSalesFeeIn }
	 * 
	 */
	public GetOfferSalesFeeIn createGetOfferSalesFeeIn() {
		return new GetOfferSalesFeeIn();
	}

	/**
	 * Create an instance of {@link GetCallScreenNumListIn }
	 * 
	 */
	public GetCallScreenNumListIn createGetCallScreenNumListIn() {
		return new GetCallScreenNumListIn();
	}

	/**
	 * Create an instance of {@link GetHistoryBillFileList }
	 * 
	 */
	public GetHistoryBillFileList createGetHistoryBillFileList() {
		return new GetHistoryBillFileList();
	}

	/**
	 * Create an instance of {@link GetHistoryBillFileIn }
	 * 
	 */
	public GetHistoryBillFileIn createGetHistoryBillFileIn() {
		return new GetHistoryBillFileIn();
	}

	/**
	 * Create an instance of {@link FactorInfo }
	 * 
	 */
	public FactorInfo createFactorInfo() {
		return new FactorInfo();
	}

	/**
	 * Create an instance of {@link GetSOfferingList }
	 * 
	 */
	public GetSOfferingList createGetSOfferingList() {
		return new GetSOfferingList();
	}

	/**
	 * Create an instance of {@link GetSubscriberRequest }
	 * 
	 */
	public GetSubscriberRequest createGetSubscriberRequest() {
		return new GetSubscriberRequest();
	}

	/**
	 * Create an instance of {@link GetSpecialOfferingIn }
	 * 
	 */
	public GetSpecialOfferingIn createGetSpecialOfferingIn() {
		return new GetSpecialOfferingIn();
	}

	/**
	 * Create an instance of
	 * {@link GetOfferSalesFeeResponse.GetOfferSalesFeeBody }
	 * 
	 */
	public GetOfferSalesFeeResponse.GetOfferSalesFeeBody createGetOfferSalesFeeResponseGetOfferSalesFeeBody() {
		return new GetOfferSalesFeeResponse.GetOfferSalesFeeBody();
	}

	/**
	 * Create an instance of {@link GetCustomerHistoryResponse }
	 * 
	 */
	public GetCustomerHistoryResponse createGetCustomerHistoryResponse() {
		return new GetCustomerHistoryResponse();
	}

	/**
	 * Create an instance of {@link GetCustomerHistoryRequest }
	 * 
	 */
	public GetCustomerHistoryRequest createGetCustomerHistoryRequest() {
		return new GetCustomerHistoryRequest();
	}

	/**
	 * Create an instance of {@link GetSubscriberHistoryRequest }
	 * 
	 */
	public GetSubscriberHistoryRequest createGetSubscriberHistoryRequest() {
		return new GetSubscriberHistoryRequest();
	}

	/**
	 * Create an instance of {@link GetCustSOfferingRequest }
	 * 
	 */
	public GetCustSOfferingRequest createGetCustSOfferingRequest() {
		return new GetCustSOfferingRequest();
	}

	/**
	 * Create an instance of {@link GetAccountHistoryResponse }
	 * 
	 */
	public GetAccountHistoryResponse createGetAccountHistoryResponse() {
		return new GetAccountHistoryResponse();
	}

	/**
	 * Create an instance of {@link GetTimeSchemaRequest }
	 * 
	 */
	public GetTimeSchemaRequest createGetTimeSchemaRequest() {
		return new GetTimeSchemaRequest();
	}

	/**
	 * Create an instance of {@link RetrivedHomeZoneInfo }
	 * 
	 */
	public RetrivedHomeZoneInfo createRetrivedHomeZoneInfo() {
		return new RetrivedHomeZoneInfo();
	}

	/**
	 * Create an instance of {@link FnFGroupInfo }
	 * 
	 */
	public FnFGroupInfo createFnFGroupInfo() {
		return new FnFGroupInfo();
	}

	/**
	 * Create an instance of {@link GetCustomerMetaDataIn }
	 * 
	 */
	public GetCustomerMetaDataIn createGetCustomerMetaDataIn() {
		return new GetCustomerMetaDataIn();
	}

	/**
	 * Create an instance of {@link GetHomeZoneListIn }
	 * 
	 */
	public GetHomeZoneListIn createGetHomeZoneListIn() {
		return new GetHomeZoneListIn();
	}

	/**
	 * Create an instance of {@link GetSubscriberHistoryResponse }
	 * 
	 */
	public GetSubscriberHistoryResponse createGetSubscriberHistoryResponse() {
		return new GetSubscriberHistoryResponse();
	}

	/**
	 * Create an instance of {@link QueryCustomerIn }
	 * 
	 */
	public QueryCustomerIn createQueryCustomerIn() {
		return new QueryCustomerIn();
	}

	/**
	 * Create an instance of {@link AccountFeeFactorInfo }
	 * 
	 */
	public AccountFeeFactorInfo createAccountFeeFactorInfo() {
		return new AccountFeeFactorInfo();
	}

	/**
	 * Create an instance of {@link GetAccountMetaDataOut }
	 * 
	 */
	public GetAccountMetaDataOut createGetAccountMetaDataOut() {
		return new GetAccountMetaDataOut();
	}

	/**
	 * Create an instance of {@link GetAccountListResponse }
	 * 
	 */
	public GetAccountListResponse createGetAccountListResponse() {
		return new GetAccountListResponse();
	}

	/**
	 * Create an instance of {@link Attrs }
	 * 
	 */
	public Attrs createAttrs() {
		return new Attrs();
	}

	/**
	 * Create an instance of {@link GetAccountListInfo }
	 * 
	 */
	public GetAccountListInfo createGetAccountListInfo() {
		return new GetAccountListInfo();
	}

	/**
	 * Create an instance of {@link HomeZoneInfo }
	 * 
	 */
	public HomeZoneInfo createHomeZoneInfo() {
		return new HomeZoneInfo();
	}

	/**
	 * Create an instance of {@link GetGroupRequest }
	 * 
	 */
	public GetGroupRequest createGetGroupRequest() {
		return new GetGroupRequest();
	}

	/**
	 * Create an instance of {@link GetSubResourceDataRequest }
	 * 
	 */
	public GetSubResourceDataRequest createGetSubResourceDataRequest() {
		return new GetSubResourceDataRequest();
	}

	/**
	 * Create an instance of {@link GetCustPOfferingResponse }
	 * 
	 */
	public GetCustPOfferingResponse createGetCustPOfferingResponse() {
		return new GetCustPOfferingResponse();
	}

	/**
	 * Create an instance of {@link GetBillMediumInfoResponse }
	 * 
	 */
	public GetBillMediumInfoResponse createGetBillMediumInfoResponse() {
		return new GetBillMediumInfoResponse();
	}

	/**
	 * Create an instance of {@link GetDictTableResponse }
	 * 
	 */
	public GetDictTableResponse createGetDictTableResponse() {
		return new GetDictTableResponse();
	}

	/**
	 * Create an instance of {@link GetSubscriberMetaDataResponse }
	 * 
	 */
	public GetSubscriberMetaDataResponse createGetSubscriberMetaDataResponse() {
		return new GetSubscriberMetaDataResponse();
	}

	/**
	 * Create an instance of {@link GetGroupOut }
	 * 
	 */
	public GetGroupOut createGetGroupOut() {
		return new GetGroupOut();
	}

	/**
	 * Create an instance of {@link GetHistoryBillFileResponse }
	 * 
	 */
	public GetHistoryBillFileResponse createGetHistoryBillFileResponse() {
		return new GetHistoryBillFileResponse();
	}

	/**
	 * Create an instance of {@link ElementInfo }
	 * 
	 */
	public ElementInfo createElementInfo() {
		return new ElementInfo();
	}

	/**
	 * Create an instance of {@link CalcOneOffFeeList }
	 * 
	 */
	public CalcOneOffFeeList createCalcOneOffFeeList() {
		return new CalcOneOffFeeList();
	}

	/**
	 * Create an instance of {@link GetDictTableRequestBody }
	 * 
	 */
	public GetDictTableRequestBody createGetDictTableRequestBody() {
		return new GetDictTableRequestBody();
	}

	/**
	 * Create an instance of {@link GetSaleOfferingsRequest }
	 * 
	 */
	public GetSaleOfferingsRequest createGetSaleOfferingsRequest() {
		return new GetSaleOfferingsRequest();
	}

	/**
	 * Create an instance of {@link GetDictTableResult }
	 * 
	 */
	public GetDictTableResult createGetDictTableResult() {
		return new GetDictTableResult();
	}

	/**
	 * Create an instance of {@link QueryContactLogRequest }
	 * 
	 */
	public QueryContactLogRequest createQueryContactLogRequest() {
		return new QueryContactLogRequest();
	}

	/**
	 * Create an instance of {@link Elements }
	 * 
	 */
	public Elements createElements() {
		return new Elements();
	}

	/**
	 * Create an instance of {@link GetCustomerRequest }
	 * 
	 */
	public GetCustomerRequest createGetCustomerRequest() {
		return new GetCustomerRequest();
	}

	/**
	 * Create an instance of {@link GetCustomerMetaDatasRequest }
	 * 
	 */
	public GetCustomerMetaDatasRequest createGetCustomerMetaDatasRequest() {
		return new GetCustomerMetaDatasRequest();
	}

	/**
	 * Create an instance of {@link GetDictTableResultList }
	 * 
	 */
	public GetDictTableResultList createGetDictTableResultList() {
		return new GetDictTableResultList();
	}

	/**
	 * Create an instance of {@link QueryCustomerResponse }
	 * 
	 */
	public QueryCustomerResponse createQueryCustomerResponse() {
		return new QueryCustomerResponse();
	}

	/**
	 * Create an instance of {@link GetCallScreenNumListResponse }
	 * 
	 */
	public GetCallScreenNumListResponse createGetCallScreenNumListResponse() {
		return new GetCallScreenNumListResponse();
	}

	/**
	 * Create an instance of {@link GetGroupMemberDataRequest }
	 * 
	 */
	public GetGroupMemberDataRequest createGetGroupMemberDataRequest() {
		return new GetGroupMemberDataRequest();
	}

	/**
	 * Create an instance of {@link GetSpecialOfferingList }
	 * 
	 */
	public GetSpecialOfferingList createGetSpecialOfferingList() {
		return new GetSpecialOfferingList();
	}

	/**
	 * Create an instance of {@link GetAccountHistoryOut }
	 * 
	 */
	public GetAccountHistoryOut createGetAccountHistoryOut() {
		return new GetAccountHistoryOut();
	}

	/**
	 * Create an instance of {@link GetAccountHistoryIn }
	 * 
	 */
	public GetAccountHistoryIn createGetAccountHistoryIn() {
		return new GetAccountHistoryIn();
	}

	/**
	 * Create an instance of {@link GetSubscriberOut }
	 * 
	 */
	public GetSubscriberOut createGetSubscriberOut() {
		return new GetSubscriberOut();
	}

	/**
	 * Create an instance of {@link GetPOfferingList }
	 * 
	 */
	public GetPOfferingList createGetPOfferingList() {
		return new GetPOfferingList();
	}

	/**
	 * Create an instance of {@link QueryCustomerOut }
	 * 
	 */
	public QueryCustomerOut createQueryCustomerOut() {
		return new QueryCustomerOut();
	}

	/**
	 * Create an instance of {@link CallScreenNumList }
	 * 
	 */
	public CallScreenNumList createCallScreenNumList() {
		return new CallScreenNumList();
	}

	/**
	 * Create an instance of {@link GetSubscriberMetaDatasRequest }
	 * 
	 */
	public GetSubscriberMetaDatasRequest createGetSubscriberMetaDatasRequest() {
		return new GetSubscriberMetaDatasRequest();
	}

	/**
	 * Create an instance of {@link ProductInstList }
	 * 
	 */
	public ProductInstList createProductInstList() {
		return new ProductInstList();
	}

	/**
	 * Create an instance of {@link GetBillMediumInfoRequest }
	 * 
	 */
	public GetBillMediumInfoRequest createGetBillMediumInfoRequest() {
		return new GetBillMediumInfoRequest();
	}

	/**
	 * Create an instance of {@link GetCustomerMetaDatasResponse }
	 * 
	 */
	public GetCustomerMetaDatasResponse createGetCustomerMetaDatasResponse() {
		return new GetCustomerMetaDatasResponse();
	}

	/**
	 * Create an instance of {@link GetCustomerHistoryOut }
	 * 
	 */
	public GetCustomerHistoryOut createGetCustomerHistoryOut() {
		return new GetCustomerHistoryOut();
	}

	/**
	 * Create an instance of {@link QueryContactLogResponse }
	 * 
	 */
	public QueryContactLogResponse createQueryContactLogResponse() {
		return new QueryContactLogResponse();
	}

	/**
	 * Create an instance of {@link SubscriberFeeFactorInfo }
	 * 
	 */
	public SubscriberFeeFactorInfo createSubscriberFeeFactorInfo() {
		return new SubscriberFeeFactorInfo();
	}

	/**
	 * Create an instance of {@link GetCustomerIn }
	 * 
	 */
	public GetCustomerIn createGetCustomerIn() {
		return new GetCustomerIn();
	}

	/**
	 * Create an instance of {@link GetAccountListRequest }
	 * 
	 */
	public GetAccountListRequest createGetAccountListRequest() {
		return new GetAccountListRequest();
	}

	/**
	 * Create an instance of {@link GetSpecialOfferingInfo }
	 * 
	 */
	public GetSpecialOfferingInfo createGetSpecialOfferingInfo() {
		return new GetSpecialOfferingInfo();
	}

	/**
	 * Create an instance of {@link ValidateOfferingRelationResponse }
	 * 
	 */
	public ValidateOfferingRelationResponse createValidateOfferingRelationResponse() {
		return new ValidateOfferingRelationResponse();
	}

	/**
	 * Create an instance of {@link GetBillMediumInfoOut }
	 * 
	 */
	public GetBillMediumInfoOut createGetBillMediumInfoOut() {
		return new GetBillMediumInfoOut();
	}

	/**
	 * Create an instance of {@link GetNetworkSettingDataIn }
	 * 
	 */
	public GetNetworkSettingDataIn createGetNetworkSettingDataIn() {
		return new GetNetworkSettingDataIn();
	}

	/**
	 * Create an instance of {@link CustomerAbstractInfo }
	 * 
	 */
	public CustomerAbstractInfo createCustomerAbstractInfo() {
		return new CustomerAbstractInfo();
	}

	/**
	 * Create an instance of {@link GetFnFDataRequest }
	 * 
	 */
	public GetFnFDataRequest createGetFnFDataRequest() {
		return new GetFnFDataRequest();
	}

	/**
	 * Create an instance of {@link CheckPasswordIn }
	 * 
	 */
	public CheckPasswordIn createCheckPasswordIn() {
		return new CheckPasswordIn();
	}

	/**
	 * Create an instance of {@link GetSubscriberMetaDatasResponse }
	 * 
	 */
	public GetSubscriberMetaDatasResponse createGetSubscriberMetaDatasResponse() {
		return new GetSubscriberMetaDatasResponse();
	}

	/**
	 * Create an instance of {@link GetCustomerMetaDataOut }
	 * 
	 */
	public GetCustomerMetaDataOut createGetCustomerMetaDataOut() {
		return new GetCustomerMetaDataOut();
	}

	/**
	 * Create an instance of {@link GetGroupDataInfo }
	 * 
	 */
	public GetGroupDataInfo createGetGroupDataInfo() {
		return new GetGroupDataInfo();
	}

	/**
	 * Create an instance of {@link ContactReasonCodeList }
	 * 
	 */
	public ContactReasonCodeList createContactReasonCodeList() {
		return new ContactReasonCodeList();
	}

	/**
	 * Create an instance of {@link GetGroupMemberDataIn }
	 * 
	 */
	public GetGroupMemberDataIn createGetGroupMemberDataIn() {
		return new GetGroupMemberDataIn();
	}

	/**
	 * Create an instance of {@link CalcOneOffFeeRequest }
	 * 
	 */
	public CalcOneOffFeeRequest createCalcOneOffFeeRequest() {
		return new CalcOneOffFeeRequest();
	}

	/**
	 * Create an instance of {@link GetSubResourceDataIn }
	 * 
	 */
	public GetSubResourceDataIn createGetSubResourceDataIn() {
		return new GetSubResourceDataIn();
	}

	/**
	 * Create an instance of {@link GetGroupMemberDataOut }
	 * 
	 */
	public GetGroupMemberDataOut createGetGroupMemberDataOut() {
		return new GetGroupMemberDataOut();
	}

	/**
	 * Create an instance of {@link CalcOneOffFeeResponse }
	 * 
	 */
	public CalcOneOffFeeResponse createCalcOneOffFeeResponse() {
		return new CalcOneOffFeeResponse();
	}

	/**
	 * Create an instance of {@link GetNetworkSettingDataRequest }
	 * 
	 */
	public GetNetworkSettingDataRequest createGetNetworkSettingDataRequest() {
		return new GetNetworkSettingDataRequest();
	}

	/**
	 * Create an instance of {@link GetAccountListOut }
	 * 
	 */
	public GetAccountListOut createGetAccountListOut() {
		return new GetAccountListOut();
	}

	/**
	 * Create an instance of {@link SubGoodsResourceList }
	 * 
	 */
	public SubGoodsResourceList createSubGoodsResourceList() {
		return new SubGoodsResourceList();
	}

	/**
	 * Create an instance of {@link GetSubscriberIn }
	 * 
	 */
	public GetSubscriberIn createGetSubscriberIn() {
		return new GetSubscriberIn();
	}

	/**
	 * Create an instance of {@link GetSpecialOfferingRequest }
	 * 
	 */
	public GetSpecialOfferingRequest createGetSpecialOfferingRequest() {
		return new GetSpecialOfferingRequest();
	}

	/**
	 * Create an instance of {@link GetCorpCustomerDataIn }
	 * 
	 */
	public GetCorpCustomerDataIn createGetCorpCustomerDataIn() {
		return new GetCorpCustomerDataIn();
	}

	/**
	 * Create an instance of {@link FnFList }
	 * 
	 */
	public FnFList createFnFList() {
		return new FnFList();
	}

	/**
	 * Create an instance of {@link TimeSchema }
	 * 
	 */
	public TimeSchema createTimeSchema() {
		return new TimeSchema();
	}

	/**
	 * Create an instance of {@link GetHomeZoneListRequest }
	 * 
	 */
	public GetHomeZoneListRequest createGetHomeZoneListRequest() {
		return new GetHomeZoneListRequest();
	}

	/**
	 * Create an instance of {@link QueryCustomerRequest }
	 * 
	 */
	public QueryCustomerRequest createQueryCustomerRequest() {
		return new QueryCustomerRequest();
	}

	/**
	 * Create an instance of {@link QueryContactLogIn }
	 * 
	 */
	public QueryContactLogIn createQueryContactLogIn() {
		return new QueryContactLogIn();
	}

	/**
	 * Create an instance of {@link BillcycleIdList }
	 * 
	 */
	public BillcycleIdList createBillcycleIdList() {
		return new BillcycleIdList();
	}

	/**
	 * Create an instance of {@link GetSubscriberMetaDataRequest }
	 * 
	 */
	public GetSubscriberMetaDataRequest createGetSubscriberMetaDataRequest() {
		return new GetSubscriberMetaDataRequest();
	}

	/**
	 * Create an instance of {@link GetSubscriberResponse }
	 * 
	 */
	public GetSubscriberResponse createGetSubscriberResponse() {
		return new GetSubscriberResponse();
	}

	/**
	 * Create an instance of {@link GetCustomerMetaDatasIn }
	 * 
	 */
	public GetCustomerMetaDatasIn createGetCustomerMetaDatasIn() {
		return new GetCustomerMetaDatasIn();
	}

	/**
	 * Create an instance of {@link HomeZoneList }
	 * 
	 */
	public HomeZoneList createHomeZoneList() {
		return new HomeZoneList();
	}

	/**
	 * Create an instance of {@link GetCallScreenNumListRequest }
	 * 
	 */
	public GetCallScreenNumListRequest createGetCallScreenNumListRequest() {
		return new GetCallScreenNumListRequest();
	}

	/**
	 * Create an instance of {@link GetHistoryBillFileRequest }
	 * 
	 */
	public GetHistoryBillFileRequest createGetHistoryBillFileRequest() {
		return new GetHistoryBillFileRequest();
	}

	/**
	 * Create an instance of {@link GetCorpCustomerDataResponse }
	 * 
	 */
	public GetCorpCustomerDataResponse createGetCorpCustomerDataResponse() {
		return new GetCorpCustomerDataResponse();
	}

	/**
	 * Create an instance of {@link GetAccountResponse }
	 * 
	 */
	public GetAccountResponse createGetAccountResponse() {
		return new GetAccountResponse();
	}

	/**
	 * Create an instance of {@link GetOfferingSpecRequest }
	 * 
	 */
	public GetOfferingSpecRequest createGetOfferingSpecRequest() {
		return new GetOfferingSpecRequest();
	}

	/**
	 * Create an instance of {@link GetOfferSalesFeeResponse }
	 * 
	 */
	public GetOfferSalesFeeResponse createGetOfferSalesFeeResponse() {
		return new GetOfferSalesFeeResponse();
	}

	/**
	 * Create an instance of {@link SubGoodsResourceInfo }
	 * 
	 */
	public SubGoodsResourceInfo createSubGoodsResourceInfo() {
		return new SubGoodsResourceInfo();
	}

	/**
	 * Create an instance of {@link CheckPasswordOut }
	 * 
	 */
	public CheckPasswordOut createCheckPasswordOut() {
		return new CheckPasswordOut();
	}

	/**
	 * Create an instance of {@link GetOfferingIdList }
	 * 
	 */
	public GetOfferingIdList createGetOfferingIdList() {
		return new GetOfferingIdList();
	}

	/**
	 * Create an instance of {@link GetSaleOfferingsResponse }
	 * 
	 */
	public GetSaleOfferingsResponse createGetSaleOfferingsResponse() {
		return new GetSaleOfferingsResponse();
	}

	/**
	 * Create an instance of {@link GetCustomerOut }
	 * 
	 */
	public GetCustomerOut createGetCustomerOut() {
		return new GetCustomerOut();
	}

	/**
	 * Create an instance of {@link FnFInfo }
	 * 
	 */
	public FnFInfo createFnFInfo() {
		return new FnFInfo();
	}

	/**
	 * Create an instance of {@link CheckPasswordRequest }
	 * 
	 */
	public CheckPasswordRequest createCheckPasswordRequest() {
		return new CheckPasswordRequest();
	}

	/**
	 * Create an instance of {@link GetAccountIn }
	 * 
	 */
	public GetAccountIn createGetAccountIn() {
		return new GetAccountIn();
	}

	/**
	 * Create an instance of {@link GetAccountRequest }
	 * 
	 */
	public GetAccountRequest createGetAccountRequest() {
		return new GetAccountRequest();
	}

	/**
	 * Create an instance of {@link GetTimeSchemaIn }
	 * 
	 */
	public GetTimeSchemaIn createGetTimeSchemaIn() {
		return new GetTimeSchemaIn();
	}

	/**
	 * Create an instance of {@link GetHistoryBillFileInfo }
	 * 
	 */
	public GetHistoryBillFileInfo createGetHistoryBillFileInfo() {
		return new GetHistoryBillFileInfo();
	}

	/**
	 * Create an instance of {@link TimeSchemaList }
	 * 
	 */
	public TimeSchemaList createTimeSchemaList() {
		return new TimeSchemaList();
	}

	/**
	 * Create an instance of
	 * {@link GetOfferingSpecResponse.GetOfferingSpecBody }
	 * 
	 */
	public GetOfferingSpecResponse.GetOfferingSpecBody createGetOfferingSpecResponseGetOfferingSpecBody() {
		return new GetOfferingSpecResponse.GetOfferingSpecBody();
	}

	/**
	 * Create an instance of {@link PromotionList }
	 * 
	 */
	public PromotionList createPromotionList() {
		return new PromotionList();
	}

	/**
	 * Create an instance of {@link GetSpecialOfferingResponse }
	 * 
	 */
	public GetSpecialOfferingResponse createGetSpecialOfferingResponse() {
		return new GetSpecialOfferingResponse();
	}

	/**
	 * Create an instance of {@link GetGroupMemberDataResponse }
	 * 
	 */
	public GetGroupMemberDataResponse createGetGroupMemberDataResponse() {
		return new GetGroupMemberDataResponse();
	}

	/**
	 * Create an instance of {@link DictTableValue }
	 * 
	 */
	public DictTableValue createDictTableValue() {
		return new DictTableValue();
	}

	/**
	 * Create an instance of {@link OfferingSpecInfo }
	 * 
	 */
	public OfferingSpecInfo createOfferingSpecInfo() {
		return new OfferingSpecInfo();
	}

	/**
	 * Create an instance of {@link GetSubscriberMetaDataIn }
	 * 
	 */
	public GetSubscriberMetaDataIn createGetSubscriberMetaDataIn() {
		return new GetSubscriberMetaDataIn();
	}

	/**
	 * Create an instance of {@link GetCustSOfferingResponse }
	 * 
	 */
	public GetCustSOfferingResponse createGetCustSOfferingResponse() {
		return new GetCustSOfferingResponse();
	}

	/**
	 * Create an instance of {@link GetNetworkSettingDataList }
	 * 
	 */
	public GetNetworkSettingDataList createGetNetworkSettingDataList() {
		return new GetNetworkSettingDataList();
	}

	/**
	 * Create an instance of {@link GetSubscriberHistoryOut }
	 * 
	 */
	public GetSubscriberHistoryOut createGetSubscriberHistoryOut() {
		return new GetSubscriberHistoryOut();
	}

	/**
	 * Create an instance of {@link Skus }
	 * 
	 */
	public Skus createSkus() {
		return new Skus();
	}

	/**
	 * Create an instance of {@link GetDictTableRequest }
	 * 
	 */
	public GetDictTableRequest createGetDictTableRequest() {
		return new GetDictTableRequest();
	}

	/**
	 * Create an instance of {@link GetSaleOfferingIdList }
	 * 
	 */
	public GetSaleOfferingIdList createGetSaleOfferingIdList() {
		return new GetSaleOfferingIdList();
	}

	/**
	 * Create an instance of {@link GetCustPOfferingIn }
	 * 
	 */
	public GetCustPOfferingIn createGetCustPOfferingIn() {
		return new GetCustPOfferingIn();
	}

	/**
	 * Create an instance of {@link GetOfferingSpecList }
	 * 
	 */
	public GetOfferingSpecList createGetOfferingSpecList() {
		return new GetOfferingSpecList();
	}

	/**
	 * Create an instance of {@link ValidateOfferingRelationRequest }
	 * 
	 */
	public ValidateOfferingRelationRequest createValidateOfferingRelationRequest() {
		return new ValidateOfferingRelationRequest();
	}

	/**
	 * Create an instance of {@link RetrivedScreenNumInfo }
	 * 
	 */
	public RetrivedScreenNumInfo createRetrivedScreenNumInfo() {
		return new RetrivedScreenNumInfo();
	}

	/**
	 * Create an instance of {@link CustomerFeeFactorInfo }
	 * 
	 */
	public CustomerFeeFactorInfo createCustomerFeeFactorInfo() {
		return new CustomerFeeFactorInfo();
	}

	/**
	 * Create an instance of {@link GetCorpCustomerDataOut }
	 * 
	 */
	public GetCorpCustomerDataOut createGetCorpCustomerDataOut() {
		return new GetCorpCustomerDataOut();
	}

	/**
	 * Create an instance of {@link GetGroupResponse }
	 * 
	 */
	public GetGroupResponse createGetGroupResponse() {
		return new GetGroupResponse();
	}

	/**
	 * Create an instance of {@link GetCorpCustomerDataRequest }
	 * 
	 */
	public GetCorpCustomerDataRequest createGetCorpCustomerDataRequest() {
		return new GetCorpCustomerDataRequest();
	}
}
