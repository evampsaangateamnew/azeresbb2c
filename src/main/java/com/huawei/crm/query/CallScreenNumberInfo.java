package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.CallScreenListInfo;

/**
 * <p>
 * Java class for CallScreenNumberInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CallScreenNumberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Index" type="{http://crm.huawei.com/basetype/}SubCallScreenNoIndex" minOccurs="0"/>
 *         &lt;element name="CallScreenNumberInfo" type="{http://crm.huawei.com/basetype/}CallScreenListInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallScreenNumberInfo", propOrder = { "index", "callScreenNumberInfo" })
public class CallScreenNumberInfo {
	@XmlElement(name = "Index")
	protected Long index;
	@XmlElement(name = "CallScreenNumberInfo")
	protected CallScreenListInfo callScreenNumberInfo;

	/**
	 * Gets the value of the index property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getIndex() {
		return index;
	}

	/**
	 * Sets the value of the index property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setIndex(Long value) {
		this.index = value;
	}

	/**
	 * Gets the value of the callScreenNumberInfo property.
	 * 
	 * @return possible object is {@link CallScreenListInfo }
	 * 
	 */
	public CallScreenListInfo getCallScreenNumberInfo() {
		return callScreenNumberInfo;
	}

	/**
	 * Sets the value of the callScreenNumberInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link CallScreenListInfo }
	 * 
	 */
	public void setCallScreenNumberInfo(CallScreenListInfo value) {
		this.callScreenNumberInfo = value;
	}
}
