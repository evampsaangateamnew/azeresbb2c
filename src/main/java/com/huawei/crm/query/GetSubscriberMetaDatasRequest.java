package com.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.RequestHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/basetype/}RequestHeader"/>
 *         &lt;element name="GetSubscriberMetaDatasBody" type="{http://crm.huawei.com/query/}GetSubscriberMetaDatasIn" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "requestHeader", "getSubscriberMetaDatasBody" })
@XmlRootElement(name = "GetSubscriberMetaDatasRequest")
public class GetSubscriberMetaDatasRequest {
	@XmlElement(name = "RequestHeader", required = true)
	protected RequestHeader requestHeader;
	@XmlElement(name = "GetSubscriberMetaDatasBody", required = true)
	protected List<GetSubscriberMetaDatasIn> getSubscriberMetaDatasBody;

	/**
	 * Gets the value of the requestHeader property.
	 * 
	 * @return possible object is {@link RequestHeader }
	 * 
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * Sets the value of the requestHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RequestHeader }
	 * 
	 */
	public void setRequestHeader(RequestHeader value) {
		this.requestHeader = value;
	}

	/**
	 * Gets the value of the getSubscriberMetaDatasBody property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the getSubscriberMetaDatasBody property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGetSubscriberMetaDatasBody().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link GetSubscriberMetaDatasIn }
	 * 
	 * 
	 */
	public List<GetSubscriberMetaDatasIn> getGetSubscriberMetaDatasBody() {
		if (getSubscriberMetaDatasBody == null) {
			getSubscriberMetaDatasBody = new ArrayList<GetSubscriberMetaDatasIn>();
		}
		return this.getSubscriberMetaDatasBody;
	}
}
