package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetSubscriberMetaDatasOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetSubscriberMetaDatasOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}SubscriberStatus" minOccurs="0"/>
 *         &lt;element name="BEID" type="{http://crm.huawei.com/basetype/}BeID" minOccurs="0"/>
 *         &lt;element name="SupplementFlag" type="{http://crm.huawei.com/basetype/}SelectFlag" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="ICCID" type="{http://crm.huawei.com/basetype/}ICCID" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubscriberMetaDatasOut", propOrder = {})
public class GetSubscriberMetaDatasOut {
	@XmlElement(name = "SubscriberId")
	protected Long subscriberId;
	@XmlElement(name = "Status")
	protected String status;
	@XmlElement(name = "BEID")
	protected String beid;
	@XmlElement(name = "SupplementFlag")
	protected String supplementFlag;
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "ICCID")
	protected String iccid;

	/**
	 * Gets the value of the subscriberId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Sets the value of the subscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setSubscriberId(Long value) {
		this.subscriberId = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the beid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBEID() {
		return beid;
	}

	/**
	 * Sets the value of the beid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBEID(String value) {
		this.beid = value;
	}

	/**
	 * Gets the value of the supplementFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSupplementFlag() {
		return supplementFlag;
	}

	/**
	 * Sets the value of the supplementFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSupplementFlag(String value) {
		this.supplementFlag = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the iccid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getICCID() {
		return iccid;
	}

	/**
	 * Sets the value of the iccid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setICCID(String value) {
		this.iccid = value;
	}
}
