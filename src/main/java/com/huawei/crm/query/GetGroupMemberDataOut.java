package com.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetGroupMemberDataOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupMemberDataOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupDataList" type="{http://crm.huawei.com/query/}GetGroupOfMemberDataInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupMemberDataOut", propOrder = { "getGroupDataList" })
public class GetGroupMemberDataOut {
	@XmlElement(name = "GetGroupDataList")
	protected List<GetGroupOfMemberDataInfo> getGroupDataList;

	/**
	 * Gets the value of the getGroupDataList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the getGroupDataList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGetGroupDataList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link GetGroupOfMemberDataInfo }
	 * 
	 * 
	 */
	public List<GetGroupOfMemberDataInfo> getGetGroupDataList() {
		if (getGroupDataList == null) {
			getGroupDataList = new ArrayList<GetGroupOfMemberDataInfo>();
		}
		return this.getGroupDataList;
	}
}
