package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for GetCustomerHistoryIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomerHistoryIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalCustomerId" type="{http://crm.huawei.com/basetype/}ExternalCustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="StartDate" type="{http://crm.huawei.com/basetype/}StartDate" minOccurs="0"/>
 *         &lt;element name="EndDate" type="{http://crm.huawei.com/basetype/}EndDate" minOccurs="0"/>
 *         &lt;element name="OrderItemType" type="{http://crm.huawei.com/basetype/}OrderItemType" minOccurs="0"/>
 *         &lt;element name="BeginRowNum" type="{http://crm.huawei.com/basetype/}BeginRowNum"/>
 *         &lt;element name="FetchRowNum" type="{http://crm.huawei.com/basetype/}FetchRowNum"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomerHistoryIn", propOrder = {})
public class GetCustomerHistoryIn {
	@XmlElement(name = "ExternalCustomerId")
	protected String externalCustomerId;
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "StartDate")
	protected String startDate;
	@XmlElement(name = "EndDate")
	protected String endDate;
	@XmlElement(name = "OrderItemType")
	protected String orderItemType;
	@XmlElement(name = "BeginRowNum")
	protected long beginRowNum;
	@XmlElement(name = "FetchRowNum")
	protected long fetchRowNum;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the externalCustomerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalCustomerId() {
		return externalCustomerId;
	}

	/**
	 * Sets the value of the externalCustomerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalCustomerId(String value) {
		this.externalCustomerId = value;
	}

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the startDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * Sets the value of the startDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStartDate(String value) {
		this.startDate = value;
	}

	/**
	 * Gets the value of the endDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * Sets the value of the endDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEndDate(String value) {
		this.endDate = value;
	}

	/**
	 * Gets the value of the orderItemType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderItemType() {
		return orderItemType;
	}

	/**
	 * Sets the value of the orderItemType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderItemType(String value) {
		this.orderItemType = value;
	}

	/**
	 * Gets the value of the beginRowNum property.
	 * 
	 */
	public long getBeginRowNum() {
		return beginRowNum;
	}

	/**
	 * Sets the value of the beginRowNum property.
	 * 
	 */
	public void setBeginRowNum(long value) {
		this.beginRowNum = value;
	}

	/**
	 * Gets the value of the fetchRowNum property.
	 * 
	 */
	public long getFetchRowNum() {
		return fetchRowNum;
	}

	/**
	 * Sets the value of the fetchRowNum property.
	 * 
	 */
	public void setFetchRowNum(long value) {
		this.fetchRowNum = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
