package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ActualCustInfo;
import com.huawei.crm.basetype.AddressList;
import com.huawei.crm.basetype.ConsumptionLimitList;
import com.huawei.crm.basetype.DNESettingList;
import com.huawei.crm.basetype.DPAInfo;
import com.huawei.crm.basetype.ExtParameterList;
import com.huawei.crm.basetype.GetSubOfferingInfo;
import com.huawei.crm.basetype.GetSubOfferingList;

/**
 * <p>
 * Java class for GetSubscriberOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetSubscriberOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerCode" type="{http://crm.huawei.com/basetype/}CustomerCode" minOccurs="0"/>
 *         &lt;element name="ActualCustomer" type="{http://crm.huawei.com/basetype/}ActualCustInfo" minOccurs="0"/>
 *         &lt;element name="ExternalCustomerId" type="{http://crm.huawei.com/basetype/}ExternalCustomerId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="SubscriberType" type="{http://crm.huawei.com/basetype/}SubscriberPrepaidFlag" minOccurs="0"/>
 *         &lt;element name="SubscriberLevel" type="{http://crm.huawei.com/basetype/}SubscriberLevel" minOccurs="0"/>
 *         &lt;element name="AccountCode" type="{http://crm.huawei.com/basetype/}AccountCode32" minOccurs="0"/>
 *         &lt;element name="NetworkType" type="{http://crm.huawei.com/basetype/}TeleType" minOccurs="0"/>
 *         &lt;element name="IMEI" type="{http://crm.huawei.com/basetype/}IMEI" minOccurs="0"/>
 *         &lt;element name="ICCID" type="{http://crm.huawei.com/basetype/}ICCID" minOccurs="0"/>
 *         &lt;element name="PIN1" type="{http://crm.huawei.com/basetype/}PIN1" minOccurs="0"/>
 *         &lt;element name="PIN2" type="{http://crm.huawei.com/basetype/}PIN2" minOccurs="0"/>
 *         &lt;element name="PUK1" type="{http://crm.huawei.com/basetype/}PUK1" minOccurs="0"/>
 *         &lt;element name="PUK2" type="{http://crm.huawei.com/basetype/}PUK2" minOccurs="0"/>
 *         &lt;element name="BrandId" type="{http://crm.huawei.com/basetype/}BrandId" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://crm.huawei.com/basetype/}Language" minOccurs="0"/>
 *         &lt;element name="WrittenLanguage" type="{http://crm.huawei.com/basetype/}WrittenLanguage" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpireDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ActiveDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="PrimaryOffering" type="{http://crm.huawei.com/basetype/}GetSubOfferingInfo" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://crm.huawei.com/basetype/}GetSubOfferingList" minOccurs="0"/>
 *         &lt;element name="DealerId" type="{http://crm.huawei.com/basetype/}DealerId" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}SubscriberStatus" minOccurs="0"/>
 *         &lt;element name="StatusReason" type="{http://crm.huawei.com/basetype/}SubscriberStatusReason" minOccurs="0"/>
 *         &lt;element name="BeId" type="{http://crm.huawei.com/basetype/}BeID" minOccurs="0"/>
 *         &lt;element name="ResignedTimestamp" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="PhoneBookRegistration" type="{http://crm.huawei.com/basetype/}PhoneBookRegistration" minOccurs="0"/>
 *         &lt;element name="ConsumptionLimitList" type="{http://crm.huawei.com/basetype/}ConsumptionLimitList" minOccurs="0"/>
 *         &lt;element name="DNESettingsList" type="{http://crm.huawei.com/basetype/}DNESettingList" minOccurs="0"/>
 *         &lt;element name="AddressList" type="{http://crm.huawei.com/basetype/}AddressList" minOccurs="0"/>
 *         &lt;element name="DPAInfo" type="{http://crm.huawei.com/basetype/}DPAInfo" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "com.huawei.crm.query.GetSubscriberOut", propOrder = {})
public class GetSubscriberOut {
	/*
	 * @Override public String toString() { for (ExtParameterInfo
	 * iterable_element : extParamList.getParameterInfo()) { } return
	 * "GetSubscriberOut [subscriberId=" + subscriberId + ", accountId=" +
	 * accountId + ", customerId=" + customerId + ", customerCode=" +
	 * customerCode + ", actualCustomer=" + actualCustomer +
	 * ", externalCustomerId=" + externalCustomerId + ", serviceNumber=" +
	 * serviceNumber + ", subscriberType=" + subscriberType +
	 * ", subscriberLevel=" + subscriberLevel + ", accountCode=" + accountCode +
	 * ", networkType=" + networkType + ", imei=" + imei + ", iccid=" + iccid +
	 * ", pin1=" + pin1 + ", pin2=" + pin2 + ", puk1=" + puk1 + ", puk2=" + puk2
	 * + ", brandId=" + brandId + ", language=" + language +
	 * ", writtenLanguage=" + writtenLanguage + ", effectiveDate=" +
	 * effectiveDate + ", expireDate=" + expireDate + ", activeDate=" +
	 * activeDate + ", primaryOffering=" + primaryOffering +
	 * ", supplementaryOfferingList=" + supplementaryOfferingList +
	 * ", dealerId=" + dealerId + ", status=" + status + ", statusReason=" +
	 * statusReason + ", beId=" + beId + ", resignedTimestamp=" +
	 * resignedTimestamp + ", phoneBookRegistration=" + phoneBookRegistration +
	 * ", consumptionLimitList=" + consumptionLimitList + ", dneSettingsList=" +
	 * dneSettingsList + ", addressList=" + addressList + ", dpaInfo=" + dpaInfo
	 * + ", extParamList=" + extParamList + "]"; }
	 */
	@XmlElement(name = "IMSI")
	protected String imsi;
	protected String actualCustomerId;
	@XmlElement(name = "IVRBlacklist")
	protected String ivrBlacklist;
	protected String password;
	@XmlElement(name = "StatusDetail")
	protected String statusDetail;
	@XmlElement(name = "LoyaltySegment")
	protected String loyaltySegment;

	/**
	 * @return the imsi
	 */
	public String getImsi() {
		return imsi;
	}

	/**
	 * @param imsi
	 *            the imsi to set
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	/**
	 * @return the actualCustomerId
	 */
	public String getActualCustomerId() {
		return actualCustomerId;
	}

	/**
	 * @param actualCustomerId
	 *            the actualCustomerId to set
	 */
	public void setActualCustomerId(String actualCustomerId) {
		this.actualCustomerId = actualCustomerId;
	}

	/**
	 * @return the ivrBlacklist
	 */
	public String getIvrBlacklist() {
		return ivrBlacklist;
	}

	/**
	 * @param ivrBlacklist
	 *            the ivrBlacklist to set
	 */
	public void setIvrBlacklist(String ivrBlacklist) {
		this.ivrBlacklist = ivrBlacklist;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the statusDetail
	 */
	public String getStatusDetail() {
		return statusDetail;
	}

	/**
	 * @param statusDetail
	 *            the statusDetail to set
	 */
	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}

	/**
	 * @return the loyaltySegment
	 */
	public String getLoyaltySegment() {
		return loyaltySegment;
	}

	/**
	 * @param loyaltySegment
	 *            the loyaltySegment to set
	 */
	public void setLoyaltySegment(String loyaltySegment) {
		this.loyaltySegment = loyaltySegment;
	}

	/**
	 * @return the imei
	 */
	public String getImei() {
		return imei;
	}

	/**
	 * @param imei
	 *            the imei to set
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}

	/**
	 * @return the iccid
	 */
	public String getIccid() {
		return iccid;
	}

	/**
	 * @param iccid
	 *            the iccid to set
	 */
	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	/**
	 * @return the pin1
	 */
	public String getPin1() {
		return pin1;
	}

	/**
	 * @param pin1
	 *            the pin1 to set
	 */
	public void setPin1(String pin1) {
		this.pin1 = pin1;
	}

	/**
	 * @return the pin2
	 */
	public String getPin2() {
		return pin2;
	}

	/**
	 * @param pin2
	 *            the pin2 to set
	 */
	public void setPin2(String pin2) {
		this.pin2 = pin2;
	}

	/**
	 * @return the puk1
	 */
	public String getPuk1() {
		return puk1;
	}

	/**
	 * @param puk1
	 *            the puk1 to set
	 */
	public void setPuk1(String puk1) {
		this.puk1 = puk1;
	}

	/**
	 * @return the puk2
	 */
	public String getPuk2() {
		return puk2;
	}

	/**
	 * @param puk2
	 *            the puk2 to set
	 */
	public void setPuk2(String puk2) {
		this.puk2 = puk2;
	}

	/**
	 * @return the dneSettingsList
	 */
	public DNESettingList getDneSettingsList() {
		return dneSettingsList;
	}

	/**
	 * @param dneSettingsList
	 *            the dneSettingsList to set
	 */
	public void setDneSettingsList(DNESettingList dneSettingsList) {
		this.dneSettingsList = dneSettingsList;
	}

	/**
	 * @return the dpaInfo
	 */
	public DPAInfo getDpaInfo() {
		return dpaInfo;
	}

	/**
	 * @param dpaInfo
	 *            the dpaInfo to set
	 */
	public void setDpaInfo(DPAInfo dpaInfo) {
		this.dpaInfo = dpaInfo;
	}

	@XmlElement(name = "SubscriberId")
	protected Long subscriberId;
	@XmlElement(name = "AccountId")
	protected Long accountId;
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "CustomerCode")
	protected String customerCode;
	@XmlElement(name = "ActualCustomer")
	protected ActualCustInfo actualCustomer;
	@XmlElement(name = "ExternalCustomerId")
	protected String externalCustomerId;
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "SubscriberType")
	protected String subscriberType;
	@XmlElement(name = "SubscriberLevel")
	protected String subscriberLevel;
	@XmlElement(name = "AccountCode")
	protected String accountCode;
	@XmlElement(name = "NetworkType")
	protected String networkType;
	@XmlElement(name = "IMEI")
	protected String imei;
	@XmlElement(name = "ICCID")
	protected String iccid;
	@XmlElement(name = "PIN1")
	protected String pin1;
	@XmlElement(name = "PIN2")
	protected String pin2;
	@XmlElement(name = "PUK1")
	protected String puk1;
	@XmlElement(name = "PUK2")
	protected String puk2;
	@XmlElement(name = "BrandId")
	protected String brandId;
	@XmlElement(name = "Language")
	protected String language;
	@XmlElement(name = "WrittenLanguage")
	protected String writtenLanguage;
	@XmlElement(name = "EffectiveDate")
	protected String effectiveDate;
	@XmlElement(name = "ExpireDate")
	protected String expireDate;
	@XmlElement(name = "ActiveDate")
	protected String activeDate;
	@XmlElement(name = "PrimaryOffering")
	protected GetSubOfferingInfo primaryOffering;
	@XmlElement(name = "SupplementaryOfferingList")
	protected GetSubOfferingList supplementaryOfferingList;
	@XmlElement(name = "DealerId")
	protected String dealerId;
	@XmlElement(name = "Status")
	protected String status;
	@XmlElement(name = "StatusReason")
	protected String statusReason;
	@XmlElement(name = "BeId")
	protected String beId;
	@XmlElement(name = "ResignedTimestamp")
	protected String resignedTimestamp;
	@XmlElement(name = "PhoneBookRegistration")
	protected String phoneBookRegistration;
	@XmlElement(name = "ConsumptionLimitList")
	protected ConsumptionLimitList consumptionLimitList;
	@XmlElement(name = "DNESettingsList")
	protected DNESettingList dneSettingsList;
	@XmlElement(name = "AddressList")
	protected AddressList addressList;
	@XmlElement(name = "DPAInfo")
	protected DPAInfo dpaInfo;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the subscriberId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Sets the value of the subscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setSubscriberId(Long value) {
		this.subscriberId = value;
	}

	/**
	 * Gets the value of the accountId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * Sets the value of the accountId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAccountId(Long value) {
		this.accountId = value;
	}

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the customerCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * Sets the value of the customerCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerCode(String value) {
		this.customerCode = value;
	}

	/**
	 * Gets the value of the actualCustomer property.
	 * 
	 * @return possible object is {@link ActualCustInfo }
	 * 
	 */
	public ActualCustInfo getActualCustomer() {
		return actualCustomer;
	}

	/**
	 * Sets the value of the actualCustomer property.
	 * 
	 * @param value
	 *            allowed object is {@link ActualCustInfo }
	 * 
	 */
	public void setActualCustomer(ActualCustInfo value) {
		this.actualCustomer = value;
	}

	/**
	 * Gets the value of the externalCustomerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalCustomerId() {
		return externalCustomerId;
	}

	/**
	 * Sets the value of the externalCustomerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalCustomerId(String value) {
		this.externalCustomerId = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the subscriberType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubscriberType() {
		return subscriberType;
	}

	/**
	 * Sets the value of the subscriberType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubscriberType(String value) {
		this.subscriberType = value;
	}

	/**
	 * Gets the value of the subscriberLevel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubscriberLevel() {
		return subscriberLevel;
	}

	/**
	 * Sets the value of the subscriberLevel property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubscriberLevel(String value) {
		this.subscriberLevel = value;
	}

	/**
	 * Gets the value of the accountCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccountCode() {
		return accountCode;
	}

	/**
	 * Sets the value of the accountCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccountCode(String value) {
		this.accountCode = value;
	}

	/**
	 * Gets the value of the networkType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetworkType() {
		return networkType;
	}

	/**
	 * Sets the value of the networkType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetworkType(String value) {
		this.networkType = value;
	}

	/**
	 * Gets the value of the imei property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIMEI() {
		return imei;
	}

	/**
	 * Sets the value of the imei property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIMEI(String value) {
		this.imei = value;
	}

	/**
	 * Gets the value of the iccid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getICCID() {
		return iccid;
	}

	/**
	 * Sets the value of the iccid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setICCID(String value) {
		this.iccid = value;
	}

	/**
	 * Gets the value of the pin1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPIN1() {
		return pin1;
	}

	/**
	 * Sets the value of the pin1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPIN1(String value) {
		this.pin1 = value;
	}

	/**
	 * Gets the value of the pin2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPIN2() {
		return pin2;
	}

	/**
	 * Sets the value of the pin2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPIN2(String value) {
		this.pin2 = value;
	}

	/**
	 * Gets the value of the puk1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPUK1() {
		return puk1;
	}

	/**
	 * Sets the value of the puk1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPUK1(String value) {
		this.puk1 = value;
	}

	/**
	 * Gets the value of the puk2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPUK2() {
		return puk2;
	}

	/**
	 * Sets the value of the puk2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPUK2(String value) {
		this.puk2 = value;
	}

	/**
	 * Gets the value of the brandId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBrandId() {
		return brandId;
	}

	/**
	 * Sets the value of the brandId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBrandId(String value) {
		this.brandId = value;
	}

	/**
	 * Gets the value of the language property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the value of the language property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLanguage(String value) {
		this.language = value;
	}

	/**
	 * Gets the value of the writtenLanguage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWrittenLanguage() {
		return writtenLanguage;
	}

	/**
	 * Sets the value of the writtenLanguage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWrittenLanguage(String value) {
		this.writtenLanguage = value;
	}

	/**
	 * Gets the value of the effectiveDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Sets the value of the effectiveDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveDate(String value) {
		this.effectiveDate = value;
	}

	/**
	 * Gets the value of the expireDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * Sets the value of the expireDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpireDate(String value) {
		this.expireDate = value;
	}

	/**
	 * Gets the value of the activeDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActiveDate() {
		return activeDate;
	}

	/**
	 * Sets the value of the activeDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActiveDate(String value) {
		this.activeDate = value;
	}

	/**
	 * Gets the value of the primaryOffering property.
	 * 
	 * @return possible object is {@link GetSubOfferingInfo }
	 * 
	 */
	public GetSubOfferingInfo getPrimaryOffering() {
		return primaryOffering;
	}

	/**
	 * Sets the value of the primaryOffering property.
	 * 
	 * @param value
	 *            allowed object is {@link GetSubOfferingInfo }
	 * 
	 */
	public void setPrimaryOffering(GetSubOfferingInfo value) {
		this.primaryOffering = value;
	}

	/**
	 * Gets the value of the supplementaryOfferingList property.
	 * 
	 * @return possible object is {@link GetSubOfferingList }
	 * 
	 */
	public GetSubOfferingList getSupplementaryOfferingList() {
		return supplementaryOfferingList;
	}

	/**
	 * Sets the value of the supplementaryOfferingList property.
	 * 
	 * @param value
	 *            allowed object is {@link GetSubOfferingList }
	 * 
	 */
	public void setSupplementaryOfferingList(GetSubOfferingList value) {
		this.supplementaryOfferingList = value;
	}

	/**
	 * Gets the value of the dealerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDealerId() {
		return dealerId;
	}

	/**
	 * Sets the value of the dealerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDealerId(String value) {
		this.dealerId = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the statusReason property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatusReason() {
		return statusReason;
	}

	/**
	 * Sets the value of the statusReason property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatusReason(String value) {
		this.statusReason = value;
	}

	/**
	 * Gets the value of the beId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeId() {
		return beId;
	}

	/**
	 * Sets the value of the beId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeId(String value) {
		this.beId = value;
	}

	/**
	 * Gets the value of the resignedTimestamp property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getResignedTimestamp() {
		return resignedTimestamp;
	}

	/**
	 * Sets the value of the resignedTimestamp property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setResignedTimestamp(String value) {
		this.resignedTimestamp = value;
	}

	/**
	 * Gets the value of the phoneBookRegistration property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPhoneBookRegistration() {
		return phoneBookRegistration;
	}

	/**
	 * Sets the value of the phoneBookRegistration property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPhoneBookRegistration(String value) {
		this.phoneBookRegistration = value;
	}

	/**
	 * Gets the value of the consumptionLimitList property.
	 * 
	 * @return possible object is {@link ConsumptionLimitList }
	 * 
	 */
	public ConsumptionLimitList getConsumptionLimitList() {
		return consumptionLimitList;
	}

	/**
	 * Sets the value of the consumptionLimitList property.
	 * 
	 * @param value
	 *            allowed object is {@link ConsumptionLimitList }
	 * 
	 */
	public void setConsumptionLimitList(ConsumptionLimitList value) {
		this.consumptionLimitList = value;
	}

	/**
	 * Gets the value of the dneSettingsList property.
	 * 
	 * @return possible object is {@link DNESettingList }
	 * 
	 */
	public DNESettingList getDNESettingsList() {
		return dneSettingsList;
	}

	/**
	 * Sets the value of the dneSettingsList property.
	 * 
	 * @param value
	 *            allowed object is {@link DNESettingList }
	 * 
	 */
	public void setDNESettingsList(DNESettingList value) {
		this.dneSettingsList = value;
	}

	/**
	 * Gets the value of the addressList property.
	 * 
	 * @return possible object is {@link AddressList }
	 * 
	 */
	public AddressList getAddressList() {
		return addressList;
	}

	/**
	 * Sets the value of the addressList property.
	 * 
	 * @param value
	 *            allowed object is {@link AddressList }
	 * 
	 */
	public void setAddressList(AddressList value) {
		this.addressList = value;
	}

	/**
	 * Gets the value of the dpaInfo property.
	 * 
	 * @return possible object is {@link DPAInfo }
	 * 
	 */
	public DPAInfo getDPAInfo() {
		return dpaInfo;
	}

	/**
	 * Sets the value of the dpaInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link DPAInfo }
	 * 
	 */
	public void setDPAInfo(DPAInfo value) {
		this.dpaInfo = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
