package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for GetHistoryBillFileIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetHistoryBillFileIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/basetype/}AccountId"/>
 *         &lt;element name="BillCycleId" type="{http://crm.huawei.com/query/}BillcycleIdList"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetHistoryBillFileIn", propOrder = {})
public class GetHistoryBillFileIn {
	@XmlElement(name = "AccountId")
	protected long accountId;
	@XmlElement(name = "BillCycleId", required = true)
	protected BillcycleIdList billCycleId;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the accountId property.
	 * 
	 */
	public long getAccountId() {
		return accountId;
	}

	/**
	 * Sets the value of the accountId property.
	 * 
	 */
	public void setAccountId(long value) {
		this.accountId = value;
	}

	/**
	 * Gets the value of the billCycleId property.
	 * 
	 * @return possible object is {@link BillcycleIdList }
	 * 
	 */
	public BillcycleIdList getBillCycleId() {
		return billCycleId;
	}

	/**
	 * Sets the value of the billCycleId property.
	 * 
	 * @param value
	 *            allowed object is {@link BillcycleIdList }
	 * 
	 */
	public void setBillCycleId(BillcycleIdList value) {
		this.billCycleId = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
