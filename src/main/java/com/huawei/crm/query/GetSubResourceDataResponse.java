package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ResponseHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="GetSubTeleResDataBody" type="{http://crm.huawei.com/query/}SubTeleResourceList" minOccurs="0"/>
 *         &lt;element name="GetSubGoodsResDataBody" type="{http://crm.huawei.com/query/}SubGoodsResourceList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "getSubTeleResDataBody", "getSubGoodsResDataBody" })
@XmlRootElement(name = "GetSubResourceDataResponse")
public class GetSubResourceDataResponse {
	@XmlElement(name = "ResponseHeader", required = true)
	protected ResponseHeader responseHeader;
	@XmlElement(name = "GetSubTeleResDataBody")
	protected SubTeleResourceList getSubTeleResDataBody;
	@XmlElement(name = "GetSubGoodsResDataBody")
	protected SubGoodsResourceList getSubGoodsResDataBody;

	/**
	 * Gets the value of the responseHeader property.
	 * 
	 * @return possible object is {@link ResponseHeader }
	 * 
	 */
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	/**
	 * Sets the value of the responseHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseHeader }
	 * 
	 */
	public void setResponseHeader(ResponseHeader value) {
		this.responseHeader = value;
	}

	/**
	 * Gets the value of the getSubTeleResDataBody property.
	 * 
	 * @return possible object is {@link SubTeleResourceList }
	 * 
	 */
	public SubTeleResourceList getGetSubTeleResDataBody() {
		return getSubTeleResDataBody;
	}

	/**
	 * Sets the value of the getSubTeleResDataBody property.
	 * 
	 * @param value
	 *            allowed object is {@link SubTeleResourceList }
	 * 
	 */
	public void setGetSubTeleResDataBody(SubTeleResourceList value) {
		this.getSubTeleResDataBody = value;
	}

	/**
	 * Gets the value of the getSubGoodsResDataBody property.
	 * 
	 * @return possible object is {@link SubGoodsResourceList }
	 * 
	 */
	public SubGoodsResourceList getGetSubGoodsResDataBody() {
		return getSubGoodsResDataBody;
	}

	/**
	 * Sets the value of the getSubGoodsResDataBody property.
	 * 
	 * @param value
	 *            allowed object is {@link SubGoodsResourceList }
	 * 
	 */
	public void setGetSubGoodsResDataBody(SubGoodsResourceList value) {
		this.getSubGoodsResDataBody = value;
	}
}
