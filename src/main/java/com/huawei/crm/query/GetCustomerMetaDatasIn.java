package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetCustomerMetaDatasIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomerMetaDatasIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalCustomerId" type="{http://crm.huawei.com/basetype/}ExternalCustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="CertificateNumber" type="{http://crm.huawei.com/basetype/}CertificateNumber" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomerMetaDatasIn", propOrder = {})
public class GetCustomerMetaDatasIn {
	@XmlElement(name = "ExternalCustomerId")
	protected String externalCustomerId;
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "CertificateNumber")
	protected String certificateNumber;

	/**
	 * Gets the value of the externalCustomerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalCustomerId() {
		return externalCustomerId;
	}

	/**
	 * Sets the value of the externalCustomerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalCustomerId(String value) {
		this.externalCustomerId = value;
	}

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the certificateNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * Sets the value of the certificateNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCertificateNumber(String value) {
		this.certificateNumber = value;
	}
}
