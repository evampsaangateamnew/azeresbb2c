package com.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.MemberSubscriberInfo;

/**
 * <p>
 * Java class for MemberSubscriberList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="MemberSubscriberList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberSubscriberList" type="{http://crm.huawei.com/basetype/}MemberSubscriberInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MemberSubscriberList", propOrder = { "memberSubscriberList" })
public class MemberSubscriberList {
	@XmlElement(name = "MemberSubscriberList")
	protected List<MemberSubscriberInfo> memberSubscriberList;

	/**
	 * Gets the value of the memberSubscriberList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the memberSubscriberList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getMemberSubscriberList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link MemberSubscriberInfo }
	 * 
	 * 
	 */
	public List<MemberSubscriberInfo> getMemberSubscriberList() {
		if (memberSubscriberList == null) {
			memberSubscriberList = new ArrayList<MemberSubscriberInfo>();
		}
		return this.memberSubscriberList;
	}
}
