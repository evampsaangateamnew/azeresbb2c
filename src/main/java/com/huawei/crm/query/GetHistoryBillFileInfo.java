package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for GetHistoryBillFileInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetHistoryBillFileInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/basetype/}AccountId"/>
 *         &lt;element name="BillCycleId" type="{http://crm.huawei.com/basetype/}BillCycleId"/>
 *         &lt;element name="OsmsId" type="{http://crm.huawei.com/basetype/}OsmsId" minOccurs="0"/>
 *         &lt;element name="OsmsInvoiceId" type="{http://crm.huawei.com/basetype/}OsmsInvoiceId" minOccurs="0"/>
 *         &lt;element name="OsmsFilePath" type="{http://crm.huawei.com/basetype/}OsmsFilePath" minOccurs="0"/>
 *         &lt;element name="OsmsFileName" type="{http://crm.huawei.com/basetype/}OsmsFileName" minOccurs="0"/>
 *         &lt;element name="OsmsInvoiceType" type="{http://crm.huawei.com/basetype/}OsmsInvoiceType" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetHistoryBillFileInfo", propOrder = {})
public class GetHistoryBillFileInfo {
	@XmlElement(name = "AccountId")
	protected long accountId;
	@XmlElement(name = "BillCycleId", required = true)
	protected String billCycleId;
	@XmlElement(name = "OsmsId")
	protected String osmsId;
	@XmlElement(name = "OsmsInvoiceId")
	protected String osmsInvoiceId;
	@XmlElement(name = "OsmsFilePath")
	protected String osmsFilePath;
	@XmlElement(name = "OsmsFileName")
	protected String osmsFileName;
	@XmlElement(name = "OsmsInvoiceType")
	protected String osmsInvoiceType;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the accountId property.
	 * 
	 */
	public long getAccountId() {
		return accountId;
	}

	/**
	 * Sets the value of the accountId property.
	 * 
	 */
	public void setAccountId(long value) {
		this.accountId = value;
	}

	/**
	 * Gets the value of the billCycleId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillCycleId() {
		return billCycleId;
	}

	/**
	 * Sets the value of the billCycleId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillCycleId(String value) {
		this.billCycleId = value;
	}

	/**
	 * Gets the value of the osmsId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOsmsId() {
		return osmsId;
	}

	/**
	 * Sets the value of the osmsId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOsmsId(String value) {
		this.osmsId = value;
	}

	/**
	 * Gets the value of the osmsInvoiceId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOsmsInvoiceId() {
		return osmsInvoiceId;
	}

	/**
	 * Sets the value of the osmsInvoiceId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOsmsInvoiceId(String value) {
		this.osmsInvoiceId = value;
	}

	/**
	 * Gets the value of the osmsFilePath property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOsmsFilePath() {
		return osmsFilePath;
	}

	/**
	 * Sets the value of the osmsFilePath property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOsmsFilePath(String value) {
		this.osmsFilePath = value;
	}

	/**
	 * Gets the value of the osmsFileName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOsmsFileName() {
		return osmsFileName;
	}

	/**
	 * Sets the value of the osmsFileName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOsmsFileName(String value) {
		this.osmsFileName = value;
	}

	/**
	 * Gets the value of the osmsInvoiceType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOsmsInvoiceType() {
		return osmsInvoiceType;
	}

	/**
	 * Sets the value of the osmsInvoiceType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOsmsInvoiceType(String value) {
		this.osmsInvoiceType = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
