package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for GetCorpCustomerDataIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetCorpCustomerDataIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="GroupId" type="{http://crm.huawei.com/basetype/}GroupId" minOccurs="0"/>
 *         &lt;element name="GroupName" type="{http://crm.huawei.com/basetype/}GroupName" minOccurs="0"/>
 *         &lt;element name="GroupNo" type="{http://crm.huawei.com/basetype/}GroupNo" minOccurs="0"/>
 *         &lt;element name="CorporateNo" type="{http://crm.huawei.com/basetype/}CorporateNumber" minOccurs="0"/>
 *         &lt;element name="CorporateName" type="{http://crm.huawei.com/basetype/}CustomerName" minOccurs="0"/>
 *         &lt;element name="ContactName" type="{http://crm.huawei.com/basetype/}RelaName" minOccurs="0"/>
 *         &lt;element name="ContactServiceNumber" type="{http://crm.huawei.com/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="MemberServiceNumber" type="{http://crm.huawei.com/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCorpCustomerDataIn", propOrder = {})
public class GetCorpCustomerDataIn {
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "GroupId")
	protected Long groupId;
	@XmlElement(name = "GroupName")
	protected String groupName;
	@XmlElement(name = "GroupNo")
	protected String groupNo;
	@XmlElement(name = "CorporateNo")
	protected String corporateNo;
	@XmlElement(name = "CorporateName")
	protected String corporateName;
	@XmlElement(name = "ContactName")
	protected String contactName;
	@XmlElement(name = "ContactServiceNumber")
	protected String contactServiceNumber;
	@XmlElement(name = "MemberServiceNumber")
	protected String memberServiceNumber;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the groupId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * Sets the value of the groupId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setGroupId(Long value) {
		this.groupId = value;
	}

	/**
	 * Gets the value of the groupName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Sets the value of the groupName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGroupName(String value) {
		this.groupName = value;
	}

	/**
	 * Gets the value of the groupNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGroupNo() {
		return groupNo;
	}

	/**
	 * Sets the value of the groupNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGroupNo(String value) {
		this.groupNo = value;
	}

	/**
	 * Gets the value of the corporateNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorporateNo() {
		return corporateNo;
	}

	/**
	 * Sets the value of the corporateNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorporateNo(String value) {
		this.corporateNo = value;
	}

	/**
	 * Gets the value of the corporateName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorporateName() {
		return corporateName;
	}

	/**
	 * Sets the value of the corporateName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorporateName(String value) {
		this.corporateName = value;
	}

	/**
	 * Gets the value of the contactName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * Sets the value of the contactName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactName(String value) {
		this.contactName = value;
	}

	/**
	 * Gets the value of the contactServiceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactServiceNumber() {
		return contactServiceNumber;
	}

	/**
	 * Sets the value of the contactServiceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactServiceNumber(String value) {
		this.contactServiceNumber = value;
	}

	/**
	 * Gets the value of the memberServiceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberServiceNumber() {
		return memberServiceNumber;
	}

	/**
	 * Sets the value of the memberServiceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberServiceNumber(String value) {
		this.memberServiceNumber = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
