package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ResponseHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="GetCustomerMetaDataBody" type="{http://crm.huawei.com/query/}GetCustomerMetaDataOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "getCustomerMetaDataBody" })
@XmlRootElement(name = "GetCustomerMetaDataResponse")
public class GetCustomerMetaDataResponse {
	@XmlElement(name = "ResponseHeader", required = true)
	protected ResponseHeader responseHeader;
	@XmlElement(name = "GetCustomerMetaDataBody")
	protected GetCustomerMetaDataOut getCustomerMetaDataBody;

	/**
	 * Gets the value of the responseHeader property.
	 * 
	 * @return possible object is {@link ResponseHeader }
	 * 
	 */
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	/**
	 * Sets the value of the responseHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseHeader }
	 * 
	 */
	public void setResponseHeader(ResponseHeader value) {
		this.responseHeader = value;
	}

	/**
	 * Gets the value of the getCustomerMetaDataBody property.
	 * 
	 * @return possible object is {@link GetCustomerMetaDataOut }
	 * 
	 */
	public GetCustomerMetaDataOut getGetCustomerMetaDataBody() {
		return getCustomerMetaDataBody;
	}

	/**
	 * Sets the value of the getCustomerMetaDataBody property.
	 * 
	 * @param value
	 *            allowed object is {@link GetCustomerMetaDataOut }
	 * 
	 */
	public void setGetCustomerMetaDataBody(GetCustomerMetaDataOut value) {
		this.getCustomerMetaDataBody = value;
	}
}
