package com.huawei.crm.query;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for GetGroupMemberDataIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupMemberDataIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="GroupId" type="{http://crm.huawei.com/basetype/}GroupId"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="StartRow" type="{http://crm.huawei.com/basetype/}StartRow" minOccurs="0"/>
 *         &lt;element name="PageSize" type="{http://crm.huawei.com/basetype/}PageSize" minOccurs="0"/>
 *         &lt;element name="IncludePaymentRelation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IncludeMemberOffers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupMemberDataIn", propOrder = {})
public class GetGroupMemberDataIn {
	@XmlElement(name = "GroupId")
	protected long groupId;
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "StartRow")
	protected BigInteger startRow;
	@XmlElement(name = "PageSize")
	protected BigInteger pageSize;
	@XmlElement(name = "IncludePaymentRelation")
	protected Boolean includePaymentRelation;
	@XmlElement(name = "IncludeMemberOffers")
	protected Boolean includeMemberOffers;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the groupId property.
	 * 
	 */
	public long getGroupId() {
		return groupId;
	}

	/**
	 * Sets the value of the groupId property.
	 * 
	 */
	public void setGroupId(long value) {
		this.groupId = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the startRow property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getStartRow() {
		return startRow;
	}

	/**
	 * Sets the value of the startRow property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setStartRow(BigInteger value) {
		this.startRow = value;
	}

	/**
	 * Gets the value of the pageSize property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getPageSize() {
		return pageSize;
	}

	/**
	 * Sets the value of the pageSize property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setPageSize(BigInteger value) {
		this.pageSize = value;
	}

	/**
	 * Gets the value of the includePaymentRelation property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isIncludePaymentRelation() {
		return includePaymentRelation;
	}

	/**
	 * Sets the value of the includePaymentRelation property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setIncludePaymentRelation(Boolean value) {
		this.includePaymentRelation = value;
	}

	/**
	 * Gets the value of the includeMemberOffers property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isIncludeMemberOffers() {
		return includeMemberOffers;
	}

	/**
	 * Sets the value of the includeMemberOffers property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setIncludeMemberOffers(Boolean value) {
		this.includeMemberOffers = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
