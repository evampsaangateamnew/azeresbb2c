package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for FnFGroupInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="FnFGroupInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FnFGroupId" type="{http://crm.huawei.com/basetype/}FnFGroupId" minOccurs="0"/>
 *         &lt;element name="FnFGroupName" type="{http://crm.huawei.com/basetype/}FnFGroupName" minOccurs="0"/>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}PrimaryOfferingId" minOccurs="0"/>
 *         &lt;element name="FnFNumberType" type="{http://crm.huawei.com/basetype/}FnFNumberType" minOccurs="0"/>
 *         &lt;element name="FnFList" type="{http://crm.huawei.com/query/}FnFList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FnFGroupInfo", propOrder = { "fnFGroupId", "fnFGroupName", "offeringId", "fnFNumberType", "fnFList",
		"extParamList" })
public class FnFGroupInfo {
	@XmlElement(name = "FnFGroupId")
	protected String fnFGroupId;
	@XmlElement(name = "FnFGroupName")
	protected String fnFGroupName;
	@XmlElement(name = "OfferingId")
	protected String offeringId;
	@XmlElement(name = "FnFNumberType")
	protected String fnFNumberType;
	@XmlElement(name = "FnFList")
	protected FnFList fnFList;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the fnFGroupId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFnFGroupId() {
		return fnFGroupId;
	}

	/**
	 * Sets the value of the fnFGroupId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFnFGroupId(String value) {
		this.fnFGroupId = value;
	}

	/**
	 * Gets the value of the fnFGroupName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFnFGroupName() {
		return fnFGroupName;
	}

	/**
	 * Sets the value of the fnFGroupName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFnFGroupName(String value) {
		this.fnFGroupName = value;
	}

	/**
	 * Gets the value of the offeringId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * Sets the value of the offeringId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingId(String value) {
		this.offeringId = value;
	}

	/**
	 * Gets the value of the fnFNumberType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFnFNumberType() {
		return fnFNumberType;
	}

	/**
	 * Sets the value of the fnFNumberType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFnFNumberType(String value) {
		this.fnFNumberType = value;
	}

	/**
	 * Gets the value of the fnFList property.
	 * 
	 * @return possible object is {@link FnFList }
	 * 
	 */
	public FnFList getFnFList() {
		return fnFList;
	}

	/**
	 * Sets the value of the fnFList property.
	 * 
	 * @param value
	 *            allowed object is {@link FnFList }
	 * 
	 */
	public void setFnFList(FnFList value) {
		this.fnFList = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
