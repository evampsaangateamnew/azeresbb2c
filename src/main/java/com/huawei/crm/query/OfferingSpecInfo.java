package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for OfferingSpecInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingSpecInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingId"/>
 *         &lt;element name="OfferingName" type="{http://crm.huawei.com/basetype/}OfferingName"/>
 *         &lt;element name="OfferingShortName" type="{http://crm.huawei.com/basetype/}OfferingShortName"/>
 *         &lt;element name="OfferingCode" type="{http://crm.huawei.com/basetype/}OfferingCode"/>
 *         &lt;element name="OfferingCategory" type="{http://crm.huawei.com/basetype/}OfferingCategory"/>
 *         &lt;element name="OfferingSubCategory" type="{http://crm.huawei.com/basetype/}OfferingCategory"/>
 *         &lt;element name="PrimaryFlag" type="{http://crm.huawei.com/basetype/}PrimaryFlag"/>
 *         &lt;element name="BundleFlag" type="{http://crm.huawei.com/basetype/}BundleFlag"/>
 *         &lt;element name="NetworkType" type="{http://crm.huawei.com/basetype/}TeleType"/>
 *         &lt;element name="ObjType" type="{http://crm.huawei.com/basetype/}ObjType"/>
 *         &lt;element name="OwnerType" type="{http://crm.huawei.com/basetype/}OwnerType"/>
 *         &lt;element name="GroupFlag" type="{http://crm.huawei.com/basetype/}GroupFlag"/>
 *         &lt;element name="GroupType" type="{http://crm.huawei.com/basetype/}GroupType"/>
 *         &lt;element name="IsMultiTimes" type="{http://crm.huawei.com/basetype/}IsMultiTimes"/>
 *         &lt;element name="MaxCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PayMode" type="{http://crm.huawei.com/basetype/}PayMode"/>
 *         &lt;element name="PreloadFee" type="{http://crm.huawei.com/basetype/}PreloadFee"/>
 *         &lt;element name="Description" type="{http://crm.huawei.com/basetype/}Description" minOccurs="0"/>
 *         &lt;element name="EffectiveTime" type="{http://crm.huawei.com/basetype/}Time"/>
 *         &lt;element name="ExpiredTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}OfferingStatus"/>
 *         &lt;element name="IsSellSingly" type="{http://crm.huawei.com/basetype/}IsSellSingly"/>
 *         &lt;element name="BrandId" type="{http://crm.huawei.com/basetype/}BrandId" minOccurs="0"/>
 *         &lt;element name="BeId" type="{http://crm.huawei.com/basetype/}BeID"/>
 *         &lt;element name="Thumbnail" type="{http://crm.huawei.com/basetype/}Thumbnail" minOccurs="0"/>
 *         &lt;element name="FullImage" type="{http://crm.huawei.com/basetype/}FullImage" minOccurs="0"/>
 *         &lt;element name="OneTimeFee" type="{http://crm.huawei.com/basetype/}OneTimeFee"/>
 *         &lt;element name="MonthlyFee" type="{http://crm.huawei.com/basetype/}MonthlyFee"/>
 *         &lt;element name="ContractPeriod" type="{http://crm.huawei.com/basetype/}ContractPeriod" minOccurs="0"/>
 *         &lt;element name="Skus" type="{http://crm.huawei.com/query/}Skus" minOccurs="0"/>
 *         &lt;element name="Attrs" type="{http://crm.huawei.com/query/}Attrs" minOccurs="0"/>
 *         &lt;element name="Elements" type="{http://crm.huawei.com/query/}Elements" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingSpecInfo", propOrder = {})
public class OfferingSpecInfo {
	@XmlElement(name = "OfferingId", required = true)
	protected String offeringId;
	@XmlElement(name = "OfferingName", required = true)
	protected String offeringName;
	@XmlElement(name = "OfferingShortName", required = true)
	protected String offeringShortName;
	@XmlElement(name = "OfferingCode", required = true)
	protected String offeringCode;
	@XmlElement(name = "OfferingCategory", required = true)
	protected String offeringCategory;
	@XmlElement(name = "OfferingSubCategory", required = true)
	protected String offeringSubCategory;
	@XmlElement(name = "PrimaryFlag", required = true)
	protected String primaryFlag;
	@XmlElement(name = "BundleFlag", required = true)
	protected String bundleFlag;
	@XmlElement(name = "NetworkType", required = true)
	protected String networkType;
	@XmlElement(name = "ObjType", required = true)
	protected String objType;
	@XmlElement(name = "OwnerType", required = true)
	protected String ownerType;
	@XmlElement(name = "GroupFlag", required = true)
	protected String groupFlag;
	@XmlElement(name = "GroupType", required = true)
	protected String groupType;
	@XmlElement(name = "IsMultiTimes", required = true)
	protected String isMultiTimes;
	@XmlElement(name = "MaxCount")
	protected Integer maxCount;
	@XmlElement(name = "PayMode", required = true)
	protected String payMode;
	@XmlElement(name = "PreloadFee", required = true)
	protected String preloadFee;
	@XmlElement(name = "Description")
	protected String description;
	@XmlElement(name = "EffectiveTime", required = true)
	protected String effectiveTime;
	@XmlElement(name = "ExpiredTime")
	protected String expiredTime;
	@XmlElement(name = "Status", required = true)
	protected String status;
	@XmlElement(name = "IsSellSingly", required = true)
	protected String isSellSingly;
	@XmlElement(name = "BrandId")
	protected String brandId;
	@XmlElement(name = "BeId", required = true)
	protected String beId;
	@XmlElement(name = "Thumbnail")
	protected String thumbnail;
	@XmlElement(name = "FullImage")
	protected String fullImage;
	@XmlElement(name = "OneTimeFee", required = true)
	protected String oneTimeFee;
	@XmlElement(name = "MonthlyFee", required = true)
	protected String monthlyFee;
	@XmlElement(name = "ContractPeriod")
	protected String contractPeriod;
	@XmlElement(name = "Skus")
	protected Skus skus;
	@XmlElement(name = "Attrs")
	protected Attrs attrs;
	@XmlElement(name = "Elements")
	protected Elements elements;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the offeringId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * Sets the value of the offeringId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingId(String value) {
		this.offeringId = value;
	}

	/**
	 * Gets the value of the offeringName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingName() {
		return offeringName;
	}

	/**
	 * Sets the value of the offeringName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingName(String value) {
		this.offeringName = value;
	}

	/**
	 * Gets the value of the offeringShortName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingShortName() {
		return offeringShortName;
	}

	/**
	 * Sets the value of the offeringShortName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingShortName(String value) {
		this.offeringShortName = value;
	}

	/**
	 * Gets the value of the offeringCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingCode() {
		return offeringCode;
	}

	/**
	 * Sets the value of the offeringCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingCode(String value) {
		this.offeringCode = value;
	}

	/**
	 * Gets the value of the offeringCategory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingCategory() {
		return offeringCategory;
	}

	/**
	 * Sets the value of the offeringCategory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingCategory(String value) {
		this.offeringCategory = value;
	}

	/**
	 * Gets the value of the offeringSubCategory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingSubCategory() {
		return offeringSubCategory;
	}

	/**
	 * Sets the value of the offeringSubCategory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingSubCategory(String value) {
		this.offeringSubCategory = value;
	}

	/**
	 * Gets the value of the primaryFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrimaryFlag() {
		return primaryFlag;
	}

	/**
	 * Sets the value of the primaryFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrimaryFlag(String value) {
		this.primaryFlag = value;
	}

	/**
	 * Gets the value of the bundleFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBundleFlag() {
		return bundleFlag;
	}

	/**
	 * Sets the value of the bundleFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBundleFlag(String value) {
		this.bundleFlag = value;
	}

	/**
	 * Gets the value of the networkType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetworkType() {
		return networkType;
	}

	/**
	 * Sets the value of the networkType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetworkType(String value) {
		this.networkType = value;
	}

	/**
	 * Gets the value of the objType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getObjType() {
		return objType;
	}

	/**
	 * Sets the value of the objType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setObjType(String value) {
		this.objType = value;
	}

	/**
	 * Gets the value of the ownerType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOwnerType() {
		return ownerType;
	}

	/**
	 * Sets the value of the ownerType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOwnerType(String value) {
		this.ownerType = value;
	}

	/**
	 * Gets the value of the groupFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGroupFlag() {
		return groupFlag;
	}

	/**
	 * Sets the value of the groupFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGroupFlag(String value) {
		this.groupFlag = value;
	}

	/**
	 * Gets the value of the groupType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGroupType() {
		return groupType;
	}

	/**
	 * Sets the value of the groupType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGroupType(String value) {
		this.groupType = value;
	}

	/**
	 * Gets the value of the isMultiTimes property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIsMultiTimes() {
		return isMultiTimes;
	}

	/**
	 * Sets the value of the isMultiTimes property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIsMultiTimes(String value) {
		this.isMultiTimes = value;
	}

	/**
	 * Gets the value of the maxCount property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getMaxCount() {
		return maxCount;
	}

	/**
	 * Sets the value of the maxCount property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 * 
	 */
	public void setMaxCount(Integer value) {
		this.maxCount = value;
	}

	/**
	 * Gets the value of the payMode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayMode() {
		return payMode;
	}

	/**
	 * Sets the value of the payMode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayMode(String value) {
		this.payMode = value;
	}

	/**
	 * Gets the value of the preloadFee property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPreloadFee() {
		return preloadFee;
	}

	/**
	 * Sets the value of the preloadFee property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPreloadFee(String value) {
		this.preloadFee = value;
	}

	/**
	 * Gets the value of the description property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the description property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDescription(String value) {
		this.description = value;
	}

	/**
	 * Gets the value of the effectiveTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveTime() {
		return effectiveTime;
	}

	/**
	 * Sets the value of the effectiveTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveTime(String value) {
		this.effectiveTime = value;
	}

	/**
	 * Gets the value of the expiredTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpiredTime() {
		return expiredTime;
	}

	/**
	 * Sets the value of the expiredTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpiredTime(String value) {
		this.expiredTime = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the isSellSingly property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIsSellSingly() {
		return isSellSingly;
	}

	/**
	 * Sets the value of the isSellSingly property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIsSellSingly(String value) {
		this.isSellSingly = value;
	}

	/**
	 * Gets the value of the brandId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBrandId() {
		return brandId;
	}

	/**
	 * Sets the value of the brandId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBrandId(String value) {
		this.brandId = value;
	}

	/**
	 * Gets the value of the beId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBeId() {
		return beId;
	}

	/**
	 * Sets the value of the beId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBeId(String value) {
		this.beId = value;
	}

	/**
	 * Gets the value of the thumbnail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getThumbnail() {
		return thumbnail;
	}

	/**
	 * Sets the value of the thumbnail property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setThumbnail(String value) {
		this.thumbnail = value;
	}

	/**
	 * Gets the value of the fullImage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFullImage() {
		return fullImage;
	}

	/**
	 * Sets the value of the fullImage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFullImage(String value) {
		this.fullImage = value;
	}

	/**
	 * Gets the value of the oneTimeFee property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOneTimeFee() {
		return oneTimeFee;
	}

	/**
	 * Sets the value of the oneTimeFee property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOneTimeFee(String value) {
		this.oneTimeFee = value;
	}

	/**
	 * Gets the value of the monthlyFee property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMonthlyFee() {
		return monthlyFee;
	}

	/**
	 * Sets the value of the monthlyFee property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMonthlyFee(String value) {
		this.monthlyFee = value;
	}

	/**
	 * Gets the value of the contractPeriod property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContractPeriod() {
		return contractPeriod;
	}

	/**
	 * Sets the value of the contractPeriod property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContractPeriod(String value) {
		this.contractPeriod = value;
	}

	/**
	 * Gets the value of the skus property.
	 * 
	 * @return possible object is {@link Skus }
	 * 
	 */
	public Skus getSkus() {
		return skus;
	}

	/**
	 * Sets the value of the skus property.
	 * 
	 * @param value
	 *            allowed object is {@link Skus }
	 * 
	 */
	public void setSkus(Skus value) {
		this.skus = value;
	}

	/**
	 * Gets the value of the attrs property.
	 * 
	 * @return possible object is {@link Attrs }
	 * 
	 */
	public Attrs getAttrs() {
		return attrs;
	}

	/**
	 * Sets the value of the attrs property.
	 * 
	 * @param value
	 *            allowed object is {@link Attrs }
	 * 
	 */
	public void setAttrs(Attrs value) {
		this.attrs = value;
	}

	/**
	 * Gets the value of the elements property.
	 * 
	 * @return possible object is {@link Elements }
	 * 
	 */
	public Elements getElements() {
		return elements;
	}

	/**
	 * Sets the value of the elements property.
	 * 
	 * @param value
	 *            allowed object is {@link Elements }
	 * 
	 */
	public void setElements(Elements value) {
		this.elements = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
