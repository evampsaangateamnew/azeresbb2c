package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ResponseHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="CalcOneOffFeeBody" type="{http://crm.huawei.com/query/}CalcOneOffFeeList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "calcOneOffFeeBody" })
@XmlRootElement(name = "CalcOneOffFeeResponse")
public class CalcOneOffFeeResponse {
	@XmlElement(name = "ResponseHeader", required = true)
	protected ResponseHeader responseHeader;
	@XmlElement(name = "CalcOneOffFeeBody")
	protected CalcOneOffFeeList calcOneOffFeeBody;

	/**
	 * Gets the value of the responseHeader property.
	 * 
	 * @return possible object is {@link ResponseHeader }
	 * 
	 */
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	/**
	 * Sets the value of the responseHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseHeader }
	 * 
	 */
	public void setResponseHeader(ResponseHeader value) {
		this.responseHeader = value;
	}

	/**
	 * Gets the value of the calcOneOffFeeBody property.
	 * 
	 * @return possible object is {@link CalcOneOffFeeList }
	 * 
	 */
	public CalcOneOffFeeList getCalcOneOffFeeBody() {
		return calcOneOffFeeBody;
	}

	/**
	 * Sets the value of the calcOneOffFeeBody property.
	 * 
	 * @param value
	 *            allowed object is {@link CalcOneOffFeeList }
	 * 
	 */
	public void setCalcOneOffFeeBody(CalcOneOffFeeList value) {
		this.calcOneOffFeeBody = value;
	}
}
