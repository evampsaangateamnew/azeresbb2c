package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.RequestHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/basetype/}RequestHeader"/>
 *         &lt;element name="GetCustomerBody" type="{http://crm.huawei.com/query/}GetCustomerIn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "requestHeader", "getCustomerBody" })
@XmlRootElement(name = "GetCustomerRequest")
public class GetCustomerRequest {
	@Override
	public String toString() {
		return "GetCustomerRequest [requestHeader=" + requestHeader + ", getCustomerBody=" + getCustomerBody + "]";
	}

	@XmlElement(name = "RequestHeader", required = true)
	protected RequestHeader requestHeader;
	@XmlElement(name = "GetCustomerBody", required = true)
	protected GetCustomerIn getCustomerBody;

	public void setGetCustomerBody(GetCustomerIn getCustomerBody) {
		this.getCustomerBody = getCustomerBody;
	}

	/**
	 * Gets the value of the requestHeader property.
	 * 
	 * @return possible object is {@link RequestHeader }
	 * 
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * Sets the value of the requestHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RequestHeader }
	 * 
	 */
	public void setRequestHeader(RequestHeader value) {
		this.requestHeader = value;
	}

	/**
	 * Gets the value of the getCustomerBody property.
	 * 
	 * @return possible object is {@link GetCustomerIn }
	 * 
	 */
	public GetCustomerIn getGetCustomerBody() {
		return getCustomerBody;
	}

	/**
	 * Sets the value of the getCustomerBody property.
	 * 
	 * @param value
	 *            allowed object is {@link GetCustomerIn }
	 * 
	 */
	public void getCustomerRequest(GetCustomerIn value) {
		this.getCustomerBody = value;
	}
}
