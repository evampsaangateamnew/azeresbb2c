
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetTimeSchemaOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetTimeSchemaOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TimeSchemaList" type="{http://crm.huawei.com/query/}TimeSchemaList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTimeSchemaOut", propOrder = {

})
public class GetTimeSchemaOut {

    @XmlElement(name = "TimeSchemaList")
    protected TimeSchemaList timeSchemaList;

    /**
     * Gets the value of the timeSchemaList property.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchemaList }
     *     
     */
    public TimeSchemaList getTimeSchemaList() {
        return timeSchemaList;
    }

    /**
     * Sets the value of the timeSchemaList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchemaList }
     *     
     */
    public void setTimeSchemaList(TimeSchemaList value) {
        this.timeSchemaList = value;
    }

}
