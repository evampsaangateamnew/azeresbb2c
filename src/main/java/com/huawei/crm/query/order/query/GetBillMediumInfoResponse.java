
package com.huawei.crm.query.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ResponseHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="GetBillMediumInfoBody" type="{http://crm.huawei.com/query/}GetBillMediumInfoOut" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseHeader",
    "getBillMediumInfoBody"
})
@XmlRootElement(name = "GetBillMediumInfoResponse")
public class GetBillMediumInfoResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(name = "GetBillMediumInfoBody")
    protected List<GetBillMediumInfoOut> getBillMediumInfoBody;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the getBillMediumInfoBody property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getBillMediumInfoBody property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetBillMediumInfoBody().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetBillMediumInfoOut }
     * 
     * 
     */
    public List<GetBillMediumInfoOut> getGetBillMediumInfoBody() {
        if (getBillMediumInfoBody == null) {
            getBillMediumInfoBody = new ArrayList<GetBillMediumInfoOut>();
        }
        return this.getBillMediumInfoBody;
    }

}
