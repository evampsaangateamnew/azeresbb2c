
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.OfferingIdList;


/**
 * <p>Java class for GetOfferingSpecIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetOfferingSpecIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OfferingIdList" type="{http://crm.huawei.com/basetype/}OfferingIdList" minOccurs="0"/>
 *         &lt;element name="isGetAttrs" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isGetElements" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isGetSkus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isGetMemberTypes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetOfferingSpecIn", propOrder = {

})
public class GetOfferingSpecIn {

    @XmlElement(name = "OfferingIdList")
    protected OfferingIdList offeringIdList;
    protected boolean isGetAttrs;
    protected boolean isGetElements;
    protected boolean isGetSkus;
    protected boolean isGetMemberTypes;

    /**
     * Gets the value of the offeringIdList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingIdList }
     *     
     */
    public OfferingIdList getOfferingIdList() {
        return offeringIdList;
    }

    /**
     * Sets the value of the offeringIdList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingIdList }
     *     
     */
    public void setOfferingIdList(OfferingIdList value) {
        this.offeringIdList = value;
    }

    /**
     * Gets the value of the isGetAttrs property.
     * 
     */
    public boolean isIsGetAttrs() {
        return isGetAttrs;
    }

    /**
     * Sets the value of the isGetAttrs property.
     * 
     */
    public void setIsGetAttrs(boolean value) {
        this.isGetAttrs = value;
    }

    /**
     * Gets the value of the isGetElements property.
     * 
     */
    public boolean isIsGetElements() {
        return isGetElements;
    }

    /**
     * Sets the value of the isGetElements property.
     * 
     */
    public void setIsGetElements(boolean value) {
        this.isGetElements = value;
    }

    /**
     * Gets the value of the isGetSkus property.
     * 
     */
    public boolean isIsGetSkus() {
        return isGetSkus;
    }

    /**
     * Sets the value of the isGetSkus property.
     * 
     */
    public void setIsGetSkus(boolean value) {
        this.isGetSkus = value;
    }

    /**
     * Gets the value of the isGetMemberTypes property.
     * 
     */
    public boolean isIsGetMemberTypes() {
        return isGetMemberTypes;
    }

    /**
     * Sets the value of the isGetMemberTypes property.
     * 
     */
    public void setIsGetMemberTypes(boolean value) {
        this.isGetMemberTypes = value;
    }

}
