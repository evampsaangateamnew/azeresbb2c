
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidateOfferingRelationIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateOfferingRelationIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddOfferingList" type="{http://crm.huawei.com/query/}GetOfferingIdList" minOccurs="0"/>
 *         &lt;element name="DeleteOfferingList" type="{http://crm.huawei.com/query/}GetOfferingIdList" minOccurs="0"/>
 *         &lt;element name="RemainOfferingList" type="{http://crm.huawei.com/query/}GetOfferingIdList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateOfferingRelationIn", propOrder = {
    "addOfferingList",
    "deleteOfferingList",
    "remainOfferingList"
})
public class ValidateOfferingRelationIn {

    @XmlElement(name = "AddOfferingList")
    protected GetOfferingIdList addOfferingList;
    @XmlElement(name = "DeleteOfferingList")
    protected GetOfferingIdList deleteOfferingList;
    @XmlElement(name = "RemainOfferingList")
    protected GetOfferingIdList remainOfferingList;

    /**
     * Gets the value of the addOfferingList property.
     * 
     * @return
     *     possible object is
     *     {@link GetOfferingIdList }
     *     
     */
    public GetOfferingIdList getAddOfferingList() {
        return addOfferingList;
    }

    /**
     * Sets the value of the addOfferingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetOfferingIdList }
     *     
     */
    public void setAddOfferingList(GetOfferingIdList value) {
        this.addOfferingList = value;
    }

    /**
     * Gets the value of the deleteOfferingList property.
     * 
     * @return
     *     possible object is
     *     {@link GetOfferingIdList }
     *     
     */
    public GetOfferingIdList getDeleteOfferingList() {
        return deleteOfferingList;
    }

    /**
     * Sets the value of the deleteOfferingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetOfferingIdList }
     *     
     */
    public void setDeleteOfferingList(GetOfferingIdList value) {
        this.deleteOfferingList = value;
    }

    /**
     * Gets the value of the remainOfferingList property.
     * 
     * @return
     *     possible object is
     *     {@link GetOfferingIdList }
     *     
     */
    public GetOfferingIdList getRemainOfferingList() {
        return remainOfferingList;
    }

    /**
     * Sets the value of the remainOfferingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetOfferingIdList }
     *     
     */
    public void setRemainOfferingList(GetOfferingIdList value) {
        this.remainOfferingList = value;
    }

}
