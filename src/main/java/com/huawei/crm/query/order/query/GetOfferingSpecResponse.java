
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ResponseHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="GetOfferingSpecBody" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="OfferingSpecList" type="{http://crm.huawei.com/query/}GetOfferingSpecList" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseHeader",
    "getOfferingSpecBody"
})
@XmlRootElement(name = "GetOfferingSpecResponse")
public class GetOfferingSpecResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(name = "GetOfferingSpecBody")
    protected GetOfferingSpecResponse.GetOfferingSpecBody getOfferingSpecBody;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the getOfferingSpecBody property.
     * 
     * @return
     *     possible object is
     *     {@link GetOfferingSpecResponse.GetOfferingSpecBody }
     *     
     */
    public GetOfferingSpecResponse.GetOfferingSpecBody getGetOfferingSpecBody() {
        return getOfferingSpecBody;
    }

    /**
     * Sets the value of the getOfferingSpecBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetOfferingSpecResponse.GetOfferingSpecBody }
     *     
     */
    public void setGetOfferingSpecBody(GetOfferingSpecResponse.GetOfferingSpecBody value) {
        this.getOfferingSpecBody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="OfferingSpecList" type="{http://crm.huawei.com/query/}GetOfferingSpecList" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class GetOfferingSpecBody {

        @XmlElement(name = "OfferingSpecList")
        protected GetOfferingSpecList offeringSpecList;

        /**
         * Gets the value of the offeringSpecList property.
         * 
         * @return
         *     possible object is
         *     {@link GetOfferingSpecList }
         *     
         */
        public GetOfferingSpecList getOfferingSpecList() {
            return offeringSpecList;
        }

        /**
         * Sets the value of the offeringSpecList property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetOfferingSpecList }
         *     
         */
        public void setOfferingSpecList(GetOfferingSpecList value) {
            this.offeringSpecList = value;
        }

    }

}
