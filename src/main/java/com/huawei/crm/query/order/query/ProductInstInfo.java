
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductInstInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductInstInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductID" type="{http://crm.huawei.com/basetype/}ProductId" minOccurs="0"/>
 *         &lt;element name="ProductCode" type="{http://crm.huawei.com/basetype/}ProductCode" minOccurs="0"/>
 *         &lt;element name="HomeZoneList" type="{http://crm.huawei.com/query/}HomeZoneList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductInstInfo", propOrder = {
    "productID",
    "productCode",
    "homeZoneList"
})
public class ProductInstInfo {

    @XmlElement(name = "ProductID")
    protected String productID;
    @XmlElement(name = "ProductCode")
    protected String productCode;
    @XmlElement(name = "HomeZoneList")
    protected HomeZoneList homeZoneList;

    /**
     * Gets the value of the productID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductID(String value) {
        this.productID = value;
    }

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the homeZoneList property.
     * 
     * @return
     *     possible object is
     *     {@link HomeZoneList }
     *     
     */
    public HomeZoneList getHomeZoneList() {
        return homeZoneList;
    }

    /**
     * Sets the value of the homeZoneList property.
     * 
     * @param value
     *     allowed object is
     *     {@link HomeZoneList }
     *     
     */
    public void setHomeZoneList(HomeZoneList value) {
        this.homeZoneList = value;
    }

}
