
package com.huawei.crm.query.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSpecialOfferingList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSpecialOfferingList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSpecialOfferingInfo" type="{http://crm.huawei.com/query/}GetSpecialOfferingInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSpecialOfferingList", propOrder = {
    "getSpecialOfferingInfo"
})
public class GetSpecialOfferingList {

    @XmlElement(name = "GetSpecialOfferingInfo", required = true)
    protected List<GetSpecialOfferingInfo> getSpecialOfferingInfo;

    /**
     * Gets the value of the getSpecialOfferingInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getSpecialOfferingInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetSpecialOfferingInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetSpecialOfferingInfo }
     * 
     * 
     */
    public List<GetSpecialOfferingInfo> getGetSpecialOfferingInfo() {
        if (getSpecialOfferingInfo == null) {
            getSpecialOfferingInfo = new ArrayList<GetSpecialOfferingInfo>();
        }
        return this.getSpecialOfferingInfo;
    }

}
