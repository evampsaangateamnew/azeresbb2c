
package com.huawei.crm.query.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FnFGroupList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FnFGroupList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFnFGroupListInfo" type="{http://crm.huawei.com/query/}FnFGroupInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FnFGroupList", propOrder = {
    "getFnFGroupListInfo"
})
public class FnFGroupList {

    @XmlElement(name = "GetFnFGroupListInfo")
    protected List<FnFGroupInfo> getFnFGroupListInfo;

    /**
     * Gets the value of the getFnFGroupListInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getFnFGroupListInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetFnFGroupListInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FnFGroupInfo }
     * 
     * 
     */
    public List<FnFGroupInfo> getGetFnFGroupListInfo() {
        if (getFnFGroupListInfo == null) {
            getFnFGroupListInfo = new ArrayList<FnFGroupInfo>();
        }
        return this.getFnFGroupListInfo;
    }

}
