
package com.huawei.crm.query.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.GetSubProductInfo;


/**
 * <p>Java class for GetNetworkSettingDataList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetNetworkSettingDataList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetNetworkSettingDataList" type="{http://crm.huawei.com/basetype/}GetSubProductInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetNetworkSettingDataList", propOrder = {
    "getNetworkSettingDataList"
})
public class GetNetworkSettingDataList {

    @XmlElement(name = "GetNetworkSettingDataList")
    protected List<GetSubProductInfo> getNetworkSettingDataList;

    /**
     * Gets the value of the getNetworkSettingDataList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getNetworkSettingDataList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetNetworkSettingDataList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetSubProductInfo }
     * 
     * 
     */
    public List<GetSubProductInfo> getGetNetworkSettingDataList() {
        if (getNetworkSettingDataList == null) {
            getNetworkSettingDataList = new ArrayList<GetSubProductInfo>();
        }
        return this.getNetworkSettingDataList;
    }

}
