
package com.huawei.crm.query.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CallScreenNumList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallScreenNumList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCallScreenListInfo" type="{http://crm.huawei.com/query/}CallScreenNumberInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallScreenNumList", propOrder = {
    "getCallScreenListInfo"
})
public class CallScreenNumList {

    @XmlElement(name = "GetCallScreenListInfo")
    protected List<CallScreenNumberInfo> getCallScreenListInfo;

    /**
     * Gets the value of the getCallScreenListInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCallScreenListInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCallScreenListInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallScreenNumberInfo }
     * 
     * 
     */
    public List<CallScreenNumberInfo> getGetCallScreenListInfo() {
        if (getCallScreenListInfo == null) {
            getCallScreenListInfo = new ArrayList<CallScreenNumberInfo>();
        }
        return this.getCallScreenListInfo;
    }

}
