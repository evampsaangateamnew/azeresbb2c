
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.HistoryList;


/**
 * <p>Java class for GetCustomerHistoryOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomerHistoryOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalCustomerId" type="{http://crm.huawei.com/basetype/}ExternalCustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="TotalRowNum" type="{http://crm.huawei.com/basetype/}TotalRowNum"/>
 *         &lt;element name="BeginRowNum" type="{http://crm.huawei.com/basetype/}BeginRowNum"/>
 *         &lt;element name="FetchRowNum" type="{http://crm.huawei.com/basetype/}FetchRowNum"/>
 *         &lt;element name="HistoryInfoList" type="{http://crm.huawei.com/basetype/}HistoryList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomerHistoryOut", propOrder = {

})
public class GetCustomerHistoryOut {

    @XmlElement(name = "ExternalCustomerId")
    protected String externalCustomerId;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "TotalRowNum")
    protected long totalRowNum;
    @XmlElement(name = "BeginRowNum")
    protected long beginRowNum;
    @XmlElement(name = "FetchRowNum")
    protected long fetchRowNum;
    @XmlElement(name = "HistoryInfoList")
    protected HistoryList historyInfoList;

    /**
     * Gets the value of the externalCustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalCustomerId() {
        return externalCustomerId;
    }

    /**
     * Sets the value of the externalCustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalCustomerId(String value) {
        this.externalCustomerId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the totalRowNum property.
     * 
     */
    public long getTotalRowNum() {
        return totalRowNum;
    }

    /**
     * Sets the value of the totalRowNum property.
     * 
     */
    public void setTotalRowNum(long value) {
        this.totalRowNum = value;
    }

    /**
     * Gets the value of the beginRowNum property.
     * 
     */
    public long getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Sets the value of the beginRowNum property.
     * 
     */
    public void setBeginRowNum(long value) {
        this.beginRowNum = value;
    }

    /**
     * Gets the value of the fetchRowNum property.
     * 
     */
    public long getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Sets the value of the fetchRowNum property.
     * 
     */
    public void setFetchRowNum(long value) {
        this.fetchRowNum = value;
    }

    /**
     * Gets the value of the historyInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link HistoryList }
     *     
     */
    public HistoryList getHistoryInfoList() {
        return historyInfoList;
    }

    /**
     * Sets the value of the historyInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoryList }
     *     
     */
    public void setHistoryInfoList(HistoryList value) {
        this.historyInfoList = value;
    }

}
