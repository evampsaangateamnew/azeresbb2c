
package com.huawei.crm.query.order.query;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.SubInstallmentInfo;


/**
 * <p>Java class for SubRscInstallmentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubRscInstallmentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="ResourceSeq" type="{http://crm.huawei.com/basetype/}ResourceSeq" minOccurs="0"/>
 *         &lt;element name="ResourceType" type="{http://crm.huawei.com/basetype/}ResourceType" minOccurs="0"/>
 *         &lt;element name="ResourceModel" type="{http://crm.huawei.com/basetype/}ResourceModel64" minOccurs="0"/>
 *         &lt;element name="ResourceCode" type="{http://crm.huawei.com/basetype/}ResourceCode" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://crm.huawei.com/basetype/}int" minOccurs="0"/>
 *         &lt;element name="SalePrice" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="Program" type="{http://crm.huawei.com/basetype/}InstallmentProgram" minOccurs="0"/>
 *         &lt;element name="SaleDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="OperatorId" type="{http://crm.huawei.com/basetype/}OperatorId" minOccurs="0"/>
 *         &lt;element name="ResourceStatus" type="{http://crm.huawei.com/basetype/}ResourceStatus2" minOccurs="0"/>
 *         &lt;element name="SubInstallmentList" type="{http://crm.huawei.com/basetype/}SubInstallmentInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubRscInstallmentInfo", propOrder = {
    "subscriberId",
    "serviceNumber",
    "resourceSeq",
    "resourceType",
    "resourceModel",
    "resourceCode",
    "quantity",
    "salePrice",
    "program",
    "saleDate",
    "operatorId",
    "resourceStatus",
    "subInstallmentList"
})
public class SubRscInstallmentInfo {

    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "ServiceNumber")
    protected String serviceNumber;
    @XmlElement(name = "ResourceSeq")
    protected String resourceSeq;
    @XmlElement(name = "ResourceType")
    protected String resourceType;
    @XmlElement(name = "ResourceModel")
    protected String resourceModel;
    @XmlElement(name = "ResourceCode")
    protected String resourceCode;
    @XmlElement(name = "Quantity")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger quantity;
    @XmlElement(name = "SalePrice")
    protected Long salePrice;
    @XmlElement(name = "Program")
    protected String program;
    @XmlElement(name = "SaleDate")
    protected String saleDate;
    @XmlElement(name = "OperatorId")
    protected String operatorId;
    @XmlElement(name = "ResourceStatus")
    protected String resourceStatus;
    @XmlElement(name = "SubInstallmentList")
    protected List<SubInstallmentInfo> subInstallmentList;

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the resourceSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceSeq() {
        return resourceSeq;
    }

    /**
     * Sets the value of the resourceSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceSeq(String value) {
        this.resourceSeq = value;
    }

    /**
     * Gets the value of the resourceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * Sets the value of the resourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceType(String value) {
        this.resourceType = value;
    }

    /**
     * Gets the value of the resourceModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceModel() {
        return resourceModel;
    }

    /**
     * Sets the value of the resourceModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceModel(String value) {
        this.resourceModel = value;
    }

    /**
     * Gets the value of the resourceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceCode() {
        return resourceCode;
    }

    /**
     * Sets the value of the resourceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceCode(String value) {
        this.resourceCode = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the salePrice property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSalePrice() {
        return salePrice;
    }

    /**
     * Sets the value of the salePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSalePrice(Long value) {
        this.salePrice = value;
    }

    /**
     * Gets the value of the program property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgram() {
        return program;
    }

    /**
     * Sets the value of the program property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgram(String value) {
        this.program = value;
    }

    /**
     * Gets the value of the saleDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaleDate() {
        return saleDate;
    }

    /**
     * Sets the value of the saleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaleDate(String value) {
        this.saleDate = value;
    }

    /**
     * Gets the value of the operatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorId() {
        return operatorId;
    }

    /**
     * Sets the value of the operatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorId(String value) {
        this.operatorId = value;
    }

    /**
     * Gets the value of the resourceStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceStatus() {
        return resourceStatus;
    }

    /**
     * Sets the value of the resourceStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceStatus(String value) {
        this.resourceStatus = value;
    }

    /**
     * Gets the value of the subInstallmentList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subInstallmentList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubInstallmentList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubInstallmentInfo }
     * 
     * 
     */
    public List<SubInstallmentInfo> getSubInstallmentList() {
        if (subInstallmentList == null) {
            subInstallmentList = new ArrayList<SubInstallmentInfo>();
        }
        return this.subInstallmentList;
    }

}
