
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetBillMediumInfoOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetBillMediumInfoOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillMediumId" type="{http://crm.huawei.com/basetype/}BillMediumId"/>
 *         &lt;element name="BillMediumName" type="{http://crm.huawei.com/query/}BillMediumName" minOccurs="0"/>
 *         &lt;element name="MediumTypeId" type="{http://crm.huawei.com/basetype/}MediumType"/>
 *         &lt;element name="MediumTypeName" type="{http://crm.huawei.com/query/}MediumTypeName" minOccurs="0"/>
 *         &lt;element name="MediumCarrier" type="{http://crm.huawei.com/query/}MediumCarrier" minOccurs="0"/>
 *         &lt;element name="ContentMode" type="{http://crm.huawei.com/basetype/}ContentMode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetBillMediumInfoOut", propOrder = {
    "billMediumId",
    "billMediumName",
    "mediumTypeId",
    "mediumTypeName",
    "mediumCarrier",
    "contentMode"
})
public class GetBillMediumInfoOut {

    @XmlElement(name = "BillMediumId", required = true)
    protected String billMediumId;
    @XmlElement(name = "BillMediumName")
    protected String billMediumName;
    @XmlElement(name = "MediumTypeId", required = true)
    protected String mediumTypeId;
    @XmlElement(name = "MediumTypeName")
    protected String mediumTypeName;
    @XmlElement(name = "MediumCarrier")
    protected String mediumCarrier;
    @XmlElement(name = "ContentMode")
    protected String contentMode;

    /**
     * Gets the value of the billMediumId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillMediumId() {
        return billMediumId;
    }

    /**
     * Sets the value of the billMediumId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillMediumId(String value) {
        this.billMediumId = value;
    }

    /**
     * Gets the value of the billMediumName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillMediumName() {
        return billMediumName;
    }

    /**
     * Sets the value of the billMediumName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillMediumName(String value) {
        this.billMediumName = value;
    }

    /**
     * Gets the value of the mediumTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediumTypeId() {
        return mediumTypeId;
    }

    /**
     * Sets the value of the mediumTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediumTypeId(String value) {
        this.mediumTypeId = value;
    }

    /**
     * Gets the value of the mediumTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediumTypeName() {
        return mediumTypeName;
    }

    /**
     * Sets the value of the mediumTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediumTypeName(String value) {
        this.mediumTypeName = value;
    }

    /**
     * Gets the value of the mediumCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediumCarrier() {
        return mediumCarrier;
    }

    /**
     * Sets the value of the mediumCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediumCarrier(String value) {
        this.mediumCarrier = value;
    }

    /**
     * Gets the value of the contentMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentMode() {
        return contentMode;
    }

    /**
     * Sets the value of the contentMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentMode(String value) {
        this.contentMode = value;
    }

}
