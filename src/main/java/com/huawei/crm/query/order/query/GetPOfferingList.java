
package com.huawei.crm.query.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.GetOfferingInfo;


/**
 * <p>Java class for GetPOfferingList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPOfferingList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOfferingInfo" type="{http://crm.huawei.com/basetype/}GetOfferingInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPOfferingList", propOrder = {
    "getOfferingInfo"
})
public class GetPOfferingList {

    @XmlElement(name = "GetOfferingInfo", required = true)
    protected List<GetOfferingInfo> getOfferingInfo;

    /**
     * Gets the value of the getOfferingInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getOfferingInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetOfferingInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetOfferingInfo }
     * 
     * 
     */
    public List<GetOfferingInfo> getGetOfferingInfo() {
        if (getOfferingInfo == null) {
            getOfferingInfo = new ArrayList<GetOfferingInfo>();
        }
        return this.getOfferingInfo;
    }

}
