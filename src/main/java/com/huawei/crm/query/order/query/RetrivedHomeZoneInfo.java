
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetrivedHomeZoneInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrivedHomeZoneInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingInfo" type="{http://crm.huawei.com/query/}OfferingInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrivedHomeZoneInfo", propOrder = {
    "offeringInfo"
})
public class RetrivedHomeZoneInfo {

    @XmlElement(name = "OfferingInfo")
    protected OfferingInfo offeringInfo;

    /**
     * Gets the value of the offeringInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingInfo }
     *     
     */
    public OfferingInfo getOfferingInfo() {
        return offeringInfo;
    }

    /**
     * Sets the value of the offeringInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingInfo }
     *     
     */
    public void setOfferingInfo(OfferingInfo value) {
        this.offeringInfo = value;
    }

}
