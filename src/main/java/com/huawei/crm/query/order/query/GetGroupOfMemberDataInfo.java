
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ExtParameterList;
import com.huawei.crm.basetype.order.query.GetSubOfferingInfo;


/**
 * <p>Java class for GetGroupOfMemberDataInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupOfMemberDataInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="GroupId" type="{http://crm.huawei.com/basetype/}GroupId" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="GroupName" type="{http://crm.huawei.com/basetype/}GroupName" minOccurs="0"/>
 *         &lt;element name="GroupNo" type="{http://crm.huawei.com/basetype/}GroupNo" minOccurs="0"/>
 *         &lt;element name="GroupStatus" type="{http://crm.huawei.com/basetype/}GroupStatus" minOccurs="0"/>
 *         &lt;element name="PrimaryOffering" type="{http://crm.huawei.com/basetype/}GetSubOfferingInfo" minOccurs="0"/>
 *         &lt;element name="BeID" type="{http://crm.huawei.com/basetype/}BeID" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpireDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="MemberAmount" type="{http://crm.huawei.com/basetype/}MemberAmount" minOccurs="0"/>
 *         &lt;element name="GroupMembers" type="{http://crm.huawei.com/query/}MemberSubscriberList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupOfMemberDataInfo", propOrder = {

})
public class GetGroupOfMemberDataInfo {

    @XmlElement(name = "GroupId")
    protected Long groupId;
    @XmlElement(name = "AccountId")
    protected Long accountId;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "GroupName")
    protected String groupName;
    @XmlElement(name = "GroupNo")
    protected String groupNo;
    @XmlElement(name = "GroupStatus")
    protected String groupStatus;
    @XmlElement(name = "PrimaryOffering")
    protected GetSubOfferingInfo primaryOffering;
    @XmlElement(name = "BeID")
    protected String beID;
    @XmlElement(name = "EffectiveDate")
    protected String effectiveDate;
    @XmlElement(name = "ExpireDate")
    protected String expireDate;
    @XmlElement(name = "MemberAmount")
    protected String memberAmount;
    @XmlElement(name = "GroupMembers")
    protected MemberSubscriberList groupMembers;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the groupId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGroupId(Long value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountId(Long value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the groupNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupNo() {
        return groupNo;
    }

    /**
     * Sets the value of the groupNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupNo(String value) {
        this.groupNo = value;
    }

    /**
     * Gets the value of the groupStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupStatus() {
        return groupStatus;
    }

    /**
     * Sets the value of the groupStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupStatus(String value) {
        this.groupStatus = value;
    }

    /**
     * Gets the value of the primaryOffering property.
     * 
     * @return
     *     possible object is
     *     {@link GetSubOfferingInfo }
     *     
     */
    public GetSubOfferingInfo getPrimaryOffering() {
        return primaryOffering;
    }

    /**
     * Sets the value of the primaryOffering property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSubOfferingInfo }
     *     
     */
    public void setPrimaryOffering(GetSubOfferingInfo value) {
        this.primaryOffering = value;
    }

    /**
     * Gets the value of the beID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeID() {
        return beID;
    }

    /**
     * Sets the value of the beID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeID(String value) {
        this.beID = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expireDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpireDate() {
        return expireDate;
    }

    /**
     * Sets the value of the expireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpireDate(String value) {
        this.expireDate = value;
    }

    /**
     * Gets the value of the memberAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberAmount() {
        return memberAmount;
    }

    /**
     * Sets the value of the memberAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberAmount(String value) {
        this.memberAmount = value;
    }

    /**
     * Gets the value of the groupMembers property.
     * 
     * @return
     *     possible object is
     *     {@link MemberSubscriberList }
     *     
     */
    public MemberSubscriberList getGroupMembers() {
        return groupMembers;
    }

    /**
     * Sets the value of the groupMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link MemberSubscriberList }
     *     
     */
    public void setGroupMembers(MemberSubscriberList value) {
        this.groupMembers = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
