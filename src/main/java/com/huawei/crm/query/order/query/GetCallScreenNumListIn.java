
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.CSType;
import com.huawei.crm.basetype.order.query.ExtParameterList;


/**
 * <p>Java class for GetCallScreenNumListIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCallScreenNumListIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="CSType" type="{http://crm.huawei.com/basetype/}CSType" minOccurs="0"/>
 *         &lt;element name="BeginRowNum" type="{http://crm.huawei.com/basetype/}BeginRowNum" minOccurs="0"/>
 *         &lt;element name="FetchRowNum" type="{http://crm.huawei.com/basetype/}FetchRowNum" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://crm.huawei.com/basetype/}Category" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCallScreenNumListIn", propOrder = {

})
public class GetCallScreenNumListIn {

    @XmlElement(name = "ServiceNumber")
    protected String serviceNumber;
    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "CSType")
    @XmlSchemaType(name = "string")
    protected CSType csType;
    @XmlElement(name = "BeginRowNum")
    protected Long beginRowNum;
    @XmlElement(name = "FetchRowNum")
    protected Long fetchRowNum;
    @XmlElement(name = "Category")
    protected String category;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the csType property.
     * 
     * @return
     *     possible object is
     *     {@link CSType }
     *     
     */
    public CSType getCSType() {
        return csType;
    }

    /**
     * Sets the value of the csType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CSType }
     *     
     */
    public void setCSType(CSType value) {
        this.csType = value;
    }

    /**
     * Gets the value of the beginRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Sets the value of the beginRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBeginRowNum(Long value) {
        this.beginRowNum = value;
    }

    /**
     * Gets the value of the fetchRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Sets the value of the fetchRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFetchRowNum(Long value) {
        this.fetchRowNum = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
