
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.PasswordInfo;


/**
 * <p>Java class for CheckPasswordIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CheckPasswordIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalCustomerId" type="{http://crm.huawei.com/basetype/}ExternalCustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="ExternalSubscriberId" type="{http://crm.huawei.com/basetype/}ExternalSubscriberId" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="PasswordInfo" type="{http://crm.huawei.com/basetype/}PasswordInfo"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckPasswordIn", propOrder = {

})
public class CheckPasswordIn {

    @XmlElement(name = "ExternalCustomerId")
    protected String externalCustomerId;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "ExternalSubscriberId")
    protected String externalSubscriberId;
    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "ServiceNumber")
    protected String serviceNumber;
    @XmlElement(name = "PasswordInfo", required = true)
    protected PasswordInfo passwordInfo;

    /**
     * Gets the value of the externalCustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalCustomerId() {
        return externalCustomerId;
    }

    /**
     * Sets the value of the externalCustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalCustomerId(String value) {
        this.externalCustomerId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the externalSubscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSubscriberId() {
        return externalSubscriberId;
    }

    /**
     * Sets the value of the externalSubscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSubscriberId(String value) {
        this.externalSubscriberId = value;
    }

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the passwordInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PasswordInfo }
     *     
     */
    public PasswordInfo getPasswordInfo() {
        return passwordInfo;
    }

    /**
     * Sets the value of the passwordInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordInfo }
     *     
     */
    public void setPasswordInfo(PasswordInfo value) {
        this.passwordInfo = value;
    }

}
