
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ExtParameterList;


/**
 * <p>Java class for RetrivedScreenNumInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrivedScreenNumInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TotalRowNum" type="{http://crm.huawei.com/basetype/}TotalRowNum"/>
 *         &lt;element name="CallScreenList" type="{http://crm.huawei.com/query/}CallScreenNumList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrivedScreenNumInfo", propOrder = {
    "totalRowNum",
    "callScreenList",
    "extParamList"
})
public class RetrivedScreenNumInfo {

    @XmlElement(name = "TotalRowNum")
    protected long totalRowNum;
    @XmlElement(name = "CallScreenList")
    protected CallScreenNumList callScreenList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the totalRowNum property.
     * 
     */
    public long getTotalRowNum() {
        return totalRowNum;
    }

    /**
     * Sets the value of the totalRowNum property.
     * 
     */
    public void setTotalRowNum(long value) {
        this.totalRowNum = value;
    }

    /**
     * Gets the value of the callScreenList property.
     * 
     * @return
     *     possible object is
     *     {@link CallScreenNumList }
     *     
     */
    public CallScreenNumList getCallScreenList() {
        return callScreenList;
    }

    /**
     * Sets the value of the callScreenList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CallScreenNumList }
     *     
     */
    public void setCallScreenList(CallScreenNumList value) {
        this.callScreenList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
