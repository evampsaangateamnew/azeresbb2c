
package com.huawei.crm.query.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryCustomerOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryCustomerOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerAbstractInfo" type="{http://crm.huawei.com/query/}CustomerAbstractInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryCustomerOut", propOrder = {
    "customerAbstractInfo"
})
public class QueryCustomerOut {

    @XmlElement(name = "CustomerAbstractInfo")
    protected List<CustomerAbstractInfo> customerAbstractInfo;

    /**
     * Gets the value of the customerAbstractInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customerAbstractInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomerAbstractInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerAbstractInfo }
     * 
     * 
     */
    public List<CustomerAbstractInfo> getCustomerAbstractInfo() {
        if (customerAbstractInfo == null) {
            customerAbstractInfo = new ArrayList<CustomerAbstractInfo>();
        }
        return this.customerAbstractInfo;
    }

}
