
package com.huawei.crm.query.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ResponseHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="GetSubRscInstallmentBody" type="{http://crm.huawei.com/query/}GetSubRscInstallmentOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseHeader",
    "getSubRscInstallmentBody"
})
@XmlRootElement(name = "GetSubRscInstallmentResponse")
public class GetSubRscInstallmentResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(name = "GetSubRscInstallmentBody")
    protected GetSubRscInstallmentOut getSubRscInstallmentBody;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the getSubRscInstallmentBody property.
     * 
     * @return
     *     possible object is
     *     {@link GetSubRscInstallmentOut }
     *     
     */
    public GetSubRscInstallmentOut getGetSubRscInstallmentBody() {
        return getSubRscInstallmentBody;
    }

    /**
     * Sets the value of the getSubRscInstallmentBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSubRscInstallmentOut }
     *     
     */
    public void setGetSubRscInstallmentBody(GetSubRscInstallmentOut value) {
        this.getSubRscInstallmentBody = value;
    }

}
