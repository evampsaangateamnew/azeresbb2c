package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for CalcOneOffFeeIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CalcOneOffFeeIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="BusinessCode" type="{http://crm.huawei.com/basetype/}BusinessCode"/>
 *         &lt;element name="Factor" type="{http://crm.huawei.com/query/}FactorInfo" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalcOneOffFeeIn", propOrder = {})
public class CalcOneOffFeeIn {
	@XmlElement(name = "BusinessCode", required = true)
	protected String businessCode;
	@XmlElement(name = "Factor")
	protected FactorInfo factor;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the businessCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBusinessCode() {
		return businessCode;
	}

	/**
	 * Sets the value of the businessCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBusinessCode(String value) {
		this.businessCode = value;
	}

	/**
	 * Gets the value of the factor property.
	 * 
	 * @return possible object is {@link FactorInfo }
	 * 
	 */
	public FactorInfo getFactor() {
		return factor;
	}

	/**
	 * Sets the value of the factor property.
	 * 
	 * @param value
	 *            allowed object is {@link FactorInfo }
	 * 
	 */
	public void setFactor(FactorInfo value) {
		this.factor = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
