package com.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.AccountInfo;
import com.huawei.crm.basetype.DealerInfo;
import com.huawei.crm.basetype.GroupSubscriberInfo;
import com.huawei.crm.basetype.RegisterCustInfo;

/**
 * <p>
 * Java class for CheckPasswordOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CheckPasswordOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Customer" type="{http://crm.huawei.com/basetype/}RegisterCustInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Account" type="{http://crm.huawei.com/basetype/}AccountInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GroupSubscriber" type="{http://crm.huawei.com/basetype/}GroupSubscriberInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Dealer" type="{http://crm.huawei.com/basetype/}DealerInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckPasswordOut", propOrder = { "customer", "account", "groupSubscriber", "dealer" })
public class CheckPasswordOut {
	@XmlElement(name = "Customer")
	protected List<RegisterCustInfo> customer;
	@XmlElement(name = "Account")
	protected List<AccountInfo> account;
	@XmlElement(name = "GroupSubscriber")
	protected List<GroupSubscriberInfo> groupSubscriber;
	@XmlElement(name = "Dealer")
	protected List<DealerInfo> dealer;

	/**
	 * Gets the value of the customer property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the customer property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCustomer().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link RegisterCustInfo }
	 * 
	 * 
	 */
	public List<RegisterCustInfo> getCustomer() {
		if (customer == null) {
			customer = new ArrayList<RegisterCustInfo>();
		}
		return this.customer;
	}

	/**
	 * Gets the value of the account property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the account property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAccount().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AccountInfo }
	 * 
	 * 
	 */
	public List<AccountInfo> getAccount() {
		if (account == null) {
			account = new ArrayList<AccountInfo>();
		}
		return this.account;
	}

	/**
	 * Gets the value of the groupSubscriber property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the groupSubscriber property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGroupSubscriber().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link GroupSubscriberInfo }
	 * 
	 * 
	 */
	public List<GroupSubscriberInfo> getGroupSubscriber() {
		if (groupSubscriber == null) {
			groupSubscriber = new ArrayList<GroupSubscriberInfo>();
		}
		return this.groupSubscriber;
	}

	/**
	 * Gets the value of the dealer property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the dealer property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getDealer().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link DealerInfo }
	 * 
	 * 
	 */
	public List<DealerInfo> getDealer() {
		if (dealer == null) {
			dealer = new ArrayList<DealerInfo>();
		}
		return this.dealer;
	}
}
