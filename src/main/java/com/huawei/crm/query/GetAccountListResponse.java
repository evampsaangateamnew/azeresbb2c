package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ResponseHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="GetAccountBody" type="{http://crm.huawei.com/query/}GetAccountListOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "getAccountBody" })
@XmlRootElement(name = "GetAccountListResponse")
public class GetAccountListResponse {
	@XmlElement(name = "ResponseHeader", required = true)
	protected ResponseHeader responseHeader;
	@XmlElement(name = "GetAccountBody")
	protected GetAccountListOut getAccountBody;

	/**
	 * Gets the value of the responseHeader property.
	 * 
	 * @return possible object is {@link ResponseHeader }
	 * 
	 */
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	/**
	 * Sets the value of the responseHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseHeader }
	 * 
	 */
	public void setResponseHeader(ResponseHeader value) {
		this.responseHeader = value;
	}

	/**
	 * Gets the value of the getAccountBody property.
	 * 
	 * @return possible object is {@link GetAccountListOut }
	 * 
	 */
	public GetAccountListOut getGetAccountBody() {
		return getAccountBody;
	}

	/**
	 * Sets the value of the getAccountBody property.
	 * 
	 * @param value
	 *            allowed object is {@link GetAccountListOut }
	 * 
	 */
	public void setGetAccountBody(GetAccountListOut value) {
		this.getAccountBody = value;
	}
}
