package com.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.RequestHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/basetype/}RequestHeader"/>
 *         &lt;element name="GetCustomerHistoryBody" type="{http://crm.huawei.com/query/}GetCustomerHistoryIn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "requestHeader", "getCustomerHistoryBody" })
@XmlRootElement(name = "GetCustomerHistoryRequest")
public class GetCustomerHistoryRequest {
	@XmlElement(name = "RequestHeader", required = true)
	protected RequestHeader requestHeader;
	@XmlElement(name = "GetCustomerHistoryBody", required = true)
	protected GetCustomerHistoryIn getCustomerHistoryBody;

	/**
	 * Gets the value of the requestHeader property.
	 * 
	 * @return possible object is {@link RequestHeader }
	 * 
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * Sets the value of the requestHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RequestHeader }
	 * 
	 */
	public void setRequestHeader(RequestHeader value) {
		this.requestHeader = value;
	}

	/**
	 * Gets the value of the getCustomerHistoryBody property.
	 * 
	 * @return possible object is {@link GetCustomerHistoryIn }
	 * 
	 */
	public GetCustomerHistoryIn getGetCustomerHistoryBody() {
		return getCustomerHistoryBody;
	}

	/**
	 * Sets the value of the getCustomerHistoryBody property.
	 * 
	 * @param value
	 *            allowed object is {@link GetCustomerHistoryIn }
	 * 
	 */
	public void setGetCustomerHistoryBody(GetCustomerHistoryIn value) {
		this.getCustomerHistoryBody = value;
	}
}
