
package com.huawei.crm.service.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.BusinessFee;
import com.huawei.crm.basetype.order.query.CustomerOrderInfo;
import com.huawei.crm.basetype.order.query.OrderAddressInfo;
import com.huawei.crm.basetype.order.query.ShippingInfo;


/**
 * <p>Java class for OrderDetailInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderDetailInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderInfo" type="{http://crm.huawei.com/basetype/}CustomerOrderInfo"/>
 *         &lt;element name="OrderLines" type="{http://crm.huawei.com/service/}OrderLineList" minOccurs="0"/>
 *         &lt;element name="ShippingInfo" type="{http://crm.huawei.com/basetype/}ShippingInfo" minOccurs="0"/>
 *         &lt;element name="OrderAddressInfo" type="{http://crm.huawei.com/basetype/}OrderAddressInfo" minOccurs="0"/>
 *         &lt;element name="BusinessFee" type="{http://crm.huawei.com/basetype/}BusinessFee" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderDetailInfo", propOrder = {
    "orderInfo",
    "orderLines",
    "shippingInfo",
    "orderAddressInfo",
    "businessFee"
})
public class OrderDetailInfo {

    @XmlElement(name = "OrderInfo", required = true)
    protected CustomerOrderInfo orderInfo;
    @XmlElement(name = "OrderLines")
    protected OrderLineList orderLines;
    @XmlElement(name = "ShippingInfo")
    protected ShippingInfo shippingInfo;
    @XmlElement(name = "OrderAddressInfo")
    protected OrderAddressInfo orderAddressInfo;
    @XmlElement(name = "BusinessFee")
    protected BusinessFee businessFee;

    /**
     * Gets the value of the orderInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerOrderInfo }
     *     
     */
    public CustomerOrderInfo getOrderInfo() {
        return orderInfo;
    }

    /**
     * Sets the value of the orderInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerOrderInfo }
     *     
     */
    public void setOrderInfo(CustomerOrderInfo value) {
        this.orderInfo = value;
    }

    /**
     * Gets the value of the orderLines property.
     * 
     * @return
     *     possible object is
     *     {@link OrderLineList }
     *     
     */
    public OrderLineList getOrderLines() {
        return orderLines;
    }

    /**
     * Sets the value of the orderLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderLineList }
     *     
     */
    public void setOrderLines(OrderLineList value) {
        this.orderLines = value;
    }

    /**
     * Gets the value of the shippingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingInfo }
     *     
     */
    public ShippingInfo getShippingInfo() {
        return shippingInfo;
    }

    /**
     * Sets the value of the shippingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingInfo }
     *     
     */
    public void setShippingInfo(ShippingInfo value) {
        this.shippingInfo = value;
    }

    /**
     * Gets the value of the orderAddressInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderAddressInfo }
     *     
     */
    public OrderAddressInfo getOrderAddressInfo() {
        return orderAddressInfo;
    }

    /**
     * Sets the value of the orderAddressInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderAddressInfo }
     *     
     */
    public void setOrderAddressInfo(OrderAddressInfo value) {
        this.orderAddressInfo = value;
    }

    /**
     * Gets the value of the businessFee property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessFee }
     *     
     */
    public BusinessFee getBusinessFee() {
        return businessFee;
    }

    /**
     * Sets the value of the businessFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessFee }
     *     
     */
    public void setBusinessFee(BusinessFee value) {
        this.businessFee = value;
    }

}
