
package com.huawei.crm.service.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.ResponseHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="RetrieveOrderDetailResponseBody">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OrderDetailInfo" type="{http://crm.huawei.com/service/}OrderDetailInfo"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseHeader",
    "retrieveOrderDetailResponseBody"
})
@XmlRootElement(name = "RetrieveOrderDetailResponse")
public class RetrieveOrderDetailResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(name = "RetrieveOrderDetailResponseBody", required = true)
    protected RetrieveOrderDetailResponse.RetrieveOrderDetailResponseBody retrieveOrderDetailResponseBody;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the retrieveOrderDetailResponseBody property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveOrderDetailResponse.RetrieveOrderDetailResponseBody }
     *     
     */
    public RetrieveOrderDetailResponse.RetrieveOrderDetailResponseBody getRetrieveOrderDetailResponseBody() {
        return retrieveOrderDetailResponseBody;
    }

    /**
     * Sets the value of the retrieveOrderDetailResponseBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveOrderDetailResponse.RetrieveOrderDetailResponseBody }
     *     
     */
    public void setRetrieveOrderDetailResponseBody(RetrieveOrderDetailResponse.RetrieveOrderDetailResponseBody value) {
        this.retrieveOrderDetailResponseBody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OrderDetailInfo" type="{http://crm.huawei.com/service/}OrderDetailInfo"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "orderDetailInfo"
    })
    public static class RetrieveOrderDetailResponseBody {

        @XmlElement(name = "OrderDetailInfo", required = true)
        protected OrderDetailInfo orderDetailInfo;

        /**
         * Gets the value of the orderDetailInfo property.
         * 
         * @return
         *     possible object is
         *     {@link OrderDetailInfo }
         *     
         */
        public OrderDetailInfo getOrderDetailInfo() {
            return orderDetailInfo;
        }

        /**
         * Sets the value of the orderDetailInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link OrderDetailInfo }
         *     
         */
        public void setOrderDetailInfo(OrderDetailInfo value) {
            this.orderDetailInfo = value;
        }

    }

}
