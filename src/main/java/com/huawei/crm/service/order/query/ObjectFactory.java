
package com.huawei.crm.service.order.query;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.crm.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.crm.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrieveOrderDetailResponse }
     * 
     */
    public RetrieveOrderDetailResponse createRetrieveOrderDetailResponse() {
        return new RetrieveOrderDetailResponse();
    }

    /**
     * Create an instance of {@link RetrievePendingOrderResponse }
     * 
     */
    public RetrievePendingOrderResponse createRetrievePendingOrderResponse() {
        return new RetrievePendingOrderResponse();
    }

    /**
     * Create an instance of {@link RetrieveOrderResponse }
     * 
     */
    public RetrieveOrderResponse createRetrieveOrderResponse() {
        return new RetrieveOrderResponse();
    }

    /**
     * Create an instance of {@link RetrievePendingOrderRequest }
     * 
     */
    public RetrievePendingOrderRequest createRetrievePendingOrderRequest() {
        return new RetrievePendingOrderRequest();
    }

    /**
     * Create an instance of {@link RetrievePendingOrderRequestBody }
     * 
     */
    public RetrievePendingOrderRequestBody createRetrievePendingOrderRequestBody() {
        return new RetrievePendingOrderRequestBody();
    }

    /**
     * Create an instance of {@link RetrieveOrderDetailResponse.RetrieveOrderDetailResponseBody }
     * 
     */
    public RetrieveOrderDetailResponse.RetrieveOrderDetailResponseBody createRetrieveOrderDetailResponseRetrieveOrderDetailResponseBody() {
        return new RetrieveOrderDetailResponse.RetrieveOrderDetailResponseBody();
    }

    /**
     * Create an instance of {@link RetrievePendingOrderResponse.RetrievePendingOrderResponseBody }
     * 
     */
    public RetrievePendingOrderResponse.RetrievePendingOrderResponseBody createRetrievePendingOrderResponseRetrievePendingOrderResponseBody() {
        return new RetrievePendingOrderResponse.RetrievePendingOrderResponseBody();
    }

    /**
     * Create an instance of {@link RetrieveOrderResponse.RetrieveOrderResponseBody }
     * 
     */
    public RetrieveOrderResponse.RetrieveOrderResponseBody createRetrieveOrderResponseRetrieveOrderResponseBody() {
        return new RetrieveOrderResponse.RetrieveOrderResponseBody();
    }

    /**
     * Create an instance of {@link RetrieveOrderRequest }
     * 
     */
    public RetrieveOrderRequest createRetrieveOrderRequest() {
        return new RetrieveOrderRequest();
    }

    /**
     * Create an instance of {@link RetrieveOrderRequestBody }
     * 
     */
    public RetrieveOrderRequestBody createRetrieveOrderRequestBody() {
        return new RetrieveOrderRequestBody();
    }

    /**
     * Create an instance of {@link RetrieveOrderDetailRequest }
     * 
     */
    public RetrieveOrderDetailRequest createRetrieveOrderDetailRequest() {
        return new RetrieveOrderDetailRequest();
    }

    /**
     * Create an instance of {@link OrderLineInfo }
     * 
     */
    public OrderLineInfo createOrderLineInfo() {
        return new OrderLineInfo();
    }

    /**
     * Create an instance of {@link OrderDetailInfo }
     * 
     */
    public OrderDetailInfo createOrderDetailInfo() {
        return new OrderDetailInfo();
    }

    /**
     * Create an instance of {@link OrderLineList }
     * 
     */
    public OrderLineList createOrderLineList() {
        return new OrderLineList();
    }

    /**
     * Create an instance of {@link RetrieveOrderList }
     * 
     */
    public RetrieveOrderList createRetrieveOrderList() {
        return new RetrieveOrderList();
    }

    /**
     * Create an instance of {@link RetrievePendingOrderList }
     * 
     */
    public RetrievePendingOrderList createRetrievePendingOrderList() {
        return new RetrievePendingOrderList();
    }

}
