
package com.huawei.crm.service.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.order.query.CustomerOrderInfo;


/**
 * <p>Java class for RetrieveOrderList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveOrderList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetrieveOrderInfo" type="{http://crm.huawei.com/basetype/}CustomerOrderInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveOrderList", propOrder = {
    "retrieveOrderInfo"
})
public class RetrieveOrderList {

    @XmlElement(name = "RetrieveOrderInfo", required = true)
    protected List<CustomerOrderInfo> retrieveOrderInfo;

    /**
     * Gets the value of the retrieveOrderInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the retrieveOrderInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRetrieveOrderInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerOrderInfo }
     * 
     * 
     */
    public List<CustomerOrderInfo> getRetrieveOrderInfo() {
        if (retrieveOrderInfo == null) {
            retrieveOrderInfo = new ArrayList<CustomerOrderInfo>();
        }
        return this.retrieveOrderInfo;
    }

}
