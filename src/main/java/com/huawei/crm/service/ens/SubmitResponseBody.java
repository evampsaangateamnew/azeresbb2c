package com.huawei.crm.service.ens;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.ens.ExtParameterList;
import com.huawei.crm.basetype.ens.OrderItemResponse;

/**
 * <p>
 * Java class for SubmitResponseBody complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalOrderId" type="{http://crm.huawei.com/basetype/}ExternalOrderId" minOccurs="0"/>
 *         &lt;element name="OrderId" type="{http://crm.huawei.com/basetype/}InternalOrderId"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *         &lt;element name="OrderItemResponses">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OrderItemResponse" type="{http://crm.huawei.com/basetype/}OrderItemResponse" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitResponseBody", propOrder = {})
public class SubmitResponseBody {
	@XmlElement(name = "ExternalOrderId")
	protected String externalOrderId;
	@XmlElement(name = "OrderId", required = true)
	protected String orderId;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;
	@XmlElement(name = "OrderItemResponses", required = true)
	protected SubmitResponseBody.OrderItemResponses orderItemResponses;

	/**
	 * Gets the value of the externalOrderId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalOrderId() {
		return externalOrderId;
	}

	/**
	 * Sets the value of the externalOrderId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalOrderId(String value) {
		this.externalOrderId = value;
	}

	/**
	 * Gets the value of the orderId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * Sets the value of the orderId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderId(String value) {
		this.orderId = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}

	/**
	 * Gets the value of the orderItemResponses property.
	 * 
	 * @return possible object is {@link SubmitResponseBody.OrderItemResponses }
	 * 
	 */
	public SubmitResponseBody.OrderItemResponses getOrderItemResponses() {
		return orderItemResponses;
	}

	/**
	 * Sets the value of the orderItemResponses property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link SubmitResponseBody.OrderItemResponses }
	 * 
	 */
	public void setOrderItemResponses(SubmitResponseBody.OrderItemResponses value) {
		this.orderItemResponses = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="OrderItemResponse" type="{http://crm.huawei.com/basetype/}OrderItemResponse" maxOccurs="unbounded"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "orderItemResponse" })
	public static class OrderItemResponses {
		@XmlElement(name = "OrderItemResponse", required = true)
		protected List<OrderItemResponse> orderItemResponse;

		/**
		 * Gets the value of the orderItemResponse property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the orderItemResponse property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getOrderItemResponse().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link OrderItemResponse }
		 * 
		 * 
		 */
		public List<OrderItemResponse> getOrderItemResponse() {
			if (orderItemResponse == null) {
				orderItemResponse = new ArrayList<OrderItemResponse>();
			}
			return this.orderItemResponse;
		}
	}
}
