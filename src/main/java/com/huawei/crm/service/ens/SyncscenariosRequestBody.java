package com.huawei.crm.service.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SyncscenariosRequestBody complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SyncscenariosRequestBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="VendorID">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MSISDN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ScenarioID">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SubScenarioID">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TransactionID">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DateTime" type="{http://crm.huawei.com/basetype/}Time"/>
 *         &lt;element name="Validate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SyncscenariosRequestBody", propOrder = {})
public class SyncscenariosRequestBody {
	@XmlElement(name = "VendorID", required = true)
	protected String vendorID;
	@XmlElement(name = "MSISDN", required = true)
	protected String msisdn;
	@XmlElement(name = "ScenarioID", required = true)
	protected String scenarioID;
	@XmlElement(name = "SubScenarioID", required = true)
	protected String subScenarioID;
	@XmlElement(name = "TransactionID", required = true)
	protected String transactionID;
	@XmlElement(name = "DateTime", required = true)
	protected String dateTime;
	@XmlElement(name = "Validate", required = true)
	protected String validate;

	/**
	 * Gets the value of the vendorID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVendorID() {
		return vendorID;
	}

	/**
	 * Sets the value of the vendorID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVendorID(String value) {
		this.vendorID = value;
	}

	/**
	 * Gets the value of the msisdn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMSISDN() {
		return msisdn;
	}

	/**
	 * Sets the value of the msisdn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMSISDN(String value) {
		this.msisdn = value;
	}

	/**
	 * Gets the value of the scenarioID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScenarioID() {
		return scenarioID;
	}

	/**
	 * Sets the value of the scenarioID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScenarioID(String value) {
		this.scenarioID = value;
	}

	/**
	 * Gets the value of the subScenarioID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubScenarioID() {
		return subScenarioID;
	}

	/**
	 * Sets the value of the subScenarioID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubScenarioID(String value) {
		this.subScenarioID = value;
	}

	/**
	 * Gets the value of the transactionID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransactionID() {
		return transactionID;
	}

	/**
	 * Sets the value of the transactionID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransactionID(String value) {
		this.transactionID = value;
	}

	/**
	 * Gets the value of the dateTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateTime() {
		return dateTime;
	}

	/**
	 * Sets the value of the dateTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDateTime(String value) {
		this.dateTime = value;
	}

	/**
	 * Gets the value of the validate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getValidate() {
		return validate;
	}

	/**
	 * Sets the value of the validate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setValidate(String value) {
		this.validate = value;
	}
}
