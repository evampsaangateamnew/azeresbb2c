package com.huawei.crm.service.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.ens.RequestHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/basetype/}RequestHeader"/>
 *         &lt;element name="CheckoutRequestBody" type="{http://crm.huawei.com/service/}CheckoutRequestBody"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "requestHeader", "checkoutRequestBody" })
@XmlRootElement(name = "CheckoutOrderRequest")
public class CheckoutOrderRequest {
	@XmlElement(name = "RequestHeader", required = true)
	protected RequestHeader requestHeader;
	@XmlElement(name = "CheckoutRequestBody", required = true)
	protected CheckoutRequestBody checkoutRequestBody;

	/**
	 * Gets the value of the requestHeader property.
	 * 
	 * @return possible object is {@link RequestHeader }
	 * 
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * Sets the value of the requestHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RequestHeader }
	 * 
	 */
	public void setRequestHeader(RequestHeader value) {
		this.requestHeader = value;
	}

	/**
	 * Gets the value of the checkoutRequestBody property.
	 * 
	 * @return possible object is {@link CheckoutRequestBody }
	 * 
	 */
	public CheckoutRequestBody getCheckoutRequestBody() {
		return checkoutRequestBody;
	}

	/**
	 * Sets the value of the checkoutRequestBody property.
	 * 
	 * @param value
	 *            allowed object is {@link CheckoutRequestBody }
	 * 
	 */
	public void setCheckoutRequestBody(CheckoutRequestBody value) {
		this.checkoutRequestBody = value;
	}
}
