package com.huawei.crm.service.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.ens.ExtParameterList;
import com.huawei.crm.basetype.ens.OrderItemStatusList;

/**
 * <p>
 * Java class for ResumeResponseBody complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ResumeResponseBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OrderId" type="{http://crm.huawei.com/basetype/}InternalOrderId"/>
 *         &lt;element name="ExternalOrderID" type="{http://crm.huawei.com/basetype/}ExternalOrderId" minOccurs="0"/>
 *         &lt;element name="OrderItemList" type="{http://crm.huawei.com/basetype/}OrderItemStatusList"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResumeResponseBody", propOrder = {})
public class ResumeResponseBody {
	@XmlElement(name = "OrderId", required = true)
	protected String orderId;
	@XmlElement(name = "ExternalOrderID")
	protected String externalOrderID;
	@XmlElement(name = "OrderItemList", required = true)
	protected OrderItemStatusList orderItemList;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the orderId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * Sets the value of the orderId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderId(String value) {
		this.orderId = value;
	}

	/**
	 * Gets the value of the externalOrderID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalOrderID() {
		return externalOrderID;
	}

	/**
	 * Sets the value of the externalOrderID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalOrderID(String value) {
		this.externalOrderID = value;
	}

	/**
	 * Gets the value of the orderItemList property.
	 * 
	 * @return possible object is {@link OrderItemStatusList }
	 * 
	 */
	public OrderItemStatusList getOrderItemList() {
		return orderItemList;
	}

	/**
	 * Sets the value of the orderItemList property.
	 * 
	 * @param value
	 *            allowed object is {@link OrderItemStatusList }
	 * 
	 */
	public void setOrderItemList(OrderItemStatusList value) {
		this.orderItemList = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
