package com.huawei.crm.service.ens;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.huawei.crm.service.enscrmquery
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: com.huawei.crm.service.enscrmquery
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link SubmitResponseBody }
	 * 
	 */
	public SubmitResponseBody createSubmitResponseBody() {
		return new SubmitResponseBody();
	}

	/**
	 * Create an instance of {@link CheckoutResponseBody }
	 * 
	 */
	public CheckoutResponseBody createCheckoutResponseBody() {
		return new CheckoutResponseBody();
	}

	/**
	 * Create an instance of {@link CheckoutOrderRequest }
	 * 
	 */
	public CheckoutOrderRequest createCheckoutOrderRequest() {
		return new CheckoutOrderRequest();
	}

	/**
	 * Create an instance of {@link CheckoutRequestBody }
	 * 
	 */
	public CheckoutRequestBody createCheckoutRequestBody() {
		return new CheckoutRequestBody();
	}

	/**
	 * Create an instance of {@link NotifyOrderRequest }
	 * 
	 */
	public NotifyOrderRequest createNotifyOrderRequest() {
		return new NotifyOrderRequest();
	}

	/**
	 * Create an instance of {@link NotifyRequestBody }
	 * 
	 */
	public NotifyRequestBody createNotifyRequestBody() {
		return new NotifyRequestBody();
	}

	/**
	 * Create an instance of {@link ResumeOrderResponse }
	 * 
	 */
	public ResumeOrderResponse createResumeOrderResponse() {
		return new ResumeOrderResponse();
	}

	/**
	 * Create an instance of {@link ResumeResponseBody }
	 * 
	 */
	public ResumeResponseBody createResumeResponseBody() {
		return new ResumeResponseBody();
	}

	/**
	 * Create an instance of {@link SyncscenariosRequest }
	 * 
	 */
	public SyncscenariosRequest createSyncscenariosRequest() {
		return new SyncscenariosRequest();
	}

	/**
	 * Create an instance of {@link SyncscenariosRequestBody }
	 * 
	 */
	public SyncscenariosRequestBody createSyncscenariosRequestBody() {
		return new SyncscenariosRequestBody();
	}

	/**
	 * Create an instance of {@link ContinueOrderRequest }
	 * 
	 */
	public ContinueOrderRequest createContinueOrderRequest() {
		return new ContinueOrderRequest();
	}

	/**
	 * Create an instance of {@link ContinueRequestBody }
	 * 
	 */
	public ContinueRequestBody createContinueRequestBody() {
		return new ContinueRequestBody();
	}

	/**
	 * Create an instance of {@link ResumeOrderRequest }
	 * 
	 */
	public ResumeOrderRequest createResumeOrderRequest() {
		return new ResumeOrderRequest();
	}

	/**
	 * Create an instance of {@link ResumeRequestBody }
	 * 
	 */
	public ResumeRequestBody createResumeRequestBody() {
		return new ResumeRequestBody();
	}

	/**
	 * Create an instance of {@link CheckoutOrderResponse }
	 * 
	 */
	public CheckoutOrderResponse createCheckoutOrderResponse() {
		return new CheckoutOrderResponse();
	}

	/**
	 * Create an instance of {@link SubmitOrderResponse }
	 * 
	 */
	public SubmitOrderResponse createSubmitOrderResponse() {
		return new SubmitOrderResponse();
	}

	/**
	 * Create an instance of {@link NotifyOrderResponse }
	 * 
	 */
	public NotifyOrderResponse createNotifyOrderResponse() {
		return new NotifyOrderResponse();
	}

	/**
	 * Create an instance of {@link NotifyResponseBody }
	 * 
	 */
	public NotifyResponseBody createNotifyResponseBody() {
		return new NotifyResponseBody();
	}

	/**
	 * Create an instance of {@link ContinueOrderResponse }
	 * 
	 */
	public ContinueOrderResponse createContinueOrderResponse() {
		return new ContinueOrderResponse();
	}

	/**
	 * Create an instance of {@link ContinueResponseBody }
	 * 
	 */
	public ContinueResponseBody createContinueResponseBody() {
		return new ContinueResponseBody();
	}

	/**
	 * Create an instance of {@link SyncscenariosResponse }
	 * 
	 */
	public SyncscenariosResponse createSyncscenariosResponse() {
		return new SyncscenariosResponse();
	}

	/**
	 * Create an instance of {@link SubmitOrderRequest }
	 * 
	 */
	public SubmitOrderRequest createSubmitOrderRequest() {
		return new SubmitOrderRequest();
	}

	/**
	 * Create an instance of {@link SubmitRequestBody }
	 * 
	 */
	public SubmitRequestBody createSubmitRequestBody() {
		return new SubmitRequestBody();
	}

	/**
	 * Create an instance of {@link OfferingRspInfo }
	 * 
	 */
	public OfferingRspInfo createOfferingRspInfo() {
		return new OfferingRspInfo();
	}

	/**
	 * Create an instance of {@link ChargeFeeRspInfo }
	 * 
	 */
	public ChargeFeeRspInfo createChargeFeeRspInfo() {
		return new ChargeFeeRspInfo();
	}

	/**
	 * Create an instance of {@link ChargeFeeRspList }
	 * 
	 */
	public ChargeFeeRspList createChargeFeeRspList() {
		return new ChargeFeeRspList();
	}

	/**
	 * Create an instance of {@link OfferingRspList }
	 * 
	 */
	public OfferingRspList createOfferingRspList() {
		return new OfferingRspList();
	}

	/**
	 * Create an instance of {@link SubmitResponseBody.OrderItemResponses }
	 * 
	 */
	public SubmitResponseBody.OrderItemResponses createSubmitResponseBodyOrderItemResponses() {
		return new SubmitResponseBody.OrderItemResponses();
	}

	/**
	 * Create an instance of {@link CheckoutResponseBody.OrderItemResponses }
	 * 
	 */
	public CheckoutResponseBody.OrderItemResponses createCheckoutResponseBodyOrderItemResponses() {
		return new CheckoutResponseBody.OrderItemResponses();
	}
}
