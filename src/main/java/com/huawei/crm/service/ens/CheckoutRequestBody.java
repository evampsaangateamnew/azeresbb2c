package com.huawei.crm.service.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItems;

/**
 * <p>
 * Java class for CheckoutRequestBody complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CheckoutRequestBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Order" type="{http://crm.huawei.com/basetype/}OrderInfo"/>
 *         &lt;element name="OrderItems" type="{http://crm.huawei.com/basetype/}OrderItems"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckoutRequestBody", propOrder = { "order", "orderItems" })
public class CheckoutRequestBody {
	@XmlElement(name = "Order", required = true)
	protected OrderInfo order;
	@XmlElement(name = "OrderItems", required = true)
	protected OrderItems orderItems;

	/**
	 * Gets the value of the order property.
	 * 
	 * @return possible object is {@link OrderInfo }
	 * 
	 */
	public OrderInfo getOrder() {
		return order;
	}

	/**
	 * Sets the value of the order property.
	 * 
	 * @param value
	 *            allowed object is {@link OrderInfo }
	 * 
	 */
	public void setOrder(OrderInfo value) {
		this.order = value;
	}

	/**
	 * Gets the value of the orderItems property.
	 * 
	 * @return possible object is {@link OrderItems }
	 * 
	 */
	public OrderItems getOrderItems() {
		return orderItems;
	}

	/**
	 * Sets the value of the orderItems property.
	 * 
	 * @param value
	 *            allowed object is {@link OrderItems }
	 * 
	 */
	public void setOrderItems(OrderItems value) {
		this.orderItems = value;
	}
}
