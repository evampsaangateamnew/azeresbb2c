package com.huawei.crm.service.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.ens.ResponseHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="SubmitResponseBody" type="{http://crm.huawei.com/service/}SubmitResponseBody" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "submitResponseBody" })
@XmlRootElement(name = "SubmitOrderResponse")
public class SubmitOrderResponse {
	@XmlElement(name = "ResponseHeader", required = true)
	protected ResponseHeader responseHeader;
	@XmlElement(name = "SubmitResponseBody")
	protected SubmitResponseBody submitResponseBody;

	/**
	 * Gets the value of the responseHeader property.
	 * 
	 * @return possible object is {@link ResponseHeader }
	 * 
	 */
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	/**
	 * Sets the value of the responseHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseHeader }
	 * 
	 */
	public void setResponseHeader(ResponseHeader value) {
		this.responseHeader = value;
	}

	/**
	 * Gets the value of the submitResponseBody property.
	 * 
	 * @return possible object is {@link SubmitResponseBody }
	 * 
	 */
	public SubmitResponseBody getSubmitResponseBody() {
		return submitResponseBody;
	}

	/**
	 * Sets the value of the submitResponseBody property.
	 * 
	 * @param value
	 *            allowed object is {@link SubmitResponseBody }
	 * 
	 */
	public void setSubmitResponseBody(SubmitResponseBody value) {
		this.submitResponseBody = value;
	}

	@Override
	public String toString() {
		return "SubmitOrderResponse [responseHeader=" + responseHeader + ", submitResponseBody=" + submitResponseBody
				+ "]";
	}
	
	
}
