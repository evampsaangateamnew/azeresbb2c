package com.huawei.crm.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for ContinueRequestBody complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ContinueRequestBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalOrderID" type="{http://crm.huawei.com/basetype/}ExternalOrderId" minOccurs="0"/>
 *         &lt;element name="OrderId" type="{http://crm.huawei.com/basetype/}InternalOrderId" minOccurs="0"/>
 *         &lt;element name="OrderItemId" type="{http://crm.huawei.com/basetype/}InternalOrderId" minOccurs="0"/>
 *         &lt;element name="WaitingKey" type="{http://crm.huawei.com/basetype/}WaitingKey" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/basetype/}Remark" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContinueRequestBody", propOrder = {})
public class ContinueRequestBody {
	@XmlElement(name = "ExternalOrderID")
	protected String externalOrderID;
	@XmlElement(name = "OrderId")
	protected String orderId;
	@XmlElement(name = "OrderItemId")
	protected String orderItemId;
	@XmlElement(name = "WaitingKey")
	protected String waitingKey;
	@XmlElement(name = "Remark")
	protected String remark;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the externalOrderID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalOrderID() {
		return externalOrderID;
	}

	/**
	 * Sets the value of the externalOrderID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalOrderID(String value) {
		this.externalOrderID = value;
	}

	/**
	 * Gets the value of the orderId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * Sets the value of the orderId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderId(String value) {
		this.orderId = value;
	}

	/**
	 * Gets the value of the orderItemId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderItemId() {
		return orderItemId;
	}

	/**
	 * Sets the value of the orderItemId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderItemId(String value) {
		this.orderItemId = value;
	}

	/**
	 * Gets the value of the waitingKey property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWaitingKey() {
		return waitingKey;
	}

	/**
	 * Sets the value of the waitingKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWaitingKey(String value) {
		this.waitingKey = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
