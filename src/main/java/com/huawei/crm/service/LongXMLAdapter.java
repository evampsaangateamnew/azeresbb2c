package com.huawei.crm.service;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class LongXMLAdapter extends XmlAdapter<String, Long> {
	@Override
	public String marshal(Long v) throws Exception {
		return "" + v;
	}

	@Override
	public Long unmarshal(String v) throws Exception {
		try {
			if (v.equals(""))
				return (long) 0;
			else
				return Long.parseLong(v);
		} catch (Exception e) {
		}
		return (long) 0;
	}
}