package com.huawei.crm.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;
import com.huawei.crm.basetype.TaxList;

/**
 * <p>
 * Java class for ChargeFeeRspInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeFeeRspInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ChargeType" type="{http://crm.huawei.com/basetype/}ChargeType" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://crm.huawei.com/basetype/}Currency" minOccurs="0"/>
 *         &lt;element name="UnitPrice" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="TaxAmount" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="DiscountAmount" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://crm.huawei.com/basetype/}Quantity" minOccurs="0"/>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="ProductId" type="{http://crm.huawei.com/basetype/}ProductId" minOccurs="0"/>
 *         &lt;element name="TaxList" type="{http://crm.huawei.com/basetype/}TaxList" minOccurs="0"/>
 *         &lt;element name="ChargeRemark" type="{http://crm.huawei.com/basetype/}Remark" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeFeeRspInfo", propOrder = {})
public class ChargeFeeRspInfo {
	@XmlElement(name = "ChargeType")
	protected String chargeType;
	@XmlElement(name = "Currency")
	protected String currency;
	@XmlElement(name = "UnitPrice")
	protected Long unitPrice;
	@XmlElement(name = "Amount")
	protected Long amount;
	@XmlElement(name = "TaxAmount")
	protected Long taxAmount;
	@XmlElement(name = "DiscountAmount")
	protected Long discountAmount;
	@XmlElement(name = "Quantity")
	protected String quantity;
	@XmlElement(name = "OfferingId")
	protected String offeringId;
	@XmlElement(name = "ProductId")
	protected String productId;
	@XmlElement(name = "TaxList")
	protected TaxList taxList;
	@XmlElement(name = "ChargeRemark")
	protected String chargeRemark;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the chargeType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChargeType() {
		return chargeType;
	}

	/**
	 * Sets the value of the chargeType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChargeType(String value) {
		this.chargeType = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the unitPrice property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getUnitPrice() {
		return unitPrice;
	}

	/**
	 * Sets the value of the unitPrice property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setUnitPrice(Long value) {
		this.unitPrice = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAmount(Long value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the taxAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getTaxAmount() {
		return taxAmount;
	}

	/**
	 * Sets the value of the taxAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setTaxAmount(Long value) {
		this.taxAmount = value;
	}

	/**
	 * Gets the value of the discountAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getDiscountAmount() {
		return discountAmount;
	}

	/**
	 * Sets the value of the discountAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setDiscountAmount(Long value) {
		this.discountAmount = value;
	}

	/**
	 * Gets the value of the quantity property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * Sets the value of the quantity property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setQuantity(String value) {
		this.quantity = value;
	}

	/**
	 * Gets the value of the offeringId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * Sets the value of the offeringId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingId(String value) {
		this.offeringId = value;
	}

	/**
	 * Gets the value of the productId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * Sets the value of the productId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProductId(String value) {
		this.productId = value;
	}

	/**
	 * Gets the value of the taxList property.
	 * 
	 * @return possible object is {@link TaxList }
	 * 
	 */
	public TaxList getTaxList() {
		return taxList;
	}

	/**
	 * Sets the value of the taxList property.
	 * 
	 * @param value
	 *            allowed object is {@link TaxList }
	 * 
	 */
	public void setTaxList(TaxList value) {
		this.taxList = value;
	}

	/**
	 * Gets the value of the chargeRemark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChargeRemark() {
		return chargeRemark;
	}

	/**
	 * Sets the value of the chargeRemark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChargeRemark(String value) {
		this.chargeRemark = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
