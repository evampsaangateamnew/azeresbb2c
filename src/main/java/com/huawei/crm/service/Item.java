package com.huawei.crm.service;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
public class Item {
	private String description;
	// @XmlID
	@XmlElement(name = "price")
	@XmlJavaTypeAdapter(type = long.class, value = com.huawei.crm.service.LongXMLAdapter.class)
	// @XmlElement(type = long.class)
	public long price;
	private BigInteger catalogNumber;

	@XmlAttribute(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlAttribute(name = "catalog-number")
	public BigInteger getCatalogNumber() {
		return catalogNumber;
	}

	public void setCatalogNumber(BigInteger catalogNumber) {
		this.catalogNumber = catalogNumber;
	}

	@Override
	public String toString() {
		return "Item [description=" + description + ", price=" + price + ", catalogNumber=" + catalogNumber + "]";
	}
}