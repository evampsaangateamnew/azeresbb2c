package com.huawei.crm.update;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.RequestHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/basetype/}RequestHeader"/>
 *         &lt;element name="SendNotificationRequestBody" type="{http://crm.huawei.com/update/}SendNotificationIn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "requestHeader", "sendNotificationRequestBody" })
@XmlRootElement(name = "SendNotificationRequest")
public class SendNotificationRequest {
	@XmlElement(name = "RequestHeader", required = true)
	protected RequestHeader requestHeader;
	@XmlElement(name = "SendNotificationRequestBody", required = true)
	protected SendNotificationIn sendNotificationRequestBody;

	/**
	 * Gets the value of the requestHeader property.
	 * 
	 * @return possible object is {@link RequestHeader }
	 * 
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * Sets the value of the requestHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RequestHeader }
	 * 
	 */
	public void setRequestHeader(RequestHeader value) {
		this.requestHeader = value;
	}

	/**
	 * Gets the value of the sendNotificationRequestBody property.
	 * 
	 * @return possible object is {@link SendNotificationIn }
	 * 
	 */
	public SendNotificationIn getSendNotificationRequestBody() {
		return sendNotificationRequestBody;
	}

	/**
	 * Sets the value of the sendNotificationRequestBody property.
	 * 
	 * @param value
	 *            allowed object is {@link SendNotificationIn }
	 * 
	 */
	public void setSendNotificationRequestBody(SendNotificationIn value) {
		this.sendNotificationRequestBody = value;
	}
}
