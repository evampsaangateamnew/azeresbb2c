package com.huawei.crm.update;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for NotificationInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="NotificationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="SequenceId" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="MediaType" type="{http://crm.huawei.com/basetype/}NotificationMediaType"/>
 *         &lt;element name="SenderAddress" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ReceiverAddressList" type="{http://crm.huawei.com/update/}ReceiverAddressList"/>
 *         &lt;element name="ContentInfo" type="{http://crm.huawei.com/update/}NotificationContentInfo"/>
 *         &lt;element name="ScheduleTime" type="{http://crm.huawei.com/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="OccasionType" type="{http://crm.huawei.com/update/}OccasionType" minOccurs="0"/>
 *         &lt;element name="Priority" type="{http://crm.huawei.com/update/}NotifyPriority" minOccurs="0"/>
 *         &lt;element name="IsContactLog" type="{http://crm.huawei.com/update/}IsContactLog" minOccurs="0"/>
 *         &lt;element name="CutsomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NotificationInfo", propOrder = {})
public class NotificationInfo {
	@XmlElement(name = "SequenceId", required = true)
	protected BigInteger sequenceId;
	@XmlElement(name = "MediaType", required = true)
	protected String mediaType;
	@XmlElement(name = "SenderAddress")
	protected String senderAddress;
	@XmlElement(name = "ReceiverAddressList", required = true)
	protected ReceiverAddressList receiverAddressList;
	@XmlElement(name = "ContentInfo", required = true)
	protected NotificationContentInfo contentInfo;
	@XmlElement(name = "ScheduleTime")
	protected String scheduleTime;
	@XmlElement(name = "OccasionType")
	protected String occasionType;
	@XmlElement(name = "Priority")
	protected String priority;
	@XmlElement(name = "IsContactLog")
	protected String isContactLog;
	@XmlElement(name = "CutsomerId")
	protected Long cutsomerId;
	@XmlElement(name = "SubscriberId")
	protected Long subscriberId;

	/**
	 * Gets the value of the sequenceId property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getSequenceId() {
		return sequenceId;
	}

	/**
	 * Sets the value of the sequenceId property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setSequenceId(BigInteger value) {
		this.sequenceId = value;
	}

	/**
	 * Gets the value of the mediaType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediaType() {
		return mediaType;
	}

	/**
	 * Sets the value of the mediaType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediaType(String value) {
		this.mediaType = value;
	}

	/**
	 * Gets the value of the senderAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * Sets the value of the senderAddress property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSenderAddress(String value) {
		this.senderAddress = value;
	}

	/**
	 * Gets the value of the receiverAddressList property.
	 * 
	 * @return possible object is {@link ReceiverAddressList }
	 * 
	 */
	public ReceiverAddressList getReceiverAddressList() {
		return receiverAddressList;
	}

	/**
	 * Sets the value of the receiverAddressList property.
	 * 
	 * @param value
	 *            allowed object is {@link ReceiverAddressList }
	 * 
	 */
	public void setReceiverAddressList(ReceiverAddressList value) {
		this.receiverAddressList = value;
	}

	/**
	 * Gets the value of the contentInfo property.
	 * 
	 * @return possible object is {@link NotificationContentInfo }
	 * 
	 */
	public NotificationContentInfo getContentInfo() {
		return contentInfo;
	}

	/**
	 * Sets the value of the contentInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link NotificationContentInfo }
	 * 
	 */
	public void setContentInfo(NotificationContentInfo value) {
		this.contentInfo = value;
	}

	/**
	 * Gets the value of the scheduleTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScheduleTime() {
		return scheduleTime;
	}

	/**
	 * Sets the value of the scheduleTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScheduleTime(String value) {
		this.scheduleTime = value;
	}

	/**
	 * Gets the value of the occasionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOccasionType() {
		return occasionType;
	}

	/**
	 * Sets the value of the occasionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOccasionType(String value) {
		this.occasionType = value;
	}

	/**
	 * Gets the value of the priority property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * Sets the value of the priority property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPriority(String value) {
		this.priority = value;
	}

	/**
	 * Gets the value of the isContactLog property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIsContactLog() {
		return isContactLog;
	}

	/**
	 * Sets the value of the isContactLog property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIsContactLog(String value) {
		this.isContactLog = value;
	}

	/**
	 * Gets the value of the cutsomerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCutsomerId() {
		return cutsomerId;
	}

	/**
	 * Sets the value of the cutsomerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCutsomerId(Long value) {
		this.cutsomerId = value;
	}

	/**
	 * Gets the value of the subscriberId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Sets the value of the subscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setSubscriberId(Long value) {
		this.subscriberId = value;
	}
}
