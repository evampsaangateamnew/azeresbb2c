package com.huawei.crm.update;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ExtParameterList;

/**
 * <p>
 * Java class for NotificationContentInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="NotificationContentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ContentInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Title" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="200"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Content" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="10000"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="AttachmentList" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AttachmentInfo" type="{http://crm.huawei.com/update/}AttachmentInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TemplateInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="TemplateId">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="20"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ParameterList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NotificationContentInfo", propOrder = { "contentInfo", "templateInfo" })
public class NotificationContentInfo {
	@XmlElement(name = "ContentInfo")
	protected NotificationContentInfo.ContentInfo contentInfo;
	@XmlElement(name = "TemplateInfo")
	protected NotificationContentInfo.TemplateInfo templateInfo;

	/**
	 * Gets the value of the contentInfo property.
	 * 
	 * @return possible object is {@link NotificationContentInfo.ContentInfo }
	 * 
	 */
	public NotificationContentInfo.ContentInfo getContentInfo() {
		return contentInfo;
	}

	/**
	 * Sets the value of the contentInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link NotificationContentInfo.ContentInfo }
	 * 
	 */
	public void setContentInfo(NotificationContentInfo.ContentInfo value) {
		this.contentInfo = value;
	}

	/**
	 * Gets the value of the templateInfo property.
	 * 
	 * @return possible object is {@link NotificationContentInfo.TemplateInfo }
	 * 
	 */
	public NotificationContentInfo.TemplateInfo getTemplateInfo() {
		return templateInfo;
	}

	/**
	 * Sets the value of the templateInfo property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link NotificationContentInfo.TemplateInfo }
	 * 
	 */
	public void setTemplateInfo(NotificationContentInfo.TemplateInfo value) {
		this.templateInfo = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;all>
	 *         &lt;element name="Title" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="200"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="Content" minOccurs="0">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="10000"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="AttachmentList" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="AttachmentInfo" type="{http://crm.huawei.com/update/}AttachmentInfo" maxOccurs="unbounded" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/all>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {})
	public static class ContentInfo {
		@XmlElement(name = "Title")
		protected String title;
		@XmlElement(name = "Content")
		protected String content;
		@XmlElement(name = "AttachmentList")
		protected NotificationContentInfo.ContentInfo.AttachmentList attachmentList;

		/**
		 * Gets the value of the title property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getTitle() {
			return title;
		}

		/**
		 * Sets the value of the title property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setTitle(String value) {
			this.title = value;
		}

		/**
		 * Gets the value of the content property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getContent() {
			return content;
		}

		/**
		 * Sets the value of the content property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setContent(String value) {
			this.content = value;
		}

		/**
		 * Gets the value of the attachmentList property.
		 * 
		 * @return possible object is
		 *         {@link NotificationContentInfo.ContentInfo.AttachmentList }
		 * 
		 */
		public NotificationContentInfo.ContentInfo.AttachmentList getAttachmentList() {
			return attachmentList;
		}

		/**
		 * Sets the value of the attachmentList property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link NotificationContentInfo.ContentInfo.AttachmentList }
		 * 
		 */
		public void setAttachmentList(NotificationContentInfo.ContentInfo.AttachmentList value) {
			this.attachmentList = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="AttachmentInfo" type="{http://crm.huawei.com/update/}AttachmentInfo" maxOccurs="unbounded" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "attachmentInfo" })
		public static class AttachmentList {
			@XmlElement(name = "AttachmentInfo")
			protected List<AttachmentInfo> attachmentInfo;

			/**
			 * Gets the value of the attachmentInfo property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the attachmentInfo property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getAttachmentInfo().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link AttachmentInfo }
			 * 
			 * 
			 */
			public List<AttachmentInfo> getAttachmentInfo() {
				if (attachmentInfo == null) {
					attachmentInfo = new ArrayList<AttachmentInfo>();
				}
				return this.attachmentInfo;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;all>
	 *         &lt;element name="TemplateId">
	 *           &lt;simpleType>
	 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *               &lt;maxLength value="20"/>
	 *             &lt;/restriction>
	 *           &lt;/simpleType>
	 *         &lt;/element>
	 *         &lt;element name="ParameterList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
	 *       &lt;/all>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {})
	public static class TemplateInfo {
		@XmlElement(name = "TemplateId", required = true)
		protected String templateId;
		@XmlElement(name = "ParameterList")
		protected ExtParameterList parameterList;

		/**
		 * Gets the value of the templateId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getTemplateId() {
			return templateId;
		}

		/**
		 * Sets the value of the templateId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setTemplateId(String value) {
			this.templateId = value;
		}

		/**
		 * Gets the value of the parameterList property.
		 * 
		 * @return possible object is {@link ExtParameterList }
		 * 
		 */
		public ExtParameterList getParameterList() {
			return parameterList;
		}

		/**
		 * Sets the value of the parameterList property.
		 * 
		 * @param value
		 *            allowed object is {@link ExtParameterList }
		 * 
		 */
		public void setParameterList(ExtParameterList value) {
			this.parameterList = value;
		}
	}
}
