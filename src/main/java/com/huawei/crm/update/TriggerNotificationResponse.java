package com.huawei.crm.update;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.crm.basetype.ResponseHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/basetype/}ResponseHeader"/>
 *         &lt;element name="TriggerNotificationResponseBody" type="{http://crm.huawei.com/update/}TriggerNotificationOut"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "triggerNotificationResponseBody" })
@XmlRootElement(name = "TriggerNotificationResponse")
public class TriggerNotificationResponse {
	@XmlElement(name = "ResponseHeader", required = true)
	protected ResponseHeader responseHeader;
	@XmlElement(name = "TriggerNotificationResponseBody", required = true)
	protected TriggerNotificationOut triggerNotificationResponseBody;

	/**
	 * Gets the value of the responseHeader property.
	 * 
	 * @return possible object is {@link ResponseHeader }
	 * 
	 */
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	/**
	 * Sets the value of the responseHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ResponseHeader }
	 * 
	 */
	public void setResponseHeader(ResponseHeader value) {
		this.responseHeader = value;
	}

	/**
	 * Gets the value of the triggerNotificationResponseBody property.
	 * 
	 * @return possible object is {@link TriggerNotificationOut }
	 * 
	 */
	public TriggerNotificationOut getTriggerNotificationResponseBody() {
		return triggerNotificationResponseBody;
	}

	/**
	 * Sets the value of the triggerNotificationResponseBody property.
	 * 
	 * @param value
	 *            allowed object is {@link TriggerNotificationOut }
	 * 
	 */
	public void setTriggerNotificationResponseBody(TriggerNotificationOut value) {
		this.triggerNotificationResponseBody = value;
	}
}
