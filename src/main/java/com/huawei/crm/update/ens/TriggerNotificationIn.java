package com.huawei.crm.update.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.basetype.ens.ExtParameterList;

/**
 * <p>
 * Java class for TriggerNotificationIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TriggerNotificationIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="SceneTypeId" type="{http://crm.huawei.com/update/}SceneTypeId"/>
 *         &lt;element name="EventCode" type="{http://crm.huawei.com/update/}EventCode"/>
 *         &lt;element name="NotifyEntityInfo" type="{http://crm.huawei.com/update/}NotifyEntityInfo" minOccurs="0"/>
 *         &lt;element name="ParameterList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *         &lt;element name="MediaType" type="{http://crm.huawei.com/basetype/}NotificationMediaType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TriggerNotificationIn", propOrder = {})
public class TriggerNotificationIn {
	@XmlElement(name = "SceneTypeId", required = true)
	protected String sceneTypeId;
	@XmlElement(name = "EventCode", required = true)
	protected String eventCode;
	@XmlElement(name = "NotifyEntityInfo")
	protected NotifyEntityInfo notifyEntityInfo;
	@XmlElement(name = "ParameterList")
	protected ExtParameterList parameterList;
	@XmlElement(name = "MediaType")
	protected String mediaType;

	/**
	 * Gets the value of the sceneTypeId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSceneTypeId() {
		return sceneTypeId;
	}

	/**
	 * Sets the value of the sceneTypeId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSceneTypeId(String value) {
		this.sceneTypeId = value;
	}

	/**
	 * Gets the value of the eventCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEventCode() {
		return eventCode;
	}

	/**
	 * Sets the value of the eventCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEventCode(String value) {
		this.eventCode = value;
	}

	/**
	 * Gets the value of the notifyEntityInfo property.
	 * 
	 * @return possible object is {@link NotifyEntityInfo }
	 * 
	 */
	public NotifyEntityInfo getNotifyEntityInfo() {
		return notifyEntityInfo;
	}

	/**
	 * Sets the value of the notifyEntityInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link NotifyEntityInfo }
	 * 
	 */
	public void setNotifyEntityInfo(NotifyEntityInfo value) {
		this.notifyEntityInfo = value;
	}

	/**
	 * Gets the value of the parameterList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getParameterList() {
		return parameterList;
	}

	/**
	 * Sets the value of the parameterList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setParameterList(ExtParameterList value) {
		this.parameterList = value;
	}

	/**
	 * Gets the value of the mediaType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMediaType() {
		return mediaType;
	}

	/**
	 * Sets the value of the mediaType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMediaType(String value) {
		this.mediaType = value;
	}
}
