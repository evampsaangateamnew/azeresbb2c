package com.huawei.crm.update.ens;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SendNotificationOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SendNotificationOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="SequenceId" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="RetCode" type="{http://crm.huawei.com/basetype/}RetCode"/>
 *                   &lt;element name="RetMsg" type="{http://crm.huawei.com/basetype/}RetMsg" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendNotificationOut", propOrder = { "resultInfo" })
public class SendNotificationOut {
	@XmlElement(name = "ResultInfo")
	protected List<SendNotificationOut.ResultInfo> resultInfo;

	/**
	 * Gets the value of the resultInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the resultInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getResultInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link SendNotificationOut.ResultInfo }
	 * 
	 * 
	 */
	public List<SendNotificationOut.ResultInfo> getResultInfo() {
		if (resultInfo == null) {
			resultInfo = new ArrayList<SendNotificationOut.ResultInfo>();
		}
		return this.resultInfo;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;all>
	 *         &lt;element name="SequenceId" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *         &lt;element name="RetCode" type="{http://crm.huawei.com/basetype/}RetCode"/>
	 *         &lt;element name="RetMsg" type="{http://crm.huawei.com/basetype/}RetMsg" minOccurs="0"/>
	 *       &lt;/all>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {})
	public static class ResultInfo {
		@XmlElement(name = "SequenceId", required = true)
		protected BigInteger sequenceId;
		@XmlElement(name = "RetCode", required = true)
		protected String retCode;
		@XmlElement(name = "RetMsg")
		protected String retMsg;

		/**
		 * Gets the value of the sequenceId property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getSequenceId() {
			return sequenceId;
		}

		/**
		 * Sets the value of the sequenceId property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setSequenceId(BigInteger value) {
			this.sequenceId = value;
		}

		/**
		 * Gets the value of the retCode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRetCode() {
			return retCode;
		}

		/**
		 * Sets the value of the retCode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRetCode(String value) {
			this.retCode = value;
		}

		/**
		 * Gets the value of the retMsg property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRetMsg() {
			return retMsg;
		}

		/**
		 * Sets the value of the retMsg property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRetMsg(String value) {
			this.retMsg = value;
		}
	}
}
