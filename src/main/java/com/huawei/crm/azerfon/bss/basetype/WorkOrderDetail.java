
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkOrderDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkOrderDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="WorkOrderId" type="{http://crm.huawei.com/azerfon/bss/basetype/}WorkOrderId"/>
 *         &lt;element name="WorkDesc" type="{http://crm.huawei.com/azerfon/bss/basetype/}Remark"/>
 *         &lt;element name="CreateTime" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time"/>
 *         &lt;element name="LastStatusTime" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time"/>
 *         &lt;element name="WorkOrderStatus" type="{http://crm.huawei.com/azerfon/bss/basetype/}WorkOrderStatus"/>
 *         &lt;element name="StatusDesc" type="{http://crm.huawei.com/azerfon/bss/basetype/}Remark"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkOrderDetail", propOrder = {

})
public class WorkOrderDetail {

    @XmlElement(name = "WorkOrderId", required = true)
    protected String workOrderId;
    @XmlElement(name = "WorkDesc", required = true)
    protected String workDesc;
    @XmlElement(name = "CreateTime", required = true)
    protected String createTime;
    @XmlElement(name = "LastStatusTime", required = true)
    protected String lastStatusTime;
    @XmlElement(name = "WorkOrderStatus", required = true)
    protected String workOrderStatus;
    @XmlElement(name = "StatusDesc", required = true)
    protected String statusDesc;

    /**
     * Gets the value of the workOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkOrderId() {
        return workOrderId;
    }

    /**
     * Sets the value of the workOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkOrderId(String value) {
        this.workOrderId = value;
    }

    /**
     * Gets the value of the workDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkDesc() {
        return workDesc;
    }

    /**
     * Sets the value of the workDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkDesc(String value) {
        this.workDesc = value;
    }

    /**
     * Gets the value of the createTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * Sets the value of the createTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateTime(String value) {
        this.createTime = value;
    }

    /**
     * Gets the value of the lastStatusTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastStatusTime() {
        return lastStatusTime;
    }

    /**
     * Sets the value of the lastStatusTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastStatusTime(String value) {
        this.lastStatusTime = value;
    }

    /**
     * Gets the value of the workOrderStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkOrderStatus() {
        return workOrderStatus;
    }

    /**
     * Sets the value of the workOrderStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkOrderStatus(String value) {
        this.workOrderStatus = value;
    }

    /**
     * Gets the value of the statusDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * Sets the value of the statusDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusDesc(String value) {
        this.statusDesc = value;
    }

}
