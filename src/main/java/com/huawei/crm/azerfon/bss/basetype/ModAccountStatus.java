
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModAccountStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModAccountStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}ServiceNumber"/>
 *         &lt;element name="Reason" type="{http://crm.huawei.com/azerfon/bss/basetype/}Remark" minOccurs="0"/>
 *         &lt;element name="ExternalOrderId" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExternalOrderId" minOccurs="0"/>
 *         &lt;element name="AccountInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountEntity"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModAccountStatus", propOrder = {

})
public class ModAccountStatus {

    @XmlElement(name = "ServiceNumber", required = true)
    protected String serviceNumber;
    @XmlElement(name = "Reason")
    protected String reason;
    @XmlElement(name = "ExternalOrderId")
    protected String externalOrderId;
    @XmlElement(name = "AccountInfo", required = true)
    protected AccountEntity accountInfo;

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the externalOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalOrderId() {
        return externalOrderId;
    }

    /**
     * Sets the value of the externalOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalOrderId(String value) {
        this.externalOrderId = value;
    }

    /**
     * Gets the value of the accountInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountEntity }
     *     
     */
    public AccountEntity getAccountInfo() {
        return accountInfo;
    }

    /**
     * Sets the value of the accountInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountEntity }
     *     
     */
    public void setAccountInfo(AccountEntity value) {
        this.accountInfo = value;
    }

}
