
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="RelaSeq" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaSeq" minOccurs="0"/>
 *         &lt;element name="Relatype" type="{http://crm.huawei.com/azerfon/bss/basetype/}Relatype" minOccurs="0"/>
 *         &lt;element name="PicType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://crm.huawei.com/azerfon/bss/basetype/}Language" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/azerfon/bss/basetype/}ActionType"/>
 *         &lt;element name="CertificateType" type="{http://crm.huawei.com/azerfon/bss/basetype/}CertificateType" minOccurs="0"/>
 *         &lt;element name="CertificateNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}CertificateNumber" minOccurs="0"/>
 *         &lt;element name="Relapriority" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaPriority" minOccurs="0"/>
 *         &lt;element name="Relaname1" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaName" minOccurs="0"/>
 *         &lt;element name="Relaname2" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaName" minOccurs="0"/>
 *         &lt;element name="Relaname3" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaName" minOccurs="0"/>
 *         &lt;element name="Relaname4" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaName" minOccurs="0"/>
 *         &lt;element name="ContactAddress" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressInfo" minOccurs="0"/>
 *         &lt;element name="Relaaddr1" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaAddress" minOccurs="0"/>
 *         &lt;element name="Relaaddr2" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaAddress" minOccurs="0"/>
 *         &lt;element name="Relaaddr3" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaAddress" minOccurs="0"/>
 *         &lt;element name="Relaaddr4" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaAddress" minOccurs="0"/>
 *         &lt;element name="Relaaddr5" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaAddress" minOccurs="0"/>
 *         &lt;element name="Relaaddr6" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaAddress" minOccurs="0"/>
 *         &lt;element name="Relaaddr7" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaAddress" minOccurs="0"/>
 *         &lt;element name="Relaaddr8" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaAddress" minOccurs="0"/>
 *         &lt;element name="Relatel1" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="Relatel2" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="Relatel3" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="Relatel4" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="Relaemail" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaEmail" minOccurs="0"/>
 *         &lt;element name="Relafax" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaFax" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactInfo", propOrder = {

})
public class ContactInfo {

    @XmlElement(name = "RelaSeq")
    protected String relaSeq;
    @XmlElement(name = "Relatype")
    protected String relatype;
    @XmlElement(name = "PicType")
    protected Object picType;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "ActionType", required = true)
    protected String actionType;
    @XmlElement(name = "CertificateType")
    protected String certificateType;
    @XmlElement(name = "CertificateNumber")
    protected String certificateNumber;
    @XmlElement(name = "Relapriority")
    protected String relapriority;
    @XmlElement(name = "Relaname1")
    protected String relaname1;
    @XmlElement(name = "Relaname2")
    protected String relaname2;
    @XmlElement(name = "Relaname3")
    protected String relaname3;
    @XmlElement(name = "Relaname4")
    protected String relaname4;
    @XmlElement(name = "ContactAddress")
    protected AddressInfo contactAddress;
    @XmlElement(name = "Relaaddr1")
    protected String relaaddr1;
    @XmlElement(name = "Relaaddr2")
    protected String relaaddr2;
    @XmlElement(name = "Relaaddr3")
    protected String relaaddr3;
    @XmlElement(name = "Relaaddr4")
    protected String relaaddr4;
    @XmlElement(name = "Relaaddr5")
    protected String relaaddr5;
    @XmlElement(name = "Relaaddr6")
    protected String relaaddr6;
    @XmlElement(name = "Relaaddr7")
    protected String relaaddr7;
    @XmlElement(name = "Relaaddr8")
    protected String relaaddr8;
    @XmlElement(name = "Relatel1")
    protected String relatel1;
    @XmlElement(name = "Relatel2")
    protected String relatel2;
    @XmlElement(name = "Relatel3")
    protected String relatel3;
    @XmlElement(name = "Relatel4")
    protected String relatel4;
    @XmlElement(name = "Relaemail")
    protected String relaemail;
    @XmlElement(name = "Relafax")
    protected String relafax;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the relaSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaSeq() {
        return relaSeq;
    }

    /**
     * Sets the value of the relaSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaSeq(String value) {
        this.relaSeq = value;
    }

    /**
     * Gets the value of the relatype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatype() {
        return relatype;
    }

    /**
     * Sets the value of the relatype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatype(String value) {
        this.relatype = value;
    }

    /**
     * Gets the value of the picType property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getPicType() {
        return picType;
    }

    /**
     * Sets the value of the picType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setPicType(Object value) {
        this.picType = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the certificateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateType() {
        return certificateType;
    }

    /**
     * Sets the value of the certificateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateType(String value) {
        this.certificateType = value;
    }

    /**
     * Gets the value of the certificateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateNumber() {
        return certificateNumber;
    }

    /**
     * Sets the value of the certificateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateNumber(String value) {
        this.certificateNumber = value;
    }

    /**
     * Gets the value of the relapriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelapriority() {
        return relapriority;
    }

    /**
     * Sets the value of the relapriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelapriority(String value) {
        this.relapriority = value;
    }

    /**
     * Gets the value of the relaname1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaname1() {
        return relaname1;
    }

    /**
     * Sets the value of the relaname1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaname1(String value) {
        this.relaname1 = value;
    }

    /**
     * Gets the value of the relaname2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaname2() {
        return relaname2;
    }

    /**
     * Sets the value of the relaname2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaname2(String value) {
        this.relaname2 = value;
    }

    /**
     * Gets the value of the relaname3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaname3() {
        return relaname3;
    }

    /**
     * Sets the value of the relaname3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaname3(String value) {
        this.relaname3 = value;
    }

    /**
     * Gets the value of the relaname4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaname4() {
        return relaname4;
    }

    /**
     * Sets the value of the relaname4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaname4(String value) {
        this.relaname4 = value;
    }

    /**
     * Gets the value of the contactAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressInfo }
     *     
     */
    public AddressInfo getContactAddress() {
        return contactAddress;
    }

    /**
     * Sets the value of the contactAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInfo }
     *     
     */
    public void setContactAddress(AddressInfo value) {
        this.contactAddress = value;
    }

    /**
     * Gets the value of the relaaddr1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaaddr1() {
        return relaaddr1;
    }

    /**
     * Sets the value of the relaaddr1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaaddr1(String value) {
        this.relaaddr1 = value;
    }

    /**
     * Gets the value of the relaaddr2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaaddr2() {
        return relaaddr2;
    }

    /**
     * Sets the value of the relaaddr2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaaddr2(String value) {
        this.relaaddr2 = value;
    }

    /**
     * Gets the value of the relaaddr3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaaddr3() {
        return relaaddr3;
    }

    /**
     * Sets the value of the relaaddr3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaaddr3(String value) {
        this.relaaddr3 = value;
    }

    /**
     * Gets the value of the relaaddr4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaaddr4() {
        return relaaddr4;
    }

    /**
     * Sets the value of the relaaddr4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaaddr4(String value) {
        this.relaaddr4 = value;
    }

    /**
     * Gets the value of the relaaddr5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaaddr5() {
        return relaaddr5;
    }

    /**
     * Sets the value of the relaaddr5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaaddr5(String value) {
        this.relaaddr5 = value;
    }

    /**
     * Gets the value of the relaaddr6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaaddr6() {
        return relaaddr6;
    }

    /**
     * Sets the value of the relaaddr6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaaddr6(String value) {
        this.relaaddr6 = value;
    }

    /**
     * Gets the value of the relaaddr7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaaddr7() {
        return relaaddr7;
    }

    /**
     * Sets the value of the relaaddr7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaaddr7(String value) {
        this.relaaddr7 = value;
    }

    /**
     * Gets the value of the relaaddr8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaaddr8() {
        return relaaddr8;
    }

    /**
     * Sets the value of the relaaddr8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaaddr8(String value) {
        this.relaaddr8 = value;
    }

    /**
     * Gets the value of the relatel1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatel1() {
        return relatel1;
    }

    /**
     * Sets the value of the relatel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatel1(String value) {
        this.relatel1 = value;
    }

    /**
     * Gets the value of the relatel2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatel2() {
        return relatel2;
    }

    /**
     * Sets the value of the relatel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatel2(String value) {
        this.relatel2 = value;
    }

    /**
     * Gets the value of the relatel3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatel3() {
        return relatel3;
    }

    /**
     * Sets the value of the relatel3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatel3(String value) {
        this.relatel3 = value;
    }

    /**
     * Gets the value of the relatel4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatel4() {
        return relatel4;
    }

    /**
     * Sets the value of the relatel4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatel4(String value) {
        this.relatel4 = value;
    }

    /**
     * Gets the value of the relaemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaemail() {
        return relaemail;
    }

    /**
     * Sets the value of the relaemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaemail(String value) {
        this.relaemail = value;
    }

    /**
     * Gets the value of the relafax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelafax() {
        return relafax;
    }

    /**
     * Sets the value of the relafax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelafax(String value) {
        this.relafax = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
