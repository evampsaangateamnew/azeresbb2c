
package com.huawei.crm.azerfon.bss.basetype;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InstallmentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InstallmentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="purchaseSeq" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="instId" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="feeType" type="{http://crm.huawei.com/azerfon/bss/basetype/}FeeType"/>
 *         &lt;element name="feeItemCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}FeeItemCode"/>
 *         &lt;element name="totalPeriods" type="{http://crm.huawei.com/azerfon/bss/basetype/}PeriodCount"/>
 *         &lt;element name="firstAmount" type="{http://crm.huawei.com/azerfon/bss/basetype/}Amount"/>
 *         &lt;element name="middleAmount" type="{http://crm.huawei.com/azerfon/bss/basetype/}Amount"/>
 *         &lt;element name="lastAmount" type="{http://crm.huawei.com/azerfon/bss/basetype/}Amount"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstallmentInfo", propOrder = {

})
public class InstallmentInfo {

    protected Object purchaseSeq;
    @XmlElement(required = true)
    protected Object instId;
    @XmlElement(required = true)
    protected String feeType;
    @XmlElement(required = true)
    protected String feeItemCode;
    @XmlElement(required = true)
    protected BigInteger totalPeriods;
    protected long firstAmount;
    protected long middleAmount;
    protected long lastAmount;

    /**
     * Gets the value of the purchaseSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getPurchaseSeq() {
        return purchaseSeq;
    }

    /**
     * Sets the value of the purchaseSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setPurchaseSeq(Object value) {
        this.purchaseSeq = value;
    }

    /**
     * Gets the value of the instId property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getInstId() {
        return instId;
    }

    /**
     * Sets the value of the instId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setInstId(Object value) {
        this.instId = value;
    }

    /**
     * Gets the value of the feeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeType() {
        return feeType;
    }

    /**
     * Sets the value of the feeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeType(String value) {
        this.feeType = value;
    }

    /**
     * Gets the value of the feeItemCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeItemCode() {
        return feeItemCode;
    }

    /**
     * Sets the value of the feeItemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeItemCode(String value) {
        this.feeItemCode = value;
    }

    /**
     * Gets the value of the totalPeriods property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalPeriods() {
        return totalPeriods;
    }

    /**
     * Sets the value of the totalPeriods property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalPeriods(BigInteger value) {
        this.totalPeriods = value;
    }

    /**
     * Gets the value of the firstAmount property.
     * 
     */
    public long getFirstAmount() {
        return firstAmount;
    }

    /**
     * Sets the value of the firstAmount property.
     * 
     */
    public void setFirstAmount(long value) {
        this.firstAmount = value;
    }

    /**
     * Gets the value of the middleAmount property.
     * 
     */
    public long getMiddleAmount() {
        return middleAmount;
    }

    /**
     * Sets the value of the middleAmount property.
     * 
     */
    public void setMiddleAmount(long value) {
        this.middleAmount = value;
    }

    /**
     * Gets the value of the lastAmount property.
     * 
     */
    public long getLastAmount() {
        return lastAmount;
    }

    /**
     * Sets the value of the lastAmount property.
     * 
     */
    public void setLastAmount(long value) {
        this.lastAmount = value;
    }

}
