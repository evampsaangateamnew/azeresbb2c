
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSubOfferingInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSubOfferingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingKey"/>
 *         &lt;element name="OfferingName" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingName"/>
 *         &lt;element name="ParentOfferingId" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingKey" minOccurs="0"/>
 *         &lt;element name="BundleFlag" type="{http://crm.huawei.com/azerfon/bss/basetype/}BundleFlag" minOccurs="0"/>
 *         &lt;element name="TrialStartDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="TrialEndDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://crm.huawei.com/azerfon/bss/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingStatus" minOccurs="0"/>
 *         &lt;element name="CreateDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="EffectiveTime" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpiredTime" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="LatestActiveDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ProductList" type="{http://crm.huawei.com/azerfon/bss/basetype/}GetSubProductList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *         &lt;element name="GroupMemberFlag" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupMemberFlag" minOccurs="0"/>
 *         &lt;element name="GroupSubID" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupSubID" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://crm.huawei.com/azerfon/bss/basetype/}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubOfferingInfo", propOrder = {

})
public class GetSubOfferingInfo {

    @XmlElement(name = "OfferingId", required = true)
    protected OfferingKey offeringId;
    @XmlElement(name = "OfferingName", required = true)
    protected String offeringName;
    @XmlElement(name = "ParentOfferingId")
    protected OfferingKey parentOfferingId;
    @XmlElement(name = "BundleFlag")
    protected String bundleFlag;
    @XmlElement(name = "TrialStartDate")
    protected String trialStartDate;
    @XmlElement(name = "TrialEndDate")
    protected String trialEndDate;
    @XmlElement(name = "Amount")
    protected Long amount;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "CreateDate")
    protected String createDate;
    @XmlElement(name = "EffectiveTime")
    protected String effectiveTime;
    @XmlElement(name = "ExpiredTime")
    protected String expiredTime;
    @XmlElement(name = "LatestActiveDate")
    protected String latestActiveDate;
    @XmlElement(name = "ProductList")
    protected GetSubProductList productList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;
    @XmlElement(name = "GroupMemberFlag")
    protected String groupMemberFlag;
    @XmlElement(name = "GroupSubID")
    protected String groupSubID;
    @XmlElement(name = "Description")
    protected String description;

    /**
     * Gets the value of the offeringId property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getOfferingId() {
        return offeringId;
    }

    /**
     * Sets the value of the offeringId property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setOfferingId(OfferingKey value) {
        this.offeringId = value;
    }

    /**
     * Gets the value of the offeringName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingName() {
        return offeringName;
    }

    /**
     * Sets the value of the offeringName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingName(String value) {
        this.offeringName = value;
    }

    /**
     * Gets the value of the parentOfferingId property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getParentOfferingId() {
        return parentOfferingId;
    }

    /**
     * Sets the value of the parentOfferingId property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setParentOfferingId(OfferingKey value) {
        this.parentOfferingId = value;
    }

    /**
     * Gets the value of the bundleFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBundleFlag() {
        return bundleFlag;
    }

    /**
     * Sets the value of the bundleFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBundleFlag(String value) {
        this.bundleFlag = value;
    }

    /**
     * Gets the value of the trialStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrialStartDate() {
        return trialStartDate;
    }

    /**
     * Sets the value of the trialStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrialStartDate(String value) {
        this.trialStartDate = value;
    }

    /**
     * Gets the value of the trialEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrialEndDate() {
        return trialEndDate;
    }

    /**
     * Sets the value of the trialEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrialEndDate(String value) {
        this.trialEndDate = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAmount(Long value) {
        this.amount = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateDate(String value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the effectiveTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveTime() {
        return effectiveTime;
    }

    /**
     * Sets the value of the effectiveTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveTime(String value) {
        this.effectiveTime = value;
    }

    /**
     * Gets the value of the expiredTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiredTime() {
        return expiredTime;
    }

    /**
     * Sets the value of the expiredTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiredTime(String value) {
        this.expiredTime = value;
    }

    /**
     * Gets the value of the latestActiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatestActiveDate() {
        return latestActiveDate;
    }

    /**
     * Sets the value of the latestActiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatestActiveDate(String value) {
        this.latestActiveDate = value;
    }

    /**
     * Gets the value of the productList property.
     * 
     * @return
     *     possible object is
     *     {@link GetSubProductList }
     *     
     */
    public GetSubProductList getProductList() {
        return productList;
    }

    /**
     * Sets the value of the productList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSubProductList }
     *     
     */
    public void setProductList(GetSubProductList value) {
        this.productList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

    /**
     * Gets the value of the groupMemberFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupMemberFlag() {
        return groupMemberFlag;
    }

    /**
     * Sets the value of the groupMemberFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupMemberFlag(String value) {
        this.groupMemberFlag = value;
    }

    /**
     * Gets the value of the groupSubID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupSubID() {
        return groupSubID;
    }

    /**
     * Sets the value of the groupSubID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupSubID(String value) {
        this.groupSubID = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
