
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CallScreenNoInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CallScreenNoInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScreenNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}ScreenNumber" minOccurs="0"/>
 *         &lt;element name="CountryCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}CountryCode" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/azerfon/bss/basetype/}CallScreenActionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallScreenNoInfo", propOrder = {
    "screenNumber",
    "countryCode",
    "actionType"
})
public class CallScreenNoInfo {

    @XmlElement(name = "ScreenNumber")
    protected String screenNumber;
    @XmlElement(name = "CountryCode")
    protected String countryCode;
    @XmlElement(name = "ActionType")
    protected String actionType;

    /**
     * Gets the value of the screenNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScreenNumber() {
        return screenNumber;
    }

    /**
     * Sets the value of the screenNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScreenNumber(String value) {
        this.screenNumber = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

}
