
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AddressClass" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressClass" minOccurs="0"/>
 *         &lt;element name="ContactSeq" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressId" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/azerfon/bss/basetype/}ActionType" minOccurs="0"/>
 *         &lt;element name="AddressType" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressType" minOccurs="0"/>
 *         &lt;element name="LocalId" type="{http://crm.huawei.com/azerfon/bss/basetype/}LocalId" minOccurs="0"/>
 *         &lt;element name="Address1" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address2" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address3" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address4" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address5" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address6" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address7" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address8" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address9" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address10" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="PostCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}PostCode" minOccurs="0"/>
 *         &lt;element name="Telephone1" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="Telephone2" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="Telephone3" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="Telephone4" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="Email1" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaEmail" minOccurs="0"/>
 *         &lt;element name="Email2" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaEmail" minOccurs="0"/>
 *         &lt;element name="Email3" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaEmail" minOccurs="0"/>
 *         &lt;element name="Email4" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaEmail" minOccurs="0"/>
 *         &lt;element name="Fax1" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaFax" minOccurs="0"/>
 *         &lt;element name="Fax2" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaFax" minOccurs="0"/>
 *         &lt;element name="Fax3" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaFax" minOccurs="0"/>
 *         &lt;element name="Fax4" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaFax" minOccurs="0"/>
 *         &lt;element name="SmsNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}SmsNumber" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressInfo", propOrder = {

})
public class AddressInfo {

    @XmlElement(name = "AddressClass")
    protected String addressClass;
    @XmlElement(name = "ContactSeq")
    protected String contactSeq;
    @XmlElement(name = "ActionType")
    protected String actionType;
    @XmlElement(name = "AddressType")
    protected String addressType;
    @XmlElement(name = "LocalId")
    protected String localId;
    @XmlElement(name = "Address1")
    protected String address1;
    @XmlElement(name = "Address2")
    protected String address2;
    @XmlElement(name = "Address3")
    protected String address3;
    @XmlElement(name = "Address4")
    protected String address4;
    @XmlElement(name = "Address5")
    protected String address5;
    @XmlElement(name = "Address6")
    protected String address6;
    @XmlElement(name = "Address7")
    protected String address7;
    @XmlElement(name = "Address8")
    protected String address8;
    @XmlElement(name = "Address9")
    protected String address9;
    @XmlElement(name = "Address10")
    protected String address10;
    @XmlElement(name = "PostCode")
    protected String postCode;
    @XmlElement(name = "Telephone1")
    protected String telephone1;
    @XmlElement(name = "Telephone2")
    protected String telephone2;
    @XmlElement(name = "Telephone3")
    protected String telephone3;
    @XmlElement(name = "Telephone4")
    protected String telephone4;
    @XmlElement(name = "Email1")
    protected String email1;
    @XmlElement(name = "Email2")
    protected String email2;
    @XmlElement(name = "Email3")
    protected String email3;
    @XmlElement(name = "Email4")
    protected String email4;
    @XmlElement(name = "Fax1")
    protected String fax1;
    @XmlElement(name = "Fax2")
    protected String fax2;
    @XmlElement(name = "Fax3")
    protected String fax3;
    @XmlElement(name = "Fax4")
    protected String fax4;
    @XmlElement(name = "SmsNumber")
    protected String smsNumber;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the addressClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressClass() {
        return addressClass;
    }

    /**
     * Sets the value of the addressClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressClass(String value) {
        this.addressClass = value;
    }

    /**
     * Gets the value of the contactSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactSeq() {
        return contactSeq;
    }

    /**
     * Sets the value of the contactSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactSeq(String value) {
        this.contactSeq = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the addressType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressType() {
        return addressType;
    }

    /**
     * Sets the value of the addressType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressType(String value) {
        this.addressType = value;
    }

    /**
     * Gets the value of the localId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalId() {
        return localId;
    }

    /**
     * Sets the value of the localId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalId(String value) {
        this.localId = value;
    }

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress2(String value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the address3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * Sets the value of the address3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress3(String value) {
        this.address3 = value;
    }

    /**
     * Gets the value of the address4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress4() {
        return address4;
    }

    /**
     * Sets the value of the address4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress4(String value) {
        this.address4 = value;
    }

    /**
     * Gets the value of the address5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress5() {
        return address5;
    }

    /**
     * Sets the value of the address5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress5(String value) {
        this.address5 = value;
    }

    /**
     * Gets the value of the address6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress6() {
        return address6;
    }

    /**
     * Sets the value of the address6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress6(String value) {
        this.address6 = value;
    }

    /**
     * Gets the value of the address7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress7() {
        return address7;
    }

    /**
     * Sets the value of the address7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress7(String value) {
        this.address7 = value;
    }

    /**
     * Gets the value of the address8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress8() {
        return address8;
    }

    /**
     * Sets the value of the address8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress8(String value) {
        this.address8 = value;
    }

    /**
     * Gets the value of the address9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress9() {
        return address9;
    }

    /**
     * Sets the value of the address9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress9(String value) {
        this.address9 = value;
    }

    /**
     * Gets the value of the address10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress10() {
        return address10;
    }

    /**
     * Sets the value of the address10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress10(String value) {
        this.address10 = value;
    }

    /**
     * Gets the value of the postCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the value of the postCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostCode(String value) {
        this.postCode = value;
    }

    /**
     * Gets the value of the telephone1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephone1() {
        return telephone1;
    }

    /**
     * Sets the value of the telephone1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephone1(String value) {
        this.telephone1 = value;
    }

    /**
     * Gets the value of the telephone2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephone2() {
        return telephone2;
    }

    /**
     * Sets the value of the telephone2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephone2(String value) {
        this.telephone2 = value;
    }

    /**
     * Gets the value of the telephone3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephone3() {
        return telephone3;
    }

    /**
     * Sets the value of the telephone3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephone3(String value) {
        this.telephone3 = value;
    }

    /**
     * Gets the value of the telephone4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephone4() {
        return telephone4;
    }

    /**
     * Sets the value of the telephone4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephone4(String value) {
        this.telephone4 = value;
    }

    /**
     * Gets the value of the email1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail1() {
        return email1;
    }

    /**
     * Sets the value of the email1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail1(String value) {
        this.email1 = value;
    }

    /**
     * Gets the value of the email2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail2() {
        return email2;
    }

    /**
     * Sets the value of the email2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail2(String value) {
        this.email2 = value;
    }

    /**
     * Gets the value of the email3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail3() {
        return email3;
    }

    /**
     * Sets the value of the email3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail3(String value) {
        this.email3 = value;
    }

    /**
     * Gets the value of the email4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail4() {
        return email4;
    }

    /**
     * Sets the value of the email4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail4(String value) {
        this.email4 = value;
    }

    /**
     * Gets the value of the fax1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax1() {
        return fax1;
    }

    /**
     * Sets the value of the fax1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax1(String value) {
        this.fax1 = value;
    }

    /**
     * Gets the value of the fax2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax2() {
        return fax2;
    }

    /**
     * Sets the value of the fax2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax2(String value) {
        this.fax2 = value;
    }

    /**
     * Gets the value of the fax3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax3() {
        return fax3;
    }

    /**
     * Sets the value of the fax3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax3(String value) {
        this.fax3 = value;
    }

    /**
     * Gets the value of the fax4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax4() {
        return fax4;
    }

    /**
     * Sets the value of the fax4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax4(String value) {
        this.fax4 = value;
    }

    /**
     * Gets the value of the smsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsNumber() {
        return smsNumber;
    }

    /**
     * Sets the value of the smsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsNumber(String value) {
        this.smsNumber = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
