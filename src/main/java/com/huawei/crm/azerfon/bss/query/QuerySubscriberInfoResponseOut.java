
package com.huawei.crm.azerfon.bss.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuerySubscriberInfoResponseOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuerySubscriberInfoResponseOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QuerySubscriberInfoRspList" type="{http://crm.huawei.com/azerfon/bss/query/}QuerySubscriberInfoRspItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubscriberInfoResponseOut", propOrder = {
    "querySubscriberInfoRspList"
})
public class QuerySubscriberInfoResponseOut {

    @XmlElement(name = "QuerySubscriberInfoRspList")
    protected List<QuerySubscriberInfoRspItem> querySubscriberInfoRspList;

    /**
     * Gets the value of the querySubscriberInfoRspList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the querySubscriberInfoRspList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuerySubscriberInfoRspList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuerySubscriberInfoRspItem }
     * 
     * 
     */
    public List<QuerySubscriberInfoRspItem> getQuerySubscriberInfoRspList() {
        if (querySubscriberInfoRspList == null) {
            querySubscriberInfoRspList = new ArrayList<QuerySubscriberInfoRspItem>();
        }
        return this.querySubscriberInfoRspList;
    }

}
