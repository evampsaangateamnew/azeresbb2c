
package com.huawei.crm.azerfon.bss.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.azerfon.bss.basetype.ExtParameterList;


/**
 * <p>Java class for GetSubscriberIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSubscriberIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ExternalSubscriberId" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExternalSubscriberId" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/azerfon/bss/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="IncludeOfferFlag" type="{http://crm.huawei.com/azerfon/bss/basetype/}IncludeOfferFlag"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="ICCID" type="{http://crm.huawei.com/azerfon/bss/basetype/}ICCID" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubscriberIn", propOrder = {

})
public class GetSubscriberIn {

    @XmlElement(name = "ExternalSubscriberId")
    protected String externalSubscriberId;
    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "IncludeOfferFlag", required = true)
    protected String includeOfferFlag;
    @XmlElement(name = "ServiceNumber")
    protected String serviceNumber;
    @XmlElement(name = "ICCID")
    protected String iccid;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the externalSubscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSubscriberId() {
        return externalSubscriberId;
    }

    /**
     * Sets the value of the externalSubscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSubscriberId(String value) {
        this.externalSubscriberId = value;
    }

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the includeOfferFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncludeOfferFlag() {
        return includeOfferFlag;
    }

    /**
     * Sets the value of the includeOfferFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncludeOfferFlag(String value) {
        this.includeOfferFlag = value;
    }

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICCID() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICCID(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
