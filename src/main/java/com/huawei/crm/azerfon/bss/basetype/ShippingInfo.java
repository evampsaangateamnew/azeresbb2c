
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShippingInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShippingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ShippingId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressId" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressId" minOccurs="0"/>
 *         &lt;element name="ShippingCarrierId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ShippingMode" type="{http://crm.huawei.com/azerfon/bss/basetype/}ShippingModeID"/>
 *         &lt;element name="Receiver">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ContactNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="ShippingTimeLimit" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ShippingBeginDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}DataType" minOccurs="0"/>
 *         &lt;element name="ShippingEndDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}DataType" minOccurs="0"/>
 *         &lt;element name="Instructions" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/azerfon/bss/basetype/}Remark" minOccurs="0"/>
 *         &lt;element name="PrintInvoiceFlag" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoiceType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoiceHeadType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoiceHeadName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="128"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoiceContentType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LogisticsShippingNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="128"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LogisticsLinkInfo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="128"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ShippingStatus" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShippingInfo", propOrder = {

})
public class ShippingInfo {

    @XmlElement(name = "ShippingId")
    protected String shippingId;
    @XmlElement(name = "AddressId")
    protected String addressId;
    @XmlElement(name = "ShippingCarrierId")
    protected String shippingCarrierId;
    @XmlElement(name = "ShippingMode", required = true)
    protected String shippingMode;
    @XmlElement(name = "Receiver", required = true)
    protected String receiver;
    @XmlElement(name = "ContactNumber")
    protected String contactNumber;
    @XmlElement(name = "ShippingTimeLimit")
    protected String shippingTimeLimit;
    @XmlElement(name = "ShippingBeginDate")
    protected String shippingBeginDate;
    @XmlElement(name = "ShippingEndDate")
    protected String shippingEndDate;
    @XmlElement(name = "Instructions")
    protected String instructions;
    @XmlElement(name = "Remark")
    protected String remark;
    @XmlElement(name = "PrintInvoiceFlag")
    protected String printInvoiceFlag;
    @XmlElement(name = "InvoiceType")
    protected String invoiceType;
    @XmlElement(name = "InvoiceHeadType")
    protected String invoiceHeadType;
    @XmlElement(name = "InvoiceHeadName")
    protected String invoiceHeadName;
    @XmlElement(name = "InvoiceContentType")
    protected String invoiceContentType;
    @XmlElement(name = "LogisticsShippingNumber")
    protected String logisticsShippingNumber;
    @XmlElement(name = "LogisticsLinkInfo")
    protected String logisticsLinkInfo;
    @XmlElement(name = "ShippingStatus")
    protected String shippingStatus;

    /**
     * Gets the value of the shippingId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingId() {
        return shippingId;
    }

    /**
     * Sets the value of the shippingId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingId(String value) {
        this.shippingId = value;
    }

    /**
     * Gets the value of the addressId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressId() {
        return addressId;
    }

    /**
     * Sets the value of the addressId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressId(String value) {
        this.addressId = value;
    }

    /**
     * Gets the value of the shippingCarrierId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingCarrierId() {
        return shippingCarrierId;
    }

    /**
     * Sets the value of the shippingCarrierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingCarrierId(String value) {
        this.shippingCarrierId = value;
    }

    /**
     * Gets the value of the shippingMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingMode() {
        return shippingMode;
    }

    /**
     * Sets the value of the shippingMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingMode(String value) {
        this.shippingMode = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiver(String value) {
        this.receiver = value;
    }

    /**
     * Gets the value of the contactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * Sets the value of the contactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactNumber(String value) {
        this.contactNumber = value;
    }

    /**
     * Gets the value of the shippingTimeLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingTimeLimit() {
        return shippingTimeLimit;
    }

    /**
     * Sets the value of the shippingTimeLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingTimeLimit(String value) {
        this.shippingTimeLimit = value;
    }

    /**
     * Gets the value of the shippingBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingBeginDate() {
        return shippingBeginDate;
    }

    /**
     * Sets the value of the shippingBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingBeginDate(String value) {
        this.shippingBeginDate = value;
    }

    /**
     * Gets the value of the shippingEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingEndDate() {
        return shippingEndDate;
    }

    /**
     * Sets the value of the shippingEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingEndDate(String value) {
        this.shippingEndDate = value;
    }

    /**
     * Gets the value of the instructions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstructions() {
        return instructions;
    }

    /**
     * Sets the value of the instructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstructions(String value) {
        this.instructions = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the printInvoiceFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrintInvoiceFlag() {
        return printInvoiceFlag;
    }

    /**
     * Sets the value of the printInvoiceFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrintInvoiceFlag(String value) {
        this.printInvoiceFlag = value;
    }

    /**
     * Gets the value of the invoiceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceType() {
        return invoiceType;
    }

    /**
     * Sets the value of the invoiceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceType(String value) {
        this.invoiceType = value;
    }

    /**
     * Gets the value of the invoiceHeadType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceHeadType() {
        return invoiceHeadType;
    }

    /**
     * Sets the value of the invoiceHeadType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceHeadType(String value) {
        this.invoiceHeadType = value;
    }

    /**
     * Gets the value of the invoiceHeadName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceHeadName() {
        return invoiceHeadName;
    }

    /**
     * Sets the value of the invoiceHeadName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceHeadName(String value) {
        this.invoiceHeadName = value;
    }

    /**
     * Gets the value of the invoiceContentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceContentType() {
        return invoiceContentType;
    }

    /**
     * Sets the value of the invoiceContentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceContentType(String value) {
        this.invoiceContentType = value;
    }

    /**
     * Gets the value of the logisticsShippingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogisticsShippingNumber() {
        return logisticsShippingNumber;
    }

    /**
     * Sets the value of the logisticsShippingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogisticsShippingNumber(String value) {
        this.logisticsShippingNumber = value;
    }

    /**
     * Gets the value of the logisticsLinkInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogisticsLinkInfo() {
        return logisticsLinkInfo;
    }

    /**
     * Sets the value of the logisticsLinkInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogisticsLinkInfo(String value) {
        this.logisticsLinkInfo = value;
    }

    /**
     * Gets the value of the shippingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingStatus() {
        return shippingStatus;
    }

    /**
     * Sets the value of the shippingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingStatus(String value) {
        this.shippingStatus = value;
    }

}
