
package com.huawei.crm.azerfon.bss.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OfferingInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingId"/>
 *         &lt;element name="EffectiveTime" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpireTime" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="NextCycleTime" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExtOfferingParam" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingInformation", propOrder = {
    "offeringId",
    "effectiveTime",
    "expireTime",
    "nextCycleTime",
    "extOfferingParam"
})
public class OfferingInformation {

    @XmlElement(name = "OfferingId", required = true)
    protected String offeringId;
    @XmlElement(name = "EffectiveTime")
    protected String effectiveTime;
    @XmlElement(name = "ExpireTime")
    protected String expireTime;
    @XmlElement(name = "NextCycleTime")
    protected String nextCycleTime;
    @XmlElement(name = "ExtOfferingParam")
    protected List<ExtParameterList> extOfferingParam;

    /**
     * Gets the value of the offeringId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingId() {
        return offeringId;
    }

    /**
     * Sets the value of the offeringId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingId(String value) {
        this.offeringId = value;
    }

    /**
     * Gets the value of the effectiveTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveTime() {
        return effectiveTime;
    }

    /**
     * Sets the value of the effectiveTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveTime(String value) {
        this.effectiveTime = value;
    }

    /**
     * Gets the value of the expireTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpireTime() {
        return expireTime;
    }

    /**
     * Sets the value of the expireTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpireTime(String value) {
        this.expireTime = value;
    }

    /**
     * Gets the value of the nextCycleTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextCycleTime() {
        return nextCycleTime;
    }

    /**
     * Sets the value of the nextCycleTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextCycleTime(String value) {
        this.nextCycleTime = value;
    }

    /**
     * Gets the value of the extOfferingParam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extOfferingParam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtOfferingParam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtParameterList }
     * 
     * 
     */
    public List<ExtParameterList> getExtOfferingParam() {
        if (extOfferingParam == null) {
            extOfferingParam = new ArrayList<ExtParameterList>();
        }
        return this.extOfferingParam;
    }

}
