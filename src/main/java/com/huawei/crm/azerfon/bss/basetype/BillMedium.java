
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillMedium complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillMedium">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="BillMediumId" type="{http://crm.huawei.com/azerfon/bss/basetype/}BillMediumId" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/azerfon/bss/basetype/}ActionType" minOccurs="0"/>
 *         &lt;element name="MediumType" type="{http://crm.huawei.com/azerfon/bss/basetype/}MediumType" minOccurs="0"/>
 *         &lt;element name="ContentMode" type="{http://crm.huawei.com/azerfon/bss/basetype/}ContentMode" minOccurs="0"/>
 *         &lt;element name="MediumContent" type="{http://crm.huawei.com/azerfon/bss/basetype/}MediumContent" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillMedium", propOrder = {

})
public class BillMedium {

    @XmlElement(name = "BillMediumId")
    protected String billMediumId;
    @XmlElement(name = "ActionType")
    protected String actionType;
    @XmlElement(name = "MediumType")
    protected String mediumType;
    @XmlElement(name = "ContentMode")
    protected String contentMode;
    @XmlElement(name = "MediumContent")
    protected String mediumContent;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the billMediumId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillMediumId() {
        return billMediumId;
    }

    /**
     * Sets the value of the billMediumId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillMediumId(String value) {
        this.billMediumId = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the mediumType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediumType() {
        return mediumType;
    }

    /**
     * Sets the value of the mediumType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediumType(String value) {
        this.mediumType = value;
    }

    /**
     * Gets the value of the contentMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentMode() {
        return contentMode;
    }

    /**
     * Sets the value of the contentMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentMode(String value) {
        this.contentMode = value;
    }

    /**
     * Gets the value of the mediumContent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediumContent() {
        return mediumContent;
    }

    /**
     * Sets the value of the mediumContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediumContent(String value) {
        this.mediumContent = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
