
package com.huawei.crm.azerfon.bss.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RechargeFeeList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RechargeFeeList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RechargeFeeInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}RechargeFeeInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RechargeFeeList", propOrder = {
    "rechargeFeeInfo"
})
public class RechargeFeeList {

    @XmlElement(name = "RechargeFeeInfo", required = true)
    protected List<RechargeFeeInfo> rechargeFeeInfo;

    /**
     * Gets the value of the rechargeFeeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rechargeFeeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRechargeFeeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RechargeFeeInfo }
     * 
     * 
     */
    public List<RechargeFeeInfo> getRechargeFeeInfo() {
        if (rechargeFeeInfo == null) {
            rechargeFeeInfo = new ArrayList<RechargeFeeInfo>();
        }
        return this.rechargeFeeInfo;
    }

}
