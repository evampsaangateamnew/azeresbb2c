
package com.huawei.crm.azerfon.bss.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.azerfon.bss.basetype.ResponseHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/azerfon/bss/basetype/}ResponseHeader"/>
 *         &lt;element name="GetLatestBillFileBody" type="{http://crm.huawei.com/azerfon/bss/query/}GetLatestBillFileOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseHeader",
    "getLatestBillFileBody"
})
@XmlRootElement(name = "GetLatestBillFileResponse")
public class GetLatestBillFileResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(name = "GetLatestBillFileBody")
    protected GetLatestBillFileOut getLatestBillFileBody;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the getLatestBillFileBody property.
     * 
     * @return
     *     possible object is
     *     {@link GetLatestBillFileOut }
     *     
     */
    public GetLatestBillFileOut getGetLatestBillFileBody() {
        return getLatestBillFileBody;
    }

    /**
     * Sets the value of the getLatestBillFileBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLatestBillFileOut }
     *     
     */
    public void setGetLatestBillFileBody(GetLatestBillFileOut value) {
        this.getLatestBillFileBody = value;
    }

}
