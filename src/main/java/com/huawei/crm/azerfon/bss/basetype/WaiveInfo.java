
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WaiveInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WaiveInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ReasonCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}ReasonCode" minOccurs="0"/>
 *         &lt;element name="WaiveFee" type="{http://crm.huawei.com/azerfon/bss/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="WaiveOperID" type="{http://crm.huawei.com/azerfon/bss/basetype/}OperatorId" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WaiveInfo", propOrder = {

})
public class WaiveInfo {

    @XmlElement(name = "ReasonCode")
    protected String reasonCode;
    @XmlElement(name = "WaiveFee")
    protected Long waiveFee;
    @XmlElement(name = "WaiveOperID")
    protected String waiveOperID;

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonCode(String value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the waiveFee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getWaiveFee() {
        return waiveFee;
    }

    /**
     * Sets the value of the waiveFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setWaiveFee(Long value) {
        this.waiveFee = value;
    }

    /**
     * Gets the value of the waiveOperID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaiveOperID() {
        return waiveOperID;
    }

    /**
     * Sets the value of the waiveOperID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaiveOperID(String value) {
        this.waiveOperID = value;
    }

}
