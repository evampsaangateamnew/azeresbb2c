
package com.huawei.crm.azerfon.bss.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSubOfferingList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSubOfferingList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSubOfferingInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}GetSubOfferingInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubOfferingList", propOrder = {
    "getSubOfferingInfo"
})
public class GetSubOfferingList {

    @XmlElement(name = "GetSubOfferingInfo", required = true)
    protected List<GetSubOfferingInfo> getSubOfferingInfo;

    /**
     * Gets the value of the getSubOfferingInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getSubOfferingInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetSubOfferingInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetSubOfferingInfo }
     * 
     * 
     */
    public List<GetSubOfferingInfo> getGetSubOfferingInfo() {
        if (getSubOfferingInfo == null) {
            getSubOfferingInfo = new ArrayList<GetSubOfferingInfo>();
        }
        return this.getSubOfferingInfo;
    }

}
