
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegisterCustInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegisterCustInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustInfo"/>
 *         &lt;element name="ManagerList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ManagerList" minOccurs="0"/>
 *         &lt;element name="AddressList" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressList" minOccurs="0"/>
 *         &lt;element name="ContactList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ContactList" minOccurs="0"/>
 *         &lt;element name="CustomerDocumentList" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerDocumentList" minOccurs="0"/>
 *         &lt;element name="DNESettingList" type="{http://crm.huawei.com/azerfon/bss/basetype/}DNESettingList" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegisterCustInfo", propOrder = {
    "custInfo",
    "managerList",
    "addressList",
    "contactList",
    "customerDocumentList",
    "dneSettingList",
    "supplementaryOfferingList",
    "extParamList"
})
public class RegisterCustInfo {

    @XmlElement(name = "CustInfo", required = true)
    protected CustInfo custInfo;
    @XmlElement(name = "ManagerList")
    protected ManagerList managerList;
    @XmlElement(name = "AddressList")
    protected AddressList addressList;
    @XmlElement(name = "ContactList")
    protected ContactList contactList;
    @XmlElement(name = "CustomerDocumentList")
    protected CustomerDocumentList customerDocumentList;
    @XmlElement(name = "DNESettingList")
    protected DNESettingList dneSettingList;
    @XmlElement(name = "SupplementaryOfferingList")
    protected OfferingList supplementaryOfferingList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the custInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CustInfo }
     *     
     */
    public CustInfo getCustInfo() {
        return custInfo;
    }

    /**
     * Sets the value of the custInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustInfo }
     *     
     */
    public void setCustInfo(CustInfo value) {
        this.custInfo = value;
    }

    /**
     * Gets the value of the managerList property.
     * 
     * @return
     *     possible object is
     *     {@link ManagerList }
     *     
     */
    public ManagerList getManagerList() {
        return managerList;
    }

    /**
     * Sets the value of the managerList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ManagerList }
     *     
     */
    public void setManagerList(ManagerList value) {
        this.managerList = value;
    }

    /**
     * Gets the value of the addressList property.
     * 
     * @return
     *     possible object is
     *     {@link AddressList }
     *     
     */
    public AddressList getAddressList() {
        return addressList;
    }

    /**
     * Sets the value of the addressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressList }
     *     
     */
    public void setAddressList(AddressList value) {
        this.addressList = value;
    }

    /**
     * Gets the value of the contactList property.
     * 
     * @return
     *     possible object is
     *     {@link ContactList }
     *     
     */
    public ContactList getContactList() {
        return contactList;
    }

    /**
     * Sets the value of the contactList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactList }
     *     
     */
    public void setContactList(ContactList value) {
        this.contactList = value;
    }

    /**
     * Gets the value of the customerDocumentList property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerDocumentList }
     *     
     */
    public CustomerDocumentList getCustomerDocumentList() {
        return customerDocumentList;
    }

    /**
     * Sets the value of the customerDocumentList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerDocumentList }
     *     
     */
    public void setCustomerDocumentList(CustomerDocumentList value) {
        this.customerDocumentList = value;
    }

    /**
     * Gets the value of the dneSettingList property.
     * 
     * @return
     *     possible object is
     *     {@link DNESettingList }
     *     
     */
    public DNESettingList getDNESettingList() {
        return dneSettingList;
    }

    /**
     * Sets the value of the dneSettingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DNESettingList }
     *     
     */
    public void setDNESettingList(DNESettingList value) {
        this.dneSettingList = value;
    }

    /**
     * Gets the value of the supplementaryOfferingList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingList }
     *     
     */
    public OfferingList getSupplementaryOfferingList() {
        return supplementaryOfferingList;
    }

    /**
     * Sets the value of the supplementaryOfferingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingList }
     *     
     */
    public void setSupplementaryOfferingList(OfferingList value) {
        this.supplementaryOfferingList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
