
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgreementSpecId" type="{http://crm.huawei.com/azerfon/bss/basetype/}AgreementSpecId" minOccurs="0"/>
 *         &lt;element name="DurationUnit" type="{http://crm.huawei.com/azerfon/bss/basetype/}DurationUnit" minOccurs="0"/>
 *         &lt;element name="DurationValue" type="{http://crm.huawei.com/azerfon/bss/basetype/}DurationValue" minOccurs="0"/>
 *         &lt;element name="ProlongationList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ProlongationList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractInfo", propOrder = {
    "agreementSpecId",
    "durationUnit",
    "durationValue",
    "prolongationList",
    "extParamList"
})
public class ContractInfo {

    @XmlElement(name = "AgreementSpecId")
    protected String agreementSpecId;
    @XmlElement(name = "DurationUnit")
    protected String durationUnit;
    @XmlElement(name = "DurationValue")
    protected String durationValue;
    @XmlElement(name = "ProlongationList")
    protected ProlongationList prolongationList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the agreementSpecId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgreementSpecId() {
        return agreementSpecId;
    }

    /**
     * Sets the value of the agreementSpecId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgreementSpecId(String value) {
        this.agreementSpecId = value;
    }

    /**
     * Gets the value of the durationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationUnit() {
        return durationUnit;
    }

    /**
     * Sets the value of the durationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationUnit(String value) {
        this.durationUnit = value;
    }

    /**
     * Gets the value of the durationValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationValue() {
        return durationValue;
    }

    /**
     * Sets the value of the durationValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationValue(String value) {
        this.durationValue = value;
    }

    /**
     * Gets the value of the prolongationList property.
     * 
     * @return
     *     possible object is
     *     {@link ProlongationList }
     *     
     */
    public ProlongationList getProlongationList() {
        return prolongationList;
    }

    /**
     * Sets the value of the prolongationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProlongationList }
     *     
     */
    public void setProlongationList(ProlongationList value) {
        this.prolongationList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
