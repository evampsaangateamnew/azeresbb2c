
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IndividualInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IndividualInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CertificateType" type="{http://crm.huawei.com/azerfon/bss/basetype/}CertificateType" minOccurs="0"/>
 *         &lt;element name="CertificateNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}CertificateNumber" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://crm.huawei.com/azerfon/bss/basetype/}Title" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://crm.huawei.com/azerfon/bss/basetype/}FirstName" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://crm.huawei.com/azerfon/bss/basetype/}MiddleName" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://crm.huawei.com/azerfon/bss/basetype/}LastName" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://crm.huawei.com/azerfon/bss/basetype/}Nationality" minOccurs="0"/>
 *         &lt;element name="Gender" type="{http://crm.huawei.com/azerfon/bss/basetype/}Gender" minOccurs="0"/>
 *         &lt;element name="DateOfBirth" type="{http://crm.huawei.com/azerfon/bss/basetype/}DateOfBirth" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/azerfon/bss/basetype/}Remark" minOccurs="0"/>
 *         &lt;element name="DPAInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}DPAInfo" minOccurs="0"/>
 *         &lt;element name="FamilyMemberList" type="{http://crm.huawei.com/azerfon/bss/basetype/}FamliyMemberList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndividualInfo", propOrder = {

})
public class IndividualInfo {

    @XmlElement(name = "CertificateType")
    protected String certificateType;
    @XmlElement(name = "CertificateNumber")
    protected String certificateNumber;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "Nationality")
    protected String nationality;
    @XmlElement(name = "Gender")
    protected String gender;
    @XmlElement(name = "DateOfBirth")
    protected String dateOfBirth;
    @XmlElement(name = "Remark")
    protected String remark;
    @XmlElement(name = "DPAInfo")
    protected DPAInfo dpaInfo;
    @XmlElement(name = "FamilyMemberList")
    protected FamliyMemberList familyMemberList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the certificateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateType() {
        return certificateType;
    }

    /**
     * Sets the value of the certificateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateType(String value) {
        this.certificateType = value;
    }

    /**
     * Gets the value of the certificateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateNumber() {
        return certificateNumber;
    }

    /**
     * Sets the value of the certificateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateNumber(String value) {
        this.certificateNumber = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfBirth(String value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the dpaInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DPAInfo }
     *     
     */
    public DPAInfo getDPAInfo() {
        return dpaInfo;
    }

    /**
     * Sets the value of the dpaInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DPAInfo }
     *     
     */
    public void setDPAInfo(DPAInfo value) {
        this.dpaInfo = value;
    }

    /**
     * Gets the value of the familyMemberList property.
     * 
     * @return
     *     possible object is
     *     {@link FamliyMemberList }
     *     
     */
    public FamliyMemberList getFamilyMemberList() {
        return familyMemberList;
    }

    /**
     * Sets the value of the familyMemberList property.
     * 
     * @param value
     *     allowed object is
     *     {@link FamliyMemberList }
     *     
     */
    public void setFamilyMemberList(FamliyMemberList value) {
        this.familyMemberList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
