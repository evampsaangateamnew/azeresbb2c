
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubscriberInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AcctRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="SubRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="CustomerRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="ActualCustomer" type="{http://crm.huawei.com/azerfon/bss/basetype/}ActualCustInfo" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/azerfon/bss/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="AccountList" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountList" minOccurs="0"/>
 *         &lt;element name="OperateType" type="{http://crm.huawei.com/azerfon/bss/basetype/}OperateType" minOccurs="0"/>
 *         &lt;element name="ActivateFlag" type="{http://crm.huawei.com/azerfon/bss/basetype/}ActivateFlag" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="OldServiceNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="IMEI" type="{http://crm.huawei.com/azerfon/bss/basetype/}IMEI" minOccurs="0"/>
 *         &lt;element name="ICCID" type="{http://crm.huawei.com/azerfon/bss/basetype/}ICCID" minOccurs="0"/>
 *         &lt;element name="OldICCID" type="{http://crm.huawei.com/azerfon/bss/basetype/}ICCID" minOccurs="0"/>
 *         &lt;element name="RelaSubEntity" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaSubEntity" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/azerfon/bss/basetype/}SubscriberStatus" minOccurs="0"/>
 *         &lt;element name="OwnerOrgId" type="{http://crm.huawei.com/azerfon/bss/basetype/}string" minOccurs="0"/>
 *         &lt;element name="DealerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}DealerId" minOccurs="0"/>
 *         &lt;element name="BlackFlag" type="{http://crm.huawei.com/azerfon/bss/basetype/}BlackFlag" minOccurs="0"/>
 *         &lt;element name="InvalidAuthPin" type="{http://crm.huawei.com/azerfon/bss/basetype/}InvalidAuthPin" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://crm.huawei.com/azerfon/bss/basetype/}Language" minOccurs="0"/>
 *         &lt;element name="WrittenLanguage" type="{http://crm.huawei.com/azerfon/bss/basetype/}WrittenLanguage" minOccurs="0"/>
 *         &lt;element name="PasswordInfoList" type="{http://crm.huawei.com/azerfon/bss/basetype/}PasswordInfoList" minOccurs="0"/>
 *         &lt;element name="ConsumptionLimitList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ConsumptionLimitList" minOccurs="0"/>
 *         &lt;element name="MNPPort" type="{http://crm.huawei.com/azerfon/bss/basetype/}MNPInfo" minOccurs="0"/>
 *         &lt;element name="DunningFlag" type="{http://crm.huawei.com/azerfon/bss/basetype/}DunningFlag" minOccurs="0"/>
 *         &lt;element name="PrimaryOfferingInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingInfo" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingList" minOccurs="0"/>
 *         &lt;element name="NetworkSettingList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtProductList" minOccurs="0"/>
 *         &lt;element name="PromotionList" type="{http://crm.huawei.com/azerfon/bss/basetype/}PromotionList" minOccurs="0"/>
 *         &lt;element name="AddressList" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressList" minOccurs="0"/>
 *         &lt;element name="ContactList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ContactList" minOccurs="0"/>
 *         &lt;element name="DNESettingList" type="{http://crm.huawei.com/azerfon/bss/basetype/}DNESettingList" minOccurs="0"/>
 *         &lt;element name="CallScreenList" type="{http://crm.huawei.com/azerfon/bss/basetype/}CallScreenList" minOccurs="0"/>
 *         &lt;element name="DPAInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}DPAInfo" minOccurs="0"/>
 *         &lt;element name="PointList" type="{http://crm.huawei.com/azerfon/bss/basetype/}PointList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriberInfo", propOrder = {

})
public class SubscriberInfo {

    @XmlElement(name = "AcctRelaId")
    protected String acctRelaId;
    @XmlElement(name = "SubRelaId")
    protected String subRelaId;
    @XmlElement(name = "CustomerRelaId")
    protected String customerRelaId;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "ActualCustomer")
    protected ActualCustInfo actualCustomer;
    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "AccountId")
    protected Long accountId;
    @XmlElement(name = "AccountList")
    protected AccountList accountList;
    @XmlElement(name = "OperateType")
    protected String operateType;
    @XmlElement(name = "ActivateFlag")
    protected String activateFlag;
    @XmlElement(name = "ServiceNumber")
    protected String serviceNumber;
    @XmlElement(name = "OldServiceNumber")
    protected String oldServiceNumber;
    @XmlElement(name = "IMEI")
    protected String imei;
    @XmlElement(name = "ICCID")
    protected String iccid;
    @XmlElement(name = "OldICCID")
    protected String oldICCID;
    @XmlElement(name = "RelaSubEntity")
    protected String relaSubEntity;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "OwnerOrgId")
    protected String ownerOrgId;
    @XmlElement(name = "DealerId")
    protected String dealerId;
    @XmlElement(name = "BlackFlag")
    protected String blackFlag;
    @XmlElement(name = "InvalidAuthPin")
    protected String invalidAuthPin;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "WrittenLanguage")
    protected String writtenLanguage;
    @XmlElement(name = "PasswordInfoList")
    protected PasswordInfoList passwordInfoList;
    @XmlElement(name = "ConsumptionLimitList")
    protected ConsumptionLimitList consumptionLimitList;
    @XmlElement(name = "MNPPort")
    protected MNPInfo mnpPort;
    @XmlElement(name = "DunningFlag")
    protected String dunningFlag;
    @XmlElement(name = "PrimaryOfferingInfo")
    protected OfferingInfo primaryOfferingInfo;
    @XmlElement(name = "SupplementaryOfferingList")
    protected OfferingList supplementaryOfferingList;
    @XmlElement(name = "NetworkSettingList")
    protected ExtProductList networkSettingList;
    @XmlElement(name = "PromotionList")
    protected PromotionList promotionList;
    @XmlElement(name = "AddressList")
    protected AddressList addressList;
    @XmlElement(name = "ContactList")
    protected ContactList contactList;
    @XmlElement(name = "DNESettingList")
    protected DNESettingList dneSettingList;
    @XmlElement(name = "CallScreenList")
    protected CallScreenList callScreenList;
    @XmlElement(name = "DPAInfo")
    protected DPAInfo dpaInfo;
    @XmlElement(name = "PointList")
    protected PointList pointList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the acctRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctRelaId() {
        return acctRelaId;
    }

    /**
     * Sets the value of the acctRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctRelaId(String value) {
        this.acctRelaId = value;
    }

    /**
     * Gets the value of the subRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubRelaId() {
        return subRelaId;
    }

    /**
     * Sets the value of the subRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubRelaId(String value) {
        this.subRelaId = value;
    }

    /**
     * Gets the value of the customerRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRelaId() {
        return customerRelaId;
    }

    /**
     * Sets the value of the customerRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRelaId(String value) {
        this.customerRelaId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the actualCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link ActualCustInfo }
     *     
     */
    public ActualCustInfo getActualCustomer() {
        return actualCustomer;
    }

    /**
     * Sets the value of the actualCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActualCustInfo }
     *     
     */
    public void setActualCustomer(ActualCustInfo value) {
        this.actualCustomer = value;
    }

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountId(Long value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the accountList property.
     * 
     * @return
     *     possible object is
     *     {@link AccountList }
     *     
     */
    public AccountList getAccountList() {
        return accountList;
    }

    /**
     * Sets the value of the accountList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountList }
     *     
     */
    public void setAccountList(AccountList value) {
        this.accountList = value;
    }

    /**
     * Gets the value of the operateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperateType() {
        return operateType;
    }

    /**
     * Sets the value of the operateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperateType(String value) {
        this.operateType = value;
    }

    /**
     * Gets the value of the activateFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivateFlag() {
        return activateFlag;
    }

    /**
     * Sets the value of the activateFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivateFlag(String value) {
        this.activateFlag = value;
    }

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the oldServiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldServiceNumber() {
        return oldServiceNumber;
    }

    /**
     * Sets the value of the oldServiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldServiceNumber(String value) {
        this.oldServiceNumber = value;
    }

    /**
     * Gets the value of the imei property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMEI() {
        return imei;
    }

    /**
     * Sets the value of the imei property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMEI(String value) {
        this.imei = value;
    }

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICCID() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICCID(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the oldICCID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldICCID() {
        return oldICCID;
    }

    /**
     * Sets the value of the oldICCID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldICCID(String value) {
        this.oldICCID = value;
    }

    /**
     * Gets the value of the relaSubEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaSubEntity() {
        return relaSubEntity;
    }

    /**
     * Sets the value of the relaSubEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaSubEntity(String value) {
        this.relaSubEntity = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the ownerOrgId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerOrgId() {
        return ownerOrgId;
    }

    /**
     * Sets the value of the ownerOrgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerOrgId(String value) {
        this.ownerOrgId = value;
    }

    /**
     * Gets the value of the dealerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * Sets the value of the dealerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerId(String value) {
        this.dealerId = value;
    }

    /**
     * Gets the value of the blackFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlackFlag() {
        return blackFlag;
    }

    /**
     * Sets the value of the blackFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlackFlag(String value) {
        this.blackFlag = value;
    }

    /**
     * Gets the value of the invalidAuthPin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvalidAuthPin() {
        return invalidAuthPin;
    }

    /**
     * Sets the value of the invalidAuthPin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvalidAuthPin(String value) {
        this.invalidAuthPin = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the writtenLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWrittenLanguage() {
        return writtenLanguage;
    }

    /**
     * Sets the value of the writtenLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWrittenLanguage(String value) {
        this.writtenLanguage = value;
    }

    /**
     * Gets the value of the passwordInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link PasswordInfoList }
     *     
     */
    public PasswordInfoList getPasswordInfoList() {
        return passwordInfoList;
    }

    /**
     * Sets the value of the passwordInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordInfoList }
     *     
     */
    public void setPasswordInfoList(PasswordInfoList value) {
        this.passwordInfoList = value;
    }

    /**
     * Gets the value of the consumptionLimitList property.
     * 
     * @return
     *     possible object is
     *     {@link ConsumptionLimitList }
     *     
     */
    public ConsumptionLimitList getConsumptionLimitList() {
        return consumptionLimitList;
    }

    /**
     * Sets the value of the consumptionLimitList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsumptionLimitList }
     *     
     */
    public void setConsumptionLimitList(ConsumptionLimitList value) {
        this.consumptionLimitList = value;
    }

    /**
     * Gets the value of the mnpPort property.
     * 
     * @return
     *     possible object is
     *     {@link MNPInfo }
     *     
     */
    public MNPInfo getMNPPort() {
        return mnpPort;
    }

    /**
     * Sets the value of the mnpPort property.
     * 
     * @param value
     *     allowed object is
     *     {@link MNPInfo }
     *     
     */
    public void setMNPPort(MNPInfo value) {
        this.mnpPort = value;
    }

    /**
     * Gets the value of the dunningFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDunningFlag() {
        return dunningFlag;
    }

    /**
     * Sets the value of the dunningFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDunningFlag(String value) {
        this.dunningFlag = value;
    }

    /**
     * Gets the value of the primaryOfferingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingInfo }
     *     
     */
    public OfferingInfo getPrimaryOfferingInfo() {
        return primaryOfferingInfo;
    }

    /**
     * Sets the value of the primaryOfferingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingInfo }
     *     
     */
    public void setPrimaryOfferingInfo(OfferingInfo value) {
        this.primaryOfferingInfo = value;
    }

    /**
     * Gets the value of the supplementaryOfferingList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingList }
     *     
     */
    public OfferingList getSupplementaryOfferingList() {
        return supplementaryOfferingList;
    }

    /**
     * Sets the value of the supplementaryOfferingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingList }
     *     
     */
    public void setSupplementaryOfferingList(OfferingList value) {
        this.supplementaryOfferingList = value;
    }

    /**
     * Gets the value of the networkSettingList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtProductList }
     *     
     */
    public ExtProductList getNetworkSettingList() {
        return networkSettingList;
    }

    /**
     * Sets the value of the networkSettingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtProductList }
     *     
     */
    public void setNetworkSettingList(ExtProductList value) {
        this.networkSettingList = value;
    }

    /**
     * Gets the value of the promotionList property.
     * 
     * @return
     *     possible object is
     *     {@link PromotionList }
     *     
     */
    public PromotionList getPromotionList() {
        return promotionList;
    }

    /**
     * Sets the value of the promotionList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PromotionList }
     *     
     */
    public void setPromotionList(PromotionList value) {
        this.promotionList = value;
    }

    /**
     * Gets the value of the addressList property.
     * 
     * @return
     *     possible object is
     *     {@link AddressList }
     *     
     */
    public AddressList getAddressList() {
        return addressList;
    }

    /**
     * Sets the value of the addressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressList }
     *     
     */
    public void setAddressList(AddressList value) {
        this.addressList = value;
    }

    /**
     * Gets the value of the contactList property.
     * 
     * @return
     *     possible object is
     *     {@link ContactList }
     *     
     */
    public ContactList getContactList() {
        return contactList;
    }

    /**
     * Sets the value of the contactList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactList }
     *     
     */
    public void setContactList(ContactList value) {
        this.contactList = value;
    }

    /**
     * Gets the value of the dneSettingList property.
     * 
     * @return
     *     possible object is
     *     {@link DNESettingList }
     *     
     */
    public DNESettingList getDNESettingList() {
        return dneSettingList;
    }

    /**
     * Sets the value of the dneSettingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DNESettingList }
     *     
     */
    public void setDNESettingList(DNESettingList value) {
        this.dneSettingList = value;
    }

    /**
     * Gets the value of the callScreenList property.
     * 
     * @return
     *     possible object is
     *     {@link CallScreenList }
     *     
     */
    public CallScreenList getCallScreenList() {
        return callScreenList;
    }

    /**
     * Sets the value of the callScreenList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CallScreenList }
     *     
     */
    public void setCallScreenList(CallScreenList value) {
        this.callScreenList = value;
    }

    /**
     * Gets the value of the dpaInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DPAInfo }
     *     
     */
    public DPAInfo getDPAInfo() {
        return dpaInfo;
    }

    /**
     * Sets the value of the dpaInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DPAInfo }
     *     
     */
    public void setDPAInfo(DPAInfo value) {
        this.dpaInfo = value;
    }

    /**
     * Gets the value of the pointList property.
     * 
     * @return
     *     possible object is
     *     {@link PointList }
     *     
     */
    public PointList getPointList() {
        return pointList;
    }

    /**
     * Sets the value of the pointList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointList }
     *     
     */
    public void setPointList(PointList value) {
        this.pointList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
