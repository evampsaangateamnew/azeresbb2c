
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderItemInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderItemInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OrderItemType" type="{http://crm.huawei.com/azerfon/bss/basetype/}OrderItemType"/>
 *         &lt;element name="ExternalOrderItemId" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExternalOrderItemId" minOccurs="0"/>
 *         &lt;element name="OrderItemId" type="{http://crm.huawei.com/azerfon/bss/basetype/}InternalOrderItemId" minOccurs="0"/>
 *         &lt;element name="ExternalCustomerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExternalCustomerId" minOccurs="0"/>
 *         &lt;element name="ExternalAccountId" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExternalAccountId" minOccurs="0"/>
 *         &lt;element name="ExternalSubscriberId" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExternalSubscriberId" minOccurs="0"/>
 *         &lt;element name="ReasonType" type="{http://crm.huawei.com/azerfon/bss/basetype/}ReasonType" minOccurs="0"/>
 *         &lt;element name="ReasonCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}ReasonCode" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/azerfon/bss/basetype/}Remark" minOccurs="0"/>
 *         &lt;element name="IsCustomerNotification" type="{http://crm.huawei.com/azerfon/bss/basetype/}IsCustomerNotification" minOccurs="0"/>
 *         &lt;element name="IsPartnerNotification" type="{http://crm.huawei.com/azerfon/bss/basetype/}IsPartnerNotification" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderItemInfo", propOrder = {

})
public class OrderItemInfo {

    @XmlElement(name = "OrderItemType", required = true)
    protected String orderItemType;
    @XmlElement(name = "ExternalOrderItemId")
    protected String externalOrderItemId;
    @XmlElement(name = "OrderItemId")
    protected String orderItemId;
    @XmlElement(name = "ExternalCustomerId")
    protected String externalCustomerId;
    @XmlElement(name = "ExternalAccountId")
    protected String externalAccountId;
    @XmlElement(name = "ExternalSubscriberId")
    protected String externalSubscriberId;
    @XmlElement(name = "ReasonType")
    protected String reasonType;
    @XmlElement(name = "ReasonCode")
    protected String reasonCode;
    @XmlElement(name = "Remark")
    protected String remark;
    @XmlElement(name = "IsCustomerNotification")
    protected String isCustomerNotification;
    @XmlElement(name = "IsPartnerNotification")
    protected String isPartnerNotification;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the orderItemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderItemType() {
        return orderItemType;
    }

    /**
     * Sets the value of the orderItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderItemType(String value) {
        this.orderItemType = value;
    }

    /**
     * Gets the value of the externalOrderItemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalOrderItemId() {
        return externalOrderItemId;
    }

    /**
     * Sets the value of the externalOrderItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalOrderItemId(String value) {
        this.externalOrderItemId = value;
    }

    /**
     * Gets the value of the orderItemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderItemId() {
        return orderItemId;
    }

    /**
     * Sets the value of the orderItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderItemId(String value) {
        this.orderItemId = value;
    }

    /**
     * Gets the value of the externalCustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalCustomerId() {
        return externalCustomerId;
    }

    /**
     * Sets the value of the externalCustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalCustomerId(String value) {
        this.externalCustomerId = value;
    }

    /**
     * Gets the value of the externalAccountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalAccountId() {
        return externalAccountId;
    }

    /**
     * Sets the value of the externalAccountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalAccountId(String value) {
        this.externalAccountId = value;
    }

    /**
     * Gets the value of the externalSubscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSubscriberId() {
        return externalSubscriberId;
    }

    /**
     * Sets the value of the externalSubscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSubscriberId(String value) {
        this.externalSubscriberId = value;
    }

    /**
     * Gets the value of the reasonType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonType() {
        return reasonType;
    }

    /**
     * Sets the value of the reasonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonType(String value) {
        this.reasonType = value;
    }

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonCode(String value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the isCustomerNotification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCustomerNotification() {
        return isCustomerNotification;
    }

    /**
     * Sets the value of the isCustomerNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCustomerNotification(String value) {
        this.isCustomerNotification = value;
    }

    /**
     * Gets the value of the isPartnerNotification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPartnerNotification() {
        return isPartnerNotification;
    }

    /**
     * Sets the value of the isPartnerNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPartnerNotification(String value) {
        this.isPartnerNotification = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
