
package com.huawei.crm.azerfon.bss.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RewardItemIdList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RewardItemIdList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RewardItemId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RewardItemId" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RewardItemIdList", propOrder = {
    "rewardItemId"
})
public class RewardItemIdList {

    @XmlElement(name = "RewardItemId", required = true)
    protected List<String> rewardItemId;

    /**
     * Gets the value of the rewardItemId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rewardItemId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRewardItemId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRewardItemId() {
        if (rewardItemId == null) {
            rewardItemId = new ArrayList<String>();
        }
        return this.rewardItemId;
    }

}
