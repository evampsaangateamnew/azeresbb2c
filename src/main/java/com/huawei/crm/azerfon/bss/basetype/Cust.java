
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * CustomerBo.xml
 * 
 * <p>Java class for Cust complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cust">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://crm.huawei.com/azerfon/bss/basetype/}CustInfo">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddressList" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressList" minOccurs="0"/>
 *         &lt;element name="ContactList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ContactList" minOccurs="0"/>
 *         &lt;element name="CustomerDocumentList" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerDocumentList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *         &lt;element name="DNESettingList" type="{http://crm.huawei.com/azerfon/bss/basetype/}DNESettingList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cust", propOrder = {

})
public class Cust {

    @XmlElement(name = "CustomerInfo")
    protected Cust.CustomerInfo customerInfo;
    @XmlElement(name = "AddressList")
    protected AddressList addressList;
    @XmlElement(name = "ContactList")
    protected ContactList contactList;
    @XmlElement(name = "CustomerDocumentList")
    protected CustomerDocumentList customerDocumentList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;
    @XmlElement(name = "DNESettingList")
    protected DNESettingList dneSettingList;

    /**
     * Gets the value of the customerInfo property.
     * 
     * @return
     *     possible object is
     *     {@link Cust.CustomerInfo }
     *     
     */
    public Cust.CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    /**
     * Sets the value of the customerInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cust.CustomerInfo }
     *     
     */
    public void setCustomerInfo(Cust.CustomerInfo value) {
        this.customerInfo = value;
    }

    /**
     * Gets the value of the addressList property.
     * 
     * @return
     *     possible object is
     *     {@link AddressList }
     *     
     */
    public AddressList getAddressList() {
        return addressList;
    }

    /**
     * Sets the value of the addressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressList }
     *     
     */
    public void setAddressList(AddressList value) {
        this.addressList = value;
    }

    /**
     * Gets the value of the contactList property.
     * 
     * @return
     *     possible object is
     *     {@link ContactList }
     *     
     */
    public ContactList getContactList() {
        return contactList;
    }

    /**
     * Sets the value of the contactList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactList }
     *     
     */
    public void setContactList(ContactList value) {
        this.contactList = value;
    }

    /**
     * Gets the value of the customerDocumentList property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerDocumentList }
     *     
     */
    public CustomerDocumentList getCustomerDocumentList() {
        return customerDocumentList;
    }

    /**
     * Sets the value of the customerDocumentList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerDocumentList }
     *     
     */
    public void setCustomerDocumentList(CustomerDocumentList value) {
        this.customerDocumentList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

    /**
     * Gets the value of the dneSettingList property.
     * 
     * @return
     *     possible object is
     *     {@link DNESettingList }
     *     
     */
    public DNESettingList getDNESettingList() {
        return dneSettingList;
    }

    /**
     * Sets the value of the dneSettingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DNESettingList }
     *     
     */
    public void setDNESettingList(DNESettingList value) {
        this.dneSettingList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://crm.huawei.com/azerfon/bss/basetype/}CustInfo">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CustomerInfo
        extends CustInfo
    {


    }

}
