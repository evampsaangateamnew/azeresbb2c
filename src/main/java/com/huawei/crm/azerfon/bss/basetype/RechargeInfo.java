
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RechargeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RechargeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="FeeActionType" type="{http://crm.huawei.com/azerfon/bss/basetype/}FeeActionType" minOccurs="0"/>
 *         &lt;element name="AcctRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/azerfon/bss/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="SubRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/azerfon/bss/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="RechargeFeeList" type="{http://crm.huawei.com/azerfon/bss/basetype/}RechargeFeeList"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RechargeInfo", propOrder = {

})
public class RechargeInfo {

    @XmlElement(name = "FeeActionType")
    protected String feeActionType;
    @XmlElement(name = "AcctRelaId")
    protected String acctRelaId;
    @XmlElement(name = "AccountId")
    protected Long accountId;
    @XmlElement(name = "ServiceNumber")
    protected String serviceNumber;
    @XmlElement(name = "SubRelaId")
    protected String subRelaId;
    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "RechargeFeeList", required = true)
    protected RechargeFeeList rechargeFeeList;

    /**
     * Gets the value of the feeActionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeActionType() {
        return feeActionType;
    }

    /**
     * Sets the value of the feeActionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeActionType(String value) {
        this.feeActionType = value;
    }

    /**
     * Gets the value of the acctRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctRelaId() {
        return acctRelaId;
    }

    /**
     * Sets the value of the acctRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctRelaId(String value) {
        this.acctRelaId = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountId(Long value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the subRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubRelaId() {
        return subRelaId;
    }

    /**
     * Sets the value of the subRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubRelaId(String value) {
        this.subRelaId = value;
    }

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the rechargeFeeList property.
     * 
     * @return
     *     possible object is
     *     {@link RechargeFeeList }
     *     
     */
    public RechargeFeeList getRechargeFeeList() {
        return rechargeFeeList;
    }

    /**
     * Sets the value of the rechargeFeeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RechargeFeeList }
     *     
     */
    public void setRechargeFeeList(RechargeFeeList value) {
        this.rechargeFeeList = value;
    }

}
