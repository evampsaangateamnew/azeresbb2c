
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OrderType" type="{http://crm.huawei.com/azerfon/bss/basetype/}OrderType"/>
 *         &lt;element name="ExternalOrderId" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExternalOrderId" minOccurs="0"/>
 *         &lt;element name="OrderId" type="{http://crm.huawei.com/azerfon/bss/basetype/}InternalOrderId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="ExternalCustomerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExternalCustomerId" minOccurs="0"/>
 *         &lt;element name="ExecuteTime" type="{http://crm.huawei.com/azerfon/bss/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="DealerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}DealerId" minOccurs="0"/>
 *         &lt;element name="BusinessFee" type="{http://crm.huawei.com/azerfon/bss/basetype/}BusinessFee" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderInfo", propOrder = {

})
public class OrderInfo {

    @XmlElement(name = "OrderType", required = true, nillable = true)
    protected String orderType;
    @XmlElement(name = "ExternalOrderId")
    protected String externalOrderId;
    @XmlElement(name = "OrderId")
    protected String orderId;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "ExternalCustomerId")
    protected String externalCustomerId;
    @XmlElement(name = "ExecuteTime")
    protected String executeTime;
    @XmlElement(name = "DealerId")
    protected String dealerId;
    @XmlElement(name = "BusinessFee")
    protected BusinessFee businessFee;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderType(String value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the externalOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalOrderId() {
        return externalOrderId;
    }

    /**
     * Sets the value of the externalOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalOrderId(String value) {
        this.externalOrderId = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the externalCustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalCustomerId() {
        return externalCustomerId;
    }

    /**
     * Sets the value of the externalCustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalCustomerId(String value) {
        this.externalCustomerId = value;
    }

    /**
     * Gets the value of the executeTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecuteTime() {
        return executeTime;
    }

    /**
     * Sets the value of the executeTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecuteTime(String value) {
        this.executeTime = value;
    }

    /**
     * Gets the value of the dealerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * Sets the value of the dealerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerId(String value) {
        this.dealerId = value;
    }

    /**
     * Gets the value of the businessFee property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessFee }
     *     
     */
    public BusinessFee getBusinessFee() {
        return businessFee;
    }

    /**
     * Sets the value of the businessFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessFee }
     *     
     */
    public void setBusinessFee(BusinessFee value) {
        this.businessFee = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
