
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderItemValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderItemValue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OrderItemInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}OrderItemInfo"/>
 *         &lt;element name="Customer" type="{http://crm.huawei.com/azerfon/bss/basetype/}RegisterCustInfo" minOccurs="0"/>
 *         &lt;element name="Account" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountEntity" minOccurs="0"/>
 *         &lt;element name="Subscriber" type="{http://crm.huawei.com/azerfon/bss/basetype/}SubscriberInfo" minOccurs="0"/>
 *         &lt;element name="GroupSubscriber" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupSubscriberInfo" minOccurs="0"/>
 *         &lt;element name="GroupMember" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupMemberInfo" minOccurs="0"/>
 *         &lt;element name="ShiftSubscriber" type="{http://crm.huawei.com/azerfon/bss/basetype/}ShiftSubscriberInfo" minOccurs="0"/>
 *         &lt;element name="PaymentPlanList" type="{http://crm.huawei.com/azerfon/bss/basetype/}PaymentPlanInfo" minOccurs="0"/>
 *         &lt;element name="BusinessFee" type="{http://crm.huawei.com/azerfon/bss/basetype/}BusinessFee" minOccurs="0"/>
 *         &lt;element name="ShippingInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}ShippingInfo" minOccurs="0"/>
 *         &lt;element name="OrderAddressInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}OrderAddressInfo" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderItemValue", propOrder = {

})
public class OrderItemValue {

    @XmlElement(name = "OrderItemInfo", required = true)
    protected OrderItemInfo orderItemInfo;
    @XmlElement(name = "Customer")
    protected RegisterCustInfo customer;
    @XmlElement(name = "Account")
    protected AccountEntity account;
    @XmlElement(name = "Subscriber")
    protected SubscriberInfo subscriber;
    @XmlElement(name = "GroupSubscriber")
    protected GroupSubscriberInfo groupSubscriber;
    @XmlElement(name = "GroupMember")
    protected GroupMemberInfo groupMember;
    @XmlElement(name = "ShiftSubscriber")
    protected ShiftSubscriberInfo shiftSubscriber;
    @XmlElement(name = "PaymentPlanList")
    protected PaymentPlanInfo paymentPlanList;
    @XmlElement(name = "BusinessFee")
    protected BusinessFee businessFee;
    @XmlElement(name = "ShippingInfo")
    protected ShippingInfo shippingInfo;
    @XmlElement(name = "OrderAddressInfo")
    protected OrderAddressInfo orderAddressInfo;

    /**
     * Gets the value of the orderItemInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderItemInfo }
     *     
     */
    public OrderItemInfo getOrderItemInfo() {
        return orderItemInfo;
    }

    /**
     * Sets the value of the orderItemInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderItemInfo }
     *     
     */
    public void setOrderItemInfo(OrderItemInfo value) {
        this.orderItemInfo = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link RegisterCustInfo }
     *     
     */
    public RegisterCustInfo getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegisterCustInfo }
     *     
     */
    public void setCustomer(RegisterCustInfo value) {
        this.customer = value;
    }

    /**
     * Gets the value of the account property.
     * 
     * @return
     *     possible object is
     *     {@link AccountEntity }
     *     
     */
    public AccountEntity getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountEntity }
     *     
     */
    public void setAccount(AccountEntity value) {
        this.account = value;
    }

    /**
     * Gets the value of the subscriber property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberInfo }
     *     
     */
    public SubscriberInfo getSubscriber() {
        return subscriber;
    }

    /**
     * Sets the value of the subscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberInfo }
     *     
     */
    public void setSubscriber(SubscriberInfo value) {
        this.subscriber = value;
    }

    /**
     * Gets the value of the groupSubscriber property.
     * 
     * @return
     *     possible object is
     *     {@link GroupSubscriberInfo }
     *     
     */
    public GroupSubscriberInfo getGroupSubscriber() {
        return groupSubscriber;
    }

    /**
     * Sets the value of the groupSubscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupSubscriberInfo }
     *     
     */
    public void setGroupSubscriber(GroupSubscriberInfo value) {
        this.groupSubscriber = value;
    }

    /**
     * Gets the value of the groupMember property.
     * 
     * @return
     *     possible object is
     *     {@link GroupMemberInfo }
     *     
     */
    public GroupMemberInfo getGroupMember() {
        return groupMember;
    }

    /**
     * Sets the value of the groupMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupMemberInfo }
     *     
     */
    public void setGroupMember(GroupMemberInfo value) {
        this.groupMember = value;
    }

    /**
     * Gets the value of the shiftSubscriber property.
     * 
     * @return
     *     possible object is
     *     {@link ShiftSubscriberInfo }
     *     
     */
    public ShiftSubscriberInfo getShiftSubscriber() {
        return shiftSubscriber;
    }

    /**
     * Sets the value of the shiftSubscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShiftSubscriberInfo }
     *     
     */
    public void setShiftSubscriber(ShiftSubscriberInfo value) {
        this.shiftSubscriber = value;
    }

    /**
     * Gets the value of the paymentPlanList property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentPlanInfo }
     *     
     */
    public PaymentPlanInfo getPaymentPlanList() {
        return paymentPlanList;
    }

    /**
     * Sets the value of the paymentPlanList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentPlanInfo }
     *     
     */
    public void setPaymentPlanList(PaymentPlanInfo value) {
        this.paymentPlanList = value;
    }

    /**
     * Gets the value of the businessFee property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessFee }
     *     
     */
    public BusinessFee getBusinessFee() {
        return businessFee;
    }

    /**
     * Sets the value of the businessFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessFee }
     *     
     */
    public void setBusinessFee(BusinessFee value) {
        this.businessFee = value;
    }

    /**
     * Gets the value of the shippingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingInfo }
     *     
     */
    public ShippingInfo getShippingInfo() {
        return shippingInfo;
    }

    /**
     * Sets the value of the shippingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingInfo }
     *     
     */
    public void setShippingInfo(ShippingInfo value) {
        this.shippingInfo = value;
    }

    /**
     * Gets the value of the orderAddressInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderAddressInfo }
     *     
     */
    public OrderAddressInfo getOrderAddressInfo() {
        return orderAddressInfo;
    }

    /**
     * Sets the value of the orderAddressInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderAddressInfo }
     *     
     */
    public void setOrderAddressInfo(OrderAddressInfo value) {
        this.orderAddressInfo = value;
    }

}
