
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FamilyMemberInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FamilyMemberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/azerfon/bss/basetype/}ActionType"/>
 *         &lt;element name="MemberCustId" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerId"/>
 *         &lt;element name="HouseHolder" type="{http://crm.huawei.com/azerfon/bss/basetype/}HouseHolder" minOccurs="0"/>
 *         &lt;element name="HouseHolderRight" type="{http://crm.huawei.com/azerfon/bss/basetype/}HouseHolderRight" minOccurs="0"/>
 *         &lt;element name="RightEffDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="RightExpDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="RoleType" type="{http://crm.huawei.com/azerfon/bss/basetype/}RoleType" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/azerfon/bss/basetype/}FamilyMemberStatus" minOccurs="0"/>
 *         &lt;element name="EffDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="ExpDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}DateString" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FamilyMemberInfo", propOrder = {

})
public class FamilyMemberInfo {

    @XmlElement(name = "ActionType", required = true)
    protected String actionType;
    @XmlElement(name = "MemberCustId")
    protected long memberCustId;
    @XmlElement(name = "HouseHolder")
    protected String houseHolder;
    @XmlElement(name = "HouseHolderRight")
    protected String houseHolderRight;
    @XmlElement(name = "RightEffDate")
    protected String rightEffDate;
    @XmlElement(name = "RightExpDate")
    protected String rightExpDate;
    @XmlElement(name = "RoleType")
    protected String roleType;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "EffDate")
    protected String effDate;
    @XmlElement(name = "ExpDate")
    protected String expDate;

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the memberCustId property.
     * 
     */
    public long getMemberCustId() {
        return memberCustId;
    }

    /**
     * Sets the value of the memberCustId property.
     * 
     */
    public void setMemberCustId(long value) {
        this.memberCustId = value;
    }

    /**
     * Gets the value of the houseHolder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseHolder() {
        return houseHolder;
    }

    /**
     * Sets the value of the houseHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseHolder(String value) {
        this.houseHolder = value;
    }

    /**
     * Gets the value of the houseHolderRight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseHolderRight() {
        return houseHolderRight;
    }

    /**
     * Sets the value of the houseHolderRight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseHolderRight(String value) {
        this.houseHolderRight = value;
    }

    /**
     * Gets the value of the rightEffDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRightEffDate() {
        return rightEffDate;
    }

    /**
     * Sets the value of the rightEffDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRightEffDate(String value) {
        this.rightEffDate = value;
    }

    /**
     * Gets the value of the rightExpDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRightExpDate() {
        return rightExpDate;
    }

    /**
     * Sets the value of the rightExpDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRightExpDate(String value) {
        this.rightExpDate = value;
    }

    /**
     * Gets the value of the roleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleType() {
        return roleType;
    }

    /**
     * Sets the value of the roleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleType(String value) {
        this.roleType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the effDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffDate() {
        return effDate;
    }

    /**
     * Sets the value of the effDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffDate(String value) {
        this.effDate = value;
    }

    /**
     * Gets the value of the expDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpDate() {
        return expDate;
    }

    /**
     * Sets the value of the expDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpDate(String value) {
        this.expDate = value;
    }

}
