
package com.huawei.crm.azerfon.bss.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderItemStatusList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderItemStatusList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderItemStatusInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}OrderItemStatusInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderItemStatusList", propOrder = {
    "orderItemStatusInfo"
})
public class OrderItemStatusList {

    @XmlElement(name = "OrderItemStatusInfo", required = true)
    protected List<OrderItemStatusInfo> orderItemStatusInfo;

    /**
     * Gets the value of the orderItemStatusInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderItemStatusInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderItemStatusInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderItemStatusInfo }
     * 
     * 
     */
    public List<OrderItemStatusInfo> getOrderItemStatusInfo() {
        if (orderItemStatusInfo == null) {
            orderItemStatusInfo = new ArrayList<OrderItemStatusInfo>();
        }
        return this.orderItemStatusInfo;
    }

}
