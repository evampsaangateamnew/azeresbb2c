
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PointRedeemSchema complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PointRedeemSchema">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SchemaID" type="{http://crm.huawei.com/azerfon/bss/basetype/}PointRedeemSchemaID"/>
 *         &lt;element name="SchemaName" type="{http://crm.huawei.com/azerfon/bss/basetype/}PointRedeemSchemaName"/>
 *         &lt;element name="SchemaCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}PointRedeemSchemaCode"/>
 *         &lt;element name="SchemaStatus" type="{http://crm.huawei.com/azerfon/bss/basetype/}PointRedeemSchemaStatus"/>
 *         &lt;element name="SchemaScenario" type="{http://crm.huawei.com/azerfon/bss/basetype/}PointRedeemSchemaScenario"/>
 *         &lt;element name="RedeemPrice" type="{http://crm.huawei.com/azerfon/bss/basetype/}PointRedeemPrice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PointRedeemSchema", propOrder = {
    "schemaID",
    "schemaName",
    "schemaCode",
    "schemaStatus",
    "schemaScenario",
    "redeemPrice"
})
public class PointRedeemSchema {

    @XmlElement(name = "SchemaID", required = true)
    protected String schemaID;
    @XmlElement(name = "SchemaName", required = true)
    protected String schemaName;
    @XmlElement(name = "SchemaCode", required = true)
    protected String schemaCode;
    @XmlElement(name = "SchemaStatus", required = true)
    protected String schemaStatus;
    @XmlElement(name = "SchemaScenario", required = true)
    protected String schemaScenario;
    @XmlElement(name = "RedeemPrice")
    protected PointRedeemPrice redeemPrice;

    /**
     * Gets the value of the schemaID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaID() {
        return schemaID;
    }

    /**
     * Sets the value of the schemaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaID(String value) {
        this.schemaID = value;
    }

    /**
     * Gets the value of the schemaName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * Sets the value of the schemaName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaName(String value) {
        this.schemaName = value;
    }

    /**
     * Gets the value of the schemaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaCode() {
        return schemaCode;
    }

    /**
     * Sets the value of the schemaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaCode(String value) {
        this.schemaCode = value;
    }

    /**
     * Gets the value of the schemaStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaStatus() {
        return schemaStatus;
    }

    /**
     * Sets the value of the schemaStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaStatus(String value) {
        this.schemaStatus = value;
    }

    /**
     * Gets the value of the schemaScenario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaScenario() {
        return schemaScenario;
    }

    /**
     * Sets the value of the schemaScenario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaScenario(String value) {
        this.schemaScenario = value;
    }

    /**
     * Gets the value of the redeemPrice property.
     * 
     * @return
     *     possible object is
     *     {@link PointRedeemPrice }
     *     
     */
    public PointRedeemPrice getRedeemPrice() {
        return redeemPrice;
    }

    /**
     * Sets the value of the redeemPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointRedeemPrice }
     *     
     */
    public void setRedeemPrice(PointRedeemPrice value) {
        this.redeemPrice = value;
    }

}
