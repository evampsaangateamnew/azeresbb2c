
package com.huawei.crm.azerfon.bss.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryMsisdnStatusItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryMsisdnStatusItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderNO">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/azerfon/bss/basetype/}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryMsisdnStatusItem", propOrder = {
    "orderNO"
})
public class QueryMsisdnStatusItem {

    @XmlElement(name = "OrderNO", required = true)
    protected String orderNO;

    /**
     * Gets the value of the orderNO property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNO() {
        return orderNO;
    }

    /**
     * Sets the value of the orderNO property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNO(String value) {
        this.orderNO = value;
    }

}
