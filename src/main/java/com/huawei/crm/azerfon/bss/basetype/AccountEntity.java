
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountEntity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AcctRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="AccountCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountCode" minOccurs="0"/>
 *         &lt;element name="PaymentType" type="{http://crm.huawei.com/azerfon/bss/basetype/}PaymentType" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountStatus" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://crm.huawei.com/azerfon/bss/basetype/}Title" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://crm.huawei.com/azerfon/bss/basetype/}FirstName" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://crm.huawei.com/azerfon/bss/basetype/}MiddleName" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://crm.huawei.com/azerfon/bss/basetype/}LastName" minOccurs="0"/>
 *         &lt;element name="AccountType" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountType" minOccurs="0"/>
 *         &lt;element name="BillLanguage" type="{http://crm.huawei.com/azerfon/bss/basetype/}Language" minOccurs="0"/>
 *         &lt;element name="BillCycleEffMode" type="{http://crm.huawei.com/azerfon/bss/basetype/}BillCycleEffMode" minOccurs="0"/>
 *         &lt;element name="BillCycleEffDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}BillCycleEffDate" minOccurs="0"/>
 *         &lt;element name="BillcycleType" type="{http://crm.huawei.com/azerfon/bss/basetype/}BillcycleType" minOccurs="0"/>
 *         &lt;element name="BillCycleId" type="{http://crm.huawei.com/azerfon/bss/basetype/}BillCycleId" minOccurs="0"/>
 *         &lt;element name="CreditLimit" type="{http://crm.huawei.com/azerfon/bss/basetype/}CreditLimit" minOccurs="0"/>
 *         &lt;element name="InitialBalance" type="{http://crm.huawei.com/azerfon/bss/basetype/}InitialBalance" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}EffectiveDate" minOccurs="0"/>
 *         &lt;element name="PaymentModeList" type="{http://crm.huawei.com/azerfon/bss/basetype/}PaymentList" minOccurs="0"/>
 *         &lt;element name="AddressList" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressList" minOccurs="0"/>
 *         &lt;element name="BillMediumList" type="{http://crm.huawei.com/azerfon/bss/basetype/}BillMediumList" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountEntity", propOrder = {

})
public class AccountEntity {

    @XmlElement(name = "AcctRelaId")
    protected String acctRelaId;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "CustomerRelaId")
    protected String customerRelaId;
    @XmlElement(name = "AccountId")
    protected Long accountId;
    @XmlElement(name = "AccountCode")
    protected String accountCode;
    @XmlElement(name = "PaymentType")
    protected String paymentType;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "AccountType")
    protected String accountType;
    @XmlElement(name = "BillLanguage")
    protected String billLanguage;
    @XmlElement(name = "BillCycleEffMode")
    protected String billCycleEffMode;
    @XmlElement(name = "BillCycleEffDate")
    protected String billCycleEffDate;
    @XmlElement(name = "BillcycleType")
    protected String billcycleType;
    @XmlElement(name = "BillCycleId")
    protected String billCycleId;
    @XmlElement(name = "CreditLimit")
    protected Long creditLimit;
    @XmlElement(name = "InitialBalance")
    protected Long initialBalance;
    @XmlElement(name = "EffectiveDate")
    protected String effectiveDate;
    @XmlElement(name = "PaymentModeList")
    protected PaymentList paymentModeList;
    @XmlElement(name = "AddressList")
    protected AddressList addressList;
    @XmlElement(name = "BillMediumList")
    protected BillMediumList billMediumList;
    @XmlElement(name = "SupplementaryOfferingList")
    protected OfferingList supplementaryOfferingList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the acctRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctRelaId() {
        return acctRelaId;
    }

    /**
     * Sets the value of the acctRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctRelaId(String value) {
        this.acctRelaId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the customerRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRelaId() {
        return customerRelaId;
    }

    /**
     * Sets the value of the customerRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRelaId(String value) {
        this.customerRelaId = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountId(Long value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the accountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Sets the value of the accountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the billLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillLanguage() {
        return billLanguage;
    }

    /**
     * Sets the value of the billLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillLanguage(String value) {
        this.billLanguage = value;
    }

    /**
     * Gets the value of the billCycleEffMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleEffMode() {
        return billCycleEffMode;
    }

    /**
     * Sets the value of the billCycleEffMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleEffMode(String value) {
        this.billCycleEffMode = value;
    }

    /**
     * Gets the value of the billCycleEffDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleEffDate() {
        return billCycleEffDate;
    }

    /**
     * Sets the value of the billCycleEffDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleEffDate(String value) {
        this.billCycleEffDate = value;
    }

    /**
     * Gets the value of the billcycleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillcycleType() {
        return billcycleType;
    }

    /**
     * Sets the value of the billcycleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillcycleType(String value) {
        this.billcycleType = value;
    }

    /**
     * Gets the value of the billCycleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleId() {
        return billCycleId;
    }

    /**
     * Sets the value of the billCycleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleId(String value) {
        this.billCycleId = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCreditLimit() {
        return creditLimit;
    }

    /**
     * Sets the value of the creditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCreditLimit(Long value) {
        this.creditLimit = value;
    }

    /**
     * Gets the value of the initialBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInitialBalance() {
        return initialBalance;
    }

    /**
     * Sets the value of the initialBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInitialBalance(Long value) {
        this.initialBalance = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the paymentModeList property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentList }
     *     
     */
    public PaymentList getPaymentModeList() {
        return paymentModeList;
    }

    /**
     * Sets the value of the paymentModeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentList }
     *     
     */
    public void setPaymentModeList(PaymentList value) {
        this.paymentModeList = value;
    }

    /**
     * Gets the value of the addressList property.
     * 
     * @return
     *     possible object is
     *     {@link AddressList }
     *     
     */
    public AddressList getAddressList() {
        return addressList;
    }

    /**
     * Sets the value of the addressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressList }
     *     
     */
    public void setAddressList(AddressList value) {
        this.addressList = value;
    }

    /**
     * Gets the value of the billMediumList property.
     * 
     * @return
     *     possible object is
     *     {@link BillMediumList }
     *     
     */
    public BillMediumList getBillMediumList() {
        return billMediumList;
    }

    /**
     * Sets the value of the billMediumList property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillMediumList }
     *     
     */
    public void setBillMediumList(BillMediumList value) {
        this.billMediumList = value;
    }

    /**
     * Gets the value of the supplementaryOfferingList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingList }
     *     
     */
    public OfferingList getSupplementaryOfferingList() {
        return supplementaryOfferingList;
    }

    /**
     * Sets the value of the supplementaryOfferingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingList }
     *     
     */
    public void setSupplementaryOfferingList(OfferingList value) {
        this.supplementaryOfferingList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
