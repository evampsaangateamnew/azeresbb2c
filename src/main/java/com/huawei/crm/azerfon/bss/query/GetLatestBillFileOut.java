
package com.huawei.crm.azerfon.bss.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.azerfon.bss.basetype.ExtParameterList;


/**
 * <p>Java class for GetLatestBillFileOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetLatestBillFileOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountId"/>
 *         &lt;element name="BillCycleId" type="{http://crm.huawei.com/azerfon/bss/basetype/}BillCycleId"/>
 *         &lt;element name="BillFilePath" type="{http://crm.huawei.com/azerfon/bss/basetype/}OsmsFilePath"/>
 *         &lt;element name="BillFileName" type="{http://crm.huawei.com/azerfon/bss/basetype/}OsmsFileName"/>
 *         &lt;element name="InvoiceId" type="{http://crm.huawei.com/azerfon/bss/basetype/}OsmsInvoiceId" minOccurs="0"/>
 *         &lt;element name="InvoiceType" type="{http://crm.huawei.com/azerfon/bss/basetype/}OsmsInvoiceType" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetLatestBillFileOut", propOrder = {

})
public class GetLatestBillFileOut {

    @XmlElement(name = "AccountId")
    protected long accountId;
    @XmlElement(name = "BillCycleId", required = true)
    protected String billCycleId;
    @XmlElement(name = "BillFilePath", required = true)
    protected String billFilePath;
    @XmlElement(name = "BillFileName", required = true)
    protected String billFileName;
    @XmlElement(name = "InvoiceId")
    protected String invoiceId;
    @XmlElement(name = "InvoiceType")
    protected String invoiceType;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the accountId property.
     * 
     */
    public long getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     */
    public void setAccountId(long value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the billCycleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleId() {
        return billCycleId;
    }

    /**
     * Sets the value of the billCycleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleId(String value) {
        this.billCycleId = value;
    }

    /**
     * Gets the value of the billFilePath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillFilePath() {
        return billFilePath;
    }

    /**
     * Sets the value of the billFilePath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillFilePath(String value) {
        this.billFilePath = value;
    }

    /**
     * Gets the value of the billFileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillFileName() {
        return billFileName;
    }

    /**
     * Sets the value of the billFileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillFileName(String value) {
        this.billFileName = value;
    }

    /**
     * Gets the value of the invoiceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceId() {
        return invoiceId;
    }

    /**
     * Sets the value of the invoiceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceId(String value) {
        this.invoiceId = value;
    }

    /**
     * Gets the value of the invoiceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceType() {
        return invoiceType;
    }

    /**
     * Sets the value of the invoiceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceType(String value) {
        this.invoiceType = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
