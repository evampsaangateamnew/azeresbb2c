
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupSubscriberInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GroupSubscriberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="GroupId" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupId" minOccurs="0"/>
 *         &lt;element name="GroupRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="CustomerRelaId" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="AcctRelaId" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="GroupName" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupName" minOccurs="0"/>
 *         &lt;element name="GroupNo" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupNo" minOccurs="0"/>
 *         &lt;element name="OperateType" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupStatus" minOccurs="0"/>
 *         &lt;element name="GroupStatus" type="{http://crm.huawei.com/azerfon/bss/basetype/}GroupStatus" minOccurs="0"/>
 *         &lt;element name="MaxGroupMember" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LimitType" type="{http://crm.huawei.com/azerfon/bss/basetype/}string" minOccurs="0"/>
 *         &lt;element name="LimitValue" type="{http://crm.huawei.com/azerfon/bss/basetype/}string" minOccurs="0"/>
 *         &lt;element name="PrimaryOfferingInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingInfo" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingList" minOccurs="0"/>
 *         &lt;element name="PromotionList" type="{http://crm.huawei.com/azerfon/bss/basetype/}PromotionList" minOccurs="0"/>
 *         &lt;element name="ContactList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ContactList" minOccurs="0"/>
 *         &lt;element name="AddressList" type="{http://crm.huawei.com/azerfon/bss/basetype/}AddressList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupSubscriberInfo", propOrder = {

})
public class GroupSubscriberInfo {

    @XmlElement(name = "GroupId")
    protected Long groupId;
    @XmlElement(name = "GroupRelaId")
    protected String groupRelaId;
    @XmlElement(name = "CustomerId")
    protected Object customerId;
    @XmlElement(name = "CustomerRelaId")
    protected Object customerRelaId;
    @XmlElement(name = "AccountId")
    protected Object accountId;
    @XmlElement(name = "AcctRelaId")
    protected Object acctRelaId;
    @XmlElement(name = "GroupName")
    protected String groupName;
    @XmlElement(name = "GroupNo")
    protected String groupNo;
    @XmlElement(name = "OperateType")
    protected String operateType;
    @XmlElement(name = "GroupStatus")
    protected String groupStatus;
    @XmlElement(name = "MaxGroupMember")
    protected Integer maxGroupMember;
    @XmlElement(name = "LimitType")
    protected String limitType;
    @XmlElement(name = "LimitValue")
    protected String limitValue;
    @XmlElement(name = "PrimaryOfferingInfo")
    protected OfferingInfo primaryOfferingInfo;
    @XmlElement(name = "SupplementaryOfferingList")
    protected OfferingList supplementaryOfferingList;
    @XmlElement(name = "PromotionList")
    protected PromotionList promotionList;
    @XmlElement(name = "ContactList")
    protected ContactList contactList;
    @XmlElement(name = "AddressList")
    protected AddressList addressList;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the groupId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGroupId(Long value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the groupRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupRelaId() {
        return groupRelaId;
    }

    /**
     * Sets the value of the groupRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupRelaId(String value) {
        this.groupRelaId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCustomerId(Object value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the customerRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCustomerRelaId() {
        return customerRelaId;
    }

    /**
     * Sets the value of the customerRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCustomerRelaId(Object value) {
        this.customerRelaId = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAccountId(Object value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the acctRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAcctRelaId() {
        return acctRelaId;
    }

    /**
     * Sets the value of the acctRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAcctRelaId(Object value) {
        this.acctRelaId = value;
    }

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the groupNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupNo() {
        return groupNo;
    }

    /**
     * Sets the value of the groupNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupNo(String value) {
        this.groupNo = value;
    }

    /**
     * Gets the value of the operateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperateType() {
        return operateType;
    }

    /**
     * Sets the value of the operateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperateType(String value) {
        this.operateType = value;
    }

    /**
     * Gets the value of the groupStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupStatus() {
        return groupStatus;
    }

    /**
     * Sets the value of the groupStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupStatus(String value) {
        this.groupStatus = value;
    }

    /**
     * Gets the value of the maxGroupMember property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxGroupMember() {
        return maxGroupMember;
    }

    /**
     * Sets the value of the maxGroupMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxGroupMember(Integer value) {
        this.maxGroupMember = value;
    }

    /**
     * Gets the value of the limitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitType() {
        return limitType;
    }

    /**
     * Sets the value of the limitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitType(String value) {
        this.limitType = value;
    }

    /**
     * Gets the value of the limitValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitValue() {
        return limitValue;
    }

    /**
     * Sets the value of the limitValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitValue(String value) {
        this.limitValue = value;
    }

    /**
     * Gets the value of the primaryOfferingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingInfo }
     *     
     */
    public OfferingInfo getPrimaryOfferingInfo() {
        return primaryOfferingInfo;
    }

    /**
     * Sets the value of the primaryOfferingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingInfo }
     *     
     */
    public void setPrimaryOfferingInfo(OfferingInfo value) {
        this.primaryOfferingInfo = value;
    }

    /**
     * Gets the value of the supplementaryOfferingList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingList }
     *     
     */
    public OfferingList getSupplementaryOfferingList() {
        return supplementaryOfferingList;
    }

    /**
     * Sets the value of the supplementaryOfferingList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingList }
     *     
     */
    public void setSupplementaryOfferingList(OfferingList value) {
        this.supplementaryOfferingList = value;
    }

    /**
     * Gets the value of the promotionList property.
     * 
     * @return
     *     possible object is
     *     {@link PromotionList }
     *     
     */
    public PromotionList getPromotionList() {
        return promotionList;
    }

    /**
     * Sets the value of the promotionList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PromotionList }
     *     
     */
    public void setPromotionList(PromotionList value) {
        this.promotionList = value;
    }

    /**
     * Gets the value of the contactList property.
     * 
     * @return
     *     possible object is
     *     {@link ContactList }
     *     
     */
    public ContactList getContactList() {
        return contactList;
    }

    /**
     * Sets the value of the contactList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactList }
     *     
     */
    public void setContactList(ContactList value) {
        this.contactList = value;
    }

    /**
     * Gets the value of the addressList property.
     * 
     * @return
     *     possible object is
     *     {@link AddressList }
     *     
     */
    public AddressList getAddressList() {
        return addressList;
    }

    /**
     * Sets the value of the addressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressList }
     *     
     */
    public void setAddressList(AddressList value) {
        this.addressList = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
