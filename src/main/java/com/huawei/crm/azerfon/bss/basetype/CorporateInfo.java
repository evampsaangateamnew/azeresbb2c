
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorporateInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CompanyName" type="{http://crm.huawei.com/azerfon/bss/basetype/}CompanyName" minOccurs="0"/>
 *         &lt;element name="ShortName" type="{http://crm.huawei.com/azerfon/bss/basetype/}CorpShortName" minOccurs="0"/>
 *         &lt;element name="CorporateNo" type="{http://crm.huawei.com/azerfon/bss/basetype/}CorporateNo" minOccurs="0"/>
 *         &lt;element name="RegistrationID" type="{http://crm.huawei.com/azerfon/bss/basetype/}RegistrationID" minOccurs="0"/>
 *         &lt;element name="Industry" type="{http://crm.huawei.com/azerfon/bss/basetype/}Industry" minOccurs="0"/>
 *         &lt;element name="SubIndustry" type="{http://crm.huawei.com/azerfon/bss/basetype/}SubIndustry" minOccurs="0"/>
 *         &lt;element name="Scale" type="{http://crm.huawei.com/azerfon/bss/basetype/}Scale" minOccurs="0"/>
 *         &lt;element name="PhoneNo" type="{http://crm.huawei.com/azerfon/bss/basetype/}PhoneNo" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaEmail" minOccurs="0"/>
 *         &lt;element name="FaxNo" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelaFax" minOccurs="0"/>
 *         &lt;element name="HomePage" type="{http://crm.huawei.com/azerfon/bss/basetype/}HomePage" minOccurs="0"/>
 *         &lt;element name="ParentCustomerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerId" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporateInfo", propOrder = {

})
public class CorporateInfo {

    @XmlElement(name = "CompanyName")
    protected String companyName;
    @XmlElement(name = "ShortName")
    protected String shortName;
    @XmlElement(name = "CorporateNo")
    protected String corporateNo;
    @XmlElement(name = "RegistrationID")
    protected String registrationID;
    @XmlElement(name = "Industry")
    protected String industry;
    @XmlElement(name = "SubIndustry")
    protected String subIndustry;
    @XmlElement(name = "Scale")
    protected String scale;
    @XmlElement(name = "PhoneNo")
    protected String phoneNo;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "FaxNo")
    protected String faxNo;
    @XmlElement(name = "HomePage")
    protected String homePage;
    @XmlElement(name = "ParentCustomerId")
    protected Long parentCustomerId;

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the shortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Sets the value of the shortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortName(String value) {
        this.shortName = value;
    }

    /**
     * Gets the value of the corporateNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateNo() {
        return corporateNo;
    }

    /**
     * Sets the value of the corporateNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateNo(String value) {
        this.corporateNo = value;
    }

    /**
     * Gets the value of the registrationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationID() {
        return registrationID;
    }

    /**
     * Sets the value of the registrationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationID(String value) {
        this.registrationID = value;
    }

    /**
     * Gets the value of the industry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * Sets the value of the industry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustry(String value) {
        this.industry = value;
    }

    /**
     * Gets the value of the subIndustry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubIndustry() {
        return subIndustry;
    }

    /**
     * Sets the value of the subIndustry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubIndustry(String value) {
        this.subIndustry = value;
    }

    /**
     * Gets the value of the scale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScale() {
        return scale;
    }

    /**
     * Sets the value of the scale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScale(String value) {
        this.scale = value;
    }

    /**
     * Gets the value of the phoneNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * Sets the value of the phoneNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNo(String value) {
        this.phoneNo = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the faxNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNo() {
        return faxNo;
    }

    /**
     * Sets the value of the faxNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNo(String value) {
        this.faxNo = value;
    }

    /**
     * Gets the value of the homePage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomePage() {
        return homePage;
    }

    /**
     * Sets the value of the homePage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomePage(String value) {
        this.homePage = value;
    }

    /**
     * Gets the value of the parentCustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getParentCustomerId() {
        return parentCustomerId;
    }

    /**
     * Sets the value of the parentCustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setParentCustomerId(Long value) {
        this.parentCustomerId = value;
    }

}
