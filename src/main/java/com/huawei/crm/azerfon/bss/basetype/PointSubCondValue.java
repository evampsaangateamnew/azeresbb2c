
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PointSubCondValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PointSubCondValue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="SubStatus" type="{http://crm.huawei.com/azerfon/bss/basetype/}SubscriberStatus" minOccurs="0"/>
 *         &lt;element name="TeleType" type="{http://crm.huawei.com/azerfon/bss/basetype/}TeleType" minOccurs="0"/>
 *         &lt;element name="PrepaidFlag" type="{http://crm.huawei.com/azerfon/bss/basetype/}SubscriberPrepaidFlag" minOccurs="0"/>
 *         &lt;element name="POfferID" type="{http://crm.huawei.com/azerfon/bss/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PointSubCondValue", propOrder = {

})
public class PointSubCondValue {

    @XmlElement(name = "SubStatus")
    protected String subStatus;
    @XmlElement(name = "TeleType")
    protected String teleType;
    @XmlElement(name = "PrepaidFlag")
    protected String prepaidFlag;
    @XmlElement(name = "POfferID")
    protected String pOfferID;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the subStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubStatus() {
        return subStatus;
    }

    /**
     * Sets the value of the subStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubStatus(String value) {
        this.subStatus = value;
    }

    /**
     * Gets the value of the teleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTeleType() {
        return teleType;
    }

    /**
     * Sets the value of the teleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTeleType(String value) {
        this.teleType = value;
    }

    /**
     * Gets the value of the prepaidFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrepaidFlag() {
        return prepaidFlag;
    }

    /**
     * Sets the value of the prepaidFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrepaidFlag(String value) {
        this.prepaidFlag = value;
    }

    /**
     * Gets the value of the pOfferID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOfferID() {
        return pOfferID;
    }

    /**
     * Sets the value of the pOfferID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOfferID(String value) {
        this.pOfferID = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
