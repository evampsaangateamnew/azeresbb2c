
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustBaseInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustBaseInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerCode" minOccurs="0"/>
 *         &lt;element name="CustomerType" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerType" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="ParentCustId" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="CustomerLevel" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustLevel" minOccurs="0"/>
 *         &lt;element name="CustomerSegment" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerSegment" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://crm.huawei.com/azerfon/bss/basetype/}Language" minOccurs="0"/>
 *         &lt;element name="WrittenLanguage" type="{http://crm.huawei.com/azerfon/bss/basetype/}Language" minOccurs="0"/>
 *         &lt;element name="CustClass" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerClass" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustStatus" minOccurs="0"/>
 *         &lt;element name="PasswordInfo" type="{http://crm.huawei.com/azerfon/bss/basetype/}PasswordInfo" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustBaseInfo", propOrder = {

})
public class CustBaseInfo {

    @XmlElement(name = "CustomerCode")
    protected String customerCode;
    @XmlElement(name = "CustomerType")
    protected String customerType;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "ParentCustId")
    protected Long parentCustId;
    @XmlElement(name = "CustomerRelaId")
    protected String customerRelaId;
    @XmlElement(name = "CustomerLevel")
    protected String customerLevel;
    @XmlElement(name = "CustomerSegment")
    protected String customerSegment;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "WrittenLanguage")
    protected String writtenLanguage;
    @XmlElement(name = "CustClass")
    protected String custClass;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "PasswordInfo")
    protected PasswordInfo passwordInfo;

    /**
     * Gets the value of the customerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Sets the value of the customerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCode(String value) {
        this.customerCode = value;
    }

    /**
     * Gets the value of the customerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerType() {
        return customerType;
    }

    /**
     * Sets the value of the customerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerType(String value) {
        this.customerType = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the parentCustId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getParentCustId() {
        return parentCustId;
    }

    /**
     * Sets the value of the parentCustId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setParentCustId(Long value) {
        this.parentCustId = value;
    }

    /**
     * Gets the value of the customerRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRelaId() {
        return customerRelaId;
    }

    /**
     * Sets the value of the customerRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRelaId(String value) {
        this.customerRelaId = value;
    }

    /**
     * Gets the value of the customerLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerLevel() {
        return customerLevel;
    }

    /**
     * Sets the value of the customerLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerLevel(String value) {
        this.customerLevel = value;
    }

    /**
     * Gets the value of the customerSegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSegment() {
        return customerSegment;
    }

    /**
     * Sets the value of the customerSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSegment(String value) {
        this.customerSegment = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the writtenLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWrittenLanguage() {
        return writtenLanguage;
    }

    /**
     * Sets the value of the writtenLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWrittenLanguage(String value) {
        this.writtenLanguage = value;
    }

    /**
     * Gets the value of the custClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustClass() {
        return custClass;
    }

    /**
     * Sets the value of the custClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustClass(String value) {
        this.custClass = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the passwordInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PasswordInfo }
     *     
     */
    public PasswordInfo getPasswordInfo() {
        return passwordInfo;
    }

    /**
     * Sets the value of the passwordInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordInfo }
     *     
     */
    public void setPasswordInfo(PasswordInfo value) {
        this.passwordInfo = value;
    }

}
