
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DealerInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DealerInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DealerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}DealerIdentifier"/>
 *         &lt;element name="DealerName" type="{http://crm.huawei.com/azerfon/bss/basetype/}DealerName" minOccurs="0"/>
 *         &lt;element name="ChannelType" type="{http://crm.huawei.com/azerfon/bss/basetype/}ChannelType" minOccurs="0"/>
 *         &lt;element name="DealerGrade" type="{http://crm.huawei.com/azerfon/bss/basetype/}DealerGrade" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/azerfon/bss/basetype/}DealerStatus" minOccurs="0"/>
 *         &lt;element name="FullName" type="{http://crm.huawei.com/azerfon/bss/basetype/}FullName" minOccurs="0"/>
 *         &lt;element name="DealerCode" type="{http://crm.huawei.com/azerfon/bss/basetype/}DealerCode" minOccurs="0"/>
 *         &lt;element name="IsEvc" type="{http://crm.huawei.com/azerfon/bss/basetype/}IsEvc" minOccurs="0"/>
 *         &lt;element name="SuperDealerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}SuperDealerId" minOccurs="0"/>
 *         &lt;element name="BeId" type="{http://crm.huawei.com/azerfon/bss/basetype/}BeID" minOccurs="0"/>
 *         &lt;element name="CreateDate" type="{http://crm.huawei.com/azerfon/bss/basetype/}Date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerInfo", propOrder = {
    "dealerId",
    "dealerName",
    "channelType",
    "dealerGrade",
    "status",
    "fullName",
    "dealerCode",
    "isEvc",
    "superDealerId",
    "beId",
    "createDate"
})
public class DealerInfo {

    @XmlElement(name = "DealerId", required = true)
    protected String dealerId;
    @XmlElement(name = "DealerName")
    protected String dealerName;
    @XmlElement(name = "ChannelType")
    protected String channelType;
    @XmlElement(name = "DealerGrade")
    protected String dealerGrade;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "FullName")
    protected String fullName;
    @XmlElement(name = "DealerCode")
    protected String dealerCode;
    @XmlElement(name = "IsEvc")
    protected String isEvc;
    @XmlElement(name = "SuperDealerId")
    protected String superDealerId;
    @XmlElement(name = "BeId")
    protected String beId;
    @XmlElement(name = "CreateDate")
    protected String createDate;

    /**
     * Gets the value of the dealerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * Sets the value of the dealerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerId(String value) {
        this.dealerId = value;
    }

    /**
     * Gets the value of the dealerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerName() {
        return dealerName;
    }

    /**
     * Sets the value of the dealerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerName(String value) {
        this.dealerName = value;
    }

    /**
     * Gets the value of the channelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelType() {
        return channelType;
    }

    /**
     * Sets the value of the channelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelType(String value) {
        this.channelType = value;
    }

    /**
     * Gets the value of the dealerGrade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerGrade() {
        return dealerGrade;
    }

    /**
     * Sets the value of the dealerGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerGrade(String value) {
        this.dealerGrade = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullName(String value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the dealerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * Sets the value of the dealerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerCode(String value) {
        this.dealerCode = value;
    }

    /**
     * Gets the value of the isEvc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsEvc() {
        return isEvc;
    }

    /**
     * Sets the value of the isEvc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsEvc(String value) {
        this.isEvc = value;
    }

    /**
     * Gets the value of the superDealerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuperDealerId() {
        return superDealerId;
    }

    /**
     * Sets the value of the superDealerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuperDealerId(String value) {
        this.superDealerId = value;
    }

    /**
     * Gets the value of the beId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeId() {
        return beId;
    }

    /**
     * Sets the value of the beId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeId(String value) {
        this.beId = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateDate(String value) {
        this.createDate = value;
    }

}
