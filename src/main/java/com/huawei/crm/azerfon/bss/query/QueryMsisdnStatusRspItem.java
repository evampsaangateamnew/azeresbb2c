
package com.huawei.crm.azerfon.bss.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryMsisdnStatusRspItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryMsisdnStatusRspItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderNO">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/azerfon/bss/basetype/}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PortStatus">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/azerfon/bss/basetype/}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ReturnStatus">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/azerfon/bss/basetype/}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ReturnCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/azerfon/bss/basetype/}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ReturnMessage">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/azerfon/bss/basetype/}string">
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryMsisdnStatusRspItem", propOrder = {
    "orderNO",
    "portStatus",
    "returnStatus",
    "returnCode",
    "returnMessage"
})
public class QueryMsisdnStatusRspItem {

    @XmlElement(name = "OrderNO", required = true)
    protected String orderNO;
    @XmlElement(name = "PortStatus", required = true)
    protected String portStatus;
    @XmlElement(name = "ReturnStatus", required = true)
    protected String returnStatus;
    @XmlElement(name = "ReturnCode", required = true)
    protected String returnCode;
    @XmlElement(name = "ReturnMessage", required = true)
    protected String returnMessage;

    /**
     * Gets the value of the orderNO property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNO() {
        return orderNO;
    }

    /**
     * Sets the value of the orderNO property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNO(String value) {
        this.orderNO = value;
    }

    /**
     * Gets the value of the portStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortStatus() {
        return portStatus;
    }

    /**
     * Sets the value of the portStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortStatus(String value) {
        this.portStatus = value;
    }

    /**
     * Gets the value of the returnStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnStatus() {
        return returnStatus;
    }

    /**
     * Sets the value of the returnStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnStatus(String value) {
        this.returnStatus = value;
    }

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnCode(String value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the returnMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnMessage() {
        return returnMessage;
    }

    /**
     * Sets the value of the returnMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnMessage(String value) {
        this.returnMessage = value;
    }

}
