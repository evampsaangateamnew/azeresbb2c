
package com.huawei.crm.azerfon.bss.query;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.crm.azerfon.bss.query package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.crm.azerfon.bss.query
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AzerfonQueryMsisdnStatusRequest }
     * 
     */
    public AzerfonQueryMsisdnStatusRequest createAzerfonQueryMsisdnStatusRequest() {
        return new AzerfonQueryMsisdnStatusRequest();
    }

    /**
     * Create an instance of {@link QueryMsisdnStatusRequestIn }
     * 
     */
    public QueryMsisdnStatusRequestIn createQueryMsisdnStatusRequestIn() {
        return new QueryMsisdnStatusRequestIn();
    }

    /**
     * Create an instance of {@link GetSubscriberRequest }
     * 
     */
    public GetSubscriberRequest createGetSubscriberRequest() {
        return new GetSubscriberRequest();
    }

    /**
     * Create an instance of {@link GetSubscriberIn }
     * 
     */
    public GetSubscriberIn createGetSubscriberIn() {
        return new GetSubscriberIn();
    }

    /**
     * Create an instance of {@link GetSubscriberResponse }
     * 
     */
    public GetSubscriberResponse createGetSubscriberResponse() {
        return new GetSubscriberResponse();
    }

    /**
     * Create an instance of {@link GetSubscriberOut }
     * 
     */
    public GetSubscriberOut createGetSubscriberOut() {
        return new GetSubscriberOut();
    }

    /**
     * Create an instance of {@link GetLatestBillFileResponse }
     * 
     */
    public GetLatestBillFileResponse createGetLatestBillFileResponse() {
        return new GetLatestBillFileResponse();
    }

    /**
     * Create an instance of {@link GetLatestBillFileOut }
     * 
     */
    public GetLatestBillFileOut createGetLatestBillFileOut() {
        return new GetLatestBillFileOut();
    }

    /**
     * Create an instance of {@link AzerfonQueryMsisdnStatusResponse }
     * 
     */
    public AzerfonQueryMsisdnStatusResponse createAzerfonQueryMsisdnStatusResponse() {
        return new AzerfonQueryMsisdnStatusResponse();
    }

    /**
     * Create an instance of {@link QueryMsisdnStatusResponseOut }
     * 
     */
    public QueryMsisdnStatusResponseOut createQueryMsisdnStatusResponseOut() {
        return new QueryMsisdnStatusResponseOut();
    }

    /**
     * Create an instance of {@link RetrievePrimaryOfferResponse }
     * 
     */
    public RetrievePrimaryOfferResponse createRetrievePrimaryOfferResponse() {
        return new RetrievePrimaryOfferResponse();
    }

    /**
     * Create an instance of {@link RetrievePrimaryOfferOut }
     * 
     */
    public RetrievePrimaryOfferOut createRetrievePrimaryOfferOut() {
        return new RetrievePrimaryOfferOut();
    }

    /**
     * Create an instance of {@link AzerfonQuerySubscriberInfoRequest }
     * 
     */
    public AzerfonQuerySubscriberInfoRequest createAzerfonQuerySubscriberInfoRequest() {
        return new AzerfonQuerySubscriberInfoRequest();
    }

    /**
     * Create an instance of {@link QuerySubscriberInfoRequestIn }
     * 
     */
    public QuerySubscriberInfoRequestIn createQuerySubscriberInfoRequestIn() {
        return new QuerySubscriberInfoRequestIn();
    }

    /**
     * Create an instance of {@link AzerfonQuerySubscriberInfoResponse }
     * 
     */
    public AzerfonQuerySubscriberInfoResponse createAzerfonQuerySubscriberInfoResponse() {
        return new AzerfonQuerySubscriberInfoResponse();
    }

    /**
     * Create an instance of {@link QuerySubscriberInfoResponseOut }
     * 
     */
    public QuerySubscriberInfoResponseOut createQuerySubscriberInfoResponseOut() {
        return new QuerySubscriberInfoResponseOut();
    }

    /**
     * Create an instance of {@link RetrievePrimaryOfferRequest }
     * 
     */
    public RetrievePrimaryOfferRequest createRetrievePrimaryOfferRequest() {
        return new RetrievePrimaryOfferRequest();
    }

    /**
     * Create an instance of {@link RetrievePrimaryOfferIn }
     * 
     */
    public RetrievePrimaryOfferIn createRetrievePrimaryOfferIn() {
        return new RetrievePrimaryOfferIn();
    }

    /**
     * Create an instance of {@link GetLatestBillFileRequest }
     * 
     */
    public GetLatestBillFileRequest createGetLatestBillFileRequest() {
        return new GetLatestBillFileRequest();
    }

    /**
     * Create an instance of {@link GetLatestBillFileIn }
     * 
     */
    public GetLatestBillFileIn createGetLatestBillFileIn() {
        return new GetLatestBillFileIn();
    }

    /**
     * Create an instance of {@link QueryMsisdnStatusRspItem }
     * 
     */
    public QueryMsisdnStatusRspItem createQueryMsisdnStatusRspItem() {
        return new QueryMsisdnStatusRspItem();
    }

    /**
     * Create an instance of {@link QuerySubscriberInfoItem }
     * 
     */
    public QuerySubscriberInfoItem createQuerySubscriberInfoItem() {
        return new QuerySubscriberInfoItem();
    }

    /**
     * Create an instance of {@link QuerySubscriberInfoRspItem }
     * 
     */
    public QuerySubscriberInfoRspItem createQuerySubscriberInfoRspItem() {
        return new QuerySubscriberInfoRspItem();
    }

    /**
     * Create an instance of {@link QueryMsisdnStatusItem }
     * 
     */
    public QueryMsisdnStatusItem createQueryMsisdnStatusItem() {
        return new QueryMsisdnStatusItem();
    }

}
