
package com.huawei.crm.azerfon.bss.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.azerfon.bss.basetype.RequestHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/azerfon/bss/basetype/}RequestHeader"/>
 *         &lt;element name="GetLatestBillFileBody" type="{http://crm.huawei.com/azerfon/bss/query/}GetLatestBillFileIn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "getLatestBillFileBody"
})
@XmlRootElement(name = "GetLatestBillFileRequest")
public class GetLatestBillFileRequest {

    @XmlElement(name = "RequestHeader", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "GetLatestBillFileBody", required = true)
    protected GetLatestBillFileIn getLatestBillFileBody;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the getLatestBillFileBody property.
     * 
     * @return
     *     possible object is
     *     {@link GetLatestBillFileIn }
     *     
     */
    public GetLatestBillFileIn getGetLatestBillFileBody() {
        return getLatestBillFileBody;
    }

    /**
     * Sets the value of the getLatestBillFileBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLatestBillFileIn }
     *     
     */
    public void setGetLatestBillFileBody(GetLatestBillFileIn value) {
        this.getLatestBillFileBody = value;
    }

}
