
package com.huawei.crm.azerfon.bss.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.azerfon.bss.basetype.ResponseHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHeader" type="{http://crm.huawei.com/azerfon/bss/basetype/}ResponseHeader"/>
 *         &lt;element name="QuerySubscriberInfoResponseBody" type="{http://crm.huawei.com/azerfon/bss/query/}QuerySubscriberInfoResponseOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseHeader",
    "querySubscriberInfoResponseBody"
})
@XmlRootElement(name = "AzerfonQuerySubscriberInfoResponse")
public class AzerfonQuerySubscriberInfoResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(name = "QuerySubscriberInfoResponseBody")
    protected QuerySubscriberInfoResponseOut querySubscriberInfoResponseBody;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the querySubscriberInfoResponseBody property.
     * 
     * @return
     *     possible object is
     *     {@link QuerySubscriberInfoResponseOut }
     *     
     */
    public QuerySubscriberInfoResponseOut getQuerySubscriberInfoResponseBody() {
        return querySubscriberInfoResponseBody;
    }

    /**
     * Sets the value of the querySubscriberInfoResponseBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuerySubscriberInfoResponseOut }
     *     
     */
    public void setQuerySubscriberInfoResponseBody(QuerySubscriberInfoResponseOut value) {
        this.querySubscriberInfoResponseBody = value;
    }

}
