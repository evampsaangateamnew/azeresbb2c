
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CSType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CSType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;maxLength value="1"/>
 *     &lt;enumeration value="B"/>
 *     &lt;enumeration value="W"/>
 *     &lt;enumeration value="N"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CSType")
@XmlEnum
public enum CSType {


    /**
     * BlackList
     * 
     */
    B,

    /**
     * WhiteList
     * 
     */
    W,

    /**
     * Not Limited
     * 
     */
    N;

    public String value() {
        return name();
    }

    public static CSType fromValue(String v) {
        return valueOf(v);
    }

}
