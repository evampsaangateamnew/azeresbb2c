
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorpCustMember complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorpCustMember">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/azerfon/bss/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="MemberList" type="{http://crm.huawei.com/azerfon/bss/basetype/}CorpMemberList"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorpCustMember", propOrder = {

})
public class CorpCustMember {

    @XmlElement(name = "CustomerRelaId")
    protected String customerRelaId;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "MemberList", required = true)
    protected CorpMemberList memberList;

    /**
     * Gets the value of the customerRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRelaId() {
        return customerRelaId;
    }

    /**
     * Sets the value of the customerRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRelaId(String value) {
        this.customerRelaId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the memberList property.
     * 
     * @return
     *     possible object is
     *     {@link CorpMemberList }
     *     
     */
    public CorpMemberList getMemberList() {
        return memberList;
    }

    /**
     * Sets the value of the memberList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorpMemberList }
     *     
     */
    public void setMemberList(CorpMemberList value) {
        this.memberList = value;
    }

}
