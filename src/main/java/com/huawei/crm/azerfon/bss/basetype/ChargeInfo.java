
package com.huawei.crm.azerfon.bss.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="FeeActionType" type="{http://crm.huawei.com/azerfon/bss/basetype/}FeeActionType" minOccurs="0"/>
 *         &lt;element name="AcctRelaId" type="{http://crm.huawei.com/azerfon/bss/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/azerfon/bss/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="DefaultPayType" type="{http://crm.huawei.com/azerfon/bss/basetype/}PayType" minOccurs="0"/>
 *         &lt;element name="ChargeFeeList" type="{http://crm.huawei.com/azerfon/bss/basetype/}ChargeFeeList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeInfo", propOrder = {

})
public class ChargeInfo {

    @XmlElement(name = "FeeActionType")
    protected String feeActionType;
    @XmlElement(name = "AcctRelaId")
    protected String acctRelaId;
    @XmlElement(name = "AccountId")
    protected Long accountId;
    @XmlElement(name = "DefaultPayType")
    protected String defaultPayType;
    @XmlElement(name = "ChargeFeeList")
    protected ChargeFeeList chargeFeeList;

    /**
     * Gets the value of the feeActionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeActionType() {
        return feeActionType;
    }

    /**
     * Sets the value of the feeActionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeActionType(String value) {
        this.feeActionType = value;
    }

    /**
     * Gets the value of the acctRelaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctRelaId() {
        return acctRelaId;
    }

    /**
     * Sets the value of the acctRelaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctRelaId(String value) {
        this.acctRelaId = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountId(Long value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the defaultPayType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultPayType() {
        return defaultPayType;
    }

    /**
     * Sets the value of the defaultPayType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultPayType(String value) {
        this.defaultPayType = value;
    }

    /**
     * Gets the value of the chargeFeeList property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeFeeList }
     *     
     */
    public ChargeFeeList getChargeFeeList() {
        return chargeFeeList;
    }

    /**
     * Sets the value of the chargeFeeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeFeeList }
     *     
     */
    public void setChargeFeeList(ChargeFeeList value) {
        this.chargeFeeList = value;
    }

}
