package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DiscountInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="DiscountInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="DiscountCode" type="{http://crm.huawei.com/basetype/}DiscountCode" minOccurs="0"/>
 *         &lt;element name="DiscountType" type="{http://crm.huawei.com/basetype/}DiscountType" minOccurs="0"/>
 *         &lt;element name="DiscountFee" type="{http://crm.huawei.com/basetype/}DiscountFee" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscountInfo", propOrder = {})
public class DiscountInfo {
	@XmlElement(name = "DiscountCode")
	protected String discountCode;
	@XmlElement(name = "DiscountType")
	protected String discountType;
	@XmlElement(name = "DiscountFee")
	protected Long discountFee;

	/**
	 * Gets the value of the discountCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDiscountCode() {
		return discountCode;
	}

	/**
	 * Sets the value of the discountCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDiscountCode(String value) {
		this.discountCode = value;
	}

	/**
	 * Gets the value of the discountType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDiscountType() {
		return discountType;
	}

	/**
	 * Sets the value of the discountType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDiscountType(String value) {
		this.discountType = value;
	}

	/**
	 * Gets the value of the discountFee property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getDiscountFee() {
		return discountFee;
	}

	/**
	 * Sets the value of the discountFee property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setDiscountFee(Long value) {
		this.discountFee = value;
	}
}
