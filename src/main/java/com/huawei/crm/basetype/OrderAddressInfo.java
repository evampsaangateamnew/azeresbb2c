package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OrderAddressInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OrderAddressInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AddressType" type="{http://crm.huawei.com/basetype/}AddressType"/>
 *         &lt;element name="AddressClass" type="{http://crm.huawei.com/basetype/}AddressClass"/>
 *         &lt;element name="Address1" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address2" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address3" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address4" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address5" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address6" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address7" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address8" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address9" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="Address10" type="{http://crm.huawei.com/basetype/}AddressName" minOccurs="0"/>
 *         &lt;element name="UnstructuredAddress" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="256"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PostCode" type="{http://crm.huawei.com/basetype/}PostCode" minOccurs="0"/>
 *         &lt;element name="ThirdPartyAddressId" type="{http://crm.huawei.com/basetype/}ExternalOrderId" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderAddressInfo", propOrder = {})
public class OrderAddressInfo {
	@XmlElement(name = "AddressType", required = true)
	protected String addressType;
	@XmlElement(name = "AddressClass", required = true)
	protected String addressClass;
	@XmlElement(name = "Address1")
	protected String address1;
	@XmlElement(name = "Address2")
	protected String address2;
	@XmlElement(name = "Address3")
	protected String address3;
	@XmlElement(name = "Address4")
	protected String address4;
	@XmlElement(name = "Address5")
	protected String address5;
	@XmlElement(name = "Address6")
	protected String address6;
	@XmlElement(name = "Address7")
	protected String address7;
	@XmlElement(name = "Address8")
	protected String address8;
	@XmlElement(name = "Address9")
	protected String address9;
	@XmlElement(name = "Address10")
	protected String address10;
	@XmlElement(name = "UnstructuredAddress")
	protected String unstructuredAddress;
	@XmlElement(name = "PostCode")
	protected String postCode;
	@XmlElement(name = "ThirdPartyAddressId")
	protected String thirdPartyAddressId;

	/**
	 * Gets the value of the addressType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddressType() {
		return addressType;
	}

	/**
	 * Sets the value of the addressType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddressType(String value) {
		this.addressType = value;
	}

	/**
	 * Gets the value of the addressClass property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddressClass() {
		return addressClass;
	}

	/**
	 * Sets the value of the addressClass property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddressClass(String value) {
		this.addressClass = value;
	}

	/**
	 * Gets the value of the address1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * Sets the value of the address1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress1(String value) {
		this.address1 = value;
	}

	/**
	 * Gets the value of the address2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * Sets the value of the address2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress2(String value) {
		this.address2 = value;
	}

	/**
	 * Gets the value of the address3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * Sets the value of the address3 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress3(String value) {
		this.address3 = value;
	}

	/**
	 * Gets the value of the address4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress4() {
		return address4;
	}

	/**
	 * Sets the value of the address4 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress4(String value) {
		this.address4 = value;
	}

	/**
	 * Gets the value of the address5 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress5() {
		return address5;
	}

	/**
	 * Sets the value of the address5 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress5(String value) {
		this.address5 = value;
	}

	/**
	 * Gets the value of the address6 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress6() {
		return address6;
	}

	/**
	 * Sets the value of the address6 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress6(String value) {
		this.address6 = value;
	}

	/**
	 * Gets the value of the address7 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress7() {
		return address7;
	}

	/**
	 * Sets the value of the address7 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress7(String value) {
		this.address7 = value;
	}

	/**
	 * Gets the value of the address8 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress8() {
		return address8;
	}

	/**
	 * Sets the value of the address8 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress8(String value) {
		this.address8 = value;
	}

	/**
	 * Gets the value of the address9 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress9() {
		return address9;
	}

	/**
	 * Sets the value of the address9 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress9(String value) {
		this.address9 = value;
	}

	/**
	 * Gets the value of the address10 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress10() {
		return address10;
	}

	/**
	 * Sets the value of the address10 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress10(String value) {
		this.address10 = value;
	}

	/**
	 * Gets the value of the unstructuredAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUnstructuredAddress() {
		return unstructuredAddress;
	}

	/**
	 * Sets the value of the unstructuredAddress property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUnstructuredAddress(String value) {
		this.unstructuredAddress = value;
	}

	/**
	 * Gets the value of the postCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * Sets the value of the postCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPostCode(String value) {
		this.postCode = value;
	}

	/**
	 * Gets the value of the thirdPartyAddressId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getThirdPartyAddressId() {
		return thirdPartyAddressId;
	}

	/**
	 * Sets the value of the thirdPartyAddressId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setThirdPartyAddressId(String value) {
		this.thirdPartyAddressId = value;
	}
}
