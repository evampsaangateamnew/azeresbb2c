package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ExtShippingInnfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ExtShippingInnfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShippingServiceID" type="{http://crm.huawei.com/basetype/}ShippingModeID"/>
 *         &lt;element name="address" type="{http://crm.huawei.com/basetype/}OrderAddressInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtShippingInnfo", propOrder = { "shippingServiceID", "address" })
public class ExtShippingInnfo {
	@XmlElement(name = "ShippingServiceID", required = true)
	protected String shippingServiceID;
	protected OrderAddressInfo address;

	/**
	 * Gets the value of the shippingServiceID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShippingServiceID() {
		return shippingServiceID;
	}

	/**
	 * Sets the value of the shippingServiceID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setShippingServiceID(String value) {
		this.shippingServiceID = value;
	}

	/**
	 * Gets the value of the address property.
	 * 
	 * @return possible object is {@link OrderAddressInfo }
	 * 
	 */
	public OrderAddressInfo getAddress() {
		return address;
	}

	/**
	 * Sets the value of the address property.
	 * 
	 * @param value
	 *            allowed object is {@link OrderAddressInfo }
	 * 
	 */
	public void setAddress(OrderAddressInfo value) {
		this.address = value;
	}
}
