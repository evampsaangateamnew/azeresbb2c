package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for InvoiceNoInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="InvoiceNoInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="InvoiceNo" type="{http://crm.huawei.com/basetype/}InvoiceNo"/>
 *         &lt;element name="PaymentNo" type="{http://crm.huawei.com/basetype/}PaymentNo"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoiceNoInfo", propOrder = {})
public class InvoiceNoInfo {
	@XmlElement(name = "InvoiceNo", required = true)
	protected String invoiceNo;
	@XmlElement(name = "PaymentNo", required = true)
	protected String paymentNo;

	/**
	 * Gets the value of the invoiceNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInvoiceNo() {
		return invoiceNo;
	}

	/**
	 * Sets the value of the invoiceNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInvoiceNo(String value) {
		this.invoiceNo = value;
	}

	/**
	 * Gets the value of the paymentNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentNo() {
		return paymentNo;
	}

	/**
	 * Sets the value of the paymentNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentNo(String value) {
		this.paymentNo = value;
	}
}
