package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MemberSubscriberInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="MemberSubscriberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberSubsId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="MemberServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="MemberType" type="{http://crm.huawei.com/basetype/}GroupMemberType" minOccurs="0"/>
 *         &lt;element name="OperateType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="MemberClass" type="{http://crm.huawei.com/basetype/}GroupMemberClass" minOccurs="0"/>
 *         &lt;element name="MemberRole" type="{http://crm.huawei.com/basetype/}GroupMemberRole" minOccurs="0"/>
 *         &lt;element name="HouseHolder" type="{http://crm.huawei.com/basetype/}HouseHolder" minOccurs="0"/>
 *         &lt;element name="MemberShortNo" type="{http://crm.huawei.com/basetype/}GroupMemberShortNo" minOccurs="0"/>
 *         &lt;element name="PaymentPlanList" type="{http://crm.huawei.com/basetype/}PaymentPlanInfo" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://crm.huawei.com/basetype/}OfferingList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *         &lt;element name="MemberStatus" type="{http://crm.huawei.com/basetype/}MemberStatus" minOccurs="0"/>
 *         &lt;element name="MemberActiveTime" type="{http://crm.huawei.com/basetype/}MemberActiveTime" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://crm.huawei.com/basetype/}FirstName" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://crm.huawei.com/basetype/}MiddleName" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://crm.huawei.com/basetype/}LastName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MemberSubscriberInfo", propOrder = { "memberSubsId", "memberServiceNumber", "memberType",
		"operateType", "memberClass", "memberRole", "houseHolder", "memberShortNo", "paymentPlanList",
		"supplementaryOfferingList", "extParamList", "memberStatus", "memberActiveTime", "firstName", "middleName",
		"lastName" })
public class MemberSubscriberInfo {
	@XmlElement(name = "MemberSubsId")
	protected Long memberSubsId;
	@XmlElement(name = "MemberServiceNumber")
	protected String memberServiceNumber;
	@XmlElement(name = "MemberType")
	protected String memberType;
	@XmlElement(name = "OperateType")
	protected Object operateType;
	@XmlElement(name = "MemberClass")
	protected String memberClass;
	@XmlElement(name = "MemberRole")
	protected String memberRole;
	@XmlElement(name = "HouseHolder")
	protected String houseHolder;
	@XmlElement(name = "MemberShortNo")
	protected String memberShortNo;
	@XmlElement(name = "PaymentPlanList")
	protected PaymentPlanInfo paymentPlanList;
	@XmlElement(name = "SupplementaryOfferingList")
	protected OfferingList supplementaryOfferingList;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;
	@XmlElement(name = "MemberStatus")
	protected String memberStatus;
	@XmlElement(name = "MemberActiveTime")
	protected String memberActiveTime;
	@XmlElement(name = "FirstName")
	protected String firstName;
	@XmlElement(name = "MiddleName")
	protected String middleName;
	@XmlElement(name = "LastName")
	protected String lastName;

	/**
	 * Gets the value of the memberSubsId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getMemberSubsId() {
		return memberSubsId;
	}

	/**
	 * Sets the value of the memberSubsId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setMemberSubsId(Long value) {
		this.memberSubsId = value;
	}

	/**
	 * Gets the value of the memberServiceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberServiceNumber() {
		return memberServiceNumber;
	}

	/**
	 * Sets the value of the memberServiceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberServiceNumber(String value) {
		this.memberServiceNumber = value;
	}

	/**
	 * Gets the value of the memberType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberType() {
		return memberType;
	}

	/**
	 * Sets the value of the memberType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberType(String value) {
		this.memberType = value;
	}

	/**
	 * Gets the value of the operateType property.
	 * 
	 * @return possible object is {@link Object }
	 * 
	 */
	public Object getOperateType() {
		return operateType;
	}

	/**
	 * Sets the value of the operateType property.
	 * 
	 * @param value
	 *            allowed object is {@link Object }
	 * 
	 */
	public void setOperateType(Object value) {
		this.operateType = value;
	}

	/**
	 * Gets the value of the memberClass property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberClass() {
		return memberClass;
	}

	/**
	 * Sets the value of the memberClass property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberClass(String value) {
		this.memberClass = value;
	}

	/**
	 * Gets the value of the memberRole property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberRole() {
		return memberRole;
	}

	/**
	 * Sets the value of the memberRole property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberRole(String value) {
		this.memberRole = value;
	}

	/**
	 * Gets the value of the houseHolder property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getHouseHolder() {
		return houseHolder;
	}

	/**
	 * Sets the value of the houseHolder property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setHouseHolder(String value) {
		this.houseHolder = value;
	}

	/**
	 * Gets the value of the memberShortNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberShortNo() {
		return memberShortNo;
	}

	/**
	 * Sets the value of the memberShortNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberShortNo(String value) {
		this.memberShortNo = value;
	}

	/**
	 * Gets the value of the paymentPlanList property.
	 * 
	 * @return possible object is {@link PaymentPlanInfo }
	 * 
	 */
	public PaymentPlanInfo getPaymentPlanList() {
		return paymentPlanList;
	}

	/**
	 * Sets the value of the paymentPlanList property.
	 * 
	 * @param value
	 *            allowed object is {@link PaymentPlanInfo }
	 * 
	 */
	public void setPaymentPlanList(PaymentPlanInfo value) {
		this.paymentPlanList = value;
	}

	/**
	 * Gets the value of the supplementaryOfferingList property.
	 * 
	 * @return possible object is {@link OfferingList }
	 * 
	 */
	public OfferingList getSupplementaryOfferingList() {
		return supplementaryOfferingList;
	}

	/**
	 * Sets the value of the supplementaryOfferingList property.
	 * 
	 * @param value
	 *            allowed object is {@link OfferingList }
	 * 
	 */
	public void setSupplementaryOfferingList(OfferingList value) {
		this.supplementaryOfferingList = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}

	/**
	 * Gets the value of the memberStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberStatus() {
		return memberStatus;
	}

	/**
	 * Sets the value of the memberStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberStatus(String value) {
		this.memberStatus = value;
	}

	/**
	 * Gets the value of the memberActiveTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberActiveTime() {
		return memberActiveTime;
	}

	/**
	 * Sets the value of the memberActiveTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberActiveTime(String value) {
		this.memberActiveTime = value;
	}

	/**
	 * Gets the value of the firstName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the value of the firstName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFirstName(String value) {
		this.firstName = value;
	}

	/**
	 * Gets the value of the middleName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * Sets the value of the middleName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMiddleName(String value) {
		this.middleName = value;
	}

	/**
	 * Gets the value of the lastName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the value of the lastName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLastName(String value) {
		this.lastName = value;
	}
}
