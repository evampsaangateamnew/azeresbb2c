package com.huawei.crm.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CorpOrgList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CorpOrgList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorpOrgInfo" type="{http://crm.huawei.com/basetype/}CorpCustOrg" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorpOrgList", propOrder = { "corpOrgInfo" })
public class CorpOrgList {
	@XmlElement(name = "CorpOrgInfo", required = true)
	protected List<CorpCustOrg> corpOrgInfo;

	/**
	 * Gets the value of the corpOrgInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the corpOrgInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCorpOrgInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CorpCustOrg }
	 * 
	 * 
	 */
	public List<CorpCustOrg> getCorpOrgInfo() {
		if (corpOrgInfo == null) {
			corpOrgInfo = new ArrayList<CorpCustOrg>();
		}
		return this.corpOrgInfo;
	}
}
