package com.huawei.crm.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ChargeFeeList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeFeeList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChargeFeeInfo" type="{http://crm.huawei.com/basetype/}ChargeFeeInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeFeeList", propOrder = { "chargeFeeInfo" })
public class ChargeFeeList {
	@XmlElement(name = "ChargeFeeInfo", required = true)
	protected List<ChargeFeeInfo> chargeFeeInfo;

	/**
	 * Gets the value of the chargeFeeInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the chargeFeeInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getChargeFeeInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ChargeFeeInfo }
	 * 
	 * 
	 */
	public List<ChargeFeeInfo> getChargeFeeInfo() {
		if (chargeFeeInfo == null) {
			chargeFeeInfo = new ArrayList<ChargeFeeInfo>();
		}
		return this.chargeFeeInfo;
	}
}
