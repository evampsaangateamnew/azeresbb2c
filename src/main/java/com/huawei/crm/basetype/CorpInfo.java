package com.huawei.crm.basetype;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CorpInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CorpInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CorpNo" type="{http://crm.huawei.com/basetype/}CorporateNumber" minOccurs="0"/>
 *         &lt;element name="IDType" type="{http://crm.huawei.com/basetype/}CertificateType" minOccurs="0"/>
 *         &lt;element name="IDNumber" type="{http://crm.huawei.com/basetype/}CertificateNumber" minOccurs="0"/>
 *         &lt;element name="IDValidity" type="{http://crm.huawei.com/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="CustNo" type="{http://crm.huawei.com/basetype/}CorporateNumber" minOccurs="0"/>
 *         &lt;element name="CustType" type="{http://crm.huawei.com/basetype/}CustomerType" minOccurs="0"/>
 *         &lt;element name="CustGrade" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="CustName" type="{http://crm.huawei.com/basetype/}CustomerName" minOccurs="0"/>
 *         &lt;element name="CustShortName" type="{http://crm.huawei.com/basetype/}CustomerShortName" minOccurs="0"/>
 *         &lt;element name="CustSize" type="{http://crm.huawei.com/basetype/}CorporateCustSizeLevel" minOccurs="0"/>
 *         &lt;element name="Industry" type="{http://crm.huawei.com/basetype/}Industry" minOccurs="0"/>
 *         &lt;element name="SubIndustry" type="{http://crm.huawei.com/basetype/}SubIndustry" minOccurs="0"/>
 *         &lt;element name="CustPhoneNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}ServiceNumber">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CustFaxNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}RelaFax">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CustEmail" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}RelaEmail">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CustWebSite" type="{http://crm.huawei.com/basetype/}CustomerWebSite" minOccurs="0"/>
 *         &lt;element name="RegisterDate" type="{http://crm.huawei.com/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="RegisterCapital" type="{http://crm.huawei.com/basetype/}RegisterCapital" minOccurs="0"/>
 *         &lt;element name="CorpCustOrg" type="{http://crm.huawei.com/basetype/}CorpOrgList" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/basetype/}Remark" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorpInfo", propOrder = {})
public class CorpInfo {
	@XmlElement(name = "CorpNo")
	protected String corpNo;
	@XmlElementRef(name = "IDType", namespace = "http://crm.huawei.com/basetype/", type = JAXBElement.class)
	protected JAXBElement<String> idType;
	@XmlElement(name = "IDNumber")
	protected String idNumber;
	@XmlElement(name = "IDValidity")
	protected String idValidity;
	@XmlElement(name = "CustNo")
	protected String custNo;
	@XmlElement(name = "CustType")
	protected String custType;
	@XmlElement(name = "CustGrade")
	protected String custGrade;
	@XmlElement(name = "CustName")
	protected String custName;
	@XmlElement(name = "CustShortName")
	protected String custShortName;
	@XmlElement(name = "CustSize")
	protected String custSize;
	@XmlElement(name = "Industry")
	protected String industry;
	@XmlElement(name = "SubIndustry")
	protected String subIndustry;
	@XmlElement(name = "CustPhoneNumber")
	protected String custPhoneNumber;
	@XmlElement(name = "CustFaxNumber")
	protected String custFaxNumber;
	@XmlElement(name = "CustEmail")
	protected String custEmail;
	@XmlElement(name = "CustWebSite")
	protected String custWebSite;
	@XmlElement(name = "RegisterDate")
	protected String registerDate;
	@XmlElement(name = "RegisterCapital")
	protected String registerCapital;
	@XmlElement(name = "CorpCustOrg")
	protected CorpOrgList corpCustOrg;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;
	@XmlElement(name = "Remark")
	protected String remark;

	/**
	 * Gets the value of the corpNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorpNo() {
		return corpNo;
	}

	/**
	 * Sets the value of the corpNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorpNo(String value) {
		this.corpNo = value;
	}

	/**
	 * Gets the value of the idType property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String
	 *         }{@code >}
	 * 
	 */
	public JAXBElement<String> getIDType() {
		return idType;
	}

	/**
	 * Sets the value of the idType property.
	 * 
	 * @param value
	 *            allowed object is {@link JAXBElement }{@code <}{@link String
	 *            }{@code >}
	 * 
	 */
	public void setIDType(JAXBElement<String> value) {
		this.idType = (value);
	}

	/**
	 * Gets the value of the idNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIDNumber() {
		return idNumber;
	}

	/**
	 * Sets the value of the idNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIDNumber(String value) {
		this.idNumber = value;
	}

	/**
	 * Gets the value of the idValidity property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIDValidity() {
		return idValidity;
	}

	/**
	 * Sets the value of the idValidity property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIDValidity(String value) {
		this.idValidity = value;
	}

	/**
	 * Gets the value of the custNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustNo() {
		return custNo;
	}

	/**
	 * Sets the value of the custNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustNo(String value) {
		this.custNo = value;
	}

	/**
	 * Gets the value of the custType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustType() {
		return custType;
	}

	/**
	 * Sets the value of the custType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustType(String value) {
		this.custType = value;
	}

	/**
	 * Gets the value of the custGrade property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustGrade() {
		return custGrade;
	}

	/**
	 * Sets the value of the custGrade property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustGrade(String value) {
		this.custGrade = value;
	}

	/**
	 * Gets the value of the custName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustName() {
		return custName;
	}

	/**
	 * Sets the value of the custName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustName(String value) {
		this.custName = value;
	}

	/**
	 * Gets the value of the custShortName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustShortName() {
		return custShortName;
	}

	/**
	 * Sets the value of the custShortName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustShortName(String value) {
		this.custShortName = value;
	}

	/**
	 * Gets the value of the custSize property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustSize() {
		return custSize;
	}

	/**
	 * Sets the value of the custSize property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustSize(String value) {
		this.custSize = value;
	}

	/**
	 * Gets the value of the industry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIndustry() {
		return industry;
	}

	/**
	 * Sets the value of the industry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIndustry(String value) {
		this.industry = value;
	}

	/**
	 * Gets the value of the subIndustry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubIndustry() {
		return subIndustry;
	}

	/**
	 * Sets the value of the subIndustry property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubIndustry(String value) {
		this.subIndustry = value;
	}

	/**
	 * Gets the value of the custPhoneNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustPhoneNumber() {
		return custPhoneNumber;
	}

	/**
	 * Sets the value of the custPhoneNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustPhoneNumber(String value) {
		this.custPhoneNumber = value;
	}

	/**
	 * Gets the value of the custFaxNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustFaxNumber() {
		return custFaxNumber;
	}

	/**
	 * Sets the value of the custFaxNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustFaxNumber(String value) {
		this.custFaxNumber = value;
	}

	/**
	 * Gets the value of the custEmail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustEmail() {
		return custEmail;
	}

	/**
	 * Sets the value of the custEmail property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustEmail(String value) {
		this.custEmail = value;
	}

	/**
	 * Gets the value of the custWebSite property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustWebSite() {
		return custWebSite;
	}

	/**
	 * Sets the value of the custWebSite property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustWebSite(String value) {
		this.custWebSite = value;
	}

	/**
	 * Gets the value of the registerDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegisterDate() {
		return registerDate;
	}

	/**
	 * Sets the value of the registerDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegisterDate(String value) {
		this.registerDate = value;
	}

	/**
	 * Gets the value of the registerCapital property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegisterCapital() {
		return registerCapital;
	}

	/**
	 * Sets the value of the registerCapital property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegisterCapital(String value) {
		this.registerCapital = value;
	}

	/**
	 * Gets the value of the corpCustOrg property.
	 * 
	 * @return possible object is {@link CorpOrgList }
	 * 
	 */
	public CorpOrgList getCorpCustOrg() {
		return corpCustOrg;
	}

	/**
	 * Sets the value of the corpCustOrg property.
	 * 
	 * @param value
	 *            allowed object is {@link CorpOrgList }
	 * 
	 */
	public void setCorpCustOrg(CorpOrgList value) {
		this.corpCustOrg = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}
}
