package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetOrderItemInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetOrderItemInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OrderItemId" type="{http://crm.huawei.com/basetype/}InternalOrderItemId"/>
 *         &lt;element name="ExternalOrderItemId" type="{http://crm.huawei.com/basetype/}ExternalOrderItemId" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}OrderStatus"/>
 *         &lt;element name="LastChangeTime" type="{http://crm.huawei.com/basetype/}Time"/>
 *         &lt;element name="CurrentNodeName" type="{http://crm.huawei.com/basetype/}CurrentNodeName" minOccurs="0"/>
 *         &lt;element name="OrderItemType" type="{http://crm.huawei.com/basetype/}OrderItemType" minOccurs="0"/>
 *         &lt;element name="ExecutionTime" type="{http://crm.huawei.com/basetype/}Time"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetOrderItemInfo", propOrder = {})
public class GetOrderItemInfo {
	@XmlElement(name = "OrderItemId", required = true)
	protected String orderItemId;
	@XmlElement(name = "ExternalOrderItemId")
	protected String externalOrderItemId;
	@XmlElement(name = "Status", required = true)
	protected String status;
	@XmlElement(name = "LastChangeTime", required = true)
	protected String lastChangeTime;
	@XmlElement(name = "CurrentNodeName")
	protected String currentNodeName;
	@XmlElement(name = "OrderItemType")
	protected String orderItemType;
	@XmlElement(name = "ExecutionTime", required = true)
	protected String executionTime;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the orderItemId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderItemId() {
		return orderItemId;
	}

	/**
	 * Sets the value of the orderItemId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderItemId(String value) {
		this.orderItemId = value;
	}

	/**
	 * Gets the value of the externalOrderItemId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternalOrderItemId() {
		return externalOrderItemId;
	}

	/**
	 * Sets the value of the externalOrderItemId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternalOrderItemId(String value) {
		this.externalOrderItemId = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the lastChangeTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLastChangeTime() {
		return lastChangeTime;
	}

	/**
	 * Sets the value of the lastChangeTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLastChangeTime(String value) {
		this.lastChangeTime = value;
	}

	/**
	 * Gets the value of the currentNodeName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrentNodeName() {
		return currentNodeName;
	}

	/**
	 * Sets the value of the currentNodeName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrentNodeName(String value) {
		this.currentNodeName = value;
	}

	/**
	 * Gets the value of the orderItemType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderItemType() {
		return orderItemType;
	}

	/**
	 * Sets the value of the orderItemType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderItemType(String value) {
		this.orderItemType = value;
	}

	/**
	 * Gets the value of the executionTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExecutionTime() {
		return executionTime;
	}

	/**
	 * Sets the value of the executionTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExecutionTime(String value) {
		this.executionTime = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
