package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CallScreenTimeSegInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CallScreenTimeSegInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CallScreenTimeSegType" type="{http://crm.huawei.com/basetype/}CallScreenTimeSegType" minOccurs="0"/>
 *         &lt;element name="TimeSchemaID" type="{http://crm.huawei.com/basetype/}CallScreenTimeSchemaID" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="CallScreenRangeTime" type="{http://crm.huawei.com/basetype/}CallScreenRangeTime" minOccurs="0"/>
 *           &lt;element name="CallScreenEnumWeekDay" type="{http://crm.huawei.com/basetype/}CallScreenEnumWeekDay" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallScreenTimeSegInfo", propOrder = { "callScreenTimeSegType", "timeSchemaID", "callScreenRangeTime",
		"callScreenEnumWeekDay" })
public class CallScreenTimeSegInfo {
	@XmlElement(name = "CallScreenTimeSegType")
	protected String callScreenTimeSegType;
	@XmlElement(name = "TimeSchemaID")
	protected Long timeSchemaID;
	@XmlElement(name = "CallScreenRangeTime")
	protected CallScreenRangeTime callScreenRangeTime;
	@XmlElement(name = "CallScreenEnumWeekDay")
	protected String callScreenEnumWeekDay;

	/**
	 * Gets the value of the callScreenTimeSegType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCallScreenTimeSegType() {
		return callScreenTimeSegType;
	}

	/**
	 * Sets the value of the callScreenTimeSegType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCallScreenTimeSegType(String value) {
		this.callScreenTimeSegType = value;
	}

	/**
	 * Gets the value of the timeSchemaID property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getTimeSchemaID() {
		return timeSchemaID;
	}

	/**
	 * Sets the value of the timeSchemaID property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setTimeSchemaID(Long value) {
		this.timeSchemaID = value;
	}

	/**
	 * Gets the value of the callScreenRangeTime property.
	 * 
	 * @return possible object is {@link CallScreenRangeTime }
	 * 
	 */
	public CallScreenRangeTime getCallScreenRangeTime() {
		return callScreenRangeTime;
	}

	/**
	 * Sets the value of the callScreenRangeTime property.
	 * 
	 * @param value
	 *            allowed object is {@link CallScreenRangeTime }
	 * 
	 */
	public void setCallScreenRangeTime(CallScreenRangeTime value) {
		this.callScreenRangeTime = value;
	}

	/**
	 * Gets the value of the callScreenEnumWeekDay property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCallScreenEnumWeekDay() {
		return callScreenEnumWeekDay;
	}

	/**
	 * Sets the value of the callScreenEnumWeekDay property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCallScreenEnumWeekDay(String value) {
		this.callScreenEnumWeekDay = value;
	}
}
