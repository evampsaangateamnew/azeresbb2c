package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PointChangeInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PointChangeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PointType" type="{http://crm.huawei.com/basetype/}PointType" minOccurs="0"/>
 *         &lt;element name="PointAmount" type="{http://crm.huawei.com/basetype/}PointAmount" minOccurs="0"/>
 *         &lt;element name="ChangeReasonCode" type="{http://crm.huawei.com/basetype/}PointChangeReason"/>
 *         &lt;element name="PayFeeAmount" type="{http://crm.huawei.com/basetype/}PayFeeAmount" minOccurs="0"/>
 *         &lt;element name="DiscountFeeAmount" type="{http://crm.huawei.com/basetype/}DiscountFeeAmount" minOccurs="0"/>
 *         &lt;element name="RedeemSchemaID" type="{http://crm.huawei.com/basetype/}PointRedeemSchemaID" minOccurs="0"/>
 *         &lt;element name="RedeemPriceID" type="{http://crm.huawei.com/basetype/}RedeemPriceID" minOccurs="0"/>
 *         &lt;element name="RedeemQuantity" type="{http://crm.huawei.com/basetype/}RedeemQuantity" minOccurs="0"/>
 *         &lt;element name="RedeemUnit" type="{http://crm.huawei.com/basetype/}RedeemUnit" minOccurs="0"/>
 *         &lt;element name="CommodityID" type="{http://crm.huawei.com/basetype/}CommodityID" minOccurs="0"/>
 *         &lt;element name="ResourceModel" type="{http://crm.huawei.com/basetype/}ResourceModel" minOccurs="0"/>
 *         &lt;element name="ResourceCode" type="{http://crm.huawei.com/basetype/}ResourceCode" minOccurs="0"/>
 *         &lt;element name="OfferID" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="OfferOrderSeq" type="{http://crm.huawei.com/basetype/}OfferOrderSeq" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PointChangeInfo", propOrder = { "pointType", "pointAmount", "changeReasonCode", "payFeeAmount",
		"discountFeeAmount", "redeemSchemaID", "redeemPriceID", "redeemQuantity", "redeemUnit", "commodityID",
		"resourceModel", "resourceCode", "offerID", "offerOrderSeq" })
public class PointChangeInfo {
	@XmlElement(name = "PointType")
	protected String pointType;
	@XmlElement(name = "PointAmount")
	protected Float pointAmount;
	@XmlElement(name = "ChangeReasonCode", required = true)
	protected String changeReasonCode;
	@XmlElement(name = "PayFeeAmount")
	protected Long payFeeAmount;
	@XmlElement(name = "DiscountFeeAmount")
	protected Long discountFeeAmount;
	@XmlElement(name = "RedeemSchemaID")
	protected String redeemSchemaID;
	@XmlElement(name = "RedeemPriceID")
	protected String redeemPriceID;
	@XmlElement(name = "RedeemQuantity")
	protected Float redeemQuantity;
	@XmlElement(name = "RedeemUnit")
	protected String redeemUnit;
	@XmlElement(name = "CommodityID")
	protected String commodityID;
	@XmlElement(name = "ResourceModel")
	protected String resourceModel;
	@XmlElement(name = "ResourceCode")
	protected String resourceCode;
	@XmlElement(name = "OfferID")
	protected String offerID;
	@XmlElement(name = "OfferOrderSeq")
	protected Long offerOrderSeq;

	/**
	 * Gets the value of the pointType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPointType() {
		return pointType;
	}

	/**
	 * Sets the value of the pointType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPointType(String value) {
		this.pointType = value;
	}

	/**
	 * Gets the value of the pointAmount property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getPointAmount() {
		return pointAmount;
	}

	/**
	 * Sets the value of the pointAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Float }
	 * 
	 */
	public void setPointAmount(Float value) {
		this.pointAmount = value;
	}

	/**
	 * Gets the value of the changeReasonCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChangeReasonCode() {
		return changeReasonCode;
	}

	/**
	 * Sets the value of the changeReasonCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChangeReasonCode(String value) {
		this.changeReasonCode = value;
	}

	/**
	 * Gets the value of the payFeeAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getPayFeeAmount() {
		return payFeeAmount;
	}

	/**
	 * Sets the value of the payFeeAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setPayFeeAmount(Long value) {
		this.payFeeAmount = value;
	}

	/**
	 * Gets the value of the discountFeeAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getDiscountFeeAmount() {
		return discountFeeAmount;
	}

	/**
	 * Sets the value of the discountFeeAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setDiscountFeeAmount(Long value) {
		this.discountFeeAmount = value;
	}

	/**
	 * Gets the value of the redeemSchemaID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRedeemSchemaID() {
		return redeemSchemaID;
	}

	/**
	 * Sets the value of the redeemSchemaID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRedeemSchemaID(String value) {
		this.redeemSchemaID = value;
	}

	/**
	 * Gets the value of the redeemPriceID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRedeemPriceID() {
		return redeemPriceID;
	}

	/**
	 * Sets the value of the redeemPriceID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRedeemPriceID(String value) {
		this.redeemPriceID = value;
	}

	/**
	 * Gets the value of the redeemQuantity property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getRedeemQuantity() {
		return redeemQuantity;
	}

	/**
	 * Sets the value of the redeemQuantity property.
	 * 
	 * @param value
	 *            allowed object is {@link Float }
	 * 
	 */
	public void setRedeemQuantity(Float value) {
		this.redeemQuantity = value;
	}

	/**
	 * Gets the value of the redeemUnit property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRedeemUnit() {
		return redeemUnit;
	}

	/**
	 * Sets the value of the redeemUnit property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRedeemUnit(String value) {
		this.redeemUnit = value;
	}

	/**
	 * Gets the value of the commodityID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCommodityID() {
		return commodityID;
	}

	/**
	 * Sets the value of the commodityID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCommodityID(String value) {
		this.commodityID = value;
	}

	/**
	 * Gets the value of the resourceModel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getResourceModel() {
		return resourceModel;
	}

	/**
	 * Sets the value of the resourceModel property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setResourceModel(String value) {
		this.resourceModel = value;
	}

	/**
	 * Gets the value of the resourceCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getResourceCode() {
		return resourceCode;
	}

	/**
	 * Sets the value of the resourceCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setResourceCode(String value) {
		this.resourceCode = value;
	}

	/**
	 * Gets the value of the offerID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferID() {
		return offerID;
	}

	/**
	 * Sets the value of the offerID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferID(String value) {
		this.offerID = value;
	}

	/**
	 * Gets the value of the offerOrderSeq property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getOfferOrderSeq() {
		return offerOrderSeq;
	}

	/**
	 * Sets the value of the offerOrderSeq property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setOfferOrderSeq(Long value) {
		this.offerOrderSeq = value;
	}
}
