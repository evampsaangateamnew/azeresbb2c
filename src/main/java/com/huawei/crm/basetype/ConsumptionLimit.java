package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ConsumptionLimit complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ConsumptionLimit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}ActionType" minOccurs="0"/>
 *         &lt;element name="LimitType" type="{http://crm.huawei.com/basetype/}LimitType"/>
 *         &lt;element name="NotifyType" type="{http://crm.huawei.com/basetype/}NotifyType" minOccurs="0"/>
 *         &lt;element name="NotifyLimit" type="{http://crm.huawei.com/basetype/}NotifyLimit" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://crm.huawei.com/basetype/}Currency" minOccurs="0"/>
 *         &lt;element name="LimitValue" type="{http://crm.huawei.com/basetype/}LimitValue" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://crm.huawei.com/basetype/}Time"/>
 *         &lt;element name="ExpireDate" type="{http://crm.huawei.com/basetype/}Time"/>
 *         &lt;element name="UsedLimitValue" type="{http://crm.huawei.com/basetype/}UsedLimitValue" minOccurs="0"/>
 *         &lt;element name="RemainLimitValue" type="{http://crm.huawei.com/basetype/}RemainLimitValue" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumptionLimit", propOrder = {})
public class ConsumptionLimit {
	@XmlElement(name = "ActionType")
	protected String actionType;
	@XmlElement(name = "LimitType", required = true)
	protected String limitType;
	@XmlElement(name = "NotifyType")
	protected String notifyType;
	@XmlElement(name = "NotifyLimit")
	protected Long notifyLimit;
	@XmlElement(name = "Currency")
	protected String currency;
	@XmlElement(name = "LimitValue")
	protected Long limitValue;
	@XmlElement(name = "EffectiveDate", required = true)
	protected String effectiveDate;
	@XmlElement(name = "ExpireDate", required = true)
	protected String expireDate;
	@XmlElement(name = "UsedLimitValue")
	protected Long usedLimitValue;
	@XmlElement(name = "RemainLimitValue")
	protected Long remainLimitValue;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the limitType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitType() {
		return limitType;
	}

	/**
	 * Sets the value of the limitType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitType(String value) {
		this.limitType = value;
	}

	/**
	 * Gets the value of the notifyType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNotifyType() {
		return notifyType;
	}

	/**
	 * Sets the value of the notifyType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNotifyType(String value) {
		this.notifyType = value;
	}

	/**
	 * Gets the value of the notifyLimit property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getNotifyLimit() {
		return notifyLimit;
	}

	/**
	 * Sets the value of the notifyLimit property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setNotifyLimit(Long value) {
		this.notifyLimit = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the limitValue property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getLimitValue() {
		return limitValue;
	}

	/**
	 * Sets the value of the limitValue property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setLimitValue(Long value) {
		this.limitValue = value;
	}

	/**
	 * Gets the value of the effectiveDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Sets the value of the effectiveDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveDate(String value) {
		this.effectiveDate = value;
	}

	/**
	 * Gets the value of the expireDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * Sets the value of the expireDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpireDate(String value) {
		this.expireDate = value;
	}

	/**
	 * Gets the value of the usedLimitValue property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getUsedLimitValue() {
		return usedLimitValue;
	}

	/**
	 * Sets the value of the usedLimitValue property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setUsedLimitValue(Long value) {
		this.usedLimitValue = value;
	}

	/**
	 * Gets the value of the remainLimitValue property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getRemainLimitValue() {
		return remainLimitValue;
	}

	/**
	 * Sets the value of the remainLimitValue property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setRemainLimitValue(Long value) {
		this.remainLimitValue = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
