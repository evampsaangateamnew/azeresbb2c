package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PaymentInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="PaymentId" type="{http://crm.huawei.com/basetype/}PaymentId" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}ActionType" minOccurs="0"/>
 *         &lt;element name="PaymentMode" type="{http://crm.huawei.com/basetype/}PaymentModeType" minOccurs="0"/>
 *         &lt;element name="CreditCardType" type="{http://crm.huawei.com/basetype/}CreditCardType" minOccurs="0"/>
 *         &lt;element name="BankCode" type="{http://crm.huawei.com/basetype/}BankCode" minOccurs="0"/>
 *         &lt;element name="BankAcctNumber" type="{http://crm.huawei.com/basetype/}BankAcctNumber" minOccurs="0"/>
 *         &lt;element name="BankIssuer" type="{http://crm.huawei.com/basetype/}BankIssuer" minOccurs="0"/>
 *         &lt;element name="BankAcctName" type="{http://crm.huawei.com/basetype/}BankAcctName" minOccurs="0"/>
 *         &lt;element name="AuthFlag" type="{http://crm.huawei.com/basetype/}AuthFlag" minOccurs="0"/>
 *         &lt;element name="CardExpDate" type="{http://crm.huawei.com/basetype/}CardExpDate" minOccurs="0"/>
 *         &lt;element name="CVVNumber" type="{http://crm.huawei.com/basetype/}CVVNumber" minOccurs="0"/>
 *         &lt;element name="EffDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="Remark" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}string">
 *               &lt;maxLength value="256"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}PaymentStatus" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentInfo", propOrder = {})
public class PaymentInfo {
	@XmlElement(name = "PaymentId")
	protected String paymentId;
	@XmlElement(name = "ActionType")
	protected String actionType;
	@XmlElement(name = "PaymentMode")
	protected String paymentMode;
	@XmlElement(name = "CreditCardType")
	protected String creditCardType;
	@XmlElement(name = "BankCode")
	protected String bankCode;
	@XmlElement(name = "BankAcctNumber")
	protected String bankAcctNumber;
	@XmlElement(name = "BankIssuer")
	protected String bankIssuer;
	@XmlElement(name = "BankAcctName")
	protected String bankAcctName;
	@XmlElement(name = "AuthFlag")
	protected String authFlag;
	@XmlElement(name = "CardExpDate")
	protected String cardExpDate;
	@XmlElement(name = "CVVNumber")
	protected String cvvNumber;
	@XmlElement(name = "EffDate")
	protected String effDate;
	@XmlElement(name = "ExpDate")
	protected String expDate;
	@XmlElement(name = "Remark")
	protected String remark;
	@XmlElement(name = "Status")
	protected String status;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the paymentId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentId() {
		return paymentId;
	}

	/**
	 * Sets the value of the paymentId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentId(String value) {
		this.paymentId = value;
	}

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the paymentMode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentMode() {
		return paymentMode;
	}

	/**
	 * Sets the value of the paymentMode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentMode(String value) {
		this.paymentMode = value;
	}

	/**
	 * Gets the value of the creditCardType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreditCardType() {
		return creditCardType;
	}

	/**
	 * Sets the value of the creditCardType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreditCardType(String value) {
		this.creditCardType = value;
	}

	/**
	 * Gets the value of the bankCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * Sets the value of the bankCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankCode(String value) {
		this.bankCode = value;
	}

	/**
	 * Gets the value of the bankAcctNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankAcctNumber() {
		return bankAcctNumber;
	}

	/**
	 * Sets the value of the bankAcctNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankAcctNumber(String value) {
		this.bankAcctNumber = value;
	}

	/**
	 * Gets the value of the bankIssuer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankIssuer() {
		return bankIssuer;
	}

	/**
	 * Sets the value of the bankIssuer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankIssuer(String value) {
		this.bankIssuer = value;
	}

	/**
	 * Gets the value of the bankAcctName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBankAcctName() {
		return bankAcctName;
	}

	/**
	 * Sets the value of the bankAcctName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBankAcctName(String value) {
		this.bankAcctName = value;
	}

	/**
	 * Gets the value of the authFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthFlag() {
		return authFlag;
	}

	/**
	 * Sets the value of the authFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAuthFlag(String value) {
		this.authFlag = value;
	}

	/**
	 * Gets the value of the cardExpDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardExpDate() {
		return cardExpDate;
	}

	/**
	 * Sets the value of the cardExpDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCardExpDate(String value) {
		this.cardExpDate = value;
	}

	/**
	 * Gets the value of the cvvNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCVVNumber() {
		return cvvNumber;
	}

	/**
	 * Sets the value of the cvvNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCVVNumber(String value) {
		this.cvvNumber = value;
	}

	/**
	 * Gets the value of the effDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffDate() {
		return effDate;
	}

	/**
	 * Sets the value of the effDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffDate(String value) {
		this.effDate = value;
	}

	/**
	 * Gets the value of the expDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpDate() {
		return expDate;
	}

	/**
	 * Sets the value of the expDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpDate(String value) {
		this.expDate = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
