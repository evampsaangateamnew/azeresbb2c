package com.huawei.crm.basetype.ens;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CustomerDocumentList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerDocumentList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerDocumentInfo" type="{http://crm.huawei.com/basetype/}CustomerDocumentInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDocumentList", propOrder = { "customerDocumentInfo" })
public class CustomerDocumentList {
	@XmlElement(name = "CustomerDocumentInfo")
	protected List<CustomerDocumentInfo> customerDocumentInfo;

	/**
	 * Gets the value of the customerDocumentInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the customerDocumentInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCustomerDocumentInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CustomerDocumentInfo }
	 * 
	 * 
	 */
	public List<CustomerDocumentInfo> getCustomerDocumentInfo() {
		if (customerDocumentInfo == null) {
			customerDocumentInfo = new ArrayList<CustomerDocumentInfo>();
		}
		return this.customerDocumentInfo;
	}
}
