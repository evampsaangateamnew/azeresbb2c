package com.huawei.crm.basetype.ens;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AgreementFine complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AgreementFine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="FixAmount" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="VaryAmount" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="NrOfMonth" type="{http://crm.huawei.com/basetype/}PeriodCount" minOccurs="0"/>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="AgreementId" type="{http://crm.huawei.com/basetype/}AgreementId" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgreementFine", propOrder = {})
public class AgreementFine {
	@XmlElement(name = "FixAmount")
	protected Long fixAmount;
	@XmlElement(name = "VaryAmount")
	protected Long varyAmount;
	@XmlElement(name = "NrOfMonth")
	protected BigInteger nrOfMonth;
	@XmlElement(name = "OfferingId")
	protected String offeringId;
	@XmlElement(name = "AgreementId")
	protected String agreementId;

	/**
	 * Gets the value of the fixAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getFixAmount() {
		return fixAmount;
	}

	/**
	 * Sets the value of the fixAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setFixAmount(Long value) {
		this.fixAmount = value;
	}

	/**
	 * Gets the value of the varyAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getVaryAmount() {
		return varyAmount;
	}

	/**
	 * Sets the value of the varyAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setVaryAmount(Long value) {
		this.varyAmount = value;
	}

	/**
	 * Gets the value of the nrOfMonth property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getNrOfMonth() {
		return nrOfMonth;
	}

	/**
	 * Sets the value of the nrOfMonth property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setNrOfMonth(BigInteger value) {
		this.nrOfMonth = value;
	}

	/**
	 * Gets the value of the offeringId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * Sets the value of the offeringId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingId(String value) {
		this.offeringId = value;
	}

	/**
	 * Gets the value of the agreementId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAgreementId() {
		return agreementId;
	}

	/**
	 * Sets the value of the agreementId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAgreementId(String value) {
		this.agreementId = value;
	}
}
