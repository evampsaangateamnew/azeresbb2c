package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for HistoryParameterInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="HistoryParameterInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ParamName" type="{http://crm.huawei.com/basetype/}ParamName"/>
 *         &lt;element name="OldParamValue" type="{http://crm.huawei.com/basetype/}ParamValue"/>
 *         &lt;element name="NewParamValue" type="{http://crm.huawei.com/basetype/}ParamValue"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoryParameterInfo", propOrder = {})
public class HistoryParameterInfo {
	@XmlElement(name = "ParamName", required = true)
	protected String paramName;
	@XmlElement(name = "OldParamValue", required = true)
	protected String oldParamValue;
	@XmlElement(name = "NewParamValue", required = true)
	protected String newParamValue;

	/**
	 * Gets the value of the paramName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getParamName() {
		return paramName;
	}

	/**
	 * Sets the value of the paramName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setParamName(String value) {
		this.paramName = value;
	}

	/**
	 * Gets the value of the oldParamValue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOldParamValue() {
		return oldParamValue;
	}

	/**
	 * Sets the value of the oldParamValue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOldParamValue(String value) {
		this.oldParamValue = value;
	}

	/**
	 * Gets the value of the newParamValue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNewParamValue() {
		return newParamValue;
	}

	/**
	 * Sets the value of the newParamValue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNewParamValue(String value) {
		this.newParamValue = value;
	}
}
