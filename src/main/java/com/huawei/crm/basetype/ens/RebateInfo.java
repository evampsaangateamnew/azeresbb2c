package com.huawei.crm.basetype.ens;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for RebateInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="RebateInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="purchaseSeq" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="rebateId" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="priceItemId" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="totalPeriods" type="{http://crm.huawei.com/basetype/}PeriodCount"/>
 *         &lt;element name="firstAmount" type="{http://crm.huawei.com/basetype/}Amount"/>
 *         &lt;element name="middleAmount" type="{http://crm.huawei.com/basetype/}Amount"/>
 *         &lt;element name="lastAmount" type="{http://crm.huawei.com/basetype/}Amount"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RebateInfo", propOrder = {})
public class RebateInfo {
	protected Object purchaseSeq;
	@XmlElement(required = true)
	protected Object rebateId;
	@XmlElement(required = true)
	protected Object priceItemId;
	@XmlElement(required = true)
	protected BigInteger totalPeriods;
	protected long firstAmount;
	protected long middleAmount;
	protected long lastAmount;

	/**
	 * Gets the value of the purchaseSeq property.
	 * 
	 * @return possible object is {@link Object }
	 * 
	 */
	public Object getPurchaseSeq() {
		return purchaseSeq;
	}

	/**
	 * Sets the value of the purchaseSeq property.
	 * 
	 * @param value
	 *            allowed object is {@link Object }
	 * 
	 */
	public void setPurchaseSeq(Object value) {
		this.purchaseSeq = value;
	}

	/**
	 * Gets the value of the rebateId property.
	 * 
	 * @return possible object is {@link Object }
	 * 
	 */
	public Object getRebateId() {
		return rebateId;
	}

	/**
	 * Sets the value of the rebateId property.
	 * 
	 * @param value
	 *            allowed object is {@link Object }
	 * 
	 */
	public void setRebateId(Object value) {
		this.rebateId = value;
	}

	/**
	 * Gets the value of the priceItemId property.
	 * 
	 * @return possible object is {@link Object }
	 * 
	 */
	public Object getPriceItemId() {
		return priceItemId;
	}

	/**
	 * Sets the value of the priceItemId property.
	 * 
	 * @param value
	 *            allowed object is {@link Object }
	 * 
	 */
	public void setPriceItemId(Object value) {
		this.priceItemId = value;
	}

	/**
	 * Gets the value of the totalPeriods property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getTotalPeriods() {
		return totalPeriods;
	}

	/**
	 * Sets the value of the totalPeriods property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setTotalPeriods(BigInteger value) {
		this.totalPeriods = value;
	}

	/**
	 * Gets the value of the firstAmount property.
	 * 
	 */
	public long getFirstAmount() {
		return firstAmount;
	}

	/**
	 * Sets the value of the firstAmount property.
	 * 
	 */
	public void setFirstAmount(long value) {
		this.firstAmount = value;
	}

	/**
	 * Gets the value of the middleAmount property.
	 * 
	 */
	public long getMiddleAmount() {
		return middleAmount;
	}

	/**
	 * Sets the value of the middleAmount property.
	 * 
	 */
	public void setMiddleAmount(long value) {
		this.middleAmount = value;
	}

	/**
	 * Gets the value of the lastAmount property.
	 * 
	 */
	public long getLastAmount() {
		return lastAmount;
	}

	/**
	 * Sets the value of the lastAmount property.
	 * 
	 */
	public void setLastAmount(long value) {
		this.lastAmount = value;
	}
}
