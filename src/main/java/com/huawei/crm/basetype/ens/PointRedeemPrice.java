package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PointRedeemPrice complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PointRedeemPrice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PriceID" type="{http://crm.huawei.com/basetype/}PointPriceID"/>
 *         &lt;element name="PointAmount" type="{http://crm.huawei.com/basetype/}PointAmount"/>
 *         &lt;element name="RedeemAmount" type="{http://crm.huawei.com/basetype/}PointRedeemAmount"/>
 *         &lt;element name="RedeemAmountUnit" type="{http://crm.huawei.com/basetype/}PointRedeemAmountUnit"/>
 *         &lt;element name="MaxPointAmount" type="{http://crm.huawei.com/basetype/}PointAmount" minOccurs="0"/>
 *         &lt;element name="MinPointAmount" type="{http://crm.huawei.com/basetype/}PointAmount" minOccurs="0"/>
 *         &lt;element name="MultiFlag" type="{http://crm.huawei.com/basetype/}PointRedeemMultiFlag" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpireDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PointRedeemPrice", propOrder = { "priceID", "pointAmount", "redeemAmount", "redeemAmountUnit",
		"maxPointAmount", "minPointAmount", "multiFlag", "effectiveDate", "expireDate" })
public class PointRedeemPrice {
	@XmlElement(name = "PriceID", required = true)
	protected String priceID;
	@XmlElement(name = "PointAmount")
	protected float pointAmount;
	@XmlElement(name = "RedeemAmount")
	protected float redeemAmount;
	@XmlElement(name = "RedeemAmountUnit", required = true)
	protected String redeemAmountUnit;
	@XmlElement(name = "MaxPointAmount")
	protected Float maxPointAmount;
	@XmlElement(name = "MinPointAmount")
	protected Float minPointAmount;
	@XmlElement(name = "MultiFlag")
	protected Long multiFlag;
	@XmlElement(name = "EffectiveDate")
	protected String effectiveDate;
	@XmlElement(name = "ExpireDate")
	protected String expireDate;

	/**
	 * Gets the value of the priceID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPriceID() {
		return priceID;
	}

	/**
	 * Sets the value of the priceID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPriceID(String value) {
		this.priceID = value;
	}

	/**
	 * Gets the value of the pointAmount property.
	 * 
	 */
	public float getPointAmount() {
		return pointAmount;
	}

	/**
	 * Sets the value of the pointAmount property.
	 * 
	 */
	public void setPointAmount(float value) {
		this.pointAmount = value;
	}

	/**
	 * Gets the value of the redeemAmount property.
	 * 
	 */
	public float getRedeemAmount() {
		return redeemAmount;
	}

	/**
	 * Sets the value of the redeemAmount property.
	 * 
	 */
	public void setRedeemAmount(float value) {
		this.redeemAmount = value;
	}

	/**
	 * Gets the value of the redeemAmountUnit property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRedeemAmountUnit() {
		return redeemAmountUnit;
	}

	/**
	 * Sets the value of the redeemAmountUnit property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRedeemAmountUnit(String value) {
		this.redeemAmountUnit = value;
	}

	/**
	 * Gets the value of the maxPointAmount property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getMaxPointAmount() {
		return maxPointAmount;
	}

	/**
	 * Sets the value of the maxPointAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Float }
	 * 
	 */
	public void setMaxPointAmount(Float value) {
		this.maxPointAmount = value;
	}

	/**
	 * Gets the value of the minPointAmount property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getMinPointAmount() {
		return minPointAmount;
	}

	/**
	 * Sets the value of the minPointAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Float }
	 * 
	 */
	public void setMinPointAmount(Float value) {
		this.minPointAmount = value;
	}

	/**
	 * Gets the value of the multiFlag property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getMultiFlag() {
		return multiFlag;
	}

	/**
	 * Sets the value of the multiFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setMultiFlag(Long value) {
		this.multiFlag = value;
	}

	/**
	 * Gets the value of the effectiveDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Sets the value of the effectiveDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveDate(String value) {
		this.effectiveDate = value;
	}

	/**
	 * Gets the value of the expireDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * Sets the value of the expireDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpireDate(String value) {
		this.expireDate = value;
	}
}
