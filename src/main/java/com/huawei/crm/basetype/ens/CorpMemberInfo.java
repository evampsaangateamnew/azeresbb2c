package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CorpMemberInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CorpMemberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}ActionType"/>
 *         &lt;element name="MemberSubId" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="MemberIden" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="MemberRole" type="{http://crm.huawei.com/basetype/}RoleType" minOccurs="0"/>
 *         &lt;element name="MemberPosition" type="{http://crm.huawei.com/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="MemberCode" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="MemberPassword" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorpMemberInfo", propOrder = {})
public class CorpMemberInfo {
	@XmlElement(name = "ActionType", required = true)
	protected String actionType;
	@XmlElement(name = "MemberSubId")
	protected String memberSubId;
	@XmlElement(name = "MemberIden")
	protected String memberIden;
	@XmlElement(name = "MemberRole")
	protected String memberRole;
	@XmlElement(name = "MemberPosition")
	protected String memberPosition;
	@XmlElement(name = "MemberCode")
	protected String memberCode;
	@XmlElement(name = "MemberPassword")
	protected String memberPassword;

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the memberSubId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberSubId() {
		return memberSubId;
	}

	/**
	 * Sets the value of the memberSubId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberSubId(String value) {
		this.memberSubId = value;
	}

	/**
	 * Gets the value of the memberIden property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberIden() {
		return memberIden;
	}

	/**
	 * Sets the value of the memberIden property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberIden(String value) {
		this.memberIden = value;
	}

	/**
	 * Gets the value of the memberRole property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberRole() {
		return memberRole;
	}

	/**
	 * Sets the value of the memberRole property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberRole(String value) {
		this.memberRole = value;
	}

	/**
	 * Gets the value of the memberPosition property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberPosition() {
		return memberPosition;
	}

	/**
	 * Sets the value of the memberPosition property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberPosition(String value) {
		this.memberPosition = value;
	}

	/**
	 * Gets the value of the memberCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberCode() {
		return memberCode;
	}

	/**
	 * Sets the value of the memberCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberCode(String value) {
		this.memberCode = value;
	}

	/**
	 * Gets the value of the memberPassword property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMemberPassword() {
		return memberPassword;
	}

	/**
	 * Sets the value of the memberPassword property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMemberPassword(String value) {
		this.memberPassword = value;
	}
}
