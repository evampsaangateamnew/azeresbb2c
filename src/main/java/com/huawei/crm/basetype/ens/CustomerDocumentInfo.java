package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * T:inf_cust_document
 * 
 * <p>
 * Java class for CustomerDocumentInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerDocumentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}ActionType"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="DocumentId" type="{http://crm.huawei.com/basetype/}DocumentId" minOccurs="0"/>
 *         &lt;element name="OsmsId" type="{http://crm.huawei.com/basetype/}OsmsId" minOccurs="0"/>
 *         &lt;element name="DocumentType" type="{http://crm.huawei.com/basetype/}DocumentType" minOccurs="0"/>
 *         &lt;element name="FileType" type="{http://crm.huawei.com/basetype/}FileType" minOccurs="0"/>
 *         &lt;element name="FileName" type="{http://crm.huawei.com/basetype/}FileName" minOccurs="0"/>
 *         &lt;element name="FilePath" type="{http://crm.huawei.com/basetype/}FilePath" minOccurs="0"/>
 *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}DocumentStatus" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/basetype/}DocumentRemark" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDocumentInfo", propOrder = {})
public class CustomerDocumentInfo {
	@XmlElement(name = "ActionType", required = true)
	protected String actionType;
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "DocumentId")
	protected Long documentId;
	@XmlElement(name = "OsmsId")
	protected String osmsId;
	@XmlElement(name = "DocumentType")
	protected String documentType;
	@XmlElement(name = "FileType")
	protected String fileType;
	@XmlElement(name = "FileName")
	protected String fileName;
	@XmlElement(name = "FilePath")
	protected String filePath;
	@XmlElement(name = "Content")
	protected String content;
	@XmlElement(name = "Status")
	protected String status;
	@XmlElement(name = "Remark")
	protected String remark;

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the documentId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getDocumentId() {
		return documentId;
	}

	/**
	 * Sets the value of the documentId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setDocumentId(Long value) {
		this.documentId = value;
	}

	/**
	 * Gets the value of the osmsId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOsmsId() {
		return osmsId;
	}

	/**
	 * Sets the value of the osmsId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOsmsId(String value) {
		this.osmsId = value;
	}

	/**
	 * Gets the value of the documentType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the value of the documentType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDocumentType(String value) {
		this.documentType = value;
	}

	/**
	 * Gets the value of the fileType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * Sets the value of the fileType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFileType(String value) {
		this.fileType = value;
	}

	/**
	 * Gets the value of the fileName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the value of the fileName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFileName(String value) {
		this.fileName = value;
	}

	/**
	 * Gets the value of the filePath property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * Sets the value of the filePath property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFilePath(String value) {
		this.filePath = value;
	}

	/**
	 * Gets the value of the content property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the value of the content property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContent(String value) {
		this.content = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the remark property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the value of the remark property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRemark(String value) {
		this.remark = value;
	}
}
