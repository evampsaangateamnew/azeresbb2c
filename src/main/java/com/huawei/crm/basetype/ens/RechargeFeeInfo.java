package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for RechargeFeeInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="RechargeFeeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="LogId" type="{http://crm.huawei.com/basetype/}LogId" minOccurs="0"/>
 *         &lt;element name="ReChargeType" type="{http://crm.huawei.com/basetype/}ReChargeType" minOccurs="0"/>
 *         &lt;element name="VoucherPIN" type="{http://crm.huawei.com/basetype/}VoucherPIN" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://crm.huawei.com/basetype/}Currency" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="PaymentMethod" type="{http://crm.huawei.com/basetype/}PaymentMethod" minOccurs="0"/>
 *         &lt;element name="AccountType" type="{http://crm.huawei.com/basetype/}AccountType" minOccurs="0"/>
 *         &lt;element name="MerchantId" type="{http://crm.huawei.com/basetype/}MerchantId" minOccurs="0"/>
 *         &lt;element name="FutureDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RechargeFeeInfo", propOrder = {})
public class RechargeFeeInfo {
	@XmlElement(name = "LogId")
	protected String logId;
	@XmlElement(name = "ReChargeType")
	protected String reChargeType;
	@XmlElement(name = "VoucherPIN")
	protected String voucherPIN;
	@XmlElement(name = "Currency")
	protected String currency;
	@XmlElement(name = "Amount")
	protected Long amount;
	@XmlElement(name = "PaymentMethod")
	protected String paymentMethod;
	@XmlElement(name = "AccountType")
	protected String accountType;
	@XmlElement(name = "MerchantId")
	protected String merchantId;
	@XmlElement(name = "FutureDate")
	protected String futureDate;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the logId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLogId() {
		return logId;
	}

	/**
	 * Sets the value of the logId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLogId(String value) {
		this.logId = value;
	}

	/**
	 * Gets the value of the reChargeType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReChargeType() {
		return reChargeType;
	}

	/**
	 * Sets the value of the reChargeType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReChargeType(String value) {
		this.reChargeType = value;
	}

	/**
	 * Gets the value of the voucherPIN property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVoucherPIN() {
		return voucherPIN;
	}

	/**
	 * Sets the value of the voucherPIN property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVoucherPIN(String value) {
		this.voucherPIN = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAmount(Long value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the paymentMethod property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * Sets the value of the paymentMethod property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPaymentMethod(String value) {
		this.paymentMethod = value;
	}

	/**
	 * Gets the value of the accountType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * Sets the value of the accountType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccountType(String value) {
		this.accountType = value;
	}

	/**
	 * Gets the value of the merchantId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * Sets the value of the merchantId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMerchantId(String value) {
		this.merchantId = value;
	}

	/**
	 * Gets the value of the futureDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFutureDate() {
		return futureDate;
	}

	/**
	 * Sets the value of the futureDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFutureDate(String value) {
		this.futureDate = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
