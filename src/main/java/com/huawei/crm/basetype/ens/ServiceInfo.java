package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ServiceInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ServiceId" type="{http://crm.huawei.com/basetype/}ServiceId" minOccurs="0"/>
 *         &lt;element name="ServiceName" type="{http://crm.huawei.com/basetype/}ServiceName" minOccurs="0"/>
 *         &lt;element name="ServiceType" type="{http://crm.huawei.com/basetype/}ServiceType" minOccurs="0"/>
 *         &lt;element name="ServiceNetworkType" type="{http://crm.huawei.com/basetype/}ServiceNetworkType" minOccurs="0"/>
 *         &lt;element name="ServiceCategory" type="{http://crm.huawei.com/basetype/}ServiceCategory" minOccurs="0"/>
 *         &lt;element name="IsMainService" type="{http://crm.huawei.com/basetype/}IsMainService" minOccurs="0"/>
 *         &lt;element name="ServiceStatus" type="{http://crm.huawei.com/basetype/}ServiceStatus" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceInfo", propOrder = {})
public class ServiceInfo {
	@XmlElement(name = "ServiceId")
	protected String serviceId;
	@XmlElement(name = "ServiceName")
	protected String serviceName;
	@XmlElement(name = "ServiceType")
	protected String serviceType;
	@XmlElement(name = "ServiceNetworkType")
	protected String serviceNetworkType;
	@XmlElement(name = "ServiceCategory")
	protected String serviceCategory;
	@XmlElement(name = "IsMainService")
	protected String isMainService;
	@XmlElement(name = "ServiceStatus")
	protected String serviceStatus;
	@XmlElement(name = "ExtParamList")
	protected ExtParameterList extParamList;

	/**
	 * Gets the value of the serviceId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceId() {
		return serviceId;
	}

	/**
	 * Sets the value of the serviceId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceId(String value) {
		this.serviceId = value;
	}

	/**
	 * Gets the value of the serviceName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * Sets the value of the serviceName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceName(String value) {
		this.serviceName = value;
	}

	/**
	 * Gets the value of the serviceType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceType() {
		return serviceType;
	}

	/**
	 * Sets the value of the serviceType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceType(String value) {
		this.serviceType = value;
	}

	/**
	 * Gets the value of the serviceNetworkType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNetworkType() {
		return serviceNetworkType;
	}

	/**
	 * Sets the value of the serviceNetworkType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNetworkType(String value) {
		this.serviceNetworkType = value;
	}

	/**
	 * Gets the value of the serviceCategory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceCategory() {
		return serviceCategory;
	}

	/**
	 * Sets the value of the serviceCategory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceCategory(String value) {
		this.serviceCategory = value;
	}

	/**
	 * Gets the value of the isMainService property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIsMainService() {
		return isMainService;
	}

	/**
	 * Sets the value of the isMainService property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIsMainService(String value) {
		this.isMainService = value;
	}

	/**
	 * Gets the value of the serviceStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceStatus() {
		return serviceStatus;
	}

	/**
	 * Sets the value of the serviceStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceStatus(String value) {
		this.serviceStatus = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link ExtParameterList }
	 * 
	 */
	public ExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link ExtParameterList }
	 * 
	 */
	public void setExtParamList(ExtParameterList value) {
		this.extParamList = value;
	}
}
