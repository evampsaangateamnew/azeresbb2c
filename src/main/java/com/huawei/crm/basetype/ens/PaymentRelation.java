package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PaymentRelation complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentRelation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}ActionType" minOccurs="0"/>
 *         &lt;element name="PayRelationKey" type="{http://crm.huawei.com/basetype/}PayRelationKey" minOccurs="0"/>
 *         &lt;element name="ServiceType" type="{http://crm.huawei.com/basetype/}PaymentServiceType" minOccurs="0"/>
 *         &lt;element name="TimeSchemaId" type="{http://crm.huawei.com/basetype/}TimeSchemaId" minOccurs="0"/>
 *         &lt;element name="LimitType" type="{http://crm.huawei.com/basetype/}PayLimitType" minOccurs="0"/>
 *         &lt;element name="LimitValueType" type="{http://crm.huawei.com/basetype/}LimitValueType" minOccurs="0"/>
 *         &lt;element name="LimitMeasureUnit" type="{http://crm.huawei.com/basetype/}LimitMeasureUnit" minOccurs="0"/>
 *         &lt;element name="LimitValue" type="{http://crm.huawei.com/basetype/}LimitValue" minOccurs="0"/>
 *         &lt;element name="EffDate" type="{http://crm.huawei.com/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="ExpDate" type="{http://crm.huawei.com/basetype/}DateString" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentRelation", propOrder = {})
public class PaymentRelation {
	@XmlElement(name = "ActionType")
	protected String actionType;
	@XmlElement(name = "PayRelationKey")
	protected String payRelationKey;
	@XmlElement(name = "ServiceType")
	protected String serviceType;
	@XmlElement(name = "TimeSchemaId")
	protected String timeSchemaId;
	@XmlElement(name = "LimitType")
	protected String limitType;
	@XmlElement(name = "LimitValueType")
	protected String limitValueType;
	@XmlElement(name = "LimitMeasureUnit")
	protected String limitMeasureUnit;
	@XmlElement(name = "LimitValue")
	protected Long limitValue;
	@XmlElement(name = "EffDate")
	protected String effDate;
	@XmlElement(name = "ExpDate")
	protected String expDate;

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the payRelationKey property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPayRelationKey() {
		return payRelationKey;
	}

	/**
	 * Sets the value of the payRelationKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPayRelationKey(String value) {
		this.payRelationKey = value;
	}

	/**
	 * Gets the value of the serviceType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceType() {
		return serviceType;
	}

	/**
	 * Sets the value of the serviceType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceType(String value) {
		this.serviceType = value;
	}

	/**
	 * Gets the value of the timeSchemaId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTimeSchemaId() {
		return timeSchemaId;
	}

	/**
	 * Sets the value of the timeSchemaId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTimeSchemaId(String value) {
		this.timeSchemaId = value;
	}

	/**
	 * Gets the value of the limitType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitType() {
		return limitType;
	}

	/**
	 * Sets the value of the limitType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitType(String value) {
		this.limitType = value;
	}

	/**
	 * Gets the value of the limitValueType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitValueType() {
		return limitValueType;
	}

	/**
	 * Sets the value of the limitValueType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitValueType(String value) {
		this.limitValueType = value;
	}

	/**
	 * Gets the value of the limitMeasureUnit property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitMeasureUnit() {
		return limitMeasureUnit;
	}

	/**
	 * Sets the value of the limitMeasureUnit property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitMeasureUnit(String value) {
		this.limitMeasureUnit = value;
	}

	/**
	 * Gets the value of the limitValue property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getLimitValue() {
		return limitValue;
	}

	/**
	 * Sets the value of the limitValue property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setLimitValue(Long value) {
		this.limitValue = value;
	}

	/**
	 * Gets the value of the effDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffDate() {
		return effDate;
	}

	/**
	 * Sets the value of the effDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffDate(String value) {
		this.effDate = value;
	}

	/**
	 * Gets the value of the expDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpDate() {
		return expDate;
	}

	/**
	 * Sets the value of the expDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpDate(String value) {
		this.expDate = value;
	}
}
