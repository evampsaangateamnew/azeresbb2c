package com.huawei.crm.basetype.ens;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PromotionInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PromotionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PromotionId" type="{http://crm.huawei.com/basetype/}PromotionId"/>
 *         &lt;element name="PromotionCode" type="{http://crm.huawei.com/basetype/}PromotionCode" minOccurs="0"/>
 *         &lt;element name="MgmNum" type="{http://crm.huawei.com/basetype/}MgmNum" minOccurs="0"/>
 *         &lt;element name="RewardItemIdList" type="{http://crm.huawei.com/basetype/}RewardItemIdList" minOccurs="0"/>
 *         &lt;element name="RewardItemList" type="{http://crm.huawei.com/basetype/}RewardItemInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PromotionInfo", propOrder = { "promotionId", "promotionCode", "mgmNum", "rewardItemIdList",
		"rewardItemList" })
public class PromotionInfo {
	@XmlElement(name = "PromotionId", required = true)
	protected String promotionId;
	@XmlElement(name = "PromotionCode")
	protected String promotionCode;
	@XmlElement(name = "MgmNum")
	protected String mgmNum;
	@XmlElement(name = "RewardItemIdList")
	protected RewardItemIdList rewardItemIdList;
	@XmlElement(name = "RewardItemList")
	protected List<RewardItemInfo> rewardItemList;

	/**
	 * Gets the value of the promotionId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPromotionId() {
		return promotionId;
	}

	/**
	 * Sets the value of the promotionId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPromotionId(String value) {
		this.promotionId = value;
	}

	/**
	 * Gets the value of the promotionCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPromotionCode() {
		return promotionCode;
	}

	/**
	 * Sets the value of the promotionCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPromotionCode(String value) {
		this.promotionCode = value;
	}

	/**
	 * Gets the value of the mgmNum property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMgmNum() {
		return mgmNum;
	}

	/**
	 * Sets the value of the mgmNum property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMgmNum(String value) {
		this.mgmNum = value;
	}

	/**
	 * Gets the value of the rewardItemIdList property.
	 * 
	 * @return possible object is {@link RewardItemIdList }
	 * 
	 */
	public RewardItemIdList getRewardItemIdList() {
		return rewardItemIdList;
	}

	/**
	 * Sets the value of the rewardItemIdList property.
	 * 
	 * @param value
	 *            allowed object is {@link RewardItemIdList }
	 * 
	 */
	public void setRewardItemIdList(RewardItemIdList value) {
		this.rewardItemIdList = value;
	}

	/**
	 * Gets the value of the rewardItemList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the rewardItemList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getRewardItemList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link RewardItemInfo }
	 * 
	 * 
	 */
	public List<RewardItemInfo> getRewardItemList() {
		if (rewardItemList == null) {
			rewardItemList = new ArrayList<RewardItemInfo>();
		}
		return this.rewardItemList;
	}
}
