package com.huawei.crm.basetype.ens;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SubRelationList complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SubRelationList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityRelationInfo" type="{http://crm.huawei.com/basetype/}EntityRelationInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubRelationList", propOrder = { "entityRelationInfo" })
public class SubRelationList {
	@XmlElement(name = "EntityRelationInfo")
	protected List<EntityRelationInfo> entityRelationInfo;

	/**
	 * Gets the value of the entityRelationInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the entityRelationInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getEntityRelationInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link EntityRelationInfo }
	 * 
	 * 
	 */
	public List<EntityRelationInfo> getEntityRelationInfo() {
		if (entityRelationInfo == null) {
			entityRelationInfo = new ArrayList<EntityRelationInfo>();
		}
		return this.entityRelationInfo;
	}
}
