package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TradingValue complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TradingValue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TradingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="CustomerRelaId" type="{http://crm.huawei.com/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="AcctRelaId" type="{http://crm.huawei.com/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://crm.huawei.com/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="SubRelaId" type="{http://crm.huawei.com/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TradingDate" type="{http://crm.huawei.com/basetype/}DateString" minOccurs="0"/>
 *         &lt;element name="ChangeAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="RelaTradingID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActualChange" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TradingDetail" type="{http://crm.huawei.com/basetype/}TradingDetailList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradingValue", propOrder = {})
public class TradingValue {
	@XmlElement(name = "TradingType")
	protected String tradingType;
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "CustomerRelaId")
	protected String customerRelaId;
	@XmlElement(name = "AcctRelaId")
	protected String acctRelaId;
	@XmlElement(name = "AccountId")
	protected Long accountId;
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "SubRelaId")
	protected String subRelaId;
	@XmlElement(name = "SubscriberId")
	protected Long subscriberId;
	@XmlElement(name = "Amount")
	protected Long amount;
	@XmlElement(name = "TradingDate")
	protected String tradingDate;
	@XmlElement(name = "ChangeAmount")
	protected Long changeAmount;
	@XmlElement(name = "RelaTradingID")
	protected String relaTradingID;
	@XmlElement(name = "ActualChange")
	protected Long actualChange;
	@XmlElement(name = "TradingDetail")
	protected TradingDetailList tradingDetail;

	/**
	 * Gets the value of the tradingType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTradingType() {
		return tradingType;
	}

	/**
	 * Sets the value of the tradingType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTradingType(String value) {
		this.tradingType = value;
	}

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the customerRelaId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerRelaId() {
		return customerRelaId;
	}

	/**
	 * Sets the value of the customerRelaId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerRelaId(String value) {
		this.customerRelaId = value;
	}

	/**
	 * Gets the value of the acctRelaId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctRelaId() {
		return acctRelaId;
	}

	/**
	 * Sets the value of the acctRelaId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctRelaId(String value) {
		this.acctRelaId = value;
	}

	/**
	 * Gets the value of the accountId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * Sets the value of the accountId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAccountId(Long value) {
		this.accountId = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the subRelaId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubRelaId() {
		return subRelaId;
	}

	/**
	 * Sets the value of the subRelaId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubRelaId(String value) {
		this.subRelaId = value;
	}

	/**
	 * Gets the value of the subscriberId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Sets the value of the subscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setSubscriberId(Long value) {
		this.subscriberId = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAmount(Long value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the tradingDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTradingDate() {
		return tradingDate;
	}

	/**
	 * Sets the value of the tradingDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTradingDate(String value) {
		this.tradingDate = value;
	}

	/**
	 * Gets the value of the changeAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getChangeAmount() {
		return changeAmount;
	}

	/**
	 * Sets the value of the changeAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setChangeAmount(Long value) {
		this.changeAmount = value;
	}

	/**
	 * Gets the value of the relaTradingID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRelaTradingID() {
		return relaTradingID;
	}

	/**
	 * Sets the value of the relaTradingID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRelaTradingID(String value) {
		this.relaTradingID = value;
	}

	/**
	 * Gets the value of the actualChange property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getActualChange() {
		return actualChange;
	}

	/**
	 * Sets the value of the actualChange property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setActualChange(Long value) {
		this.actualChange = value;
	}

	/**
	 * Gets the value of the tradingDetail property.
	 * 
	 * @return possible object is {@link TradingDetailList }
	 * 
	 */
	public TradingDetailList getTradingDetail() {
		return tradingDetail;
	}

	/**
	 * Sets the value of the tradingDetail property.
	 * 
	 * @param value
	 *            allowed object is {@link TradingDetailList }
	 * 
	 */
	public void setTradingDetail(TradingDetailList value) {
		this.tradingDetail = value;
	}
}
