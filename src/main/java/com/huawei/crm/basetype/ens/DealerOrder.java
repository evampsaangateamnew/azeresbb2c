package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DealerOrder complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="DealerOrder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="DealerId" type="{http://crm.huawei.com/basetype/}DealerIdentifier" minOccurs="0"/>
 *         &lt;element name="OrderNo" type="{http://crm.huawei.com/basetype/}OrderNo" minOccurs="0"/>
 *         &lt;element name="CreateDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="TotalAmount" type="{http://crm.huawei.com/basetype/}TotalAmount" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}DealerOrderStatus" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerOrder", propOrder = {})
public class DealerOrder {
	@XmlElement(name = "DealerId")
	protected String dealerId;
	@XmlElement(name = "OrderNo")
	protected String orderNo;
	@XmlElement(name = "CreateDate")
	protected String createDate;
	@XmlElement(name = "TotalAmount")
	protected Long totalAmount;
	@XmlElement(name = "Status")
	protected String status;

	/**
	 * Gets the value of the dealerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDealerId() {
		return dealerId;
	}

	/**
	 * Sets the value of the dealerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDealerId(String value) {
		this.dealerId = value;
	}

	/**
	 * Gets the value of the orderNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderNo() {
		return orderNo;
	}

	/**
	 * Sets the value of the orderNo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrderNo(String value) {
		this.orderNo = value;
	}

	/**
	 * Gets the value of the createDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the value of the createDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreateDate(String value) {
		this.createDate = value;
	}

	/**
	 * Gets the value of the totalAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getTotalAmount() {
		return totalAmount;
	}

	/**
	 * Sets the value of the totalAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setTotalAmount(Long value) {
		this.totalAmount = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}
}
