package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GroupMemberInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GroupMemberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupId" type="{http://crm.huawei.com/basetype/}GroupId" minOccurs="0"/>
 *         &lt;element name="GroupRelaId" type="{http://crm.huawei.com/basetype/}RelationId" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="AddMemberInfo" type="{http://crm.huawei.com/basetype/}MemberSubscriberInfo" minOccurs="0"/>
 *           &lt;element name="DelMemberInfo" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="MemberSubsId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *                     &lt;element name="MemberServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="ModiMemberInfo" type="{http://crm.huawei.com/basetype/}MemberSubscriberInfo" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMemberInfo", propOrder = { "groupId", "groupRelaId", "addMemberInfo", "delMemberInfo",
		"modiMemberInfo" })
public class GroupMemberInfo {
	@XmlElement(name = "GroupId")
	protected Long groupId;
	@XmlElement(name = "GroupRelaId")
	protected String groupRelaId;
	@XmlElement(name = "AddMemberInfo")
	protected MemberSubscriberInfo addMemberInfo;
	@XmlElement(name = "DelMemberInfo")
	protected GroupMemberInfo.DelMemberInfo delMemberInfo;
	@XmlElement(name = "ModiMemberInfo")
	protected MemberSubscriberInfo modiMemberInfo;

	/**
	 * Gets the value of the groupId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * Sets the value of the groupId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setGroupId(Long value) {
		this.groupId = value;
	}

	/**
	 * Gets the value of the groupRelaId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGroupRelaId() {
		return groupRelaId;
	}

	/**
	 * Sets the value of the groupRelaId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGroupRelaId(String value) {
		this.groupRelaId = value;
	}

	/**
	 * Gets the value of the addMemberInfo property.
	 * 
	 * @return possible object is {@link MemberSubscriberInfo }
	 * 
	 */
	public MemberSubscriberInfo getAddMemberInfo() {
		return addMemberInfo;
	}

	/**
	 * Sets the value of the addMemberInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link MemberSubscriberInfo }
	 * 
	 */
	public void setAddMemberInfo(MemberSubscriberInfo value) {
		this.addMemberInfo = value;
	}

	/**
	 * Gets the value of the delMemberInfo property.
	 * 
	 * @return possible object is {@link GroupMemberInfo.DelMemberInfo }
	 * 
	 */
	public GroupMemberInfo.DelMemberInfo getDelMemberInfo() {
		return delMemberInfo;
	}

	/**
	 * Sets the value of the delMemberInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link GroupMemberInfo.DelMemberInfo }
	 * 
	 */
	public void setDelMemberInfo(GroupMemberInfo.DelMemberInfo value) {
		this.delMemberInfo = value;
	}

	/**
	 * Gets the value of the modiMemberInfo property.
	 * 
	 * @return possible object is {@link MemberSubscriberInfo }
	 * 
	 */
	public MemberSubscriberInfo getModiMemberInfo() {
		return modiMemberInfo;
	}

	/**
	 * Sets the value of the modiMemberInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link MemberSubscriberInfo }
	 * 
	 */
	public void setModiMemberInfo(MemberSubscriberInfo value) {
		this.modiMemberInfo = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="MemberSubsId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
	 *         &lt;element name="MemberServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "memberSubsId", "memberServiceNumber" })
	public static class DelMemberInfo {
		@XmlElement(name = "MemberSubsId")
		protected Long memberSubsId;
		@XmlElement(name = "MemberServiceNumber")
		protected String memberServiceNumber;

		/**
		 * Gets the value of the memberSubsId property.
		 * 
		 * @return possible object is {@link Long }
		 * 
		 */
		public Long getMemberSubsId() {
			return memberSubsId;
		}

		/**
		 * Sets the value of the memberSubsId property.
		 * 
		 * @param value
		 *            allowed object is {@link Long }
		 * 
		 */
		public void setMemberSubsId(Long value) {
			this.memberSubsId = value;
		}

		/**
		 * Gets the value of the memberServiceNumber property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMemberServiceNumber() {
			return memberServiceNumber;
		}

		/**
		 * Sets the value of the memberServiceNumber property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setMemberServiceNumber(String value) {
			this.memberServiceNumber = value;
		}
	}
}
