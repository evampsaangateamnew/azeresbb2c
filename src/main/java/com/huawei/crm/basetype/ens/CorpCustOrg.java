package com.huawei.crm.basetype.ens;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CorpCustOrg complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CorpCustOrg">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}RelationId" minOccurs="0"/>
 *         &lt;element name="OrgId" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="OrgName" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="OrgSName" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="OrgCode" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="OrgType" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="ParentOrgId" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://crm.huawei.com/basetype/}string" minOccurs="0"/>
 *         &lt;element name="CorpCustMember" type="{http://crm.huawei.com/basetype/}CorpMemberList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorpCustOrg", propOrder = {})
public class CorpCustOrg {
	@XmlElement(name = "ActionType")
	protected String actionType;
	@XmlElement(name = "OrgId")
	protected String orgId;
	@XmlElement(name = "OrgName")
	protected String orgName;
	@XmlElement(name = "OrgSName")
	protected String orgSName;
	@XmlElement(name = "OrgCode")
	protected String orgCode;
	@XmlElement(name = "OrgType")
	protected String orgType;
	@XmlElement(name = "ParentOrgId")
	protected String parentOrgId;
	@XmlElement(name = "Status")
	protected Long status;
	@XmlElement(name = "Description")
	protected String description;
	@XmlElement(name = "CorpCustMember")
	protected CorpMemberList corpCustMember;

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the orgId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrgId() {
		return orgId;
	}

	/**
	 * Sets the value of the orgId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrgId(String value) {
		this.orgId = value;
	}

	/**
	 * Gets the value of the orgName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * Sets the value of the orgName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrgName(String value) {
		this.orgName = value;
	}

	/**
	 * Gets the value of the orgSName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrgSName() {
		return orgSName;
	}

	/**
	 * Sets the value of the orgSName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrgSName(String value) {
		this.orgSName = value;
	}

	/**
	 * Gets the value of the orgCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrgCode() {
		return orgCode;
	}

	/**
	 * Sets the value of the orgCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrgCode(String value) {
		this.orgCode = value;
	}

	/**
	 * Gets the value of the orgType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrgType() {
		return orgType;
	}

	/**
	 * Sets the value of the orgType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOrgType(String value) {
		this.orgType = value;
	}

	/**
	 * Gets the value of the parentOrgId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getParentOrgId() {
		return parentOrgId;
	}

	/**
	 * Sets the value of the parentOrgId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setParentOrgId(String value) {
		this.parentOrgId = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setStatus(Long value) {
		this.status = value;
	}

	/**
	 * Gets the value of the description property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the description property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDescription(String value) {
		this.description = value;
	}

	/**
	 * Gets the value of the corpCustMember property.
	 * 
	 * @return possible object is {@link CorpMemberList }
	 * 
	 */
	public CorpMemberList getCorpCustMember() {
		return corpCustMember;
	}

	/**
	 * Sets the value of the corpCustMember property.
	 * 
	 * @param value
	 *            allowed object is {@link CorpMemberList }
	 * 
	 */
	public void setCorpCustMember(CorpMemberList value) {
		this.corpCustMember = value;
	}
}
