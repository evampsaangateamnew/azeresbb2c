package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OfferingExtParameterInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingExtParameterInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ParamName" type="{http://crm.huawei.com/basetype/}ParamName" minOccurs="0"/>
 *         &lt;element name="ParamCode" type="{http://crm.huawei.com/basetype/}ParamCode" minOccurs="0"/>
 *         &lt;element name="ParamValue" type="{http://crm.huawei.com/basetype/}ParamValue" minOccurs="0"/>
 *         &lt;element name="ParamOldValue" type="{http://crm.huawei.com/basetype/}ParamValue" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}OfferingExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingExtParameterInfo", propOrder = {})
public class OfferingExtParameterInfo {
	@XmlElement(name = "ParamName")
	protected String paramName;
	@XmlElement(name = "ParamCode")
	protected String paramCode;
	@XmlElement(name = "ParamValue")
	protected String paramValue;
	@XmlElement(name = "ParamOldValue")
	protected String paramOldValue;
	@XmlElement(name = "ExtParamList")
	protected OfferingExtParameterList extParamList;

	/**
	 * Gets the value of the paramName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getParamName() {
		return paramName;
	}

	/**
	 * Sets the value of the paramName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setParamName(String value) {
		this.paramName = value;
	}

	/**
	 * Gets the value of the paramCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getParamCode() {
		return paramCode;
	}

	/**
	 * Sets the value of the paramCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setParamCode(String value) {
		this.paramCode = value;
	}

	/**
	 * Gets the value of the paramValue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getParamValue() {
		return paramValue;
	}

	/**
	 * Sets the value of the paramValue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setParamValue(String value) {
		this.paramValue = value;
	}

	/**
	 * Gets the value of the paramOldValue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getParamOldValue() {
		return paramOldValue;
	}

	/**
	 * Sets the value of the paramOldValue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setParamOldValue(String value) {
		this.paramOldValue = value;
	}

	/**
	 * Gets the value of the extParamList property.
	 * 
	 * @return possible object is {@link OfferingExtParameterList }
	 * 
	 */
	public OfferingExtParameterList getExtParamList() {
		return extParamList;
	}

	/**
	 * Sets the value of the extParamList property.
	 * 
	 * @param value
	 *            allowed object is {@link OfferingExtParameterList }
	 * 
	 */
	public void setExtParamList(OfferingExtParameterList value) {
		this.extParamList = value;
	}
}
