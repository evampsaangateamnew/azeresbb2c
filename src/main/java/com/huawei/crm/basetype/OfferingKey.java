package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OfferingKey complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="OfferingCode" type="{http://crm.huawei.com/basetype/}OfferingCode" minOccurs="0"/>
 *         &lt;element name="PurchaseSeq" type="{http://crm.huawei.com/basetype/}PurchaseSeq" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingKey", propOrder = { "offeringId", "offeringCode", "purchaseSeq" })
public class OfferingKey {
	@XmlElement(name = "OfferingId")
	protected String offeringId;
	@XmlElement(name = "OfferingCode")
	protected String offeringCode;
	@XmlElement(name = "PurchaseSeq")
	protected Long purchaseSeq;

	/**
	 * Gets the value of the offeringId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * Sets the value of the offeringId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingId(String value) {
		this.offeringId = value;
	}

	/**
	 * Gets the value of the offeringCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingCode() {
		return offeringCode;
	}

	/**
	 * Sets the value of the offeringCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingCode(String value) {
		this.offeringCode = value;
	}

	/**
	 * Gets the value of the purchaseSeq property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getPurchaseSeq() {
		return purchaseSeq;
	}

	/**
	 * Sets the value of the purchaseSeq property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setPurchaseSeq(Long value) {
		this.purchaseSeq = value;
	}
}
