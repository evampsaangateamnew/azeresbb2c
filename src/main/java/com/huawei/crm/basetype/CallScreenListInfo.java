package com.huawei.crm.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CallScreenListInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CallScreenListInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CallScreenListName" type="{http://crm.huawei.com/basetype/}CallScreenListName" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://crm.huawei.com/basetype/}Category" minOccurs="0"/>
 *         &lt;element name="CallScreenType" type="{http://crm.huawei.com/basetype/}CallScreenType" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}CallScreenActionType" minOccurs="0"/>
 *         &lt;element name="ScreenRoamFlag" type="{http://crm.huawei.com/basetype/}ScreenRoamFlag" minOccurs="0"/>
 *         &lt;element name="ScreenRoutingMethod" type="{http://crm.huawei.com/basetype/}ScreenRoutingMethod" minOccurs="0"/>
 *         &lt;element name="ScreenRoutingNumber" type="{http://crm.huawei.com/basetype/}ScreenRoutingNumber" minOccurs="0"/>
 *         &lt;element name="CallScreenTimeSegInfo" type="{http://crm.huawei.com/basetype/}CallScreenTimeSegInfo" minOccurs="0"/>
 *         &lt;element name="CallScreenNoInfo" type="{http://crm.huawei.com/basetype/}CallScreenNoInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallScreenListInfo", propOrder = { "callScreenListName", "category", "callScreenType", "actionType",
		"screenRoamFlag", "screenRoutingMethod", "screenRoutingNumber", "callScreenTimeSegInfo", "callScreenNoInfo" })
public class CallScreenListInfo {
	@XmlElement(name = "CallScreenListName")
	protected String callScreenListName;
	@XmlElement(name = "Category")
	protected String category;
	@XmlElement(name = "CallScreenType")
	protected String callScreenType;
	@XmlElement(name = "ActionType")
	protected String actionType;
	@XmlElement(name = "ScreenRoamFlag")
	protected String screenRoamFlag;
	@XmlElement(name = "ScreenRoutingMethod")
	protected String screenRoutingMethod;
	@XmlElement(name = "ScreenRoutingNumber")
	protected String screenRoutingNumber;
	@XmlElement(name = "CallScreenTimeSegInfo")
	protected CallScreenTimeSegInfo callScreenTimeSegInfo;
	@XmlElement(name = "CallScreenNoInfo")
	protected List<CallScreenNoInfo> callScreenNoInfo;

	/**
	 * Gets the value of the callScreenListName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCallScreenListName() {
		return callScreenListName;
	}

	/**
	 * Sets the value of the callScreenListName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCallScreenListName(String value) {
		this.callScreenListName = value;
	}

	/**
	 * Gets the value of the category property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Sets the value of the category property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCategory(String value) {
		this.category = value;
	}

	/**
	 * Gets the value of the callScreenType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCallScreenType() {
		return callScreenType;
	}

	/**
	 * Sets the value of the callScreenType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCallScreenType(String value) {
		this.callScreenType = value;
	}

	/**
	 * Gets the value of the actionType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Sets the value of the actionType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActionType(String value) {
		this.actionType = value;
	}

	/**
	 * Gets the value of the screenRoamFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScreenRoamFlag() {
		return screenRoamFlag;
	}

	/**
	 * Sets the value of the screenRoamFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScreenRoamFlag(String value) {
		this.screenRoamFlag = value;
	}

	/**
	 * Gets the value of the screenRoutingMethod property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScreenRoutingMethod() {
		return screenRoutingMethod;
	}

	/**
	 * Sets the value of the screenRoutingMethod property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScreenRoutingMethod(String value) {
		this.screenRoutingMethod = value;
	}

	/**
	 * Gets the value of the screenRoutingNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScreenRoutingNumber() {
		return screenRoutingNumber;
	}

	/**
	 * Sets the value of the screenRoutingNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScreenRoutingNumber(String value) {
		this.screenRoutingNumber = value;
	}

	/**
	 * Gets the value of the callScreenTimeSegInfo property.
	 * 
	 * @return possible object is {@link CallScreenTimeSegInfo }
	 * 
	 */
	public CallScreenTimeSegInfo getCallScreenTimeSegInfo() {
		return callScreenTimeSegInfo;
	}

	/**
	 * Sets the value of the callScreenTimeSegInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link CallScreenTimeSegInfo }
	 * 
	 */
	public void setCallScreenTimeSegInfo(CallScreenTimeSegInfo value) {
		this.callScreenTimeSegInfo = value;
	}

	/**
	 * Gets the value of the callScreenNoInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the callScreenNoInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCallScreenNoInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CallScreenNoInfo }
	 * 
	 * 
	 */
	public List<CallScreenNoInfo> getCallScreenNoInfo() {
		if (callScreenNoInfo == null) {
			callScreenNoInfo = new ArrayList<CallScreenNoInfo>();
		}
		return this.callScreenNoInfo;
	}
}
