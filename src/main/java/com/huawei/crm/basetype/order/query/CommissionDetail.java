
package com.huawei.crm.basetype.order.query;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommissionDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommissionDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DealerId" type="{http://crm.huawei.com/basetype/}DealerIdentifier" minOccurs="0"/>
 *         &lt;element name="DealerName" type="{http://crm.huawei.com/basetype/}DealerName" minOccurs="0"/>
 *         &lt;element name="Cycle" type="{http://crm.huawei.com/basetype/}Cycle" minOccurs="0"/>
 *         &lt;element name="CycleType" type="{http://crm.huawei.com/basetype/}CycleType" minOccurs="0"/>
 *         &lt;element name="ItemId" type="{http://crm.huawei.com/basetype/}ItemIdentifier" minOccurs="0"/>
 *         &lt;element name="ItemName" type="{http://crm.huawei.com/basetype/}ItemName" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}CommissionStatus" minOccurs="0"/>
 *         &lt;element name="Msisdn" type="{http://crm.huawei.com/basetype/}Msisdn" minOccurs="0"/>
 *         &lt;element name="OfferId" type="{http://crm.huawei.com/basetype/}RelatedOfferId" minOccurs="0"/>
 *         &lt;element name="CreateType" type="{http://crm.huawei.com/basetype/}CreateType" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://crm.huawei.com/basetype/}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommissionDetail", propOrder = {
    "dealerId",
    "dealerName",
    "cycle",
    "cycleType",
    "itemId",
    "itemName",
    "status",
    "msisdn",
    "offerId",
    "createType",
    "amount"
})
public class CommissionDetail {

    @XmlElement(name = "DealerId")
    protected String dealerId;
    @XmlElement(name = "DealerName")
    protected String dealerName;
    @XmlElement(name = "Cycle")
    protected String cycle;
    @XmlElement(name = "CycleType")
    protected String cycleType;
    @XmlElement(name = "ItemId")
    protected String itemId;
    @XmlElement(name = "ItemName")
    protected String itemName;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "Msisdn")
    protected String msisdn;
    @XmlElement(name = "OfferId")
    protected String offerId;
    @XmlElement(name = "CreateType")
    protected String createType;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;

    /**
     * Gets the value of the dealerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * Sets the value of the dealerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerId(String value) {
        this.dealerId = value;
    }

    /**
     * Gets the value of the dealerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerName() {
        return dealerName;
    }

    /**
     * Sets the value of the dealerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerName(String value) {
        this.dealerName = value;
    }

    /**
     * Gets the value of the cycle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * Sets the value of the cycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycle(String value) {
        this.cycle = value;
    }

    /**
     * Gets the value of the cycleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycleType() {
        return cycleType;
    }

    /**
     * Sets the value of the cycleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycleType(String value) {
        this.cycleType = value;
    }

    /**
     * Gets the value of the itemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * Sets the value of the itemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemId(String value) {
        this.itemId = value;
    }

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the offerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     * Sets the value of the offerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferId(String value) {
        this.offerId = value;
    }

    /**
     * Gets the value of the createType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateType() {
        return createType;
    }

    /**
     * Sets the value of the createType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateType(String value) {
        this.createType = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

}
