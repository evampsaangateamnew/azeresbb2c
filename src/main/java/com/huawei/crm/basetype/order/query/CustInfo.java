
package com.huawei.crm.basetype.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustBaseInfo" type="{http://crm.huawei.com/basetype/}CustBaseInfo" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="IndividualInfo" type="{http://crm.huawei.com/basetype/}IndividualInfo" minOccurs="0"/>
 *           &lt;element name="CorpInfo" type="{http://crm.huawei.com/basetype/}CorpInfo" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustInfo", propOrder = {
    "custBaseInfo",
    "individualInfo",
    "corpInfo"
})
public class CustInfo {

    @XmlElement(name = "CustBaseInfo")
    protected CustBaseInfo custBaseInfo;
    @XmlElement(name = "IndividualInfo")
    protected IndividualInfo individualInfo;
    @XmlElement(name = "CorpInfo")
    protected CorpInfo corpInfo;

    /**
     * Gets the value of the custBaseInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CustBaseInfo }
     *     
     */
    public CustBaseInfo getCustBaseInfo() {
        return custBaseInfo;
    }

    /**
     * Sets the value of the custBaseInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustBaseInfo }
     *     
     */
    public void setCustBaseInfo(CustBaseInfo value) {
        this.custBaseInfo = value;
    }

    /**
     * Gets the value of the individualInfo property.
     * 
     * @return
     *     possible object is
     *     {@link IndividualInfo }
     *     
     */
    public IndividualInfo getIndividualInfo() {
        return individualInfo;
    }

    /**
     * Sets the value of the individualInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndividualInfo }
     *     
     */
    public void setIndividualInfo(IndividualInfo value) {
        this.individualInfo = value;
    }

    /**
     * Gets the value of the corpInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CorpInfo }
     *     
     */
    public CorpInfo getCorpInfo() {
        return corpInfo;
    }

    /**
     * Sets the value of the corpInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorpInfo }
     *     
     */
    public void setCorpInfo(CorpInfo value) {
        this.corpInfo = value;
    }

}
