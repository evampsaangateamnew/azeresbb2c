
package com.huawei.crm.basetype.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ProductId" type="{http://crm.huawei.com/basetype/}ProductId" minOccurs="0"/>
 *         &lt;element name="SelectFlag" type="{http://crm.huawei.com/basetype/}SelectFlag" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}OfferingExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductInfo", propOrder = {

})
public class ProductInfo {

    @XmlElement(name = "ProductId")
    protected String productId;
    @XmlElement(name = "SelectFlag")
    protected String selectFlag;
    @XmlElement(name = "ExtParamList")
    protected OfferingExtParameterList extParamList;

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the selectFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelectFlag() {
        return selectFlag;
    }

    /**
     * Sets the value of the selectFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelectFlag(String value) {
        this.selectFlag = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingExtParameterList }
     *     
     */
    public OfferingExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingExtParameterList }
     *     
     */
    public void setExtParamList(OfferingExtParameterList value) {
        this.extParamList = value;
    }

}
