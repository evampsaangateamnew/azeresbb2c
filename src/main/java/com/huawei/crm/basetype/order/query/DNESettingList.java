
package com.huawei.crm.basetype.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DNESettingList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DNESettingList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DNESettingInfo" type="{http://crm.huawei.com/basetype/}DNESettingInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DNESettingList", propOrder = {
    "dneSettingInfo"
})
public class DNESettingList {

    @XmlElement(name = "DNESettingInfo", required = true)
    protected List<DNESettingInfo> dneSettingInfo;

    /**
     * Gets the value of the dneSettingInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dneSettingInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDNESettingInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DNESettingInfo }
     * 
     * 
     */
    public List<DNESettingInfo> getDNESettingInfo() {
        if (dneSettingInfo == null) {
            dneSettingInfo = new ArrayList<DNESettingInfo>();
        }
        return this.dneSettingInfo;
    }

}
