
package com.huawei.crm.basetype.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProlongationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProlongationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DurationID" type="{http://crm.huawei.com/basetype/}DurationId" minOccurs="0"/>
 *         &lt;element name="DurationUnit" type="{http://crm.huawei.com/basetype/}DurationUnit" minOccurs="0"/>
 *         &lt;element name="DurationValue" type="{http://crm.huawei.com/basetype/}DurationValue" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProlongationInfo", propOrder = {
    "durationID",
    "durationUnit",
    "durationValue"
})
public class ProlongationInfo {

    @XmlElement(name = "DurationID")
    protected String durationID;
    @XmlElement(name = "DurationUnit")
    @XmlSchemaType(name = "string")
    protected DurationUnit durationUnit;
    @XmlElement(name = "DurationValue")
    protected String durationValue;

    /**
     * Gets the value of the durationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationID() {
        return durationID;
    }

    /**
     * Sets the value of the durationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationID(String value) {
        this.durationID = value;
    }

    /**
     * Gets the value of the durationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link DurationUnit }
     *     
     */
    public DurationUnit getDurationUnit() {
        return durationUnit;
    }

    /**
     * Sets the value of the durationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link DurationUnit }
     *     
     */
    public void setDurationUnit(DurationUnit value) {
        this.durationUnit = value;
    }

    /**
     * Gets the value of the durationValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationValue() {
        return durationValue;
    }

    /**
     * Sets the value of the durationValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationValue(String value) {
        this.durationValue = value;
    }

}
