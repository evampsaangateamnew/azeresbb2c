
package com.huawei.crm.basetype.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BusinessFee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessFee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Charge" type="{http://crm.huawei.com/basetype/}ChargeInfo" minOccurs="0"/>
 *         &lt;element name="ReCharge" type="{http://crm.huawei.com/basetype/}RechargeInfo" minOccurs="0"/>
 *         &lt;element name="Trading" type="{http://crm.huawei.com/basetype/}TradingValue" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessFee", propOrder = {
    "charge",
    "reCharge",
    "trading"
})
public class BusinessFee {

    @XmlElement(name = "Charge")
    protected ChargeInfo charge;
    @XmlElement(name = "ReCharge")
    protected RechargeInfo reCharge;
    @XmlElement(name = "Trading")
    protected TradingValue trading;

    /**
     * Gets the value of the charge property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeInfo }
     *     
     */
    public ChargeInfo getCharge() {
        return charge;
    }

    /**
     * Sets the value of the charge property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeInfo }
     *     
     */
    public void setCharge(ChargeInfo value) {
        this.charge = value;
    }

    /**
     * Gets the value of the reCharge property.
     * 
     * @return
     *     possible object is
     *     {@link RechargeInfo }
     *     
     */
    public RechargeInfo getReCharge() {
        return reCharge;
    }

    /**
     * Sets the value of the reCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link RechargeInfo }
     *     
     */
    public void setReCharge(RechargeInfo value) {
        this.reCharge = value;
    }

    /**
     * Gets the value of the trading property.
     * 
     * @return
     *     possible object is
     *     {@link TradingValue }
     *     
     */
    public TradingValue getTrading() {
        return trading;
    }

    /**
     * Sets the value of the trading property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradingValue }
     *     
     */
    public void setTrading(TradingValue value) {
        this.trading = value;
    }

}
