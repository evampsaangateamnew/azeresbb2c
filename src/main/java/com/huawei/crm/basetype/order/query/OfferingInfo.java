
package com.huawei.crm.basetype.order.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OfferingInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}ActionType" minOccurs="0"/>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingKey"/>
 *         &lt;element name="OfferingRefId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ParentOfferingRefId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BundleOfferId" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="IsThirdPartyOfferingId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OldOfferingId" type="{http://crm.huawei.com/basetype/}OfferingKey" minOccurs="0"/>
 *         &lt;element name="OwnerType" type="{http://crm.huawei.com/basetype/}OwnerType" minOccurs="0"/>
 *         &lt;element name="EffectMode" type="{http://crm.huawei.com/basetype/}EffectMode" minOccurs="0"/>
 *         &lt;element name="EffectValue" type="{http://crm.huawei.com/basetype/}EffectValue" minOccurs="0"/>
 *         &lt;element name="ExpireMode" type="{http://crm.huawei.com/basetype/}ExpireMode" minOccurs="0"/>
 *         &lt;element name="ActiveMode" type="{http://crm.huawei.com/basetype/}ActiveMode" minOccurs="0"/>
 *         &lt;element name="ParentOfferingId" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}OfferingStatus" minOccurs="0"/>
 *         &lt;element name="EffectiveTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpiredTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="LatestActiveDate" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ProductList" type="{http://crm.huawei.com/basetype/}ExtProductList" minOccurs="0"/>
 *         &lt;element name="ContractInfo" type="{http://crm.huawei.com/basetype/}ContractInfo" minOccurs="0"/>
 *         &lt;element name="ProlongIdInfo" type="{http://crm.huawei.com/basetype/}ProlongIdInfo" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}OfferingExtParameterList" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://crm.huawei.com/basetype/}Quantity" minOccurs="0"/>
 *         &lt;element name="ResourceList" type="{http://crm.huawei.com/basetype/}ResourceList" minOccurs="0"/>
 *         &lt;element name="ReserveSeq" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SubscribeReason" type="{http://crm.huawei.com/basetype/}SubscribeReason" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/basetype/}RemarkForOffer" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingInfo", propOrder = {

})
public class OfferingInfo {

    @XmlElement(name = "ActionType")
    protected String actionType;
    @XmlElement(name = "OfferingId", required = true)
    protected OfferingKey offeringId;
    @XmlElement(name = "OfferingRefId")
    protected String offeringRefId;
    @XmlElement(name = "ParentOfferingRefId")
    protected String parentOfferingRefId;
    @XmlElement(name = "BundleOfferId")
    protected String bundleOfferId;
    @XmlElement(name = "IsThirdPartyOfferingId")
    protected String isThirdPartyOfferingId;
    @XmlElement(name = "OldOfferingId")
    protected OfferingKey oldOfferingId;
    @XmlElement(name = "OwnerType")
    protected String ownerType;
    @XmlElement(name = "EffectMode")
    protected String effectMode;
    @XmlElement(name = "EffectValue")
    protected String effectValue;
    @XmlElement(name = "ExpireMode")
    protected String expireMode;
    @XmlElement(name = "ActiveMode")
    protected String activeMode;
    @XmlElement(name = "ParentOfferingId")
    protected String parentOfferingId;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "EffectiveTime")
    protected String effectiveTime;
    @XmlElement(name = "ExpiredTime")
    protected String expiredTime;
    @XmlElement(name = "LatestActiveDate")
    protected String latestActiveDate;
    @XmlElement(name = "ProductList")
    protected ExtProductList productList;
    @XmlElement(name = "ContractInfo")
    protected ContractInfo contractInfo;
    @XmlElement(name = "ProlongIdInfo")
    protected ProlongIdInfo prolongIdInfo;
    @XmlElement(name = "ExtParamList")
    protected OfferingExtParameterList extParamList;
    @XmlElement(name = "Quantity")
    protected String quantity;
    @XmlElement(name = "ResourceList")
    protected ResourceList resourceList;
    @XmlElement(name = "ReserveSeq")
    protected String reserveSeq;
    @XmlElement(name = "SubscribeReason")
    protected String subscribeReason;
    @XmlElement(name = "Remark")
    protected String remark;

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the offeringId property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getOfferingId() {
        return offeringId;
    }

    /**
     * Sets the value of the offeringId property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setOfferingId(OfferingKey value) {
        this.offeringId = value;
    }

    /**
     * Gets the value of the offeringRefId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingRefId() {
        return offeringRefId;
    }

    /**
     * Sets the value of the offeringRefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingRefId(String value) {
        this.offeringRefId = value;
    }

    /**
     * Gets the value of the parentOfferingRefId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentOfferingRefId() {
        return parentOfferingRefId;
    }

    /**
     * Sets the value of the parentOfferingRefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentOfferingRefId(String value) {
        this.parentOfferingRefId = value;
    }

    /**
     * Gets the value of the bundleOfferId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBundleOfferId() {
        return bundleOfferId;
    }

    /**
     * Sets the value of the bundleOfferId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBundleOfferId(String value) {
        this.bundleOfferId = value;
    }

    /**
     * Gets the value of the isThirdPartyOfferingId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsThirdPartyOfferingId() {
        return isThirdPartyOfferingId;
    }

    /**
     * Sets the value of the isThirdPartyOfferingId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsThirdPartyOfferingId(String value) {
        this.isThirdPartyOfferingId = value;
    }

    /**
     * Gets the value of the oldOfferingId property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getOldOfferingId() {
        return oldOfferingId;
    }

    /**
     * Sets the value of the oldOfferingId property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setOldOfferingId(OfferingKey value) {
        this.oldOfferingId = value;
    }

    /**
     * Gets the value of the ownerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerType() {
        return ownerType;
    }

    /**
     * Sets the value of the ownerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerType(String value) {
        this.ownerType = value;
    }

    /**
     * Gets the value of the effectMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectMode() {
        return effectMode;
    }

    /**
     * Sets the value of the effectMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectMode(String value) {
        this.effectMode = value;
    }

    /**
     * Gets the value of the effectValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectValue() {
        return effectValue;
    }

    /**
     * Sets the value of the effectValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectValue(String value) {
        this.effectValue = value;
    }

    /**
     * Gets the value of the expireMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpireMode() {
        return expireMode;
    }

    /**
     * Sets the value of the expireMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpireMode(String value) {
        this.expireMode = value;
    }

    /**
     * Gets the value of the activeMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveMode() {
        return activeMode;
    }

    /**
     * Sets the value of the activeMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveMode(String value) {
        this.activeMode = value;
    }

    /**
     * Gets the value of the parentOfferingId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentOfferingId() {
        return parentOfferingId;
    }

    /**
     * Sets the value of the parentOfferingId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentOfferingId(String value) {
        this.parentOfferingId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the effectiveTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveTime() {
        return effectiveTime;
    }

    /**
     * Sets the value of the effectiveTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveTime(String value) {
        this.effectiveTime = value;
    }

    /**
     * Gets the value of the expiredTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiredTime() {
        return expiredTime;
    }

    /**
     * Sets the value of the expiredTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiredTime(String value) {
        this.expiredTime = value;
    }

    /**
     * Gets the value of the latestActiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatestActiveDate() {
        return latestActiveDate;
    }

    /**
     * Sets the value of the latestActiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatestActiveDate(String value) {
        this.latestActiveDate = value;
    }

    /**
     * Gets the value of the productList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtProductList }
     *     
     */
    public ExtProductList getProductList() {
        return productList;
    }

    /**
     * Sets the value of the productList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtProductList }
     *     
     */
    public void setProductList(ExtProductList value) {
        this.productList = value;
    }

    /**
     * Gets the value of the contractInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ContractInfo }
     *     
     */
    public ContractInfo getContractInfo() {
        return contractInfo;
    }

    /**
     * Sets the value of the contractInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractInfo }
     *     
     */
    public void setContractInfo(ContractInfo value) {
        this.contractInfo = value;
    }

    /**
     * Gets the value of the prolongIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ProlongIdInfo }
     *     
     */
    public ProlongIdInfo getProlongIdInfo() {
        return prolongIdInfo;
    }

    /**
     * Sets the value of the prolongIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProlongIdInfo }
     *     
     */
    public void setProlongIdInfo(ProlongIdInfo value) {
        this.prolongIdInfo = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingExtParameterList }
     *     
     */
    public OfferingExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingExtParameterList }
     *     
     */
    public void setExtParamList(OfferingExtParameterList value) {
        this.extParamList = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity(String value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the resourceList property.
     * 
     * @return
     *     possible object is
     *     {@link ResourceList }
     *     
     */
    public ResourceList getResourceList() {
        return resourceList;
    }

    /**
     * Sets the value of the resourceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResourceList }
     *     
     */
    public void setResourceList(ResourceList value) {
        this.resourceList = value;
    }

    /**
     * Gets the value of the reserveSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReserveSeq() {
        return reserveSeq;
    }

    /**
     * Sets the value of the reserveSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReserveSeq(String value) {
        this.reserveSeq = value;
    }

    /**
     * Gets the value of the subscribeReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscribeReason() {
        return subscribeReason;
    }

    /**
     * Sets the value of the subscribeReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscribeReason(String value) {
        this.subscribeReason = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

}
