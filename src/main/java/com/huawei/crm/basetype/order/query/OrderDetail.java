
package com.huawei.crm.basetype.order.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExternalOrderId" type="{http://crm.huawei.com/basetype/}ExternalOrderId"/>
 *         &lt;element name="InternalOrderId" type="{http://crm.huawei.com/basetype/}InternalOrderId"/>
 *         &lt;element name="OrderType" type="{http://crm.huawei.com/basetype/}OrderType"/>
 *         &lt;element name="OrderSubType" type="{http://crm.huawei.com/basetype/}OrderType" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://crm.huawei.com/basetype/}ServiceNumber" minOccurs="0"/>
 *         &lt;element name="OrderCreateTime" type="{http://crm.huawei.com/basetype/}Time"/>
 *         &lt;element name="LastStatusTime" type="{http://crm.huawei.com/basetype/}Time"/>
 *         &lt;element name="OrderStatus" type="{http://crm.huawei.com/basetype/}OrderStatus"/>
 *         &lt;element name="StatusDesc" type="{http://crm.huawei.com/basetype/}Remark"/>
 *         &lt;element name="CurrentWorkOrderList" type="{http://crm.huawei.com/basetype/}WorkOrderDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderDetail", propOrder = {
    "externalOrderId",
    "internalOrderId",
    "orderType",
    "orderSubType",
    "subscriberId",
    "serviceNumber",
    "orderCreateTime",
    "lastStatusTime",
    "orderStatus",
    "statusDesc",
    "currentWorkOrderList"
})
public class OrderDetail {

    @XmlElement(name = "ExternalOrderId", required = true)
    protected String externalOrderId;
    @XmlElement(name = "InternalOrderId", required = true)
    protected String internalOrderId;
    @XmlElement(name = "OrderType", required = true)
    protected String orderType;
    @XmlElement(name = "OrderSubType")
    protected String orderSubType;
    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "ServiceNumber")
    protected String serviceNumber;
    @XmlElement(name = "OrderCreateTime", required = true)
    protected String orderCreateTime;
    @XmlElement(name = "LastStatusTime", required = true)
    protected String lastStatusTime;
    @XmlElement(name = "OrderStatus", required = true)
    protected String orderStatus;
    @XmlElement(name = "StatusDesc", required = true)
    protected String statusDesc;
    @XmlElement(name = "CurrentWorkOrderList")
    protected List<WorkOrderDetail> currentWorkOrderList;

    /**
     * Gets the value of the externalOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalOrderId() {
        return externalOrderId;
    }

    /**
     * Sets the value of the externalOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalOrderId(String value) {
        this.externalOrderId = value;
    }

    /**
     * Gets the value of the internalOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalOrderId() {
        return internalOrderId;
    }

    /**
     * Sets the value of the internalOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalOrderId(String value) {
        this.internalOrderId = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderType(String value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the orderSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderSubType() {
        return orderSubType;
    }

    /**
     * Sets the value of the orderSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderSubType(String value) {
        this.orderSubType = value;
    }

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the serviceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceNumber(String value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the orderCreateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    /**
     * Sets the value of the orderCreateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderCreateTime(String value) {
        this.orderCreateTime = value;
    }

    /**
     * Gets the value of the lastStatusTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastStatusTime() {
        return lastStatusTime;
    }

    /**
     * Sets the value of the lastStatusTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastStatusTime(String value) {
        this.lastStatusTime = value;
    }

    /**
     * Gets the value of the orderStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * Sets the value of the orderStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderStatus(String value) {
        this.orderStatus = value;
    }

    /**
     * Gets the value of the statusDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * Sets the value of the statusDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusDesc(String value) {
        this.statusDesc = value;
    }

    /**
     * Gets the value of the currentWorkOrderList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the currentWorkOrderList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCurrentWorkOrderList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WorkOrderDetail }
     * 
     * 
     */
    public List<WorkOrderDetail> getCurrentWorkOrderList() {
        if (currentWorkOrderList == null) {
            currentWorkOrderList = new ArrayList<WorkOrderDetail>();
        }
        return this.currentWorkOrderList;
    }

}
