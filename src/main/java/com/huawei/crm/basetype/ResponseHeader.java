package com.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ResponseHeader complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="RequestHeader" type="{http://crm.huawei.com/basetype/}RequestHeader"/>
 *         &lt;element name="RetCode" type="{http://crm.huawei.com/basetype/}RetCode"/>
 *         &lt;element name="RetMsg" type="{http://crm.huawei.com/basetype/}RetMsg"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseHeader", propOrder = {})
public class ResponseHeader {
	@XmlElement(name = "RequestHeader", required = true)
	protected RequestHeader requestHeader;
	@XmlElement(name = "RetCode", required = true)
	protected String retCode;
	@XmlElement(name = "RetMsg", required = true)
	protected String retMsg;

	/**
	 * Gets the value of the requestHeader property.
	 * 
	 * @return possible object is {@link RequestHeader }
	 * 
	 */
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	/**
	 * Sets the value of the requestHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RequestHeader }
	 * 
	 */
	public void setRequestHeader(RequestHeader value) {
		this.requestHeader = value;
	}

	/**
	 * Gets the value of the retCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRetCode() {
		return retCode;
	}

	/**
	 * Sets the value of the retCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRetCode(String value) {
		this.retCode = value;
	}

	/**
	 * Gets the value of the retMsg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRetMsg() {
		return retMsg;
	}

	/**
	 * Sets the value of the retMsg property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRetMsg(String value) {
		this.retMsg = value;
	}
}
