
package com.huawei.bss.soaif._interface.orderservice.createorder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.bss.soaif._interface.common.createorder.ActiveMode;
import com.huawei.bss.soaif._interface.common.createorder.Address;
import com.huawei.bss.soaif._interface.common.createorder.BillMedium;
import com.huawei.bss.soaif._interface.common.createorder.BusinessFeeInfo;
import com.huawei.bss.soaif._interface.common.createorder.Certificate;
import com.huawei.bss.soaif._interface.common.createorder.CreditLimit;
import com.huawei.bss.soaif._interface.common.createorder.CustomerDocumentInfo;
import com.huawei.bss.soaif._interface.common.createorder.EffectiveMode;
import com.huawei.bss.soaif._interface.common.createorder.GroupSubscriberInfo;
import com.huawei.bss.soaif._interface.common.createorder.ManageCreditLimit;
import com.huawei.bss.soaif._interface.common.createorder.MemberSubscriberInfo;
import com.huawei.bss.soaif._interface.common.createorder.Name;
import com.huawei.bss.soaif._interface.common.createorder.OfferingInstance;
import com.huawei.bss.soaif._interface.common.createorder.PaymentChannel;
import com.huawei.bss.soaif._interface.common.createorder.PaymentPlanInfo;
import com.huawei.bss.soaif._interface.common.createorder.ReqHeader;
import com.huawei.bss.soaif._interface.common.createorder.ServiceNum;
import com.huawei.bss.soaif._interface.common.createorder.SimpleProperty;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="Order">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="OrderType" type="{http://www.huawei.com/bss/soaif/interface/common/}OrderType"/>
 *                     &lt;element name="ExternalOrderId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalOrderId" minOccurs="0"/>
 *                     &lt;element name="CustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="Customer">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;element name="ExistingCustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId" minOccurs="0"/>
 *                     &lt;element name="NewCustomer" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel"/>
 *                               &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title"/>
 *                               &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name"/>
 *                               &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality"/>
 *                               &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate"/>
 *                               &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date"/>
 *                               &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
 *                               &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *                               &lt;element name="ExternalCustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalCustomerId" minOccurs="0"/>
 *                               &lt;element name="CustomerType" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerType" minOccurs="0"/>
 *                               &lt;element name="CustomerRelaId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerRelaId" minOccurs="0"/>
 *                               &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                               &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                               &lt;element name="Gender" type="{http://www.huawei.com/bss/soaif/interface/common/}Gender"/>
 *                               &lt;element name="PlaceOfBirth" type="{http://www.huawei.com/bss/soaif/interface/common/}PlaceOfBirth" minOccurs="0"/>
 *                               &lt;element name="Occupation" type="{http://www.huawei.com/bss/soaif/interface/common/}Occupation" minOccurs="0"/>
 *                               &lt;element name="PINNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}PINNumber" minOccurs="0"/>
 *                               &lt;element name="FatherName" type="{http://www.huawei.com/bss/soaif/interface/common/}FatherName"/>
 *                               &lt;element name="ContactNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactNumber"/>
 *                               &lt;element name="SendToLegal" type="{http://www.huawei.com/bss/soaif/interface/common/}SendToLegal" minOccurs="0"/>
 *                               &lt;element name="TaxCertificateID" type="{http://www.huawei.com/bss/soaif/interface/common/}TaxCertificateID" minOccurs="0"/>
 *                               &lt;element name="Citizenship" type="{http://www.huawei.com/bss/soaif/interface/common/}Citizenship" minOccurs="0"/>
 *                               &lt;element name="CustomerDocumentList" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerDocumentInfo" maxOccurs="100" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="Account" maxOccurs="2">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;choice>
 *                       &lt;element name="ExistingAcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId" minOccurs="0"/>
 *                       &lt;element name="NewAccount" minOccurs="0">
 *                         &lt;complexType>
 *                           &lt;complexContent>
 *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                               &lt;sequence>
 *                                 &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType"/>
 *                                 &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *                                 &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name"/>
 *                                 &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType"/>
 *                                 &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language"/>
 *                                 &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
 *                                 &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="100" minOccurs="0"/>
 *                                 &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
 *                                 &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
 *                                 &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" minOccurs="0"/>
 *                                 &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="100" minOccurs="0"/>
 *                                 &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *                                 &lt;element name="ExternalAccountId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalAccountId" minOccurs="0"/>
 *                                 &lt;element name="AccountType" type="{http://www.huawei.com/bss/soaif/interface/common/}AccountType" minOccurs="0"/>
 *                                 &lt;element name="EffectiveDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *                                 &lt;element name="BillCycleEffMode" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleEffMode" minOccurs="0"/>
 *                                 &lt;element name="BillCycleEffDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *                                 &lt;element name="BillCycleId" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleId" minOccurs="0"/>
 *                               &lt;/sequence>
 *                             &lt;/restriction>
 *                           &lt;/complexContent>
 *                         &lt;/complexType>
 *                       &lt;/element>
 *                     &lt;/choice>
 *                     &lt;element name="IsDefaultAcct" minOccurs="0">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="1"/>
 *                           &lt;enumeration value="0"/>
 *                           &lt;enumeration value="1"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="Subscriber">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="ServiceNum" type="{http://www.huawei.com/bss/soaif/interface/common/}ServiceNum"/>
 *                     &lt;element name="ICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}ICCID"/>
 *                     &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                     &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                     &lt;element name="Password" type="{http://www.huawei.com/bss/soaif/interface/common/}Password" minOccurs="0"/>
 *                     &lt;element name="IMEI" type="{http://www.huawei.com/bss/soaif/interface/common/}IMEI" minOccurs="0"/>
 *                     &lt;element name="Taxation" minOccurs="0">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="1"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                     &lt;element name="IVRBlackList">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="5"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                     &lt;element name="LoyaltySegment">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="1"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                     &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *                     &lt;element name="ExternalSubscriberId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalSubscriberId" minOccurs="0"/>
 *                     &lt;element name="ActivateFlag" type="{http://www.huawei.com/bss/soaif/interface/common/}ActivateFlag" minOccurs="0"/>
 *                     &lt;element name="DealerId" type="{http://www.huawei.com/bss/soaif/interface/common/}DealerId" minOccurs="0"/>
 *                     &lt;element name="Remarks" minOccurs="0">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="512"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="PrimaryOffering">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance">
 *                   &lt;sequence>
 *                     &lt;element name="ActiveMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="SupplementaryOffering" maxOccurs="100" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance">
 *                   &lt;sequence>
 *                     &lt;element name="EffectiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}EffectiveMode"/>
 *                     &lt;element name="ExpirationDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *                     &lt;element name="ActiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}ActiveMode"/>
 *                   &lt;/sequence>
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="BusinessFee" type="{http://www.huawei.com/bss/soaif/interface/common/}BusinessFeeInfo" maxOccurs="100" minOccurs="0"/>
 *           &lt;element name="GroupSubscriber" type="{http://www.huawei.com/bss/soaif/interface/common/}GroupSubscriberInfo" minOccurs="0"/>
 *           &lt;element name="GroupMember" type="{http://www.huawei.com/bss/soaif/interface/common/}MemberSubscriberInfo" minOccurs="0"/>
 *           &lt;element name="PaymentPlanList" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentPlanInfo" minOccurs="0"/>
 *           &lt;element name="ManageCreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}ManageCreditLimit" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reqHeader",
    "order",
    "customer",
    "account",
    "subscriber",
    "primaryOffering",
    "supplementaryOffering",
    "businessFee",
    "groupSubscriber",
    "groupMember",
    "paymentPlanList",
    "manageCreditLimit"
})
@XmlRootElement(name = "CreateOrderReqMsg")
public class CreateOrderReqMsg {

    @XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
    protected ReqHeader reqHeader;
    @XmlElement(name = "Order", required = true)
    protected CreateOrderReqMsg.Order order;
    @XmlElement(name = "Customer", required = true)
    protected CreateOrderReqMsg.Customer customer;
    @XmlElement(name = "Account", required = true)
    protected List<CreateOrderReqMsg.Account> account;
    @XmlElement(name = "Subscriber", required = true)
    protected CreateOrderReqMsg.Subscriber subscriber;
    @XmlElement(name = "PrimaryOffering", required = true)
    protected CreateOrderReqMsg.PrimaryOffering primaryOffering;
    @XmlElement(name = "SupplementaryOffering")
    protected List<CreateOrderReqMsg.SupplementaryOffering> supplementaryOffering;
    @XmlElement(name = "BusinessFee")
    protected List<BusinessFeeInfo> businessFee;
    @XmlElement(name = "GroupSubscriber")
    protected GroupSubscriberInfo groupSubscriber;
    @XmlElement(name = "GroupMember")
    protected MemberSubscriberInfo groupMember;
    @XmlElement(name = "PaymentPlanList")
    protected PaymentPlanInfo paymentPlanList;
    @XmlElement(name = "ManageCreditLimit")
    protected ManageCreditLimit manageCreditLimit;

    /**
     * Gets the value of the reqHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ReqHeader }
     *     
     */
    public ReqHeader getReqHeader() {
        return reqHeader;
    }

    /**
     * Sets the value of the reqHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqHeader }
     *     
     */
    public void setReqHeader(ReqHeader value) {
        this.reqHeader = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrderReqMsg.Order }
     *     
     */
    public CreateOrderReqMsg.Order getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrderReqMsg.Order }
     *     
     */
    public void setOrder(CreateOrderReqMsg.Order value) {
        this.order = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrderReqMsg.Customer }
     *     
     */
    public CreateOrderReqMsg.Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrderReqMsg.Customer }
     *     
     */
    public void setCustomer(CreateOrderReqMsg.Customer value) {
        this.customer = value;
    }

    /**
     * Gets the value of the account property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the account property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateOrderReqMsg.Account }
     * 
     * 
     */
    public List<CreateOrderReqMsg.Account> getAccount() {
        if (account == null) {
            account = new ArrayList<CreateOrderReqMsg.Account>();
        }
        return this.account;
    }

    /**
     * Gets the value of the subscriber property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrderReqMsg.Subscriber }
     *     
     */
    public CreateOrderReqMsg.Subscriber getSubscriber() {
        return subscriber;
    }

    /**
     * Sets the value of the subscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrderReqMsg.Subscriber }
     *     
     */
    public void setSubscriber(CreateOrderReqMsg.Subscriber value) {
        this.subscriber = value;
    }

    /**
     * Gets the value of the primaryOffering property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrderReqMsg.PrimaryOffering }
     *     
     */
    public CreateOrderReqMsg.PrimaryOffering getPrimaryOffering() {
        return primaryOffering;
    }

    /**
     * Sets the value of the primaryOffering property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrderReqMsg.PrimaryOffering }
     *     
     */
    public void setPrimaryOffering(CreateOrderReqMsg.PrimaryOffering value) {
        this.primaryOffering = value;
    }

    /**
     * Gets the value of the supplementaryOffering property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryOffering property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryOffering().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateOrderReqMsg.SupplementaryOffering }
     * 
     * 
     */
    public List<CreateOrderReqMsg.SupplementaryOffering> getSupplementaryOffering() {
        if (supplementaryOffering == null) {
            supplementaryOffering = new ArrayList<CreateOrderReqMsg.SupplementaryOffering>();
        }
        return this.supplementaryOffering;
    }

    /**
     * Gets the value of the businessFee property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the businessFee property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusinessFee().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessFeeInfo }
     * 
     * 
     */
    public List<BusinessFeeInfo> getBusinessFee() {
        if (businessFee == null) {
            businessFee = new ArrayList<BusinessFeeInfo>();
        }
        return this.businessFee;
    }

    /**
     * Gets the value of the groupSubscriber property.
     * 
     * @return
     *     possible object is
     *     {@link GroupSubscriberInfo }
     *     
     */
    public GroupSubscriberInfo getGroupSubscriber() {
        return groupSubscriber;
    }

    /**
     * Sets the value of the groupSubscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupSubscriberInfo }
     *     
     */
    public void setGroupSubscriber(GroupSubscriberInfo value) {
        this.groupSubscriber = value;
    }

    /**
     * Gets the value of the groupMember property.
     * 
     * @return
     *     possible object is
     *     {@link MemberSubscriberInfo }
     *     
     */
    public MemberSubscriberInfo getGroupMember() {
        return groupMember;
    }

    /**
     * Sets the value of the groupMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link MemberSubscriberInfo }
     *     
     */
    public void setGroupMember(MemberSubscriberInfo value) {
        this.groupMember = value;
    }

    /**
     * Gets the value of the paymentPlanList property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentPlanInfo }
     *     
     */
    public PaymentPlanInfo getPaymentPlanList() {
        return paymentPlanList;
    }

    /**
     * Sets the value of the paymentPlanList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentPlanInfo }
     *     
     */
    public void setPaymentPlanList(PaymentPlanInfo value) {
        this.paymentPlanList = value;
    }

    /**
     * Gets the value of the manageCreditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link ManageCreditLimit }
     *     
     */
    public ManageCreditLimit getManageCreditLimit() {
        return manageCreditLimit;
    }

    /**
     * Sets the value of the manageCreditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link ManageCreditLimit }
     *     
     */
    public void setManageCreditLimit(ManageCreditLimit value) {
        this.manageCreditLimit = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice>
     *           &lt;element name="ExistingAcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId" minOccurs="0"/>
     *           &lt;element name="NewAccount" minOccurs="0">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType"/>
     *                     &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
     *                     &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name"/>
     *                     &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType"/>
     *                     &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language"/>
     *                     &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
     *                     &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="100" minOccurs="0"/>
     *                     &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
     *                     &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
     *                     &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" minOccurs="0"/>
     *                     &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="100" minOccurs="0"/>
     *                     &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
     *                     &lt;element name="ExternalAccountId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalAccountId" minOccurs="0"/>
     *                     &lt;element name="AccountType" type="{http://www.huawei.com/bss/soaif/interface/common/}AccountType" minOccurs="0"/>
     *                     &lt;element name="EffectiveDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
     *                     &lt;element name="BillCycleEffMode" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleEffMode" minOccurs="0"/>
     *                     &lt;element name="BillCycleEffDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
     *                     &lt;element name="BillCycleId" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleId" minOccurs="0"/>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *         &lt;/choice>
     *         &lt;element name="IsDefaultAcct" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="1"/>
     *               &lt;enumeration value="0"/>
     *               &lt;enumeration value="1"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "existingAcctId",
        "newAccount",
        "isDefaultAcct"
    })
    public static class Account {

        @XmlElement(name = "ExistingAcctId")
        protected String existingAcctId;
        @XmlElement(name = "NewAccount")
        protected CreateOrderReqMsg.Account.NewAccount newAccount;
        @XmlElement(name = "IsDefaultAcct")
        protected String isDefaultAcct;

        /**
         * Gets the value of the existingAcctId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExistingAcctId() {
            return existingAcctId;
        }

        /**
         * Sets the value of the existingAcctId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExistingAcctId(String value) {
            this.existingAcctId = value;
        }

        /**
         * Gets the value of the newAccount property.
         * 
         * @return
         *     possible object is
         *     {@link CreateOrderReqMsg.Account.NewAccount }
         *     
         */
        public CreateOrderReqMsg.Account.NewAccount getNewAccount() {
            return newAccount;
        }

        /**
         * Sets the value of the newAccount property.
         * 
         * @param value
         *     allowed object is
         *     {@link CreateOrderReqMsg.Account.NewAccount }
         *     
         */
        public void setNewAccount(CreateOrderReqMsg.Account.NewAccount value) {
            this.newAccount = value;
        }

        /**
         * Gets the value of the isDefaultAcct property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsDefaultAcct() {
            return isDefaultAcct;
        }

        /**
         * Sets the value of the isDefaultAcct property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsDefaultAcct(String value) {
            this.isDefaultAcct = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType"/>
         *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
         *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name"/>
         *         &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType"/>
         *         &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language"/>
         *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
         *         &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="100" minOccurs="0"/>
         *         &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
         *         &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
         *         &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" minOccurs="0"/>
         *         &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="100" minOccurs="0"/>
         *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
         *         &lt;element name="ExternalAccountId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalAccountId" minOccurs="0"/>
         *         &lt;element name="AccountType" type="{http://www.huawei.com/bss/soaif/interface/common/}AccountType" minOccurs="0"/>
         *         &lt;element name="EffectiveDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
         *         &lt;element name="BillCycleEffMode" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleEffMode" minOccurs="0"/>
         *         &lt;element name="BillCycleEffDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
         *         &lt;element name="BillCycleId" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleId" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "paymentType",
            "title",
            "name",
            "billCycleType",
            "billLanguage",
            "address",
            "billMedium",
            "currency",
            "initialBalance",
            "creditLimit",
            "paymentChannel",
            "additionalProperty",
            "externalAccountId",
            "accountType",
            "effectiveDate",
            "billCycleEffMode",
            "billCycleEffDate",
            "billCycleId"
        })
        public static class NewAccount {

            @XmlElement(name = "PaymentType", required = true)
            protected String paymentType;
            @XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/OrderService/", type = JAXBElement.class, required = false)
            protected JAXBElement<String> title;
            @XmlElement(name = "Name", required = true)
            protected Name name;
            @XmlElement(name = "BillCycleType", required = true)
            protected String billCycleType;
            @XmlElement(name = "BillLanguage", required = true)
            protected String billLanguage;
            @XmlElement(name = "Address")
            protected List<Address> address;
            @XmlElement(name = "BillMedium")
            protected List<BillMedium> billMedium;
            @XmlElement(name = "Currency")
            protected BigInteger currency;
            @XmlElement(name = "InitialBalance")
            protected String initialBalance;
            @XmlElement(name = "CreditLimit")
            protected CreditLimit creditLimit;
            @XmlElement(name = "PaymentChannel")
            protected List<PaymentChannel> paymentChannel;
            @XmlElement(name = "AdditionalProperty")
            protected List<SimpleProperty> additionalProperty;
            @XmlElement(name = "ExternalAccountId")
            protected String externalAccountId;
            @XmlElement(name = "AccountType")
            protected String accountType;
            @XmlElement(name = "EffectiveDate")
            protected String effectiveDate;
            @XmlElement(name = "BillCycleEffMode")
            protected String billCycleEffMode;
            @XmlElement(name = "BillCycleEffDate")
            protected String billCycleEffDate;
            @XmlElement(name = "BillCycleId")
            protected String billCycleId;

            /**
             * Gets the value of the paymentType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPaymentType() {
                return paymentType;
            }

            /**
             * Sets the value of the paymentType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPaymentType(String value) {
                this.paymentType = value;
            }

            /**
             * Gets the value of the title property.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link String }{@code >}
             *     
             */
            public JAXBElement<String> getTitle() {
                return title;
            }

            /**
             * Sets the value of the title property.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link String }{@code >}
             *     
             */
            public void setTitle(JAXBElement<String> value) {
                this.title = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link Name }
             *     
             */
            public Name getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link Name }
             *     
             */
            public void setName(Name value) {
                this.name = value;
            }

            /**
             * Gets the value of the billCycleType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleType() {
                return billCycleType;
            }

            /**
             * Sets the value of the billCycleType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleType(String value) {
                this.billCycleType = value;
            }

            /**
             * Gets the value of the billLanguage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillLanguage() {
                return billLanguage;
            }

            /**
             * Sets the value of the billLanguage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillLanguage(String value) {
                this.billLanguage = value;
            }

            /**
             * Gets the value of the address property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the address property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddress().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Address }
             * 
             * 
             */
            public List<Address> getAddress() {
                if (address == null) {
                    address = new ArrayList<Address>();
                }
                return this.address;
            }

            /**
             * Gets the value of the billMedium property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the billMedium property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getBillMedium().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link BillMedium }
             * 
             * 
             */
            public List<BillMedium> getBillMedium() {
                if (billMedium == null) {
                    billMedium = new ArrayList<BillMedium>();
                }
                return this.billMedium;
            }

            /**
             * Gets the value of the currency property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCurrency() {
                return currency;
            }

            /**
             * Sets the value of the currency property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCurrency(BigInteger value) {
                this.currency = value;
            }

            /**
             * Gets the value of the initialBalance property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInitialBalance() {
                return initialBalance;
            }

            /**
             * Sets the value of the initialBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInitialBalance(String value) {
                this.initialBalance = value;
            }

            /**
             * Gets the value of the creditLimit property.
             * 
             * @return
             *     possible object is
             *     {@link CreditLimit }
             *     
             */
            public CreditLimit getCreditLimit() {
                return creditLimit;
            }

            /**
             * Sets the value of the creditLimit property.
             * 
             * @param value
             *     allowed object is
             *     {@link CreditLimit }
             *     
             */
            public void setCreditLimit(CreditLimit value) {
                this.creditLimit = value;
            }

            /**
             * Gets the value of the paymentChannel property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the paymentChannel property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPaymentChannel().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link PaymentChannel }
             * 
             * 
             */
            public List<PaymentChannel> getPaymentChannel() {
                if (paymentChannel == null) {
                    paymentChannel = new ArrayList<PaymentChannel>();
                }
                return this.paymentChannel;
            }

            /**
             * Gets the value of the additionalProperty property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAdditionalProperty().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link SimpleProperty }
             * 
             * 
             */
            public List<SimpleProperty> getAdditionalProperty() {
                if (additionalProperty == null) {
                    additionalProperty = new ArrayList<SimpleProperty>();
                }
                return this.additionalProperty;
            }

            /**
             * Gets the value of the externalAccountId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExternalAccountId() {
                return externalAccountId;
            }

            /**
             * Sets the value of the externalAccountId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExternalAccountId(String value) {
                this.externalAccountId = value;
            }

            /**
             * Gets the value of the accountType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccountType() {
                return accountType;
            }

            /**
             * Sets the value of the accountType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccountType(String value) {
                this.accountType = value;
            }

            /**
             * Gets the value of the effectiveDate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveDate() {
                return effectiveDate;
            }

            /**
             * Sets the value of the effectiveDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveDate(String value) {
                this.effectiveDate = value;
            }

            /**
             * Gets the value of the billCycleEffMode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleEffMode() {
                return billCycleEffMode;
            }

            /**
             * Sets the value of the billCycleEffMode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleEffMode(String value) {
                this.billCycleEffMode = value;
            }

            /**
             * Gets the value of the billCycleEffDate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleEffDate() {
                return billCycleEffDate;
            }

            /**
             * Sets the value of the billCycleEffDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleEffDate(String value) {
                this.billCycleEffDate = value;
            }

            /**
             * Gets the value of the billCycleId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleId() {
                return billCycleId;
            }

            /**
             * Sets the value of the billCycleId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleId(String value) {
                this.billCycleId = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="ExistingCustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId" minOccurs="0"/>
     *         &lt;element name="NewCustomer" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel"/>
     *                   &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title"/>
     *                   &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name"/>
     *                   &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality"/>
     *                   &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate"/>
     *                   &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date"/>
     *                   &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
     *                   &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
     *                   &lt;element name="ExternalCustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalCustomerId" minOccurs="0"/>
     *                   &lt;element name="CustomerType" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerType" minOccurs="0"/>
     *                   &lt;element name="CustomerRelaId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerRelaId" minOccurs="0"/>
     *                   &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
     *                   &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
     *                   &lt;element name="Gender" type="{http://www.huawei.com/bss/soaif/interface/common/}Gender"/>
     *                   &lt;element name="PlaceOfBirth" type="{http://www.huawei.com/bss/soaif/interface/common/}PlaceOfBirth" minOccurs="0"/>
     *                   &lt;element name="Occupation" type="{http://www.huawei.com/bss/soaif/interface/common/}Occupation" minOccurs="0"/>
     *                   &lt;element name="PINNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}PINNumber" minOccurs="0"/>
     *                   &lt;element name="FatherName" type="{http://www.huawei.com/bss/soaif/interface/common/}FatherName"/>
     *                   &lt;element name="ContactNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactNumber"/>
     *                   &lt;element name="SendToLegal" type="{http://www.huawei.com/bss/soaif/interface/common/}SendToLegal" minOccurs="0"/>
     *                   &lt;element name="TaxCertificateID" type="{http://www.huawei.com/bss/soaif/interface/common/}TaxCertificateID" minOccurs="0"/>
     *                   &lt;element name="Citizenship" type="{http://www.huawei.com/bss/soaif/interface/common/}Citizenship" minOccurs="0"/>
     *                   &lt;element name="CustomerDocumentList" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerDocumentInfo" maxOccurs="100" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "existingCustId",
        "newCustomer"
    })
    public static class Customer {

        @XmlElement(name = "ExistingCustId")
        protected String existingCustId;
        @XmlElement(name = "NewCustomer")
        protected CreateOrderReqMsg.Customer.NewCustomer newCustomer;

        /**
         * Gets the value of the existingCustId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExistingCustId() {
            return existingCustId;
        }

        /**
         * Sets the value of the existingCustId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExistingCustId(String value) {
            this.existingCustId = value;
        }

        /**
         * Gets the value of the newCustomer property.
         * 
         * @return
         *     possible object is
         *     {@link CreateOrderReqMsg.Customer.NewCustomer }
         *     
         */
        public CreateOrderReqMsg.Customer.NewCustomer getNewCustomer() {
            return newCustomer;
        }

        /**
         * Sets the value of the newCustomer property.
         * 
         * @param value
         *     allowed object is
         *     {@link CreateOrderReqMsg.Customer.NewCustomer }
         *     
         */
        public void setNewCustomer(CreateOrderReqMsg.Customer.NewCustomer value) {
            this.newCustomer = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel"/>
         *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title"/>
         *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name"/>
         *         &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality"/>
         *         &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate"/>
         *         &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date"/>
         *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
         *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
         *         &lt;element name="ExternalCustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalCustomerId" minOccurs="0"/>
         *         &lt;element name="CustomerType" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerType" minOccurs="0"/>
         *         &lt;element name="CustomerRelaId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerRelaId" minOccurs="0"/>
         *         &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
         *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
         *         &lt;element name="Gender" type="{http://www.huawei.com/bss/soaif/interface/common/}Gender"/>
         *         &lt;element name="PlaceOfBirth" type="{http://www.huawei.com/bss/soaif/interface/common/}PlaceOfBirth" minOccurs="0"/>
         *         &lt;element name="Occupation" type="{http://www.huawei.com/bss/soaif/interface/common/}Occupation" minOccurs="0"/>
         *         &lt;element name="PINNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}PINNumber" minOccurs="0"/>
         *         &lt;element name="FatherName" type="{http://www.huawei.com/bss/soaif/interface/common/}FatherName"/>
         *         &lt;element name="ContactNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactNumber"/>
         *         &lt;element name="SendToLegal" type="{http://www.huawei.com/bss/soaif/interface/common/}SendToLegal" minOccurs="0"/>
         *         &lt;element name="TaxCertificateID" type="{http://www.huawei.com/bss/soaif/interface/common/}TaxCertificateID" minOccurs="0"/>
         *         &lt;element name="Citizenship" type="{http://www.huawei.com/bss/soaif/interface/common/}Citizenship" minOccurs="0"/>
         *         &lt;element name="CustomerDocumentList" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerDocumentInfo" maxOccurs="100" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "custLevel",
            "title",
            "name",
            "nationality",
            "certificate",
            "birthday",
            "address",
            "additionalProperty",
            "externalCustomerId",
            "customerType",
            "customerRelaId",
            "language",
            "writtenLanguage",
            "gender",
            "placeOfBirth",
            "occupation",
            "pinNumber",
            "fatherName",
            "contactNumber",
            "sendToLegal",
            "taxCertificateID",
            "citizenship",
            "customerDocumentList"
        })
        public static class NewCustomer {

            @XmlElement(name = "CustLevel", required = true)
            protected String custLevel;
            @XmlElement(name = "Title", required = true, nillable = true)
            protected String title;
            @XmlElement(name = "Name", required = true)
            protected Name name;
            @XmlElement(name = "Nationality", required = true, nillable = true)
            protected String nationality;
            @XmlElement(name = "Certificate", required = true, nillable = true)
            protected Certificate certificate;
            @XmlElement(name = "Birthday", required = true)
            protected String birthday;
            @XmlElement(name = "Address")
            protected List<Address> address;
            @XmlElement(name = "AdditionalProperty")
            protected List<SimpleProperty> additionalProperty;
            @XmlElement(name = "ExternalCustomerId")
            protected String externalCustomerId;
            @XmlElement(name = "CustomerType")
            protected String customerType;
            @XmlElement(name = "CustomerRelaId")
            protected String customerRelaId;
            @XmlElement(name = "Language")
            protected String language;
            @XmlElement(name = "WrittenLanguage")
            protected String writtenLanguage;
            @XmlElement(name = "Gender", required = true)
            protected String gender;
            @XmlElement(name = "PlaceOfBirth")
            protected String placeOfBirth;
            @XmlElement(name = "Occupation")
            protected String occupation;
            @XmlElement(name = "PINNumber")
            protected String pinNumber;
            @XmlElement(name = "FatherName", required = true)
            protected String fatherName;
            @XmlElement(name = "ContactNumber", required = true)
            protected BigInteger contactNumber;
            @XmlElement(name = "SendToLegal")
            protected String sendToLegal;
            @XmlElement(name = "TaxCertificateID")
            protected String taxCertificateID;
            @XmlElement(name = "Citizenship")
            protected String citizenship;
            @XmlElement(name = "CustomerDocumentList")
            protected List<CustomerDocumentInfo> customerDocumentList;

            /**
             * Gets the value of the custLevel property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustLevel() {
                return custLevel;
            }

            /**
             * Sets the value of the custLevel property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustLevel(String value) {
                this.custLevel = value;
            }

            /**
             * Gets the value of the title property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTitle() {
                return title;
            }

            /**
             * Sets the value of the title property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTitle(String value) {
                this.title = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link Name }
             *     
             */
            public Name getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link Name }
             *     
             */
            public void setName(Name value) {
                this.name = value;
            }

            /**
             * Gets the value of the nationality property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNationality() {
                return nationality;
            }

            /**
             * Sets the value of the nationality property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNationality(String value) {
                this.nationality = value;
            }

            /**
             * Gets the value of the certificate property.
             * 
             * @return
             *     possible object is
             *     {@link Certificate }
             *     
             */
            public Certificate getCertificate() {
                return certificate;
            }

            /**
             * Sets the value of the certificate property.
             * 
             * @param value
             *     allowed object is
             *     {@link Certificate }
             *     
             */
            public void setCertificate(Certificate value) {
                this.certificate = value;
            }

            /**
             * Gets the value of the birthday property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthday() {
                return birthday;
            }

            /**
             * Sets the value of the birthday property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthday(String value) {
                this.birthday = value;
            }

            /**
             * Gets the value of the address property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the address property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddress().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Address }
             * 
             * 
             */
            public List<Address> getAddress() {
                if (address == null) {
                    address = new ArrayList<Address>();
                }
                return this.address;
            }

            /**
             * Gets the value of the additionalProperty property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAdditionalProperty().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link SimpleProperty }
             * 
             * 
             */
            public List<SimpleProperty> getAdditionalProperty() {
                if (additionalProperty == null) {
                    additionalProperty = new ArrayList<SimpleProperty>();
                }
                return this.additionalProperty;
            }

            /**
             * Gets the value of the externalCustomerId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExternalCustomerId() {
                return externalCustomerId;
            }

            /**
             * Sets the value of the externalCustomerId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExternalCustomerId(String value) {
                this.externalCustomerId = value;
            }

            /**
             * Gets the value of the customerType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerType() {
                return customerType;
            }

            /**
             * Sets the value of the customerType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerType(String value) {
                this.customerType = value;
            }

            /**
             * Gets the value of the customerRelaId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerRelaId() {
                return customerRelaId;
            }

            /**
             * Sets the value of the customerRelaId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerRelaId(String value) {
                this.customerRelaId = value;
            }

            /**
             * Gets the value of the language property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLanguage() {
                return language;
            }

            /**
             * Sets the value of the language property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLanguage(String value) {
                this.language = value;
            }

            /**
             * Gets the value of the writtenLanguage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getWrittenLanguage() {
                return writtenLanguage;
            }

            /**
             * Sets the value of the writtenLanguage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setWrittenLanguage(String value) {
                this.writtenLanguage = value;
            }

            /**
             * Gets the value of the gender property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGender() {
                return gender;
            }

            /**
             * Sets the value of the gender property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGender(String value) {
                this.gender = value;
            }

            /**
             * Gets the value of the placeOfBirth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlaceOfBirth() {
                return placeOfBirth;
            }

            /**
             * Sets the value of the placeOfBirth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlaceOfBirth(String value) {
                this.placeOfBirth = value;
            }

            /**
             * Gets the value of the occupation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOccupation() {
                return occupation;
            }

            /**
             * Sets the value of the occupation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOccupation(String value) {
                this.occupation = value;
            }

            /**
             * Gets the value of the pinNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPINNumber() {
                return pinNumber;
            }

            /**
             * Sets the value of the pinNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPINNumber(String value) {
                this.pinNumber = value;
            }

            /**
             * Gets the value of the fatherName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFatherName() {
                return fatherName;
            }

            /**
             * Sets the value of the fatherName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFatherName(String value) {
                this.fatherName = value;
            }

            /**
             * Gets the value of the contactNumber property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getContactNumber() {
                return contactNumber;
            }

            /**
             * Sets the value of the contactNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setContactNumber(BigInteger value) {
                this.contactNumber = value;
            }

            /**
             * Gets the value of the sendToLegal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSendToLegal() {
                return sendToLegal;
            }

            /**
             * Sets the value of the sendToLegal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSendToLegal(String value) {
                this.sendToLegal = value;
            }

            /**
             * Gets the value of the taxCertificateID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxCertificateID() {
                return taxCertificateID;
            }

            /**
             * Sets the value of the taxCertificateID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxCertificateID(String value) {
                this.taxCertificateID = value;
            }

            /**
             * Gets the value of the citizenship property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCitizenship() {
                return citizenship;
            }

            /**
             * Sets the value of the citizenship property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCitizenship(String value) {
                this.citizenship = value;
            }

            /**
             * Gets the value of the customerDocumentList property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the customerDocumentList property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCustomerDocumentList().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CustomerDocumentInfo }
             * 
             * 
             */
            public List<CustomerDocumentInfo> getCustomerDocumentList() {
                if (customerDocumentList == null) {
                    customerDocumentList = new ArrayList<CustomerDocumentInfo>();
                }
                return this.customerDocumentList;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OrderType" type="{http://www.huawei.com/bss/soaif/interface/common/}OrderType"/>
     *         &lt;element name="ExternalOrderId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalOrderId" minOccurs="0"/>
     *         &lt;element name="CustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "orderType",
        "externalOrderId",
        "customerId"
    })
    public static class Order {

        @XmlElement(name = "OrderType", required = true)
        protected String orderType;
        @XmlElement(name = "ExternalOrderId")
        protected String externalOrderId;
        @XmlElement(name = "CustomerId")
        protected String customerId;

        /**
         * Gets the value of the orderType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrderType() {
            return orderType;
        }

        /**
         * Sets the value of the orderType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrderType(String value) {
            this.orderType = value;
        }

        /**
         * Gets the value of the externalOrderId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExternalOrderId() {
            return externalOrderId;
        }

        /**
         * Sets the value of the externalOrderId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExternalOrderId(String value) {
            this.externalOrderId = value;
        }

        /**
         * Gets the value of the customerId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerId() {
            return customerId;
        }

        /**
         * Sets the value of the customerId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerId(String value) {
            this.customerId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance">
     *       &lt;sequence>
     *         &lt;element name="ActiveMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activeMode"
    })
    public static class PrimaryOffering
        extends OfferingInstance
    {

        @XmlElement(name = "ActiveMode")
        protected String activeMode;

        /**
         * Gets the value of the activeMode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActiveMode() {
            return activeMode;
        }

        /**
         * Sets the value of the activeMode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActiveMode(String value) {
            this.activeMode = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ServiceNum" type="{http://www.huawei.com/bss/soaif/interface/common/}ServiceNum"/>
     *         &lt;element name="ICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}ICCID"/>
     *         &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
     *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
     *         &lt;element name="Password" type="{http://www.huawei.com/bss/soaif/interface/common/}Password" minOccurs="0"/>
     *         &lt;element name="IMEI" type="{http://www.huawei.com/bss/soaif/interface/common/}IMEI" minOccurs="0"/>
     *         &lt;element name="Taxation" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="1"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="IVRBlackList">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="5"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="LoyaltySegment">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="1"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
     *         &lt;element name="ExternalSubscriberId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalSubscriberId" minOccurs="0"/>
     *         &lt;element name="ActivateFlag" type="{http://www.huawei.com/bss/soaif/interface/common/}ActivateFlag" minOccurs="0"/>
     *         &lt;element name="DealerId" type="{http://www.huawei.com/bss/soaif/interface/common/}DealerId" minOccurs="0"/>
     *         &lt;element name="Remarks" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="512"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceNum",
        "iccid",
        "language",
        "writtenLanguage",
        "password",
        "imei",
        "taxation",
        "ivrBlackList",
        "loyaltySegment",
        "additionalProperty",
        "externalSubscriberId",
        "activateFlag",
        "dealerId",
        "remarks"
    })
    public static class Subscriber {

        @XmlElement(name = "ServiceNum", required = true)
        protected ServiceNum serviceNum;
        @XmlElement(name = "ICCID", required = true)
        protected String iccid;
        @XmlElement(name = "Language")
        protected String language;
        @XmlElement(name = "WrittenLanguage")
        protected String writtenLanguage;
        @XmlElement(name = "Password")
        protected String password;
        @XmlElement(name = "IMEI")
        protected String imei;
        @XmlElement(name = "Taxation")
        protected String taxation;
        @XmlElement(name = "IVRBlackList", required = true)
        protected String ivrBlackList;
        @XmlElement(name = "LoyaltySegment", required = true)
        protected String loyaltySegment;
        @XmlElement(name = "AdditionalProperty")
        protected List<SimpleProperty> additionalProperty;
        @XmlElement(name = "ExternalSubscriberId")
        protected String externalSubscriberId;
        @XmlElement(name = "ActivateFlag")
        protected String activateFlag;
        @XmlElement(name = "DealerId")
        protected String dealerId;
        @XmlElement(name = "Remarks")
        protected String remarks;

        /**
         * Gets the value of the serviceNum property.
         * 
         * @return
         *     possible object is
         *     {@link ServiceNum }
         *     
         */
        public ServiceNum getServiceNum() {
            return serviceNum;
        }

        /**
         * Sets the value of the serviceNum property.
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceNum }
         *     
         */
        public void setServiceNum(ServiceNum value) {
            this.serviceNum = value;
        }

        /**
         * Gets the value of the iccid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getICCID() {
            return iccid;
        }

        /**
         * Sets the value of the iccid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setICCID(String value) {
            this.iccid = value;
        }

        /**
         * Gets the value of the language property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguage() {
            return language;
        }

        /**
         * Sets the value of the language property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguage(String value) {
            this.language = value;
        }

        /**
         * Gets the value of the writtenLanguage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWrittenLanguage() {
            return writtenLanguage;
        }

        /**
         * Sets the value of the writtenLanguage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWrittenLanguage(String value) {
            this.writtenLanguage = value;
        }

        /**
         * Gets the value of the password property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Sets the value of the password property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

        /**
         * Gets the value of the imei property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIMEI() {
            return imei;
        }

        /**
         * Sets the value of the imei property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIMEI(String value) {
            this.imei = value;
        }

        /**
         * Gets the value of the taxation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxation() {
            return taxation;
        }

        /**
         * Sets the value of the taxation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxation(String value) {
            this.taxation = value;
        }

        /**
         * Gets the value of the ivrBlackList property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIVRBlackList() {
            return ivrBlackList;
        }

        /**
         * Sets the value of the ivrBlackList property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIVRBlackList(String value) {
            this.ivrBlackList = value;
        }

        /**
         * Gets the value of the loyaltySegment property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoyaltySegment() {
            return loyaltySegment;
        }

        /**
         * Sets the value of the loyaltySegment property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoyaltySegment(String value) {
            this.loyaltySegment = value;
        }

        /**
         * Gets the value of the additionalProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdditionalProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getAdditionalProperty() {
            if (additionalProperty == null) {
                additionalProperty = new ArrayList<SimpleProperty>();
            }
            return this.additionalProperty;
        }

        /**
         * Gets the value of the externalSubscriberId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExternalSubscriberId() {
            return externalSubscriberId;
        }

        /**
         * Sets the value of the externalSubscriberId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExternalSubscriberId(String value) {
            this.externalSubscriberId = value;
        }

        /**
         * Gets the value of the activateFlag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivateFlag() {
            return activateFlag;
        }

        /**
         * Sets the value of the activateFlag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivateFlag(String value) {
            this.activateFlag = value;
        }

        /**
         * Gets the value of the dealerId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDealerId() {
            return dealerId;
        }

        /**
         * Sets the value of the dealerId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDealerId(String value) {
            this.dealerId = value;
        }

        /**
         * Gets the value of the remarks property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemarks() {
            return remarks;
        }

        /**
         * Sets the value of the remarks property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemarks(String value) {
            this.remarks = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance">
     *       &lt;sequence>
     *         &lt;element name="EffectiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}EffectiveMode"/>
     *         &lt;element name="ExpirationDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
     *         &lt;element name="ActiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}ActiveMode"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "effectiveMode",
        "expirationDate",
        "activeMode"
    })
    public static class SupplementaryOffering
        extends OfferingInstance
    {

        @XmlElement(name = "EffectiveMode", required = true)
        protected EffectiveMode effectiveMode;
        @XmlElement(name = "ExpirationDate", required = true)
        protected String expirationDate;
        @XmlElement(name = "ActiveMode", required = true)
        protected ActiveMode activeMode;

        /**
         * Gets the value of the effectiveMode property.
         * 
         * @return
         *     possible object is
         *     {@link EffectiveMode }
         *     
         */
        public EffectiveMode getEffectiveMode() {
            return effectiveMode;
        }

        /**
         * Sets the value of the effectiveMode property.
         * 
         * @param value
         *     allowed object is
         *     {@link EffectiveMode }
         *     
         */
        public void setEffectiveMode(EffectiveMode value) {
            this.effectiveMode = value;
        }

        /**
         * Gets the value of the expirationDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationDate() {
            return expirationDate;
        }

        /**
         * Sets the value of the expirationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationDate(String value) {
            this.expirationDate = value;
        }

        /**
         * Gets the value of the activeMode property.
         * 
         * @return
         *     possible object is
         *     {@link ActiveMode }
         *     
         */
        public ActiveMode getActiveMode() {
            return activeMode;
        }

        /**
         * Sets the value of the activeMode property.
         * 
         * @param value
         *     allowed object is
         *     {@link ActiveMode }
         *     
         */
        public void setActiveMode(ActiveMode value) {
            this.activeMode = value;
        }

    }

}
