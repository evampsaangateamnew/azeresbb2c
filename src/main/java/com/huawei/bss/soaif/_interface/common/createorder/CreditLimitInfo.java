
package com.huawei.bss.soaif._interface.common.createorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditLimitInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditLimitInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LimitClass">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *               &lt;enumeration value="I"/>
 *               &lt;enumeration value="T"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *         &lt;element name="CreditLimitSeq" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ValidPeriod" type="{http://www.huawei.com/bss/soaif/interface/common/}TimePeriod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditLimitInfo", propOrder = {
    "creditLimitType",
    "limitClass",
    "limitValue",
    "actionType",
    "creditLimitSeq",
    "validPeriod"
})
public class CreditLimitInfo {

    @XmlElement(name = "CreditLimitType", required = true)
    protected String creditLimitType;
    @XmlElement(name = "LimitClass", required = true)
    protected String limitClass;
    @XmlElement(name = "LimitValue")
    protected long limitValue;
    @XmlElement(name = "ActionType", required = true)
    protected String actionType;
    @XmlElement(name = "CreditLimitSeq")
    protected Long creditLimitSeq;
    @XmlElement(name = "ValidPeriod")
    protected TimePeriod validPeriod;

    /**
     * Gets the value of the creditLimitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditLimitType() {
        return creditLimitType;
    }

    /**
     * Sets the value of the creditLimitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditLimitType(String value) {
        this.creditLimitType = value;
    }

    /**
     * Gets the value of the limitClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitClass() {
        return limitClass;
    }

    /**
     * Sets the value of the limitClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitClass(String value) {
        this.limitClass = value;
    }

    /**
     * Gets the value of the limitValue property.
     * 
     */
    public long getLimitValue() {
        return limitValue;
    }

    /**
     * Sets the value of the limitValue property.
     * 
     */
    public void setLimitValue(long value) {
        this.limitValue = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the creditLimitSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCreditLimitSeq() {
        return creditLimitSeq;
    }

    /**
     * Sets the value of the creditLimitSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCreditLimitSeq(Long value) {
        this.creditLimitSeq = value;
    }

    /**
     * Gets the value of the validPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link TimePeriod }
     *     
     */
    public TimePeriod getValidPeriod() {
        return validPeriod;
    }

    /**
     * Sets the value of the validPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimePeriod }
     *     
     */
    public void setValidPeriod(TimePeriod value) {
        this.validPeriod = value;
    }

}
