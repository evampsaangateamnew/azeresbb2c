package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.huawei.bss.soaif._interface.common.RspHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;sequence>
 *           &lt;element name="LoanAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
 *           &lt;element name="LoanBalanceAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
 *           &lt;element name="RepayAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long"/>
 *           &lt;element name="ETUGracePeriod" type="{http://www.huawei.com/bss/soaif/interface/common/}int"/>
 *           &lt;element name="GracePeriod" type="{http://www.huawei.com/bss/soaif/interface/common/}int"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rspHeader", "loanAmount", "loanBalanceAmount", "repayAmount", "etuGracePeriod",
		"gracePeriod" })
@XmlRootElement(name = "USSDGWLoanRspMsg")
public class USSDGWLoanRspMsg {
	@XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected RspHeader rspHeader;
	@XmlElement(name = "LoanAmount", required = false)
	@XmlJavaTypeAdapter(type = long.class, value = com.huawei.crm.service.LongXMLAdapter.class)
	protected long loanAmount;
	@XmlElement(name = "LoanBalanceAmount", required = false)
	@XmlJavaTypeAdapter(type = long.class, value = com.huawei.crm.service.LongXMLAdapter.class)
	protected long loanBalanceAmount;
	@XmlJavaTypeAdapter(type = long.class, value = com.huawei.crm.service.LongXMLAdapter.class)
	@XmlElement(name = "RepayAmount", required = false)
	protected long repayAmount;
	@XmlElement(name = "ETUGracePeriod", required = false)
	protected int etuGracePeriod;
	@XmlElement(name = "GracePeriod")
	protected int gracePeriod;

	/**
	 * Gets the value of the rspHeader property.
	 * 
	 * @return possible object is {@link RspHeader }
	 * 
	 */
	public RspHeader getRspHeader() {
		return rspHeader;
	}

	/**
	 * Sets the value of the rspHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RspHeader }
	 * 
	 */
	public void setRspHeader(RspHeader value) {
		this.rspHeader = value;
	}

	/**
	 * Gets the value of the loanAmount property.
	 * 
	 */
	public long getLoanAmount() {
		return loanAmount;
	}

	/**
	 * Sets the value of the loanAmount property.
	 * 
	 */
	public void setLoanAmount(long value) {
		this.loanAmount = value;
	}

	/**
	 * Gets the value of the loanBalanceAmount property.
	 * 
	 */
	public long getLoanBalanceAmount() {
		return loanBalanceAmount;
	}

	/**
	 * Sets the value of the loanBalanceAmount property.
	 * 
	 */
	public void setLoanBalanceAmount(long value) {
		this.loanBalanceAmount = value;
	}

	/**
	 * Gets the value of the repayAmount property.
	 * 
	 */
	public long getRepayAmount() {
		return repayAmount;
	}

	/**
	 * Sets the value of the repayAmount property.
	 * 
	 */
	public void setRepayAmount(long value) {
		this.repayAmount = value;
	}

	/**
	 * Gets the value of the etuGracePeriod property.
	 * 
	 */
	public int getETUGracePeriod() {
		return etuGracePeriod;
	}

	/**
	 * Sets the value of the etuGracePeriod property.
	 * 
	 */
	public void setETUGracePeriod(int value) {
		this.etuGracePeriod = value;
	}

	/**
	 * Gets the value of the gracePeriod property.
	 * 
	 */
	public int getGracePeriod() {
		return gracePeriod;
	}

	/**
	 * Sets the value of the gracePeriod property.
	 * 
	 */
	public void setGracePeriod(int value) {
		this.gracePeriod = value;
	}
}
