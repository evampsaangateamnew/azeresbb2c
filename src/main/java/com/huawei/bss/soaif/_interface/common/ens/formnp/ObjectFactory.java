package com.huawei.bss.soaif._interface.common.ens.formnp;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.huawei.bss.soaif._interface.common
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.huawei.bss.soaif._interface.common
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link RspHeader }
	 * 
	 */
	public RspHeader createRspHeader() {
		return new RspHeader();
	}

	/**
	 * Create an instance of {@link SimpleProperty }
	 * 
	 */
	public SimpleProperty createSimpleProperty() {
		return new SimpleProperty();
	}

	/**
	 * Create an instance of {@link ReqHeader }
	 * 
	 */
	public ReqHeader createReqHeader() {
		return new ReqHeader();
	}
}
