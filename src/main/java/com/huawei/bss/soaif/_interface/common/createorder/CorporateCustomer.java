
package com.huawei.bss.soaif._interface.common.createorder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorporateCustomer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustID" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId" minOccurs="0"/>
 *         &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel" minOccurs="0"/>
 *         &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate"/>
 *         &lt;element name="CustType" type="{http://www.huawei.com/bss/soaif/interface/common/}OrganizationType" minOccurs="0"/>
 *         &lt;element name="CustName" type="{http://www.huawei.com/bss/soaif/interface/common/}CustName"/>
 *         &lt;element name="CustShortName" type="{http://www.huawei.com/bss/soaif/interface/common/}CustShortName" minOccurs="0"/>
 *         &lt;element name="CorpNo" type="{http://www.huawei.com/bss/soaif/interface/common/}CorpNo" minOccurs="0"/>
 *         &lt;element name="CustomerGrade" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerGrade" minOccurs="0"/>
 *         &lt;element name="TINNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}TINNumber" minOccurs="0"/>
 *         &lt;element name="Taxation" type="{http://www.huawei.com/bss/soaif/interface/common/}Taxation" minOccurs="0"/>
 *         &lt;element name="TaxExemptionStartDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="TaxExemptionEndDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="CustSize" type="{http://www.huawei.com/bss/soaif/interface/common/}CustSize" minOccurs="0"/>
 *         &lt;element name="Industry" type="{http://www.huawei.com/bss/soaif/interface/common/}Industry"/>
 *         &lt;element name="SubIndustry" type="{http://www.huawei.com/bss/soaif/interface/common/}Industry" minOccurs="0"/>
 *         &lt;element name="CustPhoneNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}Phone" minOccurs="0"/>
 *         &lt;element name="CustFaxNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}Phone" minOccurs="0"/>
 *         &lt;element name="CustEmail" type="{http://www.huawei.com/bss/soaif/interface/common/}Email" minOccurs="0"/>
 *         &lt;element name="RegisterDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *         &lt;element name="RegisterCapital" type="{http://www.huawei.com/bss/soaif/interface/common/}RegisterCapital" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="BRNExpiryDate" type="{http://www.huawei.com/bss/soaif/interface/common/}BRNExpiryDate" minOccurs="0"/>
 *         &lt;element name="TaxCertificateID" type="{http://www.huawei.com/bss/soaif/interface/common/}TaxCertificateID" minOccurs="0"/>
 *         &lt;element name="ContactNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactNumber" minOccurs="0"/>
 *         &lt;element name="SendToLegal" type="{http://www.huawei.com/bss/soaif/interface/common/}SendToLegal" minOccurs="0"/>
 *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *         &lt;element name="CustLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}Remark" minOccurs="0"/>
 *         &lt;element name="Manager" type="{http://www.huawei.com/bss/soaif/interface/common/}ManagerInfo" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="CustomerDocumentList" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerDocumentInfo" maxOccurs="100" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporateCustomer", propOrder = {
    "custID",
    "custLevel",
    "certificate",
    "custType",
    "custName",
    "custShortName",
    "corpNo",
    "customerGrade",
    "tinNumber",
    "taxation",
    "taxExemptionStartDate",
    "taxExemptionEndDate",
    "custSize",
    "industry",
    "subIndustry",
    "custPhoneNumber",
    "custFaxNumber",
    "custEmail",
    "registerDate",
    "registerCapital",
    "address",
    "brnExpiryDate",
    "taxCertificateID",
    "contactNumber",
    "sendToLegal",
    "writtenLanguage",
    "custLanguage",
    "remark",
    "manager",
    "additionalProperty",
    "customerDocumentList"
})
public class CorporateCustomer {

    @XmlElement(name = "CustID")
    protected String custID;
    @XmlElement(name = "CustLevel")
    protected String custLevel;
    @XmlElement(name = "Certificate", required = true)
    protected Certificate certificate;
    @XmlElement(name = "CustType")
    protected String custType;
    @XmlElement(name = "CustName", required = true)
    protected String custName;
    @XmlElement(name = "CustShortName")
    protected String custShortName;
    @XmlElement(name = "CorpNo")
    protected String corpNo;
    @XmlElement(name = "CustomerGrade")
    protected String customerGrade;
    @XmlElement(name = "TINNumber")
    protected String tinNumber;
    @XmlElement(name = "Taxation")
    protected String taxation;
    @XmlElement(name = "TaxExemptionStartDate")
    protected String taxExemptionStartDate;
    @XmlElement(name = "TaxExemptionEndDate")
    protected String taxExemptionEndDate;
    @XmlElement(name = "CustSize")
    protected String custSize;
    @XmlElement(name = "Industry", required = true)
    protected String industry;
    @XmlElement(name = "SubIndustry")
    protected String subIndustry;
    @XmlElement(name = "CustPhoneNumber")
    protected String custPhoneNumber;
    @XmlElement(name = "CustFaxNumber")
    protected String custFaxNumber;
    @XmlElement(name = "CustEmail")
    protected String custEmail;
    @XmlElement(name = "RegisterDate", required = true)
    protected String registerDate;
    @XmlElement(name = "RegisterCapital")
    protected String registerCapital;
    @XmlElement(name = "Address")
    protected List<Address> address;
    @XmlElement(name = "BRNExpiryDate")
    protected String brnExpiryDate;
    @XmlElement(name = "TaxCertificateID")
    protected String taxCertificateID;
    @XmlElement(name = "ContactNumber")
    protected BigInteger contactNumber;
    @XmlElement(name = "SendToLegal")
    protected String sendToLegal;
    @XmlElement(name = "WrittenLanguage")
    protected String writtenLanguage;
    @XmlElement(name = "CustLanguage")
    protected String custLanguage;
    @XmlElement(name = "Remark")
    protected String remark;
    @XmlElement(name = "Manager")
    protected List<ManagerInfo> manager;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;
    @XmlElement(name = "CustomerDocumentList")
    protected List<CustomerDocumentInfo> customerDocumentList;

    /**
     * Gets the value of the custID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustID() {
        return custID;
    }

    /**
     * Sets the value of the custID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustID(String value) {
        this.custID = value;
    }

    /**
     * Gets the value of the custLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLevel() {
        return custLevel;
    }

    /**
     * Sets the value of the custLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLevel(String value) {
        this.custLevel = value;
    }

    /**
     * Gets the value of the certificate property.
     * 
     * @return
     *     possible object is
     *     {@link Certificate }
     *     
     */
    public Certificate getCertificate() {
        return certificate;
    }

    /**
     * Sets the value of the certificate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Certificate }
     *     
     */
    public void setCertificate(Certificate value) {
        this.certificate = value;
    }

    /**
     * Gets the value of the custType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustType() {
        return custType;
    }

    /**
     * Sets the value of the custType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustType(String value) {
        this.custType = value;
    }

    /**
     * Gets the value of the custName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustName() {
        return custName;
    }

    /**
     * Sets the value of the custName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustName(String value) {
        this.custName = value;
    }

    /**
     * Gets the value of the custShortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustShortName() {
        return custShortName;
    }

    /**
     * Sets the value of the custShortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustShortName(String value) {
        this.custShortName = value;
    }

    /**
     * Gets the value of the corpNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpNo() {
        return corpNo;
    }

    /**
     * Sets the value of the corpNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpNo(String value) {
        this.corpNo = value;
    }

    /**
     * Gets the value of the customerGrade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerGrade() {
        return customerGrade;
    }

    /**
     * Sets the value of the customerGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerGrade(String value) {
        this.customerGrade = value;
    }

    /**
     * Gets the value of the tinNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTINNumber() {
        return tinNumber;
    }

    /**
     * Sets the value of the tinNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTINNumber(String value) {
        this.tinNumber = value;
    }

    /**
     * Gets the value of the taxation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxation() {
        return taxation;
    }

    /**
     * Sets the value of the taxation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxation(String value) {
        this.taxation = value;
    }

    /**
     * Gets the value of the taxExemptionStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxExemptionStartDate() {
        return taxExemptionStartDate;
    }

    /**
     * Sets the value of the taxExemptionStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxExemptionStartDate(String value) {
        this.taxExemptionStartDate = value;
    }

    /**
     * Gets the value of the taxExemptionEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxExemptionEndDate() {
        return taxExemptionEndDate;
    }

    /**
     * Sets the value of the taxExemptionEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxExemptionEndDate(String value) {
        this.taxExemptionEndDate = value;
    }

    /**
     * Gets the value of the custSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustSize() {
        return custSize;
    }

    /**
     * Sets the value of the custSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustSize(String value) {
        this.custSize = value;
    }

    /**
     * Gets the value of the industry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * Sets the value of the industry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustry(String value) {
        this.industry = value;
    }

    /**
     * Gets the value of the subIndustry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubIndustry() {
        return subIndustry;
    }

    /**
     * Sets the value of the subIndustry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubIndustry(String value) {
        this.subIndustry = value;
    }

    /**
     * Gets the value of the custPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustPhoneNumber() {
        return custPhoneNumber;
    }

    /**
     * Sets the value of the custPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustPhoneNumber(String value) {
        this.custPhoneNumber = value;
    }

    /**
     * Gets the value of the custFaxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustFaxNumber() {
        return custFaxNumber;
    }

    /**
     * Sets the value of the custFaxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustFaxNumber(String value) {
        this.custFaxNumber = value;
    }

    /**
     * Gets the value of the custEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustEmail() {
        return custEmail;
    }

    /**
     * Sets the value of the custEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustEmail(String value) {
        this.custEmail = value;
    }

    /**
     * Gets the value of the registerDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterDate() {
        return registerDate;
    }

    /**
     * Sets the value of the registerDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterDate(String value) {
        this.registerDate = value;
    }

    /**
     * Gets the value of the registerCapital property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterCapital() {
        return registerCapital;
    }

    /**
     * Sets the value of the registerCapital property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterCapital(String value) {
        this.registerCapital = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the address property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Address }
     * 
     * 
     */
    public List<Address> getAddress() {
        if (address == null) {
            address = new ArrayList<Address>();
        }
        return this.address;
    }

    /**
     * Gets the value of the brnExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRNExpiryDate() {
        return brnExpiryDate;
    }

    /**
     * Sets the value of the brnExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRNExpiryDate(String value) {
        this.brnExpiryDate = value;
    }

    /**
     * Gets the value of the taxCertificateID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCertificateID() {
        return taxCertificateID;
    }

    /**
     * Sets the value of the taxCertificateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCertificateID(String value) {
        this.taxCertificateID = value;
    }

    /**
     * Gets the value of the contactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getContactNumber() {
        return contactNumber;
    }

    /**
     * Sets the value of the contactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setContactNumber(BigInteger value) {
        this.contactNumber = value;
    }

    /**
     * Gets the value of the sendToLegal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendToLegal() {
        return sendToLegal;
    }

    /**
     * Sets the value of the sendToLegal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendToLegal(String value) {
        this.sendToLegal = value;
    }

    /**
     * Gets the value of the writtenLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWrittenLanguage() {
        return writtenLanguage;
    }

    /**
     * Sets the value of the writtenLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWrittenLanguage(String value) {
        this.writtenLanguage = value;
    }

    /**
     * Gets the value of the custLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLanguage() {
        return custLanguage;
    }

    /**
     * Sets the value of the custLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLanguage(String value) {
        this.custLanguage = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the manager property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the manager property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getManager().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ManagerInfo }
     * 
     * 
     */
    public List<ManagerInfo> getManager() {
        if (manager == null) {
            manager = new ArrayList<ManagerInfo>();
        }
        return this.manager;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }

    /**
     * Gets the value of the customerDocumentList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customerDocumentList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomerDocumentList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerDocumentInfo }
     * 
     * 
     */
    public List<CustomerDocumentInfo> getCustomerDocumentList() {
        if (customerDocumentList == null) {
            customerDocumentList = new ArrayList<CustomerDocumentInfo>();
        }
        return this.customerDocumentList;
    }

}
