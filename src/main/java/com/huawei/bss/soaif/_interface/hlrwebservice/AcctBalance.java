package com.huawei.bss.soaif._interface.hlrwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AcctBalance complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AcctBalance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BalanceType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="BalanceTypeName" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="TotalAmount" type="{http://www.huawei.com/bss/soaif/interface/common/}long" minOccurs="0"/>
 *         &lt;element name="DepositFlag" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="RefundFlag" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="CurrencyID" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="BalanceDetail" type="{http://www.huawei.com/bss/soaif/interface/HLRWEBService/}BalanceDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctBalance", propOrder = { "balanceType", "balanceTypeName", "totalAmount", "depositFlag",
		"refundFlag", "currencyID", "balanceDetail" })
public class AcctBalance {
	@XmlElement(name = "BalanceType")
	protected String balanceType;
	@XmlElement(name = "BalanceTypeName")
	protected String balanceTypeName;
	@XmlElement(name = "TotalAmount")
	protected String totalAmount;
	@XmlElement(name = "DepositFlag")
	protected String depositFlag;
	@XmlElement(name = "RefundFlag")
	protected String refundFlag;
	@XmlElement(name = "CurrencyID")
	protected String currencyID;
	@XmlElement(name = "BalanceDetail")
	protected BalanceDetail balanceDetail;

	/**
	 * Gets the value of the balanceType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBalanceType() {
		return balanceType;
	}

	/**
	 * Sets the value of the balanceType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBalanceType(String value) {
		this.balanceType = value;
	}

	/**
	 * Gets the value of the balanceTypeName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBalanceTypeName() {
		return balanceTypeName;
	}

	/**
	 * Sets the value of the balanceTypeName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBalanceTypeName(String value) {
		this.balanceTypeName = value;
	}

	/**
	 * Gets the value of the totalAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public String getTotalAmount() {
		return totalAmount;
	}

	/**
	 * Sets the value of the totalAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setTotalAmount(String value) {
		this.totalAmount = value;
	}

	/**
	 * Gets the value of the depositFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDepositFlag() {
		return depositFlag;
	}

	/**
	 * Sets the value of the depositFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDepositFlag(String value) {
		this.depositFlag = value;
	}

	/**
	 * Gets the value of the refundFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRefundFlag() {
		return refundFlag;
	}

	/**
	 * Sets the value of the refundFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRefundFlag(String value) {
		this.refundFlag = value;
	}

	/**
	 * Gets the value of the currencyID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyID() {
		return currencyID;
	}

	/**
	 * Sets the value of the currencyID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrencyID(String value) {
		this.currencyID = value;
	}

	/**
	 * Gets the value of the balanceDetail property.
	 * 
	 * @return possible object is {@link BalanceDetail }
	 * 
	 */
	public BalanceDetail getBalanceDetail() {
		return balanceDetail;
	}

	/**
	 * Sets the value of the balanceDetail property.
	 * 
	 * @param value
	 *            allowed object is {@link BalanceDetail }
	 * 
	 */
	public void setBalanceDetail(BalanceDetail value) {
		this.balanceDetail = value;
	}
}
