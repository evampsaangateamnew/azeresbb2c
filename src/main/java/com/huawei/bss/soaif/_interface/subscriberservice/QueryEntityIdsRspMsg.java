package com.huawei.bss.soaif._interface.subscriberservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.RspHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;element name="EntityId" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
 *                 &lt;sequence>
 *                   &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rspHeader", "entityId" })
@XmlRootElement(name = "QueryEntityIdsRspMsg")
public class QueryEntityIdsRspMsg {
	@XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected RspHeader rspHeader;
	@XmlElement(name = "EntityId")
	protected List<QueryEntityIdsRspMsg.EntityId> entityId;

	/**
	 * Gets the value of the rspHeader property.
	 * 
	 * @return possible object is {@link RspHeader }
	 * 
	 */
	public RspHeader getRspHeader() {
		return rspHeader;
	}

	/**
	 * Sets the value of the rspHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RspHeader }
	 * 
	 */
	public void setRspHeader(RspHeader value) {
		this.rspHeader = value;
	}

	/**
	 * Gets the value of the entityId property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the entityId property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getEntityId().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link QueryEntityIdsRspMsg.EntityId }
	 * 
	 * 
	 */
	public List<QueryEntityIdsRspMsg.EntityId> getEntityId() {
		if (entityId == null) {
			entityId = new ArrayList<QueryEntityIdsRspMsg.EntityId>();
		}
		return this.entityId;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
	 *       &lt;sequence>
	 *         &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/extension>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "paymentType" })
	public static class EntityId extends ObjectAccessInfo {
		@XmlElement(name = "PaymentType")
		protected String paymentType;

		/**
		 * Gets the value of the paymentType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPaymentType() {
			return paymentType;
		}

		/**
		 * Sets the value of the paymentType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPaymentType(String value) {
			this.paymentType = value;
		}
	}
}
