package com.huawei.bss.soaif._interface.ussdgateway;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AccountCredit complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AccountCredit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditLimitType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="CreditLimitTypeName" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="TotalCreditAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TotalUsageAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TotalRemainAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CreditAmountInfo" type="{http://www.huawei.com/bss/soaif/interface/USSDGateWay/}CreditAmountInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CurrencyID" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountCredit", propOrder = { "creditLimitType", "creditLimitTypeName", "totalCreditAmount",
		"totalUsageAmount", "totalRemainAmount", "creditAmountInfo", "currencyID" })
public class AccountCredit {
	@XmlElement(name = "CreditLimitType")
	protected String creditLimitType;
	@XmlElement(name = "CreditLimitTypeName")
	protected String creditLimitTypeName;
	@XmlElement(name = "TotalCreditAmount")
	protected Long totalCreditAmount;
	@XmlElement(name = "TotalUsageAmount")
	protected Long totalUsageAmount;
	@XmlElement(name = "TotalRemainAmount")
	protected Long totalRemainAmount;
	@XmlElement(name = "CreditAmountInfo")
	protected List<CreditAmountInfo> creditAmountInfo;
	@XmlElement(name = "CurrencyID")
	protected String currencyID;

	/**
	 * Gets the value of the creditLimitType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreditLimitType() {
		return creditLimitType;
	}

	/**
	 * Sets the value of the creditLimitType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreditLimitType(String value) {
		this.creditLimitType = value;
	}

	/**
	 * Gets the value of the creditLimitTypeName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreditLimitTypeName() {
		return creditLimitTypeName;
	}

	/**
	 * Sets the value of the creditLimitTypeName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreditLimitTypeName(String value) {
		this.creditLimitTypeName = value;
	}

	/**
	 * Gets the value of the totalCreditAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getTotalCreditAmount() {
		return totalCreditAmount;
	}

	/**
	 * Sets the value of the totalCreditAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setTotalCreditAmount(Long value) {
		this.totalCreditAmount = value;
	}

	/**
	 * Gets the value of the totalUsageAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getTotalUsageAmount() {
		return totalUsageAmount;
	}

	/**
	 * Sets the value of the totalUsageAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setTotalUsageAmount(Long value) {
		this.totalUsageAmount = value;
	}

	/**
	 * Gets the value of the totalRemainAmount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getTotalRemainAmount() {
		return totalRemainAmount;
	}

	/**
	 * Sets the value of the totalRemainAmount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setTotalRemainAmount(Long value) {
		this.totalRemainAmount = value;
	}

	/**
	 * Gets the value of the creditAmountInfo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the creditAmountInfo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCreditAmountInfo().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CreditAmountInfo }
	 * 
	 * 
	 */
	public List<CreditAmountInfo> getCreditAmountInfo() {
		if (creditAmountInfo == null) {
			creditAmountInfo = new ArrayList<CreditAmountInfo>();
		}
		return this.creditAmountInfo;
	}

	/**
	 * Gets the value of the currencyID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyID() {
		return currencyID;
	}

	/**
	 * Sets the value of the currencyID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrencyID(String value) {
		this.currencyID = value;
	}
}
