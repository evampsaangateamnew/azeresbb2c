package com.huawei.bss.soaif._interface.mnpservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Subscription_Info_GetIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Subscription_Info_GetIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="USERID" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="DealerID" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="SubscriberNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Subscription_Info_GetIn", propOrder = {})
public class SubscriptionInfoGetIn {
	@XmlElement(name = "USERID")
	protected BigInteger userid;
	@XmlElement(name = "DealerID")
	protected BigInteger dealerID;
	@XmlElement(name = "SubscriberNumber", required = true)
	protected String subscriberNumber;

	/**
	 * Gets the value of the userid property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getUSERID() {
		return userid;
	}

	/**
	 * Sets the value of the userid property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setUSERID(BigInteger value) {
		this.userid = value;
	}

	/**
	 * Gets the value of the dealerID property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getDealerID() {
		return dealerID;
	}

	/**
	 * Sets the value of the dealerID property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setDealerID(BigInteger value) {
		this.dealerID = value;
	}

	/**
	 * Gets the value of the subscriberNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubscriberNumber() {
		return subscriberNumber;
	}

	/**
	 * Sets the value of the subscriberNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubscriberNumber(String value) {
		this.subscriberNumber = value;
	}
}
