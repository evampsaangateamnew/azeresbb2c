package com.huawei.bss.soaif._interface.subscriberservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.huawei.bss.soaif._interface.common.Certificate;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * com.huawei.bss.soaif._interface.subscriberservice package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	private final static QName _SupplementCustInfoReqMsgCustomerNationality_QNAME = new QName(
			"http://www.huawei.com/bss/soaif/interface/SubscriberService/", "Nationality");
	private final static QName _SupplementCustInfoReqMsgCustomerTitle_QNAME = new QName(
			"http://www.huawei.com/bss/soaif/interface/SubscriberService/", "Title");
	private final static QName _ChangeSubOwnerReqMsgNewCustInfoCustomerCertificate_QNAME = new QName(
			"http://www.huawei.com/bss/soaif/interface/SubscriberService/", "Certificate");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.huawei.bss.soaif._interface.subscriberservice
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ChangeSubMSISDNReqMsg }
	 * 
	 */
	public ChangeSubMSISDNReqMsg createChangeSubMSISDNReqMsg() {
		return new ChangeSubMSISDNReqMsg();
	}

	/**
	 * Create an instance of {@link ChangePromotionReqMsg }
	 * 
	 */
	public ChangePromotionReqMsg createChangePromotionReqMsg() {
		return new ChangePromotionReqMsg();
	}

	/**
	 * Create an instance of {@link DeactivateSubReqMsg }
	 * 
	 */
	public DeactivateSubReqMsg createDeactivateSubReqMsg() {
		return new DeactivateSubReqMsg();
	}

	/**
	 * Create an instance of {@link CancelLostReqMsg }
	 * 
	 */
	public CancelLostReqMsg createCancelLostReqMsg() {
		return new CancelLostReqMsg();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerReqMsg }
	 * 
	 */
	public ChangeSubOwnerReqMsg createChangeSubOwnerReqMsg() {
		return new ChangeSubOwnerReqMsg();
	}

	/**
	 * Create an instance of {@link ChangeRechargeBlacklistReqMsg }
	 * 
	 */
	public ChangeRechargeBlacklistReqMsg createChangeRechargeBlacklistReqMsg() {
		return new ChangeRechargeBlacklistReqMsg();
	}

	/**
	 * Create an instance of {@link ChangeSubSIMReqMsg }
	 * 
	 */
	public ChangeSubSIMReqMsg createChangeSubSIMReqMsg() {
		return new ChangeSubSIMReqMsg();
	}

	/**
	 * Create an instance of {@link ReportLostReqMsg }
	 * 
	 */
	public ReportLostReqMsg createReportLostReqMsg() {
		return new ReportLostReqMsg();
	}

	/**
	 * Create an instance of {@link ChangePaymentRelationReqMsg }
	 * 
	 */
	public ChangePaymentRelationReqMsg createChangePaymentRelationReqMsg() {
		return new ChangePaymentRelationReqMsg();
	}

	/**
	 * Create an instance of {@link AssignAcctToSubReqMsg }
	 * 
	 */
	public AssignAcctToSubReqMsg createAssignAcctToSubReqMsg() {
		return new AssignAcctToSubReqMsg();
	}

	/**
	 * Create an instance of {@link QueryEntityIdsRspMsg }
	 * 
	 */
	public QueryEntityIdsRspMsg createQueryEntityIdsRspMsg() {
		return new QueryEntityIdsRspMsg();
	}

	/**
	 * Create an instance of {@link PreDeactivateSubReqMsg }
	 * 
	 */
	public PreDeactivateSubReqMsg createPreDeactivateSubReqMsg() {
		return new PreDeactivateSubReqMsg();
	}

	/**
	 * Create an instance of {@link QuerySubInfoRspMsg }
	 * 
	 */
	public QuerySubInfoRspMsg createQuerySubInfoRspMsg() {
		return new QuerySubInfoRspMsg();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg }
	 * 
	 */
	public SupplementCustInfoReqMsg createSupplementCustInfoReqMsg() {
		return new SupplementCustInfoReqMsg();
	}

	/**
	 * Create an instance of {@link ChangeSubInfoReqMsg }
	 * 
	 */
	public ChangeSubInfoReqMsg createChangeSubInfoReqMsg() {
		return new ChangeSubInfoReqMsg();
	}

	/**
	 * Create an instance of {@link ChangeConsumptionLimitReqMsg }
	 * 
	 */
	public ChangeConsumptionLimitReqMsg createChangeConsumptionLimitReqMsg() {
		return new ChangeConsumptionLimitReqMsg();
	}

	/**
	 * Create an instance of {@link ChangePrepaidToPostpaidReqMsg }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg createChangePrepaidToPostpaidReqMsg() {
		return new ChangePrepaidToPostpaidReqMsg();
	}

	/**
	 * Create an instance of {@link ChangePrepaidToPostpaidReqMsg.Offering }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.Offering createChangePrepaidToPostpaidReqMsgOffering() {
		return new ChangePrepaidToPostpaidReqMsg.Offering();
	}

	/**
	 * Create an instance of {@link ChangePrepaidToPostpaidReqMsg.PostpaidAcct }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.PostpaidAcct createChangePrepaidToPostpaidReqMsgPostpaidAcct() {
		return new ChangePrepaidToPostpaidReqMsg.PostpaidAcct();
	}

	/**
	 * Create an instance of {@link ChangePrepaidToPostpaidReqMsg.PostpaidCust }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.PostpaidCust createChangePrepaidToPostpaidReqMsgPostpaidCust() {
		return new ChangePrepaidToPostpaidReqMsg.PostpaidCust();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg.Account }
	 * 
	 */
	public SupplementCustInfoReqMsg.Account createSupplementCustInfoReqMsgAccount() {
		return new SupplementCustInfoReqMsg.Account();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg.Customer }
	 * 
	 */
	public SupplementCustInfoReqMsg.Customer createSupplementCustInfoReqMsgCustomer() {
		return new SupplementCustInfoReqMsg.Customer();
	}

	/**
	 * Create an instance of {@link AssignAcctToSubReqMsg.AcctInfo }
	 * 
	 */
	public AssignAcctToSubReqMsg.AcctInfo createAssignAcctToSubReqMsgAcctInfo() {
		return new AssignAcctToSubReqMsg.AcctInfo();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerReqMsg.NewAcctInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewAcctInfo createChangeSubOwnerReqMsgNewAcctInfo() {
		return new ChangeSubOwnerReqMsg.NewAcctInfo();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerReqMsg.NewCustInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewCustInfo createChangeSubOwnerReqMsgNewCustInfo() {
		return new ChangeSubOwnerReqMsg.NewCustInfo();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerReqMsg.NewCustInfo.Customer }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewCustInfo.Customer createChangeSubOwnerReqMsgNewCustInfoCustomer() {
		return new ChangeSubOwnerReqMsg.NewCustInfo.Customer();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerRspMsg }
	 * 
	 */
	public ChangeSubOwnerRspMsg createChangeSubOwnerRspMsg() {
		return new ChangeSubOwnerRspMsg();
	}

	/**
	 * Create an instance of {@link ChangeSubMSISDNReqMsg.AccessInfo }
	 * 
	 */
	public ChangeSubMSISDNReqMsg.AccessInfo createChangeSubMSISDNReqMsgAccessInfo() {
		return new ChangeSubMSISDNReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link ChangeSubSIMRspMsg }
	 * 
	 */
	public ChangeSubSIMRspMsg createChangeSubSIMRspMsg() {
		return new ChangeSubSIMRspMsg();
	}

	/**
	 * Create an instance of {@link ChangePromotionReqMsg.Promotion }
	 * 
	 */
	public ChangePromotionReqMsg.Promotion createChangePromotionReqMsgPromotion() {
		return new ChangePromotionReqMsg.Promotion();
	}

	/**
	 * Create an instance of {@link ChangeSubStatusRspMsg }
	 * 
	 */
	public ChangeSubStatusRspMsg createChangeSubStatusRspMsg() {
		return new ChangeSubStatusRspMsg();
	}

	/**
	 * Create an instance of {@link ActivateSubReqMsg }
	 * 
	 */
	public ActivateSubReqMsg createActivateSubReqMsg() {
		return new ActivateSubReqMsg();
	}

	/**
	 * Create an instance of {@link ReactivateSubReqMsg }
	 * 
	 */
	public ReactivateSubReqMsg createReactivateSubReqMsg() {
		return new ReactivateSubReqMsg();
	}

	/**
	 * Create an instance of {@link QuerySubStatusRspMsg }
	 * 
	 */
	public QuerySubStatusRspMsg createQuerySubStatusRspMsg() {
		return new QuerySubStatusRspMsg();
	}

	/**
	 * Create an instance of {@link ChangeSubStatusReqMsg }
	 * 
	 */
	public ChangeSubStatusReqMsg createChangeSubStatusReqMsg() {
		return new ChangeSubStatusReqMsg();
	}

	/**
	 * Create an instance of {@link PreDeactivateSubRspMsg }
	 * 
	 */
	public PreDeactivateSubRspMsg createPreDeactivateSubRspMsg() {
		return new PreDeactivateSubRspMsg();
	}

	/**
	 * Create an instance of {@link DeactivateSubReqMsg.AccessInfo }
	 * 
	 */
	public DeactivateSubReqMsg.AccessInfo createDeactivateSubReqMsgAccessInfo() {
		return new DeactivateSubReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link AuthenticateSubReqMsg }
	 * 
	 */
	public AuthenticateSubReqMsg createAuthenticateSubReqMsg() {
		return new AuthenticateSubReqMsg();
	}

	/**
	 * Create an instance of {@link CancelLostRspMsg }
	 * 
	 */
	public CancelLostRspMsg createCancelLostRspMsg() {
		return new CancelLostRspMsg();
	}

	/**
	 * Create an instance of {@link CancelLostReqMsg.AccessInfo }
	 * 
	 */
	public CancelLostReqMsg.AccessInfo createCancelLostReqMsgAccessInfo() {
		return new CancelLostReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link CancelLostReqMsg.ChangeSIMInfo }
	 * 
	 */
	public CancelLostReqMsg.ChangeSIMInfo createCancelLostReqMsgChangeSIMInfo() {
		return new CancelLostReqMsg.ChangeSIMInfo();
	}

	/**
	 * Create an instance of {@link ActivateSubRspMsg }
	 * 
	 */
	public ActivateSubRspMsg createActivateSubRspMsg() {
		return new ActivateSubRspMsg();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerReqMsg.AccessInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.AccessInfo createChangeSubOwnerReqMsgAccessInfo() {
		return new ChangeSubOwnerReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerReqMsg.NewSubInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewSubInfo createChangeSubOwnerReqMsgNewSubInfo() {
		return new ChangeSubOwnerReqMsg.NewSubInfo();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerReqMsg.PasswordInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.PasswordInfo createChangeSubOwnerReqMsgPasswordInfo() {
		return new ChangeSubOwnerReqMsg.PasswordInfo();
	}

	/**
	 * Create an instance of {@link AssignAcctToSubRspMsg }
	 * 
	 */
	public AssignAcctToSubRspMsg createAssignAcctToSubRspMsg() {
		return new AssignAcctToSubRspMsg();
	}

	/**
	 * Create an instance of {@link ChangeSubMSISDNRspMsg }
	 * 
	 */
	public ChangeSubMSISDNRspMsg createChangeSubMSISDNRspMsg() {
		return new ChangeSubMSISDNRspMsg();
	}

	/**
	 * Create an instance of {@link QueryHandsetInstallmentRspMsg }
	 * 
	 */
	public QueryHandsetInstallmentRspMsg createQueryHandsetInstallmentRspMsg() {
		return new QueryHandsetInstallmentRspMsg();
	}

	/**
	 * Create an instance of {@link ChangeRechargeBlacklistReqMsg.AccessInfo }
	 * 
	 */
	public ChangeRechargeBlacklistReqMsg.AccessInfo createChangeRechargeBlacklistReqMsgAccessInfo() {
		return new ChangeRechargeBlacklistReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link ChangeSubSIMReqMsg.AccessInfo }
	 * 
	 */
	public ChangeSubSIMReqMsg.AccessInfo createChangeSubSIMReqMsgAccessInfo() {
		return new ChangeSubSIMReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link ChangePromotionRspMsg }
	 * 
	 */
	public ChangePromotionRspMsg createChangePromotionRspMsg() {
		return new ChangePromotionRspMsg();
	}

	/**
	 * Create an instance of {@link ReportLostReqMsg.AccessInfo }
	 * 
	 */
	public ReportLostReqMsg.AccessInfo createReportLostReqMsgAccessInfo() {
		return new ReportLostReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of
	 * {@link ChangePaymentRelationReqMsg.PaymentRelation }
	 * 
	 */
	public ChangePaymentRelationReqMsg.PaymentRelation createChangePaymentRelationReqMsgPaymentRelation() {
		return new ChangePaymentRelationReqMsg.PaymentRelation();
	}

	/**
	 * Create an instance of {@link QueryEntityIdsReqMsg }
	 * 
	 */
	public QueryEntityIdsReqMsg createQueryEntityIdsReqMsg() {
		return new QueryEntityIdsReqMsg();
	}

	/**
	 * Create an instance of {@link ChangeRechargeBlacklistRspMsg }
	 * 
	 */
	public ChangeRechargeBlacklistRspMsg createChangeRechargeBlacklistRspMsg() {
		return new ChangeRechargeBlacklistRspMsg();
	}

	/**
	 * Create an instance of {@link QuerySubInfoReqMsg }
	 * 
	 */
	public QuerySubInfoReqMsg createQuerySubInfoReqMsg() {
		return new QuerySubInfoReqMsg();
	}

	/**
	 * Create an instance of {@link ReactivateSubRspMsg }
	 * 
	 */
	public ReactivateSubRspMsg createReactivateSubRspMsg() {
		return new ReactivateSubRspMsg();
	}

	/**
	 * Create an instance of {@link AssignAcctToSubReqMsg.AccessInfo }
	 * 
	 */
	public AssignAcctToSubReqMsg.AccessInfo createAssignAcctToSubReqMsgAccessInfo() {
		return new AssignAcctToSubReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link QueryEntityIdsRspMsg.EntityId }
	 * 
	 */
	public QueryEntityIdsRspMsg.EntityId createQueryEntityIdsRspMsgEntityId() {
		return new QueryEntityIdsRspMsg.EntityId();
	}

	/**
	 * Create an instance of {@link PreDeactivateSubReqMsg.AccessInfo }
	 * 
	 */
	public PreDeactivateSubReqMsg.AccessInfo createPreDeactivateSubReqMsgAccessInfo() {
		return new PreDeactivateSubReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link ChangeConsumptionLimitRspMsg }
	 * 
	 */
	public ChangeConsumptionLimitRspMsg createChangeConsumptionLimitRspMsg() {
		return new ChangeConsumptionLimitRspMsg();
	}

	/**
	 * Create an instance of {@link QuerySubInfoRspMsg.Subscriber }
	 * 
	 */
	public QuerySubInfoRspMsg.Subscriber createQuerySubInfoRspMsgSubscriber() {
		return new QuerySubInfoRspMsg.Subscriber();
	}

	/**
	 * Create an instance of {@link ChangeSubPasswordRspMsg }
	 * 
	 */
	public ChangeSubPasswordRspMsg createChangeSubPasswordRspMsg() {
		return new ChangeSubPasswordRspMsg();
	}

	/**
	 * Create an instance of {@link QuerySubStatusReqMsg }
	 * 
	 */
	public QuerySubStatusReqMsg createQuerySubStatusReqMsg() {
		return new QuerySubStatusReqMsg();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg.AccessInfo }
	 * 
	 */
	public SupplementCustInfoReqMsg.AccessInfo createSupplementCustInfoReqMsgAccessInfo() {
		return new SupplementCustInfoReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg.ExistingCust }
	 * 
	 */
	public SupplementCustInfoReqMsg.ExistingCust createSupplementCustInfoReqMsgExistingCust() {
		return new SupplementCustInfoReqMsg.ExistingCust();
	}

	/**
	 * Create an instance of {@link ChangeSubInfoReqMsg.Subscriber }
	 * 
	 */
	public ChangeSubInfoReqMsg.Subscriber createChangeSubInfoReqMsgSubscriber() {
		return new ChangeSubInfoReqMsg.Subscriber();
	}

	/**
	 * Create an instance of {@link QueryHandsetInstallmentReqMsg }
	 * 
	 */
	public QueryHandsetInstallmentReqMsg createQueryHandsetInstallmentReqMsg() {
		return new QueryHandsetInstallmentReqMsg();
	}

	/**
	 * Create an instance of {@link ChangePaymentRelationRspMsg }
	 * 
	 */
	public ChangePaymentRelationRspMsg createChangePaymentRelationRspMsg() {
		return new ChangePaymentRelationRspMsg();
	}

	/**
	 * Create an instance of {@link QuerySubLifeCycleRspMsg }
	 * 
	 */
	public QuerySubLifeCycleRspMsg createQuerySubLifeCycleRspMsg() {
		return new QuerySubLifeCycleRspMsg();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoRspMsg }
	 * 
	 */
	public SupplementCustInfoRspMsg createSupplementCustInfoRspMsg() {
		return new SupplementCustInfoRspMsg();
	}

	/**
	 * Create an instance of {@link DeactivateSubRspMsg }
	 * 
	 */
	public DeactivateSubRspMsg createDeactivateSubRspMsg() {
		return new DeactivateSubRspMsg();
	}

	/**
	 * Create an instance of {@link ChangeConsumptionLimitReqMsg.Consumption }
	 * 
	 */
	public ChangeConsumptionLimitReqMsg.Consumption createChangeConsumptionLimitReqMsgConsumption() {
		return new ChangeConsumptionLimitReqMsg.Consumption();
	}

	/**
	 * Create an instance of {@link ChangeSubPasswordReqMsg }
	 * 
	 */
	public ChangeSubPasswordReqMsg createChangeSubPasswordReqMsg() {
		return new ChangeSubPasswordReqMsg();
	}

	/**
	 * Create an instance of {@link AuthenticateSubRspMsg }
	 * 
	 */
	public AuthenticateSubRspMsg createAuthenticateSubRspMsg() {
		return new AuthenticateSubRspMsg();
	}

	/**
	 * Create an instance of {@link QuerySubLifeCycleReqMsg }
	 * 
	 */
	public QuerySubLifeCycleReqMsg createQuerySubLifeCycleReqMsg() {
		return new QuerySubLifeCycleReqMsg();
	}

	/**
	 * Create an instance of {@link ChangePrepaidToPostpaidReqMsg.AccessInfo }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.AccessInfo createChangePrepaidToPostpaidReqMsgAccessInfo() {
		return new ChangePrepaidToPostpaidReqMsg.AccessInfo();
	}

	/**
	 * Create an instance of {@link ChangeSubInfoRspMsg }
	 * 
	 */
	public ChangeSubInfoRspMsg createChangeSubInfoRspMsg() {
		return new ChangeSubInfoRspMsg();
	}

	/**
	 * Create an instance of {@link ReportLostRspMsg }
	 * 
	 */
	public ReportLostRspMsg createReportLostRspMsg() {
		return new ReportLostRspMsg();
	}

	/**
	 * Create an instance of {@link ChangePrepaidToPostpaidRspMsg }
	 * 
	 */
	public ChangePrepaidToPostpaidRspMsg createChangePrepaidToPostpaidRspMsg() {
		return new ChangePrepaidToPostpaidRspMsg();
	}

	/**
	 * Create an instance of
	 * {@link ChangePrepaidToPostpaidReqMsg.Offering.NewPrimaryOffering }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.Offering.NewPrimaryOffering createChangePrepaidToPostpaidReqMsgOfferingNewPrimaryOffering() {
		return new ChangePrepaidToPostpaidReqMsg.Offering.NewPrimaryOffering();
	}

	/**
	 * Create an instance of
	 * {@link ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account createChangePrepaidToPostpaidReqMsgPostpaidAcctAccount() {
		return new ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account();
	}

	/**
	 * Create an instance of
	 * {@link ChangePrepaidToPostpaidReqMsg.PostpaidCust.Contact }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.PostpaidCust.Contact createChangePrepaidToPostpaidReqMsgPostpaidCustContact() {
		return new ChangePrepaidToPostpaidReqMsg.PostpaidCust.Contact();
	}

	/**
	 * Create an instance of
	 * {@link ChangePrepaidToPostpaidReqMsg.PostpaidCust.Address }
	 * 
	 */
	public ChangePrepaidToPostpaidReqMsg.PostpaidCust.Address createChangePrepaidToPostpaidReqMsgPostpaidCustAddress() {
		return new ChangePrepaidToPostpaidReqMsg.PostpaidCust.Address();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg.Account.Contact }
	 * 
	 */
	public SupplementCustInfoReqMsg.Account.Contact createSupplementCustInfoReqMsgAccountContact() {
		return new SupplementCustInfoReqMsg.Account.Contact();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg.Account.Address }
	 * 
	 */
	public SupplementCustInfoReqMsg.Account.Address createSupplementCustInfoReqMsgAccountAddress() {
		return new SupplementCustInfoReqMsg.Account.Address();
	}

	/**
	 * Create an instance of
	 * {@link SupplementCustInfoReqMsg.Account.PaymentChannel }
	 * 
	 */
	public SupplementCustInfoReqMsg.Account.PaymentChannel createSupplementCustInfoReqMsgAccountPaymentChannel() {
		return new SupplementCustInfoReqMsg.Account.PaymentChannel();
	}

	/**
	 * Create an instance of
	 * {@link SupplementCustInfoReqMsg.Account.BillMedium }
	 * 
	 */
	public SupplementCustInfoReqMsg.Account.BillMedium createSupplementCustInfoReqMsgAccountBillMedium() {
		return new SupplementCustInfoReqMsg.Account.BillMedium();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg.Customer.Contact }
	 * 
	 */
	public SupplementCustInfoReqMsg.Customer.Contact createSupplementCustInfoReqMsgCustomerContact() {
		return new SupplementCustInfoReqMsg.Customer.Contact();
	}

	/**
	 * Create an instance of {@link SupplementCustInfoReqMsg.Customer.Address }
	 * 
	 */
	public SupplementCustInfoReqMsg.Customer.Address createSupplementCustInfoReqMsgCustomerAddress() {
		return new SupplementCustInfoReqMsg.Customer.Address();
	}

	/**
	 * Create an instance of {@link AssignAcctToSubReqMsg.AcctInfo.Account }
	 * 
	 */
	public AssignAcctToSubReqMsg.AcctInfo.Account createAssignAcctToSubReqMsgAcctInfoAccount() {
		return new AssignAcctToSubReqMsg.AcctInfo.Account();
	}

	/**
	 * Create an instance of {@link ChangeSubOwnerReqMsg.NewAcctInfo.Account }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewAcctInfo.Account createChangeSubOwnerReqMsgNewAcctInfoAccount() {
		return new ChangeSubOwnerReqMsg.NewAcctInfo.Account();
	}

	/**
	 * Create an instance of
	 * {@link ChangeSubOwnerReqMsg.NewCustInfo.Customer.NoticeSuppress }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewCustInfo.Customer.NoticeSuppress createChangeSubOwnerReqMsgNewCustInfoCustomerNoticeSuppress() {
		return new ChangeSubOwnerReqMsg.NewCustInfo.Customer.NoticeSuppress();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Nationality", scope = SupplementCustInfoReqMsg.Customer.class)
	public JAXBElement<String> createSupplementCustInfoReqMsgCustomerNationality(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerNationality_QNAME, String.class,
				SupplementCustInfoReqMsg.Customer.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Title", scope = SupplementCustInfoReqMsg.Customer.class)
	public JAXBElement<String> createSupplementCustInfoReqMsgCustomerTitle(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerTitle_QNAME, String.class,
				SupplementCustInfoReqMsg.Customer.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Title", scope = ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account.class)
	public JAXBElement<String> createChangePrepaidToPostpaidReqMsgPostpaidAcctAccountTitle(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerTitle_QNAME, String.class,
				ChangePrepaidToPostpaidReqMsg.PostpaidAcct.Account.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Title", scope = ChangeSubOwnerReqMsg.NewAcctInfo.Account.class)
	public JAXBElement<String> createChangeSubOwnerReqMsgNewAcctInfoAccountTitle(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerTitle_QNAME, String.class,
				ChangeSubOwnerReqMsg.NewAcctInfo.Account.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Title", scope = SupplementCustInfoReqMsg.Account.class)
	public JAXBElement<String> createSupplementCustInfoReqMsgAccountTitle(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerTitle_QNAME, String.class,
				SupplementCustInfoReqMsg.Account.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Nationality", scope = ChangePrepaidToPostpaidReqMsg.PostpaidCust.class)
	public JAXBElement<String> createChangePrepaidToPostpaidReqMsgPostpaidCustNationality(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerNationality_QNAME, String.class,
				ChangePrepaidToPostpaidReqMsg.PostpaidCust.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Title", scope = ChangePrepaidToPostpaidReqMsg.PostpaidCust.class)
	public JAXBElement<String> createChangePrepaidToPostpaidReqMsgPostpaidCustTitle(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerTitle_QNAME, String.class,
				ChangePrepaidToPostpaidReqMsg.PostpaidCust.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Title", scope = AssignAcctToSubReqMsg.AcctInfo.Account.class)
	public JAXBElement<String> createAssignAcctToSubReqMsgAcctInfoAccountTitle(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerTitle_QNAME, String.class,
				AssignAcctToSubReqMsg.AcctInfo.Account.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Nationality", scope = ChangeSubOwnerReqMsg.NewCustInfo.Customer.class)
	public JAXBElement<String> createChangeSubOwnerReqMsgNewCustInfoCustomerNationality(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerNationality_QNAME, String.class,
				ChangeSubOwnerReqMsg.NewCustInfo.Customer.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Title", scope = ChangeSubOwnerReqMsg.NewCustInfo.Customer.class)
	public JAXBElement<String> createChangeSubOwnerReqMsgNewCustInfoCustomerTitle(String value) {
		return new JAXBElement<String>(_SupplementCustInfoReqMsgCustomerTitle_QNAME, String.class,
				ChangeSubOwnerReqMsg.NewCustInfo.Customer.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Certificate
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", name = "Certificate", scope = ChangeSubOwnerReqMsg.NewCustInfo.Customer.class)
	public JAXBElement<Certificate> createChangeSubOwnerReqMsgNewCustInfoCustomerCertificate(Certificate value) {
		return new JAXBElement<Certificate>(_ChangeSubOwnerReqMsgNewCustInfoCustomerCertificate_QNAME,
				Certificate.class, ChangeSubOwnerReqMsg.NewCustInfo.Customer.class, value);
	}
}
