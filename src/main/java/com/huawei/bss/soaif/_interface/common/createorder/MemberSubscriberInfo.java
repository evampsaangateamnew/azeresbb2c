
package com.huawei.bss.soaif._interface.common.createorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MemberSubscriberInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MemberSubscriberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberSubsId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="MemberServiceNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MemberType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OperateType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SupplementaryOffering" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OfferingInstance" type="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance"/>
 *                   &lt;element name="EffectiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}EffectiveMode"/>
 *                   &lt;element name="ExpirationDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *                   &lt;element name="ActiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}ActiveMode"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MemberSubscriberInfo", propOrder = {
    "memberSubsId",
    "memberServiceNumber",
    "memberType",
    "operateType",
    "supplementaryOffering",
    "additionalProperty"
})
public class MemberSubscriberInfo {

    @XmlElement(name = "MemberSubsId")
    protected Long memberSubsId;
    @XmlElement(name = "MemberServiceNumber")
    protected String memberServiceNumber;
    @XmlElement(name = "MemberType")
    protected String memberType;
    @XmlElement(name = "OperateType")
    protected String operateType;
    @XmlElement(name = "SupplementaryOffering")
    protected List<MemberSubscriberInfo.SupplementaryOffering> supplementaryOffering;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;

    /**
     * Gets the value of the memberSubsId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMemberSubsId() {
        return memberSubsId;
    }

    /**
     * Sets the value of the memberSubsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMemberSubsId(Long value) {
        this.memberSubsId = value;
    }

    /**
     * Gets the value of the memberServiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberServiceNumber() {
        return memberServiceNumber;
    }

    /**
     * Sets the value of the memberServiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberServiceNumber(String value) {
        this.memberServiceNumber = value;
    }

    /**
     * Gets the value of the memberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberType() {
        return memberType;
    }

    /**
     * Sets the value of the memberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberType(String value) {
        this.memberType = value;
    }

    /**
     * Gets the value of the operateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperateType() {
        return operateType;
    }

    /**
     * Sets the value of the operateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperateType(String value) {
        this.operateType = value;
    }

    /**
     * Gets the value of the supplementaryOffering property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryOffering property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryOffering().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MemberSubscriberInfo.SupplementaryOffering }
     * 
     * 
     */
    public List<MemberSubscriberInfo.SupplementaryOffering> getSupplementaryOffering() {
        if (supplementaryOffering == null) {
            supplementaryOffering = new ArrayList<MemberSubscriberInfo.SupplementaryOffering>();
        }
        return this.supplementaryOffering;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OfferingInstance" type="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstance"/>
     *         &lt;element name="EffectiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}EffectiveMode"/>
     *         &lt;element name="ExpirationDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
     *         &lt;element name="ActiveMode" type="{http://www.huawei.com/bss/soaif/interface/common/}ActiveMode"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offeringInstance",
        "effectiveMode",
        "expirationDate",
        "activeMode"
    })
    public static class SupplementaryOffering {

        @XmlElement(name = "OfferingInstance", required = true)
        protected OfferingInstance offeringInstance;
        @XmlElement(name = "EffectiveMode", required = true)
        protected EffectiveMode effectiveMode;
        @XmlElement(name = "ExpirationDate", required = true)
        protected String expirationDate;
        @XmlElement(name = "ActiveMode", required = true)
        protected ActiveMode activeMode;

        /**
         * Gets the value of the offeringInstance property.
         * 
         * @return
         *     possible object is
         *     {@link OfferingInstance }
         *     
         */
        public OfferingInstance getOfferingInstance() {
            return offeringInstance;
        }

        /**
         * Sets the value of the offeringInstance property.
         * 
         * @param value
         *     allowed object is
         *     {@link OfferingInstance }
         *     
         */
        public void setOfferingInstance(OfferingInstance value) {
            this.offeringInstance = value;
        }

        /**
         * Gets the value of the effectiveMode property.
         * 
         * @return
         *     possible object is
         *     {@link EffectiveMode }
         *     
         */
        public EffectiveMode getEffectiveMode() {
            return effectiveMode;
        }

        /**
         * Sets the value of the effectiveMode property.
         * 
         * @param value
         *     allowed object is
         *     {@link EffectiveMode }
         *     
         */
        public void setEffectiveMode(EffectiveMode value) {
            this.effectiveMode = value;
        }

        /**
         * Gets the value of the expirationDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationDate() {
            return expirationDate;
        }

        /**
         * Sets the value of the expirationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationDate(String value) {
            this.expirationDate = value;
        }

        /**
         * Gets the value of the activeMode property.
         * 
         * @return
         *     possible object is
         *     {@link ActiveMode }
         *     
         */
        public ActiveMode getActiveMode() {
            return activeMode;
        }

        /**
         * Sets the value of the activeMode property.
         * 
         * @param value
         *     allowed object is
         *     {@link ActiveMode }
         *     
         */
        public void setActiveMode(ActiveMode value) {
            this.activeMode = value;
        }

    }

}
