package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for queryBalanceIn complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="queryBalanceIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="PrimaryIdentity" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryBalanceIn", propOrder = {})
public class QueryBalanceIn {
	@XmlElement(name = "PrimaryIdentity", required = true)
	protected String primaryIdentity;

	/**
	 * Gets the value of the primaryIdentity property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPrimaryIdentity() {
		return primaryIdentity;
	}

	/**
	 * Sets the value of the primaryIdentity property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPrimaryIdentity(String value) {
		this.primaryIdentity = value;
	}
}
