package com.huawei.bss.soaif._interface.subscriberservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="ChangeSIMInfo" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="OldICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}ICCID" minOccurs="0"/>
 *                     &lt;element name="NewICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}ICCID"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "changeSIMInfo" })
@XmlRootElement(name = "CancelLostReqMsg")
public class CancelLostReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected CancelLostReqMsg.AccessInfo accessInfo;
	@XmlElement(name = "ChangeSIMInfo")
	protected CancelLostReqMsg.ChangeSIMInfo changeSIMInfo;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is {@link CancelLostReqMsg.AccessInfo }
	 * 
	 */
	public CancelLostReqMsg.AccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link CancelLostReqMsg.AccessInfo }
	 * 
	 */
	public void setAccessInfo(CancelLostReqMsg.AccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the changeSIMInfo property.
	 * 
	 * @return possible object is {@link CancelLostReqMsg.ChangeSIMInfo }
	 * 
	 */
	public CancelLostReqMsg.ChangeSIMInfo getChangeSIMInfo() {
		return changeSIMInfo;
	}

	/**
	 * Sets the value of the changeSIMInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link CancelLostReqMsg.ChangeSIMInfo }
	 * 
	 */
	public void setChangeSIMInfo(CancelLostReqMsg.ChangeSIMInfo value) {
		this.changeSIMInfo = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
	 *     &lt;/extension>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class AccessInfo extends ObjectAccessInfo {
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="OldICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}ICCID" minOccurs="0"/>
	 *         &lt;element name="NewICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}ICCID"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "oldICCID", "newICCID" })
	public static class ChangeSIMInfo {
		@XmlElement(name = "OldICCID")
		protected String oldICCID;
		@XmlElement(name = "NewICCID", required = true)
		protected String newICCID;

		/**
		 * Gets the value of the oldICCID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getOldICCID() {
			return oldICCID;
		}

		/**
		 * Sets the value of the oldICCID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setOldICCID(String value) {
			this.oldICCID = value;
		}

		/**
		 * Gets the value of the newICCID property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getNewICCID() {
			return newICCID;
		}

		/**
		 * Sets the value of the newICCID property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setNewICCID(String value) {
			this.newICCID = value;
		}
	}
}
