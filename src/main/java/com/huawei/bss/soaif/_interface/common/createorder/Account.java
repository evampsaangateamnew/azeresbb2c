
package com.huawei.bss.soaif._interface.common.createorder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Account complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Account">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId" minOccurs="0"/>
 *         &lt;element name="AcctName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="40"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *         &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
 *         &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
 *         &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
 *         &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="AcctPayMethod" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctPayMethod" minOccurs="0"/>
 *         &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Account", propOrder = {
    "acctId",
    "acctName",
    "paymentType",
    "title",
    "name",
    "billCycleType",
    "billLanguage",
    "address",
    "billMedium",
    "currency",
    "initialBalance",
    "creditLimit",
    "acctPayMethod",
    "paymentChannel",
    "additionalProperty"
})
public class Account {

    @XmlElement(name = "AcctId")
    protected String acctId;
    @XmlElement(name = "AcctName")
    protected String acctName;
    @XmlElement(name = "PaymentType")
    protected String paymentType;
    @XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/common/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElement(name = "Name")
    protected Name name;
    @XmlElement(name = "BillCycleType")
    protected String billCycleType;
    @XmlElement(name = "BillLanguage")
    protected String billLanguage;
    @XmlElement(name = "Address")
    protected List<Address> address;
    @XmlElement(name = "BillMedium")
    protected List<BillMedium> billMedium;
    @XmlElement(name = "Currency")
    protected BigInteger currency;
    @XmlElement(name = "InitialBalance")
    protected String initialBalance;
    @XmlElement(name = "CreditLimit")
    protected List<CreditLimit> creditLimit;
    @XmlElement(name = "AcctPayMethod")
    protected String acctPayMethod;
    @XmlElement(name = "PaymentChannel")
    protected List<PaymentChannel> paymentChannel;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;

    /**
     * Gets the value of the acctId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctId() {
        return acctId;
    }

    /**
     * Sets the value of the acctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctId(String value) {
        this.acctId = value;
    }

    /**
     * Gets the value of the acctName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctName() {
        return acctName;
    }

    /**
     * Sets the value of the acctName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctName(String value) {
        this.acctName = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setName(Name value) {
        this.name = value;
    }

    /**
     * Gets the value of the billCycleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleType() {
        return billCycleType;
    }

    /**
     * Sets the value of the billCycleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleType(String value) {
        this.billCycleType = value;
    }

    /**
     * Gets the value of the billLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillLanguage() {
        return billLanguage;
    }

    /**
     * Sets the value of the billLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillLanguage(String value) {
        this.billLanguage = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the address property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Address }
     * 
     * 
     */
    public List<Address> getAddress() {
        if (address == null) {
            address = new ArrayList<Address>();
        }
        return this.address;
    }

    /**
     * Gets the value of the billMedium property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the billMedium property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBillMedium().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BillMedium }
     * 
     * 
     */
    public List<BillMedium> getBillMedium() {
        if (billMedium == null) {
            billMedium = new ArrayList<BillMedium>();
        }
        return this.billMedium;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrency(BigInteger value) {
        this.currency = value;
    }

    /**
     * Gets the value of the initialBalance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitialBalance() {
        return initialBalance;
    }

    /**
     * Sets the value of the initialBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitialBalance(String value) {
        this.initialBalance = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditLimit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditLimit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreditLimit }
     * 
     * 
     */
    public List<CreditLimit> getCreditLimit() {
        if (creditLimit == null) {
            creditLimit = new ArrayList<CreditLimit>();
        }
        return this.creditLimit;
    }

    /**
     * Gets the value of the acctPayMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctPayMethod() {
        return acctPayMethod;
    }

    /**
     * Sets the value of the acctPayMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctPayMethod(String value) {
        this.acctPayMethod = value;
    }

    /**
     * Gets the value of the paymentChannel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentChannel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentChannel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentChannel }
     * 
     * 
     */
    public List<PaymentChannel> getPaymentChannel() {
        if (paymentChannel == null) {
            paymentChannel = new ArrayList<PaymentChannel>();
        }
        return this.paymentChannel;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }

}
