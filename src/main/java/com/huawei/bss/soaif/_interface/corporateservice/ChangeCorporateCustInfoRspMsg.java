
package com.huawei.bss.soaif._interface.corporateservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.bss.soaif._interface.common.querycorporate.RspHeader;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;element name="OrderId" type="{http://www.huawei.com/bss/soaif/interface/common/}OrderId" minOccurs="0"/>
 *         &lt;element name="ExternalOrderId" type="{http://www.huawei.com/bss/soaif/interface/common/}ExternalOrderId" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rspHeader",
    "orderId",
    "externalOrderId"
})
@XmlRootElement(name = "ChangeCorporateCustInfoRspMsg")
public class ChangeCorporateCustInfoRspMsg {

    @XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
    protected RspHeader rspHeader;
    @XmlElement(name = "OrderId")
    protected String orderId;
    @XmlElement(name = "ExternalOrderId")
    protected String externalOrderId;

    /**
     * Gets the value of the rspHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RspHeader }
     *     
     */
    public RspHeader getRspHeader() {
        return rspHeader;
    }

    /**
     * Sets the value of the rspHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RspHeader }
     *     
     */
    public void setRspHeader(RspHeader value) {
        this.rspHeader = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the externalOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalOrderId() {
        return externalOrderId;
    }

    /**
     * Sets the value of the externalOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalOrderId(String value) {
        this.externalOrderId = value;
    }

}
