package com.huawei.bss.soaif._interface.hlrwebservice;

import com.evampsaanga.developer.utils.SoapHandlerService;

public class HLRWebService {
	private static HLRWEBPortType portType = null;

	private HLRWebService() {
	}

	public static synchronized HLRWEBPortType getInstance() {
		if (portType == null) {
			portType = new HLRWEBServices().getHLRWEBPort();
			SoapHandlerService.configureBinding(portType);
			// ((BindingProvider)
			// portType).getRequestContext().put("javax.xml.ws.client.receiveTimeout",
			// "1000");
			// /Set timeout until a connection is established
			// ((BindingProvider)port).getRequestContext().put("javax.xml.ws.client.connectionTimeout",
			// "6000");
			// Set timeout until the response is received
			// ((BindingProvider)
			// port).getRequestContext().put("javax.xml.ws.client.receiveTimeout",
			// "1000");
		}
		return portType;
	}
}
