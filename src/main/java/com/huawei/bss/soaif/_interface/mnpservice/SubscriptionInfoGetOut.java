package com.huawei.bss.soaif._interface.mnpservice;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Subscription_Info_GetOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Subscription_Info_GetOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FirstName" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="Surname" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="FatherName" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="BirthDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="PlaceOfBirth" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="PreviousLastName" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="ContactName" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="ContactPhoneNumber1" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="ContactPhoneNumber2" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="ContactPhoneNumber3" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="FaxNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="SubscriberType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="SubscriptionType" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="BilledBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}decimal" minOccurs="0"/>
 *         &lt;element name="Non-billedBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}decimal" minOccurs="0"/>
 *         &lt;element name="TotalDebt" type="{http://www.huawei.com/bss/soaif/interface/common/}decimal" minOccurs="0"/>
 *         &lt;element name="SubscriberStatus" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="ActivationDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="ICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="IMSI" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="COSID" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="LanguageID" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="TaxID" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="CorporateIndicator" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="CorporateIDGroup" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="IDType" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="IDNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="PINNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="IssuedAuthority" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="DateofGrant" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="CompanyName" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="CorporateContactPerson" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="SubscriberIndicator" type="{http://www.huawei.com/bss/soaif/interface/common/}int" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Subscription_Info_GetOut", propOrder = { "firstName", "surname", "fatherName", "birthDate",
		"placeOfBirth", "previousLastName", "contactName", "contactPhoneNumber1", "contactPhoneNumber2",
		"contactPhoneNumber3", "faxNumber", "subscriberType", "subscriptionType", "billedBalance", "nonBilledBalance",
		"totalDebt", "subscriberStatus", "activationDate", "iccid", "imsi", "cosid", "languageID", "taxID",
		"corporateIndicator", "corporateIDGroup", "idType", "idNumber", "pinNumber", "issuedAuthority", "dateofGrant",
		"companyName", "city", "corporateContactPerson", "subscriberIndicator", "address", "custID" })
public class SubscriptionInfoGetOut {
	@XmlElement(name = "FirstName")
	protected String firstName;
	@XmlElement(name = "Surname")
	protected String surname;
	@XmlElement(name = "FatherName")
	protected String fatherName;
	@XmlElement(name = "BirthDate")
	protected String birthDate;
	@XmlElement(name = "PlaceOfBirth")
	protected String placeOfBirth;
	@XmlElement(name = "PreviousLastName")
	protected String previousLastName;
	@XmlElement(name = "ContactName")
	protected String contactName;
	@XmlElement(name = "ContactPhoneNumber1")
	protected String contactPhoneNumber1;
	@XmlElement(name = "ContactPhoneNumber2")
	protected String contactPhoneNumber2;
	@XmlElement(name = "ContactPhoneNumber3")
	protected String contactPhoneNumber3;
	@XmlElement(name = "FaxNumber")
	protected String faxNumber;
	@XmlElement(name = "SubscriberType")
	protected String subscriberType;
	@XmlElement(name = "SubscriptionType")
	protected BigInteger subscriptionType;
	@XmlElement(name = "BilledBalance")
	protected BigDecimal billedBalance;
	@XmlElement(name = "Non-billedBalance")
	protected BigDecimal nonBilledBalance;
	@XmlElement(name = "TotalDebt")
	protected BigDecimal totalDebt;
	@XmlElement(name = "SubscriberStatus")
	protected String subscriberStatus;
	@XmlElement(name = "ActivationDate")
	protected String activationDate;
	@XmlElement(name = "ICCID")
	protected String iccid;
	@XmlElement(name = "IMSI")
	protected String imsi;
	@XmlElement(name = "COSID")
	protected BigInteger cosid;
	@XmlElement(name = "LanguageID")
	protected BigInteger languageID;
	@XmlElement(name = "TaxID")
	protected BigInteger taxID;
	@XmlElement(name = "CorporateIndicator")
	protected BigInteger corporateIndicator;
	@XmlElement(name = "CorporateIDGroup")
	protected BigInteger corporateIDGroup;
	@XmlElement(name = "IDType")
	protected BigInteger idType;
	@XmlElement(name = "IDNumber")
	protected String idNumber;
	@XmlElement(name = "PINNumber")
	protected String pinNumber;
	@XmlElement(name = "IssuedAuthority")
	protected String issuedAuthority;
	@XmlElement(name = "DateofGrant")
	protected String dateofGrant;
	@XmlElement(name = "CompanyName")
	protected String companyName;
	@XmlElement(name = "City")
	protected String city;
	@XmlElement(name = "CorporateContactPerson")
	protected String corporateContactPerson;
	@XmlElement(name = "SubscriberIndicator")
	protected BigInteger subscriberIndicator;
	@XmlElement(name = "Address")
	protected String address;
	@XmlElement(name = "custID")
	protected String custID;

	public String getCustID() {
		return custID;
	}

	public void setCustID(String custID) {
		this.custID = custID;
	}

	/**
	 * Gets the value of the firstName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the value of the firstName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFirstName(String value) {
		this.firstName = value;
	}

	/**
	 * Gets the value of the surname property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Sets the value of the surname property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSurname(String value) {
		this.surname = value;
	}

	/**
	 * Gets the value of the fatherName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFatherName() {
		return fatherName;
	}

	/**
	 * Sets the value of the fatherName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFatherName(String value) {
		this.fatherName = value;
	}

	/**
	 * Gets the value of the birthDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBirthDate() {
		return birthDate;
	}

	/**
	 * Sets the value of the birthDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBirthDate(String value) {
		this.birthDate = value;
	}

	/**
	 * Gets the value of the placeOfBirth property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	/**
	 * Sets the value of the placeOfBirth property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPlaceOfBirth(String value) {
		this.placeOfBirth = value;
	}

	/**
	 * Gets the value of the previousLastName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPreviousLastName() {
		return previousLastName;
	}

	/**
	 * Sets the value of the previousLastName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPreviousLastName(String value) {
		this.previousLastName = value;
	}

	/**
	 * Gets the value of the contactName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * Sets the value of the contactName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactName(String value) {
		this.contactName = value;
	}

	/**
	 * Gets the value of the contactPhoneNumber1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactPhoneNumber1() {
		return contactPhoneNumber1;
	}

	/**
	 * Sets the value of the contactPhoneNumber1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactPhoneNumber1(String value) {
		this.contactPhoneNumber1 = value;
	}

	/**
	 * Gets the value of the contactPhoneNumber2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactPhoneNumber2() {
		return contactPhoneNumber2;
	}

	/**
	 * Sets the value of the contactPhoneNumber2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactPhoneNumber2(String value) {
		this.contactPhoneNumber2 = value;
	}

	/**
	 * Gets the value of the contactPhoneNumber3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactPhoneNumber3() {
		return contactPhoneNumber3;
	}

	/**
	 * Sets the value of the contactPhoneNumber3 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactPhoneNumber3(String value) {
		this.contactPhoneNumber3 = value;
	}

	/**
	 * Gets the value of the faxNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * Sets the value of the faxNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFaxNumber(String value) {
		this.faxNumber = value;
	}

	/**
	 * Gets the value of the subscriberType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubscriberType() {
		return subscriberType;
	}

	/**
	 * Sets the value of the subscriberType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubscriberType(String value) {
		this.subscriberType = value;
	}

	/**
	 * Gets the value of the subscriptionType property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getSubscriptionType() {
		return subscriptionType;
	}

	/**
	 * Sets the value of the subscriptionType property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setSubscriptionType(BigInteger value) {
		this.subscriptionType = value;
	}

	/**
	 * Gets the value of the billedBalance property.
	 * 
	 * @return possible object is {@link BigDecimal }
	 * 
	 */
	public BigDecimal getBilledBalance() {
		return billedBalance;
	}

	/**
	 * Sets the value of the billedBalance property.
	 * 
	 * @param value
	 *            allowed object is {@link BigDecimal }
	 * 
	 */
	public void setBilledBalance(BigDecimal value) {
		this.billedBalance = value;
	}

	/**
	 * Gets the value of the nonBilledBalance property.
	 * 
	 * @return possible object is {@link BigDecimal }
	 * 
	 */
	public BigDecimal getNonBilledBalance() {
		return nonBilledBalance;
	}

	/**
	 * Sets the value of the nonBilledBalance property.
	 * 
	 * @param value
	 *            allowed object is {@link BigDecimal }
	 * 
	 */
	public void setNonBilledBalance(BigDecimal value) {
		this.nonBilledBalance = value;
	}

	/**
	 * Gets the value of the totalDebt property.
	 * 
	 * @return possible object is {@link BigDecimal }
	 * 
	 */
	public BigDecimal getTotalDebt() {
		return totalDebt;
	}

	/**
	 * Sets the value of the totalDebt property.
	 * 
	 * @param value
	 *            allowed object is {@link BigDecimal }
	 * 
	 */
	public void setTotalDebt(BigDecimal value) {
		this.totalDebt = value;
	}

	/**
	 * Gets the value of the subscriberStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubscriberStatus() {
		return subscriberStatus;
	}

	/**
	 * Sets the value of the subscriberStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubscriberStatus(String value) {
		this.subscriberStatus = value;
	}

	/**
	 * Gets the value of the activationDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActivationDate() {
		return activationDate;
	}

	/**
	 * Sets the value of the activationDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActivationDate(String value) {
		this.activationDate = value;
	}

	/**
	 * Gets the value of the iccid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getICCID() {
		return iccid;
	}

	/**
	 * Sets the value of the iccid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setICCID(String value) {
		this.iccid = value;
	}

	/**
	 * Gets the value of the imsi property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIMSI() {
		return imsi;
	}

	/**
	 * Sets the value of the imsi property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIMSI(String value) {
		this.imsi = value;
	}

	/**
	 * Gets the value of the cosid property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getCOSID() {
		return cosid;
	}

	/**
	 * Sets the value of the cosid property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setCOSID(BigInteger value) {
		this.cosid = value;
	}

	/**
	 * Gets the value of the languageID property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getLanguageID() {
		return languageID;
	}

	/**
	 * Sets the value of the languageID property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setLanguageID(BigInteger value) {
		this.languageID = value;
	}

	/**
	 * Gets the value of the taxID property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getTaxID() {
		return taxID;
	}

	/**
	 * Sets the value of the taxID property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setTaxID(BigInteger value) {
		this.taxID = value;
	}

	/**
	 * Gets the value of the corporateIndicator property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getCorporateIndicator() {
		return corporateIndicator;
	}

	/**
	 * Sets the value of the corporateIndicator property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setCorporateIndicator(BigInteger value) {
		this.corporateIndicator = value;
	}

	/**
	 * Gets the value of the corporateIDGroup property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getCorporateIDGroup() {
		return corporateIDGroup;
	}

	/**
	 * Sets the value of the corporateIDGroup property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setCorporateIDGroup(BigInteger value) {
		this.corporateIDGroup = value;
	}

	/**
	 * Gets the value of the idType property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getIDType() {
		return idType;
	}

	/**
	 * Sets the value of the idType property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setIDType(BigInteger value) {
		this.idType = value;
	}

	/**
	 * Gets the value of the idNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIDNumber() {
		return idNumber;
	}

	/**
	 * Sets the value of the idNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIDNumber(String value) {
		this.idNumber = value;
	}

	/**
	 * Gets the value of the pinNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPINNumber() {
		return pinNumber;
	}

	/**
	 * Sets the value of the pinNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPINNumber(String value) {
		this.pinNumber = value;
	}

	/**
	 * Gets the value of the issuedAuthority property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIssuedAuthority() {
		return issuedAuthority;
	}

	/**
	 * Sets the value of the issuedAuthority property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIssuedAuthority(String value) {
		this.issuedAuthority = value;
	}

	/**
	 * Gets the value of the dateofGrant property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateofGrant() {
		return dateofGrant;
	}

	/**
	 * Sets the value of the dateofGrant property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDateofGrant(String value) {
		this.dateofGrant = value;
	}

	/**
	 * Gets the value of the companyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the value of the companyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyName(String value) {
		this.companyName = value;
	}

	/**
	 * Gets the value of the city property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the value of the city property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCity(String value) {
		this.city = value;
	}

	/**
	 * Gets the value of the corporateContactPerson property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCorporateContactPerson() {
		return corporateContactPerson;
	}

	/**
	 * Sets the value of the corporateContactPerson property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCorporateContactPerson(String value) {
		this.corporateContactPerson = value;
	}

	/**
	 * Gets the value of the subscriberIndicator property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getSubscriberIndicator() {
		return subscriberIndicator;
	}

	/**
	 * Sets the value of the subscriberIndicator property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setSubscriberIndicator(BigInteger value) {
		this.subscriberIndicator = value;
	}

	/**
	 * Gets the value of the address property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the value of the address property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress(String value) {
		this.address = value;
	}
}
