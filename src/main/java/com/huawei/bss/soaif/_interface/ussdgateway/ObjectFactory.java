package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * com.huawei.bss.soaif._interface.ussdgateway package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.huawei.bss.soaif._interface.ussdgateway
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link USSDGWChngSOReqMsg }
	 * 
	 */
	public USSDGWChngSOReqMsg createUSSDGWChngSOReqMsg() {
		return new USSDGWChngSOReqMsg();
	}

	/**
	 * Create an instance of {@link USSDGWLoanReqMsg }
	 * 
	 */
	public USSDGWLoanReqMsg createUSSDGWLoanReqMsg() {
		return new USSDGWLoanReqMsg();
	}

	/**
	 * Create an instance of {@link USSDGWQueryFreeResourceRspMsg }
	 * 
	 */
	public USSDGWQueryFreeResourceRspMsg createUSSDGWQueryFreeResourceRspMsg() {
		return new USSDGWQueryFreeResourceRspMsg();
	}

	/**
	 * Create an instance of {@link USSDGWChngPOReqMsg }
	 * 
	 */
	public USSDGWChngPOReqMsg createUSSDGWChngPOReqMsg() {
		return new USSDGWChngPOReqMsg();
	}

	/**
	 * Create an instance of {@link USSDGWQueryFreeResourceReqMsg }
	 * 
	 */
	public USSDGWQueryFreeResourceReqMsg createUSSDGWQueryFreeResourceReqMsg() {
		return new USSDGWQueryFreeResourceReqMsg();
	}

	/**
	 * Create an instance of {@link USSDGWQueryFreeResourceReqMsg.QueryObj }
	 * 
	 */
	public USSDGWQueryFreeResourceReqMsg.QueryObj createUSSDGWQueryFreeResourceReqMsgQueryObj() {
		return new USSDGWQueryFreeResourceReqMsg.QueryObj();
	}

	/**
	 * Create an instance of {@link USSDGWChngPOReqMsg.PrimaryOfferingInfo }
	 * 
	 */
	public USSDGWChngPOReqMsg.PrimaryOfferingInfo createUSSDGWChngPOReqMsgPrimaryOfferingInfo() {
		return new USSDGWChngPOReqMsg.PrimaryOfferingInfo();
	}

	/**
	 * Create an instance of {@link USSDGWQueryFreeResourceRspMsg.FreeUnitItem }
	 * 
	 */
	public USSDGWQueryFreeResourceRspMsg.FreeUnitItem createUSSDGWQueryFreeResourceRspMsgFreeUnitItem() {
		return new USSDGWQueryFreeResourceRspMsg.FreeUnitItem();
	}

	/**
	 * Create an instance of {@link AcctList }
	 * 
	 */
	public AcctList createAcctList() {
		return new AcctList();
	}

	/**
	 * Create an instance of
	 * {@link USSDGWChngSOReqMsg.SupplementaryOfferingList }
	 * 
	 */
	public USSDGWChngSOReqMsg.SupplementaryOfferingList createUSSDGWChngSOReqMsgSupplementaryOfferingList() {
		return new USSDGWChngSOReqMsg.SupplementaryOfferingList();
	}

	/**
	 * Create an instance of {@link USSDGWChngPORspMsg }
	 * 
	 */
	public USSDGWChngPORspMsg createUSSDGWChngPORspMsg() {
		return new USSDGWChngPORspMsg();
	}

	/**
	 * Create an instance of {@link USSDGWLoanReqMsg.SubAccessCode }
	 * 
	 */
	public USSDGWLoanReqMsg.SubAccessCode createUSSDGWLoanReqMsgSubAccessCode() {
		return new USSDGWLoanReqMsg.SubAccessCode();
	}

	/**
	 * Create an instance of {@link QueryBalanceResponse }
	 * 
	 */
	public QueryBalanceResponse createQueryBalanceResponse() {
		return new QueryBalanceResponse();
	}

	/**
	 * Create an instance of {@link USSDGWLoanRspMsg }
	 * 
	 */
	public USSDGWLoanRspMsg createUSSDGWLoanRspMsg() {
		return new USSDGWLoanRspMsg();
	}

	/**
	 * Create an instance of {@link USSDGWQuerySubInfoRspMsg }
	 * 
	 */
	public USSDGWQuerySubInfoRspMsg createUSSDGWQuerySubInfoRspMsg() {
		return new USSDGWQuerySubInfoRspMsg();
	}

	/**
	 * Create an instance of {@link QuerySubInfoOut }
	 * 
	 */
	public QuerySubInfoOut createQuerySubInfoOut() {
		return new QuerySubInfoOut();
	}

	/**
	 * Create an instance of {@link USSDGWQuerySubInfoReqMsg }
	 * 
	 */
	public USSDGWQuerySubInfoReqMsg createUSSDGWQuerySubInfoReqMsg() {
		return new USSDGWQuerySubInfoReqMsg();
	}

	/**
	 * Create an instance of {@link USSDGWChngSORspMsg }
	 * 
	 */
	public USSDGWChngSORspMsg createUSSDGWChngSORspMsg() {
		return new USSDGWChngSORspMsg();
	}

	/**
	 * Create an instance of {@link QueryBalanceRequest }
	 * 
	 */
	public QueryBalanceRequest createQueryBalanceRequest() {
		return new QueryBalanceRequest();
	}

	/**
	 * Create an instance of {@link QueryBalanceIn }
	 * 
	 */
	public QueryBalanceIn createQueryBalanceIn() {
		return new QueryBalanceIn();
	}

	/**
	 * Create an instance of {@link AccountCredit }
	 * 
	 */
	public AccountCredit createAccountCredit() {
		return new AccountCredit();
	}

	/**
	 * Create an instance of {@link AcctBalance }
	 * 
	 */
	public AcctBalance createAcctBalance() {
		return new AcctBalance();
	}

	/**
	 * Create an instance of {@link CreditAmountInfo }
	 * 
	 */
	public CreditAmountInfo createCreditAmountInfo() {
		return new CreditAmountInfo();
	}

	/**
	 * Create an instance of {@link OutStandingList }
	 * 
	 */
	public OutStandingList createOutStandingList() {
		return new OutStandingList();
	}

	/**
	 * Create an instance of {@link BalanceDetail }
	 * 
	 */
	public BalanceDetail createBalanceDetail() {
		return new BalanceDetail();
	}

	/**
	 * Create an instance of {@link OutStandingDetail }
	 * 
	 */
	public OutStandingDetail createOutStandingDetail() {
		return new OutStandingDetail();
	}

	/**
	 * Create an instance of {@link OfferingInfo }
	 * 
	 */
	public OfferingInfo createOfferingInfo() {
		return new OfferingInfo();
	}

	/**
	 * Create an instance of
	 * {@link USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode }
	 * 
	 */
	public USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode createUSSDGWQueryFreeResourceReqMsgQueryObjSubAccessCode() {
		return new USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode();
	}

	/**
	 * Create an instance of
	 * {@link USSDGWChngPOReqMsg.PrimaryOfferingInfo.OfferingId }
	 * 
	 */
	public USSDGWChngPOReqMsg.PrimaryOfferingInfo.OfferingId createUSSDGWChngPOReqMsgPrimaryOfferingInfoOfferingId() {
		return new USSDGWChngPOReqMsg.PrimaryOfferingInfo.OfferingId();
	}

	/**
	 * Create an instance of
	 * {@link USSDGWQueryFreeResourceRspMsg.FreeUnitItem.FreeUnitItemDetail }
	 * 
	 */
	public USSDGWQueryFreeResourceRspMsg.FreeUnitItem.FreeUnitItemDetail createUSSDGWQueryFreeResourceRspMsgFreeUnitItemFreeUnitItemDetail() {
		return new USSDGWQueryFreeResourceRspMsg.FreeUnitItem.FreeUnitItemDetail();
	}

	/**
	 * Create an instance of {@link AcctList.CorPayRelaList }
	 * 
	 */
	public AcctList.CorPayRelaList createAcctListCorPayRelaList() {
		return new AcctList.CorPayRelaList();
	}

	/**
	 * Create an instance of {@link AcctList.IndvReserAmountList }
	 * 
	 */
	public AcctList.IndvReserAmountList createAcctListIndvReserAmountList() {
		return new AcctList.IndvReserAmountList();
	}

	/**
	 * Create an instance of {@link AcctList.CorpReserAmountList }
	 * 
	 */
	public AcctList.CorpReserAmountList createAcctListCorpReserAmountList() {
		return new AcctList.CorpReserAmountList();
	}

	/**
	 * Create an instance of
	 * {@link USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId }
	 * 
	 */
	public USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId createUSSDGWChngSOReqMsgSupplementaryOfferingListOfferingId() {
		return new USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId();
	}
}
