package com.huawei.bss.soaif._interface.ussdgateway;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.RspHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;element name="queryBalanceBody" type="{http://www.huawei.com/bss/soaif/interface/USSDGateWay/}AcctList" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rspHeader", "queryBalanceBody" })
@XmlRootElement(name = "queryBalanceResponse")
public class QueryBalanceResponse {
	@XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected RspHeader rspHeader;
	protected List<AcctList> queryBalanceBody;

	/**
	 * Gets the value of the rspHeader property.
	 * 
	 * @return possible object is {@link RspHeader }
	 * 
	 */
	public RspHeader getRspHeader() {
		return rspHeader;
	}

	/**
	 * Sets the value of the rspHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RspHeader }
	 * 
	 */
	public void setRspHeader(RspHeader value) {
		this.rspHeader = value;
	}

	/**
	 * Gets the value of the queryBalanceBody property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the queryBalanceBody property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getQueryBalanceBody().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link AcctList
	 * }
	 * 
	 * 
	 */
	public List<AcctList> getQueryBalanceBody() {
		if (queryBalanceBody == null) {
			queryBalanceBody = new ArrayList<AcctList>();
		}
		return this.queryBalanceBody;
	}
}
