package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.RspHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;element name="QuerySubInfoBody" type="{http://www.huawei.com/bss/soaif/interface/USSDGateWay/}QuerySubInfoOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rspHeader", "querySubInfoBody" })
@XmlRootElement(name = "USSDGWQuerySubInfoRspMsg")
public class USSDGWQuerySubInfoRspMsg {
	@XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected RspHeader rspHeader;
	@XmlElement(name = "QuerySubInfoBody")
	protected QuerySubInfoOut querySubInfoBody;

	/**
	 * Gets the value of the rspHeader property.
	 * 
	 * @return possible object is {@link RspHeader }
	 * 
	 */
	public RspHeader getRspHeader() {
		return rspHeader;
	}

	/**
	 * Sets the value of the rspHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RspHeader }
	 * 
	 */
	public void setRspHeader(RspHeader value) {
		this.rspHeader = value;
	}

	/**
	 * Gets the value of the querySubInfoBody property.
	 * 
	 * @return possible object is {@link QuerySubInfoOut }
	 * 
	 */
	public QuerySubInfoOut getQuerySubInfoBody() {
		return querySubInfoBody;
	}

	/**
	 * Sets the value of the querySubInfoBody property.
	 * 
	 * @param value
	 *            allowed object is {@link QuerySubInfoOut }
	 * 
	 */
	public void setQuerySubInfoBody(QuerySubInfoOut value) {
		this.querySubInfoBody = value;
	}
}
