package com.huawei.bss.soaif._interface.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Address complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressClass" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="1"/>
 *               &lt;enumeration value="2"/>
 *               &lt;enumeration value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ContactSeq" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AddressType" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressType" minOccurs="0"/>
 *         &lt;element name="Address1" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address2" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address3" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address4" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address5" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address6" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address7" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address8" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address9" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="Address10" type="{http://www.huawei.com/bss/soaif/interface/common/}AddressUnit" minOccurs="0"/>
 *         &lt;element name="PostCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Email1" type="{http://www.huawei.com/bss/soaif/interface/common/}Email" minOccurs="0"/>
 *         &lt;element name="Fax1" type="{http://www.huawei.com/bss/soaif/interface/common/}Fax" minOccurs="0"/>
 *         &lt;element name="SmsNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = { "addressClass", "contactSeq", "addressType", "address1", "address2",
		"address3", "address4", "address5", "address6", "address7", "address8", "address9", "address10", "postCode",
		"email1", "fax1", "smsNumber" })
@XmlSeeAlso({ com.huawei.bss.soaif._interface.subscriberservice.SupplementCustInfoReqMsg.Customer.Address.class,
		com.huawei.bss.soaif._interface.subscriberservice.SupplementCustInfoReqMsg.Account.Address.class,
		com.huawei.bss.soaif._interface.subscriberservice.ChangePrepaidToPostpaidReqMsg.PostpaidCust.Address.class })
public class Address {
	@XmlElement(name = "AddressClass")
	protected String addressClass;
	@XmlElement(name = "ContactSeq")
	protected String contactSeq;
	@XmlElement(name = "AddressType")
	protected String addressType;
	@XmlElement(name = "Address1")
	protected String address1;
	@XmlElement(name = "Address2")
	protected String address2;
	@XmlElement(name = "Address3")
	protected String address3;
	@XmlElement(name = "Address4")
	protected String address4;
	@XmlElement(name = "Address5")
	protected String address5;
	@XmlElement(name = "Address6")
	protected String address6;
	@XmlElement(name = "Address7")
	protected String address7;
	@XmlElement(name = "Address8")
	protected String address8;
	@XmlElement(name = "Address9")
	protected String address9;
	@XmlElement(name = "Address10")
	protected String address10;
	@XmlElement(name = "PostCode")
	protected String postCode;
	@XmlElement(name = "Email1")
	protected String email1;
	@XmlElement(name = "Fax1")
	protected String fax1;
	@XmlElement(name = "SmsNumber")
	protected String smsNumber;

	/**
	 * Gets the value of the addressClass property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddressClass() {
		return addressClass;
	}

	/**
	 * Sets the value of the addressClass property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddressClass(String value) {
		this.addressClass = value;
	}

	/**
	 * Gets the value of the contactSeq property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactSeq() {
		return contactSeq;
	}

	/**
	 * Sets the value of the contactSeq property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactSeq(String value) {
		this.contactSeq = value;
	}

	/**
	 * Gets the value of the addressType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddressType() {
		return addressType;
	}

	/**
	 * Sets the value of the addressType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddressType(String value) {
		this.addressType = value;
	}

	/**
	 * Gets the value of the address1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * Sets the value of the address1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress1(String value) {
		this.address1 = value;
	}

	/**
	 * Gets the value of the address2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * Sets the value of the address2 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress2(String value) {
		this.address2 = value;
	}

	/**
	 * Gets the value of the address3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * Sets the value of the address3 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress3(String value) {
		this.address3 = value;
	}

	/**
	 * Gets the value of the address4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress4() {
		return address4;
	}

	/**
	 * Sets the value of the address4 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress4(String value) {
		this.address4 = value;
	}

	/**
	 * Gets the value of the address5 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress5() {
		return address5;
	}

	/**
	 * Sets the value of the address5 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress5(String value) {
		this.address5 = value;
	}

	/**
	 * Gets the value of the address6 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress6() {
		return address6;
	}

	/**
	 * Sets the value of the address6 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress6(String value) {
		this.address6 = value;
	}

	/**
	 * Gets the value of the address7 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress7() {
		return address7;
	}

	/**
	 * Sets the value of the address7 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress7(String value) {
		this.address7 = value;
	}

	/**
	 * Gets the value of the address8 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress8() {
		return address8;
	}

	/**
	 * Sets the value of the address8 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress8(String value) {
		this.address8 = value;
	}

	/**
	 * Gets the value of the address9 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress9() {
		return address9;
	}

	/**
	 * Sets the value of the address9 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress9(String value) {
		this.address9 = value;
	}

	/**
	 * Gets the value of the address10 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAddress10() {
		return address10;
	}

	/**
	 * Sets the value of the address10 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAddress10(String value) {
		this.address10 = value;
	}

	/**
	 * Gets the value of the postCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * Sets the value of the postCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPostCode(String value) {
		this.postCode = value;
	}

	/**
	 * Gets the value of the email1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEmail1() {
		return email1;
	}

	/**
	 * Sets the value of the email1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEmail1(String value) {
		this.email1 = value;
	}

	/**
	 * Gets the value of the fax1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFax1() {
		return fax1;
	}

	/**
	 * Sets the value of the fax1 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFax1(String value) {
		this.fax1 = value;
	}

	/**
	 * Gets the value of the smsNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSmsNumber() {
		return smsNumber;
	}

	/**
	 * Sets the value of the smsNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSmsNumber(String value) {
		this.smsNumber = value;
	}
}
