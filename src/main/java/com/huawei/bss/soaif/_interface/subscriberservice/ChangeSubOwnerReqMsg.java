package com.huawei.bss.soaif._interface.subscriberservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.Address;
import com.huawei.bss.soaif._interface.common.BillMedium;
import com.huawei.bss.soaif._interface.common.Certificate;
import com.huawei.bss.soaif._interface.common.Contact;
import com.huawei.bss.soaif._interface.common.CreditLimit;
import com.huawei.bss.soaif._interface.common.Name;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.PaymentChannel;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.common.SimpleProperty;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="NewCustInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;element name="ExistCustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
 *                     &lt;element name="Customer">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel" minOccurs="0"/>
 *                               &lt;element name="CustSegment" type="{http://www.huawei.com/bss/soaif/interface/common/}CustSegment" minOccurs="0"/>
 *                               &lt;element name="NoticeSuppress" maxOccurs="unbounded" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *                               &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *                               &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
 *                               &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate" minOccurs="0"/>
 *                               &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
 *                               &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="Status" type="{http://www.huawei.com/bss/soaif/interface/common/}CustStatus" minOccurs="0"/>
 *                               &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="NewAcctInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;element name="ExistAcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
 *                     &lt;element name="Account">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
 *                               &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *                               &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *                               &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
 *                               &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                               &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" minOccurs="0"/>
 *                               &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
 *                               &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
 *                               &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="AcctPayMethod" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctPayMethod" minOccurs="0"/>
 *                               &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="unbounded" minOccurs="0"/>
 *                               &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="NewSubInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                     &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *                     &lt;element name="IMEI" type="{http://www.huawei.com/bss/soaif/interface/common/}IMEI" minOccurs="0"/>
 *                     &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="PasswordInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Password" type="{http://www.huawei.com/bss/soaif/interface/common/}Password"/>
 *                     &lt;element name="OldPassword" type="{http://www.huawei.com/bss/soaif/interface/common/}Password"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "newCustInfo", "newAcctInfo", "newSubInfo",
		"passwordInfo" })
@XmlRootElement(name = "ChangeSubOwnerReqMsg")
public class ChangeSubOwnerReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected ChangeSubOwnerReqMsg.AccessInfo accessInfo;
	@XmlElement(name = "NewCustInfo", required = true)
	protected ChangeSubOwnerReqMsg.NewCustInfo newCustInfo;
	@XmlElement(name = "NewAcctInfo", required = true)
	protected ChangeSubOwnerReqMsg.NewAcctInfo newAcctInfo;
	@XmlElement(name = "NewSubInfo", required = true)
	protected ChangeSubOwnerReqMsg.NewSubInfo newSubInfo;
	@XmlElement(name = "PasswordInfo", required = true)
	protected ChangeSubOwnerReqMsg.PasswordInfo passwordInfo;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is {@link ChangeSubOwnerReqMsg.AccessInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.AccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ChangeSubOwnerReqMsg.AccessInfo }
	 * 
	 */
	public void setAccessInfo(ChangeSubOwnerReqMsg.AccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the newCustInfo property.
	 * 
	 * @return possible object is {@link ChangeSubOwnerReqMsg.NewCustInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewCustInfo getNewCustInfo() {
		return newCustInfo;
	}

	/**
	 * Sets the value of the newCustInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ChangeSubOwnerReqMsg.NewCustInfo }
	 * 
	 */
	public void setNewCustInfo(ChangeSubOwnerReqMsg.NewCustInfo value) {
		this.newCustInfo = value;
	}

	/**
	 * Gets the value of the newAcctInfo property.
	 * 
	 * @return possible object is {@link ChangeSubOwnerReqMsg.NewAcctInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewAcctInfo getNewAcctInfo() {
		return newAcctInfo;
	}

	/**
	 * Sets the value of the newAcctInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ChangeSubOwnerReqMsg.NewAcctInfo }
	 * 
	 */
	public void setNewAcctInfo(ChangeSubOwnerReqMsg.NewAcctInfo value) {
		this.newAcctInfo = value;
	}

	/**
	 * Gets the value of the newSubInfo property.
	 * 
	 * @return possible object is {@link ChangeSubOwnerReqMsg.NewSubInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.NewSubInfo getNewSubInfo() {
		return newSubInfo;
	}

	/**
	 * Sets the value of the newSubInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ChangeSubOwnerReqMsg.NewSubInfo }
	 * 
	 */
	public void setNewSubInfo(ChangeSubOwnerReqMsg.NewSubInfo value) {
		this.newSubInfo = value;
	}

	/**
	 * Gets the value of the passwordInfo property.
	 * 
	 * @return possible object is {@link ChangeSubOwnerReqMsg.PasswordInfo }
	 * 
	 */
	public ChangeSubOwnerReqMsg.PasswordInfo getPasswordInfo() {
		return passwordInfo;
	}

	/**
	 * Sets the value of the passwordInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ChangeSubOwnerReqMsg.PasswordInfo }
	 * 
	 */
	public void setPasswordInfo(ChangeSubOwnerReqMsg.PasswordInfo value) {
		this.passwordInfo = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo">
	 *     &lt;/extension>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class AccessInfo extends ObjectAccessInfo {
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;choice>
	 *         &lt;element name="ExistAcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
	 *         &lt;element name="Account">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                   &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
	 *                   &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
	 *                   &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
	 *                   &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
	 *                   &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
	 *                   &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" minOccurs="0"/>
	 *                   &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
	 *                   &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
	 *                   &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="AcctPayMethod" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctPayMethod" minOccurs="0"/>
	 *                   &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/choice>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "existAcctId", "account" })
	public static class NewAcctInfo {
		@XmlElement(name = "ExistAcctId")
		protected String existAcctId;
		@XmlElement(name = "Account")
		protected ChangeSubOwnerReqMsg.NewAcctInfo.Account account;

		/**
		 * Gets the value of the existAcctId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getExistAcctId() {
			return existAcctId;
		}

		/**
		 * Sets the value of the existAcctId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setExistAcctId(String value) {
			this.existAcctId = value;
		}

		/**
		 * Gets the value of the account property.
		 * 
		 * @return possible object is
		 *         {@link ChangeSubOwnerReqMsg.NewAcctInfo.Account }
		 * 
		 */
		public ChangeSubOwnerReqMsg.NewAcctInfo.Account getAccount() {
			return account;
		}

		/**
		 * Sets the value of the account property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link ChangeSubOwnerReqMsg.NewAcctInfo.Account }
		 * 
		 */
		public void setAccount(ChangeSubOwnerReqMsg.NewAcctInfo.Account value) {
			this.account = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *         &lt;element name="PaymentType" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentType" minOccurs="0"/>
		 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
		 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
		 *         &lt;element name="BillCycleType" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleType" minOccurs="0"/>
		 *         &lt;element name="BillLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
		 *         &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" minOccurs="0"/>
		 *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="BillMedium" type="{http://www.huawei.com/bss/soaif/interface/common/}BillMedium" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency" minOccurs="0"/>
		 *         &lt;element name="InitialBalance" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount" minOccurs="0"/>
		 *         &lt;element name="CreditLimit" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimit" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="AcctPayMethod" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctPayMethod" minOccurs="0"/>
		 *         &lt;element name="PaymentChannel" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentChannel" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "acctName", "paymentType", "title", "name", "billCycleType", "billLanguage",
				"contact", "address", "billMedium", "currency", "initialBalance", "creditLimit", "acctPayMethod",
				"paymentChannel", "additionalProperty" })
		public static class Account {
			@XmlElement(name = "AcctName")
			protected String acctName;
			@XmlElement(name = "PaymentType")
			protected String paymentType;
			@XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
			protected JAXBElement<String> title;
			@XmlElement(name = "Name")
			protected Name name;
			@XmlElement(name = "BillCycleType")
			protected String billCycleType;
			@XmlElement(name = "BillLanguage")
			protected String billLanguage;
			@XmlElement(name = "Contact")
			protected Contact contact;
			@XmlElement(name = "Address")
			protected List<Address> address;
			@XmlElement(name = "BillMedium")
			protected List<BillMedium> billMedium;
			@XmlElement(name = "Currency")
			protected BigInteger currency;
			@XmlElement(name = "InitialBalance")
			protected String initialBalance;
			@XmlElement(name = "CreditLimit")
			protected List<CreditLimit> creditLimit;
			@XmlElement(name = "AcctPayMethod")
			protected String acctPayMethod;
			@XmlElement(name = "PaymentChannel")
			protected List<PaymentChannel> paymentChannel;
			@XmlElement(name = "AdditionalProperty")
			protected List<SimpleProperty> additionalProperty;

			/**
			 * Gets the value of the acctName property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getAcctName() {
				return acctName;
			}

			/**
			 * Sets the value of the acctName property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setAcctName(String value) {
				this.acctName = value;
			}

			/**
			 * Gets the value of the paymentType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPaymentType() {
				return paymentType;
			}

			/**
			 * Sets the value of the paymentType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPaymentType(String value) {
				this.paymentType = value;
			}

			/**
			 * Gets the value of the title property.
			 * 
			 * @return possible object is {@link JAXBElement
			 *         }{@code <}{@link String }{@code >}
			 * 
			 */
			public JAXBElement<String> getTitle() {
				return title;
			}

			/**
			 * Sets the value of the title property.
			 * 
			 * @param value
			 *            allowed object is {@link JAXBElement
			 *            }{@code <}{@link String }{@code >}
			 * 
			 */
			public void setTitle(JAXBElement<String> value) {
				this.title = value;
			}

			/**
			 * Gets the value of the name property.
			 * 
			 * @return possible object is {@link Name }
			 * 
			 */
			public Name getName() {
				return name;
			}

			/**
			 * Sets the value of the name property.
			 * 
			 * @param value
			 *            allowed object is {@link Name }
			 * 
			 */
			public void setName(Name value) {
				this.name = value;
			}

			/**
			 * Gets the value of the billCycleType property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getBillCycleType() {
				return billCycleType;
			}

			/**
			 * Sets the value of the billCycleType property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setBillCycleType(String value) {
				this.billCycleType = value;
			}

			/**
			 * Gets the value of the billLanguage property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getBillLanguage() {
				return billLanguage;
			}

			/**
			 * Sets the value of the billLanguage property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setBillLanguage(String value) {
				this.billLanguage = value;
			}

			/**
			 * Gets the value of the contact property.
			 * 
			 * @return possible object is {@link Contact }
			 * 
			 */
			public Contact getContact() {
				return contact;
			}

			/**
			 * Sets the value of the contact property.
			 * 
			 * @param value
			 *            allowed object is {@link Contact }
			 * 
			 */
			public void setContact(Contact value) {
				this.contact = value;
			}

			/**
			 * Gets the value of the address property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the address property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getAddress().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link Address }
			 * 
			 * 
			 */
			public List<Address> getAddress() {
				if (address == null) {
					address = new ArrayList<Address>();
				}
				return this.address;
			}

			/**
			 * Gets the value of the billMedium property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the billMedium property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getBillMedium().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link BillMedium }
			 * 
			 * 
			 */
			public List<BillMedium> getBillMedium() {
				if (billMedium == null) {
					billMedium = new ArrayList<BillMedium>();
				}
				return this.billMedium;
			}

			/**
			 * Gets the value of the currency property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCurrency() {
				return currency;
			}

			/**
			 * Sets the value of the currency property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			public void setCurrency(BigInteger value) {
				this.currency = value;
			}

			/**
			 * Gets the value of the initialBalance property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getInitialBalance() {
				return initialBalance;
			}

			/**
			 * Sets the value of the initialBalance property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setInitialBalance(String value) {
				this.initialBalance = value;
			}

			/**
			 * Gets the value of the creditLimit property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the creditLimit property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getCreditLimit().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link CreditLimit }
			 * 
			 * 
			 */
			public List<CreditLimit> getCreditLimit() {
				if (creditLimit == null) {
					creditLimit = new ArrayList<CreditLimit>();
				}
				return this.creditLimit;
			}

			/**
			 * Gets the value of the acctPayMethod property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getAcctPayMethod() {
				return acctPayMethod;
			}

			/**
			 * Sets the value of the acctPayMethod property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setAcctPayMethod(String value) {
				this.acctPayMethod = value;
			}

			/**
			 * Gets the value of the paymentChannel property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the paymentChannel property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getPaymentChannel().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link PaymentChannel }
			 * 
			 * 
			 */
			public List<PaymentChannel> getPaymentChannel() {
				if (paymentChannel == null) {
					paymentChannel = new ArrayList<PaymentChannel>();
				}
				return this.paymentChannel;
			}

			/**
			 * Gets the value of the additionalProperty property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the additionalProperty
			 * property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getAdditionalProperty().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link SimpleProperty }
			 * 
			 * 
			 */
			public List<SimpleProperty> getAdditionalProperty() {
				if (additionalProperty == null) {
					additionalProperty = new ArrayList<SimpleProperty>();
				}
				return this.additionalProperty;
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;choice>
	 *         &lt;element name="ExistCustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
	 *         &lt;element name="Customer">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel" minOccurs="0"/>
	 *                   &lt;element name="CustSegment" type="{http://www.huawei.com/bss/soaif/interface/common/}CustSegment" minOccurs="0"/>
	 *                   &lt;element name="NoticeSuppress" maxOccurs="unbounded" minOccurs="0">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                   &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
	 *                   &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
	 *                   &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
	 *                   &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate" minOccurs="0"/>
	 *                   &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
	 *                   &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
	 *                   &lt;element name="Status" type="{http://www.huawei.com/bss/soaif/interface/common/}CustStatus" minOccurs="0"/>
	 *                   &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/choice>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "existCustId", "customer" })
	public static class NewCustInfo {
		@XmlElement(name = "ExistCustId")
		protected String existCustId;
		@XmlElement(name = "Customer")
		protected ChangeSubOwnerReqMsg.NewCustInfo.Customer customer;

		/**
		 * Gets the value of the existCustId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getExistCustId() {
			return existCustId;
		}

		/**
		 * Sets the value of the existCustId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setExistCustId(String value) {
			this.existCustId = value;
		}

		/**
		 * Gets the value of the customer property.
		 * 
		 * @return possible object is
		 *         {@link ChangeSubOwnerReqMsg.NewCustInfo.Customer }
		 * 
		 */
		public ChangeSubOwnerReqMsg.NewCustInfo.Customer getCustomer() {
			return customer;
		}

		/**
		 * Sets the value of the customer property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link ChangeSubOwnerReqMsg.NewCustInfo.Customer }
		 * 
		 */
		public void setCustomer(ChangeSubOwnerReqMsg.NewCustInfo.Customer value) {
			this.customer = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel" minOccurs="0"/>
		 *         &lt;element name="CustSegment" type="{http://www.huawei.com/bss/soaif/interface/common/}CustSegment" minOccurs="0"/>
		 *         &lt;element name="NoticeSuppress" maxOccurs="unbounded" minOccurs="0">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
		 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
		 *         &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
		 *         &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate" minOccurs="0"/>
		 *         &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
		 *         &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
		 *         &lt;element name="Status" type="{http://www.huawei.com/bss/soaif/interface/common/}CustStatus" minOccurs="0"/>
		 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "custLevel", "custSegment", "noticeSuppress", "title", "name", "nationality",
				"certificate", "birthday", "contact", "address", "status", "additionalProperty" })
		public static class Customer {
			@XmlElement(name = "CustLevel")
			protected String custLevel;
			@XmlElement(name = "CustSegment")
			protected String custSegment;
			@XmlElement(name = "NoticeSuppress")
			protected List<ChangeSubOwnerReqMsg.NewCustInfo.Customer.NoticeSuppress> noticeSuppress;
			@XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
			protected JAXBElement<String> title;
			@XmlElement(name = "Name")
			protected Name name;
			@XmlElementRef(name = "Nationality", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
			protected JAXBElement<String> nationality;
			@XmlElementRef(name = "Certificate", namespace = "http://www.huawei.com/bss/soaif/interface/SubscriberService/", type = JAXBElement.class, required = false)
			protected JAXBElement<Certificate> certificate;
			@XmlElement(name = "Birthday")
			protected String birthday;
			@XmlElement(name = "Contact")
			protected List<Contact> contact;
			@XmlElement(name = "Address")
			protected List<Address> address;
			@XmlElement(name = "Status")
			protected String status;
			@XmlElement(name = "AdditionalProperty")
			protected List<SimpleProperty> additionalProperty;

			/**
			 * Gets the value of the custLevel property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCustLevel() {
				return custLevel;
			}

			/**
			 * Sets the value of the custLevel property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCustLevel(String value) {
				this.custLevel = value;
			}

			/**
			 * Gets the value of the custSegment property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCustSegment() {
				return custSegment;
			}

			/**
			 * Sets the value of the custSegment property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCustSegment(String value) {
				this.custSegment = value;
			}

			/**
			 * Gets the value of the noticeSuppress property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the noticeSuppress property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getNoticeSuppress().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link ChangeSubOwnerReqMsg.NewCustInfo.Customer.NoticeSuppress }
			 * 
			 * 
			 */
			public List<ChangeSubOwnerReqMsg.NewCustInfo.Customer.NoticeSuppress> getNoticeSuppress() {
				if (noticeSuppress == null) {
					noticeSuppress = new ArrayList<ChangeSubOwnerReqMsg.NewCustInfo.Customer.NoticeSuppress>();
				}
				return this.noticeSuppress;
			}

			/**
			 * Gets the value of the title property.
			 * 
			 * @return possible object is {@link JAXBElement
			 *         }{@code <}{@link String }{@code >}
			 * 
			 */
			public JAXBElement<String> getTitle() {
				return title;
			}

			/**
			 * Sets the value of the title property.
			 * 
			 * @param value
			 *            allowed object is {@link JAXBElement
			 *            }{@code <}{@link String }{@code >}
			 * 
			 */
			public void setTitle(JAXBElement<String> value) {
				this.title = value;
			}

			/**
			 * Gets the value of the name property.
			 * 
			 * @return possible object is {@link Name }
			 * 
			 */
			public Name getName() {
				return name;
			}

			/**
			 * Sets the value of the name property.
			 * 
			 * @param value
			 *            allowed object is {@link Name }
			 * 
			 */
			public void setName(Name value) {
				this.name = value;
			}

			/**
			 * Gets the value of the nationality property.
			 * 
			 * @return possible object is {@link JAXBElement
			 *         }{@code <}{@link String }{@code >}
			 * 
			 */
			public JAXBElement<String> getNationality() {
				return nationality;
			}

			/**
			 * Sets the value of the nationality property.
			 * 
			 * @param value
			 *            allowed object is {@link JAXBElement
			 *            }{@code <}{@link String }{@code >}
			 * 
			 */
			public void setNationality(JAXBElement<String> value) {
				this.nationality = value;
			}

			/**
			 * Gets the value of the certificate property.
			 * 
			 * @return possible object is {@link JAXBElement
			 *         }{@code <}{@link Certificate }{@code >}
			 * 
			 */
			public JAXBElement<Certificate> getCertificate() {
				return certificate;
			}

			/**
			 * Sets the value of the certificate property.
			 * 
			 * @param value
			 *            allowed object is {@link JAXBElement
			 *            }{@code <}{@link Certificate }{@code >}
			 * 
			 */
			public void setCertificate(JAXBElement<Certificate> value) {
				this.certificate = value;
			}

			/**
			 * Gets the value of the birthday property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getBirthday() {
				return birthday;
			}

			/**
			 * Sets the value of the birthday property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setBirthday(String value) {
				this.birthday = value;
			}

			/**
			 * Gets the value of the contact property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the contact property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getContact().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link Contact }
			 * 
			 * 
			 */
			public List<Contact> getContact() {
				if (contact == null) {
					contact = new ArrayList<Contact>();
				}
				return this.contact;
			}

			/**
			 * Gets the value of the address property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the address property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getAddress().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link Address }
			 * 
			 * 
			 */
			public List<Address> getAddress() {
				if (address == null) {
					address = new ArrayList<Address>();
				}
				return this.address;
			}

			/**
			 * Gets the value of the status property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getStatus() {
				return status;
			}

			/**
			 * Sets the value of the status property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setStatus(String value) {
				this.status = value;
			}

			/**
			 * Gets the value of the additionalProperty property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the additionalProperty
			 * property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getAdditionalProperty().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link SimpleProperty }
			 * 
			 * 
			 */
			public List<SimpleProperty> getAdditionalProperty() {
				if (additionalProperty == null) {
					additionalProperty = new ArrayList<SimpleProperty>();
				}
				return this.additionalProperty;
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * 
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "channelType", "noticeType", "subNoticeType" })
			public static class NoticeSuppress {
				@XmlElement(name = "ChannelType", required = true)
				protected String channelType;
				@XmlElement(name = "NoticeType", required = true)
				protected String noticeType;
				@XmlElement(name = "SubNoticeType")
				protected String subNoticeType;

				/**
				 * Gets the value of the channelType property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getChannelType() {
					return channelType;
				}

				/**
				 * Sets the value of the channelType property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setChannelType(String value) {
					this.channelType = value;
				}

				/**
				 * Gets the value of the noticeType property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getNoticeType() {
					return noticeType;
				}

				/**
				 * Sets the value of the noticeType property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setNoticeType(String value) {
					this.noticeType = value;
				}

				/**
				 * Gets the value of the subNoticeType property.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getSubNoticeType() {
					return subNoticeType;
				}

				/**
				 * Sets the value of the subNoticeType property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setSubNoticeType(String value) {
					this.subNoticeType = value;
				}
			}
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
	 *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
	 *         &lt;element name="IMEI" type="{http://www.huawei.com/bss/soaif/interface/common/}IMEI" minOccurs="0"/>
	 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "language", "writtenLanguage", "imei", "additionalProperty" })
	public static class NewSubInfo {
		@XmlElement(name = "Language")
		protected String language;
		@XmlElement(name = "WrittenLanguage")
		protected String writtenLanguage;
		@XmlElement(name = "IMEI")
		protected String imei;
		@XmlElement(name = "AdditionalProperty")
		protected List<SimpleProperty> additionalProperty;

		/**
		 * Gets the value of the language property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getLanguage() {
			return language;
		}

		/**
		 * Sets the value of the language property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setLanguage(String value) {
			this.language = value;
		}

		/**
		 * Gets the value of the writtenLanguage property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getWrittenLanguage() {
			return writtenLanguage;
		}

		/**
		 * Sets the value of the writtenLanguage property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setWrittenLanguage(String value) {
			this.writtenLanguage = value;
		}

		/**
		 * Gets the value of the imei property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getIMEI() {
			return imei;
		}

		/**
		 * Sets the value of the imei property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setIMEI(String value) {
			this.imei = value;
		}

		/**
		 * Gets the value of the additionalProperty property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the additionalProperty property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAdditionalProperty().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link SimpleProperty }
		 * 
		 * 
		 */
		public List<SimpleProperty> getAdditionalProperty() {
			if (additionalProperty == null) {
				additionalProperty = new ArrayList<SimpleProperty>();
			}
			return this.additionalProperty;
		}
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Password" type="{http://www.huawei.com/bss/soaif/interface/common/}Password"/>
	 *         &lt;element name="OldPassword" type="{http://www.huawei.com/bss/soaif/interface/common/}Password"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "password", "oldPassword" })
	public static class PasswordInfo {
		@XmlElement(name = "Password", required = true)
		protected String password;
		@XmlElement(name = "OldPassword", required = true)
		protected String oldPassword;

		/**
		 * Gets the value of the password property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * Sets the value of the password property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPassword(String value) {
			this.password = value;
		}

		/**
		 * Gets the value of the oldPassword property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getOldPassword() {
			return oldPassword;
		}

		/**
		 * Sets the value of the oldPassword property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setOldPassword(String value) {
			this.oldPassword = value;
		}
	}
}
