
package com.huawei.bss.soaif._interface.common.createorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ManageCreditLimit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ManageCreditLimit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
 *         &lt;element name="CreditLimitInfoList" type="{http://www.huawei.com/bss/soaif/interface/common/}CreditLimitInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManageCreditLimit", propOrder = {
    "accountId",
    "creditLimitInfoList"
})
public class ManageCreditLimit {

    @XmlElement(name = "AccountId", required = true)
    protected String accountId;
    @XmlElement(name = "CreditLimitInfoList")
    protected CreditLimitInfo creditLimitInfoList;

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountId(String value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the creditLimitInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link CreditLimitInfo }
     *     
     */
    public CreditLimitInfo getCreditLimitInfoList() {
        return creditLimitInfoList;
    }

    /**
     * Sets the value of the creditLimitInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditLimitInfo }
     *     
     */
    public void setCreditLimitInfoList(CreditLimitInfo value) {
        this.creditLimitInfoList = value;
    }

}
