package com.huawei.bss.soaif._interface.common.ens.formnp;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionId" type="{http://www.huawei.com/bss/soaif/interface/common/}TransactionId"/>
 *         &lt;element name="ChannelId" type="{http://www.huawei.com/bss/soaif/interface/common/}ChannelId"/>
 *         &lt;element name="AccessUser">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AccessPwd">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="128"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "transactionId", "channelId", "accessUser", "accessPwd", "additionalProperty" })
@XmlRootElement(name = "ReqHeader")
public class ReqHeader {
	@XmlElement(name = "TransactionId", required = true)
	protected String transactionId;
	@XmlElement(name = "ChannelId", required = true)
	protected String channelId;
	@XmlElement(name = "AccessUser", required = true)
	protected String accessUser;
	@XmlElement(name = "AccessPwd", required = true)
	protected String accessPwd;
	@XmlElement(name = "AdditionalProperty")
	protected List<SimpleProperty> additionalProperty;

	/**
	 * Gets the value of the transactionId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * Sets the value of the transactionId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTransactionId(String value) {
		this.transactionId = value;
	}

	/**
	 * Gets the value of the channelId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * Sets the value of the channelId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChannelId(String value) {
		this.channelId = value;
	}

	/**
	 * Gets the value of the accessUser property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccessUser() {
		return accessUser;
	}

	/**
	 * Sets the value of the accessUser property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccessUser(String value) {
		this.accessUser = value;
	}

	/**
	 * Gets the value of the accessPwd property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAccessPwd() {
		return accessPwd;
	}

	/**
	 * Sets the value of the accessPwd property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAccessPwd(String value) {
		this.accessPwd = value;
	}

	/**
	 * Gets the value of the additionalProperty property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the additionalProperty property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAdditionalProperty().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link SimpleProperty }
	 * 
	 * 
	 */
	public List<SimpleProperty> getAdditionalProperty() {
		if (additionalProperty == null) {
			additionalProperty = new ArrayList<SimpleProperty>();
		}
		return this.additionalProperty;
	}
}
