
package com.huawei.bss.soaif._interface.common.createorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionId" type="{http://www.huawei.com/bss/soaif/interface/common/}TransactionId"/>
 *         &lt;element name="ChannelId" type="{http://www.huawei.com/bss/soaif/interface/common/}Channel"/>
 *         &lt;element name="AccessUser">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AccessPwd">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TenantId" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="OperatorId" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="MIDWAREChannelID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="MIDWAREAccessUser" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="MIDWAREAccessPwd" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionId",
    "channelId",
    "accessUser",
    "accessPwd",
    "tenantId",
    "operatorId",
    "midwareChannelID",
    "midwareAccessUser",
    "midwareAccessPwd"
})
@XmlRootElement(name = "ReqHeader")
public class ReqHeader {

    @XmlElement(name = "TransactionId", required = true)
    protected String transactionId;
    @XmlElement(name = "ChannelId", required = true)
    protected String channelId;
    @XmlElement(name = "AccessUser", required = true)
    protected String accessUser;
    @XmlElement(name = "AccessPwd", required = true)
    protected String accessPwd;
    @XmlElement(name = "TenantId")
    protected Object tenantId;
    @XmlElement(name = "OperatorId")
    protected Object operatorId;
    @XmlElement(name = "MIDWAREChannelID")
    protected Object midwareChannelID;
    @XmlElement(name = "MIDWAREAccessUser")
    protected Object midwareAccessUser;
    @XmlElement(name = "MIDWAREAccessPwd")
    protected Object midwareAccessPwd;

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the channelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * Sets the value of the channelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelId(String value) {
        this.channelId = value;
    }

    /**
     * Gets the value of the accessUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessUser() {
        return accessUser;
    }

    /**
     * Sets the value of the accessUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessUser(String value) {
        this.accessUser = value;
    }

    /**
     * Gets the value of the accessPwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessPwd() {
        return accessPwd;
    }

    /**
     * Sets the value of the accessPwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessPwd(String value) {
        this.accessPwd = value;
    }

    /**
     * Gets the value of the tenantId property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getTenantId() {
        return tenantId;
    }

    /**
     * Sets the value of the tenantId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setTenantId(Object value) {
        this.tenantId = value;
    }

    /**
     * Gets the value of the operatorId property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getOperatorId() {
        return operatorId;
    }

    /**
     * Sets the value of the operatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setOperatorId(Object value) {
        this.operatorId = value;
    }

    /**
     * Gets the value of the midwareChannelID property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getMIDWAREChannelID() {
        return midwareChannelID;
    }

    /**
     * Sets the value of the midwareChannelID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setMIDWAREChannelID(Object value) {
        this.midwareChannelID = value;
    }

    /**
     * Gets the value of the midwareAccessUser property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getMIDWAREAccessUser() {
        return midwareAccessUser;
    }

    /**
     * Sets the value of the midwareAccessUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setMIDWAREAccessUser(Object value) {
        this.midwareAccessUser = value;
    }

    /**
     * Gets the value of the midwareAccessPwd property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getMIDWAREAccessPwd() {
        return midwareAccessPwd;
    }

    /**
     * Sets the value of the midwareAccessPwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setMIDWAREAccessPwd(Object value) {
        this.midwareAccessPwd = value;
    }

}
