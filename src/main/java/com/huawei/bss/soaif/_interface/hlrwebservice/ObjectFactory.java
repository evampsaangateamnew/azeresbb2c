package com.huawei.bss.soaif._interface.hlrwebservice;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * com.huawei.bss.soaif._interface.hlrwebservice package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.huawei.bss.soaif._interface.hlrwebservice
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link AcctList }
	 * 
	 */
	public AcctList createAcctList() {
		return new AcctList();
	}

	/**
	 * Create an instance of {@link GetCustomerResponse }
	 * 
	 */
	public GetCustomerResponse createGetCustomerResponse() {
		return new GetCustomerResponse();
	}

	/**
	 * Create an instance of {@link GetCustomerOut }
	 * 
	 */
	public GetCustomerOut createGetCustomerOut() {
		return new GetCustomerOut();
	}

	/**
	 * Create an instance of {@link GetCustomerRequest }
	 * 
	 */
	public GetCustomerRequest createGetCustomerRequest() {
		return new GetCustomerRequest();
	}

	/**
	 * Create an instance of {@link GetCustomerIn }
	 * 
	 */
	public GetCustomerIn createGetCustomerIn() {
		return new GetCustomerIn();
	}

	/**
	 * Create an instance of {@link QueryBalanceResponse }
	 * 
	 */
	public QueryBalanceResponse createQueryBalanceResponse() {
		return new QueryBalanceResponse();
	}

	/**
	 * Create an instance of {@link GetSubscriberResponse }
	 * 
	 */
	public GetSubscriberResponse createGetSubscriberResponse() {
		return new GetSubscriberResponse();
	}

	/**
	 * Create an instance of {@link GetSubscriberOut }
	 * 
	 */
	public GetSubscriberOut createGetSubscriberOut() {
		return new GetSubscriberOut();
	}

	/**
	 * Create an instance of {@link QueryBalanceRequest }
	 * 
	 */
	public QueryBalanceRequest createQueryBalanceRequest() {
		return new QueryBalanceRequest();
	}

	/**
	 * Create an instance of {@link QueryBalanceIn }
	 * 
	 */
	public QueryBalanceIn createQueryBalanceIn() {
		return new QueryBalanceIn();
	}

	/**
	 * Create an instance of {@link GetSubscriberRequest }
	 * 
	 */
	public GetSubscriberRequest createGetSubscriberRequest() {
		return new GetSubscriberRequest();
	}

	/**
	 * Create an instance of {@link GetSubscriberIn }
	 * 
	 */
	public GetSubscriberIn createGetSubscriberIn() {
		return new GetSubscriberIn();
	}

	/**
	 * Create an instance of {@link AccountCredit }
	 * 
	 */
	public AccountCredit createAccountCredit() {
		return new AccountCredit();
	}

	/**
	 * Create an instance of {@link BalanceDetail }
	 * 
	 */
	public BalanceDetail createBalanceDetail() {
		return new BalanceDetail();
	}

	/**
	 * Create an instance of {@link AddressInfo }
	 * 
	 */
	public AddressInfo createAddressInfo() {
		return new AddressInfo();
	}

	/**
	 * Create an instance of {@link AcctBalance }
	 * 
	 */
	public AcctBalance createAcctBalance() {
		return new AcctBalance();
	}

	/**
	 * Create an instance of {@link CreditAmountInfo }
	 * 
	 */
	public CreditAmountInfo createCreditAmountInfo() {
		return new CreditAmountInfo();
	}

	/**
	 * Create an instance of {@link OutStandingList }
	 * 
	 */
	public OutStandingList createOutStandingList() {
		return new OutStandingList();
	}

	/**
	 * Create an instance of {@link OutStandingDetail }
	 * 
	 */
	public OutStandingDetail createOutStandingDetail() {
		return new OutStandingDetail();
	}

	/**
	 * Create an instance of {@link OfferingInfo }
	 * 
	 */
	public OfferingInfo createOfferingInfo() {
		return new OfferingInfo();
	}

	/**
	 * Create an instance of {@link AcctList.CorPayRelaList }
	 * 
	 */
	public AcctList.CorPayRelaList createAcctListCorPayRelaList() {
		return new AcctList.CorPayRelaList();
	}

	/**
	 * Create an instance of {@link AcctList.IndvReserAmountList }
	 * 
	 */
	public AcctList.IndvReserAmountList createAcctListIndvReserAmountList() {
		return new AcctList.IndvReserAmountList();
	}

	/**
	 * Create an instance of {@link AcctList.CorpReserAmountList }
	 * 
	 */
	public AcctList.CorpReserAmountList createAcctListCorpReserAmountList() {
		return new AcctList.CorpReserAmountList();
	}
}
