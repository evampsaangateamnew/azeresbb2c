
package com.huawei.bss.soaif._interface.common.createorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OfferingInstance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingInstance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingId" type="{http://www.huawei.com/bss/soaif/interface/common/}OfferingId"/>
 *         &lt;element name="Contract" type="{http://www.huawei.com/bss/soaif/interface/common/}Contract" minOccurs="0"/>
 *         &lt;element name="InstanceProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}OfferingInstanceProperty" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="ActionType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingInstance", propOrder = {
    "offeringId",
    "contract",
    "instanceProperty",
    "actionType"
})
@XmlSeeAlso({
    com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.PrimaryOffering.class,
    com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.SupplementaryOffering.class
})
public class OfferingInstance {

    @XmlElement(name = "OfferingId", required = true, nillable = true)
    protected OfferingId offeringId;
    @XmlElement(name = "Contract")
    protected Contract contract;
    @XmlElement(name = "InstanceProperty")
    protected List<OfferingInstanceProperty> instanceProperty;
    @XmlElement(name = "ActionType")
    protected String actionType;

    /**
     * Gets the value of the offeringId property.
     * 
     * @return
     *     possible object is
     *     {@link OfferingId }
     *     
     */
    public OfferingId getOfferingId() {
        return offeringId;
    }

    /**
     * Sets the value of the offeringId property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingId }
     *     
     */
    public void setOfferingId(OfferingId value) {
        this.offeringId = value;
    }

    /**
     * Gets the value of the contract property.
     * 
     * @return
     *     possible object is
     *     {@link Contract }
     *     
     */
    public Contract getContract() {
        return contract;
    }

    /**
     * Sets the value of the contract property.
     * 
     * @param value
     *     allowed object is
     *     {@link Contract }
     *     
     */
    public void setContract(Contract value) {
        this.contract = value;
    }

    /**
     * Gets the value of the instanceProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the instanceProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstanceProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OfferingInstanceProperty }
     * 
     * 
     */
    public List<OfferingInstanceProperty> getInstanceProperty() {
        if (instanceProperty == null) {
            instanceProperty = new ArrayList<OfferingInstanceProperty>();
        }
        return this.instanceProperty;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

}
