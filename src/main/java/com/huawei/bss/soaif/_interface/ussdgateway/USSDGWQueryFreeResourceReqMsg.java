package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="QueryObj">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="SubAccessCode">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="PrimaryIdentity" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "queryObj" })
@XmlRootElement(name = "USSDGWQueryFreeResourceReqMsg")
public class USSDGWQueryFreeResourceReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "QueryObj", required = true)
	protected USSDGWQueryFreeResourceReqMsg.QueryObj queryObj;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the queryObj property.
	 * 
	 * @return possible object is
	 *         {@link USSDGWQueryFreeResourceReqMsg.QueryObj }
	 * 
	 */
	public USSDGWQueryFreeResourceReqMsg.QueryObj getQueryObj() {
		return queryObj;
	}

	/**
	 * Sets the value of the queryObj property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link USSDGWQueryFreeResourceReqMsg.QueryObj }
	 * 
	 */
	public void setQueryObj(USSDGWQueryFreeResourceReqMsg.QueryObj value) {
		this.queryObj = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="SubAccessCode">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="PrimaryIdentity" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "subAccessCode" })
	public static class QueryObj {
		@XmlElement(name = "SubAccessCode", required = true)
		protected USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode subAccessCode;

		/**
		 * Gets the value of the subAccessCode property.
		 * 
		 * @return possible object is
		 *         {@link USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode }
		 * 
		 */
		public USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode getSubAccessCode() {
			return subAccessCode;
		}

		/**
		 * Sets the value of the subAccessCode property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode }
		 * 
		 */
		public void setSubAccessCode(USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode value) {
			this.subAccessCode = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="PrimaryIdentity" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "primaryIdentity" })
		public static class SubAccessCode {
			@XmlElement(name = "PrimaryIdentity", required = true)
			protected String primaryIdentity;

			/**
			 * Gets the value of the primaryIdentity property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPrimaryIdentity() {
				return primaryIdentity;
			}

			/**
			 * Sets the value of the primaryIdentity property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPrimaryIdentity(String value) {
				this.primaryIdentity = value;
			}
		}
	}
}
