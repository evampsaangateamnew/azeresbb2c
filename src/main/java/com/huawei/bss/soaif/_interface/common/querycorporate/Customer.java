
package com.huawei.bss.soaif._interface.common.querycorporate;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Customer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Customer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId" minOccurs="0"/>
 *         &lt;element name="CustomerCode" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerCode" minOccurs="0"/>
 *         &lt;element name="CustomerType" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerType" minOccurs="0"/>
 *         &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
 *         &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate" minOccurs="0"/>
 *         &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
 *         &lt;element name="PINNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}PINNumber" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="100" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}Language" minOccurs="0"/>
 *         &lt;element name="Gender" type="{http://www.huawei.com/bss/soaif/interface/common/}Gender" minOccurs="0"/>
 *         &lt;element name="PlaceOfBirth" type="{http://www.huawei.com/bss/soaif/interface/common/}PlaceOfBirth" minOccurs="0"/>
 *         &lt;element name="Occupation" type="{http://www.huawei.com/bss/soaif/interface/common/}Occupation" minOccurs="0"/>
 *         &lt;element name="TaxCertificateID" type="{http://www.huawei.com/bss/soaif/interface/common/}TaxCertificateID" minOccurs="0"/>
 *         &lt;element name="Citizenship" type="{http://www.huawei.com/bss/soaif/interface/common/}Citizenship" minOccurs="0"/>
 *         &lt;element name="SendToLegal" type="{http://www.huawei.com/bss/soaif/interface/common/}SendToLegal" minOccurs="0"/>
 *         &lt;element name="FatherName" type="{http://www.huawei.com/bss/soaif/interface/common/}FatherName" minOccurs="0"/>
 *         &lt;element name="ContactNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactNumber" minOccurs="0"/>
 *         &lt;element name="CustomerDocumentList" type="{http://www.huawei.com/bss/soaif/interface/common/}CustomerDocumentInfo" maxOccurs="100" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer", propOrder = {
    "custId",
    "customerCode",
    "customerType",
    "custLevel",
    "title",
    "name",
    "nationality",
    "certificate",
    "birthday",
    "pinNumber",
    "address",
    "additionalProperty",
    "language",
    "writtenLanguage",
    "gender",
    "placeOfBirth",
    "occupation",
    "taxCertificateID",
    "citizenship",
    "sendToLegal",
    "fatherName",
    "contactNumber",
    "customerDocumentList"
})
public class Customer {

    @XmlElement(name = "CustId")
    protected String custId;
    @XmlElement(name = "CustomerCode")
    protected String customerCode;
    @XmlElement(name = "CustomerType")
    protected String customerType;
    @XmlElement(name = "CustLevel")
    protected String custLevel;
    @XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/common/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElement(name = "Name")
    protected Name name;
    @XmlElementRef(name = "Nationality", namespace = "http://www.huawei.com/bss/soaif/interface/common/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nationality;
    @XmlElementRef(name = "Certificate", namespace = "http://www.huawei.com/bss/soaif/interface/common/", type = JAXBElement.class, required = false)
    protected JAXBElement<Certificate> certificate;
    @XmlElement(name = "Birthday")
    protected String birthday;
    @XmlElement(name = "PINNumber")
    protected String pinNumber;
    @XmlElement(name = "Address")
    protected List<Address> address;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "WrittenLanguage")
    protected String writtenLanguage;
    @XmlElement(name = "Gender")
    protected String gender;
    @XmlElement(name = "PlaceOfBirth")
    protected String placeOfBirth;
    @XmlElement(name = "Occupation")
    protected String occupation;
    @XmlElement(name = "TaxCertificateID")
    protected String taxCertificateID;
    @XmlElement(name = "Citizenship")
    protected String citizenship;
    @XmlElement(name = "SendToLegal")
    protected String sendToLegal;
    @XmlElement(name = "FatherName")
    protected String fatherName;
    @XmlElement(name = "ContactNumber")
    protected BigInteger contactNumber;
    @XmlElement(name = "CustomerDocumentList")
    protected List<CustomerDocumentInfo> customerDocumentList;

    /**
     * Gets the value of the custId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustId() {
        return custId;
    }

    /**
     * Sets the value of the custId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustId(String value) {
        this.custId = value;
    }

    /**
     * Gets the value of the customerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Sets the value of the customerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCode(String value) {
        this.customerCode = value;
    }

    /**
     * Gets the value of the customerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerType() {
        return customerType;
    }

    /**
     * Sets the value of the customerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerType(String value) {
        this.customerType = value;
    }

    /**
     * Gets the value of the custLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLevel() {
        return custLevel;
    }

    /**
     * Sets the value of the custLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLevel(String value) {
        this.custLevel = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setName(Name value) {
        this.name = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNationality(JAXBElement<String> value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the certificate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Certificate }{@code >}
     *     
     */
    public JAXBElement<Certificate> getCertificate() {
        return certificate;
    }

    /**
     * Sets the value of the certificate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Certificate }{@code >}
     *     
     */
    public void setCertificate(JAXBElement<Certificate> value) {
        this.certificate = value;
    }

    /**
     * Gets the value of the birthday property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * Sets the value of the birthday property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthday(String value) {
        this.birthday = value;
    }

    /**
     * Gets the value of the pinNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPINNumber() {
        return pinNumber;
    }

    /**
     * Sets the value of the pinNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPINNumber(String value) {
        this.pinNumber = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the address property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Address }
     * 
     * 
     */
    public List<Address> getAddress() {
        if (address == null) {
            address = new ArrayList<Address>();
        }
        return this.address;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the writtenLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWrittenLanguage() {
        return writtenLanguage;
    }

    /**
     * Sets the value of the writtenLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWrittenLanguage(String value) {
        this.writtenLanguage = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the placeOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    /**
     * Sets the value of the placeOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceOfBirth(String value) {
        this.placeOfBirth = value;
    }

    /**
     * Gets the value of the occupation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * Sets the value of the occupation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupation(String value) {
        this.occupation = value;
    }

    /**
     * Gets the value of the taxCertificateID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCertificateID() {
        return taxCertificateID;
    }

    /**
     * Sets the value of the taxCertificateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCertificateID(String value) {
        this.taxCertificateID = value;
    }

    /**
     * Gets the value of the citizenship property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCitizenship() {
        return citizenship;
    }

    /**
     * Sets the value of the citizenship property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCitizenship(String value) {
        this.citizenship = value;
    }

    /**
     * Gets the value of the sendToLegal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendToLegal() {
        return sendToLegal;
    }

    /**
     * Sets the value of the sendToLegal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendToLegal(String value) {
        this.sendToLegal = value;
    }

    /**
     * Gets the value of the fatherName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * Sets the value of the fatherName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFatherName(String value) {
        this.fatherName = value;
    }

    /**
     * Gets the value of the contactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getContactNumber() {
        return contactNumber;
    }

    /**
     * Sets the value of the contactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setContactNumber(BigInteger value) {
        this.contactNumber = value;
    }

    /**
     * Gets the value of the customerDocumentList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customerDocumentList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomerDocumentList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerDocumentInfo }
     * 
     * 
     */
    public List<CustomerDocumentInfo> getCustomerDocumentList() {
        if (customerDocumentList == null) {
            customerDocumentList = new ArrayList<CustomerDocumentInfo>();
        }
        return this.customerDocumentList;
    }

}
