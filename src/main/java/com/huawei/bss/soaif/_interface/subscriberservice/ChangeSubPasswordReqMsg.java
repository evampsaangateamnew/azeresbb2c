package com.huawei.bss.soaif._interface.subscriberservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo" type="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo"/>
 *           &lt;sequence>
 *             &lt;element name="OldPassword" type="{http://www.huawei.com/bss/soaif/interface/common/}Password"/>
 *             &lt;element name="NewPassword" type="{http://www.huawei.com/bss/soaif/interface/common/}Password"/>
 *           &lt;/sequence>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "oldPassword", "newPassword" })
@XmlRootElement(name = "ChangeSubPasswordReqMsg")
public class ChangeSubPasswordReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected ObjectAccessInfo accessInfo;
	@XmlElement(name = "OldPassword", required = true)
	protected String oldPassword;
	@XmlElement(name = "NewPassword", required = true)
	protected String newPassword;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is {@link ObjectAccessInfo }
	 * 
	 */
	public ObjectAccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ObjectAccessInfo }
	 * 
	 */
	public void setAccessInfo(ObjectAccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the oldPassword property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * Sets the value of the oldPassword property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOldPassword(String value) {
		this.oldPassword = value;
	}

	/**
	 * Gets the value of the newPassword property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * Sets the value of the newPassword property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNewPassword(String value) {
		this.newPassword = value;
	}
}
