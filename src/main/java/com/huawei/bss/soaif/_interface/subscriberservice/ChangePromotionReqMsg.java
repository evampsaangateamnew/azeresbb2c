package com.huawei.bss.soaif._interface.subscriberservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo" type="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo"/>
 *           &lt;element name="Promotion" maxOccurs="unbounded">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="PromotionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MgmNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RewardItemId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "promotion" })
@XmlRootElement(name = "ChangePromotionReqMsg")
public class ChangePromotionReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected ObjectAccessInfo accessInfo;
	@XmlElement(name = "Promotion", required = true)
	protected List<ChangePromotionReqMsg.Promotion> promotion;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is {@link ObjectAccessInfo }
	 * 
	 */
	public ObjectAccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ObjectAccessInfo }
	 * 
	 */
	public void setAccessInfo(ObjectAccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the promotion property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the promotion property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getPromotion().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ChangePromotionReqMsg.Promotion }
	 * 
	 * 
	 */
	public List<ChangePromotionReqMsg.Promotion> getPromotion() {
		if (promotion == null) {
			promotion = new ArrayList<ChangePromotionReqMsg.Promotion>();
		}
		return this.promotion;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="PromotionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="MgmNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="RewardItemId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "promotionId", "promotionCode", "mgmNum", "rewardItemId" })
	public static class Promotion {
		@XmlElement(name = "PromotionId", required = true)
		protected String promotionId;
		@XmlElement(name = "PromotionCode")
		protected String promotionCode;
		@XmlElement(name = "MgmNum")
		protected String mgmNum;
		@XmlElement(name = "RewardItemId")
		protected List<String> rewardItemId;

		/**
		 * Gets the value of the promotionId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPromotionId() {
			return promotionId;
		}

		/**
		 * Sets the value of the promotionId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPromotionId(String value) {
			this.promotionId = value;
		}

		/**
		 * Gets the value of the promotionCode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPromotionCode() {
			return promotionCode;
		}

		/**
		 * Sets the value of the promotionCode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPromotionCode(String value) {
			this.promotionCode = value;
		}

		/**
		 * Gets the value of the mgmNum property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMgmNum() {
			return mgmNum;
		}

		/**
		 * Sets the value of the mgmNum property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setMgmNum(String value) {
			this.mgmNum = value;
		}

		/**
		 * Gets the value of the rewardItemId property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the rewardItemId property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getRewardItemId().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link String }
		 * 
		 * 
		 */
		public List<String> getRewardItemId() {
			if (rewardItemId == null) {
				rewardItemId = new ArrayList<String>();
			}
			return this.rewardItemId;
		}
	}
}
