
package com.huawei.bss.soaif._interface.common.createorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentRelation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentRelation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentRelationId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ServiceType" maxOccurs="100" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PaymentLimit" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitUnit" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="1"/>
 *                         &lt;enumeration value="2"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="LimitPattern" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="1"/>
 *                         &lt;enumeration value="2"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="LimitValue" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="64"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="LimitMeasureUnit" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="10"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ValidPeriod" type="{http://www.huawei.com/bss/soaif/interface/common/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *         &lt;element name="TimeSchemaId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentRelation", propOrder = {
    "paymentRelationId",
    "serviceType",
    "paymentLimit",
    "validPeriod",
    "actionType",
    "timeSchemaId"
})
public class PaymentRelation {

    @XmlElement(name = "PaymentRelationId")
    protected String paymentRelationId;
    @XmlElement(name = "ServiceType")
    protected List<String> serviceType;
    @XmlElement(name = "PaymentLimit")
    protected PaymentRelation.PaymentLimit paymentLimit;
    @XmlElement(name = "ValidPeriod")
    protected TimePeriod validPeriod;
    @XmlElement(name = "ActionType", required = true)
    protected String actionType;
    @XmlElement(name = "TimeSchemaId")
    protected String timeSchemaId;

    /**
     * Gets the value of the paymentRelationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentRelationId() {
        return paymentRelationId;
    }

    /**
     * Sets the value of the paymentRelationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentRelationId(String value) {
        this.paymentRelationId = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getServiceType() {
        if (serviceType == null) {
            serviceType = new ArrayList<String>();
        }
        return this.serviceType;
    }

    /**
     * Gets the value of the paymentLimit property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentRelation.PaymentLimit }
     *     
     */
    public PaymentRelation.PaymentLimit getPaymentLimit() {
        return paymentLimit;
    }

    /**
     * Sets the value of the paymentLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentRelation.PaymentLimit }
     *     
     */
    public void setPaymentLimit(PaymentRelation.PaymentLimit value) {
        this.paymentLimit = value;
    }

    /**
     * Gets the value of the validPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link TimePeriod }
     *     
     */
    public TimePeriod getValidPeriod() {
        return validPeriod;
    }

    /**
     * Sets the value of the validPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimePeriod }
     *     
     */
    public void setValidPeriod(TimePeriod value) {
        this.validPeriod = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the timeSchemaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeSchemaId() {
        return timeSchemaId;
    }

    /**
     * Sets the value of the timeSchemaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeSchemaId(String value) {
        this.timeSchemaId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitUnit" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="1"/>
     *               &lt;enumeration value="2"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="LimitPattern" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="1"/>
     *               &lt;enumeration value="2"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="LimitValue" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="64"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="LimitMeasureUnit" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="10"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitUnit",
        "limitPattern",
        "limitValue",
        "limitMeasureUnit"
    })
    public static class PaymentLimit {

        @XmlElement(name = "LimitUnit")
        protected String limitUnit;
        @XmlElement(name = "LimitPattern")
        protected String limitPattern;
        @XmlElement(name = "LimitValue")
        protected String limitValue;
        @XmlElement(name = "LimitMeasureUnit")
        protected String limitMeasureUnit;

        /**
         * Gets the value of the limitUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitUnit() {
            return limitUnit;
        }

        /**
         * Sets the value of the limitUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitUnit(String value) {
            this.limitUnit = value;
        }

        /**
         * Gets the value of the limitPattern property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitPattern() {
            return limitPattern;
        }

        /**
         * Sets the value of the limitPattern property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitPattern(String value) {
            this.limitPattern = value;
        }

        /**
         * Gets the value of the limitValue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitValue() {
            return limitValue;
        }

        /**
         * Sets the value of the limitValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitValue(String value) {
            this.limitValue = value;
        }

        /**
         * Gets the value of the limitMeasureUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitMeasureUnit() {
            return limitMeasureUnit;
        }

        /**
         * Sets the value of the limitMeasureUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitMeasureUnit(String value) {
            this.limitMeasureUnit = value;
        }

    }

}
