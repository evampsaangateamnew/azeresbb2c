package com.huawei.bss.soaif._interface.hlrwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.RspHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;element name="getSubscriberBody" type="{http://www.huawei.com/bss/soaif/interface/HLRWEBService/}getSubscriberOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rspHeader", "getSubscriberBody" })
@XmlRootElement(name = "getSubscriberResponse")
public class GetSubscriberResponse {
	@XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected RspHeader rspHeader;
	@XmlElement(name = "GetSubscriberBody", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = false)
	protected GetSubscriberOut getSubscriberBody;

	/**
	 * Gets the value of the rspHeader property.
	 * 
	 * @return possible object is {@link RspHeader }
	 * 
	 */
	public RspHeader getRspHeader() {
		return rspHeader;
	}

	/**
	 * Sets the value of the rspHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RspHeader }
	 * 
	 */
	public void setRspHeader(RspHeader value) {
		this.rspHeader = value;
	}

	/**
	 * Gets the value of the getSubscriberBody property.
	 * 
	 * @return possible object is {@link GetSubscriberOut }
	 * 
	 */
	public GetSubscriberOut getGetSubscriberBody() {
		return getSubscriberBody;
	}

	/**
	 * Sets the value of the getSubscriberBody property.
	 * 
	 * @param value
	 *            allowed object is {@link GetSubscriberOut }
	 * 
	 */
	public void setGetSubscriberBody(GetSubscriberOut value) {
		this.getSubscriberBody = value;
	}
}
