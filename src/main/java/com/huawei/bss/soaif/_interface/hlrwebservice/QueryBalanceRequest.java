package com.huawei.bss.soaif._interface.hlrwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;element name="queryBalanceBody" type="{http://www.huawei.com/bss/soaif/interface/HLRWEBService/}queryBalanceIn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "queryBalanceBody" })
@XmlRootElement(name = "queryBalanceRequest")
public class QueryBalanceRequest {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(required = true)
	protected QueryBalanceIn queryBalanceBody;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the queryBalanceBody property.
	 * 
	 * @return possible object is {@link QueryBalanceIn }
	 * 
	 */
	public QueryBalanceIn getQueryBalanceBody() {
		return queryBalanceBody;
	}

	/**
	 * Sets the value of the queryBalanceBody property.
	 * 
	 * @param value
	 *            allowed object is {@link QueryBalanceIn }
	 * 
	 */
	public void setQueryBalanceBody(QueryBalanceIn value) {
		this.queryBalanceBody = value;
	}
}
