package com.huawei.bss.soaif._interface.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Contact complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Contact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContactId" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactId" minOccurs="0"/>
 *         &lt;element name="Priority" type="{http://www.huawei.com/bss/soaif/interface/common/}Priority" minOccurs="0"/>
 *         &lt;element name="ContactType" type="{http://www.huawei.com/bss/soaif/interface/common/}ContactType" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *         &lt;element name="OfficePhone" type="{http://www.huawei.com/bss/soaif/interface/common/}Phone" minOccurs="0"/>
 *         &lt;element name="HomePhone" type="{http://www.huawei.com/bss/soaif/interface/common/}Phone" minOccurs="0"/>
 *         &lt;element name="MobilePhone" type="{http://www.huawei.com/bss/soaif/interface/common/}Phone" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.huawei.com/bss/soaif/interface/common/}Email" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contact", propOrder = { "contactId", "priority", "contactType", "title", "name", "officePhone",
		"homePhone", "mobilePhone", "email" })
@XmlSeeAlso({ com.huawei.bss.soaif._interface.subscriberservice.SupplementCustInfoReqMsg.Customer.Contact.class,
		com.huawei.bss.soaif._interface.subscriberservice.SupplementCustInfoReqMsg.Account.Contact.class,
		com.huawei.bss.soaif._interface.subscriberservice.ChangePrepaidToPostpaidReqMsg.PostpaidCust.Contact.class })
public class Contact {
	@XmlElement(name = "ContactId")
	protected String contactId;
	@XmlElement(name = "Priority")
	protected String priority;
	@XmlElement(name = "ContactType")
	protected String contactType;
	@XmlElement(name = "Title")
	protected String title;
	@XmlElement(name = "Name")
	protected Name name;
	@XmlElement(name = "OfficePhone")
	protected String officePhone;
	@XmlElement(name = "HomePhone")
	protected String homePhone;
	@XmlElement(name = "MobilePhone")
	protected String mobilePhone;
	@XmlElement(name = "Email")
	protected String email;

	/**
	 * Gets the value of the contactId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactId() {
		return contactId;
	}

	/**
	 * Sets the value of the contactId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactId(String value) {
		this.contactId = value;
	}

	/**
	 * Gets the value of the priority property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * Sets the value of the priority property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPriority(String value) {
		this.priority = value;
	}

	/**
	 * Gets the value of the contactType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContactType() {
		return contactType;
	}

	/**
	 * Sets the value of the contactType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContactType(String value) {
		this.contactType = value;
	}

	/**
	 * Gets the value of the title property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTitle(String value) {
		this.title = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link Name }
	 * 
	 */
	public Name getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link Name }
	 * 
	 */
	public void setName(Name value) {
		this.name = value;
	}

	/**
	 * Gets the value of the officePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfficePhone() {
		return officePhone;
	}

	/**
	 * Sets the value of the officePhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfficePhone(String value) {
		this.officePhone = value;
	}

	/**
	 * Gets the value of the homePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * Sets the value of the homePhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setHomePhone(String value) {
		this.homePhone = value;
	}

	/**
	 * Gets the value of the mobilePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * Sets the value of the mobilePhone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMobilePhone(String value) {
		this.mobilePhone = value;
	}

	/**
	 * Gets the value of the email property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the value of the email property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEmail(String value) {
		this.email = value;
	}
}
