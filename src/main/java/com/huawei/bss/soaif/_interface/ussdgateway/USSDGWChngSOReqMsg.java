package com.huawei.bss.soaif._interface.ussdgateway;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="ServiceNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *           &lt;element name="IsConfirmed" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *           &lt;element name="SupplementaryOfferingList" maxOccurs="unbounded">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="OfferingId">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="OfferingId" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *                               &lt;element name="OfferingCode" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *                     &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "serviceNumber", "isConfirmed", "supplementaryOfferingList" })
@XmlRootElement(name = "USSDGWChngSOReqMsg")
public class USSDGWChngSOReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "ServiceNumber", required = true)
	protected String serviceNumber;
	@XmlElement(name = "IsConfirmed")
	protected String isConfirmed;
	@XmlElement(name = "SupplementaryOfferingList", required = true)
	protected List<USSDGWChngSOReqMsg.SupplementaryOfferingList> supplementaryOfferingList;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the isConfirmed property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIsConfirmed() {
		return isConfirmed;
	}

	/**
	 * Sets the value of the isConfirmed property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIsConfirmed(String value) {
		this.isConfirmed = value;
	}

	/**
	 * Gets the value of the supplementaryOfferingList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the supplementaryOfferingList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getSupplementaryOfferingList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link USSDGWChngSOReqMsg.SupplementaryOfferingList }
	 * 
	 * 
	 */
	public List<USSDGWChngSOReqMsg.SupplementaryOfferingList> getSupplementaryOfferingList() {
		if (supplementaryOfferingList == null) {
			supplementaryOfferingList = new ArrayList<USSDGWChngSOReqMsg.SupplementaryOfferingList>();
		}
		return this.supplementaryOfferingList;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="OfferingId">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="OfferingId" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
	 *                   &lt;element name="OfferingCode" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
	 *         &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "offeringId", "actionType", "remark" })
	public static class SupplementaryOfferingList {
		@XmlElement(name = "OfferingId", required = true)
		protected USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId offeringId;
		@XmlElement(name = "ActionType")
		protected String actionType;
		@XmlElement(name = "Remark")
		protected String remark;

		/**
		 * Gets the value of the offeringId property.
		 * 
		 * @return possible object is
		 *         {@link USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId }
		 * 
		 */
		public USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId getOfferingId() {
			return offeringId;
		}

		/**
		 * Sets the value of the offeringId property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId }
		 * 
		 */
		public void setOfferingId(USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId value) {
			this.offeringId = value;
		}

		/**
		 * Gets the value of the actionType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getActionType() {
			return actionType;
		}

		/**
		 * Sets the value of the actionType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setActionType(String value) {
			this.actionType = value;
		}

		/**
		 * Gets the value of the remark property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRemark() {
			return remark;
		}

		/**
		 * Sets the value of the remark property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRemark(String value) {
			this.remark = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="OfferingId" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
		 *         &lt;element name="OfferingCode" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "offeringId", "offeringCode" })
		public static class OfferingId {
			@XmlElement(name = "OfferingId")
			protected String offeringId;
			@XmlElement(name = "OfferingCode")
			protected String offeringCode;

			/**
			 * Gets the value of the offeringId property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getOfferingId() {
				return offeringId;
			}

			/**
			 * Sets the value of the offeringId property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setOfferingId(String value) {
				this.offeringId = value;
			}

			/**
			 * Gets the value of the offeringCode property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getOfferingCode() {
				return offeringCode;
			}

			/**
			 * Sets the value of the offeringCode property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setOfferingCode(String value) {
				this.offeringCode = value;
			}
		}
	}
}
