package com.huawei.bss.soaif._interface.subscriberservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo" type="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo"/>
 *           &lt;element name="OperateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="ChangeSimCardFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="ICCID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="OldICCID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="ResumeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "operateType", "reasonCode", "changeSimCardFlag", "iccid",
		"oldICCID", "resumeDate" })
@XmlRootElement(name = "ChangeSubStatusReqMsg")
public class ChangeSubStatusReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected ObjectAccessInfo accessInfo;
	@XmlElement(name = "OperateType", required = true)
	protected String operateType;
	@XmlElement(name = "ReasonCode")
	protected String reasonCode;
	@XmlElement(name = "ChangeSimCardFlag")
	protected String changeSimCardFlag;
	@XmlElement(name = "ICCID")
	protected String iccid;
	@XmlElement(name = "OldICCID")
	protected String oldICCID;
	@XmlElement(name = "ResumeDate")
	protected String resumeDate;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is {@link ObjectAccessInfo }
	 * 
	 */
	public ObjectAccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ObjectAccessInfo }
	 * 
	 */
	public void setAccessInfo(ObjectAccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the operateType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOperateType() {
		return operateType;
	}

	/**
	 * Sets the value of the operateType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOperateType(String value) {
		this.operateType = value;
	}

	/**
	 * Gets the value of the reasonCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReasonCode() {
		return reasonCode;
	}

	/**
	 * Sets the value of the reasonCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setReasonCode(String value) {
		this.reasonCode = value;
	}

	/**
	 * Gets the value of the changeSimCardFlag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getChangeSimCardFlag() {
		return changeSimCardFlag;
	}

	/**
	 * Sets the value of the changeSimCardFlag property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setChangeSimCardFlag(String value) {
		this.changeSimCardFlag = value;
	}

	/**
	 * Gets the value of the iccid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getICCID() {
		return iccid;
	}

	/**
	 * Sets the value of the iccid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setICCID(String value) {
		this.iccid = value;
	}

	/**
	 * Gets the value of the oldICCID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOldICCID() {
		return oldICCID;
	}

	/**
	 * Sets the value of the oldICCID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOldICCID(String value) {
		this.oldICCID = value;
	}

	/**
	 * Gets the value of the resumeDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getResumeDate() {
		return resumeDate;
	}

	/**
	 * Sets the value of the resumeDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setResumeDate(String value) {
		this.resumeDate = value;
	}
}
