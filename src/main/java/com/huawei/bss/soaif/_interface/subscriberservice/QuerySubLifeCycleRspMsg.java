package com.huawei.bss.soaif._interface.subscriberservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.LifeCycleStatus;
import com.huawei.bss.soaif._interface.common.RspHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}RspHeader"/>
 *         &lt;sequence>
 *           &lt;element name="CurrentStatusIndex" type="{http://www.huawei.com/bss/soaif/interface/common/}SubStatus"/>
 *           &lt;element name="LifeCycleStatus" type="{http://www.huawei.com/bss/soaif/interface/common/}LifeCycleStatus" maxOccurs="unbounded"/>
 *           &lt;element name="IsBlacklistStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="FraudTimes" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rspHeader", "currentStatusIndex", "lifeCycleStatus", "isBlacklistStatus",
		"fraudTimes" })
@XmlRootElement(name = "QuerySubLifeCycleRspMsg")
public class QuerySubLifeCycleRspMsg {
	@XmlElement(name = "RspHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected RspHeader rspHeader;
	@XmlElement(name = "CurrentStatusIndex", required = true)
	protected String currentStatusIndex;
	@XmlElement(name = "LifeCycleStatus", required = true)
	protected List<LifeCycleStatus> lifeCycleStatus;
	@XmlElement(name = "IsBlacklistStatus", required = true)
	protected String isBlacklistStatus;
	@XmlElement(name = "FraudTimes", required = true)
	protected BigInteger fraudTimes;

	/**
	 * Gets the value of the rspHeader property.
	 * 
	 * @return possible object is {@link RspHeader }
	 * 
	 */
	public RspHeader getRspHeader() {
		return rspHeader;
	}

	/**
	 * Sets the value of the rspHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link RspHeader }
	 * 
	 */
	public void setRspHeader(RspHeader value) {
		this.rspHeader = value;
	}

	/**
	 * Gets the value of the currentStatusIndex property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrentStatusIndex() {
		return currentStatusIndex;
	}

	/**
	 * Sets the value of the currentStatusIndex property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCurrentStatusIndex(String value) {
		this.currentStatusIndex = value;
	}

	/**
	 * Gets the value of the lifeCycleStatus property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the lifeCycleStatus property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getLifeCycleStatus().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link LifeCycleStatus }
	 * 
	 * 
	 */
	public List<LifeCycleStatus> getLifeCycleStatus() {
		if (lifeCycleStatus == null) {
			lifeCycleStatus = new ArrayList<LifeCycleStatus>();
		}
		return this.lifeCycleStatus;
	}

	/**
	 * Gets the value of the isBlacklistStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIsBlacklistStatus() {
		return isBlacklistStatus;
	}

	/**
	 * Sets the value of the isBlacklistStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIsBlacklistStatus(String value) {
		this.isBlacklistStatus = value;
	}

	/**
	 * Gets the value of the fraudTimes property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getFraudTimes() {
		return fraudTimes;
	}

	/**
	 * Sets the value of the fraudTimes property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setFraudTimes(BigInteger value) {
		this.fraudTimes = value;
	}
}
