package com.huawei.bss.soaif._interface.hlrwebservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for getSubscriberOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="getSubscriberOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubscriberId" type="{http://www.huawei.com/bss/soaif/interface/common/}long" minOccurs="0"/>
 *         &lt;element name="AccountId" type="{http://www.huawei.com/bss/soaif/interface/common/}long" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}long" minOccurs="0"/>
 *         &lt;element name="ServiceNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="IMSI" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="ICCID" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="BrandId" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="WrittenLanguage" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="ExpireDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="SubscriberType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="NetworkType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="actualCustomerId" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="IVRBlacklist" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="StatusDetail" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="subscriberLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="PrimaryOffering" type="{http://www.huawei.com/bss/soaif/interface/HLRWEBService/}OfferingInfo" minOccurs="0"/>
 *         &lt;element name="SupplementaryOfferingList" type="{http://www.huawei.com/bss/soaif/interface/HLRWEBService/}OfferingInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LoyaltySegment" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSubscriberOut", propOrder = { "subscriberId", "accountId", "customerId", "serviceNumber", "imsi",
		"iccid", "brandId", "language", "writtenLanguage", "effectiveDate", "expireDate", "subscriberType",
		"networkType", "status", "actualCustomerId", "ivrBlacklist", "password", "statusDetail", "subscriberLevel",
		"primaryOffering", "supplementaryOfferingList", "loyaltySegment" })
public class GetSubscriberOut {
	@XmlElement(name = "SubscriberId")
	protected Long subscriberId;
	@XmlElement(name = "AccountId")
	protected Long accountId;
	@XmlElement(name = "CustomerId")
	protected Long customerId;
	@XmlElement(name = "ServiceNumber")
	protected String serviceNumber;
	@XmlElement(name = "IMSI")
	protected String imsi;
	@XmlElement(name = "ICCID")
	protected String iccid;
	@XmlElement(name = "BrandId")
	protected String brandId;
	@XmlElement(name = "Language")
	protected String language;
	@XmlElement(name = "WrittenLanguage")
	protected String writtenLanguage;
	@XmlElement(name = "EffectiveDate")
	protected String effectiveDate;
	@XmlElement(name = "ExpireDate")
	protected String expireDate;
	@XmlElement(name = "SubscriberType")
	protected String subscriberType;
	@XmlElement(name = "NetworkType")
	protected String networkType;
	@XmlElement(name = "Status")
	protected String status;
	protected String actualCustomerId;
	@XmlElement(name = "IVRBlacklist")
	protected String ivrBlacklist;
	protected String password;
	@XmlElement(name = "StatusDetail")
	protected String statusDetail;
	protected String subscriberLevel;
	@XmlElement(name = "PrimaryOffering")
	protected OfferingInfo primaryOffering;
	@XmlElement(name = "SupplementaryOfferingList")
	protected List<OfferingInfo> supplementaryOfferingList;
	@XmlElement(name = "LoyaltySegment")
	protected String loyaltySegment;

	//
	// copy paste
	/**
	 * Gets the value of the subscriberId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Sets the value of the subscriberId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setSubscriberId(Long value) {
		this.subscriberId = value;
	}

	/**
	 * Gets the value of the accountId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * Sets the value of the accountId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAccountId(Long value) {
		this.accountId = value;
	}

	/**
	 * Gets the value of the customerId property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the value of the customerId property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCustomerId(Long value) {
		this.customerId = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the imsi property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIMSI() {
		return imsi;
	}

	/**
	 * Sets the value of the imsi property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIMSI(String value) {
		this.imsi = value;
	}

	/**
	 * Gets the value of the iccid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getICCID() {
		return iccid;
	}

	/**
	 * Sets the value of the iccid property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setICCID(String value) {
		this.iccid = value;
	}

	/**
	 * Gets the value of the brandId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBrandId() {
		return brandId;
	}

	/**
	 * Sets the value of the brandId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBrandId(String value) {
		this.brandId = value;
	}

	/**
	 * Gets the value of the language property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the value of the language property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLanguage(String value) {
		this.language = value;
	}

	/**
	 * Gets the value of the writtenLanguage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWrittenLanguage() {
		return writtenLanguage;
	}

	/**
	 * Sets the value of the writtenLanguage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWrittenLanguage(String value) {
		this.writtenLanguage = value;
	}

	/**
	 * Gets the value of the effectiveDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Sets the value of the effectiveDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveDate(String value) {
		this.effectiveDate = value;
	}

	/**
	 * Gets the value of the expireDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * Sets the value of the expireDate property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpireDate(String value) {
		this.expireDate = value;
	}

	/**
	 * Gets the value of the subscriberType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubscriberType() {
		return subscriberType;
	}

	/**
	 * Sets the value of the subscriberType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubscriberType(String value) {
		this.subscriberType = value;
	}

	/**
	 * Gets the value of the networkType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetworkType() {
		return networkType;
	}

	/**
	 * Sets the value of the networkType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetworkType(String value) {
		this.networkType = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the actualCustomerId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getActualCustomerId() {
		return actualCustomerId;
	}

	/**
	 * Sets the value of the actualCustomerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setActualCustomerId(String value) {
		this.actualCustomerId = value;
	}

	/**
	 * Gets the value of the ivrBlacklist property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIVRBlacklist() {
		return ivrBlacklist;
	}

	/**
	 * Sets the value of the ivrBlacklist property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIVRBlacklist(String value) {
		this.ivrBlacklist = value;
	}

	/**
	 * Gets the value of the password property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the value of the password property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPassword(String value) {
		this.password = value;
	}

	/**
	 * Gets the value of the statusDetail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatusDetail() {
		return statusDetail;
	}

	/**
	 * Sets the value of the statusDetail property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatusDetail(String value) {
		this.statusDetail = value;
	}

	/**
	 * Gets the value of the subscriberLevel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSubscriberLevel() {
		return subscriberLevel;
	}

	/**
	 * Sets the value of the subscriberLevel property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSubscriberLevel(String value) {
		this.subscriberLevel = value;
	}

	/**
	 * Gets the value of the primaryOffering property.
	 * 
	 * @return possible object is {@link OfferingInfo }
	 * 
	 */
	public OfferingInfo getPrimaryOffering() {
		return primaryOffering;
	}

	/**
	 * Sets the value of the primaryOffering property.
	 * 
	 * @param value
	 *            allowed object is {@link OfferingInfo }
	 * 
	 */
	public void setPrimaryOffering(OfferingInfo value) {
		this.primaryOffering = value;
	}

	/**
	 * Gets the value of the supplementaryOfferingList property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the supplementaryOfferingList property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getSupplementaryOfferingList().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link OfferingInfo }
	 * 
	 * 
	 */
	public List<OfferingInfo> getSupplementaryOfferingList() {
		if (supplementaryOfferingList == null) {
			supplementaryOfferingList = new ArrayList<OfferingInfo>();
		}
		return this.supplementaryOfferingList;
	}

	/**
	 * Gets the value of the loyaltySegment property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLoyaltySegment() {
		return loyaltySegment;
	}

	/**
	 * Sets the value of the loyaltySegment property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLoyaltySegment(String value) {
		this.loyaltySegment = value;
	}
}
