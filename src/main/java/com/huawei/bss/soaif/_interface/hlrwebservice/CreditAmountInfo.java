package com.huawei.bss.soaif._interface.hlrwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditAmountInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CreditAmountInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LimitClass" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.huawei.com/bss/soaif/interface/common/}long" minOccurs="0"/>
 *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="ExpireTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="CreditInstID" type="{http://www.huawei.com/bss/soaif/interface/common/}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditAmountInfo", propOrder = { "limitClass", "amount", "effectiveTime", "expireTime",
		"creditInstID" })
public class CreditAmountInfo {
	@XmlElement(name = "LimitClass")
	protected String limitClass;
	@XmlElement(name = "Amount")
	protected String amount;
	@XmlElement(name = "EffectiveTime")
	protected String effectiveTime;
	@XmlElement(name = "ExpireTime")
	protected String expireTime;
	@XmlElement(name = "CreditInstID")
	protected String creditInstID;

	/**
	 * Gets the value of the limitClass property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLimitClass() {
		return limitClass;
	}

	/**
	 * Sets the value of the limitClass property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLimitClass(String value) {
		this.limitClass = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setAmount(String value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the effectiveTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveTime() {
		return effectiveTime;
	}

	/**
	 * Sets the value of the effectiveTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveTime(String value) {
		this.effectiveTime = value;
	}

	/**
	 * Gets the value of the expireTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpireTime() {
		return expireTime;
	}

	/**
	 * Sets the value of the expireTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpireTime(String value) {
		this.expireTime = value;
	}

	/**
	 * Gets the value of the creditInstID property.
	 * 
	 * @return possible object is {@link Long }
	 * 
	 */
	public String getCreditInstID() {
		return creditInstID;
	}

	/**
	 * Sets the value of the creditInstID property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 * 
	 */
	public void setCreditInstID(String value) {
		this.creditInstID = value;
	}
}
