package com.huawei.bss.soaif._interface.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DurationUnit.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="DurationUnit">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Y"/>
 *     &lt;enumeration value="M"/>
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="W"/>
 *     &lt;enumeration value="D"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DurationUnit")
@XmlEnum
public enum DurationUnit {
	Y, M, C, W, D;
	public String value() {
		return name();
	}

	public static DurationUnit fromValue(String v) {
		return valueOf(v);
	}
}
