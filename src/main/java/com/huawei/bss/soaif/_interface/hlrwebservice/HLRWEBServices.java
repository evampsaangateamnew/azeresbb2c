package com.huawei.bss.soaif._interface.hlrwebservice;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;
import com.evampsaanga.configs.Config;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "HLRWEBServices", targetNamespace = "http://www.huawei.com/bss/soaif/interface/HLRWEBService/", wsdlLocation = "file:/D:/Projects/Evamp/HLR%20WEB/HLR%20WEB/HWBSS_HLRWEB_V10.wsdl")
public class HLRWEBServices extends Service {
	private final static URL HLRWEBSERVICES_WSDL_LOCATION;
	private final static WebServiceException HLRWEBSERVICES_EXCEPTION;
	private final static QName HLRWEBSERVICES_QNAME = new QName(
			"http://www.huawei.com/bss/soaif/interface/HLRWEBService/", "HLRWEBServices");
	static {
		URL url = null;
		WebServiceException e = null;
		try {
			url = new URL(Config.HLRWEB_WSDL_PATH);
		} catch (MalformedURLException ex) {
			e = new WebServiceException(ex);
		}
		HLRWEBSERVICES_WSDL_LOCATION = url;
		HLRWEBSERVICES_EXCEPTION = e;
	}

	public HLRWEBServices() {
		super(__getWsdlLocation(), HLRWEBSERVICES_QNAME);
	}

	public HLRWEBServices(WebServiceFeature... features) {
		super(__getWsdlLocation(), HLRWEBSERVICES_QNAME, features);
	}

	public HLRWEBServices(URL wsdlLocation) {
		super(wsdlLocation, HLRWEBSERVICES_QNAME);
	}

	public HLRWEBServices(URL wsdlLocation, WebServiceFeature... features) {
		super(wsdlLocation, HLRWEBSERVICES_QNAME, features);
	}

	public HLRWEBServices(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public HLRWEBServices(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	/**
	 * 
	 * @return returns HLRWEBPortType
	 */
	@WebEndpoint(name = "HLRWEBPort")
	public HLRWEBPortType getHLRWEBPort() {
		return super.getPort(new QName("http://www.huawei.com/bss/soaif/interface/HLRWEBService/", "HLRWEBPort"),
				HLRWEBPortType.class);
	}

	/**
	 * 
	 * @param features
	 *            A list of {@link javax.xml.ws.WebServiceFeature} to configure
	 *            on the proxy. Supported features not in the
	 *            <code>features</code> parameter will have their default
	 *            values.
	 * @return returns HLRWEBPortType
	 */
	@WebEndpoint(name = "HLRWEBPort")
	public HLRWEBPortType getHLRWEBPort(WebServiceFeature... features) {
		return super.getPort(new QName("http://www.huawei.com/bss/soaif/interface/HLRWEBService/", "HLRWEBPort"),
				HLRWEBPortType.class, features);
	}

	private static URL __getWsdlLocation() {
		if (HLRWEBSERVICES_EXCEPTION != null) {
			throw HLRWEBSERVICES_EXCEPTION;
		}
		return HLRWEBSERVICES_WSDL_LOCATION;
	}
}
