
package com.huawei.bss.soaif._interface.corporateservice;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.bss.soaif._interface.corporateservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.bss.soaif._interface.corporateservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChangeCorporateCustInfoReqMsg }
     * 
     */
    public ChangeCorporateCustInfoReqMsg createChangeCorporateCustInfoReqMsg() {
        return new ChangeCorporateCustInfoReqMsg();
    }

    /**
     * Create an instance of {@link ChangeCorporateCustInfoReqMsg.Customer }
     * 
     */
    public ChangeCorporateCustInfoReqMsg.Customer createChangeCorporateCustInfoReqMsgCustomer() {
        return new ChangeCorporateCustInfoReqMsg.Customer();
    }

    /**
     * Create an instance of {@link QueryCorporateCustReqMsg }
     * 
     */
    public QueryCorporateCustReqMsg createQueryCorporateCustReqMsg() {
        return new QueryCorporateCustReqMsg();
    }

    /**
     * Create an instance of {@link ChangeCorporateCustInfoRspMsg }
     * 
     */
    public ChangeCorporateCustInfoRspMsg createChangeCorporateCustInfoRspMsg() {
        return new ChangeCorporateCustInfoRspMsg();
    }

    /**
     * Create an instance of {@link QueryCorporateCustRspMsg }
     * 
     */
    public QueryCorporateCustRspMsg createQueryCorporateCustRspMsg() {
        return new QueryCorporateCustRspMsg();
    }

    /**
     * Create an instance of {@link ChangeCorporateCustInfoReqMsg.Customer.Address }
     * 
     */
    public ChangeCorporateCustInfoReqMsg.Customer.Address createChangeCorporateCustInfoReqMsgCustomerAddress() {
        return new ChangeCorporateCustInfoReqMsg.Customer.Address();
    }

    /**
     * Create an instance of {@link ChangeCorporateCustInfoReqMsg.Customer.Manager }
     * 
     */
    public ChangeCorporateCustInfoReqMsg.Customer.Manager createChangeCorporateCustInfoReqMsgCustomerManager() {
        return new ChangeCorporateCustInfoReqMsg.Customer.Manager();
    }

}
