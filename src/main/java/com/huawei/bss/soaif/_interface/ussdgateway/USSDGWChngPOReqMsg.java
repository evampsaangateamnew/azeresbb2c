package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="ServiceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="PrimaryOfferingInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="OfferingId">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="OfferingId" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *                               &lt;element name="OfferingCode" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *                     &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "serviceNumber", "primaryOfferingInfo" })
@XmlRootElement(name = "USSDGWChngPOReqMsg")
public class USSDGWChngPOReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "ServiceNumber", required = true)
	protected String serviceNumber;
	@XmlElement(name = "PrimaryOfferingInfo", required = true)
	protected USSDGWChngPOReqMsg.PrimaryOfferingInfo primaryOfferingInfo;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the serviceNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceNumber() {
		return serviceNumber;
	}

	/**
	 * Sets the value of the serviceNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceNumber(String value) {
		this.serviceNumber = value;
	}

	/**
	 * Gets the value of the primaryOfferingInfo property.
	 * 
	 * @return possible object is
	 *         {@link USSDGWChngPOReqMsg.PrimaryOfferingInfo }
	 * 
	 */
	public USSDGWChngPOReqMsg.PrimaryOfferingInfo getPrimaryOfferingInfo() {
		return primaryOfferingInfo;
	}

	/**
	 * Sets the value of the primaryOfferingInfo property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link USSDGWChngPOReqMsg.PrimaryOfferingInfo }
	 * 
	 */
	public void setPrimaryOfferingInfo(USSDGWChngPOReqMsg.PrimaryOfferingInfo value) {
		this.primaryOfferingInfo = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="OfferingId">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="OfferingId" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
	 *                   &lt;element name="OfferingCode" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
	 *         &lt;element name="Remark" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "offeringId", "actionType", "remark" })
	public static class PrimaryOfferingInfo {
		@XmlElement(name = "OfferingId", required = true)
		protected USSDGWChngPOReqMsg.PrimaryOfferingInfo.OfferingId offeringId;
		@XmlElement(name = "ActionType")
		protected String actionType;
		@XmlElement(name = "Remark")
		protected String remark;

		/**
		 * Gets the value of the offeringId property.
		 * 
		 * @return possible object is
		 *         {@link USSDGWChngPOReqMsg.PrimaryOfferingInfo.OfferingId }
		 * 
		 */
		public USSDGWChngPOReqMsg.PrimaryOfferingInfo.OfferingId getOfferingId() {
			return offeringId;
		}

		/**
		 * Sets the value of the offeringId property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link USSDGWChngPOReqMsg.PrimaryOfferingInfo.OfferingId }
		 * 
		 */
		public void setOfferingId(USSDGWChngPOReqMsg.PrimaryOfferingInfo.OfferingId value) {
			this.offeringId = value;
		}

		/**
		 * Gets the value of the actionType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getActionType() {
			return actionType;
		}

		/**
		 * Sets the value of the actionType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setActionType(String value) {
			this.actionType = value;
		}

		/**
		 * Gets the value of the remark property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRemark() {
			return remark;
		}

		/**
		 * Sets the value of the remark property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRemark(String value) {
			this.remark = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="OfferingId" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
		 *         &lt;element name="OfferingCode" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "offeringId", "offeringCode" })
		public static class OfferingId {
			@XmlElement(name = "OfferingId")
			protected String offeringId;
			@XmlElement(name = "OfferingCode")
			protected String offeringCode;

			/**
			 * Gets the value of the offeringId property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getOfferingId() {
				return offeringId;
			}

			/**
			 * Sets the value of the offeringId property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setOfferingId(String value) {
				this.offeringId = value;
			}

			/**
			 * Gets the value of the offeringCode property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getOfferingCode() {
				return offeringCode;
			}

			/**
			 * Sets the value of the offeringCode property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setOfferingCode(String value) {
				this.offeringCode = value;
			}
		}
	}
}
