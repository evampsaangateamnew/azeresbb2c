package com.huawei.bss.soaif._interface.subscriberservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;element name="AccessInfo" type="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo"/>
 *         &lt;element name="AcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId"/>
 *         &lt;element name="PaymentRelation" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}PaymentRelation">
 *                 &lt;sequence>
 *                   &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "reqHeader", "accessInfo", "acctId", "paymentRelation" })
@XmlRootElement(name = "ChangePaymentRelationReqMsg")
public class ChangePaymentRelationReqMsg {
	@XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
	protected ReqHeader reqHeader;
	@XmlElement(name = "AccessInfo", required = true)
	protected ObjectAccessInfo accessInfo;
	@XmlElement(name = "AcctId", required = true)
	protected String acctId;
	@XmlElement(name = "PaymentRelation", required = true)
	protected List<ChangePaymentRelationReqMsg.PaymentRelation> paymentRelation;

	/**
	 * Gets the value of the reqHeader property.
	 * 
	 * @return possible object is {@link ReqHeader }
	 * 
	 */
	public ReqHeader getReqHeader() {
		return reqHeader;
	}

	/**
	 * Sets the value of the reqHeader property.
	 * 
	 * @param value
	 *            allowed object is {@link ReqHeader }
	 * 
	 */
	public void setReqHeader(ReqHeader value) {
		this.reqHeader = value;
	}

	/**
	 * Gets the value of the accessInfo property.
	 * 
	 * @return possible object is {@link ObjectAccessInfo }
	 * 
	 */
	public ObjectAccessInfo getAccessInfo() {
		return accessInfo;
	}

	/**
	 * Sets the value of the accessInfo property.
	 * 
	 * @param value
	 *            allowed object is {@link ObjectAccessInfo }
	 * 
	 */
	public void setAccessInfo(ObjectAccessInfo value) {
		this.accessInfo = value;
	}

	/**
	 * Gets the value of the acctId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcctId() {
		return acctId;
	}

	/**
	 * Sets the value of the acctId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAcctId(String value) {
		this.acctId = value;
	}

	/**
	 * Gets the value of the paymentRelation property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the paymentRelation property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getPaymentRelation().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ChangePaymentRelationReqMsg.PaymentRelation }
	 * 
	 * 
	 */
	public List<ChangePaymentRelationReqMsg.PaymentRelation> getPaymentRelation() {
		if (paymentRelation == null) {
			paymentRelation = new ArrayList<ChangePaymentRelationReqMsg.PaymentRelation>();
		}
		return this.paymentRelation;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;extension base="{http://www.huawei.com/bss/soaif/interface/common/}PaymentRelation">
	 *       &lt;sequence>
	 *         &lt;element name="ActionType" type="{http://www.huawei.com/bss/soaif/interface/common/}ActionType"/>
	 *       &lt;/sequence>
	 *     &lt;/extension>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "actionType" })
	public static class PaymentRelation extends com.huawei.bss.soaif._interface.common.PaymentRelation {
		@XmlElement(name = "ActionType", required = true)
		protected String actionType;

		/**
		 * Gets the value of the actionType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getActionType() {
			return actionType;
		}

		/**
		 * Sets the value of the actionType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setActionType(String value) {
			this.actionType = value;
		}
	}
}
