
package com.huawei.bss.soaif._interface.common.querycorporate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcctCreditAmt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AcctCreditAmt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditLimitType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CreditLimitTypeName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TotalCreditAmt" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount"/>
 *         &lt;element name="TotalUsageAmt" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount"/>
 *         &lt;element name="TotalRemainAmt" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount"/>
 *         &lt;element name="CreditAmtInfo" maxOccurs="100">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitClass">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="I"/>
 *                         &lt;enumeration value="T"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Amount" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount"/>
 *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *                   &lt;element name="ExpireTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctCreditAmt", propOrder = {
    "creditLimitType",
    "creditLimitTypeName",
    "totalCreditAmt",
    "totalUsageAmt",
    "totalRemainAmt",
    "creditAmtInfo"
})
public class AcctCreditAmt {

    @XmlElement(name = "CreditLimitType", required = true)
    protected String creditLimitType;
    @XmlElement(name = "CreditLimitTypeName", required = true)
    protected String creditLimitTypeName;
    @XmlElement(name = "TotalCreditAmt", required = true)
    protected String totalCreditAmt;
    @XmlElement(name = "TotalUsageAmt", required = true)
    protected String totalUsageAmt;
    @XmlElement(name = "TotalRemainAmt", required = true)
    protected String totalRemainAmt;
    @XmlElement(name = "CreditAmtInfo", required = true)
    protected List<AcctCreditAmt.CreditAmtInfo> creditAmtInfo;

    /**
     * Gets the value of the creditLimitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditLimitType() {
        return creditLimitType;
    }

    /**
     * Sets the value of the creditLimitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditLimitType(String value) {
        this.creditLimitType = value;
    }

    /**
     * Gets the value of the creditLimitTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditLimitTypeName() {
        return creditLimitTypeName;
    }

    /**
     * Sets the value of the creditLimitTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditLimitTypeName(String value) {
        this.creditLimitTypeName = value;
    }

    /**
     * Gets the value of the totalCreditAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalCreditAmt() {
        return totalCreditAmt;
    }

    /**
     * Sets the value of the totalCreditAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalCreditAmt(String value) {
        this.totalCreditAmt = value;
    }

    /**
     * Gets the value of the totalUsageAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalUsageAmt() {
        return totalUsageAmt;
    }

    /**
     * Sets the value of the totalUsageAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalUsageAmt(String value) {
        this.totalUsageAmt = value;
    }

    /**
     * Gets the value of the totalRemainAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalRemainAmt() {
        return totalRemainAmt;
    }

    /**
     * Sets the value of the totalRemainAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalRemainAmt(String value) {
        this.totalRemainAmt = value;
    }

    /**
     * Gets the value of the creditAmtInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditAmtInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditAmtInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AcctCreditAmt.CreditAmtInfo }
     * 
     * 
     */
    public List<AcctCreditAmt.CreditAmtInfo> getCreditAmtInfo() {
        if (creditAmtInfo == null) {
            creditAmtInfo = new ArrayList<AcctCreditAmt.CreditAmtInfo>();
        }
        return this.creditAmtInfo;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitClass">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="I"/>
     *               &lt;enumeration value="T"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Amount" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount"/>
     *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
     *         &lt;element name="ExpireTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitClass",
        "amount",
        "effectiveTime",
        "expireTime"
    })
    public static class CreditAmtInfo {

        @XmlElement(name = "LimitClass", required = true)
        protected String limitClass;
        @XmlElement(name = "Amount", required = true)
        protected String amount;
        @XmlElement(name = "EffectiveTime", required = true)
        protected String effectiveTime;
        @XmlElement(name = "ExpireTime", required = true)
        protected String expireTime;

        /**
         * Gets the value of the limitClass property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitClass() {
            return limitClass;
        }

        /**
         * Sets the value of the limitClass property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitClass(String value) {
            this.limitClass = value;
        }

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAmount(String value) {
            this.amount = value;
        }

        /**
         * Gets the value of the effectiveTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Sets the value of the effectiveTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffectiveTime(String value) {
            this.effectiveTime = value;
        }

        /**
         * Gets the value of the expireTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpireTime() {
            return expireTime;
        }

        /**
         * Sets the value of the expireTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpireTime(String value) {
            this.expireTime = value;
        }

    }

}
