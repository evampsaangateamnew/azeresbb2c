
package com.huawei.bss.soaif._interface.corporateservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.bss.soaif._interface.common.querycorporate.Certificate;
import com.huawei.bss.soaif._interface.common.querycorporate.ReqHeader;
import com.huawei.bss.soaif._interface.common.querycorporate.ServiceNum;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;choice>
 *           &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId"/>
 *           &lt;element name="CorporateName" type="{http://www.huawei.com/bss/soaif/interface/common/}CustName"/>
 *           &lt;element name="CorpNo" type="{http://www.huawei.com/bss/soaif/interface/common/}CorpNo"/>
 *           &lt;element name="MemberServiceNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}ServiceNum"/>
 *           &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate"/>
 *           &lt;element name="TINNumber" type="{http://www.huawei.com/bss/soaif/interface/common/}TINNumber"/>
 *         &lt;/choice>
 *         &lt;element name="Taxation" type="{http://www.huawei.com/bss/soaif/interface/common/}Taxation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reqHeader",
    "custId",
    "corporateName",
    "corpNo",
    "memberServiceNumber",
    "certificate",
    "tinNumber",
    "taxation"
})
@XmlRootElement(name = "QueryCorporateCustReqMsg")
public class QueryCorporateCustReqMsg {

    @XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
    protected ReqHeader reqHeader;
    @XmlElement(name = "CustId")
    protected String custId;
    @XmlElement(name = "CorporateName")
    protected String corporateName;
    @XmlElement(name = "CorpNo")
    protected String corpNo;
    @XmlElement(name = "MemberServiceNumber")
    protected ServiceNum memberServiceNumber;
    @XmlElement(name = "Certificate")
    protected Certificate certificate;
    @XmlElement(name = "TINNumber")
    protected String tinNumber;
    @XmlElement(name = "Taxation")
    protected String taxation;

    /**
     * Gets the value of the reqHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ReqHeader }
     *     
     */
    public ReqHeader getReqHeader() {
        return reqHeader;
    }

    /**
     * Sets the value of the reqHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqHeader }
     *     
     */
    public void setReqHeader(ReqHeader value) {
        this.reqHeader = value;
    }

    /**
     * Gets the value of the custId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustId() {
        return custId;
    }

    /**
     * Sets the value of the custId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustId(String value) {
        this.custId = value;
    }

    /**
     * Gets the value of the corporateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateName() {
        return corporateName;
    }

    /**
     * Sets the value of the corporateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateName(String value) {
        this.corporateName = value;
    }

    /**
     * Gets the value of the corpNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpNo() {
        return corpNo;
    }

    /**
     * Sets the value of the corpNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpNo(String value) {
        this.corpNo = value;
    }

    /**
     * Gets the value of the memberServiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceNum }
     *     
     */
    public ServiceNum getMemberServiceNumber() {
        return memberServiceNumber;
    }

    /**
     * Sets the value of the memberServiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceNum }
     *     
     */
    public void setMemberServiceNumber(ServiceNum value) {
        this.memberServiceNumber = value;
    }

    /**
     * Gets the value of the certificate property.
     * 
     * @return
     *     possible object is
     *     {@link Certificate }
     *     
     */
    public Certificate getCertificate() {
        return certificate;
    }

    /**
     * Sets the value of the certificate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Certificate }
     *     
     */
    public void setCertificate(Certificate value) {
        this.certificate = value;
    }

    /**
     * Gets the value of the tinNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTINNumber() {
        return tinNumber;
    }

    /**
     * Sets the value of the tinNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTINNumber(String value) {
        this.tinNumber = value;
    }

    /**
     * Gets the value of the taxation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxation() {
        return taxation;
    }

    /**
     * Sets the value of the taxation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxation(String value) {
        this.taxation = value;
    }

}
