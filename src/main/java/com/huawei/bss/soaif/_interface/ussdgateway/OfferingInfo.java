package com.huawei.bss.soaif._interface.ussdgateway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OfferingInfo complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingId" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *         &lt;element name="OfferingName" type="{http://www.huawei.com/bss/soaif/interface/common/}string"/>
 *         &lt;element name="OfferingCode" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="OfferingShortName" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="NetworkType" type="{http://www.huawei.com/bss/soaif/interface/common/}string" minOccurs="0"/>
 *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *         &lt;element name="ExpiredTime" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingInfo", propOrder = { "offeringId", "offeringName", "offeringCode", "offeringShortName",
		"status", "networkType", "effectiveTime", "expiredTime" })
public class OfferingInfo {
	@XmlElement(name = "OfferingId", required = true)
	protected String offeringId;
	@XmlElement(name = "OfferingName", required = true)
	protected String offeringName;
	@XmlElement(name = "OfferingCode")
	protected String offeringCode;
	@XmlElement(name = "OfferingShortName")
	protected String offeringShortName;
	@XmlElement(name = "Status")
	protected String status;
	@XmlElement(name = "NetworkType")
	protected String networkType;
	@XmlElement(name = "EffectiveTime")
	protected String effectiveTime;
	@XmlElement(name = "ExpiredTime")
	protected String expiredTime;

	/**
	 * Gets the value of the offeringId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * Sets the value of the offeringId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingId(String value) {
		this.offeringId = value;
	}

	/**
	 * Gets the value of the offeringName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingName() {
		return offeringName;
	}

	/**
	 * Sets the value of the offeringName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingName(String value) {
		this.offeringName = value;
	}

	/**
	 * Gets the value of the offeringCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingCode() {
		return offeringCode;
	}

	/**
	 * Sets the value of the offeringCode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingCode(String value) {
		this.offeringCode = value;
	}

	/**
	 * Gets the value of the offeringShortName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOfferingShortName() {
		return offeringShortName;
	}

	/**
	 * Sets the value of the offeringShortName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOfferingShortName(String value) {
		this.offeringShortName = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the networkType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNetworkType() {
		return networkType;
	}

	/**
	 * Sets the value of the networkType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNetworkType(String value) {
		this.networkType = value;
	}

	/**
	 * Gets the value of the effectiveTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEffectiveTime() {
		return effectiveTime;
	}

	/**
	 * Sets the value of the effectiveTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEffectiveTime(String value) {
		this.effectiveTime = value;
	}

	/**
	 * Gets the value of the expiredTime property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExpiredTime() {
		return expiredTime;
	}

	/**
	 * Sets the value of the expiredTime property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExpiredTime(String value) {
		this.expiredTime = value;
	}
}
