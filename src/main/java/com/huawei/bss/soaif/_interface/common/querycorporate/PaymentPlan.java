
package com.huawei.bss.soaif._interface.common.querycorporate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentPlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctId" type="{http://www.huawei.com/bss/soaif/interface/common/}AcctId" minOccurs="0"/>
 *         &lt;element name="SubId" type="{http://www.huawei.com/bss/soaif/interface/common/}SubId" minOccurs="0"/>
 *         &lt;element name="ServiceNum" type="{http://www.huawei.com/bss/soaif/interface/common/}ServiceNum" minOccurs="0"/>
 *         &lt;element name="PaymentRelation" type="{http://www.huawei.com/bss/soaif/interface/common/}PaymentRelation" maxOccurs="100"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentPlan", propOrder = {
    "acctId",
    "subId",
    "serviceNum",
    "paymentRelation"
})
public class PaymentPlan {

    @XmlElement(name = "AcctId")
    protected String acctId;
    @XmlElement(name = "SubId")
    protected String subId;
    @XmlElement(name = "ServiceNum")
    protected ServiceNum serviceNum;
    @XmlElement(name = "PaymentRelation", required = true)
    protected List<PaymentRelation> paymentRelation;

    /**
     * Gets the value of the acctId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctId() {
        return acctId;
    }

    /**
     * Sets the value of the acctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctId(String value) {
        this.acctId = value;
    }

    /**
     * Gets the value of the subId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubId() {
        return subId;
    }

    /**
     * Sets the value of the subId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubId(String value) {
        this.subId = value;
    }

    /**
     * Gets the value of the serviceNum property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceNum }
     *     
     */
    public ServiceNum getServiceNum() {
        return serviceNum;
    }

    /**
     * Sets the value of the serviceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceNum }
     *     
     */
    public void setServiceNum(ServiceNum value) {
        this.serviceNum = value;
    }

    /**
     * Gets the value of the paymentRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentRelation }
     * 
     * 
     */
    public List<PaymentRelation> getPaymentRelation() {
        if (paymentRelation == null) {
            paymentRelation = new ArrayList<PaymentRelation>();
        }
        return this.paymentRelation;
    }

}
