
package com.huawei.bss.soaif.chargesubscriber._interface.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Contract complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Contract">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgreementId" type="{http://www.huawei.com/bss/soaif/interface/common/}AgreementId" minOccurs="0"/>
 *         &lt;element name="DurationUnit" type="{http://www.huawei.com/bss/soaif/interface/common/}DurationUnit" minOccurs="0"/>
 *         &lt;element name="DurationValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Prolongation" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DurationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DurationUnit" type="{http://www.huawei.com/bss/soaif/interface/common/}DurationUnit" minOccurs="0"/>
 *                   &lt;element name="DurationValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contract", propOrder = {
    "agreementId",
    "durationUnit",
    "durationValue",
    "prolongation",
    "additionalProperty"
})
public class Contract {

    @XmlElement(name = "AgreementId")
    protected String agreementId;
    @XmlElement(name = "DurationUnit")
    protected String durationUnit;
    @XmlElement(name = "DurationValue")
    protected String durationValue;
    @XmlElement(name = "Prolongation")
    protected List<Contract.Prolongation> prolongation;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;

    /**
     * Gets the value of the agreementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgreementId() {
        return agreementId;
    }

    /**
     * Sets the value of the agreementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgreementId(String value) {
        this.agreementId = value;
    }

    /**
     * Gets the value of the durationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationUnit() {
        return durationUnit;
    }

    /**
     * Sets the value of the durationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationUnit(String value) {
        this.durationUnit = value;
    }

    /**
     * Gets the value of the durationValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationValue() {
        return durationValue;
    }

    /**
     * Sets the value of the durationValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationValue(String value) {
        this.durationValue = value;
    }

    /**
     * Gets the value of the prolongation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prolongation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProlongation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Contract.Prolongation }
     * 
     * 
     */
    public List<Contract.Prolongation> getProlongation() {
        if (prolongation == null) {
            prolongation = new ArrayList<Contract.Prolongation>();
        }
        return this.prolongation;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DurationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DurationUnit" type="{http://www.huawei.com/bss/soaif/interface/common/}DurationUnit" minOccurs="0"/>
     *         &lt;element name="DurationValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "durationID",
        "durationUnit",
        "durationValue"
    })
    public static class Prolongation {

        @XmlElement(name = "DurationID")
        protected String durationID;
        @XmlElement(name = "DurationUnit")
        protected String durationUnit;
        @XmlElement(name = "DurationValue")
        protected String durationValue;

        /**
         * Gets the value of the durationID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDurationID() {
            return durationID;
        }

        /**
         * Sets the value of the durationID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDurationID(String value) {
            this.durationID = value;
        }

        /**
         * Gets the value of the durationUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDurationUnit() {
            return durationUnit;
        }

        /**
         * Sets the value of the durationUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDurationUnit(String value) {
            this.durationUnit = value;
        }

        /**
         * Gets the value of the durationValue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDurationValue() {
            return durationValue;
        }

        /**
         * Sets the value of the durationValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDurationValue(String value) {
            this.durationValue = value;
        }

    }

}
