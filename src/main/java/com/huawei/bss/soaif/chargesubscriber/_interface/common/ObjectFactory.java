
package com.huawei.bss.soaif.chargesubscriber._interface.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.bss.soaif._interface.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CustomerTitle_QNAME = new QName("http://www.huawei.com/bss/soaif/interface/common/", "Title");
    private final static QName _CustomerNationality_QNAME = new QName("http://www.huawei.com/bss/soaif/interface/common/", "Nationality");
    private final static QName _CustomerCertificate_QNAME = new QName("http://www.huawei.com/bss/soaif/interface/common/", "Certificate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.bss.soaif._interface.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AcctBalance }
     * 
     */
    public AcctBalance createAcctBalance() {
        return new AcctBalance();
    }

    /**
     * Create an instance of {@link PaymentRelation }
     * 
     */
    public PaymentRelation createPaymentRelation() {
        return new PaymentRelation();
    }

    /**
     * Create an instance of {@link OfferingInstanceProperty }
     * 
     */
    public OfferingInstanceProperty createOfferingInstanceProperty() {
        return new OfferingInstanceProperty();
    }

    /**
     * Create an instance of {@link AcctCreditAmt }
     * 
     */
    public AcctCreditAmt createAcctCreditAmt() {
        return new AcctCreditAmt();
    }

    /**
     * Create an instance of {@link Contract }
     * 
     */
    public Contract createContract() {
        return new Contract();
    }

    /**
     * Create an instance of {@link OutstandingAmt }
     * 
     */
    public OutstandingAmt createOutstandingAmt() {
        return new OutstandingAmt();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link RspHeader }
     * 
     */
    public RspHeader createRspHeader() {
        return new RspHeader();
    }

    /**
     * Create an instance of {@link SimpleProperty }
     * 
     */
    public SimpleProperty createSimpleProperty() {
        return new SimpleProperty();
    }

    /**
     * Create an instance of {@link ReqHeader }
     * 
     */
    public ReqHeader createReqHeader() {
        return new ReqHeader();
    }

    /**
     * Create an instance of {@link TimeFormat }
     * 
     */
    public TimeFormat createTimeFormat() {
        return new TimeFormat();
    }

    /**
     * Create an instance of {@link ObjectAccessInfo }
     * 
     */
    public ObjectAccessInfo createObjectAccessInfo() {
        return new ObjectAccessInfo();
    }

    /**
     * Create an instance of {@link Account }
     * 
     */
    public Account createAccount() {
        return new Account();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link PagingInfo }
     * 
     */
    public PagingInfo createPagingInfo() {
        return new PagingInfo();
    }

    /**
     * Create an instance of {@link OfferingId }
     * 
     */
    public OfferingId createOfferingId() {
        return new OfferingId();
    }

    /**
     * Create an instance of {@link TimePeriod }
     * 
     */
    public TimePeriod createTimePeriod() {
        return new TimePeriod();
    }

    /**
     * Create an instance of {@link BillMedium }
     * 
     */
    public BillMedium createBillMedium() {
        return new BillMedium();
    }

    /**
     * Create an instance of {@link ActiveMode }
     * 
     */
    public ActiveMode createActiveMode() {
        return new ActiveMode();
    }

    /**
     * Create an instance of {@link Notification }
     * 
     */
    public Notification createNotification() {
        return new Notification();
    }

    /**
     * Create an instance of {@link NoticeMethod }
     * 
     */
    public NoticeMethod createNoticeMethod() {
        return new NoticeMethod();
    }

    /**
     * Create an instance of {@link ManagerInfo }
     * 
     */
    public ManagerInfo createManagerInfo() {
        return new ManagerInfo();
    }

    /**
     * Create an instance of {@link CreditLimit }
     * 
     */
    public CreditLimit createCreditLimit() {
        return new CreditLimit();
    }

    /**
     * Create an instance of {@link Name }
     * 
     */
    public Name createName() {
        return new Name();
    }

    /**
     * Create an instance of {@link SIMInfo }
     * 
     */
    public SIMInfo createSIMInfo() {
        return new SIMInfo();
    }

    /**
     * Create an instance of {@link Subscriber }
     * 
     */
    public Subscriber createSubscriber() {
        return new Subscriber();
    }

    /**
     * Create an instance of {@link BusinessFeeInfo }
     * 
     */
    public BusinessFeeInfo createBusinessFeeInfo() {
        return new BusinessFeeInfo();
    }

    /**
     * Create an instance of {@link Consumption }
     * 
     */
    public Consumption createConsumption() {
        return new Consumption();
    }

    /**
     * Create an instance of {@link OfferingInstance }
     * 
     */
    public OfferingInstance createOfferingInstance() {
        return new OfferingInstance();
    }

    /**
     * Create an instance of {@link PaymentPlan }
     * 
     */
    public PaymentPlan createPaymentPlan() {
        return new PaymentPlan();
    }

    /**
     * Create an instance of {@link Certificate }
     * 
     */
    public Certificate createCertificate() {
        return new Certificate();
    }

    /**
     * Create an instance of {@link PaymentChannel }
     * 
     */
    public PaymentChannel createPaymentChannel() {
        return new PaymentChannel();
    }

    /**
     * Create an instance of {@link LifeCycleStatus }
     * 
     */
    public LifeCycleStatus createLifeCycleStatus() {
        return new LifeCycleStatus();
    }

    /**
     * Create an instance of {@link EffectiveMode }
     * 
     */
    public EffectiveMode createEffectiveMode() {
        return new EffectiveMode();
    }

    /**
     * Create an instance of {@link OrderInfo }
     * 
     */
    public OrderInfo createOrderInfo() {
        return new OrderInfo();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link AcctBalance.BalanceDetail }
     * 
     */
    public AcctBalance.BalanceDetail createAcctBalanceBalanceDetail() {
        return new AcctBalance.BalanceDetail();
    }

    /**
     * Create an instance of {@link PaymentRelation.PaymentLimit }
     * 
     */
    public PaymentRelation.PaymentLimit createPaymentRelationPaymentLimit() {
        return new PaymentRelation.PaymentLimit();
    }

    /**
     * Create an instance of {@link OfferingInstanceProperty.SubPropertyInstance }
     * 
     */
    public OfferingInstanceProperty.SubPropertyInstance createOfferingInstancePropertySubPropertyInstance() {
        return new OfferingInstanceProperty.SubPropertyInstance();
    }

    /**
     * Create an instance of {@link AcctCreditAmt.CreditAmtInfo }
     * 
     */
    public AcctCreditAmt.CreditAmtInfo createAcctCreditAmtCreditAmtInfo() {
        return new AcctCreditAmt.CreditAmtInfo();
    }

    /**
     * Create an instance of {@link Contract.Prolongation }
     * 
     */
    public Contract.Prolongation createContractProlongation() {
        return new Contract.Prolongation();
    }

    /**
     * Create an instance of {@link OutstandingAmt.OutStandingDetail }
     * 
     */
    public OutstandingAmt.OutStandingDetail createOutstandingAmtOutStandingDetail() {
        return new OutstandingAmt.OutStandingDetail();
    }

    /**
     * Create an instance of {@link Customer.NoticeSuppress }
     * 
     */
    public Customer.NoticeSuppress createCustomerNoticeSuppress() {
        return new Customer.NoticeSuppress();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/common/", name = "Title", scope = Customer.class)
    public JAXBElement<String> createCustomerTitle(String value) {
        return new JAXBElement<String>(_CustomerTitle_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/common/", name = "Nationality", scope = Customer.class)
    public JAXBElement<String> createCustomerNationality(String value) {
        return new JAXBElement<String>(_CustomerNationality_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Certificate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/common/", name = "Certificate", scope = Customer.class)
    public JAXBElement<Certificate> createCustomerCertificate(Certificate value) {
        return new JAXBElement<Certificate>(_CustomerCertificate_QNAME, Certificate.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bss/soaif/interface/common/", name = "Title", scope = Account.class)
    public JAXBElement<String> createAccountTitle(String value) {
        return new JAXBElement<String>(_CustomerTitle_QNAME, String.class, Account.class, value);
    }

}
