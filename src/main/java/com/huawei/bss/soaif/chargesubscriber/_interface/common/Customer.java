
package com.huawei.bss.soaif.chargesubscriber._interface.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Customer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Customer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustId" type="{http://www.huawei.com/bss/soaif/interface/common/}CustId" minOccurs="0"/>
 *         &lt;element name="CustLevel" type="{http://www.huawei.com/bss/soaif/interface/common/}CustLevel" minOccurs="0"/>
 *         &lt;element name="CustSegment" type="{http://www.huawei.com/bss/soaif/interface/common/}CustSegment" minOccurs="0"/>
 *         &lt;element name="NoticeSuppress" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Title" type="{http://www.huawei.com/bss/soaif/interface/common/}Title" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.huawei.com/bss/soaif/interface/common/}Name" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://www.huawei.com/bss/soaif/interface/common/}Nationality" minOccurs="0"/>
 *         &lt;element name="Certificate" type="{http://www.huawei.com/bss/soaif/interface/common/}Certificate" minOccurs="0"/>
 *         &lt;element name="Birthday" type="{http://www.huawei.com/bss/soaif/interface/common/}Date" minOccurs="0"/>
 *         &lt;element name="Contact" type="{http://www.huawei.com/bss/soaif/interface/common/}Contact" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://www.huawei.com/bss/soaif/interface/common/}Address" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.huawei.com/bss/soaif/interface/common/}CustStatus" minOccurs="0"/>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer", propOrder = {
    "custId",
    "custLevel",
    "custSegment",
    "noticeSuppress",
    "title",
    "name",
    "nationality",
    "certificate",
    "birthday",
    "contact",
    "address",
    "status",
    "additionalProperty"
})
public class Customer {

    @XmlElement(name = "CustId")
    protected String custId;
    @XmlElement(name = "CustLevel")
    protected String custLevel;
    @XmlElement(name = "CustSegment")
    protected String custSegment;
    @XmlElement(name = "NoticeSuppress")
    protected List<Customer.NoticeSuppress> noticeSuppress;
    @XmlElementRef(name = "Title", namespace = "http://www.huawei.com/bss/soaif/interface/common/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElement(name = "Name")
    protected Name name;
    @XmlElementRef(name = "Nationality", namespace = "http://www.huawei.com/bss/soaif/interface/common/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nationality;
    @XmlElementRef(name = "Certificate", namespace = "http://www.huawei.com/bss/soaif/interface/common/", type = JAXBElement.class, required = false)
    protected JAXBElement<Certificate> certificate;
    @XmlElement(name = "Birthday")
    protected String birthday;
    @XmlElement(name = "Contact")
    protected List<Contact> contact;
    @XmlElement(name = "Address")
    protected List<Address> address;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;

    /**
     * Gets the value of the custId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustId() {
        return custId;
    }

    /**
     * Sets the value of the custId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustId(String value) {
        this.custId = value;
    }

    /**
     * Gets the value of the custLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLevel() {
        return custLevel;
    }

    /**
     * Sets the value of the custLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLevel(String value) {
        this.custLevel = value;
    }

    /**
     * Gets the value of the custSegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustSegment() {
        return custSegment;
    }

    /**
     * Sets the value of the custSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustSegment(String value) {
        this.custSegment = value;
    }

    /**
     * Gets the value of the noticeSuppress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the noticeSuppress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNoticeSuppress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Customer.NoticeSuppress }
     * 
     * 
     */
    public List<Customer.NoticeSuppress> getNoticeSuppress() {
        if (noticeSuppress == null) {
            noticeSuppress = new ArrayList<Customer.NoticeSuppress>();
        }
        return this.noticeSuppress;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setName(Name value) {
        this.name = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNationality(JAXBElement<String> value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the certificate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Certificate }{@code >}
     *     
     */
    public JAXBElement<Certificate> getCertificate() {
        return certificate;
    }

    /**
     * Sets the value of the certificate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Certificate }{@code >}
     *     
     */
    public void setCertificate(JAXBElement<Certificate> value) {
        this.certificate = value;
    }

    /**
     * Gets the value of the birthday property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * Sets the value of the birthday property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthday(String value) {
        this.birthday = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Contact }
     * 
     * 
     */
    public List<Contact> getContact() {
        if (contact == null) {
            contact = new ArrayList<Contact>();
        }
        return this.contact;
    }

    /**
     * Gets the value of the address property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the address property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Address }
     * 
     * 
     */
    public List<Address> getAddress() {
        if (address == null) {
            address = new ArrayList<Address>();
        }
        return this.address;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "channelType",
        "noticeType",
        "subNoticeType"
    })
    public static class NoticeSuppress {

        @XmlElement(name = "ChannelType", required = true)
        protected String channelType;
        @XmlElement(name = "NoticeType", required = true)
        protected String noticeType;
        @XmlElement(name = "SubNoticeType")
        protected String subNoticeType;

        /**
         * Gets the value of the channelType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannelType() {
            return channelType;
        }

        /**
         * Sets the value of the channelType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannelType(String value) {
            this.channelType = value;
        }

        /**
         * Gets the value of the noticeType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNoticeType() {
            return noticeType;
        }

        /**
         * Sets the value of the noticeType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNoticeType(String value) {
            this.noticeType = value;
        }

        /**
         * Gets the value of the subNoticeType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubNoticeType() {
            return subNoticeType;
        }

        /**
         * Sets the value of the subNoticeType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubNoticeType(String value) {
            this.subNoticeType = value;
        }

    }

}
