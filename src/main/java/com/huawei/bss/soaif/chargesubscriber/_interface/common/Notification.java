
package com.huawei.bss.soaif.chargesubscriber._interface.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Notification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Notification">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustNoticeFlag" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PartnerNoticeFlag" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NoticeMethod" type="{http://www.huawei.com/bss/soaif/interface/common/}NoticeMethod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Notification", propOrder = {
    "custNoticeFlag",
    "partnerNoticeFlag",
    "noticeMethod"
})
public class Notification {

    @XmlElement(name = "CustNoticeFlag", defaultValue = "Y")
    protected String custNoticeFlag;
    @XmlElement(name = "PartnerNoticeFlag", defaultValue = "Y")
    protected String partnerNoticeFlag;
    @XmlElement(name = "NoticeMethod")
    protected NoticeMethod noticeMethod;

    /**
     * Gets the value of the custNoticeFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustNoticeFlag() {
        return custNoticeFlag;
    }

    /**
     * Sets the value of the custNoticeFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustNoticeFlag(String value) {
        this.custNoticeFlag = value;
    }

    /**
     * Gets the value of the partnerNoticeFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerNoticeFlag() {
        return partnerNoticeFlag;
    }

    /**
     * Sets the value of the partnerNoticeFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerNoticeFlag(String value) {
        this.partnerNoticeFlag = value;
    }

    /**
     * Gets the value of the noticeMethod property.
     * 
     * @return
     *     possible object is
     *     {@link NoticeMethod }
     *     
     */
    public NoticeMethod getNoticeMethod() {
        return noticeMethod;
    }

    /**
     * Sets the value of the noticeMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link NoticeMethod }
     *     
     */
    public void setNoticeMethod(NoticeMethod value) {
        this.noticeMethod = value;
    }

}
