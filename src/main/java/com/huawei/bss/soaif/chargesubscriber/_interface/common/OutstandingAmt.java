
package com.huawei.bss.soaif.chargesubscriber._interface.common;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutstandingAmt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutstandingAmt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillCycleId" type="{http://www.huawei.com/bss/soaif/interface/common/}BillCycleId"/>
 *         &lt;element name="BillCyclePeriod" type="{http://www.huawei.com/bss/soaif/interface/common/}TimePeriod"/>
 *         &lt;element name="DueDate" type="{http://www.huawei.com/bss/soaif/interface/common/}DateTime"/>
 *         &lt;element name="OutStandingDetail" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OutstandingAmt" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount"/>
 *                   &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutstandingAmt", propOrder = {
    "billCycleId",
    "billCyclePeriod",
    "dueDate",
    "outStandingDetail"
})
public class OutstandingAmt {

    @XmlElement(name = "BillCycleId", required = true)
    protected String billCycleId;
    @XmlElement(name = "BillCyclePeriod", required = true)
    protected TimePeriod billCyclePeriod;
    @XmlElement(name = "DueDate", required = true)
    protected String dueDate;
    @XmlElement(name = "OutStandingDetail", required = true)
    protected List<OutstandingAmt.OutStandingDetail> outStandingDetail;

    /**
     * Gets the value of the billCycleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleId() {
        return billCycleId;
    }

    /**
     * Sets the value of the billCycleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleId(String value) {
        this.billCycleId = value;
    }

    /**
     * Gets the value of the billCyclePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link TimePeriod }
     *     
     */
    public TimePeriod getBillCyclePeriod() {
        return billCyclePeriod;
    }

    /**
     * Sets the value of the billCyclePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimePeriod }
     *     
     */
    public void setBillCyclePeriod(TimePeriod value) {
        this.billCyclePeriod = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDueDate(String value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the outStandingDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the outStandingDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOutStandingDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OutstandingAmt.OutStandingDetail }
     * 
     * 
     */
    public List<OutstandingAmt.OutStandingDetail> getOutStandingDetail() {
        if (outStandingDetail == null) {
            outStandingDetail = new ArrayList<OutstandingAmt.OutStandingDetail>();
        }
        return this.outStandingDetail;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OutstandingAmt" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount"/>
     *         &lt;element name="Currency" type="{http://www.huawei.com/bss/soaif/interface/common/}Currency"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "outstandingAmt",
        "currency"
    })
    public static class OutStandingDetail {

        @XmlElement(name = "OutstandingAmt")
        protected long outstandingAmt;
        @XmlElement(name = "Currency", required = true)
        protected BigInteger currency;

        /**
         * Gets the value of the outstandingAmt property.
         * 
         */
        public long getOutstandingAmt() {
            return outstandingAmt;
        }

        /**
         * Sets the value of the outstandingAmt property.
         * 
         */
        public void setOutstandingAmt(long value) {
            this.outstandingAmt = value;
        }

        /**
         * Gets the value of the currency property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrency() {
            return currency;
        }

        /**
         * Sets the value of the currency property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrency(BigInteger value) {
            this.currency = value;
        }

    }

}
