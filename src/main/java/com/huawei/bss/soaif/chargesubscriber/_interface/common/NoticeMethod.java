
package com.huawei.bss.soaif.chargesubscriber._interface.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NoticeMethod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NoticeMethod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NotifyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NotifyMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NoticeMethod", propOrder = {
    "notifyType",
    "notifyMethod"
})
public class NoticeMethod {

    @XmlElement(name = "NotifyType")
    protected String notifyType;
    @XmlElement(name = "NotifyMethod")
    protected String notifyMethod;

    /**
     * Gets the value of the notifyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotifyType() {
        return notifyType;
    }

    /**
     * Sets the value of the notifyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotifyType(String value) {
        this.notifyType = value;
    }

    /**
     * Gets the value of the notifyMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotifyMethod() {
        return notifyMethod;
    }

    /**
     * Sets the value of the notifyMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotifyMethod(String value) {
        this.notifyMethod = value;
    }

}
