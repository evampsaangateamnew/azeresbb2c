
package com.huawei.bss.soaif.chargesubscriber._interface.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OfferingInstanceProperty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferingInstanceProperty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PropertyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PropertyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;choice>
 *           &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="SubPropertyInstance" maxOccurs="unbounded">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="SubPropertyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferingInstanceProperty", propOrder = {
    "propertyCode",
    "propertyType",
    "value",
    "subPropertyInstance"
})
public class OfferingInstanceProperty {

    @XmlElement(name = "PropertyCode", required = true)
    protected String propertyCode;
    @XmlElement(name = "PropertyType", required = true)
    protected String propertyType;
    @XmlElement(name = "Value")
    protected String value;
    @XmlElement(name = "SubPropertyInstance")
    protected List<OfferingInstanceProperty.SubPropertyInstance> subPropertyInstance;

    /**
     * Gets the value of the propertyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyCode() {
        return propertyCode;
    }

    /**
     * Sets the value of the propertyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyCode(String value) {
        this.propertyCode = value;
    }

    /**
     * Gets the value of the propertyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyType() {
        return propertyType;
    }

    /**
     * Sets the value of the propertyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyType(String value) {
        this.propertyType = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the subPropertyInstance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subPropertyInstance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubPropertyInstance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OfferingInstanceProperty.SubPropertyInstance }
     * 
     * 
     */
    public List<OfferingInstanceProperty.SubPropertyInstance> getSubPropertyInstance() {
        if (subPropertyInstance == null) {
            subPropertyInstance = new ArrayList<OfferingInstanceProperty.SubPropertyInstance>();
        }
        return this.subPropertyInstance;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubPropertyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subPropertyCode",
        "value"
    })
    public static class SubPropertyInstance {

        @XmlElement(name = "SubPropertyCode", required = true)
        protected String subPropertyCode;
        @XmlElement(name = "Value", required = true)
        protected String value;

        /**
         * Gets the value of the subPropertyCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubPropertyCode() {
            return subPropertyCode;
        }

        /**
         * Sets the value of the subPropertyCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubPropertyCode(String value) {
            this.subPropertyCode = value;
        }

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }

}
