
package com.huawei.bss.soaif.chargesubscriber._interface.rechargeservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.huawei.bss.soaif.chargesubscriber._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif.chargesubscriber._interface.common.ReqHeader;
import com.huawei.bss.soaif.chargesubscriber._interface.common.SimpleProperty;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.huawei.com/bss/soaif/interface/common/}ReqHeader"/>
 *         &lt;sequence>
 *           &lt;element name="AccessInfo" type="{http://www.huawei.com/bss/soaif/interface/common/}ObjectAccessInfo"/>
 *           &lt;element name="ContentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="Quantity" type="{http://www.huawei.com/bss/soaif/interface/common/}Amount"/>
 *           &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bss/soaif/interface/common/}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reqHeader",
    "accessInfo",
    "contentId",
    "quantity",
    "description",
    "additionalProperty"
})
@XmlRootElement(name = "ChargeForSubscriberReqMsg")
public class ChargeForSubscriberReqMsg {

    @XmlElement(name = "ReqHeader", namespace = "http://www.huawei.com/bss/soaif/interface/common/", required = true)
    protected ReqHeader reqHeader;
    @XmlElement(name = "AccessInfo", required = true)
    protected ObjectAccessInfo accessInfo;
    @XmlElement(name = "ContentId", required = true)
    protected String contentId;
    @XmlElement(name = "Quantity")
    protected long quantity;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;

    /**
     * Gets the value of the reqHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ReqHeader }
     *     
     */
    public ReqHeader getReqHeader() {
        return reqHeader;
    }

    /**
     * Sets the value of the reqHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqHeader }
     *     
     */
    public void setReqHeader(ReqHeader value) {
        this.reqHeader = value;
    }

    /**
     * Gets the value of the accessInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectAccessInfo }
     *     
     */
    public ObjectAccessInfo getAccessInfo() {
        return accessInfo;
    }

    /**
     * Sets the value of the accessInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectAccessInfo }
     *     
     */
    public void setAccessInfo(ObjectAccessInfo value) {
        this.accessInfo = value;
    }

    /**
     * Gets the value of the contentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * Sets the value of the contentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentId(String value) {
        this.contentId = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     */
    public void setQuantity(long value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }

}
