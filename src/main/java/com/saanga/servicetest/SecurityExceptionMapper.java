package com.saanga.servicetest;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class SecurityExceptionMapper implements ExceptionMapper<BadRequestException> {
	public Response toResponse(BadRequestException exception) {
		return Response.status(Response.Status.REQUEST_TIMEOUT).build();
	}
}