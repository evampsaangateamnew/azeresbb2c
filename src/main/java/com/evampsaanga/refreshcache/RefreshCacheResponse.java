package com.evampsaanga.refreshcache;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class RefreshCacheResponse extends BaseResponse{

    private String entityId;

    public String getEntityId() {
	return entityId;
    }

    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }
    
}
