package com.evampsaanga.refreshcache;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RefreshCacheRequest")
public class RefreshCacheRequest extends BaseRequest {
	
}
