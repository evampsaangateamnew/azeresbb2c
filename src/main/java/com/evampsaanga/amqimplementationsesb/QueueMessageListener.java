package com.evampsaanga.amqimplementationsesb;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import org.apache.log4j.Logger;
import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.developer.utils.Helper;

/**
 * Message listener for ESB Logs
 * 
 * @author EvampSaanga
 *
 */
public class QueueMessageListener implements MessageListener {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	/**
	 * Method overrides implementation for MessageListener Interface
	 */

	@Override
	public void onMessage(Message messagefromQueue) {
		try {
			if (messagefromQueue instanceof TextMessage) {
				TextMessage msg = (TextMessage) messagefromQueue;
				String selectMsisdnQuery = "Insert into ";
				logger.info("Message Came To Local AMQ consumer" + msg.getText());
				Logs logs = null;
				try {
					logs = Helper.JsonToObject(msg.getText(), Logs.class);
				} catch (Exception ex) {
					logger.info(Helper.GetException(ex));
				}
				if (logs != null) {
					int transactionId = -1;
					try {
						transactionId = getTransactionId(logs.getTransactionName().toUpperCase());
					} catch (Exception ex) {
						logger.info(Helper.GetException(ex));
					}

					PreparedStatement preparedStatement = null;
					try {
						selectMsisdnQuery = selectMsisdnQuery + ESBLogsTable.GENERAL_REPORTING
								+ " (transactionName,thirdPartyName,channel,msisdn,ip,requestDateTime,responseDateTime,responseCode,responseDescription,userType,"
								+ "receiverMsisdn,mediumForTopup,amount,scratchCardNumber,apiurl,onnet,activatedOfferId ,tariffId) "
								+ "values ('" + transactionId + "','" + logs.getThirdPartyName() + "','"
								+ logs.getChannel() + "','" + logs.getMsisdn() + "','" + logs.getIp() + "','"
								+ logs.getRequestDateTime() + "','" + logs.getResponseDateTime() + "','"
								+ logs.getResponseCode() + "','" + logs.getResponseDescription() + "','"
								+ logs.getUserType() + "','" + logs.getReceiverMsisdn() + "','"
								+ logs.getMediumForTopup() + "','" + logs.getAmount() + "','"
								+ logs.getScratchCardNumber() + "','" + logs.getApiurl() + "','" + logs.isOnNet()
								+ "','" + logs.getActivatedOfferId() + "','" + logs.getTariffId() + "')";

						logger.debug("QueryFORSQLESB:" + selectMsisdnQuery);
						preparedStatement = DBFactory.getDbConnection().prepareStatement(selectMsisdnQuery,
								Statement.RETURN_GENERATED_KEYS);
						preparedStatement.executeUpdate();
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					} finally {
						if (preparedStatement != null)
							preparedStatement.close();
					}

				}

			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		logger.debug("Request Completed");
	}

	public static Map<String, String> hashmapTransactionMappings = new HashMap<String, String>();
	

	public static void loadTransactionMappings() {
		ResultSet resultSet = null;
		PreparedStatement prep = null;
		try {

			resultSet = DBFactory.getDbConnection().prepareStatement("select * from transactionMappings")
					.executeQuery();

			while (resultSet.next()) {
				// hashmap key will mapped with transaction names.
				logger.debug("Caching Transactions - Transaction Name:" + resultSet.getString(2) + "-> ID:"
						+ resultSet.getString(1));
				hashmapTransactionMappings.put(resultSet.getString(2).toUpperCase(), resultSet.getString(1));
			}

		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (prep != null)
					prep.close();
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}

	}

	public static int getTransactionId(String transactionName) {
		if (hashmapTransactionMappings.isEmpty()) {
			loadTransactionMappings();
			logger.info("hashmap is empty loading hashmap of transaction Mapping");
			return Integer.valueOf(hashmapTransactionMappings.get(transactionName));
		} else {
			if (hashmapTransactionMappings.containsKey(transactionName)) {
				logger.info("hashmap is loaded value is "+Integer.valueOf(hashmapTransactionMappings.get(transactionName)));
				return Integer.valueOf(hashmapTransactionMappings.get(transactionName));
				
			}
		}
		return -1;

	}

	// public static int getTransactionId(String transactionName) {
	// int transactionId = -1;
	// ResultSet resultSet = null;
	// PreparedStatement prep = null;
	// try {
	//
	// resultSet = DBFactory.getDbConnection().prepareStatement("select
	// transactionId FROM transactionMappings where transactionName='" +
	// transactionName + "'").executeQuery();
	//
	// if (!resultSet.next()) {
	// prep = DBFactory.getDbConnection().prepareStatement("insert into
	// transactionMappings (transactionName) values ('" + transactionName +
	// "')", Statement.RETURN_GENERATED_KEYS );
	// prep.executeUpdate();
	//
	// ResultSet generatedKeys = prep.getGeneratedKeys();
	// if (generatedKeys.next()) {
	// transactionId = generatedKeys.getInt(1);
	// return transactionId;
	// }
	// } else {
	// return resultSet.getInt(1);
	// }
	//
	// } catch (Exception ex) {
	// logger.error(Helper.GetException(ex));
	// }
	// finally {
	// try{
	// if(resultSet != null)
	// resultSet.close();
	// if(prep != null)
	// prep.close();
	// }catch (Exception e) {
	// logger.error(Helper.GetException(e));
	// }
	// }
	// return transactionId;
	// }
}
