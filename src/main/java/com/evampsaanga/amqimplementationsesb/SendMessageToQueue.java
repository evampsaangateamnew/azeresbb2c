package com.evampsaanga.amqimplementationsesb;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsappserver.RecieveLogsFromAppServer;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.developer.utils.Helper;

/**
 * Class contains methods for receiving messages from ESB Server which are
 * processed to be inserted in logs table.
 * 
 * @author EvampSaanga
 *
 */
public class SendMessageToQueue {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	private static ConnectionFactory factory = null;
	private static Connection connection = null;
	private static Session session = null;
	private static Destination destination = null;
	private static MessageProducer producer = null;
	private static MessageConsumer consumer = null;
	private static String url = "";
	private static String username = "";
	private static String password = "";
	private static String queueName = "";

	/**
	 * Initialize connection for Queue
	 */
	public static void init() {
		try {
			initializeQueueConfiguration();
			factory = new ActiveMQConnectionFactory(url);
			connection = factory.createConnection(username, password);
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queueName);
			producer = session.createProducer(destination);
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
	}

	/**
	 * Method to send message to Queue, receives message as input
	 * 
	 * @param queueInputRequest
	 */
	public static void sendMessagetoQueue(String queueInputRequest) {
		try {
			if (producer == null) {
				init();
				TextMessage message = session.createTextMessage();
				message.setText(queueInputRequest);
				producer.send(message);
				consumer = session.createConsumer(destination);
				QueueMessageListener msgLis = new QueueMessageListener();
				consumer.setMessageListener(msgLis);
				RecieveLogsFromAppServer.sendMessagetoQueue("STARTED APP");
			} else {
				TextMessage message = session.createTextMessage();
				message.setText(queueInputRequest);
				producer.send(message);
			}
		} catch (JMSException e) {
			logger.error(Helper.GetException(e));
		}
	}

	/**
	 * Method to initialize all configurations for Queue
	 */
	private static void initializeQueueConfiguration() {
		logger.debug("Initializing queue configuration");
		url = ConfigurationManager.getConfigurationFromCache("esb.logs.url");
		username = ConfigurationManager.getConfigurationFromCache("esb.logs.username");
		password = ConfigurationManager.getConfigurationFromCache("esb.logs.password");
		queueName = ConfigurationManager.getConfigurationFromCache("esb.logs.queuename");
	}
}
