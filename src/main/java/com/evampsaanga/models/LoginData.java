package com.evampsaanga.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginData {

	private String default_billing;

	private String customer_gender;

	private String otp_source;

	private String pic_veon;

	private String default_shipping;

	private String brand_name;

	private String pic_tariffs_permissions;

	private String pic_pin;

	private String first_failure;

	private String company_name;

	private String confirmation;

	private String gender;

	private String failures_num;

	private String reward_warning_notification;

	private String website_id;

	private String middlename;

	private String account_id;

	private String group_id;

	private String brand_id;

	private String store_id;

	private String ngbss_status;

	private String contact_number;

	private String disable_auto_group_change;

	private String pic_allowed_tariffs;

	private String updated_at;

	private String reward_update_notification;

	private String email;

	private String prefix;

	private String offering_name;

	private String language;

	private String increment_id;

	private String custom_profile_image;

	private String password_hash;

	private String lock_expires;

	private String pic_attribute2;

	private String pic_attribute1;

	private String lastname;

	private String entity_id;

	private String pic_attribute4;

	private String customer_type;

	private String pic_attribute3;

	private String title;

	private String rp_token_created_at;

	private String pic_status;

	private String created_at;

	private String pic_allowed_offers;

	private String is_active;

	private String pic_tnc;

	private String password_unhashed;

	private String msisdn;

	private String firstname;

	private String taxvat;

	private String suffix;

	private String subscriber_type;

	private String otp_msisdn;

	private String offering_id;

	private String pic_new_reset;

	private String dob;

	private String pic_serial_no;

	private String rp_token;

	private String channel;

	private String created_in;

	private String customer_id;

	private String token;
/*	private String rateus_android;
	private String rateus_ios;*/

	/*public String getRateus_android() {
		return rateus_android;
	}

	public void setRateus_android(String rateus_android) {
		this.rateus_android = rateus_android;
	}

	public String getRateus_ios() {
		return rateus_ios;
	}

	public void setRateus_ios(String rateus_ios) {
		this.rateus_ios = rateus_ios;
	}*/

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDefault_billing() {
		return default_billing;
	}

	public void setDefault_billing(String default_billing) {
		this.default_billing = default_billing;
	}

	public String getCustomer_gender() {
		return customer_gender;
	}

	public void setCustomer_gender(String customer_gender) {
		this.customer_gender = customer_gender;
	}

	public String getOtp_source() {
		return otp_source;
	}

	public void setOtp_source(String otp_source) {
		this.otp_source = otp_source;
	}

	public String getPic_veon() {
		return pic_veon;
	}

	public void setPic_veon(String pic_veon) {
		this.pic_veon = pic_veon;
	}

	public String getDefault_shipping() {
		return default_shipping;
	}

	public void setDefault_shipping(String default_shipping) {
		this.default_shipping = default_shipping;
	}

	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}

	public String getPic_tariffs_permissions() {
		return pic_tariffs_permissions;
	}

	public void setPic_tariffs_permissions(String pic_tariffs_permissions) {
		this.pic_tariffs_permissions = pic_tariffs_permissions;
	}

	public String getPic_pin() {
		return pic_pin;
	}

	public void setPic_pin(String pic_pin) {
		this.pic_pin = pic_pin;
	}

	public String getFirst_failure() {
		return first_failure;
	}

	public void setFirst_failure(String first_failure) {
		this.first_failure = first_failure;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFailures_num() {
		return failures_num;
	}

	public void setFailures_num(String failures_num) {
		this.failures_num = failures_num;
	}

	public String getReward_warning_notification() {
		return reward_warning_notification;
	}

	public void setReward_warning_notification(String reward_warning_notification) {
		this.reward_warning_notification = reward_warning_notification;
	}

	public String getWebsite_id() {
		return website_id;
	}

	public void setWebsite_id(String website_id) {
		this.website_id = website_id;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getAccount_id() {
		return account_id;
	}

	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	public String getNgbss_status() {
		return ngbss_status;
	}

	public void setNgbss_status(String ngbss_status) {
		this.ngbss_status = ngbss_status;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getDisable_auto_group_change() {
		return disable_auto_group_change;
	}

	public void setDisable_auto_group_change(String disable_auto_group_change) {
		this.disable_auto_group_change = disable_auto_group_change;
	}

	public String getPic_allowed_tariffs() {
		return pic_allowed_tariffs;
	}

	public void setPic_allowed_tariffs(String pic_allowed_tariffs) {
		this.pic_allowed_tariffs = pic_allowed_tariffs;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getReward_update_notification() {
		return reward_update_notification;
	}

	public void setReward_update_notification(String reward_update_notification) {
		this.reward_update_notification = reward_update_notification;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getOffering_name() {
		return offering_name;
	}

	public void setOffering_name(String offering_name) {
		this.offering_name = offering_name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getIncrement_id() {
		return increment_id;
	}

	public void setIncrement_id(String increment_id) {
		this.increment_id = increment_id;
	}

	public String getCustom_profile_image() {
		return custom_profile_image;
	}

	public void setCustom_profile_image(String custom_profile_image) {
		this.custom_profile_image = custom_profile_image;
	}

	public String getPassword_hash() {
		return password_hash;
	}

	public void setPassword_hash(String password_hash) {
		this.password_hash = password_hash;
	}

	public String getLock_expires() {
		return lock_expires;
	}

	public void setLock_expires(String lock_expires) {
		this.lock_expires = lock_expires;
	}

	public String getPic_attribute2() {
		return pic_attribute2;
	}

	public void setPic_attribute2(String pic_attribute2) {
		this.pic_attribute2 = pic_attribute2;
	}

	public String getPic_attribute1() {
		return pic_attribute1;
	}

	public void setPic_attribute1(String pic_attribute1) {
		this.pic_attribute1 = pic_attribute1;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEntity_id() {
		return entity_id;
	}

	public void setEntity_id(String entity_id) {
		this.entity_id = entity_id;
	}

	public String getPic_attribute4() {
		return pic_attribute4;
	}

	public void setPic_attribute4(String pic_attribute4) {
		this.pic_attribute4 = pic_attribute4;
	}

	public String getCustomer_type() {
		return customer_type;
	}

	public void setCustomer_type(String customer_type) {
		this.customer_type = customer_type;
	}

	public String getPic_attribute3() {
		return pic_attribute3;
	}

	public void setPic_attribute3(String pic_attribute3) {
		this.pic_attribute3 = pic_attribute3;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRp_token_created_at() {
		return rp_token_created_at;
	}

	public void setRp_token_created_at(String rp_token_created_at) {
		this.rp_token_created_at = rp_token_created_at;
	}

	public String getPic_status() {
		return pic_status;
	}

	public void setPic_status(String pic_status) {
		this.pic_status = pic_status;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getPic_allowed_offers() {
		return pic_allowed_offers;
	}

	public void setPic_allowed_offers(String pic_allowed_offers) {
		this.pic_allowed_offers = pic_allowed_offers;
	}

	public String getIs_active() {
		return is_active;
	}

	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}

	public String getPic_tnc() {
		return pic_tnc;
	}

	public void setPic_tnc(String pic_tnc) {
		this.pic_tnc = pic_tnc;
	}

	public String getPassword_unhashed() {
		return password_unhashed;
	}

	public void setPassword_unhashed(String password_unhashed) {
		this.password_unhashed = password_unhashed;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getTaxvat() {
		return taxvat;
	}

	public void setTaxvat(String taxvat) {
		this.taxvat = taxvat;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSubscriber_type() {
		return subscriber_type;
	}

	public void setSubscriber_type(String subscriber_type) {
		this.subscriber_type = subscriber_type;
	}

	public String getOtp_msisdn() {
		return otp_msisdn;
	}

	public void setOtp_msisdn(String otp_msisdn) {
		this.otp_msisdn = otp_msisdn;
	}

	public String getOffering_id() {
		return offering_id;
	}

	public void setOffering_id(String offering_id) {
		this.offering_id = offering_id;
	}

	public String getPic_new_reset() {
		return pic_new_reset;
	}

	public void setPic_new_reset(String pic_new_reset) {
		this.pic_new_reset = pic_new_reset;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPic_serial_no() {
		return pic_serial_no;
	}

	public void setPic_serial_no(String pic_serial_no) {
		this.pic_serial_no = pic_serial_no;
	}

	public String getRp_token() {
		return rp_token;
	}

	public void setRp_token(String rp_token) {
		this.rp_token = rp_token;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCreated_in() {
		return created_in;
	}

	public void setCreated_in(String created_in) {
		this.created_in = created_in;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

}