package com.evampsaanga.models;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class LoginRequest extends BaseRequest{
	
	private String password;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
