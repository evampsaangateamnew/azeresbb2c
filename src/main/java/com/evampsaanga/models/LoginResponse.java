package com.evampsaanga.models;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.magento.getpredefineddataV2.GetPredefinedDataResponseData;

public class LoginResponse extends BaseResponse {

	private LoginData loginData;
	private GetPredefinedDataResponseData predefinedData;

	public LoginData getLoginData() {
		return loginData;
	}

	public void setLoginData(LoginData loginData) {
		this.loginData = loginData;
	}

	public GetPredefinedDataResponseData getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(GetPredefinedDataResponseData predefinedData) {
		this.predefinedData = predefinedData;
	}

}
