package com.evampsaanga.configs;

public class Transactions {

	// MNP
	public static final String MNP_GET_SUBSCRIBER = "MNP GET SUBSCRIBER";
	public static final String MNP_GET_SUBSCRIBER_VERIFY_PASSPORT_ID = "VERIFY PASSPORT ID";

	// NAR TV
	public static final String NAR_TV_GET_SUBSCRIPTIONS = "NAR TV GET SUBSCRIPTIONS";
	public static final String NAR_TV_GET_SUBSCRIBE = "NAR TV SUBSCRIBE";

	// LOAN
	public static final String LOAN_GET_PROVISIONS = "LOAN GET PROVISIONS";
	public static final String LOAN_GET_COLLECTIONS = "LOAN GET COLLECTIONS";
	public static final String LOAN_GET_REQUEST_FEE_CHARGINGS = "LOAN GET REQUEST FEE CHARGINGS";
	public static final String LOAN_PROVIDE_NEW_CREDIT = "LOAN PROVIDE NEW CREDIT";
	public static final String LOAN_GET_DEBT_INFO = "LOAN GET DEBT INFO";
	public static final String LOAN_GET_LOAN = "GET LOAN";
	public static final String LOAN_GET_LOAN_REQUEST_HISTORY = "LOAN GET LOAN REQUEST HISTORY";
	public static final String LOAN_GET_LOAN_PAYMENT_HISTORY = "LOAN GET LOAN PAYMENT HISTORY";
	public static final String LOAN_GET_SUBSCRIBER_HISTORY = "LOAN GET SUBSCRIBER HISTORY";
	public static final String LOAN_REQUEST_MONEY = "LOAN REQUEST MONEY";

	public static final String BASE_MAGENTO_TRANSACTION_NAME = "MagentoServies";
	public static final String APP_MENU_TRANSACTION_NAME = "AppMenu";
	public static final String APP_RESUME_TRANSACTION_NAME = "APP RESUME";
	public static final String APP_FAQ_TRANSACTION_NAME = "App Faq";
	public static final String LOGIN_TRANSACTION_NAME = "Log In";
	public static final String LOGOUT_TRANSACTION_NAME = "LOG OUT";
	public static final String LOGIN_EXTERNAL_TRANSACTION_NAME = "External Log In";
	public static final String REFRESH_ESB_CACHE_NAME = "REFRESH ESB CACHE";
	public static final String SAVE_CUSTOMER_TRANSACTION_NAME = "Save Customer";
	public static final String SEND_OTP_TRANSACTION_NAME = "Send otp";
	public static final String RESEND_OTP_TRANSACTION_NAME = "Resend otp";
	public static final String SIGNUP_TRANSACTION_NAME = "SIGN UP";
	public static final String SIGNUP_VERIFY_OTP_TRANSACTION_NAME = "Sign up verify otp";
	public static final String FORGOT_PASSWORD_TRANSACTION_NAME = "Forgot Pass";
	public static final String CHANGE_PASSWORD_TRANSACTION_NAME = "CHANGE PASSWORD";

	public static final String CUSTOMER_DATA_TRANSACTION_NAME_B2B = "Get Customer Data B2B";
	public static final String CUSTOMER_DATA_TRANSACTION_NAME = "GET CUSTOMER DATA";
	public static final String EXCHANGE_SERVICE_TRANSACTION_NAME = "Exchange Service";

	public static final String CONTACTUS__TRANSACTION_NAME = "Contact Us";
	public static final String TARRIF_DETAILS_TRANSACTION_NAME = "Tarrif Details";
	public static final String SUPPLEMENTARY_SERVICES_TRANSACTION_NAME = "Supplementary Services";
	public static final String SPECIAL_OFFERS = "Special Offers";
	public static final String GET_ROAMING_TRANSACTION_NAME = "Get Roaming";
	public static final String HOME_PAGE_TRANSACTION_NAME = "HOME PAGE";

	public static final String TOPUP_TRANSACTION_NAME = "TOP UP";
	public static final String TOPUP_TRANSACTION_NAME_B2B = "TOPUP B2B";

	public static final String TRANSFER_MONEY_TRANSACTION_NAME = "MONEY TRANSFER";
	public static final String LOAN_REQUEST_TRANSACTION_NAME = "Loan Request";
	public static final String LOAN_REQUEST_HISTORY_TRANSACTION_NAME = "Loan Request History";
	public static final String LOAN_PAYMENT_HISTORY_TRANSACTION_NAME = "Loan Payment History";

	public static final String GET_FNF_TRANSACTION_NAME = "Get FnF";
	public static final String GET_FNF_TRANSACTION_NAME_B2B = "Get FnF B2B";

	// public static final String GET_CDRS_BY_DATE_TRANSACTION_NAME = "Get CDRs
	// By
	// Date";

	// Get CDR By Date For Both B2B & B2C
	public static final String GET_CDRS_BY_DATE_TRANSACTION_NAME = "GET CDRS BY DATE";
	public static final String GET_CDRS_BY_DATE_TRANSACTION_NAME_B2B = "GET CDRS BY DATE B2B";

	public static final String GET_CDRS_SUMMARY_TRANSACTION_NAME_B2B = "Get CDRs Summary B2B";
	public static final String GET_CDRS_SUMMARY_TRANSACTION_NAME = "GET CDRs SUMMARY";

	public static final String GET_CDRS_OPERATION_HISTORY_TRANSACTION_NAME = "Get CDRs Operation History";
	public static final String GET_CDRS_OPERATION_HISTORY_TRANSACTION_NAME_B2B = "Get CDRs Operation History B2B";

	public static final String ADD_FNF_TRANSACTION_NAME = "ADD FNF";
	public static final String DELETE_FNF_TRANSACTION_NAME = "DELETE FNF";
	public static final String GET_CDRSBY_DATE_OTP_TRANSACTION_NAME = "VERIFY ACCOUNT DETAILS";
	public static final String VERIFY_CDRSBY_DATE_OTP_TRANSACTION_NAME = "Verify CDRsByDate OTP";
	public static final String UPDATE_CUSTOMER_EMAIL_TRANSACTION_NAME = "Update Customer Email";
	public static final String STORE_LOCATOR_TRANSACTION_NAME = "STORE LOCATOR";
	public static final String REPORT_LOST_SIM_TRANSACTION_NAME = "Report Lost";
	public static final String HLR_QUERY_BALANCE_TRANSACTION_NAME = "HRL QUERY BALANCE";

	// public static final String NOTIFICATIONS_HISTORY_TRANSACTION_NAME = "Get
	// Notifications";

	public static final String NOTIFICATIONS_HISTORY_TRANSACTION_NAME = "GET NOTIFICATIONS HISTORY";
	public static final String NOTIFICATIONS_HISTORY_TRANSACTION_NAME_B2B = "GET NOTIFICATIONS HISTORY B2B";

	public static final String NOTIFICATIONS_COUNT_TRANSACTION_NAME = "GET NOTIFICATIONS COUNT";
	public static final String NOTIFICATIONS_COUNT_TRANSACTION_NAME_B2B = "GET NOTIFICATIONS COUNT B2B";

	public static final String GET_PREDEFINED_DATA_TRANSACTION_NAME = "Get Pre Define Data";

	public static final String GET_SENDSMS_MAGENTO_TRANSACTION_NAME = "Send SMS";
	public static final String GET_SENDSMS_MAGENTO_TRANSACTION_NAME_B2B = "Send SMS B2B";

	public static final String CHANGE_TARRIF_TRANSACTION_NAME = "Change Tarrif";
	public static final String CALL_FORWARDING_TRANSACTION_NAME = "Call Forwarding";

	public static final String CHANGE_BILLING_LANGUAGE_TRANSACTION_NAME = "Change Language";
	public static final String CHANGE_BILLING_LANGUAGE_TRANSACTION_NAME_B2B = "Change Language B2B";

	public static final String REFRESHAPP_CACHE_TRANSACTION_NAME = "Refresh APP server Cache";

	public static final String CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME = "Change Supplimentry Offering";
	public static final String CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME_B2B = "Change Supplimentry Offering B2B";
	public static final String CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME_SIGNUP = "Change Supplimentry Offering Signup";
	public static final String CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME_SIGNUP_B2B = "Change Supplimentry Offering B2B Signup";

	public static final String GET_CORE_SERVICES_TRANSACTION_NAME = "GET CORE SERVICE";
	public static final String GET_PAYG_STATUS_TRANSACTION_NAME = "GET PAYG STATUS";
	public static final String GET_CORE_SERVICES_TRANSACTION_NAME_B2B = "GET CORE SERVICE B2B";
	public static final String GET_PAYG_STATUS_TRANSACTION_NAME_B2B = "GET AYG STATUS B2B";

	public static final String GET_BULK_CORE_SERVICES_TRANSACTION_NAME = "GET BULK CORE SERVICE";
	public static final String GET_BULK_CORE_SERVICES_TRANSACTION_NAME_B2B = "GET BULK CORE SERVICE B2B";

	public static final String MANIPULATE_CORE_SERVICES_TRANSACTION_NAME = "MANIPULATE CORE SERVICES";
	public static final String GET_TRANSACTION_NAME = "Refresh APP server Cache";

	public static final String MYSUBSCRIPTION_TRANSACTION_NAME = "My Subscription Usage details";
	public static final String MYSUBSCRIPTION_TRANSACTION_NAME_B2B = "My Subscription Usage details B2B";

	public static final String MYSUBSCRIPTION__FOR_PORTAL_TRANSACTION_NAME = "MY SUBSCRIPTIONS FOR PORTAL";
	public static final String SEND_SMS_STATUS = "Free SMS status";
	public static final String SEND_FREE_SMS = "Free SMS";
	public static final String PAYMENT_LOG = "QUERY PAYMENT LOG";
	public static final String MANIPULATE_FNF_TRANSACTION_NAME = "Manipulate FnF";
	public static final String SEND_INTERNET_SETTINGS = "SEND INTERNET SETTINGS";
	public static final String UPLOAD_IMAGE_TRANSACTION_NAME = "UPLOAD IMAGE";
	public static final String VERIFY_APP_VERSION_TRANSACTION_NAME = "VERIFY APP VERSION";
	public static final String HISTORY_RESEND_OTP_TRANSACTION_NAME = "HISTORY RESEND OTP";
	public static final String VERIFY_CDRS_BY_DATE_OTP_TRANSACTION_NAME = "Verify CDRsByDate OTP";
	public static final String ADD_FCM_KEY_TRANSACTION_NAME = "ADD FCM KEY";
	public static final String ADD_UPDATE_NOTIFICATIONS_CONFIGURATIONS = "ADD/UPDATE NOTIFICATIONS CONFIGURATIONS";
	public static final String USERS_GROUP_DATA = "USER GROUP DATA B2B";
	public static final String INVOICE = "Query Invoice";
	public static final String QUERY_BALANCE_PIC = "Query Balance PIC Data";
	public static final String MANAGE_ORDER = "MANAGE ORDER B2B";
	public static final String RATE_US = "Rate Us";
	public static final String DASHBOARD = "DASHBOARD B2B";
	public static final String SEND_EMAIL = "Send Email";
	public static final String CHANGE_NETWORK_SETTINGS = "Change Network Settings";
	public static final String ULDUZUM_GET_CATEGORIES_TRANSACTION_NAME = "ULDUZUM GET CATEGORIES";
	public static final String ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME = "ULDUZUM GET CATEGORIES";
	public static final String ULDUZUM_GET_USAGE_TOTAL_TRANSACTION_NAME = "ULDUZUM GET USAGE TOTALS";
	public static final String ULDUZUM_GET_USAGE_HISTORY_TRANSACTION_NAME = "ULDUZUM GET USAGE HISTORY";
	public static final String ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME = "ULDUZUM GET UNUSED CODES";
	public static final String ACTION_HISTORY = "ACTION HISTORY B2B";

	// Get Home Page For B2B
	public static final String GET_HOME_PAGE_B2B = "HOME PAGE B2B";
	public static final String ACTION_HISTORY_B2B = "Action History B2B";
	public static final String COMPANY_INVOICE = "COMPANY INVOICE B2B";

	public static final String EXCHANGE_SERVICE_GETVALUES_TRANSACTION_NAME = "Exchange Service Get Values";
	public static final String EXCHANGE_SERVICE_DELETE_RECORDS_TRANSACTION_NAME = "Exchange Service Delete Records";
	public static final String EXCHANGE_SERVICE_COUNT_TRANSACTION_NAME = "Exchange Service Count";

	// Plastic Card
	public static final String PLASTIC_CARD_INITIATE_PAYMENT_TRANSACTION_NAME = "PLASTIC CARD INITIATE PAYMENT";
	public static final String PLASTIC_CARD_PROCESS_PAYMENT_TRANSACTION_NAME = "PLASTIC CARD PROCESS PAYMENT";
	public static final String GET_FAST_PAYMENT_REQUEST_TRANSACTION_NAME = "PLASTIC CARD GET FAST PAYMENT";
	public static final String GET_SAVED_CARDS_REQUEST_TRANSACTION_NAME = "PLASTIC CARD GET SAVED CARDS";
	public static final String DELETE_FAST_TOPUP_REQUEST_TRANSACTION_NAME = "PLASTIC CARD DELETE FAST TOPUP";
	public static final String ADD_PAYMENT_SCHEDULER_REQUEST_TRANSACTION_NAME = "PLASTIC CARD ADD PAYMENT SCHEDULER";
	public static final String GET_SCHEDULED_PAYMENTS_REQUEST_TRANSACTION_NAME = "PLASTIC CARD GET SCHEDULED PAYMENTS";
	public static final String DELETE_SAVED_CARD_REQUEST_TRANSACTION_NAME = "PLASTIC CARD DELETE SAVED CARD";
	public static final String DELETE_PAYMENT_SCHEDULER_REQUEST_TRANSACTION_NAME = "PLASTIC CARD DELETE PAYMENT SCHEDULER";
	public static final String PLASTIC_CARD_MAKE_PAYMENT_PROCESS_TRANSACTION_NAME = "MAKE PAYMENT PROCESS";
	public static final String INTERNET_SERVICES_TRANSACTION_NAME = "GET INTERNET SERVICES";

	// Survey
	public static final String GET_SURVEYS_TRANSACTION_NAME = "GET SURVEYS";
	public static final String SAVE_SURVEY_TRANSACTION_NAME = "SAVE SURVEY";
	public static final String GET_SURVEY_COUNT_TRANSACTION_NAME = "GET SURVEY COUNT";
	public static final String GET_ALL_SURVEY_COUNT_TRANSACTION_NAME = "GET ALL SURVEY COUNT";
	public static final String GET_USER_SURVEY_TRANSACTION_NAME = "GET USER SURVEY";

}
