package com.evampsaanga.configs;

public class ThirdPartyNames {

	public static final String APPMENU = "INTERNAL MAGENTO SERVICES";
	public static final String INTERNAL_CACHE = "INTERNAL_CACHE";
	public static final String APPFAQ = "INTERNAL MAGENTO SERVICES";
	public static final String LOGIN = "CRM_GetCustomer,HLRWEB_GetSubscriber,INTERNAL";
	public static final String SAVE_CUSTOMER = "INTERNAL MAGENTO SERVICES";
	public static final String SEND_OTP = "CRM_GetCustomer,HLRWEB_GetSubscriber,INTERNAL";
	public static final String RESEND_OTP = "CRM_GetCustomer,HLRWEB_GetSubscriber,INTERNAL";
	public static final String SIGNUP_VERIFY = "INTERNAL MAGENTO SERVICES";
	public static final String FORGOT_PASS = "INTERNAL MAGENTO SERVICES";
	public static final String CHANGE_PASS = "INTERNAL MAGENTO SERVICES";
	public static final String CUSTOMER_DATA = "CRM_GetCustomer,CRM_GetSubscriber,HLRWEB_GetSubscriber";

	public static final String EXCHANGE_SERVICE = "Query Adjustment ExchangeService";
	public static final String EXCHANGE_SERVICE_GETVALUES = "ExchangeService Get Values";
	public static final String CONTACTUS = "INTERNAL MAGENTO SERVICES";
	public static final String TARRIF_DETAILS = "INTERNAL MAGENTO SERVICES TARIFF DETAILS";
	public static final String SUPPLEMENTARY_SERVICES = "INTERNAL";
	public static final String SPECIAL_OFFERS = "INTERNAL MAGENTO SPECIAL OFFERS";
	public static final String GET_ROAMING = "INTERNAL MAGENTO SERVICES";
	public static final String HOME_PAGE = "HLRWEB_BalanaceService, CRMServices, SubscriberServices,QueryLoan";
	public static final String TOPUP = "CBSARService";
	public static final String TRANSFER_MONEY = "Bakcel Database";
	public static final String LOAN_REQUEST = "USSDService";
	public static final String LOAN_REQUEST_HISTORY = "CBSARService";
	public static final String LOAN_PAYMENT_HISTORY = "CBSARService";
	public static final String GET_FNF = "CRMServices_GetFnFData";
	public static final String GET_CDRS_BY_DATE = "INTERNAL BAKCELL";
	public static final String GET_CDRS_SUMMARY = "INTERNAL BAKCELL";
	public static final String GET_CDRS_OPERATION_HISTORY = "INTERNAL BAKCELL";
	public static final String MANIPULATE_FNF = "BAKCELL ORDER HANDLER";
	public static final String GET_CDRS_BYDATE_OTP = "INTERNAL MAGENTO SERVICES";
	public static final String VERIFY_CDRS_BYDATE_OTP = "INTERNAL MAGENTO SERVICES";
	public static final String UPDATE_CUSTOMER_EMAIL = "INTERNAL MAGENTO SERVICES";
	public static final String STORE_LOCATORL = "INTERNAL MAGENTO SERVICES";
	public static final String HLR_QUERY_BALANCE = "HLR BALANCE";
	public static final String GET_NOTIFICATION = "INTERNAL APP_SERVER DB";
	public static final String GET_NOTIFICATION_COUNT = "INTERNAL APP_SERVER DB";
	public static final String GET_PREDEFINED_DATA = "INTERNAL MAGENTO SERVICES";
	public static final String SEND_SMS_MAGENTO = "INTERNAL MAGENTO SERVICES";
	public static final String CHANGE_TARRIF = "Huawei CRM Services";
	public static final String CALL_FORWARDING = "Order Handler";
	public static final String CHANGE_LANGUAGE = "Order Handler";
	public static final String REFRESH_APPSERVER_CACHE = "APP Server API";
	public static final String CHANGE_SUPPLIMENTRY_OFFERING = "USSDServices";
	public static final String GET_CORE_SERVICES = "CRMSubscriberService";
	public static final String MNP_GET_SUBSCRIBER = "MNP Get Subscriber";
	public static final String MY_SUBSCRIPTION = "CBSBBService freeunit";
	public static final String SEND_SMS_MAIN = "SMSC";
	public static final String FREE_SMS_STATUS = "Database";
	public static final String SEND_FREE_SMS = "Database Internal";
	public static final String PAYMENT_LOG = "CBSARService";
	public static final String SEND_INTERNET_SETTINGS = "INTERNAL APP_SERVER DB";
	public static final String MANAGE_ORDER = "ESB Database";
	public static final String RATEUS = "INTERNAL MAGENTO SERVICES";
	public static final String SENDEMAIL = "INTERNAL DEVELOPMENT ESB";
	public static final String BAKCELL_ORDER_HANDLER = "BAKCELL ORDER HANDLER";
	public static final String ULDUZUM = "ULDUZUM THIRD PARTY";

	public static final String ODS = "ODS";

	// Plastic Card
	public static final String PLASTIC_CARD__INITIATE_PAYMENT = "Initiate Payment";
	public static final String PLASTIC_CARD_MAKE_PAYMENT_PROCESS = "Make payment process";
	public static final String PLASTIC_CARD__GET_CARD_TOKEN = "Get Card Token";
	public static final String GET_FAST_PAYMENT_REQUEST = "Get Fast Payment";
	public static final String GET_SAVED_CARDS_REQUEST = "Get Saved Cards";
	public static final String DELETE_FAST_TOPUP_REQUEST = "Delete Fast Topup";
	public static final String ADD_PAYMENT_SCHEDULER_REQUEST = "Add Payment Scheduler";
	public static final String GET_SCHEDULED_PAYMENTS_REQUEST = "Get Schedule Payments";
	public static final String DELETE_SAVED_CARD_REQUEST = "Delete Saved Card";
	public static final String DELETE_PAYMENT_SCHEDULER_CARD_REQUEST = "Delete Payment Scheduler";
	public static final String INTERNET_SERVICES = "INTERNAL";

	// survey
	public static final String GET_SURVEYS = "Get Surveys";
	public static final String SAVE_SURVEY = "Save Survey";
	public static final String GET_SURVEYS_COUNT = "Get Survey Count";
	public static final String GET_ALL_SURVEYS_COUNT = "Get All Survey Count";
	public static final String GET_USER_SURVEY = "Get User Survey";

}
