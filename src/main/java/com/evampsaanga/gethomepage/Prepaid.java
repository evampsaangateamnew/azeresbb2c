package com.evampsaanga.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "mainWallet", "countryWideWallet", "bounusWallet" })
public class Prepaid {
	@JsonProperty("mainWallet")
	private MainWallet mainWallet = new MainWallet();
	@JsonProperty("countryWideWallet")
	private CountryWideWallet countryWideWallet = new CountryWideWallet();
	@JsonProperty("bounusWallet")
	private BounusWallet bounusWallet = new BounusWallet();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("mainWallet")
	public MainWallet getMainWallet() {
		return mainWallet;
	}

	@JsonProperty("mainWallet")
	public void setMainWallet(MainWallet mainWallet) {
		this.mainWallet = mainWallet;
	}

	@JsonProperty("countryWideWallet")
	public CountryWideWallet getCountryWideWallet() {
		return countryWideWallet;
	}

	@JsonProperty("countryWideWallet")
	public void setCountryWideWallet(CountryWideWallet countryWideWallet) {
		this.countryWideWallet = countryWideWallet;
	}

	@JsonProperty("bounusWallet")
	public BounusWallet getBounusWallet() {
		return bounusWallet;
	}

	@JsonProperty("bounusWallet")
	public void setBounusWallet(BounusWallet bounusWallet) {
		this.bounusWallet = bounusWallet;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
