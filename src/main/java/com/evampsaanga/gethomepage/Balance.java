package com.evampsaanga.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "prepaid", "postpaid" })
public class Balance {
	@JsonIgnore
	private String returnCode = "";
	@JsonIgnore
	private String returnMsg = "";

	/**
	 * @return the returnCode
	 */
	public String getReturnCode() {
		return returnCode;
	}

	/**
	 * @param returnCode
	 *            the returnCode to set
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * @return the returnMsg
	 */
	public String getReturnMsg() {
		return returnMsg;
	}

	/**
	 * @param returnMsg
	 *            the returnMsg to set
	 */
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	@JsonProperty("prepaid")
	private Prepaid prepaid = new Prepaid();
	@JsonProperty("postpaid")
	private Postpaid postpaid = new Postpaid();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("prepaid")
	public Prepaid getPrepaid() {
		return prepaid;
	}

	@JsonProperty("prepaid")
	public void setPrepaid(Prepaid prepaid) {
		this.prepaid = prepaid;
	}

	@JsonProperty("postpaid")
	public Postpaid getPostpaid() {
		return postpaid;
	}

	@JsonProperty("postpaid")
	public void setPostpaid(Postpaid postpaid) {
		this.postpaid = postpaid;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
