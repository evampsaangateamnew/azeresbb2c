package com.evampsaanga.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "balance", "installments", "mrc", "credit", "freeResources", "offeringNameDisplay","status","creditValue" })
public class Data_ {
	@JsonProperty("balance")
	private Balance balance = new Balance();


	@JsonProperty("mrc")
	private Mrc mrc = new Mrc();
	@JsonProperty("credit")
	private Credit credit = new Credit();
	@JsonProperty("freeResources")
	private FreeResources freeResources = new FreeResources();
	@JsonProperty("offeringNameDisplay")
	private String offeringNameDisplay;
	@JsonProperty("status")
	private String status;
	@JsonProperty("creditValue")
	private String creditValue;


	
	
	
	public String getCreditValue() {
		return creditValue;
	}

	public void setCreditValue(String creditValue) {
		this.creditValue = creditValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOfferingNameDisplay() {
		return offeringNameDisplay;
	}

	public void setOfferingNameDisplay(String offeringNameDisplay) {
		this.offeringNameDisplay = offeringNameDisplay;
	}

	public FreeResources getFreeResources() {
		return freeResources;
	}

	public void setFreeResources(FreeResources freeResources) {
		this.freeResources = freeResources;
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("balance")
	public Balance getBalance() {
		return balance;
	}

	@JsonProperty("balance")
	public void setBalance(Balance balance) {
		this.balance = balance;
	}

	/**
	 * @return the installments
	 */
	

	@JsonProperty("mrc")
	public Mrc getMrc() {
		return mrc;
	}

	@JsonProperty("mrc")
	public void setMrc(Mrc mrc) {
		this.mrc = mrc;
	}

	@JsonProperty("credit")
	public Credit getCredit() {
		return credit;
	}

	@JsonProperty("credit")
	public void setCredit(Credit credit) {
		this.credit = credit;
	}



	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
