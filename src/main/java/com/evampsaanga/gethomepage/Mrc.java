package com.evampsaanga.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.evampsaanga.configs.Constants;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "mrcTitleValue", "mrcCurrency", "mrcDate", "mrcInitialDate", "mrcLimit", "mrcStatus" })
public class Mrc {
	@JsonProperty("mrcStatus")
	private String mrcStatus = "Unpaid";
	@JsonProperty("mrcTitleValue")
	private String mrcTitleValue = "";
	@JsonProperty("mrcCurrency")
	private String mrcCurrency = "AZN";
	@JsonProperty("mrcDate")
	private String mrcDate = "";
	@JsonProperty("mrcInitialDate")
	private String mrcInitialDate = "";
	@JsonProperty("mrcLimit")
	private String mrcLimit = "10.0";
	@JsonProperty("mrcType")
	private String mrcType = Constants.MRC_TYPE_MONTHLY;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Mrc() {
	}

	public Mrc(String mrcTitleValue, String mrcCurrency, String mrcDate, String mrcInitialDate, String mrcLimit,
			String mrcType) {
		super();
		this.mrcTitleValue = mrcTitleValue;
		this.mrcCurrency = mrcCurrency;
		this.mrcDate = mrcDate;
		this.mrcInitialDate = mrcInitialDate;
		this.mrcLimit = mrcLimit;
		this.mrcType = mrcType;
	}

	public String getMrcType() {
		return mrcType;
	}

	public void setMrcType(String mrcType) {
		this.mrcType = mrcType;
	}

	@JsonProperty("mrcTitleValue")
	public String getMrcTitleValue() {
		return mrcTitleValue;
	}

	@JsonProperty("mrcTitleValue")
	public void setMrcTitleValue(String mrcTitleValue) {
		this.mrcTitleValue = mrcTitleValue;
	}

	@JsonProperty("mrcCurrency")
	public String getMrcCurrency() {
		return mrcCurrency;
	}

	@JsonProperty("mrcCurrency")
	public void setMrcCurrency(String mrcCurrency) {
		this.mrcCurrency = mrcCurrency;
	}

	@JsonProperty("mrcDate")
	public String getMrcDate() {
		return mrcDate;
	}

	@JsonProperty("mrcDate")
	public void setMrcDate(String mrcDate) {
		this.mrcDate = mrcDate;
	}

	@JsonProperty("mrcInitialDate")
	public String getMrcInitialDate() {
		return mrcInitialDate;
	}

	@JsonProperty("mrcInitialDate")
	public void setMrcInitialDate(String mrcInitialDate) {
		this.mrcInitialDate = mrcInitialDate;
	}

	@JsonProperty("mrcLimit")
	public String getMrcLimit() {
		return mrcLimit;
	}

	@JsonProperty("mrcLimit")
	public void setMrcLimit(String mrcLimit) {
		this.mrcLimit = mrcLimit;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public String getMrcStatus() {
		return mrcStatus;
	}

	public void setMrcStatus(String mrcStatus) {
		this.mrcStatus = mrcStatus;
	}
}
