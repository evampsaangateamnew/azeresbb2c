package com.evampsaanga.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "availableBalanceCorporateValue", "availableBalanceIndividualValue", "availableCreditLabel",
		"balanceCorporateValue", "balanceIndividualValue", "balanceLabel", "corporateLabel",
		"currentCreditCorporateValue", "currentCreditIndividualValue", "currentCreditLabel", "individualLabel",
		"outstandingIndividualDebt", "outstandingIndividualDebtLabel", "template" })
public class Postpaid {
	@JsonProperty("availableBalanceCorporateValue")
	private String availableBalanceCorporateValue = "0";
	@JsonProperty("availableBalanceIndividualValue")
	private String availableBalanceIndividualValue = "0";
	@JsonProperty("balanceCorporateValue")
	private String balanceCorporateValue = "0";
	@JsonProperty("balanceIndividualValue")
	private String balanceIndividualValue = "0";
	@JsonProperty("currentCreditCorporateValue")
	private String currentCreditCorporateValue = "0";
	@JsonProperty("currentCreditIndividualValue")
	private String currentCreditIndividualValue = "0";
	@JsonProperty("outstandingIndividualDebt")
	private String outstandingIndividualDebt = "0";


	@JsonProperty("totalPayments")
	private String totalPayments = "0";
	@JsonProperty("totalPaymentsLabel")
	private String totalPaymentsLabel = "Total Payments";

	@JsonProperty("currentCreditLimit")
	private String currentCreditLimit = "0";
	@JsonProperty("currentCreditLimitLabel")
	private String currentCreditLimitLabel = "Credit Limit";

	public String getCurrentCreditLimit() {
		return currentCreditLimit;
	}

	public void setCurrentCreditLimit(String currentCreditLimit) {
		this.currentCreditLimit = currentCreditLimit;
	}

	public String getCurrentCreditLimitLabel() {
		return currentCreditLimitLabel;
	}

//	public void setCurrentCreditLimitLabel(String currentCreditLimitLabel) {
//		this.currentCreditLimitLabel = currentCreditLimitLabel;
//	}

	public String getOutstandingIndividualDebt() {
		return outstandingIndividualDebt;
	}

	public void setOutstandingIndividualDebt(String outstandingIndividualDebt) {
		this.outstandingIndividualDebt = outstandingIndividualDebt;
	}

	public String getTotalPayments() {
		return totalPayments;
	}

	public void setTotalPayments(String totalPayments) {
		this.totalPayments = totalPayments;
	}

	public String getTotalPaymentsLabel() {
		return totalPaymentsLabel;
	}

	public void setTotalPaymentsLabel(String totalPaymentsLabel) {
		this.totalPaymentsLabel = totalPaymentsLabel;
	}

	public Postpaid() {
		super();
	}

	public Postpaid(String mainBalance, String creditLimit, String totalPayments, String outStandingDebt) {
		super();
		this.balanceIndividualValue = mainBalance;
		this.currentCreditLimit = creditLimit;
		this.totalPayments = totalPayments;
		this.outstandingIndividualDebt = outStandingDebt;

	}

	public Postpaid(String balanceIndividualValue, String currentCreditCorporateValue,
			String currentCreditIndividualValue, String outstandingIndividualDebt, int template) {
		super();

		this.balanceIndividualValue = balanceIndividualValue;
		this.currentCreditCorporateValue = currentCreditCorporateValue;
		this.currentCreditIndividualValue = currentCreditIndividualValue;
		this.outstandingIndividualDebt = outstandingIndividualDebt;
	
	}

	public Postpaid(String availableBalanceIndividualValue, String balanceCorporateValue, String balanceIndividualValue,
			String currentCreditCorporateValue, String currentCreditIndividualValue, String outstandingIndividualDebt,
			int template) {
		super();
		this.availableBalanceCorporateValue = availableBalanceCorporateValue;
		this.availableBalanceIndividualValue = availableBalanceIndividualValue;
		this.balanceCorporateValue = balanceCorporateValue;
		this.balanceIndividualValue = balanceIndividualValue;
		this.currentCreditCorporateValue = currentCreditCorporateValue;
		this.currentCreditIndividualValue = currentCreditIndividualValue;
		this.outstandingIndividualDebt = outstandingIndividualDebt;
	
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("availableBalanceCorporateValue")
	public String getAvailableBalanceCorporateValue() {
		return availableBalanceCorporateValue;
	}

	@JsonProperty("availableBalanceCorporateValue")
	public void setAvailableBalanceCorporateValue(String availableBalanceCorporateValue) {
		this.availableBalanceCorporateValue = availableBalanceCorporateValue;
	}

	@JsonProperty("availableBalanceIndividualValue")
	public String getAvailableBalanceIndividualValue() {
		return availableBalanceIndividualValue;
	}

	@JsonProperty("availableBalanceIndividualValue")
	public void setAvailableBalanceIndividualValue(String availableBalanceIndividualValue) {
		this.availableBalanceIndividualValue = availableBalanceIndividualValue;
	}

	@JsonProperty("balanceCorporateValue")
	public String getBalanceCorporateValue() {
		return balanceCorporateValue;
	}

	@JsonProperty("balanceCorporateValue")
	public void setBalanceCorporateValue(String balanceCorporateValue) {
		this.balanceCorporateValue = balanceCorporateValue;
	}

	@JsonProperty("balanceIndividualValue")
	public String getBalanceIndividualValue() {
		return balanceIndividualValue;
	}

	@JsonProperty("balanceIndividualValue")
	public void setBalanceIndividualValue(String balanceIndividualValue) {
		this.balanceIndividualValue = balanceIndividualValue;
	}

	@JsonProperty("currentCreditCorporateValue")
	public String getCurrentCreditCorporateValue() {
		return currentCreditCorporateValue;
	}

	@JsonProperty("currentCreditCorporateValue")
	public void setCurrentCreditCorporateValue(String currentCreditCorporateValue) {
		this.currentCreditCorporateValue = currentCreditCorporateValue;
	}

	@JsonProperty("currentCreditIndividualValue")
	public String getCurrentCreditIndividualValue() {
		return currentCreditIndividualValue;
	}

	@JsonProperty("currentCreditIndividualValue")
	public void setCurrentCreditIndividualValue(String currentCreditIndividualValue) {
		this.currentCreditIndividualValue = currentCreditIndividualValue;
	}

	@JsonProperty("outstandingIndividualDebt")
	public String getOutstandingIndividualDept() {
		return outstandingIndividualDebt;
	}

	@JsonProperty("outstandingIndividualDebt")
	public void setOutstandingIndividualDept(String outstandingIndividualDept) {
		this.outstandingIndividualDebt = outstandingIndividualDept;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
