package com.evampsaanga.magentoswervices.mysubscriptionofferingids;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.azerfon.suplementryservices.Data;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class MySubscriptionsResponseClient extends BaseResponse {
	@JsonInclude(JsonInclude.Include.ALWAYS)
	com.evampsaanga.azerfon.suplementryservices.Data data = new Data();

	public com.evampsaanga.azerfon.suplementryservices.Data getData() {
		return data;
	}

	public void setData(com.evampsaanga.azerfon.suplementryservices.Data data) {
		this.data = data;
	}
}
