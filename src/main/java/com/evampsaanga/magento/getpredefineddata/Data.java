package com.evampsaanga.magento.getpredefineddata;

public class Data {

	private Topup topup = new Topup();
	private Fnf fnf = new Fnf();
	private Tnc tnc = new Tnc();
	private ExchangeService exchangeService;
	private NarTv narTv;

	public Topup getTopup() {
		return topup;
	}

	public void setTopup(Topup topup) {
		this.topup = topup;
	}

	public Fnf getFnf() {
		return fnf;
	}

	public void setFnf(Fnf fnf) {
		this.fnf = fnf;
	}

	public Tnc getTnc() {
		return tnc;
	}

	public void setTnc(Tnc tnc) {
		this.tnc = tnc;
	}

	public ExchangeService getExchangeService() {
		return exchangeService;
	}

	public void setExchangeService(ExchangeService exchangeService) {
		this.exchangeService = exchangeService;
	}

	public NarTv getNarTv() {
		return narTv;
	}

	public void setNarTv(NarTv narTv) {
		this.narTv = narTv;
	}

	@Override
	public String toString() {
		return "Data [topup=" + topup + ", fnf=" + fnf + ", tnc=" + tnc + ", exchangeService=" + exchangeService
				+ ", narTv=" + narTv + "]";
	}

}
