package com.evampsaanga.magento.getpredefineddata;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetpreDefineDataResponseClient extends BaseResponse {

	com.evampsaanga.magento.getpredefineddata.Data data = new Data();

	/**
	 * @return the data
	 */
	public com.evampsaanga.magento.getpredefineddata.Data getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(com.evampsaanga.magento.getpredefineddata.Data data) {
		this.data = data;
	}
}
