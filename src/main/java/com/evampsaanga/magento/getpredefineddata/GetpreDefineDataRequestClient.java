package com.evampsaanga.magento.getpredefineddata;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class GetpreDefineDataRequestClient extends BaseRequest {
	private String offeringId;

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}
}
