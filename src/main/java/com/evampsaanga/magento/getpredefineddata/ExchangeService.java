package com.evampsaanga.magento.getpredefineddata;

public class ExchangeService {

	private Dov dov;
	private Vod vod;

	public Dov getDov() {
		return dov;
	}

	public void setDov(Dov dov) {
		this.dov = dov;
	}

	public Vod getVod() {
		return vod;
	}

	public void setVod(Vod vod) {
		this.vod = vod;
	}

	@Override
	public String toString() {
		return "ExchangeService [dov=" + dov + ", vod=" + vod + "]";
	}

}
