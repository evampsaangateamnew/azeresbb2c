package com.evampsaanga.magento.tariffdetails;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown=true)
public class TariffDetailsRequest extends BaseRequest {

	String subscribedOfferingName = "";
	String storeId = "";
	String subscriberType = "";
	String customerType = "";
	String offeringId = "";

	/**
	 * @return the subscribedOfferingName
	 */
	public String getSubscribedOfferingName() {
		return subscribedOfferingName;
	}

	/**
	 * @param subscribedOfferingName
	 *            the subscribedOfferingName to set
	 */
	public void setSubscribedOfferingName(String subscribedOfferingName) {
		this.subscribedOfferingName = subscribedOfferingName;
	}

	/**
	 * @return the storeId
	 */
	public String getStoreId() {
		return storeId;
	}

	/**
	 * @param storeId
	 *            the storeId to set
	 */
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	/**
	 * @return the subscriberType
	 */
	public String getSubscriberType() {
		return subscriberType;
	}

	/**
	 * @param subscriberType
	 *            the subscriberType to set
	 */
	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}
}
