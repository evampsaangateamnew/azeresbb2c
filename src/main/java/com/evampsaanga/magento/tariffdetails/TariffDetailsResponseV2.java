package com.evampsaanga.magento.tariffdetails;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class TariffDetailsResponseV2 extends BaseResponse {
	private TarrifDataV2 data;

	public TarrifDataV2 getData() {
		return data;
	}

	public void setData(TarrifDataV2 data) {
		this.data = data;
	}
	
	

}
