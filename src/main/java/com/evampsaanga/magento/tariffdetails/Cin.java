package com.evampsaanga.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cin {
	@JsonProperty
	private String subscribable = "";
	@JsonProperty
	private Details details;
	@JsonProperty
	private Description description;
	@JsonProperty
	private CinHeader header;

	public CinHeader getHeader() {
		return header;
	}

	public void setHeader(CinHeader header) {
		this.header = header;
	}

	public String getSubscribable() {
		return subscribable;
	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}
}
