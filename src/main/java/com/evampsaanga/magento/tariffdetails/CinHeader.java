package com.evampsaanga.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CinHeader {
	private String id;
	private String offeringId = "";
	private String shortDescription = "";
	@JsonProperty
	private KlassPackagePricesCall Call;
	private String tag = "";
	private String name = "";
	private String tagIcon = "";
	private String mrcLabel = "";
	private String offeringName = "";
	@JsonProperty
	private SMS SMS;
	private String mrcValue = "";
	@JsonProperty
	private Internet Internet;
	private String currency = "";
	
	private String type; 
	private String tariffType;
	
	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private Integer sortOrder;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTagIcon() {
		return tagIcon;
	}

	public void setTagIcon(String tagIcon) {
		this.tagIcon = tagIcon;
	}

	public String getMrcLabel() {
		return mrcLabel;
	}

	public void setMrcLabel(String mrcLabel) {
		this.mrcLabel = mrcLabel;
	}

	public String getOfferingName() {
		return offeringName;
	}

	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	public String getMrcValue() {
		return mrcValue;
	}

	public void setMrcValue(String mrcValue) {
		this.mrcValue = mrcValue;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	
	
}