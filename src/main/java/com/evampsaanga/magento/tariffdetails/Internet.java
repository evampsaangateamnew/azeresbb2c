package com.evampsaanga.magento.tariffdetails;

public class Internet {
	private String internetDownloadAndUploadLabel = "";
	private String internetValue = "";
	private String internetLabel = "";
	private String internetDownloadAndUploadValue = "";
	private String internetIcon = "";

	public String getInternetDownloadAndUploadLabel() {
		return internetDownloadAndUploadLabel;
	}

	public void setInternetDownloadAndUploadLabel(String internetDownloadAndUploadLabel) {
		this.internetDownloadAndUploadLabel = internetDownloadAndUploadLabel;
	}

	public String getInternetValue() {
		return internetValue;
	}

	public void setInternetValue(String internetValue) {
		this.internetValue = internetValue;
	}

	public String getInternetLabel() {
		return internetLabel;
	}

	public void setInternetLabel(String internetLabel) {
		this.internetLabel = internetLabel;
	}

	public String getInternetDownloadAndUploadValue() {
		return internetDownloadAndUploadValue;
	}

	public void setInternetDownloadAndUploadValue(String internetDownloadAndUploadValue) {
		this.internetDownloadAndUploadValue = internetDownloadAndUploadValue;
	}

	public String getInternetIcon() {
		return internetIcon;
	}

	public void setInternetIcon(String internetIcon) {
		this.internetIcon = internetIcon;
	}

	@Override
	public String toString() {
		return "ClassPojo [internetDownloadAndUploadLabel = " + internetDownloadAndUploadLabel + ", internetValue = "
				+ internetValue + ", internetLabel = " + internetLabel + ", internetDownloadAndUploadValue = "
				+ internetDownloadAndUploadValue + ", internetIcon = " + internetIcon + "]";
	}
}