package com.evampsaanga.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KlassPostpaid {
	private String subscribable;
	private Details details;
	private PackagePrices packagePrices;
	@JsonProperty
	private KlassHeader header;
	
	private String type; 
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public String getSubscribable() {
		return subscribable;
	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public PackagePrices getPackagePrices() {
		return packagePrices;
	}

	public void setPackagePrices(PackagePrices packagePrices) {
		this.packagePrices = packagePrices;
	}

	@Override
	public String toString() {
		return "ClassPojo [subscribable = " + subscribable + ", details = " + details + ", packagePrices = "
				+ packagePrices + ", header = " + header + "]";
	}
}
