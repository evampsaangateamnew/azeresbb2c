package com.evampsaanga.magento.tariffdetails;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class TariffDetailsResponse extends BaseResponse {
	private com.evampsaanga.magento.tariffdetails.Data data = new Data();

	public com.evampsaanga.magento.tariffdetails.Data getData() {
		return data;
	}

	public void setData(com.evampsaanga.magento.tariffdetails.Data data) {
		this.data = data;
	}
}
