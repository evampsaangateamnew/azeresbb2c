package com.evampsaanga.magento.tariffdetails;

public class Time {
	private String toTimeValue = "";
	private String fromTimeValue = "";
	private String fromTimeLabel = "";
	private String toTimeLabel = "";
	private String timeDescription = "";

	public String getToTimeValue() {
		return toTimeValue;
	}

	public void setToTimeValue(String toTimeValue) {
		this.toTimeValue = toTimeValue;
	}

	public String getFromTimeValue() {
		return fromTimeValue;
	}

	public void setFromTimeValue(String fromTimeValue) {
		this.fromTimeValue = fromTimeValue;
	}

	public String getFromTimeLabel() {
		return fromTimeLabel;
	}

	public void setFromTimeLabel(String fromTimeLabel) {
		this.fromTimeLabel = fromTimeLabel;
	}

	public String getToTimeLabel() {
		return toTimeLabel;
	}

	public void setToTimeLabel(String toTimeLabel) {
		this.toTimeLabel = toTimeLabel;
	}

	public String getTimeDescription() {
		return timeDescription;
	}

	public void setTimeDescription(String timeDescription) {
		this.timeDescription = timeDescription;
	}

	@Override
	public String toString() {
		return "ClassPojo [toTimeValue = " + toTimeValue + ", fromTimeValue = " + fromTimeValue + ", fromTimeLabel = "
				+ fromTimeLabel + ", toTimeLabel = " + toTimeLabel + ", timeDescription = " + timeDescription + "]";
	}
}