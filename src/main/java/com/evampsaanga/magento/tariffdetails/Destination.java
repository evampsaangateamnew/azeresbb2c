package com.evampsaanga.magento.tariffdetails;

public class Destination {
	private String destinationOffnetValue = "";
	private String destinationInternetLabel = "";
	private String shortDescription = "";
	private String destinationValue = "";
	private String destinationInternationalValue = "";
	private String destinationInternationalLabel = "";
	private String destinationOnnetLabel = "";
	private String destinationOffnetLabel = "";
	private String destinationInternetValue = "";
	private String destinationLabel = "";
	private String destinationOnnetValue = "";
	private String destinationIcon = "";

	public String getDestinationOffnetValue() {
		return destinationOffnetValue;
	}

	public void setDestinationOffnetValue(String destinationOffnetValue) {
		this.destinationOffnetValue = destinationOffnetValue;
	}

	public String getDestinationInternetLabel() {
		return destinationInternetLabel;
	}

	public void setDestinationInternetLabel(String destinationInternetLabel) {
		this.destinationInternetLabel = destinationInternetLabel;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getDestinationValue() {
		return destinationValue;
	}

	public void setDestinationValue(String destinationValue) {
		this.destinationValue = destinationValue;
	}

	public String getDestinationInternationalValue() {
		return destinationInternationalValue;
	}

	public void setDestinationInternationalValue(String destinationInternationalValue) {
		this.destinationInternationalValue = destinationInternationalValue;
	}

	public String getDestinationInternationalLabel() {
		return destinationInternationalLabel;
	}

	public void setDestinationInternationalLabel(String destinationInternationalLabel) {
		this.destinationInternationalLabel = destinationInternationalLabel;
	}

	public String getDestinationOnnetLabel() {
		return destinationOnnetLabel;
	}

	public void setDestinationOnnetLabel(String destinationOnnetLabel) {
		this.destinationOnnetLabel = destinationOnnetLabel;
	}

	public String getDestinationOffnetLabel() {
		return destinationOffnetLabel;
	}

	public void setDestinationOffnetLabel(String destinationOffnetLabel) {
		this.destinationOffnetLabel = destinationOffnetLabel;
	}

	public String getDestinationInternetValue() {
		return destinationInternetValue;
	}

	public void setDestinationInternetValue(String destinationInternetValue) {
		this.destinationInternetValue = destinationInternetValue;
	}

	public String getDestinationLabel() {
		return destinationLabel;
	}

	public void setDestinationLabel(String destinationLabel) {
		this.destinationLabel = destinationLabel;
	}

	public String getDestinationOnnetValue() {
		return destinationOnnetValue;
	}

	public void setDestinationOnnetValue(String destinationOnnetValue) {
		this.destinationOnnetValue = destinationOnnetValue;
	}

	public String getDestinationIcon() {
		return destinationIcon;
	}

	public void setDestinationIcon(String destinationIcon) {
		this.destinationIcon = destinationIcon;
	}

	@Override
	public String toString() {
		return "ClassPojo [destinationOffnetValue = " + destinationOffnetValue + ", destinationInternetLabel = "
				+ destinationInternetLabel + ", shortDescription = " + shortDescription + ", destinationValue = "
				+ destinationValue + ", destinationInternationalValue = " + destinationInternationalValue
				+ ", destinationInternationalLabel = " + destinationInternationalLabel + ", destinationOnnetLabel = "
				+ destinationOnnetLabel + ", destinationOffnetLabel = " + destinationOffnetLabel
				+ ", destinationInternetValue = " + destinationInternetValue + ", destinationLabel = "
				+ destinationLabel + ", destinationOnnetValue = " + destinationOnnetValue + ", destinationIcon = "
				+ destinationIcon + "]";
	}
}