package com.evampsaanga.magento.tariffdetails;

public class PostpaidBusinessIndividualHeader {
	private String id = "";
	private String offeringName = "";
	private String offeringId = "";
	private String tagIcon = "";
	private String tag = "";
	private String currency = "";
	private String name = "";
	private String mrcLabel = "";
	private String mrcValue = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferingName() {
		return offeringName;
	}

	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getTagIcon() {
		return tagIcon;
	}

	public void setTagIcon(String tagIcon) {
		this.tagIcon = tagIcon;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMrcLabel() {
		return mrcLabel;
	}

	public void setMrcLabel(String mrcLabel) {
		this.mrcLabel = mrcLabel;
	}

	public String getMrcValue() {
		return mrcValue;
	}

	public void setMrcValue(String mrcValue) {
		this.mrcValue = mrcValue;
	}
}
