package com.evampsaanga.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KlassHeader {
	@JsonProperty("Call")
	private KlassHeaderCall Call;
	@JsonProperty("SMS")
	private KlassHeaderSMS SMS;
	@JsonProperty("Internet")
	private KlassHeaderInternet Internet;
	@JsonProperty("countryWide")
	private CountryWide countryWide;
	@JsonProperty("Whatsapp")
	private Whatsapp Whatsapp;
	@JsonProperty("bonusSix")
	private BonusSix bonusSix;
	private String offeringName = "";
	@JsonProperty("bonusSeven")
	private BonusSeven bonusSeven;
	@JsonProperty("bonusEight")
	private BonusEight bonusEight;
	private String tag = "";
	private String tagIcon = "";
	private String mrcLabel = "";
	private String currency = "";
	private String id = "";
	private String offeringId = "";
	private String name = "";
	private String mrcValue = "";
	private Integer sortOrder;
	
	private String type ;
	
	private String tariffType;
	
	
	
	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BonusEight getBonusEight() {
		return bonusEight;
	}

	public void setBonusEight(BonusEight bonusEight) {
		this.bonusEight = bonusEight;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public CountryWide getCountryWide() {
		return countryWide;
	}

	public void setCountryWide(CountryWide countryWide) {
		this.countryWide = countryWide;
	}

	public String getTagIcon() {
		return tagIcon;
	}

	public void setTagIcon(String tagIcon) {
		this.tagIcon = tagIcon;
	}

	public String getMrcLabel() {
		return mrcLabel;
	}

	public void setMrcLabel(String mrcLabel) {
		this.mrcLabel = mrcLabel;
	}

	public BonusSix getBonusSix() {
		return bonusSix;
	}

	public void setBonusSix(BonusSix bonusSix) {
		this.bonusSix = bonusSix;
	}

	public String getOfferingName() {
		return offeringName;
	}

	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BonusSeven getBonusSeven() {
		return bonusSeven;
	}

	public void setBonusSeven(BonusSeven bonusSeven) {
		this.bonusSeven = bonusSeven;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMrcValue() {
		return mrcValue;
	}

	public void setMrcValue(String mrcValue) {
		this.mrcValue = mrcValue;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	
}