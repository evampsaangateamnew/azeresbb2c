package com.evampsaanga.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TarrifDataV2 {
	@JsonProperty("Postpaid")
	private Postpaid Postpaid = null;
	@JsonProperty("Prepaid")
	private Prepaid Prepaid = null;
	public Postpaid getPostpaid() {
		return Postpaid;
	}
	public void setPostpaid(Postpaid postpaid) {
		Postpaid = postpaid;
	}
	public Prepaid getPrepaid() {
		return Prepaid;
	}
	public void setPrepaid(Prepaid prepaid) {
		Prepaid = prepaid;
	}
	
	
}
