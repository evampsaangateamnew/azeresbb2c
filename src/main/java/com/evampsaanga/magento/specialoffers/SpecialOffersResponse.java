package com.evampsaanga.magento.specialoffers;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class SpecialOffersResponse extends BaseResponse {
	
	
	List<Datum> data=new ArrayList<>();

	public List<Datum> getData() {
		return data;
	}

	public void setData(List<Datum> data) {
		this.data = data;
	}
	

}
