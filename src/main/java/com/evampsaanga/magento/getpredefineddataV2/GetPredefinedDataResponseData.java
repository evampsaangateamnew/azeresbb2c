package com.evampsaanga.magento.getpredefineddataV2;

public class GetPredefinedDataResponseData {

	private String content;
	private String firstPopup;
	private String lateOnPopup;
	private String popupTitle;
	private String popupContent;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFirstPopup() {
		return firstPopup;
	}

	public void setFirstPopup(String firstPopup) {
		this.firstPopup = firstPopup;
	}

	public String getLateOnPopup() {
		return lateOnPopup;
	}

	public void setLateOnPopup(String lateOnPopup) {
		this.lateOnPopup = lateOnPopup;
	}

	public String getPopupTitle() {
		return popupTitle;
	}

	public void setPopupTitle(String popupTitle) {
		this.popupTitle = popupTitle;
	}

	public String getPopupContent() {
		return popupContent;
	}

	public void setPopupContent(String popupContent) {
		this.popupContent = popupContent;
	}

}
