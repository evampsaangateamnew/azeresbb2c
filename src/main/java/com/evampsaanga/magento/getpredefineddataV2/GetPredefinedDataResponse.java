package com.evampsaanga.magento.getpredefineddataV2;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetPredefinedDataResponse extends BaseResponse {
	private GetPredefinedDataResponseData data;

	public GetPredefinedDataResponseData getData() {
		return data;
	}

	public void setData(GetPredefinedDataResponseData data) {
		this.data = data;
	}

}
