
package com.evampsaanga.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "titleLabel",
    "subtitle1Label",
    "subtitle1Value",
    "subtitle2Label",
    "subtitle2Value",
    "subtitle3Label",
    "subtitle3Value",
    "subtitle4Label",
    "subtitle4Value",
    "shortDescription"
})
public class TitleSubtitle {

    @JsonProperty("titleLabel")
    private String titleLabel;
    @JsonProperty("subtitle1Label")
    private String subtitle1Label;
    @JsonProperty("subtitle1Value")
    private String subtitle1Value;
    @JsonProperty("subtitle2Label")
    private String subtitle2Label;
    @JsonProperty("subtitle2Value")
    private String subtitle2Value;
    @JsonProperty("subtitle3Label")
    private String subtitle3Label;
    @JsonProperty("subtitle3Value")
    private String subtitle3Value;
    @JsonProperty("subtitle4Label")
    private String subtitle4Label;
    @JsonProperty("subtitle4Value")
    private String subtitle4Value;
    @JsonProperty("shortDescription")
    private String shortDescription;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("titleLabel")
    public String getTitleLabel() {
        return titleLabel;
    }

    @JsonProperty("titleLabel")
    public void setTitleLabel(String titleLabel) {
        this.titleLabel = titleLabel;
    }

    @JsonProperty("subtitle1Label")
    public String getSubtitle1Label() {
        return subtitle1Label;
    }

    @JsonProperty("subtitle1Label")
    public void setSubtitle1Label(String subtitle1Label) {
        this.subtitle1Label = subtitle1Label;
    }

    @JsonProperty("subtitle1Value")
    public String getSubtitle1Value() {
        return subtitle1Value;
    }

    @JsonProperty("subtitle1Value")
    public void setSubtitle1Value(String subtitle1Value) {
        this.subtitle1Value = subtitle1Value;
    }

    @JsonProperty("subtitle2Label")
    public String getSubtitle2Label() {
        return subtitle2Label;
    }

    @JsonProperty("subtitle2Label")
    public void setSubtitle2Label(String subtitle2Label) {
        this.subtitle2Label = subtitle2Label;
    }

    @JsonProperty("subtitle2Value")
    public String getSubtitle2Value() {
        return subtitle2Value;
    }

    @JsonProperty("subtitle2Value")
    public void setSubtitle2Value(String subtitle2Value) {
        this.subtitle2Value = subtitle2Value;
    }

    @JsonProperty("subtitle3Label")
    public String getSubtitle3Label() {
        return subtitle3Label;
    }

    @JsonProperty("subtitle3Label")
    public void setSubtitle3Label(String subtitle3Label) {
        this.subtitle3Label = subtitle3Label;
    }

    @JsonProperty("subtitle3Value")
    public String getSubtitle3Value() {
        return subtitle3Value;
    }

    @JsonProperty("subtitle3Value")
    public void setSubtitle3Value(String subtitle3Value) {
        this.subtitle3Value = subtitle3Value;
    }

    @JsonProperty("subtitle4Label")
    public String getSubtitle4Label() {
        return subtitle4Label;
    }

    @JsonProperty("subtitle4Label")
    public void setSubtitle4Label(String subtitle4Label) {
        this.subtitle4Label = subtitle4Label;
    }

    @JsonProperty("subtitle4Value")
    public String getSubtitle4Value() {
        return subtitle4Value;
    }

    @JsonProperty("subtitle4Value")
    public void setSubtitle4Value(String subtitle4Value) {
        this.subtitle4Value = subtitle4Value;
    }

    @JsonProperty("shortDescription")
    public String getShortDescription() {
        return shortDescription;
    }

    @JsonProperty("shortDescription")
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
