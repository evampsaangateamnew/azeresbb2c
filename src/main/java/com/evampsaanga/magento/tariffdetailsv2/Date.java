
package com.evampsaanga.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dateDescription",
    "fromDateLabel",
    "fromDateValue",
    "toDateLabel",
    "toDateValue"
})
public class Date {

    @JsonProperty("dateDescription")
    private String dateDescription;
    @JsonProperty("fromDateLabel")
    private String fromDateLabel;
    @JsonProperty("fromDateValue")
    private String fromDateValue;
    @JsonProperty("toDateLabel")
    private String toDateLabel;
    @JsonProperty("toDateValue")
    private String toDateValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("dateDescription")
    public String getDateDescription() {
        return dateDescription;
    }

    @JsonProperty("dateDescription")
    public void setDateDescription(String dateDescription) {
        this.dateDescription = dateDescription;
    }

    @JsonProperty("fromDateLabel")
    public String getFromDateLabel() {
        return fromDateLabel;
    }

    @JsonProperty("fromDateLabel")
    public void setFromDateLabel(String fromDateLabel) {
        this.fromDateLabel = fromDateLabel;
    }

    @JsonProperty("fromDateValue")
    public String getFromDateValue() {
        return fromDateValue;
    }

    @JsonProperty("fromDateValue")
    public void setFromDateValue(String fromDateValue) {
        this.fromDateValue = fromDateValue;
    }

    @JsonProperty("toDateLabel")
    public String getToDateLabel() {
        return toDateLabel;
    }

    @JsonProperty("toDateLabel")
    public void setToDateLabel(String toDateLabel) {
        this.toDateLabel = toDateLabel;
    }

    @JsonProperty("toDateValue")
    public String getToDateValue() {
        return toDateValue;
    }

    @JsonProperty("toDateValue")
    public void setToDateValue(String toDateValue) {
        this.toDateValue = toDateValue;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
