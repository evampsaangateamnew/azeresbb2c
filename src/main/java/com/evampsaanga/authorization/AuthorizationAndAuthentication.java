package com.evampsaanga.authorization;

import org.apache.log4j.Logger;

import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;

public class AuthorizationAndAuthentication {

	public static final Logger logger = Logger.getLogger("azerfon-esb");
	
	public static boolean authenticateAndAuthorizeCredentials(String credentials)
	{
		try {
			credentials = Decrypter.getInstance().decrypt(credentials);
			if (credentials == null || !credentials.equals(Constants.CREDENTIALS)) 
				return false;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		return true;
	}
	
}
