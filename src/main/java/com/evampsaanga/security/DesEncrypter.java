package com.evampsaanga.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import org.apache.log4j.Logger;
import com.evampsaanga.developer.utils.Helper;

public class DesEncrypter {
	Cipher ecipher;
	Cipher dcipher;

	public DesEncrypter() throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException,
			InvalidKeySpecException, NoSuchPaddingException {
		// String keyString="1234567890111110";
		String keyString = "euldlmdc";
		SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
		byte[] utf8 = keyString.getBytes("UTF8");
		DESKeySpec dks = new DESKeySpec(utf8);
		SecretKey key = skf.generateSecret(dks);
		ecipher = Cipher.getInstance("DES");
		dcipher = Cipher.getInstance("DES");
		ecipher.init(Cipher.ENCRYPT_MODE, key);
		dcipher.init(Cipher.DECRYPT_MODE, key);
	}

	@SuppressWarnings("restriction")
	public String encrypt(String str) {
		try {
			// Encode the string into bytes using utf-8
			byte[] utf8 = str.getBytes("UTF8");
			// Encrypt
			byte[] enc = ecipher.doFinal(utf8);
			// Encode bytes to base64 to get a string
			return new sun.misc.BASE64Encoder().encode(enc);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		return null;
	}

	@SuppressWarnings("restriction")
	public String decrypt(String str) {
		try {
			// Decode base64 to get bytes
			byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
			// Decrypt
			byte[] utf8 = dcipher.doFinal(dec);
			// Decode using utf-8
			return new String(utf8, "UTF8");
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		return null;
	}

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	/*
	 * private static void saveKeyToFile(SecretKey key) throws
	 * FileNotFoundException, IOException { try { ObjectOutputStream oos = new
	 * ObjectOutputStream(new FileOutputStream("key.txt"));
	 * oos.writeObject(key); oos.close(); } catch (Exception e) {
	 * 
	 * 
	 * } }
	 */
	public static String encodeString(String data) {
		try {
			DesEncrypter encrypter = new DesEncrypter();
			return encrypter.encrypt(data);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
			return "";
		}
	}

	public static String decodeString(String data) {
		try {
			DesEncrypter encrypter = new DesEncrypter();
			return encrypter.decrypt(data);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
			e.printStackTrace();
			return "";
		}
	}
}