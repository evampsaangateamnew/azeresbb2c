package com.evampsaanga.azerfon.getUsersV2;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.cache.UserGroupDataCache;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.validator.rules.ChannelNotEmpty;
import com.evampsaanga.validator.rules.IPNotEmpty;
import com.evampsaanga.validator.rules.LangNotEmpty;
import com.evampsaanga.validator.rules.MSISDNNotEmpty;
import com.evampsaanga.validator.rules.ValidationResult;

/**
 * Generates user group data and user data
 * @author Aqeel Abbas 
 *
 */
@Path("/bakcell")
public class GetUsersGroupInfoService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	/**
	 * 
	 * @param requestBody
	 * @param credential
	 * @return user groups
	 * @throws SQLException
	 * @throws InterruptedException
	 */
	@POST
	@Path("/getusers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UsersGroupResponse getUsersGroupData(@Body String requestBody, @Header("credentials") String credential)
			throws SQLException, InterruptedException {

		// Logging incoming request to log file
		Helper.logInfoMessageV2("Request lanaded on GetUsersGroup with data as :" + requestBody);
		Helper.logInfoMessageV2("--------------Test data as--------------");

		// Below is declaration block for variables to be used
		// Logs object to store values which are to be inserted in database for
		// reporting
		Logs logs = new Logs();

		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.USERS_GROUP_DATA);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.CustomerData);

		// Request object to store parsed data from requested string
		UsersGroupRequest usersGroupRequest = null;
		// response object which is to be returned to user
		ArrayList<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
		UsersGroupResponse usersgroupResponse = new UsersGroupResponse();
		Map<String, UsersGroupData> usersMap = new HashMap<>();
		// Authenticating the request using credentials string received in
		// header
		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		try {
			usersGroupRequest = Helper.JsonToObject(requestBody, UsersGroupRequest.class);
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			logger.error(Helper.GetException(ex));
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400);
		}

		// if requested string is converted to Java class then processing of
		// request goes below
		if (usersGroupRequest != null) {
			// block sets the mandatory parameters to logs object which are
			// taken from request body
			
			logs.setIp(usersGroupRequest.getiP());
			logs.setChannel(usersGroupRequest.getChannel());
			logs.setMsisdn(usersGroupRequest.getmsisdn());

			// if authentication is successful request forwarded for
			// validation of parameters
			if (authenticationresult) {
				ValidationResult validationResult = validateRequest(usersGroupRequest);

				if (validationResult.isValidationResult()) {
					logs.setResponseCode(usersgroupResponse.getReturnCode());
					logs.setResponseDescription(usersgroupResponse.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
				} else
					// if request parameters fails validation response is
					// prepared with validation error
					prepareErrorResponse(usersgroupResponse, logs, validationResult.getValidationCode(),
							validationResult.getValidationMessage());
			}
		}
		// getting the data from DB view
		UserGroupDataCache userCache = new UserGroupDataCache();

		try {
			usersGroupData = (ArrayList<UsersGroupData>) userCache.checkUsersInCache(requestBody);
			if (usersGroupData == null || usersGroupData.isEmpty()) {
				Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - No Users Found");
				prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

			} else {
				HashMap<String, UsersGroupResponseData> map = new HashMap<String, UsersGroupResponseData>();
				ArrayList<UsersGroupData> usersData = new ArrayList<UsersGroupData>();

				for (int i = 0; i < usersGroupData.size(); i++) {
					//String groupID = usersGroupData.get(i).getGroup_cust_name();
					String groupID = usersGroupData.get(i).getGroup_name();
					String groupName = usersGroupData.get(i).getGroup_name_disp();
					usersMap.put(usersGroupData.get(i).getMsisdn(), usersGroupData.get(i));
					if (map.containsKey(groupID)) {
						
						((UsersGroupResponseData) map.get(groupID)).getUsersGroupData()
								.add((UsersGroupData) usersGroupData.get(i));
						((UsersGroupResponseData) map.get(groupID))
								.setUserCount(((UsersGroupResponseData) map.get(groupID)).getUsersGroupData().size());
						// map.get(groupID).getUsersGroupData().add(usersGroupData.get(i));
					} else {
						UsersGroupResponseData data = new UsersGroupResponseData();
						usersData = new ArrayList<UsersGroupData>();
						usersData.add(usersGroupData.get(i));
						data.setUsersGroupData(usersData);
						data.setGroupName(groupName);
						data.setUserCount(data.getUsersGroupData().size());
						map.put(groupID, data);
					}
				}

				/*
				 * Iterator it = map.entrySet().iterator(); while (it.hasNext())
				 * { Map.Entry pair = (Map.Entry) it.next();
				 * ((UsersGroupResponseData) pair.getValue())
				 * .setUserCount(((UsersGroupResponseData)
				 * pair.getValue()).getUsersGroupData().size()); }
				 */
				usersgroupResponse.setGroupData(map);
				usersgroupResponse.setUsers(usersMap);
				// usersgroupResponse.setUsers(usersGroupData);
				logger.info("sizeCount&GroupData :"+usersGroupData.size());
				Helper.logInfoMessageV2("sizeCount&GroupData :"+usersGroupData.size());
				usersgroupResponse.setUserCount(usersGroupData.size());
				usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				usersgroupResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			}
		} catch (Exception e1) {
			logger.error(Helper.GetException(e1));
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		}
		Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - getusers End point reached");
		return usersgroupResponse;

	}

	
	public String getUsersCount(String requestBody) throws JSONException{
		UserGroupDataCache userCache = new UserGroupDataCache();
		JSONObject jobj = new JSONObject(requestBody);
		String usersCount = userCache.countUsers(jobj.getString("customerID"));
		logger.info("UserCont-- "+usersCount);
		return usersCount;
		
	}
	
	/**
	 * Validates the request
	 * @param client
	 * @return validation response
	 */
	private ValidationResult validateRequest(UsersGroupRequest client) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValidationResult(true);
		// Validating all the fields against non empty rule
		validationResult = new MSISDNNotEmpty().validateObject(client.getmsisdn());
		validationResult = new ChannelNotEmpty().validateObject(client.getChannel());
		validationResult = new IPNotEmpty().validateObject(client.getiP());
		validationResult = new LangNotEmpty().validateObject(client.getLang());
		return validationResult;
	}

	/**
	 * prepares error response and update logs in queue
	 * 
	 * @param usersGroupDataResponse
	 * @param logs
	 * @param returnCode
	 * @param returnMessage
	 */
	public void prepareErrorResponse(UsersGroupResponse usersGroupDataResponse, Logs logs, String returnCode,
			String returnMessage) {
		usersGroupDataResponse.setReturnCode(returnCode);
		usersGroupDataResponse.setReturnMsg(returnMessage);
		logs.setResponseCode(usersGroupDataResponse.getReturnCode());
		logs.setResponseDescription(usersGroupDataResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
	}
}
