package com.evampsaanga.azerfon.getUsersV2;

import java.util.HashMap;
import java.util.Map;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
/**
 * Response container for user group data
 * @author Aqeel Abbas
 *
 */
public class UsersGroupResponse extends BaseResponse {

	int userCount;
	private HashMap<String, UsersGroupResponseData> groupData;
	private Map<String, UsersGroupData> users;


	public Map<String, UsersGroupData> getUsers() {
		return users;
	}

	public void setUsers(Map<String, UsersGroupData> users) {
		this.users = users;
	}

	public int getUserCount() {
		return userCount;
	}

	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}

	public HashMap<String, UsersGroupResponseData> getGroupData() {
		return groupData;
	}

	public void setGroupData(HashMap<String, UsersGroupResponseData> groupData) {
		this.groupData = groupData;
	}

}
