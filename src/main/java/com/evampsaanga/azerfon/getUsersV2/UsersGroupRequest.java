package com.evampsaanga.azerfon.getUsersV2;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

/**
 * Request Container For User and Group Data info for Phase 2
 * @author Aqeel Abbas
 *
 */
public class UsersGroupRequest extends BaseRequest {
	private String customerID;
	private String acctCode;

	public UsersGroupRequest(String channel, String lang, String msisdn, String iP) {
		super(channel, lang, msisdn, iP);
		// TODO Auto-generated constructor stub
	}
	public UsersGroupRequest(){}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getAcctCode() {
		return acctCode;
	}
	public void setAcctCode(String acctCode) {
		this.acctCode = acctCode;
	}

}
