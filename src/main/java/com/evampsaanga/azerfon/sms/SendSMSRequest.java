package com.evampsaanga.azerfon.sms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.evampsaanga.azerfon.requestheaders.BaseRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SendSMSRequest")
public class SendSMSRequest extends BaseRequest {
	@XmlElement(name = "textmsg", required = true)
	private String textmsg = "";
	@XmlElement(name = "sender", required = false)
	private String sender = "";

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * @return the textmsg
	 */
	public String getTextmsg() {
		return textmsg;
	}

	/**
	 * @param textmsg
	 *            the textmsg to set
	 */
	public void setTextmsg(String textmsg) {
		this.textmsg = textmsg;
	}
}
