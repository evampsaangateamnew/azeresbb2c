package com.evampsaanga.azerfon.getcustomerrequest;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetCustomerRequestResponse extends BaseResponse {
	private com.evampsaanga.azerfon.getcustomerrequest.CustomerData customerData = new CustomerData();

	/**
	 * @return the customerData
	 */
	public com.evampsaanga.azerfon.getcustomerrequest.CustomerData getCustomerData() {
		return customerData;
	}

	/**
	 * @param customerData
	 *            the customerData to set
	 */
	public void setCustomerData(com.evampsaanga.azerfon.getcustomerrequest.CustomerData customerData) {
		this.customerData = customerData;
	}
}
