package com.evampsaanga.azerfon.verifycdrsotp;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class VerifyCDRsOTPRequest extends BaseRequest {
	private String pin = "";
	private String accountId = "";
	private String customerId = "";

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
}
