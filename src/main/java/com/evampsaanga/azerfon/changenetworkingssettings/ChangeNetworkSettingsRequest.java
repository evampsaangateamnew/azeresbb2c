package com.evampsaanga.azerfon.changenetworkingssettings;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class ChangeNetworkSettingsRequest extends BaseRequest {
	private String productId="";
	private String selectFlag="";
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getSelectFlag() {
		return selectFlag;
	}
	public void setSelectFlag(String selectFlag) {
		this.selectFlag = selectFlag;
	}

}
