package com.evampsaanga.azerfon.survey;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetSurveyCountResponse extends BaseResponse {
	private String surveyCount;

	public String getSurveyCount() {
		return surveyCount;
	}

	public void setSurveyCount(String surveyCount) {
		this.surveyCount = surveyCount;
	}

	@Override
	public String toString() {
		return "GetSurveyCountResponse [surveyCount=" + surveyCount + ", toString()=" + super.toString() + "]";
	}

}
