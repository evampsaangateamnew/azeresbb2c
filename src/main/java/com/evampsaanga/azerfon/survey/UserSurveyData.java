package com.evampsaanga.azerfon.survey;

public class UserSurveyData {
	private String answer;
	private String offeringId;
	private String offeringType;
	private String comment;
	private String answerId;
	private String questionId;
	private String surveyId;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getOfferingType() {
		return offeringType;
	}

	public void setOfferingType(String offeringType) {
		this.offeringType = offeringType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getAnswerId() {
		return answerId;
	}

	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	@Override
	public String toString() {
		return "UserSurveyData [answer=" + answer + ", offeringId=" + offeringId + ", offeringType=" + offeringType
				+ ", comment=" + comment + ", answerId=" + answerId + ", questionId=" + questionId + ", surveyId="
				+ surveyId + "]";
	}

}
