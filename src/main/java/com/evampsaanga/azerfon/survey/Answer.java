package com.evampsaanga.azerfon.survey;

public class Answer {
	private String id;
	private String answerTextEN;
	private String answerTextAZ;
	private String answerTextRU;

	public Answer(String id, String answerTextEN, String answerTextAZ, String answerTextRU) {
		super();
		this.id = id;
		this.answerTextEN = answerTextEN;
		this.answerTextAZ = answerTextAZ;
		this.answerTextRU = answerTextRU;
	}

	public Answer() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAnswerTextEN() {
		return answerTextEN;
	}

	public void setAnswerTextEN(String answerTextEN) {
		this.answerTextEN = answerTextEN;
	}

	public String getAnswerTextAZ() {
		return answerTextAZ;
	}

	public void setAnswerTextAZ(String answerTextAZ) {
		this.answerTextAZ = answerTextAZ;
	}

	public String getAnswerTextRU() {
		return answerTextRU;
	}

	public void setAnswerTextRU(String answerTextRU) {
		this.answerTextRU = answerTextRU;
	}

	@Override
	public String toString() {
		return "Answer [id=" + id + ", answerTextEN=" + answerTextEN + ", answerTextAZ=" + answerTextAZ
				+ ", answerTextRU=" + answerTextRU + "]";
	}

}