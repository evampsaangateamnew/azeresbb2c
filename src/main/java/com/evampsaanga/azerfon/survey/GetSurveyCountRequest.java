package com.evampsaanga.azerfon.survey;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class GetSurveyCountRequest extends BaseRequest {
	private String surveyId;
	private String offeringId;

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "GetSurveyCountRequest [surveyId=" + surveyId + ", offeringId=" + offeringId + ", toString()="
				+ super.toString() + "]";
	}

}
