package com.evampsaanga.azerfon.survey;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.developer.utils.Helper;

public class SurveyDAO {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public static boolean saveSurveyDataToDB(SaveSurveyRequest cclient) {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		Connection conn = null;
		try {
			conn = DBFactory.getMagentoDBConnection();
			String query = "insert into user_survey_data (msisdn, comment, answer_id, question_id, survey_id, offering_id, offering_type, recordTimeStamp)"
					+ " values (?, ?, ?, ?, ?, ?, ?, ?)";

			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preparedStmt.setString(1, cclient.getmsisdn());
			preparedStmt.setString(2, cclient.getComment());
			preparedStmt.setString(3, cclient.getAnswerId());
			preparedStmt.setString(4, cclient.getQuestionId());
			preparedStmt.setString(5, cclient.getSurveyId());
			preparedStmt.setString(6, cclient.getOfferingIdSurvey());
			preparedStmt.setString(7, cclient.getOfferingTypeSurvey());
			preparedStmt.setString(8, Helper.getCurrentTimeStamp());
			logger.info(cclient.getmsisdn() + "-User Survey Data insertion query-" + query);
			preparedStmt.executeUpdate();

			resultSet = preparedStmt.getGeneratedKeys();
			if (resultSet.next()) {
				logger.info(cclient.getmsisdn() + "-generate update : " + resultSet.getInt(1));
			}
			return true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStmt != null)
					preparedStmt.close();
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return false;
	}

	public static List<Survey> getSurveysFromDB(String msisdn) {
		logger.info(msisdn + "- Getting survey data from database");
		List<Survey> surveys = new ArrayList<>();
		String query = "SELECT * FROM survey where enabled = 1";
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		java.sql.Connection conn = null;
		try {
			conn = DBFactory.getMagentoDBConnection();
			stmt = conn.prepareStatement(query);
			logger.info(msisdn + "-Get Surveys query-" + query);
			resultSet = stmt.executeQuery(query);

			while (resultSet.next()) {
				Survey survey = new Survey();
				survey.setId(resultSet.getString("id"));
				survey.setTitleEn(resultSet.getString("title_en"));
				survey.setTitleAz(resultSet.getString("title_az"));
				survey.setTitleRu(resultSet.getString("title_ru"));
				survey.setCommentEnable(resultSet.getString("comments_enabled"));
				survey.setCommentsTitleEn(resultSet.getString("comments_title_en"));
				survey.setCommentsTitleAz(resultSet.getString("comments_title_az"));
				survey.setCommentsTitleRu(resultSet.getString("comments_title_ru"));
				survey.setScreenName(resultSet.getString("screen_name"));
				survey.setTimeLimit(resultSet.getString("time_limit"));
				survey.setOnTransactionCompleteEn(resultSet.getString("on_transaction_complete_en"));
				survey.setOnTransactionCompleteAz(resultSet.getString("on_transaction_complete_az"));
				survey.setOnTransactionCompleteRu(resultSet.getString("on_transaction_complete_ru"));
				survey.setVisitLimit(resultSet.getString("visit_limit"));
				survey.setCustomerType(resultSet.getString("customer_type"));
				survey.setSurveyLimit(resultSet.getString("survey_limit"));
				survey.setQuestions(getQuestionsBySurveyID(survey.getId(), msisdn));
				survey.setSurveyCount(getSurveyCount(survey.getId(), msisdn));
				surveys.add(survey);
			}

			logger.info(msisdn + "- return data from survey");
			return surveys;

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return null;
	}

	public static String getSurveyCount(String surveyId, String msisdn) {
		logger.info(msisdn + "-Getting survey count for survey id-" + surveyId);
		String surveyCount = "0";
		String query = "SELECT count(*) as surveyCount FROM user_survey_data where survey_id =" + surveyId
				+ " and msisdn= " + msisdn;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		java.sql.Connection conn = null;
		try {
			conn = DBFactory.getMagentoDBConnection();
			stmt = conn.prepareStatement(query);
			resultSet = stmt.executeQuery(query);
			logger.info(msisdn + "-Get Survey count query-" + query);
			while (resultSet.next()) {
				surveyCount = resultSet.getInt("surveyCount") + "";
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
		}
		return surveyCount;
	}

	private static List<Question> getQuestionsBySurveyID(String surveyId, String msisdn) {
		logger.info(msisdn + "- Getting questions data from database");
		List<Question> questions = new ArrayList<>();
		String query = "SELECT * FROM question where survey_id=" + surveyId;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		java.sql.Connection conn = null;
		try {
			conn = DBFactory.getMagentoDBConnection();
			stmt = conn.prepareStatement(query);
			logger.info(msisdn + "-Questions By Survey ID query-" + query);
			resultSet = stmt.executeQuery(query);

			while (resultSet.next()) {
				Question question = new Question();
				question.setId(resultSet.getString("id"));
				question.setAnswerType(resultSet.getString("answer_type"));
				question.setQuestionTextAZ(resultSet.getString("text_az"));
				question.setQuestionTextEN(resultSet.getString("text_en"));
				question.setQuestionTextRU(resultSet.getString("text_ru"));
				question.setAnswers(getAnswersByQuestionId(question.getId(), msisdn));
				questions.add(question);
			}
			logger.info(msisdn + "- return data from question ");
			return questions;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
		}
		return null;
	}

	private static List<Answer> getAnswersByQuestionId(String questionId, String msisdn) {
		logger.info(msisdn + "- Getting answers data from database");
		List<Answer> answers = new ArrayList<>();
		String query = "SELECT * FROM answer where question_id = " + questionId;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		java.sql.Connection conn = null;
		try {
			conn = DBFactory.getMagentoDBConnection();
			stmt = conn.prepareStatement(query);
			logger.info(msisdn + "-Get Answers by Question Id query-" + query);
			resultSet = stmt.executeQuery(query);

			while (resultSet.next()) {
				Answer answer = new Answer();
				answer.setId(resultSet.getString("id"));
				answer.setAnswerTextAZ(resultSet.getString("text_az"));
				answer.setAnswerTextEN(resultSet.getString("text_en"));
				answer.setAnswerTextRU(resultSet.getString("text_ru"));

				answers.add(answer);
			}
			logger.info(msisdn + "- return data from answers ");
			return answers;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
		}
		return null;
	}

	public static List<SurveyCountResponseData> getAllSurveyCount(String msisdn) {
		logger.info(msisdn + "-Getting survey count for All survey");
		List<SurveyCountResponseData> responseData = new ArrayList<>();
		String query = "SELECT survey.id, count(user_survey_data.id) as surveyCount"
				+ " FROM survey LEFT JOIN user_survey_data"
				+ " ON survey.id = user_survey_data.survey_id and user_survey_data.msisdn=" + msisdn
				+ " GROUP BY survey.id";
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		java.sql.Connection conn = null;
		try {
			conn = DBFactory.getMagentoDBConnection();
			stmt = conn.prepareStatement(query);
			logger.info(msisdn + "-Get All Survey Count query-" + query);
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()) {
				SurveyCountResponseData respData = new SurveyCountResponseData();
				respData.setSurveyId(resultSet.getString("id"));
				respData.setSurveyCount(resultSet.getString("surveyCount"));
				responseData.add(respData);
			}
			return responseData;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
		}
		return null;
	}

	public List<UserSurveyData> getUserSurveyList(String msisdn) {
		logger.info(msisdn + "-Getting user survey for " + msisdn);
		List<UserSurveyData> responseData = new ArrayList<>();
		String query = "SELECT user_survey_data.*, answer.text_en "
				+ "FROM user_survey_data, answer WHERE user_survey_data.answer_id = answer.id "
				+ "and user_survey_data.offering_id IS NOT NULL and user_survey_data.msisdn=" + msisdn;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		java.sql.Connection conn = null;
		try {
			conn = DBFactory.getMagentoDBConnection();
			stmt = conn.prepareStatement(query);
			logger.info(msisdn + "-Get User Survey List query-" + query);
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()) {
				UserSurveyData respData = new UserSurveyData();
				respData.setAnswer(resultSet.getString("text_en"));
				respData.setOfferingId(resultSet.getString("offering_id"));
				respData.setOfferingType(resultSet.getString("offering_type"));
				respData.setComment(resultSet.getString("comment"));
				respData.setQuestionId(resultSet.getString("question_id"));
				respData.setAnswerId(resultSet.getString("answer_id"));
				respData.setSurveyId(resultSet.getString("survey_id"));
				responseData.add(respData);
			}
			return responseData;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
		}
		return null;
	}

	public static boolean updateExistingUserSurvey(SaveSurveyRequest cclient) {
		logger.info(cclient.getmsisdn() + "-Updating user survey with offering id " + cclient.getOfferingIdSurvey());
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			conn = DBFactory.getMagentoDBConnection();
			stmt = conn.prepareStatement("Update user_survey_data set comment='" + cclient.getComment()
					+ "', answer_id=" + cclient.getAnswerId() + " where offering_id=" + cclient.getOfferingIdSurvey()
					+ " and msisdn=" + cclient.getmsisdn());
			logger.info(cclient.getmsisdn() + "-update Existing User Survey query-" + stmt);
			int returnValue = stmt.executeUpdate();
			if (returnValue == 1) {
				return true;
			}
			return false;

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
		}
		return false;
	}
}
