package com.evampsaanga.azerfon.survey;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetAllSurveyCountResponse extends BaseResponse {
	List<SurveyCountResponseData> surveyCountData = new ArrayList<>();

	public List<SurveyCountResponseData> getSurveyCountData() {
		return surveyCountData;
	}

	public void setSurveyCountData(List<SurveyCountResponseData> surveyCountData) {
		this.surveyCountData = surveyCountData;
	}

	@Override
	public String toString() {
		return "GetAllSurveyCountResponse [surveyCountData=" + surveyCountData + "]";
	}

}
