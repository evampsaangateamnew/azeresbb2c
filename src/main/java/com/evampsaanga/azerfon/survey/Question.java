package com.evampsaanga.azerfon.survey;

import java.util.ArrayList;
import java.util.List;

public class Question {
	private String id;
	private String questionTextEN;
	private String questionTextAZ;
	private String questionTextRU;
	private String answerType;
	private List<Answer> answers = new ArrayList<Answer>();

	public Question(String id, String questionTextEN, String questionTextAZ, String questionTextRU, String answerType,
			List<Answer> answers) {
		super();
		this.id = id;
		this.questionTextEN = questionTextEN;
		this.questionTextAZ = questionTextAZ;
		this.questionTextRU = questionTextRU;
		this.answerType = answerType;
		this.answers = answers;
	}

	public Question() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestionTextEN() {
		return questionTextEN;
	}

	public void setQuestionTextEN(String questionTextEN) {
		this.questionTextEN = questionTextEN;
	}

	public String getQuestionTextAZ() {
		return questionTextAZ;
	}

	public void setQuestionTextAZ(String questionTextAZ) {
		this.questionTextAZ = questionTextAZ;
	}

	public String getQuestionTextRU() {
		return questionTextRU;
	}

	public void setQuestionTextRU(String questionTextRU) {
		this.questionTextRU = questionTextRU;
	}

	public String getAnswerType() {
		return answerType;
	}

	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	@Override
	public String toString() {
		return "Question [id=" + id + ", questionTextEN=" + questionTextEN + ", questionTextAZ=" + questionTextAZ
				+ ", questionTextRU=" + questionTextRU + ", answerType=" + answerType + ", answers=" + answers + "]";
	}

}