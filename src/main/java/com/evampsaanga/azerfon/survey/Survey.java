package com.evampsaanga.azerfon.survey;

import java.util.ArrayList;
import java.util.List;

public class Survey {
	private String id;
	private String screenName;
	private String timeLimit;
	private String visitLimit;
	private String surveyLimit;
	private String surveyCount = "";
	private String onTransactionCompleteEn;
	private String onTransactionCompleteAz;
	private String onTransactionCompleteRu;
	private String commentEnable;
	private String commentsTitleEn;
	private String commentsTitleAz;
	private String commentsTitleRu;
	private String titleEn;
	private String titleAz;
	private String titleRu;
	private String customerType;
	private List<Question> questions = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}

	public String getVisitLimit() {
		return visitLimit;
	}

	public void setVisitLimit(String visitLimit) {
		this.visitLimit = visitLimit;
	}

	public String getSurveyLimit() {
		return surveyLimit;
	}

	public void setSurveyLimit(String surveyLimit) {
		this.surveyLimit = surveyLimit;
	}

	public String getSurveyCount() {
		return surveyCount;
	}

	public void setSurveyCount(String surveyCount) {
		this.surveyCount = surveyCount;
	}

	public String getOnTransactionCompleteEn() {
		return onTransactionCompleteEn;
	}

	public void setOnTransactionCompleteEn(String onTransactionCompleteEn) {
		this.onTransactionCompleteEn = onTransactionCompleteEn;
	}

	public String getOnTransactionCompleteAz() {
		return onTransactionCompleteAz;
	}

	public void setOnTransactionCompleteAz(String onTransactionCompleteAz) {
		this.onTransactionCompleteAz = onTransactionCompleteAz;
	}

	public String getOnTransactionCompleteRu() {
		return onTransactionCompleteRu;
	}

	public void setOnTransactionCompleteRu(String onTransactionCompleteRu) {
		this.onTransactionCompleteRu = onTransactionCompleteRu;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public String getCommentEnable() {
		return commentEnable;
	}

	public void setCommentEnable(String commentEnable) {
		this.commentEnable = commentEnable;
	}

	public String getCommentsTitleEn() {
		return commentsTitleEn;
	}

	public void setCommentsTitleEn(String commentsTitleEn) {
		this.commentsTitleEn = commentsTitleEn;
	}

	public String getCommentsTitleAz() {
		return commentsTitleAz;
	}

	public void setCommentsTitleAz(String commentsTitleAz) {
		this.commentsTitleAz = commentsTitleAz;
	}

	public String getCommentsTitleRu() {
		return commentsTitleRu;
	}

	public void setCommentsTitleRu(String commentsTitleRu) {
		this.commentsTitleRu = commentsTitleRu;
	}

	public String getTitleEn() {
		return titleEn;
	}

	public void setTitleEn(String titleEn) {
		this.titleEn = titleEn;
	}

	public String getTitleAz() {
		return titleAz;
	}

	public void setTitleAz(String titleAz) {
		this.titleAz = titleAz;
	}

	public String getTitleRu() {
		return titleRu;
	}

	public void setTitleRu(String titleRu) {
		this.titleRu = titleRu;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	@Override
	public String toString() {
		return "Survey [id=" + id + ", screenName=" + screenName + ", timeLimit=" + timeLimit + ", visitLimit="
				+ visitLimit + ", surveyLimit=" + surveyLimit + ", surveyCount=" + surveyCount
				+ ", onTransactionCompleteEn=" + onTransactionCompleteEn + ", onTransactionCompleteAz="
				+ onTransactionCompleteAz + ", onTransactionCompleteRu=" + onTransactionCompleteRu + ", commentEnable="
				+ commentEnable + ", commentsTitleEn=" + commentsTitleEn + ", commentsTitleAz=" + commentsTitleAz
				+ ", commentsTitleRu=" + commentsTitleRu + ", titleEn=" + titleEn + ", titleAz=" + titleAz
				+ ", titleRu=" + titleRu + ", customerType=" + customerType + ", questions=" + questions + "]";
	}

}