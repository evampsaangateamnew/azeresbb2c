package com.evampsaanga.azerfon.survey;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetSurveysResponse extends BaseResponse {
	private List<Survey> surveys = new ArrayList<>();

	public List<Survey> getSurveys() {
		return surveys;
	}

	public void setSurveys(List<Survey> surveys) {
		this.surveys = surveys;
	}

	@Override
	public String toString() {
		return "GetSurveysResponse [surveys=" + surveys + ", toString()=" + super.toString() + "]";
	}

}
