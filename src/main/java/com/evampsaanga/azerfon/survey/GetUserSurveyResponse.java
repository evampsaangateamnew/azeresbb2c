package com.evampsaanga.azerfon.survey;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetUserSurveyResponse extends BaseResponse {
	List<UserSurveyData> userSurveys = new ArrayList<>();

	public List<UserSurveyData> getUserSurveys() {
		return userSurveys;
	}

	public void setUserSurveys(List<UserSurveyData> userSurveys) {
		this.userSurveys = userSurveys;
	}

	@Override
	public String toString() {
		return "GetUserSurveyResponse [userSurveys=" + userSurveys + ", toString()=" + super.toString() + "]";
	}

}
