package com.evampsaanga.azerfon.survey;

public class SurveyCountResponseData {
	private String surveyId;
	private String surveyCount;

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getSurveyCount() {
		return surveyCount;
	}

	public void setSurveyCount(String surveyCount) {
		this.surveyCount = surveyCount;
	}

	@Override
	public String toString() {
		return "SurveyCountResponseData [surveyId=" + surveyId + ", surveyCount=" + surveyCount + "]";
	}

}
