package com.evampsaanga.azerfon.survey;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class SaveSurveyResponse extends BaseResponse {

	@Override
	public String toString() {
		return "SaveSurveyResponse [toString()=" + super.toString() + "]";
	}

}
