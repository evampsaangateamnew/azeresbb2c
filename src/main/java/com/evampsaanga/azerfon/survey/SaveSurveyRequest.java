package com.evampsaanga.azerfon.survey;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class SaveSurveyRequest extends BaseRequest {
	private String comment;
	private String answerId;
	private String questionId;
	private String surveyId;
	private String offeringIdSurvey = "";
	private String offeringTypeSurvey = "";

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getAnswerId() {
		return answerId;
	}

	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getOfferingIdSurvey() {
		return offeringIdSurvey;
	}

	public void setOfferingIdSurvey(String offeringIdSurvey) {
		this.offeringIdSurvey = offeringIdSurvey;
	}

	public String getOfferingTypeSurvey() {
		return offeringTypeSurvey;
	}

	public void setOfferingTypeSurvey(String offeringTypeSurvey) {
		this.offeringTypeSurvey = offeringTypeSurvey;
	}

	@Override
	public String toString() {
		return "SaveSurveyRequest [comment=" + comment + ", answerId=" + answerId + ", questionId=" + questionId
				+ ", surveyId=" + surveyId + ", offeringIdSurvey=" + offeringIdSurvey + ", offeringTypeSurvey="
				+ offeringTypeSurvey + "]";
	}
}
