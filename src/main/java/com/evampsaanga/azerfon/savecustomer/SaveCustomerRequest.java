package com.evampsaanga.azerfon.savecustomer;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.evampsaanga.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SaveCustomerRequest")
public class SaveCustomerRequest extends BaseRequest {
	@JsonProperty("temp")
	private String temp;
	@JsonProperty("password")
	private String password;
	@JsonProperty("confirm_password")
	private String confirmPassword;
	@JsonProperty("terms_and_conditions")
	private String termsAndConditions;
	@JsonProperty("customerData")
	private CustomerData customerData;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("temp")
	public String getTemp() {
		return temp;
	}

	@JsonProperty("temp")
	public void setTemp(String temp) {
		this.temp = temp;
	}
	
	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	@JsonProperty("password")
	public void setPassword(String password) {
		this.password = password;
	}

	@JsonProperty("confirm_password")
	public String getConfirmPassword() {
		return confirmPassword;
	}

	@JsonProperty("confirm_password")
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@JsonProperty("terms_and_conditions")
	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	@JsonProperty("terms_and_conditions")
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	@JsonProperty("customerData")
	public CustomerData getCustomerData() {
		return customerData;
	}

	@JsonProperty("customerData")
	public void setCustomerData(CustomerData customerData) {
		this.customerData = customerData;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
