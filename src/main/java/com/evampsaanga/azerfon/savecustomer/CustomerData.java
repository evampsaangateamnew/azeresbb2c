package com.evampsaanga.azerfon.savecustomer;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class CustomerData {
	private String token = "";
	private String imageURL = "";
	private String sim = "";
	private String pin = "";
	private String puk = "";
	private String customerId = "";

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the imageURL
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * @param imageURL
	 *            the imageURL to set
	 */
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	/**
	 * @return the sim
	 */
	public String getSim() {
		return sim;
	}

	/**
	 * @param sim
	 *            the sim to set
	 */
	public void setSim(String sim) {
		this.sim = sim;
	}

	/**
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * @param pin
	 *            the pin to set
	 */
	public void setPin(String pin) {
		this.pin = pin;
	}

	/**
	 * @return the puk
	 */
	public String getPuk() {
		return puk;
	}

	/**
	 * @param puk
	 *            the puk to set
	 */
	public void setPuk(String puk) {
		this.puk = puk;
	}

	/*
	 * json.remove("token"); json.remove("imageURL"); json.remove("sim");
	 * json.remove("pin"); json.remove("puk");
	 */
	private String billingLanguage = "";

	/**
	 * @return the billingLanguage
	 */
	public String getBillingLanguage() {
		return billingLanguage;
	}

	/**
	 * @param billingLanguage
	 *            the billingLanguage to set
	 */
	public void setBillingLanguage(String billingLanguage) {
		this.billingLanguage = billingLanguage;
	}

	@JsonProperty("offeringName")
	private String offeringName = "";
	@JsonProperty("brandName")
	private String brandName = "";

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName
	 *            the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the offeringName
	 */
	public String getOfferingName() {
		return offeringName;
	}

	/**
	 * @param offeringName
	 *            the offeringName to set
	 */
	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	@JsonProperty("title")
	private String title = "";
	@JsonProperty("firstName")
	private String firstName = "";
	@JsonProperty("middleName")
	private String middleName = "";
	@JsonProperty("lastName")
	private String lastName = "";
	@JsonProperty("customerType")
	private String customerType = "";
	@JsonProperty("gender")
	private String gender = "";
	@JsonProperty("dob")
	private String dob = "";
	@JsonProperty("accountId")
	private String accountId = "";
	@JsonProperty("language")
	private String language = "";
	@JsonProperty("effectiveDate")
	private String effectiveDate = "";
	@JsonProperty("expiryDate")
	private String expiryDate = "";
	@JsonProperty("subscriberType")
	private String subscriberType = "";
	@JsonProperty("status")
	private String status = "";
	@JsonProperty("statusDetails")
	private String statusDetails = "";
	@JsonProperty("brandId")
	private String brandId = "";
	@JsonProperty("loyaltySegment")
	private String loyaltySegment = "";
	@JsonProperty("offeringId")
	private String offeringId = "";
	@JsonProperty("msisdn")
	private String msisdn = "";
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("firstName")
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty("firstName")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("middleName")
	public String getMiddleName() {
		return middleName;
	}

	@JsonProperty("middleName")
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}

	@JsonProperty("lastName")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("customerType")
	public String getCustomerType() {
		return customerType;
	}

	@JsonProperty("customerType")
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}

	@JsonProperty("gender")
	public void setGender(String gender) {
		this.gender = gender;
	}

	@JsonProperty("dob")
	public String getDob() {
		return dob;
	}

	@JsonProperty("dob")
	public void setDob(String dob) {
		this.dob = dob;
	}

	@JsonProperty("accountId")
	public String getAccountId() {
		return accountId;
	}

	@JsonProperty("accountId")
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@JsonProperty("language")
	public String getLanguage() {
		return language;
	}

	@JsonProperty("language")
	public void setLanguage(String language) {
		this.language = language;
	}

	@JsonProperty("effectiveDate")
	public String getEffectiveDate() {
		return effectiveDate;
	}

	@JsonProperty("effectiveDate")
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@JsonProperty("expiryDate")
	public String getExpiryDate() {
		return expiryDate;
	}

	@JsonProperty("expiryDate")
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@JsonProperty("subscriberType")
	public String getSubscriberType() {
		return subscriberType;
	}

	@JsonProperty("subscriberType")
	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("statusDetails")
	public String getStatusDetails() {
		return statusDetails;
	}

	@JsonProperty("statusDetails")
	public void setStatusDetails(String statusDetails) {
		this.statusDetails = statusDetails;
	}

	@JsonProperty("brandId")
	public String getBrandId() {
		return brandId;
	}

	@JsonProperty("brandId")
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	@JsonProperty("loyaltySegment")
	public String getLoyaltySegment() {
		return loyaltySegment;
	}

	@JsonProperty("loyaltySegment")
	public void setLoyaltySegment(String loyaltySegment) {
		this.loyaltySegment = loyaltySegment;
	}

	@JsonProperty("offeringId")
	public String getOfferingId() {
		return offeringId;
	}

	@JsonProperty("offeringId")
	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@JsonProperty("msisdn")
	public String getMsisdn() {
		return msisdn;
	}

	@JsonProperty("msisdn")
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
