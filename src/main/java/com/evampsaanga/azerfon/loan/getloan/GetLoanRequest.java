package com.evampsaanga.azerfon.loan.getloan;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class GetLoanRequest extends BaseRequest {
	private String loanAmount;
	private String friendMsisdn;

	public String getFriendMsisdn() {
		return friendMsisdn;
	}

	public void setFriendMsisdn(String friendMsisdn) {
		this.friendMsisdn = friendMsisdn;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	@Override
	public String toString() {
		return "GetLoanRequest [friendMsisdn=" + friendMsisdn + "]";
	}

}
