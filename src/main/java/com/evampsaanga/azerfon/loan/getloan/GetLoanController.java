package com.evampsaanga.azerfon.loan.getloan;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.azerfon.validator.RequestValidator;
import com.evampsaanga.azerfon.validator.ResponseValidator;
import com.evampsaanga.azerfon.validator.ValidatorService;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Helper;

@Path("/azerfon/")
public class GetLoanController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getloan")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetLoanResponse Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			// ------------------------- Setting Transaction Name
			// ---------------------- //

			logs.setTransactionName(Transactions.LOAN_REQUEST_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.LOAN_REQUEST_HISTORY);
			logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());

			GetLoanResponse getLoanResponse = new GetLoanResponse();
			GetLoanRequest cclient = null;

			String transactionName = Transactions.LOAN_REQUEST_TRANSACTION_NAME;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text
			// --------------------------- //

			cclient = Helper.JsonToObject(requestBody, GetLoanRequest.class);

			if (cclient != null) {
				logs.setMsisdn(cclient.getmsisdn());
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setReceiverMsisdn(cclient.getFriendMsisdn());

			}

			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				GetLoanRequest getLoanRequest = new GetLoanRequest();

				getLoanRequest = Helper.JsonToObject(requestBody, GetLoanRequest.class);

				getLoanResponse = AzerfonThirdPartyCalls.getLoan(token, transactionName, getLoanRequest,
						getLoanResponse);
				if (getLoanResponse.getReturnCode().equalsIgnoreCase(ResponseCodes.SUCESS_CODE_200)) {
					logger.info(token + "Charging call skipped.");
					// Commented Because of CR, on 5-08-2020. Now this charging
					// will be handled at Simberella end.
					// AzerfonThirdPartyCalls.responseChargecustomer(token,
					// getLoanRequest.getmsisdn(), "500328");
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDescription("SUCCESS");
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
				}

			} else {

				getLoanResponse.setReturnCode(responseValidator.getResponseCode());
				getLoanResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(getLoanResponse.getReturnCode());
				logs.setResponseDescription(getLoanResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(
					token + "-Response Returned From-" + transactionName + "-" + Helper.ObjectToJson(getLoanResponse));

			return getLoanResponse;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			GetLoanResponse resp = new GetLoanResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

			return resp;
		}
	}
}
