package com.evampsaanga.azerfon.loan.getprovisions;

import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProvisionsResponse extends BaseResponse {

	private List<Provisions> provisions;

	public List<Provisions> getProvisions() {
		return provisions;
	}

	public void setProvisions(List<Provisions> provisions) {
		this.provisions = provisions;
	}

	@Override
	public String toString() {
		return "GetProvisionsResponse [provisions=" + provisions + "]";
	}

}