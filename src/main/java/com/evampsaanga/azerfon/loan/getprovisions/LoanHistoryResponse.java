/**
 * 
 */
package com.evampsaanga.azerfon.loan.getprovisions;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class LoanHistoryResponse extends BaseResponse {

	private LoanHistoryResponseData data;

	public LoanHistoryResponseData getData() {
		return data;
	}

	public void setData(LoanHistoryResponseData data) {
		this.data = data;
	}

}
