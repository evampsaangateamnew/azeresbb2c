package com.evampsaanga.azerfon.loan.getcollections;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.azerfon.validator.RequestValidator;
import com.evampsaanga.azerfon.validator.ResponseValidator;
import com.evampsaanga.azerfon.validator.ValidatorService;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Helper;

@Path("/azerfon/")
public class GetCollectionsController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getcollections")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCollectionsResponse Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			// ------------------------- Setting Transaction Name ---------------------- //

			GetCollectionsResponse getCollectionsResponse = new GetCollectionsResponse();
			GetCollectionsRequest cclient = null;

			String transactionName = Transactions.LOAN_PAYMENT_HISTORY_TRANSACTION_NAME;
			logs.setTransactionName(Transactions.LOAN_PAYMENT_HISTORY_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.LOAN_PAYMENT_HISTORY);
			logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
			
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text --------------------------- //

			cclient = Helper.JsonToObject(requestBody, GetCollectionsRequest.class);
			
			if(cclient != null)
			{
				logs.setMsisdn(cclient.getmsisdn());
				logs.setChannel(cclient.getChannel());
			}
			
			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				GetCollectionsRequest getProvisionsRequest = new GetCollectionsRequest();

				getProvisionsRequest = Helper.JsonToObject(requestBody, GetCollectionsRequest.class);

				getCollectionsResponse = AzerfonThirdPartyCalls.loanGetCollections(token, transactionName,
						getProvisionsRequest, getCollectionsResponse);
				logs.setResponseCode(getCollectionsResponse.getReturnCode());
				logs.setResponseDescription(getCollectionsResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			} else {

				getCollectionsResponse.setReturnCode(responseValidator.getResponseCode());
				getCollectionsResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(getCollectionsResponse.getReturnCode());
				logs.setResponseDescription(getCollectionsResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(token + "-Response Returned From-" + transactionName + "-"
					+ Helper.ObjectToJson(getCollectionsResponse));

			return getCollectionsResponse;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			GetCollectionsResponse resp = new GetCollectionsResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

			return resp;
		}
	}
}
