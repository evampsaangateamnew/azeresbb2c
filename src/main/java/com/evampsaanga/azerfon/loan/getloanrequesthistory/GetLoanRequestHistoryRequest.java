package com.evampsaanga.azerfon.loan.getloanrequesthistory;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class GetLoanRequestHistoryRequest extends BaseRequest {

	private String startDate;
	private String endDate;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "GetLoanPaymentHistoryRequest [startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}
