package com.evampsaanga.azerfon.loan.getloanrequesthistory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.azerfon.validator.RequestValidator;
import com.evampsaanga.azerfon.validator.ResponseValidator;
import com.evampsaanga.azerfon.validator.ValidatorService;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Helper;

@Path("/azerfon/")
public class GetLoanRequestHistoryController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getloanrequesthistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetLoanRequestHistoryResponse Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			logs.setTransactionName(Transactions.LOAN_REQUEST_HISTORY_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.LOAN_REQUEST);
			logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
			// ------------------------- Setting Transaction Name ---------------------- //

			
			GetLoanRequestHistoryResponse getLoanRequestHistoryResponse = new GetLoanRequestHistoryResponse();
			GetLoanRequestHistoryRequest cclient = null;

			String transactionName = Transactions.LOAN_REQUEST_HISTORY_TRANSACTION_NAME;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text --------------------------- //

			cclient = Helper.JsonToObject(requestBody, GetLoanRequestHistoryRequest.class);
			if(cclient != null)
			{
				logs.setMsisdn(cclient.getmsisdn());
				logs.setChannel(cclient.getChannel());
			}
			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();
			
			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				GetLoanRequestHistoryRequest getLoanRequestHistoryRequest = new GetLoanRequestHistoryRequest();

				getLoanRequestHistoryRequest = Helper.JsonToObject(requestBody, GetLoanRequestHistoryRequest.class);

				getLoanRequestHistoryResponse = AzerfonThirdPartyCalls.loanRequestHistory(token, transactionName,
						getLoanRequestHistoryRequest, getLoanRequestHistoryResponse);
				logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
				logs.setResponseDescription("SUCCESS");
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			} else {

				getLoanRequestHistoryResponse.setReturnCode(responseValidator.getResponseCode());
				getLoanRequestHistoryResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(getLoanRequestHistoryResponse.getReturnCode());
				logs.setResponseDescription(getLoanRequestHistoryResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(token + "-Response Returned From-" + transactionName + "-"
					+ Helper.ObjectToJson(getLoanRequestHistoryResponse));

			return getLoanRequestHistoryResponse;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			GetLoanRequestHistoryResponse resp = new GetLoanRequestHistoryResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

			return resp;
		}
	}
}
