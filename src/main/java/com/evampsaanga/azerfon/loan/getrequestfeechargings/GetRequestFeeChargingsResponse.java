package com.evampsaanga.azerfon.loan.getrequestfeechargings;

import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetRequestFeeChargingsResponse extends BaseResponse {

	private List<RequestFeeCharging> requestFeeChargings;

	public List<RequestFeeCharging> getRequestFeeChargings() {
		return requestFeeChargings;
	}

	public void setRequestFeeChargings(List<RequestFeeCharging> requestFeeChargings) {
		this.requestFeeChargings = requestFeeChargings;
	}

	@Override
	public String toString() {
		return "GetRequestFeeChargingsResponse [requestFeeChargings=" + requestFeeChargings + "]";
	}

}