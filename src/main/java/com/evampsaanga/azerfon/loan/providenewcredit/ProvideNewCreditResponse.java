package com.evampsaanga.azerfon.loan.providenewcredit;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProvideNewCreditResponse extends BaseResponse {

}