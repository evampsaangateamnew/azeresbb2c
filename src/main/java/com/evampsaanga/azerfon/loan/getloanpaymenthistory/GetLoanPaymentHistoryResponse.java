package com.evampsaanga.azerfon.loan.getloanpaymenthistory;

import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetLoanPaymentHistoryResponse extends BaseResponse {

	private List<LoanPayment> loanPayment;

	public List<LoanPayment> getLoanPayment() {
		return loanPayment;
	}

	public void setLoanPayment(List<LoanPayment> loanPayment) {
		this.loanPayment = loanPayment;
	}

	@Override
	public String toString() {
		return "GetLoanPaymentHistoryResponse [loanPayment=" + loanPayment + "]";
	}

}