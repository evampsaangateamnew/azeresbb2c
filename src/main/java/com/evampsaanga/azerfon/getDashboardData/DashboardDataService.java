package com.evampsaanga.azerfon.getDashboardData;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.azerfon.getUsersV2.GetUsersGroupInfoService;
import com.evampsaanga.azerfon.getUsersV2.UsersGroupRequest;
import com.evampsaanga.azerfon.queryBalancePICV2.QueryBalancePicResponse;
import com.evampsaanga.azerfon.queryBalancePICV2.QueryBalancePicService;
import com.evampsaanga.azerfon.queryinvoiceV2.QueryInvoiceRequestData;
import com.evampsaanga.azerfon.queryinvoiceV2.QueryInvoiceResponse;
import com.evampsaanga.azerfon.queryinvoiceV2.QueryInvoiceService;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.magentoservices.MagentoServices;
import com.evampsaanga.models.LoginRequest;
import com.evampsaanga.models.LoginResponse;
import com.evampsaanga.validator.rules.ChannelNotEmpty;
import com.evampsaanga.validator.rules.IPNotEmpty;
import com.evampsaanga.validator.rules.LangNotEmpty;
import com.evampsaanga.validator.rules.MSISDNNotEmpty;
import com.evampsaanga.validator.rules.ValidationResult;
/**
 * 
 * @author Aqeel Abbas
 * Generates dashboard data for mobile app
 *
 */
@Path("/bakcell")
public class DashboardDataService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
/**
 * 
 * @param requestBody
 * @param credential
 * @param isFromB2B
 * @return dashboard information for mobile app
 * @throws SQLException
 * @throws InterruptedException
 */
	@POST
	@Path("/getdashboardinfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DashboardDataResponse getDashboardData(@Body String requestBody, @Header("credentials") String credential,
			@Header("isFromB2B") String isFromB2B) throws SQLException, InterruptedException {
		// Logging incoming request to log file
		Helper.logInfoMessageV2("E&S -- Request Land Time " + new Date());
		Helper.logInfoMessageV2("Request lanaded on getdashboardinfo with data as :" + requestBody);

		String reqString = "";
		// Below is declaration block for variables to be used		
		DashboardDataRequest dashboardDataRequest = new DashboardDataRequest();
		DashboardDataResponse dashboardDataResponse = new DashboardDataResponse();
		// Logs object to store values which are to be inserted in database for
		// reporting
		Logs logs = new Logs();
		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.DASHBOARD);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.CustomerData);

		// Authenticating the request using credentials string received in
		// header
		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401,
					"authenticateAndAuthorizeCredentials");

		String token = "";
		String TrnsactionName = Transactions.BASE_MAGENTO_TRANSACTION_NAME + " " + Transactions.LOGIN_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "-Request Data-" + requestBody);
			dashboardDataRequest = Helper.JsonToObject(requestBody, DashboardDataRequest.class);
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			logger.error(Helper.GetException(ex));
			prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400,
					"JsonToObject");
		}
		if (authenticationresult && dashboardDataRequest != null) {
			// block sets the mandatory parameters to logs object which are
			// taken from request body
			logs.setIp(dashboardDataRequest.getiP());
			logs.setChannel(dashboardDataRequest.getChannel());
			logs.setMsisdn(dashboardDataRequest.getmsisdn());
			//validating request packet			
			ValidationResult validationResult = validateRequest(dashboardDataRequest);
			if (validationResult.isValidationResult()) {
				logs.setResponseCode(dashboardDataResponse.getReturnCode());
				logs.setResponseDescription(dashboardDataResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
			} else
				// if request parameters fails validation response is
				// prepared with validation error
				prepareErrorResponse(dashboardDataResponse, logs, validationResult.getValidationCode(),
						validationResult.getValidationMessage(), "validateRequest");
			// verifying login
			MagentoServices loginObj = new MagentoServices();
			// Login Response
			Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Request for user info: " + requestBody);
			LoginResponse loginResponse = new LoginResponse();
			LoginRequest loginRequest = new LoginRequest();
			try {
				loginRequest = Helper.JsonToObject(requestBody, LoginRequest.class);
			} catch (Exception ex) {
				logger.error("Exception:", ex);
				prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400, "JsonTOObject Conversion");
			}
			try {
				loginResponse = loginObj.getUserAuthenticated(loginRequest, isFromB2B,token);
				Helper.logInfoMessageV2("E&S -- DASHBOARD Login Response Time " + new Date());
				if (!ResponseCodes.SUCESS_CODE_200.equals(loginResponse.getReturnCode())) {
					Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Error while getting response from Login: " + loginResponse.getReturnCode());
					loginObj.prepareErrorResponse(loginResponse, logs, ResponseCodes.ERROR_400_CODE,
							ResponseCodes.ERROR_400);
					dashboardDataResponse.setLoginData(loginResponse.getLoginData());
				} else {
					
					String customerId = loginResponse.getLoginData().getCustomer_id();
					GetUsersGroupInfoService usersGroupInfoService = new GetUsersGroupInfoService();

					// Setting request values for user group data
					UsersGroupRequest usersGroupRequest = new UsersGroupRequest();
					usersGroupRequest.setChannel(dashboardDataRequest.getChannel());
					usersGroupRequest.setCustomerID(customerId);
					usersGroupRequest.setiP(dashboardDataRequest.getiP());
					usersGroupRequest.setLang(dashboardDataRequest.getLang());
					usersGroupRequest.setMsisdn(dashboardDataRequest.getmsisdn());
					reqString = Helper.ObjectToJson(usersGroupRequest);
					JSONObject jobj = new JSONObject(reqString);
					jobj.remove("acctCode");
					reqString = jobj.toString();
					//omitting user groups from the dashboard response
					/*Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Request Packet To get user info: " + reqString);
					UsersGroupResponse usersGroupResponse = usersGroupInfoService.getUsersGroupData(reqString,
							credential);
					Helper.logInfoMessageV2("E&S -- DASHBOARD user group Response Time " + new Date());
					if (!"200".equals(usersGroupResponse.getReturnCode())) {
						Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " -Error while getting response from user group info service: " + usersGroupResponse.getReturnCode());
						usersGroupInfoService.prepareErrorResponse(usersGroupResponse, logs,
								ResponseCodes.ERROR_400_CODE, "Unable to Fetch Users Group Information");
						dashboardDataResponse.setGroupData(usersGroupResponse.getGroupData());
						dashboardDataResponse.setUsers(usersGroupResponse.getUsers());
					}*/
					
					Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Request Packet To get user count: " + reqString);
					String userCount = usersGroupInfoService.getUsersCount(reqString);
					Helper.logInfoMessageV2("E&S -- DASHBOARD user count Response Time " + userCount + " " + new Date());
					dashboardDataResponse.setUserCount(userCount);
					
					
					//*********************
					//changing as customer id is not working
					usersGroupRequest.setCustomerID(loginResponse.getLoginData().getPic_attribute1());
					reqString = Helper.ObjectToJson(usersGroupRequest);
					
					jobj = new JSONObject(reqString);
					jobj.remove("acctCode");
					reqString = jobj.toString();
					//TODO
					//*********************
					Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " -Request to Fetch Balance Person Incharge Information " + reqString);
					QueryBalancePicService balancePicService = new QueryBalancePicService();
					QueryBalancePicResponse balancePicResponse = balancePicService.queryBalancePic(reqString,
							credential);
					Helper.logInfoMessageV2("E&S -- DASHBOARD PIC Balance Response Time " + new Date());
					if (!"200".equals(balancePicResponse.getReturnCode())) {
						Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " -Error while getting response from query balance pic service: " + balancePicResponse.getReturnCode());

						balancePicService.prepareErrorResponse(balancePicResponse, logs, ResponseCodes.ERROR_400_CODE,
								"Unable to Fetch Balance Person Incharge Information");
					}
					
					Calendar aCalendar = Calendar.getInstance();
					// add -1 month to current month
					aCalendar.add(Calendar.MONTH, -1);
					// set DATE to 1, so first date of previous month
					aCalendar.set(Calendar.DATE, 1);

					Date firstDateOfPreviousMonth = aCalendar.getTime();
					SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
					f.format(firstDateOfPreviousMonth);
					// set actual maximum date of previous month
					aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
					// read it
					Date lastDateOfPreviousMonth = aCalendar.getTime();
					f.format(lastDateOfPreviousMonth);

					// setting request vaalues for query invoice
					QueryInvoiceRequestData requestData = new QueryInvoiceRequestData();
					requestData.setChannel(dashboardDataRequest.getChannel());
					// changing it to customer account code from customer id(customerId)
					//TODO
					requestData.setCustomerID(loginResponse.getLoginData().getPic_attribute1());
					//*************
					requestData.setiP(dashboardDataRequest.getiP());
					requestData.setLang(dashboardDataRequest.getLang());
					requestData.setMsisdn(dashboardDataRequest.getmsisdn());
					//testing
					requestData.setStartTime(f.format(firstDateOfPreviousMonth).toString());
					requestData.setEndTime(f.format(lastDateOfPreviousMonth).toString());
					/*requestData.setStartTime("2016123000000");
					requestData.setEndTime("2017123000000");*/
					//*****************
					reqString = Helper.ObjectToJson(requestData);
					
					Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Reuqest to get invoice info: " + reqString);
					//Helper.logInfoMessage("Requrest String =" + reqString);
					QueryInvoiceService invoiceService = new QueryInvoiceService();
					QueryInvoiceResponse invoiceResponse = new QueryInvoiceResponse();
					try {
						invoiceResponse = invoiceService.queryInvoice(reqString, credential);
						Helper.logInfoMessageV2("E&S -- DASHBOARD Query Invoice Response Time " + new Date());
						if (!"200".equals(invoiceResponse.getReturnCode())) {
							Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Error while getting response from query invoice service invoice: " + invoiceResponse.getReturnCode());

							prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE,
									ResponseCodes.ERROR_400, "queryInvoice");
							invoiceService.prepareErrorResponse(invoiceResponse, logs, ResponseCodes.ERROR_400_CODE,
									ResponseCodes.ERROR_400);
						}
					} catch (Exception ex) {
						logger.error("Error", ex);
						prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE,
								ResponseCodes.ERROR_400, "queryInvoice");
						invoiceService.prepareErrorResponse(invoiceResponse, logs, ResponseCodes.ERROR_400_CODE,
								ResponseCodes.ERROR_400);
					} finally {
						try {
							dashboardDataResponse.setReturnCode("200");
							dashboardDataResponse.setReturnMsg("Successful");
							dashboardDataResponse.setLoginData(loginResponse.getLoginData());
							Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Setting predefined");
							dashboardDataResponse.setPredefinedData(loginResponse.getPredefinedData());
							//dashboardDataResponse.setGroupData(usersGroupResponse.getGroupData());
							//dashboardDataResponse.setUsers(usersGroupResponse.getUsers());
							dashboardDataResponse.setUserCount(userCount);
							dashboardDataResponse.setQueryBalancePicResponseData(
									balancePicResponse.getQueryBalancePicResponseData());
							dashboardDataResponse
									.setQueryInvoiceResponseData(invoiceResponse.getQueryInvoiceResponseData());
						} catch (Exception e) {
							logger.error("ERROR:", e);
						}
					}

				}
			} catch (Exception e) {
				logger.error("ERROR:", e);
				Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Error while getting response from Login");
				loginObj.prepareErrorResponse(loginResponse, logs, ResponseCodes.ERROR_400_CODE,
						ResponseCodes.ERROR_400);
				dashboardDataResponse.setLoginData(loginResponse.getLoginData());
			}
		}
		Helper.logInfoMessageV2("E&S -- DASHBOARD Response Dispatch Time " + new Date());
		return dashboardDataResponse;

	}
/**
 * 
 * @param dashboardDataRequest
 * @return validation result
 */
	private ValidationResult validateRequest(DashboardDataRequest dashboardDataRequest) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValidationResult(true);
		// Validating all the fields against non empty rule
		validationResult = new MSISDNNotEmpty().validateObject(dashboardDataRequest.getmsisdn());
		validationResult = new ChannelNotEmpty().validateObject(dashboardDataRequest.getChannel());
		validationResult = new IPNotEmpty().validateObject(dashboardDataRequest.getiP());
		validationResult = new LangNotEmpty().validateObject(dashboardDataRequest.getLang());
		return validationResult;
	}
/**
 * prepares error response and updates logs queue
 * @param dashboardDataResponse
 * @param logs
 * @param returnCode
 * @param returnMessage
 * @param api
 */
	private void prepareErrorResponse(DashboardDataResponse dashboardDataResponse, Logs logs, String returnCode,
			String returnMessage, String api) {
		dashboardDataResponse.setReturnCode(returnCode);
		dashboardDataResponse.setReturnMsg(returnMessage + " " + api);
		logs.setResponseCode(dashboardDataResponse.getReturnCode());
		logs.setResponseDescription(dashboardDataResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
	}
}
