package com.evampsaanga.azerfon.getDashboardData;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;
/**
 * Request Container for Dashboard api phase 2
 * @author EvampSaanga
 *
 */
public class DashboardDataRequest extends BaseRequest{
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
}
