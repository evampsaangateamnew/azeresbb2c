package com.evampsaanga.azerfon.changetarif;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class ChangeTarrifRequestClient extends BaseRequest {
	private String offeringId = "";
	private String actionType = "";

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
}
