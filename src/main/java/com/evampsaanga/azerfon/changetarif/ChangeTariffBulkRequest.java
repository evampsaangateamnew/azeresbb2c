
package com.evampsaanga.azerfon.changetarif;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "recieverMsisdn",
    "destination tariff",
    "tariff_permissions"
})
public class ChangeTariffBulkRequest extends BaseRequest {

    @JsonProperty("recieverMsisdn")
    private List<RecieverMsisdn> recieverMsisdn = null;
    @JsonProperty("destination tariff")
    private String destinationTariff;
    @JsonProperty("tariff_permissions")
    private String tariffPermissions;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("recieverMsisdn")
    public List<RecieverMsisdn> getRecieverMsisdn() {
        return recieverMsisdn;
    }

    @JsonProperty("recieverMsisdn")
    public void setRecieverMsisdn(List<RecieverMsisdn> recieverMsisdn) {
        this.recieverMsisdn = recieverMsisdn;
    }

    @JsonProperty("destination tariff")
    public String getDestinationTariff() {
        return destinationTariff;
    }

    @JsonProperty("destination tariff")
    public void setDestinationTariff(String destinationTariff) {
        this.destinationTariff = destinationTariff;
    }

    @JsonProperty("tariff_permissions")
    public String getTariffPermissions() {
        return tariffPermissions;
    }

    @JsonProperty("tariff_permissions")
    public void setTariffPermissions(String tariffPermissions) {
        this.tariffPermissions = tariffPermissions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
