package com.evampsaanga.azerfon.forgotpassword;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.evampsaanga.azerfon.requestheaders.BaseRequest;
/**
 * Forgot Password Request Container For Phase 2
 * @author Aqeel Abbas
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ForgotPasswordRequest")
public class ForgotPasswordRequestV2 extends BaseRequest {
	
	@XmlElement(name = "temp", required = true)
	private String temp = "";
	@XmlElement(name = "password", required = true)
	private String password = "";
	@XmlElement(name = "confirmPassword", required = true)
	private String confirmPassword = "";
	private String userName;
	
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * @param confirmPassword
	 *            the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}
}
