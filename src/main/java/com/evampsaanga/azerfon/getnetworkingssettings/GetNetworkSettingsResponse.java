package com.evampsaanga.azerfon.getnetworkingssettings;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetNetworkSettingsResponse extends BaseResponse {
	com.huawei.crm.query.GetNetworkSettingDataList getNetworkSettingDataBody=null;

	public com.huawei.crm.query.GetNetworkSettingDataList getGetNetworkSettingDataBody() {
		return getNetworkSettingDataBody;
	}

	public void setGetNetworkSettingDataBody(com.huawei.crm.query.GetNetworkSettingDataList getNetworkSettingDataBody) {
		this.getNetworkSettingDataBody = getNetworkSettingDataBody;
	}
	

}
