package com.evampsaanga.azerfon.getcoreservicesbulkV2;

import com.evampsaanga.configs.Constants;
/**
 * Core Services list Data Container
 * @author Aqeel Abbas
 *
 */
public class CoreServicesList {
	private String offeringId = "";
	private String title = "";
	private String desc = "";
	private String status = "";
	private String forwardNumber = "";

	/**
	 * @return the forwardNumber
	 */
	public String getForwardNumber() {
		return forwardNumber;
	}

	/**
	 * @param forwardNumber
	 *            the forwardNumber to set
	 */
	public void setForwardNumber(String forwardNumber) {
		this.forwardNumber = forwardNumber;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public CoreServicesList(String offeringId, String title, String desc, String status, String forwardNumber) {
		super();
		this.offeringId = offeringId;
		if(!this.offeringId.equalsIgnoreCase(Constants.I_AM_BACK_OFFERING_ID))
			this.title = title;
		this.desc = desc;
		this.status = status;
		this.forwardNumber = forwardNumber;
	}

	/**
	 * @return the offeringId
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * @param offeringId
	 *            the offeringId to set
	 */
	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
}
