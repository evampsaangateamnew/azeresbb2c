package com.evampsaanga.azerfon.createorder.clientsample;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bss.soaif._interface.common.createorder.ReqHeader;
import com.huawei.bss.soaif._interface.orderservice.createorder.OrderInterfaces;
import com.huawei.bss.soaif._interface.orderservice.createorder.OrderServices;

public class CreateOrderService {

	private static OrderInterfaces orderInterfaces = null;

	private CreateOrderService() {
	}

	public static synchronized OrderInterfaces getInstance() {
		if (orderInterfaces == null) {
			orderInterfaces = new OrderServices().getOrderServicePort();
		//	SoapHandlerService.configureBinding(orderInterfaces);
			SoapHandlerService.configureBinding(orderInterfaces);
		}
		return orderInterfaces;
	}
	
	public static ReqHeader getCreateOderHeader()
	{
		ReqHeader reqHeader = new ReqHeader();
		reqHeader.setAccessPwd("r8q0a5WwGNboj9I35XzNcQ==");
		reqHeader.setChannelId("42");
		reqHeader.setAccessUser("dpc");
		reqHeader.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqHeader.setTenantId("101");
		return reqHeader;
	}
}
