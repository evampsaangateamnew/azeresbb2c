package com.evampsaanga.azerfon.getcdrsoperationhistory;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.evampsaanga.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CDRsOperationHistoryRequestResponse")
@XmlRootElement(name = "CDRsOperationHistoryRequestResponse")
public class CDRsOperationHistoryRequestResponse extends BaseResponse {
	private ArrayList<CDRsOperationDetails> records = new ArrayList<>();

	public ArrayList<CDRsOperationDetails> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<CDRsOperationDetails> records) {
		this.records = records;
	}

	public CDRsOperationHistoryRequestResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
}
