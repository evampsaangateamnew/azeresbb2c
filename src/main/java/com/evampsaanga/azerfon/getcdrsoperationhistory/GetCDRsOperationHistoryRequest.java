package com.evampsaanga.azerfon.getcdrsoperationhistory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.evampsaanga.azerfon.requestheaders.BaseRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "GetCDRsOperationHistoryRequest")
public class GetCDRsOperationHistoryRequest extends BaseRequest {
	@XmlElement(name = "startDate", required = true)
	String startDate = "";
	@XmlElement(name = "endDate", required = true)
	String endDate = "";
	@XmlElement(name = "accountId", required = true)
	String accountId = "";
	@XmlElement(name = "customerId", required = true)
	String customerId = "";

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public GetCDRsOperationHistoryRequest() {
	}
}
