package com.evampsaanga.azerfon.lostreport;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.OrderHandleService;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;

@Path("/azerfon")
public class ReportLostRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ReportLostResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		ReportLostResponseClient resp = new ReportLostResponseClient();
		Logs logs = new Logs();
		try {
			logger.info("Request Landed on Lost Report:" + requestBody);
			ReportLostRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, ReportLostRequestClient.class);
				// logger.info("<<<<<<<<<<< Request packet >>>>>>>>>>>" + cclient.toString());
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					if (cclient.getReasonCode().equals("1")) {
						SubmitOrderResponse response = reportlost(cclient.getmsisdn(), "SC999",
								cclient.getOperationType());
						if (response.getResponseHeader().getRetCode().equals("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						} else {
							resp.setReturnCode(response.getResponseHeader().getRetCode());
							resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						}
					}
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public SubmitOrderResponse reportlost(String msisdn, String reasoncode, String OperationType) {
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForReportLost());
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo order = new OrderInfo();
		order.setOrderType(Constants.RL_ORDER_ITEM);
		submitRequestBody.setOrder(order);
		OrderItems orderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemInfo ordertieminfo = new OrderItemInfo();
		ordertieminfo.setOrderItemType(Constants.RL_ORDER_ITEM_TYPE);
		ordertieminfo.setReasonCode(Constants.RL_REASON_CODE);
		ordertieminfo.setReasonType(Constants.RL_REASON_TYPE);
		OrderItemValue.setOrderItemInfo(ordertieminfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn);
		subscriber.setOperateType(Constants.RL_OPERATOR_TYPE);
		OrderItemValue.setSubscriber(subscriber);
		orderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		SubmitOrderResponse response = OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
		return response;
	}
}
