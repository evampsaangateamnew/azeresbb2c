package com.evampsaanga.azerfon.sendemail;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class SendEmailRequest extends BaseRequest {
	String text="";
	String from="";
	String subject="";
	String to="";
	String isB2B="";

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	

}
