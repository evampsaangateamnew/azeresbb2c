package com.evampsaanga.azerfon.queryBalancePICV2;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class QueryBalancePicRequest extends BaseRequest{
	private String customerID;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

}
