package com.evampsaanga.azerfon.queryBalancePICV2;

public class QueryBalancePicResponseData {
	private String total_limit;
	private String available_credit;
	private String outstanding_debt;
	private String unbilled_balance;
	private String balanceCredit;

	public String getTotal_limit() {
		return total_limit;
	}

	public void setTotal_limit(String total_limit) {
		this.total_limit = total_limit;
	}

	public String getAvailable_credit() {
		return available_credit;
	}

	public void setAvailable_credit(String available_credit) {
		this.available_credit = available_credit;
	}

	public String getOutstanding_debt() {
		return outstanding_debt;
	}

	public void setOutstanding_debt(String outstanding_debt) {
		this.outstanding_debt = outstanding_debt;
	}

	public String getUnbilled_balance() {
		return unbilled_balance;
	}

	public void setUnbilled_balance(String unbilled_balance) {
		this.unbilled_balance = unbilled_balance;
	}

	public String getBalanceCredit() {
		return balanceCredit;
	}

	public void setBalanceCredit(String balanceCredit) {
		this.balanceCredit = balanceCredit;
	}

}
