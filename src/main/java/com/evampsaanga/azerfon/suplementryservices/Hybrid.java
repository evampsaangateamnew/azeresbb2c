package com.evampsaanga.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hybrid {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("offers")
	private Offers[] offers;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("filters")
	private InternetFilters filters = null;

	public Offers[] getOffers() {
		return offers;
	}

	public void setOffers(Offers[] offers) {
		this.offers = offers;
	}

	public InternetFilters getFilters() {
		return filters;
	}

	public void setFilters(InternetFilters filters) {
		this.filters = filters;
	}
}
