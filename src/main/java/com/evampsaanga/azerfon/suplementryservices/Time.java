package com.evampsaanga.azerfon.suplementryservices;

public class Time {
	private String toTimeValue;
	private String fromTimeValue;
	private String fromTimeLabel;
	private String toTimeLabel;
	private String timeDescription;
	private String fragmentIcon;
	
	   public String getFragmentIcon() {
			return fragmentIcon;
		}

		public void setFragmentIcon(String fragmentIcon) {
			this.fragmentIcon = fragmentIcon;
		}


	public String getToTimeValue() {
		return toTimeValue;
	}

	public void setToTimeValue(String toTimeValue) {
		this.toTimeValue = toTimeValue;
	}

	public String getFromTimeValue() {
		return fromTimeValue;
	}

	public void setFromTimeValue(String fromTimeValue) {
		this.fromTimeValue = fromTimeValue;
	}

	public String getFromTimeLabel() {
		return fromTimeLabel;
	}

	public void setFromTimeLabel(String fromTimeLabel) {
		this.fromTimeLabel = fromTimeLabel;
	}

	public String getToTimeLabel() {
		return toTimeLabel;
	}

	public void setToTimeLabel(String toTimeLabel) {
		this.toTimeLabel = toTimeLabel;
	}

	public String getTimeDescription() {
		return timeDescription;
	}

	public void setTimeDescription(String timeDescription) {
		this.timeDescription = timeDescription;
	}
}
