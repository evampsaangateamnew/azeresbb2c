package com.evampsaanga.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Header {
	
	    private String freeResourceFValue;

	    private String whatsappIcon;

	    private String internetDesc;

	    private String type;

	    private String whatsappUnit;

	    private String whatsappLable;

	    private String internetDestinationValue2;

	    private String freeResourceJUnit;

	    private String price;

	    private String internetDestinationValue1;

	    private String offerLevel;

	    private String id;

	    private String freeResourceGValue;

	    private String internetIcon;

	    private String freeResourceJDestinationLabel2;

	    private String smsLable;

	    private String freeResourceJDestinationLabel1;

	    private String internetValue;

	    private String visibleFor;

	    private String whatsappValue;

	    private String freeResourceHDestinationValue1;

	    private String freeResourceEDesc;

	    private String freeResourceHDestinationValue2;

	    private String freeResourceEIcon;

	    private String freeResourceEUnit;

	    private String sortOrder;

	    private String freeResourceGDestinationLabel2;

	    private String freeResourceGDestinationLabel1;

	    private String internetLabel;

	    private String freeResourceELable;

	    private String hideFor;

	    private String freeResourceHDestinationLabel1;

	    private String smsDestinationValue1;

	    private String smsDestinationValue2;

	    private String freeResourceGUnit;

	    private String callIcon;

	    private String freeResourceHDestinationLabel2;

	    private String smsDestinationLabel2;

	    private String freeResourceFDestinationValue1;

	    private String smsDestinationLabel1;

	    private String freeResourceFDestinationValue2;

	    private String canSubscribe;

	    private String freeResourceGDesc;

	    private String freeResourceGIcon;

	    private String freeResourceEDestinationValue1;

	    private String whatsappDesc;

	    private String freeResourceEDestinationValue2;

	    private String callDestinationValue2;

	    private String callDestinationValue1;

	    private String freeResourceIDestinationValue1;

	    private String freeResourceILable;

	    private String freeResourceIUnit;

	    private String freeResourceIDestinationValue2;

	    private String freeResourceEDestinationLabel2;

	    private String freeResourceEDestinationLabel1;

	    private String freeResourceFLable;

	    private String freeResourceIIcon;

	    private String preReqOfferId;

	    private String smsDesc;

	    private String freeResourceHValue;

	    private String internetDestinationLabel2;

	    private String internetDestinationLabel1;

	    private String validityLabel;

	    private String freeResourceEValue;

	    private String offerGroupNameLabel;

	    private String whatsappDestinationLabel2;

	    private String validityInformation;

	    private String whatsappDestinationLabel1;

	    private String deactivate;

	    private String offerGroupNameValue;

	    private String whatsappDestinationValue1;

	    private String smsUnit;

	    private String whatsappDestinationValue2;

	    private String smsIcon;

	    private String callLable;

	    private String freeResourceJLable;

	    private String tag;

	    private String callDesc;

	    private String allowedForRenew;

	    private String sortOrderMS;

	    private String callValue;

	    private String callUnit;

	    private String validityDate;

	    private String name;

	    private String isTopUp;

	    private String freeResourceHLable;

	    private String freeResourceHDesc;

	    private String freeResourceFIcon;

	    private String freeResourceIValue;

	    private String smsValue;

	    private String callDestinationLabel1;

	    private String freeResourceGLable;

	    private String freeResourceFDestinationLabel2;

	    private String freeResourceIDestinationLabel1;

	    private String callDestinationLabel2;

	    private String freeResourceIDestinationLabel2;

	    private String freeResourceHIcon;

	    private String freeResourceJValue;

	    private String freeResourceFUnit;

	    private String freeResourceFDesc;

	    private String tagValidFromDate;

	    private String internetUnit;

	    private String tagValidToDate;

	    private String freeResourceFDestinationLabel1;

	    private String appOfferFilter;

	    private String offeringId;

	    private String freeResourceJDestinationValue1;

	    private String freeResourceJDestinationValue2;

	    private String freeResourceJIcon;

	    private String freeResourceHUnit;

	    private String renew;

	    private String freeResourceGDestinationValue1;

	    private String freeResourceGDestinationValue2;
		private String freeResourceJDesc;
	    private String freeResourceIDesc;
	    
	    public String getFreeResourceJDesc() {
			return freeResourceJDesc;
		}

		public void setFreeResourceJDesc(String freeResourceJDesc) {
			this.freeResourceJDesc = freeResourceJDesc;
		}

		public String getFreeResourceIDesc() {
			return freeResourceIDesc;
		}

		public void setFreeResourceIDesc(String freeResourceIDesc) {
			this.freeResourceIDesc = freeResourceIDesc;
		}

	

	    public String getFreeResourceFValue ()
	    {
	        return freeResourceFValue;
	    }

	    public void setFreeResourceFValue (String freeResourceFValue)
	    {
	        this.freeResourceFValue = freeResourceFValue;
	    }

	    public String getWhatsappIcon ()
	    {
	        return whatsappIcon;
	    }

	    public void setWhatsappIcon (String whatsappIcon)
	    {
	        this.whatsappIcon = whatsappIcon;
	    }

	    public String getInternetDesc ()
	    {
	        return internetDesc;
	    }

	    public void setInternetDesc (String internetDesc)
	    {
	        this.internetDesc = internetDesc;
	    }

	    public String getType ()
	    {
	        return type;
	    }

	    public void setType (String type)
	    {
	        this.type = type;
	    }

	    public String getWhatsappUnit ()
	    {
	        return whatsappUnit;
	    }

	    public void setWhatsappUnit (String whatsappUnit)
	    {
	        this.whatsappUnit = whatsappUnit;
	    }

	    public String getWhatsappLable ()
	    {
	        return whatsappLable;
	    }

	    public void setWhatsappLable (String whatsappLable)
	    {
	        this.whatsappLable = whatsappLable;
	    }

	    public String getInternetDestinationValue2 ()
	    {
	        return internetDestinationValue2;
	    }

	    public void setInternetDestinationValue2 (String internetDestinationValue2)
	    {
	        this.internetDestinationValue2 = internetDestinationValue2;
	    }

	    public String getFreeResourceJUnit ()
	    {
	        return freeResourceJUnit;
	    }

	    public void setFreeResourceJUnit (String freeResourceJUnit)
	    {
	        this.freeResourceJUnit = freeResourceJUnit;
	    }

	    public String getPrice ()
	    {
	        return price;
	    }

	    public void setPrice (String price)
	    {
	        this.price = price;
	    }

	    public String getInternetDestinationValue1 ()
	    {
	        return internetDestinationValue1;
	    }

	    public void setInternetDestinationValue1 (String internetDestinationValue1)
	    {
	        this.internetDestinationValue1 = internetDestinationValue1;
	    }

	    public String getOfferLevel ()
	    {
	        return offerLevel;
	    }

	    public void setOfferLevel (String offerLevel)
	    {
	        this.offerLevel = offerLevel;
	    }

	    public String getId ()
	    {
	        return id;
	    }

	    public void setId (String id)
	    {
	        this.id = id;
	    }

	    public String getFreeResourceGValue ()
	    {
	        return freeResourceGValue;
	    }

	    public void setFreeResourceGValue (String freeResourceGValue)
	    {
	        this.freeResourceGValue = freeResourceGValue;
	    }

	    public String getInternetIcon ()
	    {
	        return internetIcon;
	    }

	    public void setInternetIcon (String internetIcon)
	    {
	        this.internetIcon = internetIcon;
	    }

	    public String getFreeResourceJDestinationLabel2 ()
	    {
	        return freeResourceJDestinationLabel2;
	    }

	    public void setFreeResourceJDestinationLabel2 (String freeResourceJDestinationLabel2)
	    {
	        this.freeResourceJDestinationLabel2 = freeResourceJDestinationLabel2;
	    }

	    public String getSmsLable ()
	    {
	        return smsLable;
	    }

	    public void setSmsLable (String smsLable)
	    {
	        this.smsLable = smsLable;
	    }

	    public String getFreeResourceJDestinationLabel1 ()
	    {
	        return freeResourceJDestinationLabel1;
	    }

	    public void setFreeResourceJDestinationLabel1 (String freeResourceJDestinationLabel1)
	    {
	        this.freeResourceJDestinationLabel1 = freeResourceJDestinationLabel1;
	    }

	    public String getInternetValue ()
	    {
	        return internetValue;
	    }

	    public void setInternetValue (String internetValue)
	    {
	        this.internetValue = internetValue;
	    }

	    public String getVisibleFor ()
	    {
	        return visibleFor;
	    }

	    public void setVisibleFor (String visibleFor)
	    {
	        this.visibleFor = visibleFor;
	    }

	    public String getWhatsappValue ()
	    {
	        return whatsappValue;
	    }

	    public void setWhatsappValue (String whatsappValue)
	    {
	        this.whatsappValue = whatsappValue;
	    }

	    public String getFreeResourceHDestinationValue1 ()
	    {
	        return freeResourceHDestinationValue1;
	    }

	    public void setFreeResourceHDestinationValue1 (String freeResourceHDestinationValue1)
	    {
	        this.freeResourceHDestinationValue1 = freeResourceHDestinationValue1;
	    }

	    public String getFreeResourceEDesc ()
	    {
	        return freeResourceEDesc;
	    }

	    public void setFreeResourceEDesc (String freeResourceEDesc)
	    {
	        this.freeResourceEDesc = freeResourceEDesc;
	    }

	    public String getFreeResourceHDestinationValue2 ()
	    {
	        return freeResourceHDestinationValue2;
	    }

	    public void setFreeResourceHDestinationValue2 (String freeResourceHDestinationValue2)
	    {
	        this.freeResourceHDestinationValue2 = freeResourceHDestinationValue2;
	    }

	    public String getFreeResourceEIcon ()
	    {
	        return freeResourceEIcon;
	    }

	    public void setFreeResourceEIcon (String freeResourceEIcon)
	    {
	        this.freeResourceEIcon = freeResourceEIcon;
	    }

	    public String getFreeResourceEUnit ()
	    {
	        return freeResourceEUnit;
	    }

	    public void setFreeResourceEUnit (String freeResourceEUnit)
	    {
	        this.freeResourceEUnit = freeResourceEUnit;
	    }

	    public String getSortOrder ()
	    {
	        return sortOrder;
	    }

	    public void setSortOrder (String sortOrder)
	    {
	        this.sortOrder = sortOrder;
	    }

	    public String getFreeResourceGDestinationLabel2 ()
	    {
	        return freeResourceGDestinationLabel2;
	    }

	    public void setFreeResourceGDestinationLabel2 (String freeResourceGDestinationLabel2)
	    {
	        this.freeResourceGDestinationLabel2 = freeResourceGDestinationLabel2;
	    }

	    public String getFreeResourceGDestinationLabel1 ()
	    {
	        return freeResourceGDestinationLabel1;
	    }

	    public void setFreeResourceGDestinationLabel1 (String freeResourceGDestinationLabel1)
	    {
	        this.freeResourceGDestinationLabel1 = freeResourceGDestinationLabel1;
	    }

	    public String getInternetLabel ()
	    {
	        return internetLabel;
	    }

	    public void setInternetLabel (String internetLabel)
	    {
	        this.internetLabel = internetLabel;
	    }

	    public String getFreeResourceELable ()
	    {
	        return freeResourceELable;
	    }

	    public void setFreeResourceELable (String freeResourceELable)
	    {
	        this.freeResourceELable = freeResourceELable;
	    }

	    public String getHideFor ()
	    {
	        return hideFor;
	    }

	    public void setHideFor (String hideFor)
	    {
	        this.hideFor = hideFor;
	    }

	    public String getFreeResourceHDestinationLabel1 ()
	    {
	        return freeResourceHDestinationLabel1;
	    }

	    public void setFreeResourceHDestinationLabel1 (String freeResourceHDestinationLabel1)
	    {
	        this.freeResourceHDestinationLabel1 = freeResourceHDestinationLabel1;
	    }

	    public String getSmsDestinationValue1 ()
	    {
	        return smsDestinationValue1;
	    }

	    public void setSmsDestinationValue1 (String smsDestinationValue1)
	    {
	        this.smsDestinationValue1 = smsDestinationValue1;
	    }

	    public String getSmsDestinationValue2 ()
	    {
	        return smsDestinationValue2;
	    }

	    public void setSmsDestinationValue2 (String smsDestinationValue2)
	    {
	        this.smsDestinationValue2 = smsDestinationValue2;
	    }

	    public String getFreeResourceGUnit ()
	    {
	        return freeResourceGUnit;
	    }

	    public void setFreeResourceGUnit (String freeResourceGUnit)
	    {
	        this.freeResourceGUnit = freeResourceGUnit;
	    }

	    public String getCallIcon ()
	    {
	        return callIcon;
	    }

	    public void setCallIcon (String callIcon)
	    {
	        this.callIcon = callIcon;
	    }

	    public String getFreeResourceHDestinationLabel2 ()
	    {
	        return freeResourceHDestinationLabel2;
	    }

	    public void setFreeResourceHDestinationLabel2 (String freeResourceHDestinationLabel2)
	    {
	        this.freeResourceHDestinationLabel2 = freeResourceHDestinationLabel2;
	    }

	    public String getSmsDestinationLabel2 ()
	    {
	        return smsDestinationLabel2;
	    }

	    public void setSmsDestinationLabel2 (String smsDestinationLabel2)
	    {
	        this.smsDestinationLabel2 = smsDestinationLabel2;
	    }

	    public String getFreeResourceFDestinationValue1 ()
	    {
	        return freeResourceFDestinationValue1;
	    }

	    public void setFreeResourceFDestinationValue1 (String freeResourceFDestinationValue1)
	    {
	        this.freeResourceFDestinationValue1 = freeResourceFDestinationValue1;
	    }

	    public String getSmsDestinationLabel1 ()
	    {
	        return smsDestinationLabel1;
	    }

	    public void setSmsDestinationLabel1 (String smsDestinationLabel1)
	    {
	        this.smsDestinationLabel1 = smsDestinationLabel1;
	    }

	    public String getFreeResourceFDestinationValue2 ()
	    {
	        return freeResourceFDestinationValue2;
	    }

	    public void setFreeResourceFDestinationValue2 (String freeResourceFDestinationValue2)
	    {
	        this.freeResourceFDestinationValue2 = freeResourceFDestinationValue2;
	    }

	    public String getCanSubscribe ()
	    {
	        return canSubscribe;
	    }

	    public void setCanSubscribe (String canSubscribe)
	    {
	        this.canSubscribe = canSubscribe;
	    }

	    public String getFreeResourceGDesc ()
	    {
	        return freeResourceGDesc;
	    }

	    public void setFreeResourceGDesc (String freeResourceGDesc)
	    {
	        this.freeResourceGDesc = freeResourceGDesc;
	    }

	    public String getFreeResourceGIcon ()
	    {
	        return freeResourceGIcon;
	    }

	    public void setFreeResourceGIcon (String freeResourceGIcon)
	    {
	        this.freeResourceGIcon = freeResourceGIcon;
	    }

	    public String getFreeResourceEDestinationValue1 ()
	    {
	        return freeResourceEDestinationValue1;
	    }

	    public void setFreeResourceEDestinationValue1 (String freeResourceEDestinationValue1)
	    {
	        this.freeResourceEDestinationValue1 = freeResourceEDestinationValue1;
	    }

	    public String getWhatsappDesc ()
	    {
	        return whatsappDesc;
	    }

	    public void setWhatsappDesc (String whatsappDesc)
	    {
	        this.whatsappDesc = whatsappDesc;
	    }

	    public String getFreeResourceEDestinationValue2 ()
	    {
	        return freeResourceEDestinationValue2;
	    }

	    public void setFreeResourceEDestinationValue2 (String freeResourceEDestinationValue2)
	    {
	        this.freeResourceEDestinationValue2 = freeResourceEDestinationValue2;
	    }

	    public String getCallDestinationValue2 ()
	    {
	        return callDestinationValue2;
	    }

	    public void setCallDestinationValue2 (String callDestinationValue2)
	    {
	        this.callDestinationValue2 = callDestinationValue2;
	    }

	    public String getCallDestinationValue1 ()
	    {
	        return callDestinationValue1;
	    }

	    public void setCallDestinationValue1 (String callDestinationValue1)
	    {
	        this.callDestinationValue1 = callDestinationValue1;
	    }

	    public String getFreeResourceIDestinationValue1 ()
	    {
	        return freeResourceIDestinationValue1;
	    }

	    public void setFreeResourceIDestinationValue1 (String freeResourceIDestinationValue1)
	    {
	        this.freeResourceIDestinationValue1 = freeResourceIDestinationValue1;
	    }

	    public String getFreeResourceILable ()
	    {
	        return freeResourceILable;
	    }

	    public void setFreeResourceILable (String freeResourceILable)
	    {
	        this.freeResourceILable = freeResourceILable;
	    }

	    public String getFreeResourceIUnit ()
	    {
	        return freeResourceIUnit;
	    }

	    public void setFreeResourceIUnit (String freeResourceIUnit)
	    {
	        this.freeResourceIUnit = freeResourceIUnit;
	    }

	    public String getFreeResourceIDestinationValue2 ()
	    {
	        return freeResourceIDestinationValue2;
	    }

	    public void setFreeResourceIDestinationValue2 (String freeResourceIDestinationValue2)
	    {
	        this.freeResourceIDestinationValue2 = freeResourceIDestinationValue2;
	    }

	    public String getFreeResourceEDestinationLabel2 ()
	    {
	        return freeResourceEDestinationLabel2;
	    }

	    public void setFreeResourceEDestinationLabel2 (String freeResourceEDestinationLabel2)
	    {
	        this.freeResourceEDestinationLabel2 = freeResourceEDestinationLabel2;
	    }

	    public String getFreeResourceEDestinationLabel1 ()
	    {
	        return freeResourceEDestinationLabel1;
	    }

	    public void setFreeResourceEDestinationLabel1 (String freeResourceEDestinationLabel1)
	    {
	        this.freeResourceEDestinationLabel1 = freeResourceEDestinationLabel1;
	    }

	    public String getFreeResourceFLable ()
	    {
	        return freeResourceFLable;
	    }

	    public void setFreeResourceFLable (String freeResourceFLable)
	    {
	        this.freeResourceFLable = freeResourceFLable;
	    }

	    public String getFreeResourceIIcon ()
	    {
	        return freeResourceIIcon;
	    }

	    public void setFreeResourceIIcon (String freeResourceIIcon)
	    {
	        this.freeResourceIIcon = freeResourceIIcon;
	    }

	    public String getPreReqOfferId ()
	    {
	        return preReqOfferId;
	    }

	    public void setPreReqOfferId (String preReqOfferId)
	    {
	        this.preReqOfferId = preReqOfferId;
	    }

	    public String getSmsDesc ()
	    {
	        return smsDesc;
	    }

	    public void setSmsDesc (String smsDesc)
	    {
	        this.smsDesc = smsDesc;
	    }

	    public String getFreeResourceHValue ()
	    {
	        return freeResourceHValue;
	    }

	    public void setFreeResourceHValue (String freeResourceHValue)
	    {
	        this.freeResourceHValue = freeResourceHValue;
	    }

	    public String getInternetDestinationLabel2 ()
	    {
	        return internetDestinationLabel2;
	    }

	    public void setInternetDestinationLabel2 (String internetDestinationLabel2)
	    {
	        this.internetDestinationLabel2 = internetDestinationLabel2;
	    }

	    public String getInternetDestinationLabel1 ()
	    {
	        return internetDestinationLabel1;
	    }

	    public void setInternetDestinationLabel1 (String internetDestinationLabel1)
	    {
	        this.internetDestinationLabel1 = internetDestinationLabel1;
	    }

	    public String getValidityLabel ()
	    {
	        return validityLabel;
	    }

	    public void setValidityLabel (String validityLabel)
	    {
	        this.validityLabel = validityLabel;
	    }

	    public String getFreeResourceEValue ()
	    {
	        return freeResourceEValue;
	    }

	    public void setFreeResourceEValue (String freeResourceEValue)
	    {
	        this.freeResourceEValue = freeResourceEValue;
	    }

	    public String getOfferGroupNameLabel ()
	    {
	        return offerGroupNameLabel;
	    }

	    public void setOfferGroupNameLabel (String offerGroupNameLabel)
	    {
	        this.offerGroupNameLabel = offerGroupNameLabel;
	    }

	    public String getWhatsappDestinationLabel2 ()
	    {
	        return whatsappDestinationLabel2;
	    }

	    public void setWhatsappDestinationLabel2 (String whatsappDestinationLabel2)
	    {
	        this.whatsappDestinationLabel2 = whatsappDestinationLabel2;
	    }

	    public String getValidityInformation ()
	    {
	        return validityInformation;
	    }

	    public void setValidityInformation (String validityInformation)
	    {
	        this.validityInformation = validityInformation;
	    }

	    public String getWhatsappDestinationLabel1 ()
	    {
	        return whatsappDestinationLabel1;
	    }

	    public void setWhatsappDestinationLabel1 (String whatsappDestinationLabel1)
	    {
	        this.whatsappDestinationLabel1 = whatsappDestinationLabel1;
	    }

	    public String getDeactivate ()
	    {
	        return deactivate;
	    }

	    public void setDeactivate (String deactivate)
	    {
	        this.deactivate = deactivate;
	    }

	    public String getOfferGroupNameValue ()
	    {
	        return offerGroupNameValue;
	    }

	    public void setOfferGroupNameValue (String offerGroupNameValue)
	    {
	        this.offerGroupNameValue = offerGroupNameValue;
	    }

	    public String getWhatsappDestinationValue1 ()
	    {
	        return whatsappDestinationValue1;
	    }

	    public void setWhatsappDestinationValue1 (String whatsappDestinationValue1)
	    {
	        this.whatsappDestinationValue1 = whatsappDestinationValue1;
	    }

	    public String getSmsUnit ()
	    {
	        return smsUnit;
	    }

	    public void setSmsUnit (String smsUnit)
	    {
	        this.smsUnit = smsUnit;
	    }

	    public String getWhatsappDestinationValue2 ()
	    {
	        return whatsappDestinationValue2;
	    }

	    public void setWhatsappDestinationValue2 (String whatsappDestinationValue2)
	    {
	        this.whatsappDestinationValue2 = whatsappDestinationValue2;
	    }

	    public String getSmsIcon ()
	    {
	        return smsIcon;
	    }

	    public void setSmsIcon (String smsIcon)
	    {
	        this.smsIcon = smsIcon;
	    }

	    public String getCallLable ()
	    {
	        return callLable;
	    }

	    public void setCallLable (String callLable)
	    {
	        this.callLable = callLable;
	    }

	    public String getFreeResourceJLable ()
	    {
	        return freeResourceJLable;
	    }

	    public void setFreeResourceJLable (String freeResourceJLable)
	    {
	        this.freeResourceJLable = freeResourceJLable;
	    }

	    public String getTag ()
	    {
	        return tag;
	    }

	    public void setTag (String tag)
	    {
	        this.tag = tag;
	    }

	    public String getCallDesc ()
	    {
	        return callDesc;
	    }

	    public void setCallDesc (String callDesc)
	    {
	        this.callDesc = callDesc;
	    }

	    public String getAllowedForRenew ()
	    {
	        return allowedForRenew;
	    }

	    public void setAllowedForRenew (String allowedForRenew)
	    {
	        this.allowedForRenew = allowedForRenew;
	    }

	    public String getSortOrderMS ()
	    {
	        return sortOrderMS;
	    }

	    public void setSortOrderMS (String sortOrderMS)
	    {
	        this.sortOrderMS = sortOrderMS;
	    }

	    public String getCallValue ()
	    {
	        return callValue;
	    }

	    public void setCallValue (String callValue)
	    {
	        this.callValue = callValue;
	    }

	    public String getCallUnit ()
	    {
	        return callUnit;
	    }

	    public void setCallUnit (String callUnit)
	    {
	        this.callUnit = callUnit;
	    }

	    public String getValidityDate ()
	    {
	        return validityDate;
	    }

	    public void setValidityDate (String validityDate)
	    {
	        this.validityDate = validityDate;
	    }

	    public String getName ()
	    {
	        return name;
	    }

	    public void setName (String name)
	    {
	        this.name = name;
	    }

	    public String getIsTopUp ()
	    {
	        return isTopUp;
	    }

	    public void setIsTopUp (String isTopUp)
	    {
	        this.isTopUp = isTopUp;
	    }

	    public String getFreeResourceHLable ()
	    {
	        return freeResourceHLable;
	    }

	    public void setFreeResourceHLable (String freeResourceHLable)
	    {
	        this.freeResourceHLable = freeResourceHLable;
	    }

	    public String getFreeResourceHDesc ()
	    {
	        return freeResourceHDesc;
	    }

	    public void setFreeResourceHDesc (String freeResourceHDesc)
	    {
	        this.freeResourceHDesc = freeResourceHDesc;
	    }

	    public String getFreeResourceFIcon ()
	    {
	        return freeResourceFIcon;
	    }

	    public void setFreeResourceFIcon (String freeResourceFIcon)
	    {
	        this.freeResourceFIcon = freeResourceFIcon;
	    }

	    public String getFreeResourceIValue ()
	    {
	        return freeResourceIValue;
	    }

	    public void setFreeResourceIValue (String freeResourceIValue)
	    {
	        this.freeResourceIValue = freeResourceIValue;
	    }

	    public String getSmsValue ()
	    {
	        return smsValue;
	    }

	    public void setSmsValue (String smsValue)
	    {
	        this.smsValue = smsValue;
	    }

	    public String getCallDestinationLabel1 ()
	    {
	        return callDestinationLabel1;
	    }

	    public void setCallDestinationLabel1 (String callDestinationLabel1)
	    {
	        this.callDestinationLabel1 = callDestinationLabel1;
	    }

	    public String getFreeResourceGLable ()
	    {
	        return freeResourceGLable;
	    }

	    public void setFreeResourceGLable (String freeResourceGLable)
	    {
	        this.freeResourceGLable = freeResourceGLable;
	    }

	    public String getFreeResourceFDestinationLabel2 ()
	    {
	        return freeResourceFDestinationLabel2;
	    }

	    public void setFreeResourceFDestinationLabel2 (String freeResourceFDestinationLabel2)
	    {
	        this.freeResourceFDestinationLabel2 = freeResourceFDestinationLabel2;
	    }

	    public String getFreeResourceIDestinationLabel1 ()
	    {
	        return freeResourceIDestinationLabel1;
	    }

	    public void setFreeResourceIDestinationLabel1 (String freeResourceIDestinationLabel1)
	    {
	        this.freeResourceIDestinationLabel1 = freeResourceIDestinationLabel1;
	    }

	    public String getCallDestinationLabel2 ()
	    {
	        return callDestinationLabel2;
	    }

	    public void setCallDestinationLabel2 (String callDestinationLabel2)
	    {
	        this.callDestinationLabel2 = callDestinationLabel2;
	    }

	    public String getFreeResourceIDestinationLabel2 ()
	    {
	        return freeResourceIDestinationLabel2;
	    }

	    public void setFreeResourceIDestinationLabel2 (String freeResourceIDestinationLabel2)
	    {
	        this.freeResourceIDestinationLabel2 = freeResourceIDestinationLabel2;
	    }

	    public String getFreeResourceHIcon ()
	    {
	        return freeResourceHIcon;
	    }

	    public void setFreeResourceHIcon (String freeResourceHIcon)
	    {
	        this.freeResourceHIcon = freeResourceHIcon;
	    }

	    public String getFreeResourceJValue ()
	    {
	        return freeResourceJValue;
	    }

	    public void setFreeResourceJValue (String freeResourceJValue)
	    {
	        this.freeResourceJValue = freeResourceJValue;
	    }

	    public String getFreeResourceFUnit ()
	    {
	        return freeResourceFUnit;
	    }

	    public void setFreeResourceFUnit (String freeResourceFUnit)
	    {
	        this.freeResourceFUnit = freeResourceFUnit;
	    }

	    public String getFreeResourceFDesc ()
	    {
	        return freeResourceFDesc;
	    }

	    public void setFreeResourceFDesc (String freeResourceFDesc)
	    {
	        this.freeResourceFDesc = freeResourceFDesc;
	    }

	    public String getTagValidFromDate ()
	    {
	        return tagValidFromDate;
	    }

	    public void setTagValidFromDate (String tagValidFromDate)
	    {
	        this.tagValidFromDate = tagValidFromDate;
	    }

	    public String getInternetUnit ()
	    {
	        return internetUnit;
	    }

	    public void setInternetUnit (String internetUnit)
	    {
	        this.internetUnit = internetUnit;
	    }

	    public String getTagValidToDate ()
	    {
	        return tagValidToDate;
	    }

	    public void setTagValidToDate (String tagValidToDate)
	    {
	        this.tagValidToDate = tagValidToDate;
	    }

	    public String getFreeResourceFDestinationLabel1 ()
	    {
	        return freeResourceFDestinationLabel1;
	    }

	    public void setFreeResourceFDestinationLabel1 (String freeResourceFDestinationLabel1)
	    {
	        this.freeResourceFDestinationLabel1 = freeResourceFDestinationLabel1;
	    }

	    public String getAppOfferFilter ()
	    {
	        return appOfferFilter;
	    }

	    public void setAppOfferFilter (String appOfferFilter)
	    {
	        this.appOfferFilter = appOfferFilter;
	    }

	    public String getOfferingId ()
	    {
	        return offeringId;
	    }

	    public void setOfferingId (String offeringId)
	    {
	        this.offeringId = offeringId;
	    }

	    public String getFreeResourceJDestinationValue1 ()
	    {
	        return freeResourceJDestinationValue1;
	    }

	    public void setFreeResourceJDestinationValue1 (String freeResourceJDestinationValue1)
	    {
	        this.freeResourceJDestinationValue1 = freeResourceJDestinationValue1;
	    }

	    public String getFreeResourceJDestinationValue2 ()
	    {
	        return freeResourceJDestinationValue2;
	    }

	    public void setFreeResourceJDestinationValue2 (String freeResourceJDestinationValue2)
	    {
	        this.freeResourceJDestinationValue2 = freeResourceJDestinationValue2;
	    }

	    public String getFreeResourceJIcon ()
	    {
	        return freeResourceJIcon;
	    }

	    public void setFreeResourceJIcon (String freeResourceJIcon)
	    {
	        this.freeResourceJIcon = freeResourceJIcon;
	    }

	    public String getFreeResourceHUnit ()
	    {
	        return freeResourceHUnit;
	    }

	    public void setFreeResourceHUnit (String freeResourceHUnit)
	    {
	        this.freeResourceHUnit = freeResourceHUnit;
	    }

	    public String getRenew ()
	    {
	        return renew;
	    }

	    public void setRenew (String renew)
	    {
	        this.renew = renew;
	    }

	    public String getFreeResourceGDestinationValue1 ()
	    {
	        return freeResourceGDestinationValue1;
	    }

	    public void setFreeResourceGDestinationValue1 (String freeResourceGDestinationValue1)
	    {
	        this.freeResourceGDestinationValue1 = freeResourceGDestinationValue1;
	    }

	    public String getFreeResourceGDestinationValue2 ()
	    {
	        return freeResourceGDestinationValue2;
	    }

	    public void setFreeResourceGDestinationValue2 (String freeResourceGDestinationValue2)
	    {
	        this.freeResourceGDestinationValue2 = freeResourceGDestinationValue2;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [freeResourceFValue = "+freeResourceFValue+", whatsappIcon = "+whatsappIcon+", internetDesc = "+internetDesc+", type = "+type+", whatsappUnit = "+whatsappUnit+", whatsappLable = "+whatsappLable+", internetDestinationValue2 = "+internetDestinationValue2+", freeResourceJUnit = "+freeResourceJUnit+", price = "+price+", internetDestinationValue1 = "+internetDestinationValue1+", offerLevel = "+offerLevel+", id = "+id+", freeResourceGValue = "+freeResourceGValue+", internetIcon = "+internetIcon+", freeResourceJDestinationLabel2 = "+freeResourceJDestinationLabel2+", smsLable = "+smsLable+", freeResourceJDestinationLabel1 = "+freeResourceJDestinationLabel1+", internetValue = "+internetValue+", visibleFor = "+visibleFor+", whatsappValue = "+whatsappValue+", freeResourceHDestinationValue1 = "+freeResourceHDestinationValue1+", freeResourceEDesc = "+freeResourceEDesc+", freeResourceHDestinationValue2 = "+freeResourceHDestinationValue2+", freeResourceEIcon = "+freeResourceEIcon+", freeResourceEUnit = "+freeResourceEUnit+", sortOrder = "+sortOrder+", freeResourceGDestinationLabel2 = "+freeResourceGDestinationLabel2+", freeResourceGDestinationLabel1 = "+freeResourceGDestinationLabel1+", internetLabel = "+internetLabel+", freeResourceELable = "+freeResourceELable+", hideFor = "+hideFor+", freeResourceHDestinationLabel1 = "+freeResourceHDestinationLabel1+", smsDestinationValue1 = "+smsDestinationValue1+", smsDestinationValue2 = "+smsDestinationValue2+", freeResourceGUnit = "+freeResourceGUnit+", callIcon = "+callIcon+", freeResourceHDestinationLabel2 = "+freeResourceHDestinationLabel2+", smsDestinationLabel2 = "+smsDestinationLabel2+", freeResourceFDestinationValue1 = "+freeResourceFDestinationValue1+", smsDestinationLabel1 = "+smsDestinationLabel1+", freeResourceFDestinationValue2 = "+freeResourceFDestinationValue2+", canSubscribe = "+canSubscribe+", freeResourceGDesc = "+freeResourceGDesc+", freeResourceGIcon = "+freeResourceGIcon+", freeResourceEDestinationValue1 = "+freeResourceEDestinationValue1+", whatsappDesc = "+whatsappDesc+", freeResourceEDestinationValue2 = "+freeResourceEDestinationValue2+", callDestinationValue2 = "+callDestinationValue2+", callDestinationValue1 = "+callDestinationValue1+", freeResourceIDestinationValue1 = "+freeResourceIDestinationValue1+", freeResourceILable = "+freeResourceILable+", freeResourceIUnit = "+freeResourceIUnit+", freeResourceIDestinationValue2 = "+freeResourceIDestinationValue2+", freeResourceEDestinationLabel2 = "+freeResourceEDestinationLabel2+", freeResourceEDestinationLabel1 = "+freeResourceEDestinationLabel1+", freeResourceFLable = "+freeResourceFLable+", freeResourceIIcon = "+freeResourceIIcon+", preReqOfferId = "+preReqOfferId+", smsDesc = "+smsDesc+", freeResourceHValue = "+freeResourceHValue+", internetDestinationLabel2 = "+internetDestinationLabel2+", internetDestinationLabel1 = "+internetDestinationLabel1+", validityLabel = "+validityLabel+", freeResourceEValue = "+freeResourceEValue+", offerGroupNameLabel = "+offerGroupNameLabel+", whatsappDestinationLabel2 = "+whatsappDestinationLabel2+", validityInformation = "+validityInformation+", whatsappDestinationLabel1 = "+whatsappDestinationLabel1+", deactivate = "+deactivate+", offerGroupNameValue = "+offerGroupNameValue+", whatsappDestinationValue1 = "+whatsappDestinationValue1+", smsUnit = "+smsUnit+", whatsappDestinationValue2 = "+whatsappDestinationValue2+", smsIcon = "+smsIcon+", callLable = "+callLable+", freeResourceJLable = "+freeResourceJLable+", tag = "+tag+", callDesc = "+callDesc+", allowedForRenew = "+allowedForRenew+", sortOrderMS = "+sortOrderMS+", callValue = "+callValue+", callUnit = "+callUnit+", validityDate = "+validityDate+", name = "+name+", isTopUp = "+isTopUp+", freeResourceHLable = "+freeResourceHLable+", freeResourceHDesc = "+freeResourceHDesc+", freeResourceFIcon = "+freeResourceFIcon+", freeResourceIValue = "+freeResourceIValue+", smsValue = "+smsValue+", callDestinationLabel1 = "+callDestinationLabel1+", freeResourceGLable = "+freeResourceGLable+", freeResourceFDestinationLabel2 = "+freeResourceFDestinationLabel2+", freeResourceIDestinationLabel1 = "+freeResourceIDestinationLabel1+", callDestinationLabel2 = "+callDestinationLabel2+", freeResourceIDestinationLabel2 = "+freeResourceIDestinationLabel2+", freeResourceHIcon = "+freeResourceHIcon+", freeResourceJValue = "+freeResourceJValue+", freeResourceFUnit = "+freeResourceFUnit+", freeResourceFDesc = "+freeResourceFDesc+", tagValidFromDate = "+tagValidFromDate+", internetUnit = "+internetUnit+", tagValidToDate = "+tagValidToDate+", freeResourceFDestinationLabel1 = "+freeResourceFDestinationLabel1+", appOfferFilter = "+appOfferFilter+", offeringId = "+offeringId+", freeResourceJDestinationValue1 = "+freeResourceJDestinationValue1+", freeResourceJDestinationValue2 = "+freeResourceJDestinationValue2+", freeResourceJIcon = "+freeResourceJIcon+", freeResourceHUnit = "+freeResourceHUnit+", renew = "+renew+", freeResourceGDestinationValue1 = "+freeResourceGDestinationValue1+", freeResourceGDestinationValue2 = "+freeResourceGDestinationValue2+"]";
	    }
	}