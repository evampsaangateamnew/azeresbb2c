package com.evampsaanga.azerfon.suplementryservices.internet;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder({ "resultCode", "msg", "execTime", "data" })
public class InternetMagentoResponse {
	private InternetData data;
	private String resultCode;
	private String msg;
	private String execTime;

	public InternetData getData() {
		return data;
	}

	public void setData(InternetData data) {
		this.data = data;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getExecTime() {
		return execTime;
	}

	public void setExecTime(String execTime) {
		this.execTime = execTime;
	}

	@Override
	public String toString() {
		return "ClassPojo [data = " + data + ", resultCode = " + resultCode + ", msg = " + msg + ", execTime = "
				+ execTime + "]";
	}
}
