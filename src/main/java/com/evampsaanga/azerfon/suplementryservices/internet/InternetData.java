package com.evampsaanga.azerfon.suplementryservices.internet;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class InternetData {
	@JsonProperty("Daily")
	private Daily Daily = new Daily();
//	@JsonProperty("Weekly")
//	private Weekly Weekly = new Weekly();
	@JsonProperty("Monthly")
	private Monthly Monthly = new Monthly();
//	@JsonProperty("Hourly")
//	private Hourly Hourly= new Hourly();
	@JsonProperty("All packs")
	private All All= new All();

	@JsonProperty("Daily")
	public Daily getDaily() {
		return Daily;
	}

	@JsonProperty("Daily")
	public void setDaily(Daily daily) {
		Daily = daily;
	}

//	@JsonProperty("Weekly")
//	public Weekly getWeekly() {
//		return Weekly;
//	}
//
//	@JsonProperty("Weekly")
//	public void setWeekly(Weekly weekly) {
//		Weekly = weekly;
//	}

	@JsonProperty("Monthly")
	public Monthly getMonthly() {
		return Monthly;
	}

	@JsonProperty("Monthly")
	public void setMonthly(Monthly monthly) {
		Monthly = monthly;
	}

//	@JsonProperty("Hourly")
//	public Hourly getHourly() {
//		return Hourly;
//	}
//
//	@JsonProperty("Hourly")
//	public void setHourly(Hourly hourly) {
//		Hourly = hourly;
//	}

	@JsonProperty("All packs")
	public All getAll() {
		return All;
	}

	@JsonProperty("All packs")
	public void setAll(All all) {
		All = all;
	}
	
	
}
