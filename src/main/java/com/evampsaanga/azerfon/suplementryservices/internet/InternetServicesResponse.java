package com.evampsaanga.azerfon.suplementryservices.internet;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class InternetServicesResponse extends BaseResponse {
	@JsonInclude(JsonInclude.Include.ALWAYS)
	InternetData data = new InternetData();

	public InternetData getData() {
		return data;
	}

	public void setData(InternetData data) {
		this.data = data;
	}
	
}
