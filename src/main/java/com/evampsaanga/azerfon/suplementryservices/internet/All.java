package com.evampsaanga.azerfon.suplementryservices.internet;

import com.evampsaanga.azerfon.suplementryservices.InternetFilters;
import com.evampsaanga.azerfon.suplementryservices.Offers;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class All {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("offers")
	private Offers[] offers;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("filters")
	private InternetFilters filters = null;
}
