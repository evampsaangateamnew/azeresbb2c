package com.evampsaanga.azerfon.suplementryservices;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class SupplementryServicesResponse extends BaseResponse {
	@JsonInclude(JsonInclude.Include.ALWAYS)
	com.evampsaanga.azerfon.suplementryservices.Data data = new Data();

	public com.evampsaanga.azerfon.suplementryservices.Data getData() {
		return data;
	}

	public void setData(com.evampsaanga.azerfon.suplementryservices.Data data) {
		this.data = data;
	}
}
