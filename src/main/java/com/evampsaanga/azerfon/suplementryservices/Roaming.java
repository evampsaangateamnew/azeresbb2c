package com.evampsaanga.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Roaming {
	@JsonProperty("filters")
	private InternetFilters filters = null;
	@JsonProperty("offers")
	private Offers[] offers;

	public Offers[] getOffers() {
		return offers;
	}

	public void setOffers(Offers[] offers) {
		this.offers = offers;
	}

	public Countries[] getCountries() {
		return countries;
	}

	public void setCountries(Countries[] countries) {
		this.countries = countries;
	}
	@JsonProperty("countries")
	private Countries[] countries;
}