package com.evampsaanga.azerfon.getsubscriber;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Helper;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;

@Path("/bakcell")
public class GetSubscriberLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetSubscriberResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		GetSubscriberRequestClient cclient = null;
		try {
			try {
				cclient = Helper.JsonToObject(requestBody, GetSubscriberRequestClient.class);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetSubscriberResponseClient resp = new GetSubscriberResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				return resp;
			}
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		GetSubscriberResponse realResponse = new GetSubscriberResponse();
		GetSubscriberResponseClient response = new GetSubscriberResponseClient();
		com.evampsaanga.azerfon.getsubscriber.CRMSubscriberService instance = new com.evampsaanga.azerfon.getsubscriber.CRMSubscriberService();
		if (cclient != null)
		{
			realResponse = instance.GetSubscriberRequest(cclient.getmsisdn());
		}
		if (realResponse != null)
		{
			response.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			response.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			response.setSubscriberResponse(realResponse);
	
		}
		return response;
	}
}
