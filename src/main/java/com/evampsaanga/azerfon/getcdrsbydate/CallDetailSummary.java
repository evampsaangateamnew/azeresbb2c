package com.evampsaanga.azerfon.getcdrsbydate;

public class CallDetailSummary {
	private String service_type = "";
	private String unit = "";
	private String total_usage = "";
	private String chargeable_amount = "";
	private String count = "";

	public CallDetailSummary(String service_type, String unit, String total_usage, String chargeable_amount,
			String count) {
		super();
		this.service_type = service_type;
		this.unit = unit;
		this.total_usage = total_usage;
		this.chargeable_amount = chargeable_amount;
		this.count = count;
	}

	/**
	 * @return the service_type
	 */
	public String getService_type() {
		return service_type;
	}

	/**
	 * @param service_type
	 *            the service_type to set
	 */
	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit
	 *            the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @return the total_usage
	 */
	public String getTotal_usage() {
		return total_usage;
	}

	/**
	 * @param total_usage
	 *            the total_usage to set
	 */
	public void setTotal_usage(String total_usage) {
		this.total_usage = total_usage;
	}

	/**
	 * @return the chargeable_amount
	 */
	public String getChargeable_amount() {
		return chargeable_amount;
	}

	/**
	 * @param chargeable_amount
	 *            the chargeable_amount to set
	 */
	public void setChargeable_amount(String chargeable_amount) {
		this.chargeable_amount = chargeable_amount;
	}

	/**
	 * @return the count
	 */
	public String getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(String count) {
		this.count = count;
	}
}
