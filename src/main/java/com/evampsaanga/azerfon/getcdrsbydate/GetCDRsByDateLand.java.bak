package com.evampsaanga.bakcell.getcdrsbydate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.bakcell.db.DBBakcellFactory;
import com.evampsaanga.bakcell.utilities.ConversionUtilities;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;

@Path("/bakcell/")
public class GetCDRsByDateLand {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");
	
	public static HashMap<String, String> usageHistoryTranslationMapping = new HashMap<>();
	
	public static void populateUsageHistoryTranslationMapping()
	{
		Connection myConnection = DBBakcellFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String getAllTranslationQuery = "select * from E_CARE_ID_DESCRIPTIONS";
		try
		{
		statement = myConnection.prepareStatement(getAllTranslationQuery);
		resultSet = statement.executeQuery();
		while(resultSet.next())
			usageHistoryTranslationMapping.put(resultSet.getString("ID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+resultSet.getString("ID_LANG"), resultSet.getString("DESCRIPTION"));
		}catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		finally {
			try{
				if(resultSet != null)
					resultSet.close();
				if(statement != null)
					statement.close();
			}catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCDRsByDateRequestResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CDRS_BY_DATE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_CDRS_BY_DATE);
		logs.setTableType(LogsType.GetCdrsByDate);
		try {
			logger.info("Request Landed on GetCDRsByDateLand:" + requestBody);
			GetCDRsByDateRequestClient cclient = new GetCDRsByDateRequestClient();
			try {
				cclient = Helper.JsonToObject(requestBody, GetCDRsByDateRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			try {
				if (cclient != null) {
					if (!cclient.getStartDate().isEmpty()) {
						new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getStartDate());
					} else {
						GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
					resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
					resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} catch (Exception ex) {
				GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			try {
				new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getEndDate());
			} catch (Exception ex) {
				GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			try {
				Date datestart = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getStartDate());
				Date dateend = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getEndDate());
				new SimpleDateFormat("yyyy-mm-dd").parse(cclient.getEndDate());
				if (datestart.compareTo(dateend) > 0) {
					GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
					resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
					resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} catch (Exception e) {
				logger.error(
						"Date Comparison Exceptiom Please provide valid start date before end date" + e.getMessage());
			}
			if (cclient.getAccountId().equals("")) {
				GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetCDRsByDateRequestResponse res = new GetCDRsByDateRequestResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					String lang = "2002";
					if(cclient.getLang().equalsIgnoreCase("3"))
						lang = "2002";
					if(cclient.getLang().equalsIgnoreCase("2"))
						lang = "2052";
					if(cclient.getLang().equalsIgnoreCase("4"))
						lang = "2060";
					return getResponse(cclient.getStartDate(), cclient.getEndDate(), cclient.getmsisdn(),
							cclient.getAccountId(), logs, cclient.getCustomerId(),lang);
				} else {
					GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public CustomerID getCustomerIDFromView(String msisdn) {
		CustomerID customerId = new CustomerID();
		Connection myConnection = DBBakcellFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			String sql = "select * from V_E_CARE_CUST_INFO t where PRI_IDENTITY = substr('" + msisdn + "',-9)";
			logger.info(sql);
			statement = myConnection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				customerId.setCbscustomerId(resultSet.getString("CBS_CUST_ID"));
				customerId.setCrmcustomerId(resultSet.getString("CRM_CUST_ID"));
			}
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		return customerId;
	}

	public GetCDRsByDateRequestResponse getResponse(String startDate, String endDate, String msisdn, String accountId,
			Logs logs, String customerIdAPI,String lang) {
		CustomerID customerId = getCustomerIDFromView(msisdn);
		Connection myConnection = DBBakcellFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			ArrayList<CDRDetails> list = new ArrayList<>();
			if (customerId.getCrmcustomerId().equalsIgnoreCase(customerIdAPI)) {
				logger.info("MSISDN-" + msisdn + "-CustomerID From DB-" + customerId.getCrmcustomerId()
						+ "-CustomerID From API-" + customerIdAPI + "-ID matched.");
				String sql = "select * from e_care_dev.v_e_care_cdr where e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE >= to_date("
						+ "'" + startDate + " 00:00:00','" + "yyyy-mm-dd hh24:mi:ss"
						+ "') and e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE <=" + " to_date(" + "'" + endDate
						+ " 23:59:59','" + "yyyy-mm-dd hh24:mi:ss" + "') and e_care_dev.v_e_care_cdr.PRI_IDENTITY='"
						+ Constants.AZERI_COUNTRY_CODE + msisdn + "' and  e_care_dev.v_e_care_cdr.OWNER_CUST_ID='"
						+ customerId.getCbscustomerId() + "'" + " order by CUST_LOCAL_START_DATE desc";
				logger.info(sql);
				statement = myConnection.prepareStatement(sql);
				resultSet = statement.executeQuery();
				if(usageHistoryTranslationMapping.isEmpty())
					populateUsageHistoryTranslationMapping();
				while (resultSet.next()) {
					Date formattedStarDate = new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
							.parse(resultSet.getString("CUST_LOCAL_START_DATE"));
					Date formattedEndDate = new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
							.parse(resultSet.getString("CUST_LOCAL_END_DATE"));
					String service = "";
					if(usageHistoryTranslationMapping.containsKey(resultSet.getString("USG_SERV_TYPE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang))
						service = usageHistoryTranslationMapping.get(resultSet.getString("USG_SERV_TYPE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang);
					else
						service = resultSet.getString("USG_SERV_TYPE_NAME");
					
					logger.info("RECIPIENT_NUMBER: "+resultSet.getString("RECIPIENT_NUMBER"));
					logger.info("CUST_LOCAL_START_DATE: "+resultSet.getString("CUST_LOCAL_START_DATE"));
					logger.info("CUST_LOCAL_END_DATE: "+resultSet.getString("CUST_LOCAL_END_DATE"));
					logger.info("USG_SERV_TYPE_NAME: "+resultSet.getString("USG_SERV_TYPE_NAME"));
					logger.info("TIME_TYPE: "+resultSet.getString("TIME_TYPE"));
					logger.info("CARRIER_NAME: "+resultSet.getString("CARRIER_NAME"));
					logger.info("ACTUAL_USAGE: "+resultSet.getString("ACTUAL_USAGE"));
					logger.info("MEASURE_NAME: "+resultSet.getString("MEASURE_NAME"));
					logger.info("USG_SERV_TYPE_NAME: "+resultSet.getString("USG_SERV_TYPE_NAME"));
					logger.info("SPECIALZONEID: "+resultSet.getString("SPECIALZONEID"));
					
					list.add(new CDRDetails(resultSet.getString("RECIPIENT_NUMBER"),
							new SimpleDateFormat(Constants.SQL_DATE_FORMAT_History).format(formattedStarDate),
							new SimpleDateFormat(Constants.SQL_DATE_FORMAT_History).format(formattedEndDate),
							service, 
							usageHistoryTranslationMapping.get(resultSet.getString("TIME_TYPE")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang),
							resultSet.getString("CARRIER_NAME"),
							resultSet.getString("ACTUAL_USAGE") + " " + usageHistoryTranslationMapping.get(resultSet.getString("MEASURE_NAME")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang),
							ConversionUtilities.numberFormattor(resultSet.getString("CHARGEABLE_AMOUNT")),
							resultSet.getString("USG_SERV_TYPE_NAME"), 
							usageHistoryTranslationMapping.get(resultSet.getString("SPECIALZONEID")+Constants.TRANSLATION_CDRS_KEY_SEPARATOR+lang)));
				}
			} else
				logger.error("MSISDN-" + msisdn + "-CustomerID From DB-" + customerId.getCrmcustomerId()
						+ "-CustomerID From API-" + customerIdAPI + "-ID not matched.");
			
			String voiceList[] = ConfigurationManager.getCDRSumProperties("VOICE").split(",");
			String smsList[] = ConfigurationManager.getCDRSumProperties("SMS").split(",");
			String dataList[] = ConfigurationManager.getCDRSumProperties("DATA").split(",");
			for(int i=0;i<list.size();i++)
			{
				for(int j=0;j<voiceList.length;j++)
					if(list.get(i).getType() != null && list.get(i).getType().equalsIgnoreCase(voiceList[j]))
						list.get(i).setType("VOICE");
			
				for(int j=0;j<smsList.length;j++)
					if(list.get(i).getType() != null && list.get(i).getType().equalsIgnoreCase(smsList[j]))
						list.get(i).setType("SMS");
			
				for(int j=0;j<dataList.length;j++)
					if(list.get(i).getType() != null && list.get(i).getType().equalsIgnoreCase(dataList[j]))
						list.get(i).setType("DATA");
				if(list.get(i).getType() == null || ( !list.get(i).getType().equalsIgnoreCase("VOICE") && !list.get(i).getType().equalsIgnoreCase("SMS") && !list.get(i).getType().equalsIgnoreCase("DATA")))
					list.get(i).setType("OTHERS");
			}
			
			GetCDRsByDateRequestResponse rep11 = new GetCDRsByDateRequestResponse();
			rep11.setRecords(list);
			rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			logs.setResponseCode(rep11.getReturnCode());
			logs.setResponseDescription(rep11.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return rep11;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (Exception ex2) {
				logger.error(Helper.GetException(ex2));
			}
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (Exception ex2) {
				logger.error(Helper.GetException(ex2));
			}
			try {
				myConnection.close();
			} catch (SQLException ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		GetCDRsByDateRequestResponse resp = new GetCDRsByDateRequestResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		return resp;
	}
}
