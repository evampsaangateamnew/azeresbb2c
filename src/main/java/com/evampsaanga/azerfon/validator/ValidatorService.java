package com.evampsaanga.azerfon.validator;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.azerfon.requestheaders.BaseRequest;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.gethomepage.GetHomePageRequestClient;
import com.evampsaanga.gethomepage.GetHomePageResponse;

public class ValidatorService {

	public ResponseValidator processValidation(BaseRequest requestValidator, String credential, String token) {
		Constants.logger.info(token + "-Validating request");
		ResponseValidator resp = new ResponseValidator();

		if (requestValidator != null) {
			Constants.logger.info(token + "-Checking request is not null SUCCESS");
			String validationCredentials = null;
			try {
				validationCredentials = Decrypter.getInstance().decrypt(credential);
				if ((validationCredentials != null && validationCredentials.equals(Constants.CREDENTIALS))
						&& credential != null) {
					Constants.logger.info(token + "-Checking Credentials is not null and validated with saved value");

					// positive check
					if (requestValidator.getmsisdn() != null && !requestValidator.getmsisdn().isEmpty()) {
						Constants.logger.info(token + "-Checking MSISDN not null and Empty");
						String verification = Helper.validateRequest(requestValidator);
						if (!verification.equals("")) {
							Constants.logger.info(token + "Checking Verification Successfull");
							ResponseValidator res = new ResponseValidator();
							res.setResponseCode(ResponseCodes.ERROR_400);
							res.setResponseDescription(verification);

						}
						resp.setResponseCode("00");
						resp.setResponseDescription("SUCCESS");
//						else
//						{
//							resp.setResponseCode(ResponseCodes.ERROR_MSISDN_CODE);
//							resp.setResponseDescription(ResponseCodes.ERROR_MSISDN);
//						}
					} else {

						resp.setResponseCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					}

				} else {
					resp.setResponseCode(ResponseCodes.ERROR_401_CODE);
					resp.setResponseDescription(ResponseCodes.ERROR_401);
				}

			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));

				resp.setResponseCode(ResponseCodes.ERROR_401_CODE);
				resp.setResponseDescription(ResponseCodes.ERROR_401);
			}

		} else {
			resp.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		}
		Constants.logger.info(token + "-Validation Module Response Code" + resp.getResponseCode());
		Constants.logger.info(token + "-Validation Module Response Description" + resp.getResponseDescription());
		return resp;
	}

	public static void main(String[] args) {

		String a = "{\"msisdn\":\"\",\"iP\":\"127.0.0.1\",\"channel\":\"web\",\"lang\":\"3\",\"reportStartDate\":\"20181101\",\"reportEndDate\":\"20181130\"}";
		RequestValidator requestValidator = new RequestValidator();

		requestValidator.setChannel("");
		requestValidator.setiP("");
		requestValidator.setIsB2B("");
		requestValidator.setLang("");
		requestValidator.setMsisdn("");
		ValidatorService validatorService = new ValidatorService();

		ResponseValidator responseValidator = validatorService.processValidation(requestValidator,
				"RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir", "");

		if (a != null && !a.isEmpty()) {
			System.out.println("Hi");
		} else {
			System.out.println("Bye");
		}
	}
}
