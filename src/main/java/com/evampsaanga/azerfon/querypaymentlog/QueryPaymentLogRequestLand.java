package com.evampsaanga.azerfon.querypaymentlog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.CBSARService;
import com.huawei.bme.cbsinterface.arservices.QueryPaymentLogRequest;
import com.huawei.bme.cbsinterface.arservices.QueryPaymentLogRequest.AcctAccessCode;
import com.huawei.bme.cbsinterface.arservices.QueryPaymentLogRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryPaymentLogResultMsg;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;

@Path("/bakcell/")
public class QueryPaymentLogRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QueryPaymentLogResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.PAYMENT_LOG);
		logs.setThirdPartyName(ThirdPartyNames.PAYMENT_LOG);
		logs.setTableType(LogsType.AppFaq);
		try {
			logger.info("Request Landed on QueryPaymentLogRequestLand:" + requestBody);
			QueryPaymentLogRequestClient cclient = new QueryPaymentLogRequestClient();
			try {
				cclient = Helper.JsonToObject(requestBody, QueryPaymentLogRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
				}
				
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null)
				try {
					if (!Helper.isValidFormat("yyyy-mm-dd", cclient.getStartDate())) {
						QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
						resp.setReturnMsg("start date is not in proper format");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);	return resp;
					}
				} catch (Exception ex) {
					QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
					resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
					resp.setReturnMsg("start date was improper");
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);	return resp;
				}
			try {
				if (cclient != null) {
					if (cclient.getEndDate().isEmpty()) {
						if (!Helper.isValidFormat("yyyy-mm-dd", cclient.getEndDate())) {
							QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
							resp.setReturnMsg("end date is not in proper format");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);		return resp;
						}
					} else {
						QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
						resp.setReturnMsg("end date is not in proper format");
						return resp;
					}
				} else {
					QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_400);
					return resp;
				}
			} catch (Exception ex) {
				QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg("end date was improper");
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						QueryPaymentLogResponseClient res = new QueryPaymentLogResponseClient();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						return res;
					}
				} else {
					QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
					QueryPaymentLogResultMsg querypaymentresponse = null;
					try {
						querypaymentresponse = queryPaymentLog(cclient);
						if (querypaymentresponse.getResultHeader().getResultCode().equals("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setQueryPaymentLog(querypaymentresponse.getQueryPaymentLogResult());
						} else {
							resp.setReturnCode(querypaymentresponse.getResultHeader().getResultCode());
							resp.setReturnMsg(querypaymentresponse.getResultHeader().getResultDesc());
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
					return resp;
				} else {
					QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		QueryPaymentLogResponseClient resp = new QueryPaymentLogResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		return resp;
	}

	public QueryPaymentLogResultMsg queryPaymentLog(QueryPaymentLogRequestClient cclient) {
		QueryPaymentLogRequestMsg queryPaymentLogRequestMsg = new QueryPaymentLogRequestMsg();
		queryPaymentLogRequestMsg.setRequestHeader(getLoanLogRequestHeader());
		QueryPaymentLogRequest queryPaymentLogRequest = new QueryPaymentLogRequest();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startdate = "";
		try {
			Date date = sdf.parse(cclient.getStartDate());
			startdate = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
		} catch (ParseException e) {
			logger.error(Helper.GetException(e));
		}
		String endDate = "";
		try {
			Date date = sdf.parse(cclient.getEndDate());
			endDate = new SimpleDateFormat("yyyyMMddHHmmss").format(date);// date.getTime();
		} catch (ParseException e) {
			logger.error(Helper.GetException(e));
		}
		AcctAccessCode code = new AcctAccessCode();
		code.setPrimaryIdentity(cclient.getmsisdn());
		queryPaymentLogRequest.setAcctAccessCode(code);
		queryPaymentLogRequest.setBeginRowNum(0);
		queryPaymentLogRequest.setFetchRowNum(100);
		queryPaymentLogRequest.setTotalRowNum(300);
		queryPaymentLogRequest.setStartTime(startdate);
		queryPaymentLogRequest.setEndTime(endDate);
		queryPaymentLogRequestMsg.setQueryPaymentLogRequest(queryPaymentLogRequest);
		return CBSARService.getInstance().queryPaymentLog(queryPaymentLogRequestMsg);
	}

	public com.huawei.bme.cbsinterface.cbscommon.RequestHeader getLoanLogRequestHeader() {
		com.huawei.bme.cbsinterface.cbscommon.RequestHeader loanRequestHeader = new com.huawei.bme.cbsinterface.cbscommon.RequestHeader();
		loanRequestHeader.setVersion(Constants.LL_VERSION);
		loanRequestHeader.setBusinessCode(Constants.LL_BUSINESS_CODE);
		OwnershipInfo ownershipinf = new OwnershipInfo();
		ownershipinf.setBEID(Constants.LL_BEID);
		ownershipinf.setBRID(Constants.LL_BRID);
		loanRequestHeader.setOwnershipInfo(ownershipinf);
		SecurityInfo value = new SecurityInfo();
		value.setLoginSystemCode(ConfigurationManager.getConfigurationFromCache("cbs.username"));
		value.setPassword(ConfigurationManager.getConfigurationFromCache("cbs.password"));
		loanRequestHeader.setAccessSecurity(value);
		OperatorInfo opInfo = new OperatorInfo();
		opInfo.setChannelID(Constants.LL_CHANNEL_ID);
		opInfo.setOperatorID(Constants.LL_OPERATOR_ID);
		loanRequestHeader.setOperatorInfo(opInfo);
		loanRequestHeader.setMessageSeq(Helper.generateTransactionID());
		return loanRequestHeader;
	}
}
