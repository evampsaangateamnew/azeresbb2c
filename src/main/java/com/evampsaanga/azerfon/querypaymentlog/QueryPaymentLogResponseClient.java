package com.evampsaanga.azerfon.querypaymentlog;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.huawei.bme.cbsinterface.arservices.QueryPaymentLogResult;

public class QueryPaymentLogResponseClient extends BaseResponse {
	QueryPaymentLogResult querypaymentlogresult = new QueryPaymentLogResult();

	public void setQueryPaymentLog(QueryPaymentLogResult queryPaymentLogResult) {
		querypaymentlogresult = queryPaymentLogResult;
	}
}
