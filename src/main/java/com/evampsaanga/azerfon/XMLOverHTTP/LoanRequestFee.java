package com.evampsaanga.azerfon.XMLOverHTTP;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;

import com.evampsaanga.configs.ConfigurationManager;

public class LoanRequestFee {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public String getLoanReqFeeResponse(String msisdn) throws JAXBException {
		ChargeRequest chargeRequest = new ChargeRequest();
		chargeRequest.setmSISDN(msisdn);
		chargeRequest.setTransactId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		java.io.StringWriter sw = new StringWriter();
		JAXBContext jaxbContext = JAXBContext.newInstance(ChargeRequest.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.marshal(chargeRequest, sw);
		logger.info(sw.toString());
		System.out.println(sw.toString());
		RestClient restClient = new RestClient();
		String response = restClient.getResponseFromESB(ConfigurationManager.getConfigurationFromCache("loan.req.fee.url"), sw.toString());
		logger.info(response);
		return response;
	}

	public static void main(String[] args) throws JAXBException {
		LoanRequestFee fee = new LoanRequestFee();
		fee.getLoanReqFeeResponse("558749947");
	}
}
