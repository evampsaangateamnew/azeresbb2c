package com.evampsaanga.azerfon.signupresendotp;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.saanga.magento.apiclient.RestClient;

@Path("/backcell")
public class SignUpResendOTPRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SignUpResponse Get(@Header("credentials") String credential, @Body() String requestBody) throws Exception {
		SignUpRequest cclient = null;
		SignUpResponse resp = new SignUpResponse();
		String token = "";
		String TrnsactionName = Transactions.RESEND_OTP_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "-Request Data-" + requestBody);
			cclient = Helper.JsonToObject(requestBody, SignUpRequest.class);
		} catch (Exception ex) {
			logger.info(token+Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.info(token+Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("msisdn", cclient.getmsisdn());
					jsonObject.put("storeId", cclient.getLang());
					jsonObject.put("requestPlatform", "signup-" + cclient.getChannel());
					String response = RestClient.SendCallToMagento(token,
							ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.resendOtp"),
							jsonObject.toString());
					try {
						MagentoResponseValidateMsisdn data = Helper.JsonToObject(response,
								MagentoResponseValidateMsisdn.class);
						if (data.getResultCode().equals("24")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setPinMsg(data.getMsg());
							return resp;
						} else {
							resp.setReturnCode(data.getResultCode());
							resp.setReturnMsg(data.getMsg());
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logger.info(token+Helper.GetException(ex));
						return resp;
					}
				} catch (JSONException ex) {
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logger.info(token+Helper.GetException(ex));
					return resp;
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logger.info(token+Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				return resp;
			}
		}
		return resp;
	}
}
