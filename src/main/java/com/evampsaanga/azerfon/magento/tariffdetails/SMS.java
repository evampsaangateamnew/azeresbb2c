
package com.evampsaanga.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "smsIcon",
    "smsLabel",
    "smsValue",
    "smsMetrics"
})
public class SMS {

    @JsonProperty("smsIcon")
    private String smsIcon;
    @JsonProperty("smsLabel")
    private String smsLabel;
    @JsonProperty("smsValue")
    private String smsValue;
    @JsonProperty("smsMetrics")
    private String smsMetrics;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("smsIcon")
    public String getSmsIcon() {
        return smsIcon;
    }

    @JsonProperty("smsIcon")
    public void setSmsIcon(String smsIcon) {
        this.smsIcon = smsIcon;
    }

    @JsonProperty("smsLabel")
    public String getSmsLabel() {
        return smsLabel;
    }

    @JsonProperty("smsLabel")
    public void setSmsLabel(String smsLabel) {
        this.smsLabel = smsLabel;
    }

    @JsonProperty("smsValue")
    public String getSmsValue() {
        return smsValue;
    }

    @JsonProperty("smsValue")
    public void setSmsValue(String smsValue) {
        this.smsValue = smsValue;
    }

    @JsonProperty("smsMetrics")
    public String getSmsMetrics() {
        return smsMetrics;
    }

    @JsonProperty("smsMetrics")
    public void setSmsMetrics(String smsMetrics) {
        this.smsMetrics = smsMetrics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
