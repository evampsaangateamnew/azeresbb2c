
package com.evampsaanga.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "advantagesLabel",
    "description",
    "advantagesIcon"
})
public class Advantages {

    @JsonProperty("advantagesLabel")
    private String advantagesLabel;
    @JsonProperty("advantagesIcon")
    private String advantagesIcon;
   
	@JsonProperty("description")
    private String description;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("advantagesLabel")
    public String getAdvantagesLabel() {
        return advantagesLabel;
    }

    @JsonProperty("advantagesLabel")
    public void setAdvantagesLabel(String advantagesLabel) {
        this.advantagesLabel = advantagesLabel;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("advantagesIcon")
    public String getAdvantagesIcon() {
		return advantagesIcon;
	}
    @JsonProperty("advantagesIcon")
	public void setAdvantagesIcon(String advantagesIcon) {
		this.advantagesIcon = advantagesIcon;
	}

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
