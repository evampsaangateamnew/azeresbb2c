
package com.evampsaanga.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "callIcon",
    "callLabel",
    "callValue",
    "callMetrics"
})
public class Call {

    @JsonProperty("callIcon")
    private String callIcon;
    @JsonProperty("callLabel")
    private String callLabel;
    @JsonProperty("callValue")
    private String callValue;
    @JsonProperty("callMetrics")
    private String callMetrics;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("callIcon")
    public String getCallIcon() {
        return callIcon;
    }

    @JsonProperty("callIcon")
    public void setCallIcon(String callIcon) {
        this.callIcon = callIcon;
    }

    @JsonProperty("callLabel")
    public String getCallLabel() {
        return callLabel;
    }

    @JsonProperty("callLabel")
    public void setCallLabel(String callLabel) {
        this.callLabel = callLabel;
    }

    @JsonProperty("callValue")
    public String getCallValue() {
        return callValue;
    }

    @JsonProperty("callValue")
    public void setCallValue(String callValue) {
        this.callValue = callValue;
    }

    @JsonProperty("callMetrics")
    public String getCallMetrics() {
        return callMetrics;
    }

    @JsonProperty("callMetrics")
    public void setCallMetrics(String callMetrics) {
        this.callMetrics = callMetrics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
