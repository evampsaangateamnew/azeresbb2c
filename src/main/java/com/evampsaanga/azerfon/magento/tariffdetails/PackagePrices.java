
package com.evampsaanga.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "packagePricesSectionLabel",
    "Call",
    "SMS",
    "Internet",
    "Call_Payg",
    "Sms_Payg",
    "Internet_Payg"
})
public class PackagePrices {

    @JsonProperty("packagePricesSectionLabel")
    private String packagePricesSectionLabel;
    @JsonProperty("Call")
    private Call_ call;
    @JsonProperty("SMS")
    private SMS_ sMS;
    @JsonProperty("Internet")
    private Internet_ internet;
    @JsonProperty("Call_Payg")
    private CallPayg callPayg;
    @JsonProperty("Sms_Payg")
    private SmsPayg smsPayg;
    @JsonProperty("Internet_Payg")
    private InternetPayg internetPayg;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("packagePricesSectionLabel")
    public String getPackagePricesSectionLabel() {
        return packagePricesSectionLabel;
    }

    @JsonProperty("packagePricesSectionLabel")
    public void setPackagePricesSectionLabel(String packagePricesSectionLabel) {
        this.packagePricesSectionLabel = packagePricesSectionLabel;
    }

    @JsonProperty("Call")
    public Call_ getCall() {
        return call;
    }

    @JsonProperty("Call")
    public void setCall(Call_ call) {
        this.call = call;
    }

    @JsonProperty("SMS")
    public SMS_ getSMS() {
        return sMS;
    }

    @JsonProperty("SMS")
    public void setSMS(SMS_ sMS) {
        this.sMS = sMS;
    }

    @JsonProperty("Internet")
    public Internet_ getInternet() {
        return internet;
    }

    @JsonProperty("Internet")
    public void setInternet(Internet_ internet) {
        this.internet = internet;
    }

    @JsonProperty("Call_Payg")
    public CallPayg getCallPayg() {
        return callPayg;
    }

    @JsonProperty("Call_Payg")
    public void setCallPayg(CallPayg callPayg) {
        this.callPayg = callPayg;
    }

    @JsonProperty("Sms_Payg")
    public SmsPayg getSmsPayg() {
        return smsPayg;
    }

    @JsonProperty("Sms_Payg")
    public void setSmsPayg(SmsPayg smsPayg) {
        this.smsPayg = smsPayg;
    }

    @JsonProperty("Internet_Payg")
    public InternetPayg getInternetPayg() {
        return internetPayg;
    }

    @JsonProperty("Internet_Payg")
    public void setInternetPayg(InternetPayg internetPayg) {
        this.internetPayg = internetPayg;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
