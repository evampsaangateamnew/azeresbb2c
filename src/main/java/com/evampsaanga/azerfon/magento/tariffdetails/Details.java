
package com.evampsaanga.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "detailSectionLabel",
    "Call",
    "fragmentB",
    "fragmentC",
    "fragmentD",
    "destination",
    "advantages",
    "titleSubtitle",
    "date",
    "time",
    "roaming",
    "freeResource"
})
public class Details {

    @JsonProperty("detailSectionLabel")
    private String detailSectionLabel;
    @JsonProperty("Call")
    private Call__ call;
    @JsonProperty("fragmentB")
    private FragmentB fragmentB;
    @JsonProperty("fragmentC")
    private FragmentC fragmentC;
    @JsonProperty("fragmentD")
    private FragmentD fragmentD;
    @JsonProperty("destination")
    private Destination destination;
    @JsonProperty("advantages")
    private Advantages advantages;
    @JsonProperty("titleSubtitle")
    private TitleSubtitle titleSubtitle;
    @JsonProperty("date")
    private Date date;
    @JsonProperty("time")
    private Time time;
    @JsonProperty("roaming")
    private Roaming roaming;
    @JsonProperty("freeResource")
    private FreeResource freeResource;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("detailSectionLabel")
    public String getDetailSectionLabel() {
        return detailSectionLabel;
    }

    @JsonProperty("detailSectionLabel")
    public void setDetailSectionLabel(String detailSectionLabel) {
        this.detailSectionLabel = detailSectionLabel;
    }

    @JsonProperty("Call")
    public Call__ getCall() {
        return call;
    }

    @JsonProperty("Call")
    public void setCall(Call__ call) {
        this.call = call;
    }

    @JsonProperty("fragmentB")
    public FragmentB getFragmentB() {
        return fragmentB;
    }

    @JsonProperty("fragmentB")
    public void setFragmentB(FragmentB fragmentB) {
        this.fragmentB = fragmentB;
    }

    @JsonProperty("fragmentC")
    public FragmentC getFragmentC() {
        return fragmentC;
    }

    @JsonProperty("fragmentC")
    public void setFragmentC(FragmentC fragmentC) {
        this.fragmentC = fragmentC;
    }

    @JsonProperty("fragmentD")
    public FragmentD getFragmentD() {
        return fragmentD;
    }

    @JsonProperty("fragmentD")
    public void setFragmentD(FragmentD fragmentD) {
        this.fragmentD = fragmentD;
    }

    @JsonProperty("destination")
    public Destination getDestination() {
        return destination;
    }

    @JsonProperty("destination")
    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    @JsonProperty("advantages")
    public Advantages getAdvantages() {
        return advantages;
    }

    @JsonProperty("advantages")
    public void setAdvantages(Advantages advantages) {
        this.advantages = advantages;
    }

    @JsonProperty("titleSubtitle")
    public TitleSubtitle getTitleSubtitle() {
        return titleSubtitle;
    }

    @JsonProperty("titleSubtitle")
    public void setTitleSubtitle(TitleSubtitle titleSubtitle) {
        this.titleSubtitle = titleSubtitle;
    }

    @JsonProperty("date")
    public Date getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Date date) {
        this.date = date;
    }

    @JsonProperty("time")
    public Time getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Time time) {
        this.time = time;
    }

    @JsonProperty("roaming")
    public Roaming getRoaming() {
        return roaming;
    }

    @JsonProperty("roaming")
    public void setRoaming(Roaming roaming) {
        this.roaming = roaming;
    }

    @JsonProperty("freeResource")
    public FreeResource getFreeResource() {
        return freeResource;
    }

    @JsonProperty("freeResource")
    public void setFreeResource(FreeResource freeResource) {
        this.freeResource = freeResource;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
