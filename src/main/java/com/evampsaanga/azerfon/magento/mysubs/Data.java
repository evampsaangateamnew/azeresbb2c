package com.evampsaanga.azerfon.magento.mysubs;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Internet", "Hybrid", "Call", "SMS", "TM", "Campaign", "Roaming" })
public class Data {
	@JsonProperty("Internet")
	private Internet internet;
	@JsonProperty("Hybrid")
	private Object hybrid;
	@JsonProperty("Call")
	private Object call;
	@JsonProperty("SMS")
	private Object sMS;
	@JsonProperty("TM")
	private Object tM;
	@JsonProperty("Campaign")
	private Campaign campaign;
	@JsonProperty("Roaming")
	private Object roaming;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("Internet")
	public Internet getInternet() {
		return internet;
	}

	@JsonProperty("Internet")
	public void setInternet(Internet internet) {
		this.internet = internet;
	}

	@JsonProperty("Hybrid")
	public Object getHybrid() {
		return hybrid;
	}

	@JsonProperty("Hybrid")
	public void setHybrid(Object hybrid) {
		this.hybrid = hybrid;
	}

	@JsonProperty("Call")
	public Object getCall() {
		return call;
	}

	@JsonProperty("Call")
	public void setCall(Object call) {
		this.call = call;
	}

	@JsonProperty("SMS")
	public Object getSMS() {
		return sMS;
	}

	@JsonProperty("SMS")
	public void setSMS(Object sMS) {
		this.sMS = sMS;
	}

	@JsonProperty("TM")
	public Object getTM() {
		return tM;
	}

	@JsonProperty("TM")
	public void setTM(Object tM) {
		this.tM = tM;
	}

	@JsonProperty("Campaign")
	public Campaign getCampaign() {
		return campaign;
	}

	@JsonProperty("Campaign")
	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	@JsonProperty("Roaming")
	public Object getRoaming() {
		return roaming;
	}

	@JsonProperty("Roaming")
	public void setRoaming(Object roaming) {
		this.roaming = roaming;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
