package com.evampsaanga.azerfon.magento.mysubs2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "timeDescription", "fromTimeLabel", "fromTimeValue", "toTimeLabel", "toTimeValue" })
public class Time {
	@JsonProperty("timeDescription")
	private String timeDescription;
	@JsonProperty("fromTimeLabel")
	private String fromTimeLabel;
	@JsonProperty("fromTimeValue")
	private String fromTimeValue;
	@JsonProperty("toTimeLabel")
	private String toTimeLabel;
	@JsonProperty("toTimeValue")
	private String toTimeValue;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("timeDescription")
	public String getTimeDescription() {
		return timeDescription;
	}

	@JsonProperty("timeDescription")
	public void setTimeDescription(String timeDescription) {
		this.timeDescription = timeDescription;
	}

	@JsonProperty("fromTimeLabel")
	public String getFromTimeLabel() {
		return fromTimeLabel;
	}

	@JsonProperty("fromTimeLabel")
	public void setFromTimeLabel(String fromTimeLabel) {
		this.fromTimeLabel = fromTimeLabel;
	}

	@JsonProperty("fromTimeValue")
	public String getFromTimeValue() {
		return fromTimeValue;
	}

	@JsonProperty("fromTimeValue")
	public void setFromTimeValue(String fromTimeValue) {
		this.fromTimeValue = fromTimeValue;
	}

	@JsonProperty("toTimeLabel")
	public String getToTimeLabel() {
		return toTimeLabel;
	}

	@JsonProperty("toTimeLabel")
	public void setToTimeLabel(String toTimeLabel) {
		this.toTimeLabel = toTimeLabel;
	}

	@JsonProperty("toTimeValue")
	public String getToTimeValue() {
		return toTimeValue;
	}

	@JsonProperty("toTimeValue")
	public void setToTimeValue(String toTimeValue) {
		this.toTimeValue = toTimeValue;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
