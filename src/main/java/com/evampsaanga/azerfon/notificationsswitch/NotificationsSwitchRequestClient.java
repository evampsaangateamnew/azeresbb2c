package com.evampsaanga.azerfon.notificationsswitch;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class NotificationsSwitchRequestClient extends BaseRequest {
	String enable = "";

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}
}
