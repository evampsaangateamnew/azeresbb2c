package com.evampsaanga.azerfon.getcdrssummary;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.azerfon.db.DBBakcellFactory;
import com.evampsaanga.azerfon.getcdrsbydate.CustomerID;
import com.evampsaanga.azerfon.getcdrsbydate.GetCDRsByDateLand;
import com.evampsaanga.azerfon.utilities.ConversionUtilities;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;

@Path("/azerfon/")
public class GetCDRsSummary {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCDRsSummaryRequestResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CDRS_SUMMARY_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_CDRS_SUMMARY);
		logs.setTableType(LogsType.GetCdrsSumay);
		String token = "";
		String TrnsactionName = Transactions.GET_CDRS_SUMMARY_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			logger.info(token + "Request Landed on GetCDRsSummary:" + requestBody);
			GetCDRsSummaryRequest cclient = new GetCDRsSummaryRequest();
			
			try {
				cclient = Helper.JsonToObject(requestBody, GetCDRsSummaryRequest.class);
				if(cclient.getEndDate()!= null && !cclient.getEndDate().isEmpty()){
					cclient.setEndDate(Helper.addOneDay(cclient.getEndDate()));
				}
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.GET_CDRS_SUMMARY_TRANSACTION_NAME_B2B);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.info(token + Helper.GetException(ex));
				GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			try {
				if (cclient != null) {
					if (!cclient.getStartDate().isEmpty())
						new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getStartDate());
					else {
						GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
					resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
					resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} catch (Exception ex) {
				GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			try {
				new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getEndDate());
			} catch (Exception ex) {
				GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			try {
				Date datestart = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getStartDate());
				Date dateend = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getEndDate());
				Date today = new Date();
				if (datestart.compareTo(dateend) > 0) {
					GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
					resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
					resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else if (today.compareTo(datestart) < 0) {
					GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
					resp.setReturnCode(ResponseCodes.START_DATE_CANNOT_BE_IN_FUTURE_CODE);
					resp.setReturnMsg(ResponseCodes.START_DATE_CANNOT_BE_IN_FUTURE_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} 
				// We are doing this because we have to increase end date to get records from ODS
	//				else if (today.compareTo(dateend) < 0) {
//					GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
//					resp.setReturnCode(ResponseCodes.END_DATE_CANNOT_BE_IN_FUTURE_CODE);
//					resp.setReturnMsg(ResponseCodes.END_DATE_CANNOT_BE_IN_FUTURE_DES);
//					logs.setResponseCode(resp.getReturnCode());
//					logs.setResponseDescription(resp.getReturnMsg());
//					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//					logs.updateLog(logs);
//					return resp;
//				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(token + "Date Comparison Exceptiom" + e.getMessage());
			}
			if (cclient.getAccountId().isEmpty()) {
				GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getAccountId().equals("")) {
					GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
					resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
					resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetCDRsSummaryRequestResponse res = new GetCDRsSummaryRequestResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					String lang = "2002";
					if (cclient.getLang().equalsIgnoreCase("3"))
						lang = "2002";
					if (cclient.getLang().equalsIgnoreCase("2"))
						lang = "2052";
					if (cclient.getLang().equalsIgnoreCase("4"))
						lang = "2060";

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

					String start = "00:00:00";
					Date startime = simpleDateFormat.parse(start);
					Date endtime = simpleDateFormat.parse("05:00:00");

					// current time
					String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());

					logger.info(token + "TimeStamp :" + timeStamp);
					Date current_time = simpleDateFormat.parse(timeStamp);

					if (current_time.after(startime) && current_time.before(endtime)) {
						GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
						logger.info(token + "Time is between 00:00:00 and 05:00:00 ");
						resp.setReturnCode(ResponseCodes.ERROR_CODE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM);
						resp.setReturnMsg(ResponseCodes.ERROR_MESSAGE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;

					} else {
						return getResponse(cclient.getStartDate(), cclient.getEndDate(), cclient.getmsisdn(),
								cclient.getAccountId(), logs, cclient.getCustomerId(), lang, logger, token,
								cclient.getChannel());
					}

				} else {
					GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
		}
		GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public CustomerID getCustomerIDFromView(String msisdn, Logger logger, String token) {
		logger.info("********START of getCustomerIDFromView***********" + msisdn);
		CustomerID customerId = new CustomerID();
		Connection myConnection = DBBakcellFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			String sql = "select * from V_E_CARE_CUST_INFO t where MSISDN = substr(?,-9) and rn = 1";
			logger.info(token + sql);
			statement = myConnection.prepareStatement(sql);
			statement.setString(1, msisdn);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				customerId.setCbscustomerId(resultSet.getString("CBS_CUST_ID"));
				logger.info("=======================Customer IDs ================");
				logger.info("CBS_CUST_ID: " + resultSet.getString("CBS_CUST_ID"));
				customerId.setCrmcustomerId(resultSet.getString("CRM_CUST_ID"));
				logger.info("CRM_CUST_ID: " + resultSet.getString("CRM_CUST_ID"));
				logger.info("===================================================");
			}
		} catch (Exception e) {
			logger.info(token + Helper.GetException(e));
		}
		logger.info(token + "FROM DB CRM_CUST_ID :" + customerId.getCrmcustomerId());
		logger.info(token + "FROM DB DBS_CUST_ID :" + customerId.getCbscustomerId());
		logger.info("********END of getCustomerIDFromView***********");
		return customerId;
	}

	public GetCDRsSummaryRequestResponse getResponse(String startDate, String endDate, String msisdn, String accountId,
			Logs logs, String customerIdAPI, String lang, Logger logger, String token, String channel) {
		if (GetCDRsByDateLand.usageHistoryTranslationMapping.isEmpty())
			GetCDRsByDateLand.populateUsageHistoryTranslationMapping(token);
		CustomerID customerId = getCustomerIDFromView(msisdn, logger, token);
		double totalUsageVoice = 0;
		double totalVoiceCharge = 0;
		int totalUsageSms = 0;
		double totalChargeSms = 0;
		double totalUsageData = 0;
		double totalChargeData = 0;
		double totalUsageOther = 0.0;
		double totalChargeOther = 0.0;
		try {
			if (!channel.equals("web")) {
				startDate = startDate.split(" ")[0];
				endDate = endDate.split(" ")[0];
			}
			startDate = Helper.parseDateDynamically(token, startDate + " 00:00:00", "yyyy.MM.dd HH:mm:SS");
			endDate = Helper.parseDateDynamically(token, endDate + " 23:59:59", "yyyy.MM.dd HH:mm:SS");
			String msisdnInt = Constants.AZERI_COUNTRY_CODE + msisdn;
			// String sql = "select * from e_care_dev.v_e_care_cdr where
			// e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE >=
			// to_date(?,'dd.mm.yyyy hh24:mi:ss') and
			// e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE <=
			// to_date(?,'dd.mm.yyyy hh24:mi:ss') and
			// e_care_dev.v_e_care_cdr.PRI_IDENTITY=?";//"select * from
			// e_care_dev.v_e_care_cdr where
			// e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE >=
			// to_date(?,'yyyy-mm-dd hh24:mi:ss') and
			// e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE <=" + "
			// to_date(?,'yyyy-mm-dd hh24:mi:ss') and
			// e_care_dev.v_e_care_cdr.PRI_IDENTITY=?";

			// "select * from e_care_dev.v_e_care_cdr where
			// e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE >=
			// to_date(?,'yyyy-mm-dd hh24:mi:ss') and
			// e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE <=" + "
			// to_date(?,'yyyy-mm-dd hh24:mi:ss') and
			// e_care_dev.v_e_care_cdr.PRI_IDENTITY=?";
			String sql = "select * from e_care_dev.v_e_care_cdr where e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE >= to_date(?,'yyyy-mm-dd') and e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE < to_date(?,'yyyy-mm-dd') and e_care_dev.v_e_care_cdr.PRI_IDENTITY=?";

			startDate = startDate.split(" ")[0];
			endDate = endDate.split(" ")[0];
			logger.info(token + "Start Date:" + startDate);
			logger.info(token + "End Date:" + endDate);
			logger.info(token + "msisdn:" + msisdnInt);
			logger.info(token
					+ "select * from e_care_dev.v_e_care_cdr where e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE >= to_date("
					+ startDate.split(" ")[0]
					+ ",'yyyy-mm-dd') and e_care_dev.v_e_care_cdr.CUST_LOCAL_START_DATE < to_date("
					+ endDate.split(" ")[0] + ",'yyyy-mm-dd') and e_care_dev.v_e_care_cdr.PRI_IDENTITY=" + msisdnInt);
			HashMap<String, CDRSummaryDetails> map = new HashMap<>();
			if (customerId != null && customerId.getCrmcustomerId() != null
					&& customerId.getCrmcustomerId().equalsIgnoreCase(customerIdAPI)) {
				logger.info(token + "MSISDN-" + msisdn + "-CustomerID From DB-" + customerId.getCrmcustomerId()
						+ "-CustomerID From API-" + customerIdAPI + "-ID matched.");

				try {
					Connection myConnection = DBBakcellFactory.getConnection();
					PreparedStatement statement = myConnection.prepareStatement(sql);
					statement.setString(1, startDate);
					statement.setString(2, endDate);
					statement.setString(3, msisdnInt);
					logger.info(token + "MSISDN is " + msisdn + "AND QUERY FOR CDR SUMMARY IS " + statement.toString());
					ResultSet resultSet = statement.executeQuery();
					logger.info(token + "QUERY:" + token + ":" + statement.toString());

					logger.info(token + "================ ODS DATA ============================");
					while (resultSet.next()) {

						logger.info(token + "============================================");
						logger.info(token + "USG_SERV_TYPE_NAME" + resultSet.getString("USG_SERV_TYPE_NAME"));
						logger.info(token + "OWNER_CUST_ID" + resultSet.getString("OWNER_CUST_ID"));
						logger.info(token + "CHARGEABLE_AMOUNT" + resultSet.getString("CHARGEABLE_AMOUNT"));
						logger.info(token + "ACTUAL_USAGE" + resultSet.getString("ACTUAL_USAGE"));
						logger.info(token + "TYPE" + resultSet.getString("TYPE"));

						logger.info("============================================");

						if (resultSet.getString("OWNER_CUST_ID").equalsIgnoreCase(customerId.getCbscustomerId())) {
							logger.info(token + "OWNER_CUSTOMER_ID FROM DB" + resultSet.getString("OWNER_CUST_ID"));
							logger.info(
									token + "CUSTOMER_ID_FROM PREVIOUS BIEW FROM DB" + customerId.getCbscustomerId());
							if (map.containsKey(resultSet.getString("USG_SERV_TYPE_NAME"))) {
								CDRSummaryDetails value = map.get(resultSet.getString("USG_SERV_TYPE_NAME"));
								value.setChargeable_amount(ConversionUtilities
										.numberFormattor((Double.parseDouble(value.getChargeable_amount())
												+ Double.parseDouble(resultSet.getString("CHARGEABLE_AMOUNT")))));
								value.setCount(value.getCount() + 1);
								value.setService_type(value.getService_type());
								value.setTotal_usage(
										ConversionUtilities.numberFormattor(Double.parseDouble(value.getTotal_usage())
												+ Double.parseDouble(resultSet.getString("ACTUAL_USAGE"))));
								value.setUnit(value.getUnit());
								value.setType(resultSet.getString("TYPE"));
								map.put(resultSet.getString("USG_SERV_TYPE_NAME"), value);
								logger.info(token + "USG_SERV_TYPE_NAME-> VALUE: " + Helper.ObjectToJson(value));
							} else {
								CDRSummaryDetails cdrSummaryDetails = new CDRSummaryDetails(
										resultSet.getString("USG_SERV_TYPE_NAME"), resultSet.getString("MEASURE_NAME"),
										resultSet.getString("ACTUAL_USAGE"), resultSet.getString("CHARGEABLE_AMOUNT"),
										1, resultSet.getString("TYPE"));
								map.put(resultSet.getString("USG_SERV_TYPE_NAME"), cdrSummaryDetails);
							}
						}
					}
				} catch (Exception ex) {
					logger.info(token + Helper.GetException(ex));
				}
			} else
				logger.info(token + "MSISDN-" + msisdn + "-CustomerID From DB-" + customerId.getCrmcustomerId()
						+ "-CustomerID From API-" + customerIdAPI + "-ID not matched.");
			String voiceList[] = ConfigurationManager.getConfigurationFromCache("cdr.sum.VOICE").split(",");
			String smsList[] = ConfigurationManager.getConfigurationFromCache("cdr.sum.SMS").split(",");
			String dataList[] = ConfigurationManager.getConfigurationFromCache("cdr.sum.DATA").split(",");
			ArrayList<CDRSummaryDetails> temp1 = new ArrayList<>();
			ArrayList<CDRSummaryDetails> temp2 = new ArrayList<>();
			ArrayList<CDRSummaryDetails> temp3 = new ArrayList<>();
			ArrayList<CDRSummaryDetails> temp4 = new ArrayList<>();
			boolean recordAddedFlag = false;
			for (Entry<String, CDRSummaryDetails> entry : map.entrySet()) {
				String key = entry.getKey();

				for (int i = 0; i < voiceList.length; i++) {

					logger.info(token + "---------------------VOICE USAGE HISTORY CHECK-----------------");
					logger.info(token + "---------------------voiceList-----------------" + voiceList[i]);
					logger.info(token + "---------------------key-----------------------" + key);
					logger.info(token + "---------------------VOICE USAGE HISTORY CHECK-----------------");
					if (key != null && voiceList[i].trim().equalsIgnoreCase(key.trim())) {
						String usage = map.get(key).getTotal_usage();
						String chargedAmount = map.get(key).getChargeable_amount();
						totalUsageVoice += (Double.valueOf(usage).doubleValue());
						totalVoiceCharge += (Double.valueOf(chargedAmount).doubleValue());
						map.get(key).setTotal_usage(ConversionUtilities.convertSecondToHourMinuteTime(usage));
						map.get(key).setChargeable_amount(ConversionUtilities.numberFormattor(chargedAmount));
						temp1.add(map.get(key));
						recordAddedFlag = true;
					}
				}
				for (int i = 0; i < smsList.length; i++) {

					logger.info(token + "---------------------SMS USAGE HISTORY CHECK-----------------");
					logger.info(token + "---------------------voiceList-----------------" + smsList[i]);
					logger.info(token + "---------------------key-----------------------" + key);
					logger.info(token + "---------------------VOICE USAGE HISTORY CHECK-----------------");
					logger.info(token + "Key and Value: " + key + " : " + smsList[i]);
					if (key != null && smsList[i].trim().equalsIgnoreCase(key.trim())) {
						map.get(key).setTotal_usage(
								"" + Integer.valueOf(map.get(key).getTotal_usage().replaceAll("\\..*", "")));
						String usage = map.get(key).getTotal_usage();
						String chargedAmount = map.get(key).getChargeable_amount();
						totalUsageSms += (Integer.valueOf(usage).intValue());
						totalChargeSms += (Double.valueOf(chargedAmount).doubleValue());
						map.get(key).setChargeable_amount(ConversionUtilities.numberFormattor(chargedAmount));
						temp2.add(map.get(key));
						recordAddedFlag = true;
					}
				}
				for (int i = 0; i < dataList.length; i++) {
					logger.info(token + "---------------------DATA USAGE HISTORY CHECK-----------------");
					logger.info(token + "---------------------voiceList-----------------" + dataList[i]);
					logger.info(token + "---------------------key-----------------------" + key);
					logger.info(token + "---------------------VOICE USAGE HISTORY CHECK-----------------");

					if (key != null && dataList[i].trim().equalsIgnoreCase(key.trim())) {
						String usage = map.get(key).getTotal_usage();
						String chargedAmount = map.get(key).getChargeable_amount();
						totalUsageData += (Double.valueOf(usage).doubleValue());
						totalChargeData += (Double.valueOf(chargedAmount).doubleValue());
						map.get(key).setTotal_usage(ConversionUtilities.dataConversion(usage, false));
						map.get(key).setChargeable_amount(ConversionUtilities.numberFormattor(chargedAmount));
						temp3.add(map.get(key));
						recordAddedFlag = true;
					}
				}
				if (key != null && !recordAddedFlag) {

					String usage = map.get(key).getTotal_usage();
					String chargedAmount = map.get(key).getChargeable_amount();
					totalUsageOther += Double.parseDouble(usage);
					totalChargeOther += (Double.valueOf(chargedAmount).doubleValue());
					map.get(key).setUnit("");
					map.get(key).setTotal_usage(ConversionUtilities.numberFormattor(usage));
					map.get(key).setChargeable_amount(ConversionUtilities.numberFormattor(chargedAmount));
					temp4.add(map.get(key));
					logger.info(token + "---------------------OTHER USAGE HISTORY CHECK-----------------");
					logger.info(token + "---------------------voiceList-----------------" + usage);
					logger.info(token + "---------------------key-----------------------" + key);
					logger.info(token + "---------------------VOICE USAGE HISTORY CHECK-----------------");
				}
				recordAddedFlag = false;
			}

			String key = "";
			for (int i = 0; i < temp1.size(); i++) {
				key = GetCDRsByDateLand.cdrsColumnMapping.get("USG_SERV_TYPE_NAME")
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + temp1.get(i).getType()
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + temp1.get(i).getService_type()
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang;
				logger.info(token + "VOICE-USAGESUMMARY-KEY: " + key);
				temp1.get(i).setService_type(GetCDRsByDateLand.usageHistoryTranslationMapping.get(key));
			}

			for (int i = 0; i < temp2.size(); i++) {
				key = GetCDRsByDateLand.cdrsColumnMapping.get("USG_SERV_TYPE_NAME")
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + temp2.get(i).getType()
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + temp2.get(i).getService_type()
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang;
				logger.info(token + "SMS-USAGESUMMARY-KEY: " + key);
				temp2.get(i).setService_type(GetCDRsByDateLand.usageHistoryTranslationMapping.get(key));
			}

			for (int i = 0; i < temp3.size(); i++) {
				key = GetCDRsByDateLand.cdrsColumnMapping.get("USG_SERV_TYPE_NAME")
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + temp3.get(i).getType()
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + temp3.get(i).getService_type()
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang;
				logger.info(token + "INTERNET-USAGESUMMARY-KEY: " + key);
				temp3.get(i).setService_type(GetCDRsByDateLand.usageHistoryTranslationMapping.get(key));
			}

			for (int i = 0; i < temp4.size(); i++) {
				key = GetCDRsByDateLand.cdrsColumnMapping.get("USG_SERV_TYPE_NAME")
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + temp4.get(i).getType()
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + temp4.get(i).getService_type()
						+ Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang;
				logger.info(token + "OTHER-USAGESUMMARY-KEY: " + key);
				temp4.get(i).setService_type(GetCDRsByDateLand.usageHistoryTranslationMapping.get(key));
			}

			CDRSummaryResponseBody cdrSummaryResponseBody = new CDRSummaryResponseBody();
			CDRSummary cdrSummaryVoice = new CDRSummary();
			CDRSummary cdrSummarySms = new CDRSummary();
			CDRSummary cdrSummaryData = new CDRSummary();
			CDRSummary cdrSummaryOther = new CDRSummary();
			cdrSummaryVoice.setRecords(temp1);
			cdrSummaryVoice
					.setTotalUsage(ConversionUtilities.convertSecondToHourMinuteTime(Double.toString(totalUsageVoice)));
			cdrSummaryVoice.setTotalCharge(ConversionUtilities.numberFormattor(Double.toString(totalVoiceCharge)));
			cdrSummaryResponseBody.setVoiceRecords(cdrSummaryVoice);
			cdrSummarySms.setRecords(temp2);
			cdrSummarySms.setTotalUsage("" + Integer.valueOf((totalUsageSms)));
			cdrSummarySms.setTotalCharge(ConversionUtilities.numberFormattor(Double.toString(totalChargeSms)));
			cdrSummaryResponseBody.setSmsRecords(cdrSummarySms);
			cdrSummaryData.setRecords(temp3);
			String totalusagestring = ConversionUtilities.dataConversion(Double.toString(totalUsageData), false);
			String totalsmsusagep = totalusagestring.replaceAll("\\..*", "");
			cdrSummaryData.setTotalUsage(totalsmsusagep);
			cdrSummaryData.setTotalCharge(ConversionUtilities.numberFormattor(Double.toString(totalChargeData)));
			cdrSummaryResponseBody.setDataRecords(cdrSummaryData);
			cdrSummaryOther.setRecords(temp4);
			cdrSummaryOther.setTotalUsage(Double.toString(totalUsageOther));
			cdrSummaryOther.setTotalCharge(ConversionUtilities.numberFormattor(totalChargeOther));
			cdrSummaryResponseBody.setOtherRecords(cdrSummaryOther);
			GetCDRsSummaryRequestResponse rep11 = new GetCDRsSummaryRequestResponse();
			rep11.setSummaryResponseBody(cdrSummaryResponseBody);
			rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			logs.setResponseCode(rep11.getReturnCode());
			logs.setResponseDescription(rep11.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return rep11;
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
		} finally {
		}
		GetCDRsSummaryRequestResponse resp = new GetCDRsSummaryRequestResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public static void main(String[] args) {
		// try {

		String startDate = "2019-09-03";
		String endDate = "2019-09-03";
		startDate = startDate.split(" ")[0];
		System.out.println(startDate);

		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
		//
		// String start="00:00:00";
		// Date startime = simpleDateFormat.parse(start);
		// Date endtime = simpleDateFormat.parse("05:00:00");
		//
		// //current time
		// String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new
		// Date());
		//
		// System.out.println("TimeStamp :"+timeStamp);
		// Date current_time = simpleDateFormat.parse(timeStamp);
		//
		// if (current_time.after(startime) && current_time.before(endtime)) {
		// System.out.println("Yes");
		//
		// }
		// else
		// {
		// System.out.println("NO");
		// }
		//
		// } catch (ParseException e) {
		// e.printStackTrace();
		// }

	}

	private static Object substr(String string, int i) {
		// TODO Auto-generated method stub
		return null;
	}
}
