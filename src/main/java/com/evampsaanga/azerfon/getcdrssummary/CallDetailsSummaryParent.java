package com.evampsaanga.azerfon.getcdrssummary;

public class CallDetailsSummaryParent {
	String voice;
	String sms;
	String data;
	String others;

	public String getVoice() {
		return voice;
	}

	public void setVoice(String voice) {
		this.voice = voice;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public CallDetailsSummaryParent(String voice, String sms, String data, String others) {
		super();
		this.voice = voice;
		this.sms = sms;
		this.data = data;
		this.others = others;
	}
}
