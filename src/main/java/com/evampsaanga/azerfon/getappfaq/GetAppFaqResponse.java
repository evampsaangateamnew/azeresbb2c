package com.evampsaanga.azerfon.getappfaq;

import java.util.ArrayList;
import java.util.List;
import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAppFaqResponse extends BaseResponse {
	@JsonProperty("execTime")
	private Double execTime = -0.1;
	@JsonProperty("data")
	private List<Datum> data = new ArrayList<Datum>();

	/**
	 * @return the execTime
	 */
	public Double getExecTime() {
		return execTime;
	}

	/**
	 * @param execTime
	 *            the execTime to set
	 */
	public void setExecTime(Double execTime) {
		this.execTime = execTime;
	}

	/**
	 * @return the data
	 */
	public List<Datum> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<Datum> data) {
		this.data = data;
	}
}
