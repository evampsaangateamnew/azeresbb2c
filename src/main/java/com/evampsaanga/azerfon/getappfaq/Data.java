package com.evampsaanga.azerfon.getappfaq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Category 1", "Category 2" })
public class Data {
	@JsonProperty("Category 1")
	private List<Category1> category1 = new ArrayList<Category1>();
	@JsonProperty("Category 2")
	private List<Category2> category2 = new ArrayList<Category2>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("Category 1")
	public List<Category1> getCategory1() {
		return category1;
	}

	@JsonProperty("Category 1")
	public void setCategory1(List<Category1> category1) {
		this.category1 = category1;
	}

	@JsonProperty("Category 2")
	public List<Category2> getCategory2() {
		return category2;
	}

	@JsonProperty("Category 2")
	public void setCategory2(List<Category2> category2) {
		this.category2 = category2;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
