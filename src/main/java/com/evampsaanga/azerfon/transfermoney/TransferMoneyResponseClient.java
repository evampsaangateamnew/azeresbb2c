package com.evampsaanga.azerfon.transfermoney;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class TransferMoneyResponseClient extends BaseResponse {
	private String oldBalance = "";
	private String newBalance = "";

	/**
	 * @return the oldBalance
	 */
	public String getOldBalance() {
		return oldBalance;
	}

	/**
	 * @param oldBalance
	 *            the oldBalance to set
	 */
	public void setOldBalance(String oldBalance) {
		this.oldBalance = oldBalance;
	}

	/**
	 * @return the newBalance
	 */
	public String getNewBalance() {
		return newBalance;
	}

	/**
	 * @param newBalance
	 *            the newBalance to set
	 */
	public void setNewBalance(String newBalance) {
		this.newBalance = newBalance;
	}
}
