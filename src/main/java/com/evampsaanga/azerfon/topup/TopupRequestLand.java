package com.evampsaanga.azerfon.topup;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.azerfon.utilities.TopUpEncrypterService;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.CBSARService;
import com.huawei.bme.cbsinterface.arservices.PaymentRequest;
import com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentInfo;
import com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentObj;
import com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentObj.AcctAccessCode;
import com.huawei.bme.cbsinterface.arservices.PaymentRequestMsg;
import com.huawei.bme.cbsinterface.arservices.PaymentResultMsg;
import com.huawei.bme.cbsinterface.arservices.RechargeRequest;
import com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeInfo;
import com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeInfo.CardPayment;
import com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeObj;
import com.huawei.bme.cbsinterface.arservices.RechargeRequestMsg;
import com.huawei.bme.cbsinterface.arservices.RechargeResultMsg;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;

@Path("/azerfon")
public class TopupRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	Logs logs = new Logs();

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TopupResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {
		logs.setTransactionName(Transactions.TOPUP_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.TOPUP);
		logs.setTableType(LogsType.Topup);
		String token = "";
		String TrnsactionName = Transactions.TOPUP_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			logger.info(token + token + "Request Landed on TopupRequestLand:" + requestBody);
			TopupRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, TopupRequestClient.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.TOPUP_TRANSACTION_NAME_B2B);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setUserType(cclient.getCustomerType());
					logs.setLang(cclient.getLang());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setScratchCardNumber(cclient.getCardPinNumber());
					logs.setReceiverMsisdn(cclient.getTopupnum());

				}
			} catch (Exception ex) {
				logger.info(token + token + Helper.GetException(ex));
				TopupResponseClient resp = new TopupResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.info(token + token + Helper.GetException(ex));
				}
				if (credentials == null) {
					TopupResponseClient resp = new TopupResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						TopupResponseClient res = new TopupResponseClient();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					TopupResponseClient resp = new TopupResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					
					try {
						if (cclient.getmsisdn().equals(cclient.getTopupnum())) {
							if (cclient.getCustomerType().equals("postpaid")) 
							{
							//	TopupResponseClient resp = new TopupResponseClient();
								TopupResponseClient resp = getResponseForPostPaid(cclient, logger, token);
								if (resp.getReturnCode().equalsIgnoreCase("0")) {
									resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								}
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);

							} else if (cclient.getCustomerType().equals("prepaid")) {
								TopupResponseClient resp = getResponseForPrepaid(cclient, logs, logger, token);
								logs.setUserType(cclient.getCustomerType());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} else {
							GetSubscriberResponse respns = new com.evampsaanga.azerfon.getsubscriber.CRMSubscriberService()
									.GetSubscriberRequest(cclient.getTopupnum());
							if (respns.getResponseHeader().getRetCode().equals("0")) {
								String subscriberType = ConfigurationManager
										.getConfigurationFromCache(ConfigurationManager.MAPPING_HLR_SUBSCRIBER_TYPE
												+ respns.getGetSubscriberBody().getSubscriberType());
								if (subscriberType.equalsIgnoreCase(Constants.USER_TYPE_PREPAID)) {
									TopupResponseClient res = getResponseForPrepaid(cclient, logs, logger, token);
									logs.setUserType(cclient.getCustomerType());
									logs.setResponseCode(res.getReturnCode());
									logs.setResponseDescription(res.getReturnMsg());

									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									logger.info(token + "Response From ESB prepaid case: " + Helper.ObjectToJson(res));
									return res;

								} else if (subscriberType.equalsIgnoreCase(Constants.USER_TYPE_POSTPAID)) {
									TopupResponseClient resp = getResponseForPostPaid(cclient, logger, token);
									logs.setUserType(cclient.getCustomerType());
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setScratchCardNumber(cclient.getCardPinNumber());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									logger.info(token + "Response From ESB postpaid case: " + Helper.ObjectToJson(resp));
									return resp;

								}
							} else {
								TopupResponseClient resp=new TopupResponseClient();
								resp.setReturnCode(respns.getResponseHeader().getRetCode());
								resp.setReturnMsg(respns.getResponseHeader().getRetMsg());
								logs.setUserType(cclient.getCustomerType());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setScratchCardNumber(cclient.getCardPinNumber());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								logger.info(token + "Response From ESB: " + Helper.ObjectToJson(resp));
								return resp;

							}
						}

						
					} catch (Exception ex) {
						logger.info(token + Helper.GetException(ex));
					}
				} else {
					TopupResponseClient resp = new TopupResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
		}
		TopupResponseClient resp = new TopupResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	private TopupResponseClient getResponseForPostPaid(TopupRequestClient cclient, Logger loger, String token) {
		TopupResponseClient resp = new TopupResponseClient();
		try {
			String cardPinNumber = cclient.getCardPinNumber();
			logger.info(token + "Card Pin Number--->>>>  " + cardPinNumber);
			cardPinNumber = cardPinNumber.replaceAll("-", "");
			logger.info(token + "Updated Card Pin Number--->>>>  " + cardPinNumber);
			PaymentResultMsg payment = AzerfonThirdPartyCalls.QueryPostPaidPayment(cclient.getTopupnum(), cardPinNumber,
					logger, token);
			if (payment.getResultHeader().getResultCode().equalsIgnoreCase("0")) {
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				BalanceInfo balance = new BalanceInfo();
				
				if(payment.getPaymentResult().getBalanceChgInfo() != null)
				{
					Long oldBalance = payment.getPaymentResult().getBalanceChgInfo().get(0).getOldBalanceAmt();
					NumberFormat df = new DecimalFormat("#0.00");
					df.setRoundingMode(RoundingMode.FLOOR);
					String oldBalanceStr = df
							.format(oldBalance.longValue() / Constants.MONEY_DIVIDEND);
					
					Long newBalance = payment.getPaymentResult().getBalanceChgInfo().get(0).getNewBalanceAmt();
					NumberFormat df2 = new DecimalFormat("#0.00");
					df.setRoundingMode(RoundingMode.FLOOR);
					String newBalanceStr = df2
							.format(newBalance.longValue() / Constants.MONEY_DIVIDEND);
					
					balance.setNewBalance(newBalanceStr);
					balance.setOldBalance(oldBalanceStr);
				}
				
				resp.setBalance(balance);
			} else {
				resp.setReturnCode(payment.getResultHeader().getResultCode());
				resp.setReturnMsg(payment.getResultHeader().getResultDesc());
			}
			try {
			} catch (Exception ex) {
				logger.info(token + Helper.GetException(ex));
			}
			logs.setResponseCode(payment.getResultHeader().getResultCode());
			logs.setReceiverMsisdn(cclient.getTopupnum());
			logs.setScratchCardNumber(cclient.getCardPinNumber());
			logs.setResponseDescription(payment.getResultHeader().getResultDesc());
			logs.updateLog(logs);
			return resp;
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			logs.setReceiverMsisdn(cclient.getTopupnum());
			logs.setScratchCardNumber(cclient.getCardPinNumber());
			logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.updateLog(logs);
			return resp;
		}
	}

	private TopupResponseClient getResponseForPrepaid(TopupRequestClient cclient, Logs logs, Logger logger,
			String token) {
		
		try {
			String cardPinNumber = cclient.getCardPinNumber();
			logger.info(token + "Card Pin Number--->>>>  " + cardPinNumber);
			cardPinNumber = cardPinNumber.replaceAll("-", "");
			logger.info(token + "Updated Card Pin Number--->>>>  " + cardPinNumber);
			RechargeResultMsg responsePrepaid = AzerfonThirdPartyCalls.QueryPrePaidPayment(cclient.getTopupnum(),
					cardPinNumber, logger, token);
			if (responsePrepaid.getResultHeader().getResultCode().equals("0")) {
				TopupResponseClient resp = new TopupResponseClient();
				logger.info(token+ "succes code from Azerfon Prepaid topup" +responsePrepaid.getResultHeader().getResultCode());
				try {
					if (responsePrepaid.getRechargeResult().getBalanceChgInfo().get(0) != null) {
						BalanceInfo balance = new BalanceInfo();
						balance.setNewBalance(Helper.getBakcellMoney(
								responsePrepaid.getRechargeResult().getBalanceChgInfo().get(0).getNewBalanceAmt())
								+ "");
						balance.setOldBalance(Helper.getBakcellMoney(
								responsePrepaid.getRechargeResult().getBalanceChgInfo().get(0).getOldBalanceAmt())
								+ "");
						resp.setBalance(balance);
					}
				} catch (Exception ex) {
					logger.info(token + Helper.GetException(ex));
				}
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
				logs.setReceiverMsisdn(cclient.getTopupnum());
				logs.setScratchCardNumber(cclient.getCardPinNumber());
				logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
				logs.updateLog(logs);
				return resp;
			} else {
				TopupResponseClient respprepaid = new TopupResponseClient();
				respprepaid.setReturnCode(responsePrepaid.getResultHeader().getResultCode());
				respprepaid.setReturnMsg(responsePrepaid.getResultHeader().getResultDesc());
				logs.setResponseCode((responsePrepaid.getResultHeader().getResultCode()));
				logs.setReceiverMsisdn(cclient.getTopupnum());
				logs.setScratchCardNumber(cclient.getCardPinNumber());
				logs.setResponseDescription(responsePrepaid.getResultHeader().getResultDesc());
				logger.info(token+ "succes code from Azerfon Prepaid topup Else case" +responsePrepaid.getResultHeader().getResultCode());
				logs.updateLog(logs);
				return respprepaid;
			}
			
			
		} catch (Exception ex) {
			TopupResponseClient resp = new TopupResponseClient();
			logger.info(token + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setReceiverMsisdn(cclient.getTopupnum());
			logs.setScratchCardNumber(cclient.getCardPinNumber());
			logs.updateLog(logs);
			return resp;
		}
	}

	/*
	 * public PaymentResultMsg QueryPostPaidPayment(String msisdn, String
	 * cardpinnumber,Logger logger, String token) { PaymentRequestMsg
	 * paymentRequestMsg = new PaymentRequestMsg();
	 * paymentRequestMsg.setRequestHeader(CBSARService.
	 * getRequestPaymentRequestHeader()); PaymentRequest paymentRequest = new
	 * PaymentRequest();
	 * paymentRequest.setPaymentChannelID(Constants.TOPUP_POSTPAID_PAYMENT_CHANNELID
	 * ); paymentRequest.setOpType(Constants.TOPUP_POSTPAID_OPERATION_TYPE);
	 * PaymentObj paymentObject = new PaymentObj(); AcctAccessCode acctAccessCode =
	 * new AcctAccessCode(); acctAccessCode.setPrimaryIdentity(msisdn);
	 * acctAccessCode.setPayType(Constants.TOPUP_POSTPAID_PAY_TYPE);
	 * paymentObject.setAcctAccessCode(acctAccessCode);
	 * paymentRequest.setPaymentObj(paymentObject); PaymentInfo paymentInfo = new
	 * PaymentInfo();
	 * com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentInfo.CardPayment
	 * cardPayment = new
	 * com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentInfo.CardPayment
	 * (); try {
	 * cardPayment.setCardPinNumber(TopUpEncrypterService.encrypt_data(cardpinnumber
	 * )); } catch (Exception ex) { logger.info(token+Helper.GetException(ex)); }
	 * paymentInfo.setCardPayment(cardPayment);
	 * paymentRequest.setPaymentInfo(paymentInfo);
	 * paymentRequestMsg.setPaymentRequest(paymentRequest); PaymentResultMsg
	 * response = CBSARService.getInstance().payment(paymentRequestMsg); return
	 * response; }
	 */

	/*
	 * public RechargeResultMsg QueryPrePaidPayment(String msisdn, String
	 * cardpinnumber,Logger logger,String token) { RechargeRequestMsg
	 * rechargeRequestMsg = new RechargeRequestMsg(); RechargeRequest
	 * requestMsgRecharge = new RechargeRequest();
	 * com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeObj.
	 * AcctAccessCode acctAcessCode = new
	 * com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeObj.
	 * AcctAccessCode(); acctAcessCode.setPrimaryIdentity(msisdn); RechargeObj
	 * rechargeObject = new RechargeObj();
	 * rechargeObject.setAcctAccessCode(acctAcessCode);
	 * requestMsgRecharge.setRechargeObj(rechargeObject); RechargeInfo rechargeInfo
	 * = new RechargeInfo(); CardPayment cardPayment = new CardPayment(); try {
	 * cardPayment.setCardPinNumber(TopUpEncrypterService.encrypt_data(cardpinnumber
	 * )); } catch (Exception ex) { logger.info(token+Helper.GetException(ex)); }
	 * rechargeInfo.setCardPayment(cardPayment);
	 * requestMsgRecharge.setRechargeInfo(rechargeInfo);
	 * rechargeRequestMsg.setRechargeRequest(requestMsgRecharge);
	 * rechargeRequestMsg.setRequestHeader(CBSARService.
	 * getRequestPaymentRequestHeader()); return
	 * CBSARService.getInstance().recharge(rechargeRequestMsg); }
	 */
	public static void main(String[] args) {
		String cardPinNumber = "121212121212121";
		System.out.println("Card Pin Number--->>>>  " + cardPinNumber);
		cardPinNumber = cardPinNumber.replaceAll("-", "");
		System.out.println("Updated Card Pin Number--->>>>  " + cardPinNumber);

	}
}
