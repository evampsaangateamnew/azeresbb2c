package com.evampsaanga.azerfon.topup;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class TopupResponseClient extends BaseResponse {
	private BalanceInfo balance = new BalanceInfo();

	public BalanceInfo getBalance() {
		return balance;
	}

	public void setBalance(BalanceInfo balance) {
		this.balance = balance;
	}
}
