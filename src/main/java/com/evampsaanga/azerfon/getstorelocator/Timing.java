package com.evampsaanga.azerfon.getstorelocator;

public class Timing {
	private String timings;
	private String day;

	public String getTimings() {
		return timings;
	}

	public void setTimings(String timings) {
		this.timings = timings;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	@Override
	public String toString() {
		return "ClassPojo [timings = " + timings + ", day = " + day + "]";
	}
}
