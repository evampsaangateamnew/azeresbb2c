package com.evampsaanga.azerfon.roamingcountriesdata;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetRoamingCountriesResponse extends BaseResponse {
	private com.evampsaanga.azerfon.roamingcountriesdata.Data data=new com.evampsaanga.azerfon.roamingcountriesdata.Data();

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

}
