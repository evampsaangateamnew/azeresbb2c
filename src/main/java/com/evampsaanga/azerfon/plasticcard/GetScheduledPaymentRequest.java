package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class GetScheduledPaymentRequest extends BaseRequest {

	@Override
	public String toString() {
		return "GetScheduledPaymentRequest [toString()=" + super.toString() + "]";
	}

}
