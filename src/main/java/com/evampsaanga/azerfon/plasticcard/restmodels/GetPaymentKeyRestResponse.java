package com.evampsaanga.azerfon.plasticcard.restmodels;

public class GetPaymentKeyRestResponse {
	protected Status status;
	protected String paymentKey;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	@Override
	public String toString() {
		return "GetPaymentKeyRestResponse [status=" + status + ", paymentKey=" + paymentKey + "]";
	}
}
