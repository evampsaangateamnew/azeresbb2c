package com.evampsaanga.azerfon.plasticcard.restmodels;

public class GetPaymentResultRequest {
	private String payment_key;
	private String hash_code;

	public String getPayment_key() {
		return payment_key;
	}

	public void setPayment_key(String payment_key) {
		this.payment_key = payment_key;
	}

	public String getHash_code() {
		return hash_code;
	}

	public void setHash_code(String hash_code) {
		this.hash_code = hash_code;
	}

	@Override
	public String toString() {
		return "GetPaymentResultRequest [payment_key=" + payment_key + ", hash_code=" + hash_code + "]";
	}
}
