package com.evampsaanga.azerfon.plasticcard.restmodels;

public class GetPaymentResultResponse {
	private Status status;
	private String paymentKey;
	private String merchantName;
	private String amount;
	private String checkCount;
	private String paymentDate;
	private String cardNumber;
	private String language;
	private String description;
	private String rrn;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCheckCount() {
		return checkCount;
	}

	public void setCheckCount(String checkCount) {
		this.checkCount = checkCount;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	@Override
	public String toString() {
		return "GetPaymentResultResponse [status=" + status + ", paymentKey=" + paymentKey + ", merchantName="
				+ merchantName + ", amount=" + amount + ", checkCount=" + checkCount + ", paymentDate=" + paymentDate
				+ ", cardNumber=" + cardNumber + ", language=" + language + ", description=" + description + ", rrn="
				+ rrn + "]";
	}

}
