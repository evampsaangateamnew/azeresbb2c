package com.evampsaanga.azerfon.plasticcard.restmodels;

public class GetPaymentKeyRestRequest {
	protected String merchantName;
	protected Integer amount;
	protected String lang;
	protected String cardType;
	protected String description;
	protected String hashCode;

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHashCode() {
		return hashCode;
	}

	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	@Override
	public String toString() {
		return "GetPaymentKeyRestRequest [merchantName=" + merchantName + ", amount=" + amount + ", lang=" + lang
				+ ", cardType=" + cardType + ", description=" + description + ", hashCode=" + hashCode + "]";
	}
	
	
}
