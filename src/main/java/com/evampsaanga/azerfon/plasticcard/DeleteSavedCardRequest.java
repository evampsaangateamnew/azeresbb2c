package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class DeleteSavedCardRequest extends BaseRequest {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeleteSavedCardRequest [id=" + id + ", toString()=" + super.toString() + "]";
	}
	
}
