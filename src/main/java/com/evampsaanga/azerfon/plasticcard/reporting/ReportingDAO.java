package com.evampsaanga.azerfon.plasticcard.reporting;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.azerfon.plasticcard.DeletePaymentSchedulerRequest;
import com.evampsaanga.azerfon.plasticcard.PaymentSchedulerRequest;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;

public class ReportingDAO {
    public static final Logger logger = Logger.getLogger("plasticcardlogs");

    public static void insertSavedPaymentReport(PlasticCardReportModel scReportModel, int fastTopupId) {
        PreparedStatement preparedStmt = null;
        try {
			Connection conn = DBFactory.getDbConnection();
			String query = "insert into plastic_card_saved_payments_report (msisdn,amount,topup_number,card_mask_number,card_type,save_date_time,status,payment_key,fast_topup_id)"
					+ " values (?,?,?,?,?,?,?,?,?)";

            // create the mysql insert prepared statement
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, scReportModel.getMsisdn());
            preparedStmt.setString(2, scReportModel.getAmount());
            preparedStmt.setString(3, scReportModel.getTopupNumber());
            preparedStmt.setString(4, scReportModel.getCardMaskNumber());
            preparedStmt.setString(5, scReportModel.getCardType());
            preparedStmt.setString(6, scReportModel.getSaveDateTime());
            preparedStmt.setString(7, scReportModel.getStatus());
            preparedStmt.setString(8, scReportModel.getPaymentKey());
			preparedStmt.setString(9, fastTopupId+"");
            logger.info(scReportModel.getMsisdn() + "-Saved Payment Report Query-" + preparedStmt);
            logger.info(
                    scReportModel.getMsisdn() + "-Saved Payment Report Query Result: " + preparedStmt.executeUpdate());

            preparedStmt.close();

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (preparedStmt != null)
                    preparedStmt.close();

            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }

        }
    }

	public static void updateSavedPaymentReport(String msisdn, String paymentKeyByID, String fast_topup_id) {
        PreparedStatement preparedStmt = null;
        try {
			String query;
			if(fast_topup_id.isEmpty())
				query = "update plastic_card_saved_payments_report set removal_date_time=? , status=? where payment_key=?";
			else
				query = "update plastic_card_saved_payments_report set removal_date_time=? , status=? where payment_key=? and fast_topup_id=?";
			Connection conn = DBFactory.getDbConnection();

            // create the mysql insert prepared statement
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, ReportingDAO.getDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT));
            preparedStmt.setString(2, Constants.PLASTIC_CARD_STATUS_REMOVED);
            preparedStmt.setString(3, paymentKeyByID);
			if (!fast_topup_id.isEmpty())
				preparedStmt.setString(4, fast_topup_id);
            logger.info(msisdn + "-Updating saved payment report query-" + preparedStmt);

            logger.info(msisdn + "-Updating saved payment report query result-" + preparedStmt.executeUpdate());

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (preparedStmt != null)
                    preparedStmt.close();

            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }
        }

    }

    public static PlasticCardReportModel getSavedCardDetails(String msisdn, String paymentKey) {
        PlasticCardReportModel tempObj = new PlasticCardReportModel();
        ResultSet rs = null;
        Statement stmt = null;
        try {
            String query = "select msisdn,payment_key,card_type,card_mask_number from plastic_card_saved_cards where payment_key='"
                    + paymentKey + "'";

            java.sql.Connection conn = DBFactory.getDbConnection();

            // create the mysql select prepared statement
            stmt = conn.createStatement();
            logger.info(msisdn + "-Query-" + stmt);
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                tempObj.setMsisdn(rs.getString("msisdn"));
                tempObj.setPaymentKey(rs.getString("payment_key"));
                tempObj.setCardType(rs.getInt("card_type") + "");
                tempObj.setCardMaskNumber(rs.getString("card_mask_number"));
            }

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }

        }
        return tempObj;
    }

	public static String getPaymentKeyFromSavedCardsByID(String msisdn, String id) {
		String pKey = "";
		ResultSet rs = null;
		Statement stmt = null;
		try {
			String query = "select payment_key from plastic_card_saved_cards where id='" + id + "'";
			Connection conn = DBFactory.getDbConnection();
			// create the mysql select prepared statement
			stmt = conn.createStatement();
			logger.info(msisdn + "-Query-" + stmt);
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				pKey = rs.getString("payment_key");
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return pKey;
	}
    public static String getPaymentKeyFromFastTOPUPByID(String msisdn, String id) {
        String pKey = "";
        ResultSet rs = null;
        Statement stmt = null;
        try {
            String query = "select payment_key_fk from plastic_card_fast_topup where id='" + id + "'";

            java.sql.Connection conn = DBFactory.getDbConnection();

            // create the mysql select prepared statement
            stmt = conn.createStatement();
            logger.info(msisdn + "-Query-" + stmt);
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                pKey = rs.getString("payment_key_fk");
            }

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }

        }
        return pKey;
    }

    public static void insertSavedCardReport(PlasticCardReportModel scReportModel) {
        PreparedStatement preparedStmt = null;
        try {
            Connection conn = DBFactory.getDbConnection();
            String query = "insert into plastic_card_saved_cards_report (msisdn,card_mask_number,card_type,save_date_time,status,payment_key)"
                    + " values (?,?,?,?,?,?)";

            // create the mysql insert prepared statement
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, scReportModel.getMsisdn());
            preparedStmt.setString(2, scReportModel.getCardMaskNumber());
            preparedStmt.setString(3, scReportModel.getCardType());
            preparedStmt.setString(4, scReportModel.getSaveDateTime());
            preparedStmt.setString(5, scReportModel.getStatus());
            preparedStmt.setString(6, scReportModel.getPaymentKey());
            logger.info(scReportModel.getMsisdn() + "-Saved Card Report Query-" + preparedStmt);
            logger.info(scReportModel.getMsisdn() + "-Saved Card Report Query Result: " + preparedStmt.executeUpdate());

            preparedStmt.close();

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (preparedStmt != null)
                    preparedStmt.close();

            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }

        }

    }

    public static void updateSavedCardReport(String msisdn, String paymentKey) {
        PreparedStatement preparedStmt = null;
        try {
            String query = "update plastic_card_saved_cards_report set removal_date_time=? , status=? where payment_key=?";

            java.sql.Connection conn = DBFactory.getDbConnection();

            // create the mysql insert prepared statement
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, ReportingDAO.getDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT));
            preparedStmt.setString(2, Constants.PLASTIC_CARD_STATUS_REMOVED);
            preparedStmt.setString(3, paymentKey);
            logger.info(msisdn + "-Updating saved card report query-" + preparedStmt);

            logger.info(msisdn + "-Updating saved card report query result-" + preparedStmt.executeUpdate());

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (preparedStmt != null)
                    preparedStmt.close();

            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }
        }

    }

    public static void insertSavedCardUsageReport(PlasticCardReportModel scUsageReport) {
        PreparedStatement preparedStmt = null;
        try {
            Connection conn = DBFactory.getDbConnection();
            String query = "insert into plastic_card_saved_card_usage_report (msisdn,amount,topup_number,card_mask_number,card_type,usage_date_time,payment_key)"
                    + " values (?,?,?,?,?,?,?)";

            // create the mysql insert prepared statement
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, scUsageReport.getMsisdn());
            preparedStmt.setString(2, scUsageReport.getAmount());
            preparedStmt.setString(3, scUsageReport.getTopupNumber());
            preparedStmt.setString(4, scUsageReport.getCardMaskNumber());
            preparedStmt.setString(5, scUsageReport.getCardType());
            preparedStmt.setString(6, ReportingDAO.getDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT));
            preparedStmt.setString(7, scUsageReport.getPaymentKey());
            logger.info(scUsageReport.getMsisdn() + "-Saved Card Usage Report Query-" + preparedStmt);
            logger.info(scUsageReport.getMsisdn() + "-Saved Card Usage Report Query Result: "
                    + preparedStmt.executeUpdate());

            preparedStmt.close();

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (preparedStmt != null)
                    preparedStmt.close();

            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }

        }

    }

	public static void insertAutopaymentScheduler(PaymentSchedulerRequest cclient, int savedPaymentId) {
        PreparedStatement preparedStmt = null;
        try {
            Connection conn = DBFactory.getDbConnection();
            String query = "insert into plastic_card_autopayment_settings_report (msisdn,amount,save_date_time,removal_date_time,status,billing_cycle,saved_card_id,saved_payment_id)"
                    + " values (?,?,?,?,?,?,?,?)";

            // create the mysql insert prepared statement
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, cclient.getmsisdn());
            preparedStmt.setString(2, cclient.getAmount());
            preparedStmt.setString(3, ReportingDAO.getDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT));
            preparedStmt.setString(4, "");
            preparedStmt.setString(5, Constants.PLASTIC_CARD_STATUS_ACTIVE);
            preparedStmt.setString(6, cclient.getBillingCycle());
            preparedStmt.setInt(7, Integer.parseInt(cclient.getSavedCardId()));
			preparedStmt.setInt(8, savedPaymentId);
            logger.info(cclient.getmsisdn() + "-Auto Payment Settings Report Query-" + preparedStmt);
            logger.info(cclient.getmsisdn() + "-Auto Payment Settings Report Query Result: "
                    + preparedStmt.executeUpdate());

            preparedStmt.close();

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (preparedStmt != null)
                    preparedStmt.close();

            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }

        }

    }

    public static void updateAutoPaymentSchedulerReport(DeletePaymentSchedulerRequest cclient) {
        PreparedStatement preparedStmt = null;
        try {
            String query = "update plastic_card_autopayment_settings_report set removal_date_time=? , status=? where saved_payment_id=?";

            java.sql.Connection conn = DBFactory.getDbConnection();

            // create the mysql insert prepared statement
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, ReportingDAO.getDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT));
            preparedStmt.setString(2, Constants.PLASTIC_CARD_STATUS_REMOVED);
            preparedStmt.setInt(3, cclient.getId());
            logger.info(cclient.getmsisdn() + "-Updating auto payment setting report query-" + preparedStmt);

            logger.info(cclient.getmsisdn() + "-Updating auto payment setting report query result-"
                    + preparedStmt.executeUpdate());

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (preparedStmt != null)
                    preparedStmt.close();

            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }
        }

    }

    public static void insertPaymentsForUnsavedCards(PlasticCardReportModel unsavedCardPaymentReportModel) {
        PreparedStatement preparedStmt = null;
        try {
            Connection conn = DBFactory.getDbConnection();
            String query = "insert into plastic_card_payments_unsaved_cards_report (msisdn,amount,date_time,card_type,card_mask_number,repeat_payment)"
                    + " values (?,?,?,?,?,?)";

            // create the mysql insert prepared statement
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, unsavedCardPaymentReportModel.getMsisdn());
            preparedStmt.setString(2, unsavedCardPaymentReportModel.getAmount());
            preparedStmt.setString(3, ReportingDAO.getDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT));
            preparedStmt.setString(4, unsavedCardPaymentReportModel.getCardType());
            preparedStmt.setString(5, unsavedCardPaymentReportModel.getCardMaskNumber());
            preparedStmt.setString(6, unsavedCardPaymentReportModel.isTickbox() == true ? "true" : "false");
            logger.info(
                    unsavedCardPaymentReportModel.getMsisdn() + "-Auto Payment Settings Report Query-" + preparedStmt);
            logger.info(unsavedCardPaymentReportModel.getMsisdn() + "-Auto Payment Settings Report Query Result: "
                    + preparedStmt.executeUpdate());

            preparedStmt.close();

        } catch (Exception e) {
            logger.error(Helper.GetException(e));
        } finally {
            try {
                if (preparedStmt != null)
                    preparedStmt.close();

            } catch (SQLException e) {
                logger.error(Helper.GetException(e));
            }

        }

    }

    private static String getDateTime(String formatter) {
        // Create formatter
        DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern(formatter);

        // Local date time instance
        LocalDateTime localDateTime = LocalDateTime.now();

        // Get formatted String
        return FOMATTER.format(localDateTime);
    }

}
