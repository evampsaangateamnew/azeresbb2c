package com.evampsaanga.azerfon.plasticcard.reporting;

public class PlasticCardReportModel {
	private String msisdn;
	private String topupNumber;
	private String amount;
	private String cardMaskNumber;
	private String cardType;
	private String saveDateTime;
	private String removalDateTime;
	private String status;
	private String paymentKey;
	private boolean tickbox;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getTopupNumber() {
		return topupNumber;
	}

	public void setTopupNumber(String topupNumber) {
		this.topupNumber = topupNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCardMaskNumber() {
		return cardMaskNumber;
	}

	public void setCardMaskNumber(String cardMaskNumber) {
		this.cardMaskNumber = cardMaskNumber;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getSaveDateTime() {
		return saveDateTime;
	}

	public void setSaveDateTime(String saveDateTime) {
		this.saveDateTime = saveDateTime;
	}

	public String getRemovalDateTime() {
		return removalDateTime;
	}

	public void setRemovalDateTime(String removalDateTime) {
		this.removalDateTime = removalDateTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public boolean isTickbox() {
		return tickbox;
	}

	public void setTickbox(boolean tickbox) {
		this.tickbox = tickbox;
	}

	@Override
	public String toString() {
		return "PlasticCardReportModel [msisdn=" + msisdn + ", topupNumber=" + topupNumber + ", amount=" + amount
				+ ", cardMaskNumber=" + cardMaskNumber + ", cardType=" + cardType + ", saveDateTime=" + saveDateTime
				+ ", removalDateTime=" + removalDateTime + ", status=" + status + ", paymentKey=" + paymentKey
				+ ", tickbox=" + tickbox + "]";
	}

}
