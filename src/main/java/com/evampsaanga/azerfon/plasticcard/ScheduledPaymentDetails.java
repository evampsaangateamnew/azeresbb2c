package com.evampsaanga.azerfon.plasticcard;

public class ScheduledPaymentDetails {

	private int id;
	private String amount;
	private String msisdn;
	private String billingCycle; // 1. daily, 2. weekly, 3. monthly
	private String startDate;
	private String recurrenceNumber;
	private String savedCardId;
	private String nextScheduledDate;
	private String cardType;
	private String recurrentDay;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) { 
		this.billingCycle = billingCycle;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getRecurrenceNumber() {
		return recurrenceNumber;
	}

	public void setRecurrenceNumber(String recurrenceNumber) {
		this.recurrenceNumber = recurrenceNumber;
	}

	public String getSavedCardId() {
		return savedCardId;
	}

	public void setSavedCardId(String savedCardId) {
		this.savedCardId = savedCardId;
	}

	public String getNextScheduledDate() {
		return nextScheduledDate;
	}

	public void setNextScheduledDate(String nextScheduledDate) {
		this.nextScheduledDate = nextScheduledDate;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getRecurrentDay() {
		return recurrentDay;
	}

	public void setRecurrentDay(String recurrentDay) {
		this.recurrentDay = recurrentDay;
	}

	@Override
	public String toString() {
		return "ScheduledPaymentDetails [id=" + id + ", amount=" + amount + ", msisdn=" + msisdn + ", billingCycle="
				+ billingCycle + ", startDate=" + startDate + ", recurrenceNumber=" + recurrenceNumber
				+ ", savedCardId=" + savedCardId + ", nextScheduledDate=" + nextScheduledDate + ", cardType=" + cardType
				+ ", recurrentDay=" + recurrentDay + "]";
	}
	
}
