package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class DeleteSavedCardResponse extends BaseResponse {

	@Override
	public String toString() {
		return "DeleteSavedCardResponse [toString()=" + super.toString() + "]";
	}

}
