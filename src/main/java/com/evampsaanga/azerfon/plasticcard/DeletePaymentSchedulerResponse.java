package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class DeletePaymentSchedulerResponse extends BaseResponse {

	@Override
	public String toString() {
		return "DeletePaymentSchedulerResponse [toString()=" + super.toString() + "]";
	}

}
