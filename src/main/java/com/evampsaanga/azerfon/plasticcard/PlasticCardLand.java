package com.evampsaanga.azerfon.plasticcard;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import javax.xml.ws.WebServiceException;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.azerfon.XMLOverHTTP.RestClient;
import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.azerfon.plasticcard.posting.payment.request.PaymentRequest;
import com.evampsaanga.azerfon.plasticcard.posting.payment.request.PaymentRequestBody;
import com.evampsaanga.azerfon.plasticcard.posting.payment.request.PaymentRequestEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.payment.response.PaymentResponseEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.queryrequest.request.QueryStatusCheckRequest;
import com.evampsaanga.azerfon.plasticcard.posting.queryrequest.request.QueryStatusRequestBody;
import com.evampsaanga.azerfon.plasticcard.posting.queryrequest.request.QueryStatusRequestEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.queryrequest.response.QueryStatusResponseEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.recharge.request.RechargeRequest;
import com.evampsaanga.azerfon.plasticcard.posting.recharge.request.RechargeRequestBody;
import com.evampsaanga.azerfon.plasticcard.posting.recharge.request.RechargeRequestEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.recharge.response.RechargeResponseEnvelope;
import com.evampsaanga.azerfon.plasticcard.reporting.PlasticCardReportModel;
import com.evampsaanga.azerfon.plasticcard.reporting.ReportingDAO;
import com.evampsaanga.azerfon.plasticcard.restmodels.GetPaymentKeyRestRequest;
import com.evampsaanga.azerfon.plasticcard.restmodels.GetPaymentKeyRestResponse;
import com.evampsaanga.azerfon.plasticcard.restmodels.GetPaymentResultRequest;
import com.evampsaanga.azerfon.plasticcard.restmodels.GetPaymentResultResponse;
import com.evampsaanga.azerfon.plasticcard.topupclient.GpmReqGetCardTokenInfo;
import com.evampsaanga.azerfon.plasticcard.topupclient.GpmReqGetEcommPaymentKeyRest;
import com.evampsaanga.azerfon.plasticcard.topupclient.GpmReqPayCs;
import com.evampsaanga.azerfon.plasticcard.topupclient.GpmReqUserInfo;
import com.evampsaanga.azerfon.plasticcard.topupclient.GpmRespGetCardToken;
import com.evampsaanga.azerfon.plasticcard.topupclient.GpmRespGetEcommPaymentKey;
import com.evampsaanga.azerfon.plasticcard.topupclient.GpmRespGetEcommPaymentResultRP;
import com.evampsaanga.azerfon.plasticcard.topupclient.GpmStatus;
import com.evampsaanga.azerfon.plasticcard.topupclient.RepeatPaymentWebService;
import com.evampsaanga.azerfon.requestheaders.BaseRequest;
import com.evampsaanga.azerfon.plasticcard.PlasticCardCustomerData;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.CRMServices;
import com.evampsaanga.services.GoldenPayService;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.basetype.RequestHeader;
import com.huawei.crm.query.GetCustomerIn;
import com.huawei.crm.query.GetCustomerRequest;
import com.huawei.crm.query.GetCustomerResponse;

@Path("/azerfon")
public class PlasticCardLand {
	public static final Logger logger = Logger.getLogger("plasticcardlogs");

	/**
	 * This API will be used to initiate Payment in case of new cards this API will
	 * generate Payment Key and will save payment key into Database to save card
	 * details after getting call back from golden pay. Callback will hit process
	 * payment API of this class
	 *
	 * @param credential
	 * @param contentType
	 * @param requestBody
	 * @return
	 */
	@POST
	@Path("/initiatepayment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentInitiateResponse initiatePayment(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.PLASTIC_CARD_INITIATE_PAYMENT_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.PLASTIC_CARD__INITIATE_PAYMENT);
		logs.setTableType(LogsType.PlasticCardRequest);

		String token = "";
		String TrnsactionName = Transactions.PLASTIC_CARD_INITIATE_PAYMENT_TRANSACTION_NAME;
		try {
			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			logger.info("Request Landed on Plastic Card Land-Payment Initiate:" + requestBody);
			PlasticCardInitiatePaymentRequest cclient = null;

			try {
				cclient = Helper.JsonToObject(requestBody, PlasticCardInitiatePaymentRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());

				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				PaymentInitiateResponse resp = new PaymentInitiateResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					PaymentInitiateResponse resp = new PaymentInitiateResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						PaymentInitiateResponse res = new PaymentInitiateResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					PaymentInitiateResponse resp = new PaymentInitiateResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

					if (cclient.getAmount() != null && !cclient.getAmount().isEmpty()
							&& !cclient.getAmount().equalsIgnoreCase("0")) {
						logger.info(cclient.getmsisdn() + "- Request Proceeded in Initiate Payment - Direct Payment");
						// new Code Added from here
						String cardType = cclient.getCardType() == 1 ? "m" : "v";
						String hashKey = getHashKeyDirectPayment(cclient, cardType, cclient.getAmount());

						GetPaymentKeyRestRequest getPaymentKeyRestRequest = new GetPaymentKeyRestRequest();

						getPaymentKeyRestRequest.setAmount(processMultiFactorAmount(cclient.getAmount()));
						getPaymentKeyRestRequest.setCardType(cardType);
						getPaymentKeyRestRequest.setLang(getLangDirectPayment(cclient.getLang()));
						getPaymentKeyRestRequest.setDescription(cclient.getmsisdn());
						getPaymentKeyRestRequest.setMerchantName(
								ConfigurationManager.getConfigurationFromCache("golden.pay.merchant.name"));
						getPaymentKeyRestRequest.setHashCode(hashKey);

						String requestData = Helper.ObjectToJson(getPaymentKeyRestRequest);
						logger.info(cclient.getmsisdn() + "- GetPaymentKeyRestRequest Json " + requestData);

						String url = "https://rest.goldenpay.az/web/service/merchant/getPaymentKey";
						String data = com.saanga.magento.apiclient.RestClient.SendCallToGoldenPay(url, requestData);

						logger.info(cclient.getmsisdn() + "- Data Recieved from RestAPI : " + url + " is : " + data);
						GetPaymentKeyRestResponse responseFromRest = Helper.JsonToObject(data,
								GetPaymentKeyRestResponse.class);

						saveCard(cclient, responseFromRest.getPaymentKey(), cclient.getSaved(), "true", "false");

						PaymentInitiateResponse resp = new PaymentInitiateResponse();

						if (responseFromRest.getStatus().getCode() == 1) {
							resp.setPayment_key1("https://rest.goldenpay.az/web/paypage?payment_key="
									+ responseFromRest.getPaymentKey());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

							logger.info(cclient.getmsisdn() + "- Initiate Direct Payment Response" + resp.toString());

							return resp;
						} else {
							resp.setPayment_key1(url + "=" + responseFromRest.getPaymentKey());
							resp.setReturnCode(responseFromRest.getStatus().getCode() + "");
							resp.setReturnMsg(responseFromRest.getStatus().getMessage());

							logger.info(cclient.getmsisdn() + "- Initiate Direct Payment Response" + resp.toString());
							return resp;
						}

					} else {

						logger.info(cclient.getmsisdn()
								+ "-Request Call to get First Name, Last Name and status for Golden Pay Request");
						PlasticCardCustomerData customerDataResp = this.getCustomerInfoPC(cclient);
						if (customerDataResp != null) {
							if (isCustomerStatusEligible(customerDataResp.getStatus())) {
								RepeatPaymentWebService service = GoldenPayService.getInstance();
								GpmReqUserInfo userInfoRequestObj = new GpmReqUserInfo();
								userInfoRequestObj.setFirstname(customerDataResp.getFirstName());
								userInfoRequestObj.setLastname(customerDataResp.getLastName());

								userInfoRequestObj.setCardType(cclient.getCardType() == 1 ? "m" : "v");
								userInfoRequestObj.setDescription(cclient.getmsisdn());
								userInfoRequestObj.setFirstname(customerDataResp.getFirstName());
								userInfoRequestObj.setLastname(customerDataResp.getLastName());
								userInfoRequestObj.setPhone(cclient.getmsisdn());
								userInfoRequestObj.setPin(
										ConfigurationManager.getConfigurationFromCache("golden.pay.pin").trim());
								userInfoRequestObj.setLang(getLang(cclient.getLang()));

								logger.info(cclient.getmsisdn() + "- Request Call to Golden Pay Initiate Payment");
								GpmRespGetEcommPaymentKey response = service.registerCardRP(userInfoRequestObj);
								if (response.getStatus().getCode() == 1) {
									logger.info(cclient.getmsisdn() + "-Payment initiated Successfully. Payment Key:"
											+ response.getPaymentKey());
									PaymentInitiateResponse resp = new PaymentInitiateResponse();

									logger.info(cclient.getmsisdn() + "- Saving initiate payment details");
									this.saveCard(cclient, response.getPaymentKey(), cclient.getSaved(), "false",
											"false");

									resp.setPayment_key1(
											ConfigurationManager.getConfigurationFromCache("golden.pay.paypage").trim()
													+ response.getPaymentKey());

									resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
									logger.info(cclient.getmsisdn() + "-Response Returned: " + resp.toString());
									// Operational Report
									logs.updateLog(logs);

									return resp;
								} else {
									logger.info(cclient.getmsisdn() + "Payment initiation failed. Response return"
											+ response.getStatus().getMessage());
								}
							} else {
								logger.info(cclient.getmsisdn() + "-Customer is not in active state");
								PaymentInitiateResponse resp = new PaymentInitiateResponse();
								resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
								resp.setReturnMsg("Customer is not in active state");
								logs.setResponseDescription(ResponseCodes.GENERIC_ERROR_DES);
								logs.setResponseCode(ResponseCodes.GENERIC_ERROR_CODE);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}

						} else {
							logger.info(cclient.getmsisdn() + "-Customer Data Call failed");
							PaymentInitiateResponse resp = new PaymentInitiateResponse();
							resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
							resp.setReturnMsg("Customer Data Call failed");
							logs.setResponseDescription(ResponseCodes.GENERIC_ERROR_DES);
							logs.setResponseCode(ResponseCodes.GENERIC_ERROR_CODE);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					}

				} else {
					PaymentInitiateResponse resp = new PaymentInitiateResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}

			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		PaymentInitiateResponse resp = new PaymentInitiateResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * This API will be used after Call back from Golden Pay
	 *
	 * @param credential
	 * @param contentType
	 * @param requestBody
	 * @return
	 */
	@POST
	@Path("/processpayment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ProcessPaymentResponse processpayment(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		String paymentGenericResponseDescription = "";
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.PLASTIC_CARD_PROCESS_PAYMENT_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.PLASTIC_CARD__GET_CARD_TOKEN);
		logs.setTableType(LogsType.PlasticCardRequest);
		try {
			logger.info("Request Landed on Plastic Card-Process Payment:" + requestBody);
			PlasticCardGetCardTokenRequest cclient = null;

			try {
				cclient = Helper.JsonToObject(requestBody, PlasticCardGetCardTokenRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}

			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				ProcessPaymentResponse resp = new ProcessPaymentResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					ProcessPaymentResponse resp = new ProcessPaymentResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						ProcessPaymentResponse res = new ProcessPaymentResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					ProcessPaymentResponse resp = new ProcessPaymentResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

					logger.info(cclient.getmsisdn() + "- Request Call to Golden Pay Get Card Token");
					/**
					 * If below payment status check is true that means payment initiated on golden
					 * pay is successful.
					 */
					if (cclient.isPaymentStatus()) {
						logger.info(cclient.getmsisdn() + "- Payment Call back Status Success");

						/**
						 * check if payment is direct than do posting only else will save card token
						 **/
						VerifyCardDetails verifyCardDetails = getAmountAndCardParmsFromDB(cclient,
								cclient.getPaymentKey(), "", "");
						boolean isDirectPayment = false;
						boolean paymentProcessed = false;
						String trxnid = getTransactionId();
						if (verifyCardDetails.getDirectPayment().equalsIgnoreCase("true"))
							isDirectPayment = true;

						if (verifyCardDetails.getPaymentProcessed().equalsIgnoreCase("true"))
							paymentProcessed = true;

						if (!paymentProcessed) {
							updateSaveCard(verifyCardDetails, "true");
							if (isDirectPayment) {
								logger.info(cclient.getmsisdn() + "-Direct Payment : " + isDirectPayment);

								logger.info(
										cclient.getmsisdn() + "- Payment Call back Status Success from direct payment");

								/**
								 * STEP 3: Verify Card Token
								 */

								cclient.setMsisdn(verifyCardDetails.getMsisdn());

								/**
								 * STEP 7: Save Payment Details
								 *
								 */

								String postingStatus = "NOT DONE";

								/**
								 * STEP 8: Invoke Posting API
								 *
								 */

								logger.info(cclient.getmsisdn() + "-Invoking NGBSS Posting API - DIRECT PAYMENT");
								boolean statusPosting = doPosting(cclient, verifyCardDetails.getTopup_number(),
										verifyCardDetails.getAmount(), logs, trxnid);

								if (statusPosting)
									postingStatus = "SUCCESS";
								else
									postingStatus = "FAILED";

								logger.info(cclient.getmsisdn()
										+ "-Saving charging and posting response details - DIRECT PAYMENT");
								GpmRespGetEcommPaymentResultRP responseMakePayment = new GpmRespGetEcommPaymentResultRP();
								GpmStatus value = new GpmStatus();
								value.setCode(1);
								value.setMessage("SUCESS");
								responseMakePayment.setStatus(value);
								saveChargingPostingDetails(cclient, verifyCardDetails, responseMakePayment,
										postingStatus);

								/**
								 * STEP 9:Save payment
								 */

								if (statusPosting) {

									/**
									 * Unsaved Card Report saving. Reporting Point 7 & 8.
									 */
									GetPaymentResultRequest getPaymentResultRequest = new GetPaymentResultRequest();
									getPaymentResultRequest.setHash_code(getHashKeyPaymentResult(cclient));
									getPaymentResultRequest.setPayment_key(cclient.getPaymentKey());

									String url = "https://rest.goldenpay.az/web/service/merchant/getPaymentResult";
									String data = com.saanga.magento.apiclient.RestClient.SendCallToGoldenPay(url,
											Helper.ObjectToJson(getPaymentResultRequest));

									logger.info(cclient.getmsisdn() + "- Data Recieved from RestAPI : " + url + " is : "
											+ data);
									GetPaymentResultResponse responseFromRest = Helper.JsonToObject(data,
											GetPaymentResultResponse.class);
									PlasticCardReportModel unsavedCardPaymentReportModel = new PlasticCardReportModel();
									unsavedCardPaymentReportModel.setAmount(verifyCardDetails.getAmount());
									unsavedCardPaymentReportModel.setCardMaskNumber(responseFromRest.getCardNumber());
									unsavedCardPaymentReportModel.setCardType(verifyCardDetails.getCardType());
									unsavedCardPaymentReportModel.setMsisdn(verifyCardDetails.getMsisdn());
									unsavedCardPaymentReportModel.setSaveDateTime(
											this.getDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT));
									ReportingDAO.insertPaymentsForUnsavedCards(unsavedCardPaymentReportModel);

									ProcessPaymentResponse resp = new ProcessPaymentResponse();
									resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

									logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
									logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

									logs.updateLog(logs);
									return resp;
								} else {
									logger.info(cclient.getmsisdn() + "-Posting API failed - DIRECT PAYMENT");
									paymentGenericResponseDescription = "Posting failed - DIRECT PAYMENT";
								}
							} else {

								/**
								 * STEP 2: Save Card Token into Database for current and repeat payments (Auto
								 * payments)
								 */
								RepeatPaymentWebService service = GoldenPayService.getInstance();
								GpmReqGetCardTokenInfo reqGetCardTokenInfo = new GpmReqGetCardTokenInfo();
								reqGetCardTokenInfo.setPaymentKey(cclient.getPaymentKey());
								reqGetCardTokenInfo.setPin(
										ConfigurationManager.getConfigurationFromCache("golden.pay.pin").trim());
								GpmRespGetCardToken response = service.getCardTokenRP(reqGetCardTokenInfo);

								logger.info(cclient.getmsisdn() + " -GET CARD TOKEN SUCCESS PROCESS PAYMENT"
										+ " card_mask_number : " + response.getCardNumber() + " card_token : "
										+ response.getCardToken());

								saveUserToken(cclient, response);

								/**
								 * Saving Report Saved cards
								 */

								PlasticCardReportModel spReportModel = new PlasticCardReportModel();
								spReportModel.setAmount(verifyCardDetails.getAmount());
								spReportModel.setCardMaskNumber(response.getCardNumber());
								spReportModel.setCardType(verifyCardDetails.getCardType());
								spReportModel.setMsisdn(verifyCardDetails.getMsisdn());
								spReportModel.setPaymentKey(verifyCardDetails.getPaymentKey());
								spReportModel
										.setSaveDateTime(this.getDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT));
								spReportModel.setStatus(Constants.PLASTIC_CARD_STATUS_ACTIVE);
								spReportModel.setTopupNumber(verifyCardDetails.getTopup_number());

								/**
								 * Saved Card Report
								 */

								ReportingDAO.insertSavedCardReport(spReportModel);

								ProcessPaymentResponse resp = new ProcessPaymentResponse();
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

								logger.info(cclient.getmsisdn() + "-Response Returned: " + resp.toString());
								logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

								logs.updateLog(logs);
								return resp;

							}
						} else {
							logger.info(cclient.getmsisdn() + "-Payment Call Could not proceed. Already done");
							paymentGenericResponseDescription = "Payment call back is Already processed";
						}
					}

				} else {
					logger.info(cclient.getmsisdn() + "-Payment Call Back in failure. Could not proceed.");
					paymentGenericResponseDescription = "Payment call back is failed";

				}
			} else {

				ProcessPaymentResponse resp = new ProcessPaymentResponse();
				resp.setReturnCode(ResponseCodes.PAYMENT_GENERIC_ERROR_CODE);
				resp.setReturnMsg(paymentGenericResponseDescription);
				logs.setResponseDescription(ResponseCodes.PAYMENT_GENERIC_ERROR_CODE);
				logs.setResponseCode(paymentGenericResponseDescription);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		} catch (

		Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		ProcessPaymentResponse resp = new ProcessPaymentResponse();
		resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
		resp.setReturnMsg(paymentGenericResponseDescription);
		logs.setResponseDescription(paymentGenericResponseDescription);
		logs.setResponseCode(ResponseCodes.GENERIC_ERROR_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * makePaymentProcess Used to process payments of already saved cards and for
	 * fast top up.
	 *
	 * @param cclient
	 * @return
	 */
	@POST
	@Path("/makepaymentprocess")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MakePaymentProcessResponse makePaymentProcess(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		int code = 0;
		String paymentResponseGenericError = "";
		logs.setTransactionName(Transactions.PLASTIC_CARD_MAKE_PAYMENT_PROCESS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.PLASTIC_CARD_MAKE_PAYMENT_PROCESS);
		logs.setTableType(LogsType.PlasticCardRequest);

		String token = "";
		String TrnsactionName = Transactions.PLASTIC_CARD_INITIATE_PAYMENT_TRANSACTION_NAME;
		try {
			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			logger.info("Request Landed on Plastic Card Land-Make Payment Process:" + requestBody);
			MakePaymentProcessRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, MakePaymentProcessRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				MakePaymentProcessResponse resp = new MakePaymentProcessResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					MakePaymentProcessResponse resp = new MakePaymentProcessResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						MakePaymentProcessResponse res = new MakePaymentProcessResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					MakePaymentProcessResponse resp = new MakePaymentProcessResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

					String postingStatus = "NOT DONE";
					logger.info(cclient.getmsisdn()
							+ "-Request Call to get First Name, Last Name and status for Golden Pay Request");
					PlasticCardCustomerData customerDataResp = this.getCustomerInfoPC(cclient);
					if (customerDataResp != null) {
						if (isCustomerStatusEligible(customerDataResp.getStatus())) {
							String trxnid = getTransactionId();
							MakePaymentProcessResponse resp = new MakePaymentProcessResponse();
							VerifyCardDetails verifyCardDetails = getAmountAndCardParmsFromDB(cclient,
									cclient.getPaymentKey(), cclient.getAmount(), cclient.getTopupNumber());
							GpmRespGetEcommPaymentResultRP status = makePayment(cclient, verifyCardDetails, trxnid);

							code = status.getStatus().getCode();
							if (status.getStatus().getCode() == 1) {

								/**
								 * POSTING API
								 */
								logger.info(cclient.getmsisdn() + "-Invoking NGBSS Posting API");
								boolean statusPosting = doPosting(cclient, cclient.getTopupNumber(),
										cclient.getAmount(), logs, trxnid);

								if (statusPosting)
									postingStatus = "SUCCESS";
								else
									postingStatus = "FAILED";

								logger.info(cclient.getmsisdn() + "-Saving charging and posting response details");
								saveChargingPostingDetails(cclient, verifyCardDetails, status, postingStatus);

								if (statusPosting) {
									int fastTopupId = addFastTopupAmount(verifyCardDetails.getMsisdn(),
											verifyCardDetails.getAmount(), verifyCardDetails.getTopup_number(),
											cclient.getPaymentKey(), 1);
									/**
									 * Saving Saved card Usage Report Data
									 */
									PlasticCardReportModel scUsageReport = ReportingDAO
											.getSavedCardDetails(cclient.getmsisdn(), cclient.getPaymentKey());
									scUsageReport.setMsisdn(cclient.getmsisdn());
									scUsageReport.setAmount(cclient.getAmount());
									scUsageReport.setPaymentKey(cclient.getPaymentKey());
									scUsageReport.setTopupNumber(cclient.getTopupNumber());

									ReportingDAO.insertSavedCardUsageReport(scUsageReport);
									scUsageReport.setSaveDateTime(Constants.PLASTIC_CARD_REPORT_DATE_FORMAT);
									scUsageReport.setStatus(Constants.PLASTIC_CARD_STATUS_ACTIVE);
									ReportingDAO.insertSavedPaymentReport(scUsageReport, fastTopupId);

									resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

									logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
									logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

									logs.updateLog(logs);
									logger.info(cclient.getmsisdn() + "-Response Returned: " + resp.toString());
									return resp;
								} else {
									logger.info(cclient.getmsisdn() + "-Posting API failed");
									paymentResponseGenericError = "Posting API failed";
								}

							} else {
								logger.info(cclient.getmsisdn() + "Charging Failed");
								paymentResponseGenericError = "Charging API failed";
								logger.info(cclient.getmsisdn() + "-Saving charging and posting response details");
								saveChargingPostingDetails(cclient, verifyCardDetails, status, postingStatus);
							}
						} else {
							logger.info(cclient.getmsisdn() + "Customer in not in active state");
							paymentResponseGenericError = "Customer is not in active state";
						}
					} else {
						logger.info(cclient.getmsisdn() + "-Customer API Call failed");
						paymentResponseGenericError = "Customer API call failed";
					}

				} else {
					logger.info(cclient.getmsisdn() + "-Credentials not valid");
				}

			} else {
				MakePaymentProcessResponse resp = new MakePaymentProcessResponse();
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDescription(ResponseCodes.ERROR_401);
				logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		MakePaymentProcessResponse resp = new MakePaymentProcessResponse();
//        resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		if (code == 0)
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		else
			resp.setReturnCode(code + "");
		resp.setReturnMsg(paymentResponseGenericError);
		logs.setResponseDescription(paymentResponseGenericError);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;

	}

	@POST
	@Path("/getsavedcards")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SavedCardResponse getSavedCards(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_SAVED_CARDS_REQUEST_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_SAVED_CARDS_REQUEST);
		logs.setTableType(LogsType.GetSavedCardsRequest);
		try {
			logger.info("Request Landed on getSavedCards:" + requestBody);
			SavedCardRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, SavedCardRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				SavedCardResponse resp = new SavedCardResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					SavedCardResponse resp = new SavedCardResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						SavedCardResponse res = new SavedCardResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					SavedCardResponse resp = new SavedCardResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to GET SAVED CARDS");
					List<SingleCardDetails> responseFromDB = getCardsFromDB(cclient);
					SavedCardResponse resp = new SavedCardResponse();
					resp.setLastAmount(getLastAmount(cclient.getmsisdn()));
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					resp.setSingleCardDetails(responseFromDB);
					logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.setUserType(Constants.USER_TYPE_PREPAID);
					logs.updateLog(logs);
					return resp;

				} else {
					SavedCardResponse resp = new SavedCardResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		SavedCardResponse resp = new SavedCardResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getfastpayment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FastPaymentResponse getFastPayment(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_FAST_PAYMENT_REQUEST_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_FAST_PAYMENT_REQUEST);
		logs.setTableType(LogsType.GetFastPaymentRequest);
		try {
			logger.info("Request Landed on getFastPayment:" + requestBody);
			FastPaymentRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, FastPaymentRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				FastPaymentResponse resp = new FastPaymentResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					FastPaymentResponse resp = new FastPaymentResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						FastPaymentResponse res = new FastPaymentResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					FastPaymentResponse resp = new FastPaymentResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to GET Fast Payment");
					List<FastPaymentDetails> responseFromDB = getFastPaymentDetailsFromDB(cclient);

					FastPaymentResponse resp = new FastPaymentResponse();
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					resp.setFastPaymentDetails(responseFromDB);
					logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.setUserType(Constants.USER_TYPE_PREPAID);
					logs.updateLog(logs);
					return resp;

				} else {
					FastPaymentResponse resp = new FastPaymentResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		FastPaymentResponse resp = new FastPaymentResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/deletefasttopup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DeleteFastTopupResponse deleteFastTopup(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.DELETE_FAST_TOPUP_REQUEST_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.DELETE_FAST_TOPUP_REQUEST);
		logs.setTableType(LogsType.DeleteFastTopupRequest);
		try {
			logger.info("Request Landed on deleteFastTopup:" + requestBody);
			DeleteFastTopupRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, DeleteFastTopupRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				DeleteFastTopupResponse resp = new DeleteFastTopupResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					DeleteFastTopupResponse resp = new DeleteFastTopupResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						DeleteFastTopupResponse res = new DeleteFastTopupResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					DeleteFastTopupResponse resp = new DeleteFastTopupResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to DELETE FAST TOPUP");
					boolean responseFromDB = deleteFastTopupFromDB(cclient.getmsisdn(), cclient.getId() + "", 1);

					if (responseFromDB) {
						DeleteFastTopupResponse resp = new DeleteFastTopupResponse();
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					} else {
						DeleteFastTopupResponse resp = new DeleteFastTopupResponse();
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseDescription(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseCode(ResponseCodes.GENERIC_ERROR_CODE);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					}

				} else {
					DeleteFastTopupResponse resp = new DeleteFastTopupResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		DeleteFastTopupResponse resp = new DeleteFastTopupResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/addpaymentscheduler")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PaymentSchedulerResponse addPaymentScheduler(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ADD_PAYMENT_SCHEDULER_REQUEST_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ADD_PAYMENT_SCHEDULER_REQUEST);
		logs.setTableType(LogsType.AddPaymentSchedulerRequest);
		try {
			logger.info("Request Landed on addPaymentScheduler:" + requestBody);
			PaymentSchedulerRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, PaymentSchedulerRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				PaymentSchedulerResponse resp = new PaymentSchedulerResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					PaymentSchedulerResponse resp = new PaymentSchedulerResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						PaymentSchedulerResponse res = new PaymentSchedulerResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					PaymentSchedulerResponse resp = new PaymentSchedulerResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to ADD PAYMENT SCHEDULER");

					int responseFromDB = savePaymentSchedulerInDB(cclient);

					if (responseFromDB > 0) {
						ReportingDAO.insertAutopaymentScheduler(cclient, responseFromDB);
						PaymentSchedulerResponse resp = new PaymentSchedulerResponse();
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					} else {
						PaymentSchedulerResponse resp = new PaymentSchedulerResponse();
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseDescription(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseCode(ResponseCodes.GENERIC_ERROR_CODE);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					}

				} else {
					PaymentSchedulerResponse resp = new PaymentSchedulerResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		PaymentSchedulerResponse resp = new PaymentSchedulerResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getscheduledpayments")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetScheduledPaymentResponse getScheduledPayment(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_SCHEDULED_PAYMENTS_REQUEST_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_SCHEDULED_PAYMENTS_REQUEST);
		logs.setTableType(LogsType.GetScheduledPaymentRequest);
		try {
			logger.info("Request Landed on getScheduledPayment:" + requestBody);
			GetScheduledPaymentRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetScheduledPaymentRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetScheduledPaymentResponse resp = new GetScheduledPaymentResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetScheduledPaymentResponse resp = new GetScheduledPaymentResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetScheduledPaymentResponse res = new GetScheduledPaymentResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					GetScheduledPaymentResponse resp = new GetScheduledPaymentResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to GET SCHEDULED PAYMENT");
					List<ScheduledPaymentDetails> responseFromDB = getScheduledPaymentFromDB(cclient);

					GetScheduledPaymentResponse resp = new GetScheduledPaymentResponse();
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					resp.setScheduledPaymentDetailsList(responseFromDB);
					logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.setUserType(Constants.USER_TYPE_PREPAID);
					logs.updateLog(logs);
					return resp;

				} else {
					GetScheduledPaymentResponse resp = new GetScheduledPaymentResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetScheduledPaymentResponse resp = new GetScheduledPaymentResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/deletesavedcard")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DeleteSavedCardResponse deleteSavedCard(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.DELETE_SAVED_CARD_REQUEST_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.DELETE_SAVED_CARD_REQUEST);
		logs.setTableType(LogsType.DeleteSavedCardRequest);
		try {
			logger.info("Request Landed on deleteSavedCard:" + requestBody);
			DeleteSavedCardRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, DeleteSavedCardRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				DeleteSavedCardResponse resp = new DeleteSavedCardResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					DeleteSavedCardResponse resp = new DeleteSavedCardResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						DeleteSavedCardResponse res = new DeleteSavedCardResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					DeleteSavedCardResponse resp = new DeleteSavedCardResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to DELETE Saved Card");
					boolean responseFromDB = deleteSavedCardFromDB(cclient);

					if (responseFromDB) {
						DeleteSavedCardResponse resp = new DeleteSavedCardResponse();
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					} else {
						DeleteSavedCardResponse resp = new DeleteSavedCardResponse();
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseDescription(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseCode(ResponseCodes.GENERIC_ERROR_CODE);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					}

				} else {
					DeleteSavedCardResponse resp = new DeleteSavedCardResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		DeleteSavedCardResponse resp = new DeleteSavedCardResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/deletepaymentscheduler")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DeletePaymentSchedulerResponse deletePaymentScheduler(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.DELETE_PAYMENT_SCHEDULER_REQUEST_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.DELETE_PAYMENT_SCHEDULER_CARD_REQUEST);
		logs.setTableType(LogsType.DeletePaymentSchedulerRequest);
		try {
			logger.info("Request Landed on deletePaymentScheduler:" + requestBody);
			DeletePaymentSchedulerRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, DeletePaymentSchedulerRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				DeletePaymentSchedulerResponse resp = new DeletePaymentSchedulerResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					DeletePaymentSchedulerResponse resp = new DeletePaymentSchedulerResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						DeletePaymentSchedulerResponse res = new DeletePaymentSchedulerResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					DeletePaymentSchedulerResponse resp = new DeletePaymentSchedulerResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "- Request Call to Auto Payment Scheduler");
					boolean responseFromDB = deletePaymentSchedulerFromDB(cclient);

					if (responseFromDB) {
						ReportingDAO.updateAutoPaymentSchedulerReport(cclient);
						DeletePaymentSchedulerResponse resp = new DeletePaymentSchedulerResponse();
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					} else {
						DeletePaymentSchedulerResponse resp = new DeletePaymentSchedulerResponse();
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseDescription(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseCode(ResponseCodes.GENERIC_ERROR_CODE);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setUserType(Constants.USER_TYPE_PREPAID);
						logs.updateLog(logs);
						return resp;
					}

				} else {
					DeletePaymentSchedulerResponse resp = new DeletePaymentSchedulerResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		DeletePaymentSchedulerResponse resp = new DeletePaymentSchedulerResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	private boolean deletePaymentSchedulerFromDB(DeletePaymentSchedulerRequest cclient) {

		String deleteQuery = "delete from plastic_card_payment_scheduler where id=" + cclient.getId();
		PreparedStatement preparedStmt = null;
		boolean check = false;
		Connection conn = DBFactory.getDbConnection();
		try {
			preparedStmt = conn.prepareStatement(deleteQuery);
			if (preparedStmt.executeUpdate() > 0)
				check = true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (preparedStmt != null)
					preparedStmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return check;
	}

	private boolean deleteSavedCardFromDB(DeleteSavedCardRequest cclient) {
		String paymentKey = ReportingDAO.getPaymentKeyFromSavedCardsByID(cclient.getmsisdn(), cclient.getId() + "");
		/**
		 * Report of deleting saved payments
		 */

		ReportingDAO.updateSavedPaymentReport(cclient.getmsisdn(), paymentKey, "");
		/**
		 * Report of Deleting Saved Card
		 */
		ReportingDAO.updateSavedCardReport(cclient.getmsisdn(), paymentKey);

		String deleteQuery = "delete from plastic_card_saved_cards where id=" + cclient.getId();
		PreparedStatement preparedStmt = null;
		boolean check = false;
		Connection conn = DBFactory.getDbConnection();
		try {
			preparedStmt = conn.prepareStatement(deleteQuery);
			if (preparedStmt.executeUpdate() > 0) {
				deletePaymentRelatedSavedCardFromDB(cclient.getId());
				check = true;
			}
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (preparedStmt != null)
					preparedStmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return check;
	}

	private boolean deletePaymentRelatedSavedCardFromDB(int id) {
		String deleteQuery = "delete from plastic_card_payment_scheduler where saved_card_id=" + id;
		PreparedStatement preparedStmt = null;
		boolean check = false;
		Connection conn = DBFactory.getDbConnection();
		try {
			preparedStmt = conn.prepareStatement(deleteQuery);
			if (preparedStmt.executeUpdate() > 0)
				check = true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (preparedStmt != null)
					preparedStmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return check;
	}

	private List<ScheduledPaymentDetails> getScheduledPaymentFromDB(GetScheduledPaymentRequest cclient) {
		ArrayList<ScheduledPaymentDetails> paymentDetails = new ArrayList<>();
		String selectListQuery = "select ap.amount,ap.billing_cycle,ap.id,ap.next_schedule_date,ap.recurrence_number,ap.saved_card_id,ap.start_date,ap.msisdn,sc.card_type from plastic_card_payment_scheduler ap,plastic_card_saved_cards sc where ap.msisdn='"
				+ cclient.getmsisdn() + "' and ap.saved_card_id=sc.id";
		Connection conn = DBFactory.getDbConnection();
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {
			preparedStmt = conn.prepareStatement(selectListQuery);
			resultSet = preparedStmt.executeQuery();
			while (resultSet.next()) {
				ScheduledPaymentDetails details = new ScheduledPaymentDetails();
				details.setAmount(resultSet.getString("amount"));
				details.setBillingCycle(resultSet.getInt("billing_cycle") + "");
				details.setId(resultSet.getInt("id"));
				details.setNextScheduledDate(resultSet.getString("next_schedule_date"));
				details.setRecurrenceNumber(resultSet.getString("recurrence_number"));
				details.setSavedCardId(resultSet.getString("saved_card_id"));
				details.setStartDate(resultSet.getString("start_date"));
				details.setMsisdn(resultSet.getString("msisdn"));
				details.setCardType(resultSet.getInt("card_type") == 1 ? Constants.CARD_MASTER : Constants.CARD_VISA);
				details.setRecurrentDay(
						getRecurrenceDay(cclient.getLang(), details.getStartDate(), resultSet.getInt("billing_cycle")));
				paymentDetails.add(details);
			}
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (preparedStmt != null)
					preparedStmt.close();
				if (resultSet != null)
					resultSet.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return paymentDetails;
	}

	private String getRecurrenceDay(String lang, String startDate, int billingCycle) {

		if (billingCycle == 1)
			return ConfigurationManager.getConfigurationFromCache("plastic.card.recurrence.daily." + lang);
		else if (billingCycle == 2) {
			try {
				if (startDate != null) {
					return ConfigurationManager.getConfigurationFromCache("plastic.card.recurrence.weekly." + lang)
							+ " " + ConfigurationManager.getConfigurationFromCache(new SimpleDateFormat("EEEE")
									.format(new SimpleDateFormat("yyyy-MM-dd").parse(startDate)) + "." + lang);
				}
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		} else if (billingCycle == 3) {
			try {
				if (startDate != null) {
					String day = new SimpleDateFormat("dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(startDate));
					return ConfigurationManager
							.getConfigurationFromCache("recurrence.monthly." + day + "." + getLang(lang));
				}
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		} else {
			return "";
		}
		return "";
	}

	private int savePaymentSchedulerInDB(PaymentSchedulerRequest cclient) {
		PreparedStatement preparedStmt = null;
		ResultSet rs = null;

		int generatedId = 0;
		try {
			Connection conn = DBFactory.getDbConnection();
			String query = "insert into plastic_card_payment_scheduler (msisdn, amount, billing_cycle, start_date, next_schedule_date, recurrence_number, recurrence_remaining,recurrence_frequency, saved_card_id)"
					+ " values (?, ?, ?, ?, ?,?,?,?,?)";

			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preparedStmt.setString(1, cclient.getmsisdn());
			preparedStmt.setString(2, cclient.getAmount());
			preparedStmt.setInt(3, Integer.parseInt(cclient.getBillingCycle()));
			preparedStmt.setString(4, cclient.getStartDate());
			preparedStmt.setString(5, getNextSchedulingDate(cclient.getStartDate(), cclient.getBillingCycle(),
					cclient.getRecurrenceFrequency()));
			preparedStmt.setInt(6, Integer.parseInt(cclient.getRecurrenceNumber()));
			preparedStmt.setInt(7, Integer.parseInt(cclient.getRecurrenceNumber()));
			preparedStmt.setInt(8, Integer.parseInt(cclient.getRecurrenceFrequency()));
			preparedStmt.setInt(9, Integer.parseInt(cclient.getSavedCardId()));
			logger.info(cclient.getmsisdn() + "-Auto Payment Scheduler saving query-" + preparedStmt);

			preparedStmt.executeUpdate();

			rs = preparedStmt.getGeneratedKeys();
			if (rs.next()) {
				generatedId = rs.getInt(1);
			}

			logger.info(cclient.getmsisdn() + "-Auto Payment Scheduler query Result: " + generatedId);

			return generatedId;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (preparedStmt != null)
					preparedStmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return generatedId;
	}

	private String getNextSchedulingDate(String startDate, String billingCycle, String recurenceFrequency) {

		try {
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			if (billingCycle.equalsIgnoreCase("1"))
				calendar.add(Calendar.DATE, 1);
			else if (billingCycle.equalsIgnoreCase("2"))
				calendar.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(recurenceFrequency));
			else if (billingCycle.equalsIgnoreCase("3"))
				calendar.add(Calendar.MONTH, Integer.parseInt(recurenceFrequency));
			return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}

	private boolean deleteFastTopupFromDB(String msisdn, String id, int flagTypeOfRequest) {

		Statement stmt = null;
		String query = "";
		try {
			ReportingDAO.updateSavedPaymentReport(msisdn, ReportingDAO.getPaymentKeyFromFastTOPUPByID(msisdn, id), id);
			if (flagTypeOfRequest == 1) {
				logger.info(msisdn + "-Request is to delete by ID.");
				query = "Delete from plastic_card_fast_topup where id=" + id;
			} else if (flagTypeOfRequest == 2) {
				logger.info(msisdn + "-Request is to delete by Payment Key.");
				query = "Delete from plastic_card_fast_topup where payment_key_fk='" + id + "'";
			}
			Connection conn = DBFactory.getDbConnection();

			// create the mysql insert prepared statement
			stmt = conn.createStatement();
			logger.info(msisdn + "-Delete fast topup record from DB Query-" + stmt);
			int returnCode = stmt.executeUpdate(query);

			if (returnCode == 1) {
				if (stmt != null)
					stmt.close();
				return true;
			} else {
				if (stmt != null)
					stmt.close();
				return false;
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
		}
		return false;
	}

	// function to get fast payment details from DB
	private List<FastPaymentDetails> getFastPaymentDetailsFromDB(FastPaymentRequest cclient) {
		List<FastPaymentDetails> fastPaymentDetailsList = new ArrayList<>();

		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			String query = "SELECT pf.id, pf.amount, pf.topup_number, ps.payment_key, ps.card_type FROM plastic_card_saved_cards as ps INNER JOIN plastic_card_fast_topup as pf ON ps.payment_key = pf.payment_key_fk where pf.payment_status =1 and ps.msisdn='"
					+ cclient.getmsisdn() + "'";

			java.sql.Connection conn = DBFactory.getDbConnection();

			// create the mysql select prepared statement
			stmt = conn.prepareStatement(query);
			logger.info(cclient.getmsisdn() + "-Get custom fields from DB Query-" + stmt);
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				FastPaymentDetails fastPaymentDetails = new FastPaymentDetails();

				fastPaymentDetails.setId(rs.getInt("id"));
				fastPaymentDetails.setAmount(Double.parseDouble(rs.getString("amount")));
				fastPaymentDetails.setPaymentKey(rs.getString("payment_key"));
				fastPaymentDetails.setTopupNumber(rs.getString("topup_number"));
				fastPaymentDetails
						.setCardType(rs.getInt("card_type") == 1 ? Constants.CARD_MASTER : Constants.CARD_VISA);

				fastPaymentDetailsList.add(fastPaymentDetails);
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		logger.info(cclient.getmsisdn() + "-Details from DB-" + fastPaymentDetailsList.toString());

		return fastPaymentDetailsList;
	}

	private String getLastAmount(String msisdn) {
		String query = "SELECT amount FROM plastic_card_fast_topup where msisdn='" + msisdn
				+ "' and payment_status='1' order by date_time desc limit 1";
		String amount = "0";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			java.sql.Connection conn = DBFactory.getDbConnection();

			// create the mysql select prepared statement
			stmt = conn.prepareStatement(query);
			logger.info(msisdn + "-Get last amount from DB Query-" + stmt);
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				amount = rs.getString("amount");
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		logger.info(msisdn + "-last topup from db from DB-" + amount);
		return amount;
	}

	// function to get saved cards from DB
	private List<SingleCardDetails> getCardsFromDB(SavedCardRequest cclient) {

		List<SingleCardDetails> singleCardDetailsList = new ArrayList<>();

		ResultSet rs = null;
		Statement stmt = null;
		try {
			String query = "select plastic_card_saved_cards.id," + " plastic_card_saved_cards.card_type,"
					+ " plastic_card_saved_cards.payment_key," + " plastic_card_saved_cards.card_mask_number"
					+ " from plastic_card_saved_cards where plastic_card_saved_cards.card_token!='' and plastic_card_saved_cards.msisdn="
					+ cclient.getmsisdn();

			java.sql.Connection conn = DBFactory.getDbConnection();

			// create the mysql select prepared statement
			stmt = conn.createStatement();
			logger.info(cclient.getmsisdn() + "-Get custom fields from DB Query-" + stmt);
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				SingleCardDetails singleCardDetails = new SingleCardDetails();

				singleCardDetails.setId(rs.getInt("id"));
				singleCardDetails.setCardMaskNumber(rs.getString("card_mask_number"));
				singleCardDetails.setPaymentKey(rs.getString("payment_key"));
				singleCardDetails
						.setCardType(rs.getInt("card_type") == 1 ? Constants.CARD_MASTER : Constants.CARD_VISA);

				singleCardDetailsList.add(singleCardDetails);
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		logger.info(cclient.getmsisdn() + "-Details from DB-" + singleCardDetailsList.toString());
		return singleCardDetailsList;

	}

	/**
	 *
	 * @param cclient
	 * @param verifyCardDetails
	 * @return
	 */
	private GpmRespGetEcommPaymentResultRP makePayment(BaseRequest cclient, VerifyCardDetails verifyCardDetails,
			String trxnid) {
		RepeatPaymentWebService service = GoldenPayService.getInstance();
		/**
		 * STEP 3: Verify Card Token
		 *
		 * Commenting as this is giving error 905 (Invalid amount)
		 */

		logger.info(cclient.getmsisdn() + "-Verify Card Details successful");
		/**
		 * STEP 5: INVOKE GOLDEN PAY API: GetEcommPaymentKeyRP to get Payment Key 2
		 */
		logger.info(cclient.getmsisdn() + "-Invoking Get Payment Key-2 API");
		String hashKey = getHashKey(cclient, verifyCardDetails, trxnid);
		GpmReqGetEcommPaymentKeyRest reqGetEcommPaymentKey = new GpmReqGetEcommPaymentKeyRest();
		reqGetEcommPaymentKey.setAmount(processMultiFactorAmount(verifyCardDetails.getAmount() + ""));
		reqGetEcommPaymentKey.setCardType(verifyCardDetails.getCardType());
		reqGetEcommPaymentKey.setLang(cclient.getLang());
		reqGetEcommPaymentKey.setDescription(cclient.getmsisdn() + "|" + trxnid);
		reqGetEcommPaymentKey
				.setMerchantName(ConfigurationManager.getConfigurationFromCache("golden.pay.merchant.name"));
		reqGetEcommPaymentKey.setHashCode(hashKey);
		GpmRespGetEcommPaymentKey responseEcommPaymentKey = service.getEcommPaymentKeyRP(reqGetEcommPaymentKey);

		if (responseEcommPaymentKey.getStatus().getCode() == 1) {
			logger.info(cclient.getmsisdn() + "-Payment Key-2 API successful");
			/**
			 * STEP 6: INVOKE GOLDEN PAY API: MAKE PAYMENT
			 *
			 */
			GpmReqPayCs reqMakePayment = new GpmReqPayCs();
			reqMakePayment.setCardToken(verifyCardDetails.getCardToken());
			reqMakePayment.setPaymentKey(responseEcommPaymentKey.getPaymentKey());
			reqMakePayment.setPin(ConfigurationManager.getConfigurationFromCache("golden.pay.pin"));
			logger.info(cclient.getmsisdn() + "-Calling Make Payment API");
			GpmRespGetEcommPaymentResultRP responseMakePayment = service.makePaymentRP(reqMakePayment);
			logger.info(cclient.getmsisdn() + "-Make Payment API Response" + responseMakePayment.toString());
			return responseMakePayment;
		} else {
			logger.info(cclient.getmsisdn() + "-Get EComm Payment Key-2 failed");
			return null;
		}

	}

	/**
	 * Posting Type Flag 1: Top-up and 2: Payment
	 *
	 * @param cclient
	 * @param postingType
	 * @return
	 * @throws IOException
	 * @throws JAXBException
	 */
	private boolean doPosting(BaseRequest cclient, String topupNumber, String amount, Logs logs, String trxnid)
			throws IOException, JAXBException {
		logger.info(cclient.getmsisdn() + "- Request received in do Posting with topup number: " + topupNumber
				+ "-amount: " + amount + "- and Base Request:" + cclient.toString());

		String postingAPIURL = ConfigurationManager.getConfigurationFromCache("golden.pay.ngbss.posting.url");
		String postingDateTimeFormat = "yyyy/MM/dd hh:mm:ss";
		String userName = "ecare";
		String password = "Ecare001";
		int postingType = 0;
		/**
		 * STEP 1 POSTING: Query Status API
		 */

		RestClient rcXML = new RestClient();

		QueryStatusRequestEnvelope envelope = new QueryStatusRequestEnvelope();
		envelope.setReqtype(11);
		envelope.setUsername(userName);
		envelope.setPassword(password);

		QueryStatusRequestBody body = new QueryStatusRequestBody();
		QueryStatusCheckRequest checkRequest = new QueryStatusCheckRequest();
		checkRequest.setDate(this.getDateTime(postingDateTimeFormat));
		checkRequest.setMsisdn(topupNumber);
		checkRequest.setParameterset(null);
		checkRequest.setSpid("100005");
		checkRequest.setServiceid("50005");
		checkRequest.setTxnid(trxnid);

		body.setCheckrequest(checkRequest);
		envelope.setBody(body);
		logger.info(cclient.getmsisdn() + "-Query Status Request Payload-" + envelope.toString());
		logger.info(cclient.getmsisdn() + "-Query Status Request URL-" + postingAPIURL);
		logger.info(
				cclient.getmsisdn() + "-Query Status Request Payload-XML-" + MarshallerParser.getObjectToXML(envelope));
		String queryStatusResponseStr = rcXML.getResponseFromESB(postingAPIURL,
				MarshallerParser.getObjectToXML(envelope));

		logger.info(cclient.getmsisdn() + "-Query Status Response-" + queryStatusResponseStr);

		QueryStatusResponseEnvelope queryStatusResponse = (QueryStatusResponseEnvelope) MarshallerParser
				.getXMLToObject(queryStatusResponseStr, new QueryStatusResponseEnvelope());

		if (queryStatusResponse != null && queryStatusResponse.getBody() != null
				&& queryStatusResponse.getBody().getCheckresponse() != null
				&& queryStatusResponse.getBody().getCheckresponse().getCause().equalsIgnoreCase("100")) {
			if (queryStatusResponse.getBody().getCheckresponse().getSubtype().equalsIgnoreCase("0"))
				postingType = 1;
			else if (queryStatusResponse.getBody().getCheckresponse().getSubtype().equalsIgnoreCase("1"))
				postingType = 2;
			else
				logger.info(cclient.getmsisdn() + "-Query Status Sub Type not valid:"
						+ queryStatusResponse.getBody().getCheckresponse().getSubtype());
			/**
			 * STEP 2 : POSTING RECHARGE API IF FLAG =1
			 */
			if (postingType == 1) {
				RechargeRequestEnvelope rechargeRequestEnvelope = new RechargeRequestEnvelope();
				rechargeRequestEnvelope.setReqtype(21);
				rechargeRequestEnvelope.setUsername(userName);
				rechargeRequestEnvelope.setPassword(password);
				RechargeRequestBody rechargeRequestBody = new RechargeRequestBody();
				RechargeRequest rechargerequest = new RechargeRequest();
				rechargerequest.setDate(this.getDateTime(postingDateTimeFormat));
				rechargerequest.setMsisdn("994" + topupNumber);
				rechargerequest.setParameterset(null);
				rechargerequest.setSpid("100005");
				rechargerequest.setServiceid("50000");
				rechargerequest.setTxnid(trxnid);
				rechargerequest.setAccountid(queryStatusResponse.getBody().getCheckresponse().getAccountid());
				rechargerequest.setPaymentamnt(amount);
				rechargerequest.setParameterset("37:" + trxnid + "|42:" + trxnid);
				rechargeRequestBody.setPaymentrequest(rechargerequest);
				rechargeRequestEnvelope.setBody(rechargeRequestBody);
				logger.info(cclient.getmsisdn() + "-Recharge Posting Request Payload-"
						+ rechargeRequestEnvelope.toString());
				logger.info(cclient.getmsisdn() + "-Recharge Posting Request URL-" + postingAPIURL);
				logger.info(cclient.getmsisdn() + "-Recharge Posting Request Payload-XML-"
						+ MarshallerParser.getObjectToXML(rechargeRequestEnvelope));
				String rechargeresponseStr = rcXML.getResponseFromESB(postingAPIURL,
						MarshallerParser.getObjectToXML(rechargeRequestEnvelope));

				logger.info(cclient.getmsisdn() + "-Recharge Posting Response-" + rechargeresponseStr);

				RechargeResponseEnvelope rechargeResponse = (RechargeResponseEnvelope) MarshallerParser
						.getXMLToObject(rechargeresponseStr, new RechargeResponseEnvelope());
				if (rechargeResponse != null && rechargeResponse.getBody() != null
						&& rechargeResponse.getBody().getPaymentresponse() != null
						&& rechargeResponse.getBody().getPaymentresponse().getCause().equalsIgnoreCase("100")) {

					return true;
				} else {
					logger.info(cclient.getmsisdn() + "-Posting Recharge failed.");
					return false;
				}
			} else if (postingType == 2) {
				/**
				 * STEP 3: POSTING PAYMENT API IF FLAG=2
				 */

				PaymentRequest paymentRequest = new PaymentRequest();
				paymentRequest.setAccountid(queryStatusResponse.getBody().getCheckresponse().getAccountid());
				paymentRequest.setDate(this.getDateTime(postingDateTimeFormat));
				paymentRequest.setMsisdn("994" + topupNumber);
				paymentRequest.setParameterset("37:" + trxnid + "|42:" + trxnid);
				paymentRequest.setPaymentamnt(amount);
				paymentRequest.setServiceid("50000");
				paymentRequest.setSpid("100005");
				paymentRequest.setTxnid(trxnid);

				PaymentRequestBody paymentRequestBody = new PaymentRequestBody();
				paymentRequestBody.setPaymentrequest(paymentRequest);

				PaymentRequestEnvelope paymentRequestEnvelope = new PaymentRequestEnvelope();
				paymentRequestEnvelope.setReqtype(21);
				paymentRequestEnvelope.setUsername(userName);
				paymentRequestEnvelope.setPassword(password);
				paymentRequestEnvelope.setBody(paymentRequestBody);
				logger.info(
						cclient.getmsisdn() + "-Payment Posting Request Payload-" + paymentRequestEnvelope.toString());
				logger.info(cclient.getmsisdn() + "-Payment Posting Request URL-" + postingAPIURL);
				logger.info(cclient.getmsisdn() + "-Payment Posting Request Payload-XML-"
						+ MarshallerParser.getObjectToXML(paymentRequestEnvelope));

				String paymentResponseStr = rcXML.getResponseFromESB(postingAPIURL,
						MarshallerParser.getObjectToXML(paymentRequestEnvelope));

				logger.info(cclient.getmsisdn() + "-Payment Posting Response-" + paymentResponseStr);

				PaymentResponseEnvelope paymentResponse = (PaymentResponseEnvelope) MarshallerParser
						.getXMLToObject(paymentResponseStr, new PaymentResponseEnvelope());
				if (paymentResponse != null && paymentResponse.getBody() != null
						&& paymentResponse.getBody().getPaymentresponse() != null
						&& paymentResponse.getBody().getPaymentresponse().getCause().equalsIgnoreCase("100")) {

					return true;
				} else {
					logger.info(cclient.getmsisdn() + "-Posting Payment failed.");
					return false;
				}
			} else {
				logger.info(cclient.getmsisdn() + "-Invalid Request");
				return false;
			}
		} else {
			logger.info(cclient.getmsisdn() + "-Check Staus Query Response has issue");

			return false;
		}
	}

	private void saveChargingPostingDetails(BaseRequest cclient, VerifyCardDetails verifyCardDetails,
			GpmRespGetEcommPaymentResultRP responseMakePayment, String postingStatus) {
		PreparedStatement preparedStmt = null;
		try {
			Connection conn = DBFactory.getDbConnection();
			String query = "insert into plastic_card_charging_details (msisdn,payment_key,amount,payment_date,card_mask_number,rrn,charging_status,charging_message,posting_status)"
					+ " values (?, ?, ?, ?, ?,?,?,?,?)";

			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, cclient.getmsisdn());
			preparedStmt.setString(2, verifyCardDetails.getPaymentKey());
			preparedStmt.setString(3, verifyCardDetails.getAmount());
			preparedStmt.setString(4, Helper.getCurrentTimeStamp());
			preparedStmt.setString(5, responseMakePayment.getCardNumber());
			preparedStmt.setString(6, responseMakePayment.getRrn());
			preparedStmt.setInt(7, responseMakePayment.getStatus().getCode());
			preparedStmt.setString(8, responseMakePayment.getStatus().getMessage());
			preparedStmt.setString(9, postingStatus);
			logger.info(cclient.getmsisdn() + "-Charging and posting Details saving query-" + preparedStmt);
			logger.info(cclient.getmsisdn() + "- Charging and posting Details saving query Result: "
					+ preparedStmt.executeUpdate());

			preparedStmt.close();

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}

	}

	private String getHashKey(BaseRequest cclient, VerifyCardDetails verifyCardDetails, String trxnid) {
		// HashKeyParams= (auth_key + merchantName + cardType + amount +
		// description)

		StringBuilder hashKey = new StringBuilder();
		hashKey.append(ConfigurationManager.getConfigurationFromCache("golden.pay.auth.key"));
		hashKey.append(ConfigurationManager.getConfigurationFromCache("golden.pay.merchant.name"));
		hashKey.append(verifyCardDetails.getCardType());
		hashKey.append(processMultiFactorAmount(verifyCardDetails.getAmount() + ""));
		hashKey.append(cclient.getmsisdn() + "|" + trxnid);
		logger.info(cclient.getmsisdn() + "- Generating HashKey for: " + hashKey.toString());
		return DigestUtils.md5Hex(hashKey.toString());
	}

	private String getHashKeyDirectPayment(BaseRequest cclient, String cardType, String amount) {

		StringBuilder hashKey = new StringBuilder();
		hashKey.append(ConfigurationManager.getConfigurationFromCache("golden.pay.auth.key"));
		hashKey.append(ConfigurationManager.getConfigurationFromCache("golden.pay.merchant.name"));
		hashKey.append(cardType);
		hashKey.append(processMultiFactorAmount(amount));
		hashKey.append(cclient.getmsisdn());
		logger.info(cclient.getmsisdn() + "- Generating HashKey for (DirectPayment) : " + hashKey.toString());
		return DigestUtils.md5Hex(hashKey.toString());
	}

	private String getHashKeyPaymentResult(PlasticCardGetCardTokenRequest cclient) {

		StringBuilder hashKey = new StringBuilder();

		hashKey.append(ConfigurationManager.getConfigurationFromCache("golden.pay.auth.key"));
		hashKey.append(cclient.getPaymentKey());

		logger.info(cclient.getmsisdn() + "- Generating HashKey for (getPaymentResult) : " + hashKey.toString());
		return DigestUtils.md5Hex(hashKey.toString());
	}

	/**
	 * if amount and top up number is empty than the request is from processpayment
	 * API else the request is from processmakepayment API
	 *
	 * @param cclient
	 * @param paymentKey
	 * @param amount
	 * @param topupNumber
	 * @return
	 */

	private VerifyCardDetails getAmountAndCardParmsFromDB(BaseRequest cclient, String paymentKey, String amount,
			String topupNumber) {
		VerifyCardDetails verifyCardDetails = new VerifyCardDetails();
		ResultSet rs = null;
		Statement stmt = null;
		try {
			String query = "select * from plastic_card_saved_cards where payment_key='" + paymentKey + "'";

			java.sql.Connection conn = DBFactory.getDbConnection();

			// create the mysql select prepared statement
			stmt = conn.createStatement();
			logger.info(cclient.getmsisdn() + "-Get custom fields from DB Query-" + stmt.toString());
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				verifyCardDetails.setMsisdn(rs.getString("msisdn"));
				if (amount.isEmpty() || amount.equalsIgnoreCase(null)) {
					verifyCardDetails.setAmount(rs.getString("amount"));
				} else {
					verifyCardDetails.setAmount(amount);
				}
				verifyCardDetails.setCardToken(rs.getString("card_token"));
				verifyCardDetails.setPaymentKey(rs.getString("payment_key"));
				verifyCardDetails.setDirectPayment(rs.getString("direct_payment"));
				verifyCardDetails.setCardType(rs.getInt("card_type") == 1 ? "m" : "v");
				verifyCardDetails.setPaymentProcessed(rs.getString("payment_status"));
				if (topupNumber.equalsIgnoreCase("")) {
					verifyCardDetails.setTopup_number(rs.getString("topup_number"));
				} else {
					verifyCardDetails.setTopup_number(topupNumber);
				}
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		logger.info(verifyCardDetails.getMsisdn() + "-Details from DB-" + verifyCardDetails.toString());
		return verifyCardDetails;
	}

	public boolean getRepeatPaymentStatusByPaymentKey(String msisdn, String paymentKey) {

		ResultSet rs = null;
		Statement stmt = null;
		boolean flag = false;
		try {
			String query = "select plastic_card_saved_cards.repeat_payment from plastic_card_saved_cards where payment_key='"
					+ paymentKey + "'";

			java.sql.Connection conn = DBFactory.getDbConnection();

			// create the mysql select prepared statement
			stmt = conn.createStatement();
			logger.info(msisdn + "-Get repeat_Payment Status Query-" + stmt.toString());
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (rs.getString("repeat_payment").equalsIgnoreCase("true"))
					flag = true;
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		logger.info(msisdn + "-Details from DB flag is-" + flag);
		return flag;
	}

	private boolean saveUserToken(PlasticCardGetCardTokenRequest cclient, GpmRespGetCardToken response) {
		PreparedStatement preparedStmt = null;
		int status = 0;
		try {
			String query = "update ignore plastic_card_saved_cards set card_token=?,card_mask_number=? where payment_key=?";

			java.sql.Connection conn = DBFactory.getDbConnection();
			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, response.getCardToken());
			preparedStmt.setString(2, response.getCardNumber());
			preparedStmt.setString(3, cclient.getPaymentKey());
			logger.info(cclient.getmsisdn() + "-Saving Card Token query-" + preparedStmt);
			status = preparedStmt.executeUpdate();
			logger.info(cclient.getmsisdn() + "-Saving Card Token query-" + preparedStmt + " : result: " + status);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
		}
		if (status > 0)
			return true;
		else
			return false;

	}

	private boolean saveCard(PlasticCardInitiatePaymentRequest cclient, String paymentKey, String repeat_payment,
			String directPayment, String paymentStatus) {
		PreparedStatement preparedStmt = null;
		ResultSet rs = null;
		try {
			Connection conn = DBFactory.getDbConnection();
			String query = "insert into plastic_card_saved_cards (msisdn,payment_key,date_time,card_type,card_token,card_mask_number,repeat_payment,amount,direct_payment,topup_number,payment_status)"
					+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, cclient.getmsisdn());
			preparedStmt.setString(2, paymentKey);
			preparedStmt.setString(3, Helper.getCurrentTimeStamp());
			preparedStmt.setInt(4, cclient.getCardType());
			preparedStmt.setString(5, "");
			preparedStmt.setString(6, Calendar.getInstance().getTimeInMillis() + "");
			preparedStmt.setString(7, repeat_payment);
			preparedStmt.setString(8, cclient.getAmount());
			preparedStmt.setString(9, directPayment);
			preparedStmt.setString(10, cclient.getTopupNumber());
			preparedStmt.setString(11, paymentStatus);
			logger.info(cclient.getmsisdn() + "-Card saving insertion query-" + preparedStmt);
			preparedStmt.executeUpdate();
			preparedStmt.close();

			return true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return false;

	}

	private int addFastTopupAmount(String msisdn, String amount, String topupNumber, String paymentKey,
			int payment_status) {
		PreparedStatement preparedStmt = null;
		ResultSet rs = null;
		int generatedId = 0;
		try {
			Connection conn = DBFactory.getDbConnection();
			String query = "insert into plastic_card_fast_topup (amount,msisdn,topup_number,date_time,payment_key_fk,payment_status)"
					+ " values (?, ?, ?, ?,?,?)";

			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preparedStmt.setString(1, amount);
			preparedStmt.setString(2, msisdn);
			preparedStmt.setString(3, topupNumber);
			preparedStmt.setString(4, Helper.getCurrentTimeStamp());
			preparedStmt.setString(5, paymentKey);
			preparedStmt.setInt(6, payment_status);
			logger.info(msisdn + "-Fast Top up payment insertion query-" + preparedStmt);
			preparedStmt.executeUpdate();

			rs = preparedStmt.getGeneratedKeys();
			if (rs.next()) {
				generatedId = rs.getInt(1);
			}

			logger.info(msisdn + "-Fast Top up payment query Result: " + generatedId);

			return generatedId;

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}

		return generatedId;
	}

	private boolean updateSaveCard(VerifyCardDetails verifyCardDetails, String paymentStatus) {
		PreparedStatement preparedStmt = null;
		ResultSet rs = null;
		try {
			Connection conn = DBFactory.getDbConnection();
			String query = "UPDATE plastic_card_saved_cards SET payment_status = ? "
					+ "WHERE msisdn = ? AND payment_key = ? ";

			// create the mysql insert prepared statement
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, paymentStatus);
			preparedStmt.setString(2, verifyCardDetails.getMsisdn());
			preparedStmt.setString(3, verifyCardDetails.getPaymentKey());

			logger.info(verifyCardDetails.getMsisdn() + "-Card update query-" + preparedStmt);
			preparedStmt.executeUpdate();
			preparedStmt.close();

			return true;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (preparedStmt != null)
					preparedStmt.close();

			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}

		}
		return false;

	}

//	private GetCustomerRequestResponse getCustomerData(BaseRequest cclient, Logs logs, String token) {
//		GetCustomerRequestClient customerDataRequest = new GetCustomerRequestClient();
//		customerDataRequest.setChannel(cclient.getChannel());
//		customerDataRequest.setiP(cclient.getiP());
//		customerDataRequest.setIsB2B(cclient.getIsB2B());
//		customerDataRequest.setLang(cclient.getLang());
//		customerDataRequest.setMsisdn(cclient.getmsisdn());
//
//		new GetCustomerDataLand();
//		GetCustomerRequestResponse customerDataResp = GetCustomerDataLand.RequestSoap(customerDataRequest, logs, token);
//		return customerDataResp;
//	}

	private String getLang(String lang) {
		switch (lang) {
		case "2":
			return "ru";
		case "3":
			return "en";
		case "4":
			return "az";

		}
		return "en";
	}

	private String getLangDirectPayment(String lang) {
		switch (lang) {
		case "2":
			return "ru";
		case "3":
			return "en";
		case "4":
			return "lv";

		}
		return "en";
	}

	public static Integer processMultiFactorAmount(String amount) {

		return ((int) (Double.valueOf(amount) * Constants.GOLDEN_PAY_MULTIPLY_FACTOR));

	}

	public static Integer processMultiFactorAmountUndo(String amount) {

		return ((int) (Double.valueOf(amount) / Constants.GOLDEN_PAY_MULTIPLY_FACTOR));

	}

	private boolean isCustomerStatusEligible(String statusCode) {
		switch (statusCode) {
		case "B01":// B01 - Active
			return true;
		case "B03":// 03 - Two way block
			return true;
		case "B04": // B04 - One way block
			return true;
		case "F02": // F02 – Active (Postpaid)
			return true;
		default:
			return false;
		}

	}

	private String getDateTime(String formatter) {
		// Create formatter
		DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern(formatter);

		// Local date time instance
		LocalDateTime localDateTime = LocalDateTime.now();

		// Get formatted String
		return FOMATTER.format(localDateTime);
	}

	public static String getTransactionId() {

		return System.currentTimeMillis() + "";
	}

	private PlasticCardCustomerData getCustomerInfoPC(BaseRequest cclient) {

		PlasticCardCustomerData customerData = new PlasticCardCustomerData();
		String firstName = "";
		String lastName = "";
		String statusCode = "";

		GetCustomerRequest cDR = new GetCustomerRequest();
		cDR.setRequestHeader(getRequestHeader());
		GetCustomerIn gCI = new GetCustomerIn();
		gCI.setServiceNumber(cclient.getmsisdn());

		cDR.setGetCustomerBody(gCI);
		try {
			logger.info(cclient.getmsisdn() + "-GetCustomerRequest -" + cDR.toString());
			GetCustomerResponse sR = CRMServices.getInstance().getCustomerData(cDR);
			logger.info(cclient.getmsisdn() + "-GetCustomerResponse -" + sR.toString());
			if (sR.getResponseHeader().getRetCode().equals("0")) {
				GetSubscriberResponse respns = new com.evampsaanga.azerfon.getsubscriber.CRMSubscriberService()
						.GetSubscriberRequest(cclient.getmsisdn());
				logger.info(cclient.getmsisdn() + "-GetSubscriberResponse-" + respns.toString());
				if (respns.getResponseHeader().getRetCode().equals("0")) {
					if (sR.getGetCustomerBody().getFirstName() != null)
						firstName = getDummyNameOrMSISDN(sR.getGetCustomerBody().getFirstName(), cclient.getmsisdn());
					if (sR.getGetCustomerBody().getLastName() != null)
						lastName = getDummyNameOrMSISDN(sR.getGetCustomerBody().getLastName(), cclient.getmsisdn());
					statusCode = respns.getGetSubscriberBody().getStatus();
				}
			}
			if (firstName.equals(lastName)) {
				lastName = "";
			}
			customerData.setFirstName(firstName);
			customerData.setLastName(lastName);
			customerData.setStatus(statusCode);
			return customerData;
		} catch (Exception ee) {
			if (ee instanceof WebServiceException) {
				logger.info(cclient + "-Exception while getting Customer data");
			} else {
				logger.error(Helper.GetException(ee));
			}
			return null;
		}
	}

	public RequestHeader getRequestHeader() {
		RequestHeader reqh = new RequestHeader();
		String accessUser = ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim();
		reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqh.setTechnicalChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqh.setAccessUser(accessUser);
		reqh.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqh.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqh.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		logger.info("REQUEST HEADER IS >>>>>>>>>>>>>>>>>>> :" + reqh);
		return reqh;
	}

	private String getDummyNameOrMSISDN(String name, String msisdn) {
		logger.info(msisdn + "-getDummyNameOrMSISDN : " + name);
		String customerName = "";
		if (name.equalsIgnoreCase("No")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("Name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("No Name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("first")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("last")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("first name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("last name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("first name last name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("Null")) {
			customerName = msisdn;
		} else {
			customerName = name;
		}
		return customerName;
	}

}