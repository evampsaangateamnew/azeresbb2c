package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class ProcessPaymentResponse extends BaseResponse{

	@Override
	public String toString() {
		return "GetCardTokenResponse [toString()=" + super.toString() + "]";
	}

}
