package com.evampsaanga.azerfon.plasticcard;

public class VerifyCardDetails {
	String msisdn;
	String amount;
	String cardToken;
	String paymentKey;
	String cardType;
	String topup_number;
	String directPayment;
	String paymentProcessed;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCardToken() {
		return cardToken;
	}

	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getTopup_number() {
		return topup_number;
	}

	public void setTopup_number(String topup_number) {
		this.topup_number = topup_number;
	}

	public String getDirectPayment() {
		return directPayment;
	}

	public void setDirectPayment(String directPayment) {
		this.directPayment = directPayment;
	}

	public String getPaymentProcessed() {
		return paymentProcessed;
	}

	public void setPaymentProcessed(String paymentProcessed) {
		this.paymentProcessed = paymentProcessed;
	}

	@Override
	public String toString() {
		return "VerifyCardDetails [msisdn=" + msisdn + ", amount=" + amount + ", cardToken=" + cardToken
				+ ", paymentKey=" + paymentKey + ", cardType=" + cardType + ", topup_number=" + topup_number
				+ ", directPayment=" + directPayment + ", paymentProcessed=" + paymentProcessed + "]";
	}

}
