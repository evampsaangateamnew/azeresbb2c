package com.evampsaanga.azerfon.plasticcard.posting.recharge.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "envelope")
@XmlAccessorType(XmlAccessType.FIELD)
public class RechargeRequestEnvelope {
	public int reqtype;
	public String username;
	public String password;
	public RechargeRequestBody body;

	public int getReqtype() {
		return reqtype;
	}

	public void setReqtype(int reqtype) {
		this.reqtype = reqtype;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public RechargeRequestBody getBody() {
		return body;
	}

	public void setBody(RechargeRequestBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "RechargeRequestEnvelope [reqtype=" + reqtype + ", username=" + username + ", password=" + password + ", body=" + body
				+ "]";
	}
}
