package com.evampsaanga.azerfon.plasticcard.posting.payment.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "paymentresponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentResponse {
	public String date;
	public String txnid;
	public String inrefid;
	public String spid;
	public String serviceid;
	public String cause;
	public String msisdn;
	public String subtype;
	public String substatus;
	public String paymentamnt;
	public String parameterset;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getInrefid() {
		return inrefid;
	}

	public void setInrefid(String inrefid) {
		this.inrefid = inrefid;
	}

	public String getSpid() {
		return spid;
	}

	public void setSpid(String spid) {
		this.spid = spid;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public String getSubstatus() {
		return substatus;
	}

	public void setSubstatus(String substatus) {
		this.substatus = substatus;
	}

	public String getPaymentamnt() {
		return paymentamnt;
	}

	public void setPaymentamnt(String paymentamnt) {
		this.paymentamnt = paymentamnt;
	}

	public String getParameterset() {
		return parameterset;
	}

	public void setParameterset(String parameterset) {
		this.parameterset = parameterset;
	}

	@Override
	public String toString() {
		return "PaymentResponse [date=" + date + ", txnid=" + txnid + ", inrefid=" + inrefid + ", spid=" + spid
				+ ", serviceid=" + serviceid + ", cause=" + cause + ", msisdn=" + msisdn + ", subtype=" + subtype
				+ ", substatus=" + substatus + ", paymentamnt=" + paymentamnt + ", parameterset=" + parameterset + "]";
	}

}
