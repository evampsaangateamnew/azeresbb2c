package com.evampsaanga.azerfon.plasticcard.posting.queryrequest.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "checkresponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryStatusCheckResponse {

	public String date;
	public String txnid;
	public String inrefid;
	public String msisdn;
	public String accountid;
	public String spid;
	public String serviceid;
	public String cause;
	public String subtype;
	public String substatus;
	public String parameterset;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getInrefid() {
		return inrefid;
	}

	public void setInrefid(String inrefid) {
		this.inrefid = inrefid;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getAccountid() {
		return accountid;
	}

	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	public String getSpid() {
		return spid;
	}

	public void setSpid(String spid) {
		this.spid = spid;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public String getSubstatus() {
		return substatus;
	}

	public void setSubstatus(String substatus) {
		this.substatus = substatus;
	}

	public String getParameterset() {
		return parameterset;
	}

	public void setParameterset(String parameterset) {
		this.parameterset = parameterset;
	}

	@Override
	public String toString() {
		return "QueryStatusCheckResponse [date=" + date + ", txnid=" + txnid + ", inrefid=" + inrefid + ", msisdn="
				+ msisdn + ", accountid=" + accountid + ", spid=" + spid + ", serviceid=" + serviceid + ", cause="
				+ cause + ", subtype=" + subtype + ", substatus=" + substatus + ", parameterset=" + parameterset + "]";
	}

}
