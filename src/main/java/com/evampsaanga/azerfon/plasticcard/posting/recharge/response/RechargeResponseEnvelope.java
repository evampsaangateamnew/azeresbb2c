package com.evampsaanga.azerfon.plasticcard.posting.recharge.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "envelope")
@XmlAccessorType(XmlAccessType.FIELD)
public class RechargeResponseEnvelope {
	public int reqtype;
	public RechargeResponseBody body;

	public int getReqtype() {
		return reqtype;
	}

	public void setReqtype(int reqtype) {
		this.reqtype = reqtype;
	}

	public RechargeResponseBody getBody() {
		return body;
	}

	public void setBody(RechargeResponseBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "RechargeResponseEnvelope [reqtype=" + reqtype + ", body=" + body + "]";
	}

}
