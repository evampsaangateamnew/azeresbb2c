package com.evampsaanga.azerfon.plasticcard.posting.queryrequest.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType; 
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="body")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryStatusRequestBody {
	public QueryStatusCheckRequest checkrequest;

	public QueryStatusCheckRequest getCheckrequest() {
		return checkrequest;
	}

	public void setCheckrequest(QueryStatusCheckRequest checkrequest) {
		this.checkrequest = checkrequest;
	}

	@Override
	public String toString() {
		return "QueryStatusRequestBody [checkrequest=" + checkrequest + "]";
	}
}
