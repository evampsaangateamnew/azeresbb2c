package com.evampsaanga.azerfon.plasticcard.posting.queryrequest.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "checkRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryStatusCheckRequest {
	private String date;

	private String parameterset;

	private String serviceid;

	private String msisdn;

	private String spid;

	private String txnid;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getParameterset() {
		return parameterset;
	}

	public void setParameterset(String parameterset) {
		this.parameterset = parameterset;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getSpid() {
		return spid;
	}

	public void setSpid(String spid) {
		this.spid = spid;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	@Override
	public String toString() {
		return "QueryStatusCheckRequest [date=" + date + ", parameterset=" + parameterset + ", serviceid=" + serviceid
				+ ", msisdn=" + msisdn + ", spid=" + spid + ", txnid=" + txnid + "]";
	}

}