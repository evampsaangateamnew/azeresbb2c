package com.evampsaanga.azerfon.plasticcard.posting.recharge.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "paymentrequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class RechargeRequest {
	public String date;
	public String txnid;
	public String spid;
	public String serviceid;
	public String msisdn;
	public String accountid;
	public String paymentamnt;
	public String parameterset;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getSpid() {
		return spid;
	}

	public void setSpid(String spid) {
		this.spid = spid;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getAccountid() {
		return accountid;
	}

	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	public String getPaymentamnt() {
		return paymentamnt;
	}

	public void setPaymentamnt(String paymentamnt) {
		this.paymentamnt = paymentamnt;
	}

	public String getParameterset() {
		return parameterset;
	}

	public void setParameterset(String parameterset) {
		this.parameterset = parameterset;
	}

	@Override
	public String toString() {
		return "RechargeRequest [date=" + date + ", txnid=" + txnid + ", spid=" + spid + ", serviceid="
				+ serviceid + ", msisdn=" + msisdn + ", accountid=" + accountid + ", paymentamnt=" + paymentamnt
				+ ", parameterset=" + parameterset + "]";
	}

}
