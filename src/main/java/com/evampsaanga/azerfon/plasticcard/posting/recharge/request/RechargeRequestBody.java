package com.evampsaanga.azerfon.plasticcard.posting.recharge.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "body")
@XmlAccessorType(XmlAccessType.FIELD)
public class RechargeRequestBody {
	public RechargeRequest paymentrequest;

	public RechargeRequest getPaymentrequest() {
		return paymentrequest;
	}

	public void setPaymentrequest(RechargeRequest paymentrequest) {
		this.paymentrequest = paymentrequest;
	}

	@Override
	public String toString() {
		return "RechargeRequestBody [paymentrequest=" + paymentrequest + "]";
	}

}
