package com.evampsaanga.azerfon.plasticcard.posting.payment.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "envelope")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentResponseEnvelope {
	public int reqtype;
	public PaymentResponseBody body;
	public int getReqtype() {
		return reqtype;
	}
	public void setReqtype(int reqtype) {
		this.reqtype = reqtype;
	}
	public PaymentResponseBody getBody() {
		return body;
	}
	public void setBody(PaymentResponseBody body) {
		this.body = body;
	}
	@Override
	public String toString() {
		return "PaymentResponseEnvelope [reqtype=" + reqtype + ", body=" + body + "]";
	}
	
	
}
