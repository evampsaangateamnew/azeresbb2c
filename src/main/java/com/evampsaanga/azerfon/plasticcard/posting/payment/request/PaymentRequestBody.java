package com.evampsaanga.azerfon.plasticcard.posting.payment.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.azerfon.plasticcard.posting.payment.request.PaymentRequest;

@XmlRootElement(name = "body")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentRequestBody {

	public PaymentRequest paymentrequest;

	public PaymentRequest getPaymentrequest() {
		return paymentrequest;
	}

	public void setPaymentrequest(PaymentRequest paymentrequest) {
		this.paymentrequest = paymentrequest;
	}

	@Override
	public String toString() {
		return "PaymentRequestBody [paymentRequest=" + paymentrequest + "]";
	}

}
