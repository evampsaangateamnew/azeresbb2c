
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MakePaymentRP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MakePaymentRP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reqMakePayment" type="{http://webservice.web.pg.goldenpay.az/}gpmReqPayCs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MakePaymentRPRequest", propOrder = {
    "reqMakePayment"
})
public class MakePaymentRP {

    protected GpmReqPayCs reqMakePayment;

    /**
     * Gets the value of the reqMakePayment property.
     * 
     * @return
     *     possible object is
     *     {@link GpmReqPayCs }
     *     
     */
    public GpmReqPayCs getReqMakePayment() {
        return reqMakePayment;
    }

    /**
     * Sets the value of the reqMakePayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmReqPayCs }
     *     
     */
    public void setReqMakePayment(GpmReqPayCs value) {
        this.reqMakePayment = value;
    }

}
