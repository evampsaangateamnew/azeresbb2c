
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetEcommPaymentKeyRP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetEcommPaymentKeyRP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reqGetEcommPaymentKey" type="{http://webservice.web.pg.goldenpay.az/}gpmReqGetEcommPaymentKeyRest" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetEcommPaymentKeyRPRequest", propOrder = {
    "reqGetEcommPaymentKey"
})
public class GetEcommPaymentKeyRP {

    protected GpmReqGetEcommPaymentKeyRest reqGetEcommPaymentKey;

    /**
     * Gets the value of the reqGetEcommPaymentKey property.
     * 
     * @return
     *     possible object is
     *     {@link GpmReqGetEcommPaymentKeyRest }
     *     
     */
    public GpmReqGetEcommPaymentKeyRest getReqGetEcommPaymentKey() {
        return reqGetEcommPaymentKey;
    }

    /**
     * Sets the value of the reqGetEcommPaymentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmReqGetEcommPaymentKeyRest }
     *     
     */
    public void setReqGetEcommPaymentKey(GpmReqGetEcommPaymentKeyRest value) {
        this.reqGetEcommPaymentKey = value;
    }

}
