
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReverseRP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReverseRP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reqReversePaymentKey" type="{http://webservice.web.pg.goldenpay.az/}gpmReqReversePaymentKey" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReverseRPRequest", propOrder = {
    "reqReversePaymentKey"
})
public class ReverseRP {

    protected GpmReqReversePaymentKey reqReversePaymentKey;

    /**
     * Gets the value of the reqReversePaymentKey property.
     * 
     * @return
     *     possible object is
     *     {@link GpmReqReversePaymentKey }
     *     
     */
    public GpmReqReversePaymentKey getReqReversePaymentKey() {
        return reqReversePaymentKey;
    }

    /**
     * Sets the value of the reqReversePaymentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmReqReversePaymentKey }
     *     
     */
    public void setReqReversePaymentKey(GpmReqReversePaymentKey value) {
        this.reqReversePaymentKey = value;
    }

}
