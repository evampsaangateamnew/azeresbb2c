
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.evampsaanga.bakcell.plasticcard package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ChangeCardStatusRP_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "ChangeCardStatusRP");
    private final static QName _ChangeCardStatusRPResponse_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "ChangeCardStatusRPResponse");
    private final static QName _GetCardTokenRP_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "GetCardTokenRP");
    private final static QName _GetCardTokenRPResponse_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "GetCardTokenRPResponse");
    private final static QName _GetEcommPaymentKeyRP_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "GetEcommPaymentKeyRP");
    private final static QName _GetEcommPaymentKeyRPResponse_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "GetEcommPaymentKeyRPResponse");
    private final static QName _GetPaymentResultRP_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "GetPaymentResultRP");
    private final static QName _GetPaymentResultRPResponse_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "GetPaymentResultRPResponse");
    private final static QName _MakePaymentRP_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "MakePaymentRP");
    private final static QName _MakePaymentRPResponse_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "MakePaymentRPResponse");
    private final static QName _RegisterCardRP_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "RegisterCardRP");
    private final static QName _RegisterCardRPResponse_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "RegisterCardRPResponse");
    private final static QName _ReverseRP_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "ReverseRP");
    private final static QName _ReverseRPResponse_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "ReverseRPResponse");
    private final static QName _VerifyCardRP_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "VerifyCardRP");
    private final static QName _VerifyCardRPResponse_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "VerifyCardRPResponse");
    private final static QName _CardTokenResult_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "cardTokenResult");
    private final static QName _PaymentItem_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "paymentItem");
    private final static QName _PaymentKeyResult_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "paymentKeyResult");
    private final static QName _PaymentResult_QNAME = new QName("http://webservice.web.pg.goldenpay.az/", "paymentResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.evampsaanga.bakcell.plasticcard
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChangeCardStatusRP }
     * 
     */
    public ChangeCardStatusRP createChangeCardStatusRP() {
        return new ChangeCardStatusRP();
    }

    /**
     * Create an instance of {@link ChangeCardStatusRPResponse }
     * 
     */
    public ChangeCardStatusRPResponse createChangeCardStatusRPResponse() {
        return new ChangeCardStatusRPResponse();
    }

    /**
     * Create an instance of {@link GetCardTokenRP }
     * 
     */
    public GetCardTokenRP createGetCardTokenRP() {
        return new GetCardTokenRP();
    }

    /**
     * Create an instance of {@link GetCardTokenRPResponse }
     * 
     */
    public GetCardTokenRPResponse createGetCardTokenRPResponse() {
        return new GetCardTokenRPResponse();
    }

    /**
     * Create an instance of {@link GetEcommPaymentKeyRP }
     * 
     */
    public GetEcommPaymentKeyRP createGetEcommPaymentKeyRP() {
        return new GetEcommPaymentKeyRP();
    }

    /**
     * Create an instance of {@link GetEcommPaymentKeyRPResponse }
     * 
     */
    public GetEcommPaymentKeyRPResponse createGetEcommPaymentKeyRPResponse() {
        return new GetEcommPaymentKeyRPResponse();
    }

    /**
     * Create an instance of {@link GetPaymentResultRP }
     * 
     */
    public GetPaymentResultRP createGetPaymentResultRP() {
        return new GetPaymentResultRP();
    }

    /**
     * Create an instance of {@link GetPaymentResultRPResponse }
     * 
     */
    public GetPaymentResultRPResponse createGetPaymentResultRPResponse() {
        return new GetPaymentResultRPResponse();
    }

    /**
     * Create an instance of {@link MakePaymentRP }
     * 
     */
    public MakePaymentRP createMakePaymentRP() {
        return new MakePaymentRP();
    }

    /**
     * Create an instance of {@link MakePaymentRPResponse }
     * 
     */
    public MakePaymentRPResponse createMakePaymentRPResponse() {
        return new MakePaymentRPResponse();
    }

    /**
     * Create an instance of {@link RegisterCardRP }
     * 
     */
    public RegisterCardRP createRegisterCardRP() {
        return new RegisterCardRP();
    }

    /**
     * Create an instance of {@link RegisterCardRPResponse }
     * 
     */
    public RegisterCardRPResponse createRegisterCardRPResponse() {
        return new RegisterCardRPResponse();
    }

    /**
     * Create an instance of {@link ReverseRP }
     * 
     */
    public ReverseRP createReverseRP() {
        return new ReverseRP();
    }

    /**
     * Create an instance of {@link ReverseRPResponse }
     * 
     */
    public ReverseRPResponse createReverseRPResponse() {
        return new ReverseRPResponse();
    }

    /**
     * Create an instance of {@link VerifyCardRP }
     * 
     */
    public VerifyCardRP createVerifyCardRP() {
        return new VerifyCardRP();
    }

    /**
     * Create an instance of {@link VerifyCardRPResponse }
     * 
     */
    public VerifyCardRPResponse createVerifyCardRPResponse() {
        return new VerifyCardRPResponse();
    }

    /**
     * Create an instance of {@link GpmRespGetCardToken }
     * 
     */
    public GpmRespGetCardToken createGpmRespGetCardToken() {
        return new GpmRespGetCardToken();
    }

    /**
     * Create an instance of {@link GpmRespGetEcommPaymentKey }
     * 
     */
    public GpmRespGetEcommPaymentKey createGpmRespGetEcommPaymentKey() {
        return new GpmRespGetEcommPaymentKey();
    }

    /**
     * Create an instance of {@link GpmRespGetEcommPaymentResultRP }
     * 
     */
    public GpmRespGetEcommPaymentResultRP createGpmRespGetEcommPaymentResultRP() {
        return new GpmRespGetEcommPaymentResultRP();
    }

    /**
     * Create an instance of {@link GpmReqUserInfo }
     * 
     */
    public GpmReqUserInfo createGpmReqUserInfo() {
        return new GpmReqUserInfo();
    }

    /**
     * Create an instance of {@link GpmStatus }
     * 
     */
    public GpmStatus createGpmStatus() {
        return new GpmStatus();
    }

    /**
     * Create an instance of {@link GpmReqChangeCardStatus }
     * 
     */
    public GpmReqChangeCardStatus createGpmReqChangeCardStatus() {
        return new GpmReqChangeCardStatus();
    }

    /**
     * Create an instance of {@link GpmReqPayCs }
     * 
     */
    public GpmReqPayCs createGpmReqPayCs() {
        return new GpmReqPayCs();
    }

    /**
     * Create an instance of {@link GpmReqReversePaymentKey }
     * 
     */
    public GpmReqReversePaymentKey createGpmReqReversePaymentKey() {
        return new GpmReqReversePaymentKey();
    }

    /**
     * Create an instance of {@link GpmReqGetCardTokenInfo }
     * 
     */
    public GpmReqGetCardTokenInfo createGpmReqGetCardTokenInfo() {
        return new GpmReqGetCardTokenInfo();
    }

    /**
     * Create an instance of {@link GpmCardSecurityStatus }
     * 
     */
    public GpmCardSecurityStatus createGpmCardSecurityStatus() {
        return new GpmCardSecurityStatus();
    }

    /**
     * Create an instance of {@link GpmReqGetEcommPaymentKeyRest }
     * 
     */
    public GpmReqGetEcommPaymentKeyRest createGpmReqGetEcommPaymentKeyRest() {
        return new GpmReqGetEcommPaymentKeyRest();
    }

    /**
     * Create an instance of {@link GpmReqVerifyCardInfo }
     * 
     */
    public GpmReqVerifyCardInfo createGpmReqVerifyCardInfo() {
        return new GpmReqVerifyCardInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCardStatusRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "ChangeCardStatusRP")
    public JAXBElement<ChangeCardStatusRP> createChangeCardStatusRP(ChangeCardStatusRP value) {
        return new JAXBElement<ChangeCardStatusRP>(_ChangeCardStatusRP_QNAME, ChangeCardStatusRP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCardStatusRPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "ChangeCardStatusRPResponse")
    public JAXBElement<ChangeCardStatusRPResponse> createChangeCardStatusRPResponse(ChangeCardStatusRPResponse value) {
        return new JAXBElement<ChangeCardStatusRPResponse>(_ChangeCardStatusRPResponse_QNAME, ChangeCardStatusRPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardTokenRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "GetCardTokenRP")
    public JAXBElement<GetCardTokenRP> createGetCardTokenRP(GetCardTokenRP value) {
        return new JAXBElement<GetCardTokenRP>(_GetCardTokenRP_QNAME, GetCardTokenRP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardTokenRPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "GetCardTokenRPResponse")
    public JAXBElement<GetCardTokenRPResponse> createGetCardTokenRPResponse(GetCardTokenRPResponse value) {
        return new JAXBElement<GetCardTokenRPResponse>(_GetCardTokenRPResponse_QNAME, GetCardTokenRPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEcommPaymentKeyRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "GetEcommPaymentKeyRP")
    public JAXBElement<GetEcommPaymentKeyRP> createGetEcommPaymentKeyRP(GetEcommPaymentKeyRP value) {
        return new JAXBElement<GetEcommPaymentKeyRP>(_GetEcommPaymentKeyRP_QNAME, GetEcommPaymentKeyRP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEcommPaymentKeyRPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "GetEcommPaymentKeyRPResponse")
    public JAXBElement<GetEcommPaymentKeyRPResponse> createGetEcommPaymentKeyRPResponse(GetEcommPaymentKeyRPResponse value) {
        return new JAXBElement<GetEcommPaymentKeyRPResponse>(_GetEcommPaymentKeyRPResponse_QNAME, GetEcommPaymentKeyRPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentResultRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "GetPaymentResultRP")
    public JAXBElement<GetPaymentResultRP> createGetPaymentResultRP(GetPaymentResultRP value) {
        return new JAXBElement<GetPaymentResultRP>(_GetPaymentResultRP_QNAME, GetPaymentResultRP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPaymentResultRPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "GetPaymentResultRPResponse")
    public JAXBElement<GetPaymentResultRPResponse> createGetPaymentResultRPResponse(GetPaymentResultRPResponse value) {
        return new JAXBElement<GetPaymentResultRPResponse>(_GetPaymentResultRPResponse_QNAME, GetPaymentResultRPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakePaymentRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "MakePaymentRP")
    public JAXBElement<MakePaymentRP> createMakePaymentRP(MakePaymentRP value) {
        return new JAXBElement<MakePaymentRP>(_MakePaymentRP_QNAME, MakePaymentRP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakePaymentRPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "MakePaymentRPResponse")
    public JAXBElement<MakePaymentRPResponse> createMakePaymentRPResponse(MakePaymentRPResponse value) {
        return new JAXBElement<MakePaymentRPResponse>(_MakePaymentRPResponse_QNAME, MakePaymentRPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterCardRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "RegisterCardRP")
    public JAXBElement<RegisterCardRP> createRegisterCardRP(RegisterCardRP value) {
        return new JAXBElement<RegisterCardRP>(_RegisterCardRP_QNAME, RegisterCardRP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterCardRPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "RegisterCardRPResponse")
    public JAXBElement<RegisterCardRPResponse> createRegisterCardRPResponse(RegisterCardRPResponse value) {
        return new JAXBElement<RegisterCardRPResponse>(_RegisterCardRPResponse_QNAME, RegisterCardRPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReverseRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "ReverseRP")
    public JAXBElement<ReverseRP> createReverseRP(ReverseRP value) {
        return new JAXBElement<ReverseRP>(_ReverseRP_QNAME, ReverseRP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReverseRPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "ReverseRPResponse")
    public JAXBElement<ReverseRPResponse> createReverseRPResponse(ReverseRPResponse value) {
        return new JAXBElement<ReverseRPResponse>(_ReverseRPResponse_QNAME, ReverseRPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyCardRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "VerifyCardRP")
    public JAXBElement<VerifyCardRP> createVerifyCardRP(VerifyCardRP value) {
        return new JAXBElement<VerifyCardRP>(_VerifyCardRP_QNAME, VerifyCardRP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyCardRPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "VerifyCardRPResponse")
    public JAXBElement<VerifyCardRPResponse> createVerifyCardRPResponse(VerifyCardRPResponse value) {
        return new JAXBElement<VerifyCardRPResponse>(_VerifyCardRPResponse_QNAME, VerifyCardRPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GpmRespGetCardToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "cardTokenResult")
    public JAXBElement<GpmRespGetCardToken> createCardTokenResult(GpmRespGetCardToken value) {
        return new JAXBElement<GpmRespGetCardToken>(_CardTokenResult_QNAME, GpmRespGetCardToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "paymentItem")
    public JAXBElement<Object> createPaymentItem(Object value) {
        return new JAXBElement<Object>(_PaymentItem_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GpmRespGetEcommPaymentKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "paymentKeyResult")
    public JAXBElement<GpmRespGetEcommPaymentKey> createPaymentKeyResult(GpmRespGetEcommPaymentKey value) {
        return new JAXBElement<GpmRespGetEcommPaymentKey>(_PaymentKeyResult_QNAME, GpmRespGetEcommPaymentKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GpmRespGetEcommPaymentResultRP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.web.pg.goldenpay.az/", name = "paymentResult")
    public JAXBElement<GpmRespGetEcommPaymentResultRP> createPaymentResult(GpmRespGetEcommPaymentResultRP value) {
        return new JAXBElement<GpmRespGetEcommPaymentResultRP>(_PaymentResult_QNAME, GpmRespGetEcommPaymentResultRP.class, null, value);
    }

}
