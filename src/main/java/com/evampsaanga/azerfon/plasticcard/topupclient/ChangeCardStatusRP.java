
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeCardStatusRP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeCardStatusRP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reqChangeCardStatus" type="{http://webservice.web.pg.goldenpay.az/}gpmReqChangeCardStatus" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeCardStatusRPRequest", propOrder = {
    "reqChangeCardStatus"
})
public class ChangeCardStatusRP {

    protected GpmReqChangeCardStatus reqChangeCardStatus;

    /**
     * Gets the value of the reqChangeCardStatus property.
     * 
     * @return
     *     possible object is
     *     {@link GpmReqChangeCardStatus }
     *     
     */
    public GpmReqChangeCardStatus getReqChangeCardStatus() {
        return reqChangeCardStatus;
    }

    /**
     * Sets the value of the reqChangeCardStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmReqChangeCardStatus }
     *     
     */
    public void setReqChangeCardStatus(GpmReqChangeCardStatus value) {
        this.reqChangeCardStatus = value;
    }

}
