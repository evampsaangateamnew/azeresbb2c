
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegisterCardRP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegisterCardRP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reqUserInfo" type="{http://webservice.web.pg.goldenpay.az/}gpmReqUserInfo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegisterCardRPRequest", propOrder = {
    "reqUserInfo"
})
public class RegisterCardRP {

    protected GpmReqUserInfo reqUserInfo;

    /**
     * Gets the value of the reqUserInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GpmReqUserInfo }
     *     
     */
    public GpmReqUserInfo getReqUserInfo() {
        return reqUserInfo;
    }

    /**
     * Sets the value of the reqUserInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmReqUserInfo }
     *     
     */
    public void setReqUserInfo(GpmReqUserInfo value) {
        this.reqUserInfo = value;
    }

}
