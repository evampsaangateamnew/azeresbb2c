
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VerifyCardRP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyCardRP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reqVerifyCardInfo" type="{http://webservice.web.pg.goldenpay.az/}gpmReqVerifyCardInfo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyCardRPRequest", propOrder = {
    "reqVerifyCardInfo"
})
public class VerifyCardRP {

    protected GpmReqVerifyCardInfo reqVerifyCardInfo;

    /**
     * Gets the value of the reqVerifyCardInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GpmReqVerifyCardInfo }
     *     
     */
    public GpmReqVerifyCardInfo getReqVerifyCardInfo() {
        return reqVerifyCardInfo;
    }

    /**
     * Sets the value of the reqVerifyCardInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmReqVerifyCardInfo }
     *     
     */
    public void setReqVerifyCardInfo(GpmReqVerifyCardInfo value) {
        this.reqVerifyCardInfo = value;
    }

}
