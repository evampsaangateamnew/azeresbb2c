
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gpmRespGetCardToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gpmRespGetCardToken"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="status" type="{http://webservice.web.pg.goldenpay.az/}gpmStatus" minOccurs="0"/&gt;
 *         &lt;element name="cardSecurityStatus" type="{http://webservice.web.pg.goldenpay.az/}gpmCardSecurityStatus" minOccurs="0"/&gt;
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cardToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gpmRespGetCardToken", propOrder = {
    "status",
    "cardSecurityStatus",
    "cardNumber",
    "cardToken"
})
public class GpmRespGetCardToken {

    protected GpmStatus status;
    protected GpmCardSecurityStatus cardSecurityStatus;
    protected String cardNumber;
    protected String cardToken;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link GpmStatus }
     *     
     */
    public GpmStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmStatus }
     *     
     */
    public void setStatus(GpmStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the cardSecurityStatus property.
     * 
     * @return
     *     possible object is
     *     {@link GpmCardSecurityStatus }
     *     
     */
    public GpmCardSecurityStatus getCardSecurityStatus() {
        return cardSecurityStatus;
    }

    /**
     * Sets the value of the cardSecurityStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmCardSecurityStatus }
     *     
     */
    public void setCardSecurityStatus(GpmCardSecurityStatus value) {
        this.cardSecurityStatus = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the cardToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardToken() {
        return cardToken;
    }

    /**
     * Sets the value of the cardToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardToken(String value) {
        this.cardToken = value;
    }

}
