
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCardTokenRP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCardTokenRP"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reqGetCardTokenInfo" type="{http://webservice.web.pg.goldenpay.az/}gpmReqGetCardTokenInfo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCardTokenRPRequest", propOrder = {
    "reqGetCardTokenInfo"
})
public class GetCardTokenRP {

    protected GpmReqGetCardTokenInfo reqGetCardTokenInfo;

    /**
     * Gets the value of the reqGetCardTokenInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GpmReqGetCardTokenInfo }
     *     
     */
    public GpmReqGetCardTokenInfo getReqGetCardTokenInfo() {
        return reqGetCardTokenInfo;
    }

    /**
     * Sets the value of the reqGetCardTokenInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmReqGetCardTokenInfo }
     *     
     */
    public void setReqGetCardTokenInfo(GpmReqGetCardTokenInfo value) {
        this.reqGetCardTokenInfo = value;
    }

}
