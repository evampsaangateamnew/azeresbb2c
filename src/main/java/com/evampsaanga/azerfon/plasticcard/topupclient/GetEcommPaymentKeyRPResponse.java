
package com.evampsaanga.azerfon.plasticcard.topupclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetEcommPaymentKeyRPResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetEcommPaymentKeyRPResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://webservice.web.pg.goldenpay.az/}gpmRespGetEcommPaymentKey" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetEcommPaymentKeyRPResponseEntity", propOrder = {
    "_return"
})
public class GetEcommPaymentKeyRPResponse {

    @XmlElement(name = "return")
    protected GpmRespGetEcommPaymentKey _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GpmRespGetEcommPaymentKey }
     *     
     */
    public GpmRespGetEcommPaymentKey getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GpmRespGetEcommPaymentKey }
     *     
     */
    public void setReturn(GpmRespGetEcommPaymentKey value) {
        this._return = value;
    }

}
