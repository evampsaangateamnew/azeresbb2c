package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class MakePaymentProcessResponse extends BaseResponse{

	@Override
	public String toString() {
		return "MakePaymentProcessResponse [toString()=" + super.toString() + "]";
	}

}
