package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class MakePaymentProcessRequest extends BaseRequest {

	String paymentKey;
	String amount;
	String topupNumber;

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTopupNumber() {
		return topupNumber;
	}

	public void setTopupNumber(String topupNumber) {
		this.topupNumber = topupNumber;
	}

	@Override
	public String toString() {
		return "MakePaymentProcessRequest [paymentKey=" + paymentKey + ", amount=" + amount + ", topupNumber="
				+ topupNumber + ", toString()=" + super.toString() + "]";
	}

}
