package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class PlasticCardGetCardTokenRequest extends BaseRequest {
	int id;// the last record inserted primary key.
	String paymentKey;
	boolean paymentStatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public boolean isPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@Override
	public String toString() {
		return "PlasticCardGetCardTokenRequest [id=" + id + ", paymentKey=" + paymentKey + ", paymentStatus="
				+ paymentStatus + ", toString()=" + super.toString() + "]";
	}

	

}
