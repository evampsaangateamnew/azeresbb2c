package com.evampsaanga.azerfon.plasticcard;

public class FastPaymentDetails {
	private int id;
	private String cardType;
	private Double amount;
	private String topupNumber;
	private String paymentKey;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getTopupNumber() {
		return topupNumber;
	}

	public void setTopupNumber(String topupNumber) {
		this.topupNumber = topupNumber;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	@Override
	public String toString() {
		return "FastPaymentDetails [id=" + id + ", cardType=" + cardType + ", amount=" + amount + ", topupNumber="
				+ topupNumber + ", paymentKey=" + paymentKey + "]";
	}

	

}