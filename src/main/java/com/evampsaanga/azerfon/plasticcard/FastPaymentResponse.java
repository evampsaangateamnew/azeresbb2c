package com.evampsaanga.azerfon.plasticcard;

import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class FastPaymentResponse extends BaseResponse {
	private List<FastPaymentDetails> fastPaymentDetails;

	public List<FastPaymentDetails> getFastPaymentDetails() {
		return fastPaymentDetails;
	}

	public void setFastPaymentDetails(List<FastPaymentDetails> fastPaymentDetails) {
		this.fastPaymentDetails = fastPaymentDetails;
	}

	@Override
	public String toString() {
		return "FastPaymentResponse [fastPaymentDetails=" + fastPaymentDetails + ", toString()=" + super.toString()
				+ "]";
	}
}
