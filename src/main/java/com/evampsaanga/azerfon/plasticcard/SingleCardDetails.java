package com.evampsaanga.azerfon.plasticcard;

public class SingleCardDetails {
	private int id;
	private String cardMaskNumber;
	private String cardType;
	private String paymentKey;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCardMaskNumber() {
		return cardMaskNumber;
	}

	public void setCardMaskNumber(String cardMaskNumber) {
		this.cardMaskNumber = cardMaskNumber;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	@Override
	public String toString() {
		return "SingleCardDetails [id=" + id + ", cardMaskNumber=" + cardMaskNumber + ", cardType=" + cardType
				+ ", paymentKey=" + paymentKey + ", toString()=" + super.toString() + "]";
	}

	

}
