package com.evampsaanga.azerfon.plasticcard;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.plasticcard.posting.payment.request.PaymentRequestEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.payment.response.PaymentResponseEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.queryrequest.request.QueryStatusRequestEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.queryrequest.response.QueryStatusResponseEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.recharge.request.RechargeRequestEnvelope;
import com.evampsaanga.azerfon.plasticcard.posting.recharge.response.RechargeResponseEnvelope;
import com.evampsaanga.developer.utils.Helper;

public class MarshallerParser {

	static Logger logger = Logger.getLogger("plasticcardlogs");

	public static String getObjectToXML(Object envelope) {
		String xmlContent = "";
		JAXBContext jaxbContext = null;

		try {
			if (envelope instanceof QueryStatusRequestEnvelope)
				jaxbContext = JAXBContext.newInstance(QueryStatusRequestEnvelope.class);

			if (envelope instanceof RechargeRequestEnvelope)
				jaxbContext = JAXBContext.newInstance(RechargeRequestEnvelope.class);

			if (envelope instanceof PaymentRequestEnvelope)
				jaxbContext = JAXBContext.newInstance(PaymentRequestEnvelope.class);

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter sw = new StringWriter();

			jaxbMarshaller.marshal(envelope, sw);

			xmlContent = sw.toString();

		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlContent;
	}

	public static Object getXMLToObject(String XML, Object object) {
		JAXBContext jaxbContext = null;

		try {
			if (object instanceof QueryStatusResponseEnvelope) {

				logger.info("unmarchalling Query Statys Response Envelop");
				jaxbContext = JAXBContext.newInstance(QueryStatusResponseEnvelope.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				return (QueryStatusResponseEnvelope) jaxbUnmarshaller.unmarshal(new StringReader(XML));
			}

			if (object instanceof RechargeResponseEnvelope) {
				logger.info("unmarchalling RechargeResponseEnvelope");
				jaxbContext = JAXBContext.newInstance(RechargeResponseEnvelope.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				return (RechargeResponseEnvelope) jaxbUnmarshaller.unmarshal(new StringReader(XML));
			}

			if (object instanceof PaymentResponseEnvelope) {
				logger.info("unmarchalling PaymentResponseEnvelope");
				jaxbContext = JAXBContext.newInstance(PaymentResponseEnvelope.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				return (PaymentResponseEnvelope) jaxbUnmarshaller.unmarshal(new StringReader(XML));
			}

		} catch (JAXBException e) {
			logger.error(Helper.GetException(e));
		}
		return jaxbContext;

	}
}
