package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class DeletePaymentSchedulerRequest extends BaseRequest {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeletePaymentSchedulerRequest [id=" + id + ", toString()=" + super.toString() + "]";
	}
	
}
