package com.evampsaanga.azerfon.plasticcard;

import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class SavedCardResponse extends BaseResponse {
	private List<SingleCardDetails> singleCardDetails;
	private String lastAmount;

	public List<SingleCardDetails> getSingleCardDetails() {
		return singleCardDetails;
	}

	public void setSingleCardDetails(List<SingleCardDetails> singleCardDetails) {
		this.singleCardDetails = singleCardDetails;
	}

	public String getLastAmount() {
		return lastAmount;
	}

	public void setLastAmount(String lastAmount) {
		this.lastAmount = lastAmount;
	}

	@Override
	public String toString() {
		return "SavedCardResponse [singleCardDetails=" + singleCardDetails + ", lastAmount=" + lastAmount
				+ ", toString()=" + super.toString() + "]";
	}

}