package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class DeleteFastTopupResponse extends BaseResponse{

	@Override
	public String toString() {
		return "DeleteFastTopupResponse [toString()=" + super.toString() + "]";
	}
}
