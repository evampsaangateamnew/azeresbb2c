package com.evampsaanga.azerfon.plasticcard;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class PaymentInitiateResponse extends BaseResponse {
	private String payment_key1 = "";

	public String getPayment_key1() {
		return payment_key1;
	}

	public void setPayment_key1(String payment_key1) {
		this.payment_key1 = payment_key1;
	}

	@Override
	public String toString() {
		return "PlasticCardResponse [payment_key1=" + payment_key1 + ", toString()=" + super.toString() + "]";
	}

}
