package com.evampsaanga.azerfon.plasticcard;

public class CardDetails {

	int id;
	String msisdn;
	String paymentKey;
	String date_time;
	int card_type;
	String card_token;
	String card_mask_number;

	public CardDetails() {
	}

	public CardDetails(int id, String msisdn, String paymentKey, String date_time, int card_type, String card_token,
			String card_mask_number) {
		super();
		this.id = id;
		this.msisdn = msisdn;
		this.paymentKey = paymentKey;
		this.date_time = date_time;
		this.card_type = card_type;
		this.card_token = card_token;
		this.card_mask_number = card_mask_number;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public String getDate_time() {
		return date_time;
	}

	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}

	public int getCard_type() {
		return card_type;
	}

	public void setCard_type(int card_type) {
		this.card_type = card_type;
	}

	public String getCard_token() {
		return card_token;
	}

	public void setCard_token(String card_token) {
		this.card_token = card_token;
	}

	public String getCard_mask_number() {
		return card_mask_number;
	}

	public void setCard_mask_number(String card_mask_number) {
		this.card_mask_number = card_mask_number;
	}

	@Override
	public String toString() {
		return "CardDetails [id=" + id + ", msisdn=" + msisdn + ", paymentKey=" + paymentKey + ", date_time="
				+ date_time + ", card_type=" + card_type + ", card_token=" + card_token + ", card_mask_number="
				+ card_mask_number + "]";
	}

}
