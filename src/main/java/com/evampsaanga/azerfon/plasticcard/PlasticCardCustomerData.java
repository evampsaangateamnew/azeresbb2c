package com.evampsaanga.azerfon.plasticcard;

public class PlasticCardCustomerData {
	private String firstName;
	private String LastName;
	private String status;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PlasticCardCustomerData [firstName=" + firstName + ", LastName=" + LastName + ", status=" + status
				+ "]";
	}

}
