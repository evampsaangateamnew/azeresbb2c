package com.evampsaanga.azerfon.getvalues.exchangeservice;

public class DataVoiceValues {
	
	private String data;
	private String voice;
	private String dataUnit;
	private String voiceUnit;
	
	public String getDataUnit() {
		return dataUnit;
	}
	public void setDataUnit(String dataUnit) {
		this.dataUnit = dataUnit;
	}
	public String getVoiceUnit() {
		return voiceUnit;
	}
	public void setVoiceUnit(String voiceUnit) {
		this.voiceUnit = voiceUnit;
	}
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getVoice() {
		return voice;
	}
	public void setVoice(String voice) {
		this.voice = voice;
	}


}
