package com.evampsaanga.azerfon.getvalues.exchangeservice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Helper;


public class ExchangeServiceDeleteRecordsLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	Logs logs = new Logs();

	String token = "";
	String TrnsactionName = Transactions.EXCHANGE_SERVICE_DELETE_RECORDS_TRANSACTION_NAME;
	public void deleteRecord()
	{
		token = Helper.retrieveToken(TrnsactionName, "ExchangeDelete");
         String daysValue="60";
		logger.info(token + "Request Landed on ExchangeServiceDeleteRecordsLand:" );
		String query="DELETE FROM exchangeservices_usercount WHERE created_at < NOW() - INTERVAL '"+daysValue+"' DAY";
		try (Connection con = DBFactory.getDbConnection(); PreparedStatement stmt = con.prepareStatement(query);) {
			logger.info(token+"Executed For  " + stmt.executeUpdate());
			stmt.close();
		} catch (SQLException ex) {
			logger.error(token+Helper.GetException(ex));
		}
	}

}
