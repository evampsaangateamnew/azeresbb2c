package com.evampsaanga.azerfon.getvalues.exchangeservice;

import java.util.ArrayList;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class ExchangeServicesValuesResponse extends BaseResponse {
	
	
	public String getIsServiceEnabled() {
		return isServiceEnabled;
	}
	public void setIsServiceEnabled(String isServiceEnabled) {
		this.isServiceEnabled = isServiceEnabled;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public ArrayList<DataVoiceValues> getVoicevalues() {
		return voicevalues;
	}
	public void setVoicevalues(ArrayList<DataVoiceValues> voicevalues) {
		this.voicevalues = voicevalues;
	}
	public ArrayList<DataVoiceValues> getDatavalues() {
		return datavalues;
	}
	public void setDatavalues(ArrayList<DataVoiceValues> datavalues) {
		this.datavalues = datavalues;
	}
	private String isServiceEnabled;
	private String count;
	private ArrayList<DataVoiceValues> voicevalues=new ArrayList<>();
	private ArrayList<DataVoiceValues> datavalues=new ArrayList<>();





}
