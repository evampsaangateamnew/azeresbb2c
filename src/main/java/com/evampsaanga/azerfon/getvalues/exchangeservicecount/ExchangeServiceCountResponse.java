package com.evampsaanga.azerfon.getvalues.exchangeservicecount;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class ExchangeServiceCountResponse extends BaseResponse {
	private String count;
	

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	

}
