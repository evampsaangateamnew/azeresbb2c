package com.evampsaanga.azerfon.getvalues.exchangeservicecount;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Path("/azerfon")
public class ExchangeServiceCountLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ExchangeServiceCountResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_FNF_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_FNF);
		logs.setTableType(LogsType.GetFnF);
		ExchangeServiceCountRequest cclient = null;
		ExchangeServiceCountResponse resp = new ExchangeServiceCountResponse();
		String token = "";
		String TrnsactionName = Transactions.EXCHANGE_SERVICE_COUNT_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			logger.info(token + "Request lanaded in ExchangeServiceCountLand :" + requestBody);
			try {
				cclient = Helper.JsonToObject(requestBody, ExchangeServiceCountRequest.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.EXCHANGE_SERVICE_COUNT_TRANSACTION_NAME);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (JsonParseException ex1) {
				logger.info(token + Helper.GetException(ex1));

				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} catch (JsonMappingException ex1) {

				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logger.info(token + Helper.GetException(ex1));
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} catch (IOException ex1) {

				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logger.info(token + Helper.GetException(ex1));
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {

					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

					logs.updateLog(logs);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {

					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					/*java.sql.PreparedStatement preparedStatement = null;
					java.sql.ResultSet resultSet = null;
					String sql = "SELECT DATEDIFF(NOW(), created_at) AS days ,count FROM exchangeservices_usercount WHERE msisdn=?";
					preparedStatement = DBFactory.getDbConnection().prepareStatement(sql);
					preparedStatement.setString(1, cclient.getmsisdn());
					resultSet = preparedStatement.executeQuery();
					String count = null;
					String days = null;
					while (resultSet.next()) {
						count = resultSet.getString("count");
						days = resultSet.getString("days");
					}
					logger.info(token + "COUNT" + count);
					logger.info(token + "Days" + days);
					int countService = Integer.parseInt(count);
					int daysService = Integer.parseInt(days);
					if (daysService > 30) {
						resp.setCount("0");
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

					} else {
						resp.setCount(countService + "");
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

					}
					logger.info(token + Helper.ObjectToJson(resp));*/
					return exchangecount(cclient.getmsisdn(), token);

				} else {

					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
		}

		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);

		return resp;
	}

	public static ExchangeServiceCountResponse exchangecount(String msisdn, String token) {
		ExchangeServiceCountResponse resp = new ExchangeServiceCountResponse();
		
		// String sql="SELECT DATEDIFF(NOW(), created_at) AS days ,count As
		// count FROM exchangeservices_usercount WHERE msisdn=?";
		java.sql.ResultSet resultSet =null;
		String sql = "SELECT count(*) as count FROM exchangeservices_usercount WHERE created_at BETWEEN NOW() - INTERVAL 30 DAY AND NOW() AND msisdn = ?";
		try (PreparedStatement preparedStatement = DBFactory.getDbConnection().prepareStatement(sql);) 
		{
//			java.sql.PreparedStatement preparedStatement = null;
			 
//			preparedStatement = DBFactory.getDbConnection().prepareStatement(sql);

			preparedStatement.setString(1, msisdn);
			resultSet = preparedStatement.executeQuery();
			String count = null;
			String days = null;
			if (resultSet.next()) {
				logger.info(token + "Result Result Exist :" + resultSet);
				count = resultSet.getString("count");
				// days=resultSet.getString("days");
				logger.info(token + "COUNT" + count);
				logger.info(token + "Days" + days);
				int countService = Integer.parseInt(count);

				resp.setCount(countService + "");

				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

			} else {
				logger.info(" Result DOES NOT Exist :" + resultSet);
				// no record exists in database
				resp.setReturnCode(ResponseCodes.EXCHANGE_SERVICE_RECORD_DOESNT_EXIST_CODE);
				resp.setReturnMsg(ResponseCodes.EXCHANGE_SERVICE_RECORD_DOESNT_EXIST_DESC);

			}
			
			logger.info(token + "Response From ExchangeService Count: " + Helper.ObjectToJson(resp));
			
		}

		catch (Exception e) {

			logger.info(token + Helper.GetException(e));
		}
		finally {
			if(resultSet!=null)
				try {
					resultSet.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info(Helper.GetException(e));
				}
		}
		
		return resp;
	}

}
