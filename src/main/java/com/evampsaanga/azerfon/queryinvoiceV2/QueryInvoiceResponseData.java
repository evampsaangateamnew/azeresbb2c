package com.evampsaanga.azerfon.queryinvoiceV2;

public class QueryInvoiceResponseData {
	private String invoiceAmount;
	private String invoiceDate;
	private String dueDate;
	private String SettleDate;
	private String invoiceDateDisp;
	private String dueDateDisp;
	private String status;
	private String SettleDateDisp;
	
	
	public String getSettleDateDisp() {
		return SettleDateDisp;
	}

	public void setSettleDateDisp(String settleDateDisp) {
		SettleDateDisp = settleDateDisp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getInvoiceDateDisp() {
		return invoiceDateDisp;
	}

	public void setInvoiceDateDisp(String invoiceDateDisp) {
		this.invoiceDateDisp = invoiceDateDisp;
	}

	public String getDueDateDisp() {
		return dueDateDisp;
	}

	public void setDueDateDisp(String dueDateDisp) {
		this.dueDateDisp = dueDateDisp;
	}

	public String getSettleDate() {
		return SettleDate;
	}

	public void setSettleDate(String settleDate) {
		SettleDate = settleDate;
	}

	
}
