package com.evampsaanga.azerfon.getpaygstatus;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.azerfon.getnetworksettings.GetNetworkSettingsRequestClient;
import com.evampsaanga.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.basetype.GetSubProductInfo;
import com.huawei.crm.query.GetNetworkSettingDataResponse;

@Path("/azerfon")
public class GetPaygStatusRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetPaygStatusResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		GetPaygStatusResponse resp = new GetPaygStatusResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_PAYG_STATUS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_CORE_SERVICES);
		logs.setTableType(LogsType.GetCoreServices);
		String token = "";
		String TrnsactionName = Transactions.GET_PAYG_STATUS_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "Request Landed on GET_PAYG_STATUS " + requestBody);

			String credentials = null;
			GetPaygStatusRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetPaygStatusRequest.class);
			}
			catch (Exception ex1) {
				logger.info(token + Helper.GetException(ex1));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
					logs.setTransactionName(Transactions.GET_PAYG_STATUS_TRANSACTION_NAME_B2B);
				}
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				}
				catch (Exception ex) {
					logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					logger.info(token + "credentials Null check.....");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(cclient.getmsisdn() + "-credentials validated credentials matched.....");
					boolean flag = false;
					CRMSubscriberService crmSub = new CRMSubscriberService();
					GetSubscriberResponse subsResponse = crmSub.GetSubscriberRequest(cclient.getmsisdn());
					if(subsResponse != null && subsResponse.getGetSubscriberBody() != null && subsResponse.getGetSubscriberBody() != null)
					{
						if(subsResponse.getGetSubscriberBody().getSupplementaryOfferingList() != null && subsResponse.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo() != null)
						{
							for(int i=0;i<subsResponse.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo().size();i++)
								if(subsResponse.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo().get(i).getOfferingId().getOfferingId().equalsIgnoreCase("1483592628"))
									flag = true;
						}
						if(flag)
						{
							Data data = new Data();
							data.setOfferingId(Constants.PAYG_OFFERING_ID);
							data.setStatus("inactive");
							resp.setData(data);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						else
						{
							Data data = new Data();
							data.setOfferingId(Constants.PAYG_OFFERING_ID);
							data.setStatus("active");
							resp.setData(data);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					}
					else
					{
						resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}

				}
				else {
					logger.info(token + "credentials  check ELSe not mathed credeitials.....");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
			else
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
	}
}
