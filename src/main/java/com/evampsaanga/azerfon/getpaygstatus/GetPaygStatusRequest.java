package com.evampsaanga.azerfon.getpaygstatus;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPaygStatusRequest extends BaseRequest {

}
