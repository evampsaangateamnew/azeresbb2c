package com.evampsaanga.azerfon.getpaygstatus;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetPaygStatusResponse extends BaseResponse {
	private Data data = new Data();

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}
}
