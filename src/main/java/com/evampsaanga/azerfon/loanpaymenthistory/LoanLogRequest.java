package com.evampsaanga.azerfon.loanpaymenthistory;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class LoanLogRequest extends BaseRequest {
	private String startDate = "";
	private String endDate = "";

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
