package com.evampsaanga.azerfon.loanpaymenthistory;

import java.util.ArrayList;
import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class LoanLogResponse extends BaseResponse {
	ArrayList<LoanPaymentHistory> loanPayment = new ArrayList<LoanPaymentHistory>();

	/**
	 * @return the loanPayment
	 */
	public ArrayList<LoanPaymentHistory> getLoanPayment() {
		return loanPayment;
	}

	/**
	 * @param loanPayment
	 *            the loanPayment to set
	 */
	public void setLoanPayment(ArrayList<LoanPaymentHistory> loanPayment) {
		this.loanPayment = loanPayment;
	}

	/**
	 * @return the loanRequest
	 */
	public LoanLogResponse() {
		super();
	}
}
