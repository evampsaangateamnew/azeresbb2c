package com.evampsaanga.azerfon.rateusV2;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

/**
 * Request Container
 * 
 * @author Aqeel Abbas
 *
 */
public class RateUsRequest extends BaseRequest {

	private String entityId;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

}
