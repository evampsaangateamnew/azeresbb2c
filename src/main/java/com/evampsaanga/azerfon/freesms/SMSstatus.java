package com.evampsaanga.azerfon.freesms;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SMSstatus {
	private int onNetSMS = 0;
	private int offNetSMS = 0;
	private boolean smsSent = false;
	@JsonIgnore
	private boolean limitReachedOnnet = false;
	@JsonIgnore
	private boolean limitReachedOffnet = false;

	public boolean isLimitReachedOnnet() {
		return limitReachedOnnet;
	}

	public void setLimitReachedOnnet(boolean limitReachedOnnet) {
		this.limitReachedOnnet = limitReachedOnnet;
	}

	public boolean isLimitReachedOffnet() {
		return limitReachedOffnet;
	}

	public void setLimitReachedOffnet(boolean limitReachedOffnet) {
		this.limitReachedOffnet = limitReachedOffnet;
	}

	/**
	 * @return the smsSent
	 */
	public boolean isSmsSent() {
		return smsSent;
	}

	/**
	 * @param smsSent
	 *            the smsSent to set
	 */
	public void setSmsSent(boolean smsSent) {
		this.smsSent = smsSent;
	}

	public SMSstatus(int onNetSMS, int offNetSMS, boolean smsSent) {
		super();
		this.onNetSMS = onNetSMS;
		this.offNetSMS = offNetSMS;
		this.smsSent = smsSent;
	}
	
	public SMSstatus(int onNetSMS, int offNetSMS, boolean smsSent, boolean limitReachedOnnet, boolean limitReachedOffnet) {
		super();
		this.onNetSMS = onNetSMS;
		this.offNetSMS = offNetSMS;
		this.smsSent = smsSent;
		this.limitReachedOnnet = limitReachedOnnet;
		this.limitReachedOffnet = limitReachedOffnet;
	}

	/**
	 * @return the onNetSMS
	 */
	public int getOnNetSMS() {
		return onNetSMS;
	}

	/**
	 * @param onNetSMS
	 *            the onNetSMS to set
	 */
	public void setOnNetSMS(int onNetSMS) {
		this.onNetSMS = onNetSMS;
	}

	/**
	 * @return the offNetSMS
	 */
	public int getOffNetSMS() {
		return offNetSMS;
	}

	/**
	 * @param offNetSMS
	 *            the offNetSMS to set
	 */
	public void setOffNetSMS(int offNetSMS) {
		this.offNetSMS = offNetSMS;
	}
}
