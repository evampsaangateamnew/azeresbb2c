package com.evampsaanga.azerfon.freesms;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class SendSMSRequest extends BaseRequest {
	private String recieverMsisdn = "";
	private String textmsg = "";
	private String msgLang;

	/**
	 * @return the recieverMsisdn
	 */
	public String getRecieverMsisdn() {
		return recieverMsisdn;
	}

	/**
	 * @param recieverMsisdn
	 *            the recieverMsisdn to set
	 */
	public void setRecieverMsisdn(String recieverMsisdn) {
		this.recieverMsisdn = recieverMsisdn;
	}

	/**
	 * @return the textmsg
	 */
	public String getTextmsg() {
		return textmsg;
	}

	/**
	 * @param textmsg
	 *            the textmsg to set
	 */
	public void setTextmsg(String textmsg) {
		this.textmsg = textmsg;
	}

	public String getMsgLang() {
		return msgLang;
	}

	public void setMsgLang(String msgLang) {
		this.msgLang = msgLang;
	}
}
