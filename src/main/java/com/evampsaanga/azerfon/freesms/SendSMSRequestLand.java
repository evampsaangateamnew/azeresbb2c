package com.evampsaanga.azerfon.freesms;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.mail.Flags.Flag;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.azerfon.getcustomerrequest.GetCustomerDataLand;
import com.evampsaanga.azerfon.getcustomerrequest.GetCustomerRequestClient;
import com.evampsaanga.azerfon.getcustomerrequest.GetCustomerRequestResponse;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.SendSMCService;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/azerfon")
public class SendSMSRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SendSMSResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		SendSMSRequest cclient = null;
		SendSMSResponse resp = new SendSMSResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SEND_FREE_SMS);
		logs.setThirdPartyName(ThirdPartyNames.SEND_FREE_SMS);
		logs.setTableType(LogsType.AppFaq);
		try {
			cclient = Helper.JsonToObject(requestBody, SendSMSRequest.class);
			cclient.setTextmsg(new String(Base64.decodeBase64(cclient.getTextmsg().getBytes())));
			logs.setReceiverMsisdn(cclient.getRecieverMsisdn());
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());

			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		try {
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					SMSstatus status = sendSMS(cclient, logs);
					if ( (status.getOffNetSMS() ==2 && status.getOnNetSMS() ==2))
					{
						resp.setReturnCode(ResponseCodes.OFFNET_SMS_NOTALLOWED_CODE);
						resp.setReturnMsg(ResponseCodes.OFFNET_SMS_NOTALLOWED_DESC);
					}
					else if (status.getOffNetSMS() != -1 && status.getOnNetSMS() != -1 && status.isSmsSent()) {
						resp.setStatus(status);
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					} else if (status.getOffNetSMS() != -1 && status.getOnNetSMS() != -1 && !status.isSmsSent()) {
						if (status.isLimitReachedOnnet())
							resp.setReturnCode(ResponseCodes.SMS_LIMIT_CODE_199);
						else
							resp.setReturnCode(ResponseCodes.SMS_LIMIT_CODE_198);
						resp.setReturnMsg(
								"remaining sms:onnet=" + status.getOnNetSMS() + ",offnet=" + status.getOffNetSMS());
					} 
					
					else {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
					}
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info("Response returned from FreeSMS ESB: "+ Helper.ObjectToJson(resp));
					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/broadcastsms")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SendSMSResponse broadcastsms(@Header("credentials") String credential, @Body() String requestBody) {
		SendSMSRequest cclient = null;
		SendSMSResponse resp = new SendSMSResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SEND_FREE_SMS);
		logs.setThirdPartyName(ThirdPartyNames.SEND_FREE_SMS);
		logs.setTableType(LogsType.AppFaq);
		try {
			cclient = Helper.JsonToObject(requestBody, SendSMSRequest.class);
			cclient.setTextmsg(new String(Base64.decodeBase64(cclient.getTextmsg().getBytes())));
			logs.setReceiverMsisdn(cclient.getRecieverMsisdn());
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());

			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		try {
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					SMSstatus status = sendSMS(cclient, logs);
					if (status.getOffNetSMS() != -1 && status.getOnNetSMS() != -1 && status.isSmsSent()) {
						resp.setStatus(status);
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					} else if (status.getOffNetSMS() != -1 && status.getOnNetSMS() != -1 && !status.isSmsSent()) {
						if (status.isLimitReachedOnnet())
							resp.setReturnCode(ResponseCodes.SMS_LIMIT_CODE_199);
						else
							resp.setReturnCode(ResponseCodes.SMS_LIMIT_CODE_198);
						resp.setReturnMsg(
								"remaining sms:onnet=" + status.getOnNetSMS() + ",offnet=" + status.getOffNetSMS());
					} else {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
					}
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		}
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public boolean sendFreeSMSToSMSC(SendSMSRequest cclient) throws IOException, Exception {
		SendSMCService service = new SendSMCService();
		return service.sendSMS(cclient.getRecieverMsisdn(), cclient.getTextmsg(), cclient.getChannel(),
				cclient.getLang(), cclient.getiP(), cclient.getmsisdn());
	}

	public SMSstatus sendSMS(SendSMSRequest cclient, Logs logs)
			throws JsonProcessingException, ClassNotFoundException, SQLException {
		String tableName = ConfigurationManager.getConfigurationFromCache("db.table.freesms");
		logger.debug(cclient.getmsisdn() + "- Message Language-" + cclient.getMsgLang().toLowerCase());
		logger.debug(cclient.getmsisdn() + "-" + ConfigurationManager
				.getConfigurationFromCache("db.table.freesms.length." + cclient.getMsgLang().toLowerCase()));
		int standatdchlength = Integer.parseInt(ConfigurationManager
				.getConfigurationFromCache("db.table.freesms.length." + cclient.getMsgLang().toLowerCase()));
		int textmsg = cclient.getTextmsg().length();
		GetCustomerDataLand getCustomer = new GetCustomerDataLand();
		GetCustomerRequestClient getCustomerRequestClient = new GetCustomerRequestClient();
		getCustomerRequestClient.setChannel(cclient.getChannel());
		getCustomerRequestClient.setiP(cclient.getiP());
		getCustomerRequestClient.setLang(cclient.getLang());
		getCustomerRequestClient.setMsisdn(cclient.getRecieverMsisdn());
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(getCustomerRequestClient);
		GetCustomerRequestResponse responseGetCustomer = getCustomer.Get(Constants.CREDENTIALSUNCODED, "",
				jsonInString);
		if (responseGetCustomer.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
			// reciver is on-net
			logger.info("----ONnet Number---- chececk--");
			logs.setOnNet(true);
			PreparedStatement stmt = null;
			ResultSet rs = null;
			PreparedStatement stmt3 = null;
			PreparedStatement stmt2 = null;
			try {
				int onnet = -1;
				int offnet = -1;
				int quotient;
				if (textmsg < standatdchlength) {
					quotient = 1;
				} else {
					quotient = textmsg / standatdchlength;
					int remainder = textmsg % standatdchlength;
					if (remainder > 0) {
						quotient = quotient + 1;
					}
				}
				stmt = DBFactory.getDbConnection().prepareStatement(
						"select onnet,offnet from " + tableName + " where msisdn=" + "'" + cclient.getmsisdn() + "'");
				rs = stmt.executeQuery();
				if (rs.next()) {
					offnet = Integer.parseInt(rs.getString("offnet"));
					onnet = Integer.parseInt(rs.getString("onnet"));
					logger.info("number exists already updating");
					if (onnet > 0 && onnet >= quotient) {
						stmt2 = DBFactory.getDbConnection()
								.prepareStatement("update " + tableName + " SET onnet = onnet - " + quotient
										+ " where msisdn=" + "'" + cclient.getmsisdn() + "'");
						if (sendFreeSMSToSMSC(cclient)) {
							stmt2.executeUpdate();
							// Now Send SMS
							return new SMSstatus(onnet - quotient, offnet, true);
						} else {
							return new SMSstatus(onnet, offnet, false);
						}
					} else {
						// limit reached
						return new SMSstatus(onnet, offnet, false, true, false);
					}
				} else {
					int freesmsconfigure = Integer
							.parseInt(ConfigurationManager.getConfigurationFromCache("onnet.freesms.configure"));
					int freesmsconfigureOffnet = Integer
							.parseInt(ConfigurationManager.getConfigurationFromCache("offnet.freesms.configure"));
					int finaloonetsms = freesmsconfigure - quotient;
					logger.info("number doesnt exists already adding");
					stmt3 = DBFactory.getDbConnection().prepareStatement(
							"insert into " + tableName + " (msisdn,recievermsisdn,onnet,offnet) VALUES( '"
									+ cclient.getmsisdn() + "','" + cclient.getRecieverMsisdn() + "','" + finaloonetsms
									+ "','" + freesmsconfigureOffnet + "')");
					// Now Send SMS
					if (sendFreeSMSToSMSC(cclient)) {
						stmt3.executeUpdate();
						// Now Send SMS
						return new SMSstatus(finaloonetsms, freesmsconfigureOffnet, true);
					} else {
						return new SMSstatus(finaloonetsms, freesmsconfigureOffnet, false, true, false);
					}
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			} finally {
				try {
					if (rs != null)
						rs.close();
				} catch (Exception ex2) {
					logger.error(Helper.GetException(ex2));
				}
				try {
					if (stmt3 != null) {
						stmt3.close();
					}
				} catch (Exception ex2) {
					logger.error(Helper.GetException(ex2));
				}
				try {
					if (stmt2 != null)
						stmt2.close();
				} catch (Exception ex2) {
					logger.error(Helper.GetException(ex2));
				}
				try {
					if (stmt != null) {
						stmt.close();
					}
				} catch (Exception ex2) {
					logger.error(Helper.GetException(ex2));
				}
			}
		} else {
			logger.info("-----OFFNET Number -- ELSE check--");
			/*
			// recver is off-net
			logs.setOnNet(false);
			PreparedStatement stmt = null;
			ResultSet rs = null;
			PreparedStatement stmt2 = null;
			PreparedStatement stmt3 = null;
			try {
				int onnet = -1;
				int offnet = -1;
				int quotient;
				if (textmsg < standatdchlength) {
					quotient = 1;
				} else {
					quotient = textmsg / standatdchlength;
					int remainder = textmsg % standatdchlength;
					if (remainder > 0) {
						quotient = quotient + 1;
					}
				}
				stmt = DBFactory.getDbConnection().prepareStatement(
						"select onnet,offnet from " + tableName + " where msisdn=" + "'" + cclient.getmsisdn() + "'");
				rs = stmt.executeQuery();
				if (rs.next()) {
					offnet = Integer.parseInt(rs.getString("offnet"));
					onnet = Integer.parseInt(rs.getString("onnet"));
					logger.info("number exists already updating");
					if (offnet > 0 && offnet >= quotient) {
						stmt2 = DBFactory.getDbConnection()
								.prepareStatement("update " + tableName + " SET offnet = offnet - " + quotient
										+ " where msisdn=" + "'" + cclient.getmsisdn() + "'");
						if (sendFreeSMSToSMSC(cclient)) {
							stmt2.executeUpdate();
							return new SMSstatus(onnet, offnet - quotient, true);
						} else {
							return new SMSstatus(onnet, offnet, false, false, true);
						}
						// Now Send SMS
					} else {
						// limit reached
						return new SMSstatus(onnet, offnet, false);
					}
				} else {
					logger.info("number doesnt exists already adding");
					int freesmsconfigure = Integer
							.parseInt(ConfigurationManager.getConfigurationFromCache("offnet.freesms.configure"));
					int freesmsconfigureOnnet = Integer
							.parseInt(ConfigurationManager.getConfigurationFromCache("onnet.freesms.configure"));
					int finaloffnetsms = freesmsconfigure - quotient;
					// Now Send SMS
					stmt3 = DBFactory.getDbConnection().prepareStatement(
							"insert into " + tableName + " (msisdn,recievermsisdn,offnet,onnet) VALUES( '"
									+ cclient.getmsisdn() + "','" + cclient.getRecieverMsisdn() + "','" + finaloffnetsms
									+ "','" + freesmsconfigureOnnet + "')");
					if (sendFreeSMSToSMSC(cclient)) {
						stmt3.executeUpdate();
						return new SMSstatus(freesmsconfigureOnnet, finaloffnetsms, true);
					} else {
						return new SMSstatus(freesmsconfigureOnnet, finaloffnetsms, false, false, true);
					}
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			} finally {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (stmt2 != null)
					stmt2.close();
				if (stmt3 != null)
					stmt3.close();
			}
			return new SMSstatus(-1, -1, false);
		*/
			logger.info("Returning from Free SMS Method");
			return new SMSstatus(2, 2, false);	
		}
		logger.info("Returning from Free SMS Method");
		return new SMSstatus(-1, -1, false);
	}

	public boolean sendSMSBulk(SendSMSRequest cclient, Logs logs)
			throws JsonProcessingException, ClassNotFoundException, SQLException {
		boolean retFlag = false;
		logger.debug(cclient.getmsisdn() + "- Message Language-" + cclient.getMsgLang().toLowerCase());
		logger.debug(cclient.getmsisdn() + "-" + ConfigurationManager
				.getConfigurationFromCache("db.table.freesms.length." + cclient.getMsgLang().toLowerCase()));
		GetCustomerDataLand getCustomer = new GetCustomerDataLand();
		GetCustomerRequestClient getCustomerRequestClient = new GetCustomerRequestClient();
		getCustomerRequestClient.setMsisdn(cclient.getRecieverMsisdn());
		getCustomerRequestClient.setChannel(cclient.getChannel());
		getCustomerRequestClient.setiP(cclient.getiP());
		getCustomerRequestClient.setLang(cclient.getLang());
		
		ObjectMapper mapper = new ObjectMapper();

		String jsonInString = mapper.writeValueAsString(getCustomerRequestClient);
		GetCustomerRequestResponse responseGetCustomer = getCustomer.Get(Constants.CREDENTIALSUNCODED, "",
				jsonInString);
		if (responseGetCustomer.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
			//on-net number
			try {
				retFlag = sendFreeSMSToSMSC(cclient);
				return retFlag;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return retFlag;
			}
		} else {
			//off-net number
			try {
				retFlag = sendFreeSMSToSMSC(cclient);
				return retFlag;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return retFlag;
			}
		}
	}

}
