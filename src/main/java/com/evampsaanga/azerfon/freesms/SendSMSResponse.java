package com.evampsaanga.azerfon.freesms;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class SendSMSResponse extends BaseResponse {
	private SMSstatus status;

	/**
	 * @return the status
	 */
	public SMSstatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(SMSstatus status) {
		this.status = status;
	}
}
