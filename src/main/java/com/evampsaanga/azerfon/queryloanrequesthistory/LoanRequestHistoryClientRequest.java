package com.evampsaanga.azerfon.queryloanrequesthistory;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class LoanRequestHistoryClientRequest extends BaseRequest {
	private String startDate = "";
	private String endDate = "";

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
