package com.evampsaanga.azerfon.notifications;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;

@Path("/azerfon")
public class NotificationRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	Logs logs = new Logs();

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotificationResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		logs.setTransactionName(Transactions.NOTIFICATIONS_HISTORY_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_NOTIFICATION);
		logs.setTableType(LogsType.GetNotification);
		try {
			logger.info("Request Landed on NotificiationsLand:" + requestBody);
			NotificationRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, NotificationRequestClient.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.NOTIFICATIONS_HISTORY_TRANSACTION_NAME_B2B);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				NotificationResponseClient resp = new NotificationResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					NotificationResponseClient resp = new NotificationResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						NotificationResponseClient res = new NotificationResponseClient();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					NotificationResponseClient resp = new NotificationResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					return getResponse(cclient.getmsisdn(), cclient.getLang(), logs);
				} else {
					NotificationResponseClient resp = new NotificationResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		NotificationResponseClient resp = new NotificationResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public NotificationResponseClient getResponse(String msisdn, String lang, Logs logs)
			throws ClassNotFoundException, SQLException {
		String sql = "select datetime, messageAzeri, messageEnglish, messageRussian, icon, actionType, actionID, btnTxt  from  History where msisdn='"
				+ msisdn + "' GROUP BY notificationid ORDER BY id DESC";
		try {
			Connection myConnection = com.evampsaanga.azerfon.db.DBFactory.getAppConnection();
			PreparedStatement statement = myConnection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			List<Notification> notificationsList = new ArrayList<>();
			while (resultSet.next()) {

				// String dateString = resultSet.getString("datetime");
				// String updatetime = getTimeSecMinutesHoursDays(dateString);

				Notification notification = new Notification();
				notification.setDatetime(
						Helper.parseDateDynamically("", resultSet.getString("datetime"), "dd/MM/YY HH:mm:ss"));

				if (lang.equalsIgnoreCase("4")) {
					notification.setMessage(resultSet.getString("messageAzeri"));
				} else if (lang.equalsIgnoreCase("3")) {
					notification.setMessage(resultSet.getString("messageEnglish"));
				} else if (lang.equalsIgnoreCase("2")) {
					notification.setMessage(resultSet.getString("messageRussian"));
				}
				notification.setIcon(resultSet.getString("icon"));
				notification.setActionType(resultSet.getString("actionType"));
				notification.setActionID(resultSet.getString("actionID"));
				notification.setBtnTxt(resultSet.getString("btnTxt"));
				notificationsList.add(notification);
			}
			if (notificationsList.size() > 0) {
				String sql1 = "UPDATE History SET read_status=? where msisdn=? ";
				try (PreparedStatement statement1 = myConnection.prepareStatement(sql1);) {
					statement1.setInt(1, 0);
					statement1.setString(2, msisdn);
					statement1.executeUpdate();
					statement1.close();
				} catch (Exception e) {
					logger.error("Update History Exception " + Helper.GetException(e));
				} finally {
					resultSet.close();
					statement.close();
				}
			}
			NotificationResponseClient rep11 = new NotificationResponseClient();
			rep11.setNotificationsList(notificationsList);
			rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			logs.setResponseCode(rep11.getReturnCode());
			logs.setResponseDescription(rep11.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return rep11;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		NotificationResponseClient resp = new NotificationResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public static String getTimeSecMinutesHoursDays(String dateString) throws ParseException {
		SimpleDateFormat endDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentDateAndTime = endDateFormat.format(new Date());
		Date endDate = endDateFormat.parse(currentDateAndTime);
		SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate = startDateFormat.parse(dateString);
		long difference = endDate.getTime() - startDate.getTime();
		String minutesHoursDays = calculateTime(difference);
		return minutesHoursDays;
	}

	public static String calculateTime(long milis) {
		String time = "";
		long day = (milis / (1000 * 60 * 60 * 24));
		long hours = (((milis) - (1000 * 60 * 60 * 24 * day)) / (1000 * 60 * 60));
		long minute = ((milis) - (1000 * 60 * 60 * 24 * day) - (1000 * 60 * 60 * hours)) / (1000 * 60);
		long second = ((milis / 1000) % 60);
		if (day > 0) {
			if (day == 1) {
				time = day + " day";
			} else {
				time = day + " days";
			}
		} else if (day < 1 && hours > 0) {
			if (hours == 1) {
				time = hours + " hr";
			} else {
				time = hours + " hrs";
			}
		} else if (hours < 1 && minute > 0) {
			if (minute == 1) {
				time = minute + " min";
			} else {
				time = minute + " mins";
			}
		} else if (minute < 1 && second > 0) {
			if (second == 1) {
				time = second + " sec";
			} else {
				time = second + " secs";
			}
		} else {
			if (second <= 0) {
				time = "0 sec";
			}
		}
		return time;
	}
}
