package com.evampsaanga.azerfon.changelanguage;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.services.OrderHandleService;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;

@Path("/azerfon/")
public class ChangeLanguageRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ChangeLanguageResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		ChangeLanguageResponse resp = new ChangeLanguageResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_BILLING_LANGUAGE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_LANGUAGE);
		logs.setTableType(LogsType.ChangeLanguage);
		try {
			logger.info("Request Landed on ChangeLanguageRequestLand:" + requestBody);
			ChangeLanguageRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, ChangeLanguageRequest.class);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {

				if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
					logs.setTransactionName(Transactions.CHANGE_BILLING_LANGUAGE_TRANSACTION_NAME_B2B);
				}
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					/// Call ChangeLanguage here
					SubmitOrderResponse response = getResponse(cclient);
					if (response.getResponseHeader().getRetCode().equals("0")) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} else {
						resp.setReturnCode(response.getResponseHeader().getRetCode());
						resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	private SubmitOrderResponse getResponse(ChangeLanguageRequest cclient) {
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForChangeLanguage());
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setOrderType(Constants.CL_ORDER_ITEM);
		submitRequestBody.setOrder(orderInfo);
		OrderItems orderItems = new OrderItems();
		OrderItemValue orderItemValue = new OrderItemValue();
		OrderItemInfo orderItemInfo = new OrderItemInfo();
		orderItemInfo.setOrderItemType(Constants.CL_ORDER_ITEM_TYPE);
		orderItemValue.setOrderItemInfo(orderItemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(cclient.getmsisdn());
		subscriber.setLanguage(cclient.getLanguage());
		subscriber.setWrittenLanguage(cclient.getLanguage());
		orderItemValue.setSubscriber(subscriber);
		orderItems.getOrderItem().add(orderItemValue);
		submitRequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		return OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
	}
}
