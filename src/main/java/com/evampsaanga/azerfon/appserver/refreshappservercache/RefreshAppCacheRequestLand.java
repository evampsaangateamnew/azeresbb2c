package com.evampsaanga.azerfon.appserver.refreshappservercache;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SOAPLoggingHandler;
import com.saanga.magento.apiclient.RestClient;

@Path("/appserver/")
public class RefreshAppCacheRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public String getAppserverURLByKey(String key) {
		return ConfigurationManager.getConfigurationFromCache(key);
	}

	@POST
	@Path("/refreshcache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RefreshAppCacheResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.REFRESHAPP_CACHE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.REFRESH_APPSERVER_CACHE);
		logs.setTableType(LogsType.RefreshappserverCache);
		logger.info("request body refresh cache  =" + requestBody);
		RefreshAppCacheRequestClient cclient = null;
		RefreshAppCacheResponseClient resp = new RefreshAppCacheResponseClient();
		try {
			cclient = Helper.JsonToObject(requestBody, RefreshAppCacheRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				if (!cclient.getChannel().equals("web")) {
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("cacheType", cclient.getCacheType());
						try {
							logger.info("RequestPacket" + jsonObject.toString());
							String response = RestClient.SendCallToAppserver(
									getAppserverURLByKey("appserver.app.refresh1"), jsonObject.toString());
							AppServerResponse data = Helper.JsonToObject(response, AppServerResponse.class);
							logger.info("Result from App server 1 cache" + response);
							if (data.getResultCode().equals("00")) {
								resp.setAppServer1("Successful For: "+getAppserverURLByKey("appserver.app.refresh1"));
								logs.setResponseCode(data.getResultCode());
								logs.setResponseDescription(data.getResultDesc());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.setApiurl(getAppserverURLByKey("appserver.app.refresh1"));
								logs.updateLog(logs);
								logger.info("Result code Esb " + data.getResultCode());
								logger.info("Result Description Esb " + data.getResultDesc());
							} else {
								resp.setReturnCode(data.getResultCode());
								resp.setReturnMsg(data.getResultDesc());
								resp.setAppServer1("Failed FOR: "+getAppserverURLByKey("appserver.app.refresh1"));
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
						
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.setApiurl(getAppserverURLByKey("appserver.app.refresh1"));
								logs.updateLog(logs);
							}
						}
							catch (Exception ex) {
								logger.error(Helper.GetException(ex));
								resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
								resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
								logs.setResponseCode(resp.getReturnCode());
								resp.setAppServer1("Failed FOR: "+getAppserverURLByKey("appserver.app.refresh1"));
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							
							}
							
							try{
							String response2 = RestClient.SendCallToAppserver(
									getAppserverURLByKey("appserver.app.refresh2"), jsonObject.toString());
							AppServerResponse dataserver2 = Helper.JsonToObject(response2, AppServerResponse.class);
							logger.info("Result from App server 2 cache " + response2);
							if (dataserver2.getResultCode().equals("00")) {
								logs.setResponseCode(dataserver2.getResultCode());
								resp.setAppServer2("Successful FOR: "+getAppserverURLByKey("appserver.app.refresh2"));
								logs.setResponseDescription(dataserver2.getResultDesc());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.setApiurl(getAppserverURLByKey("appserver.app.refresh2"));
								logs.updateLog(logs);
								logger.info("Result code Esb " + dataserver2.getResultCode());
								logger.info("Result Description Esb " + dataserver2.getResultDesc());
							} else {
								resp.setAppServer2("Failed FOR: "+getAppserverURLByKey("appserver.app.refresh2"));
								logs.setResponseCode(dataserver2.getResultCode());
								logs.setResponseDescription(dataserver2.getResultDesc());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.setApiurl(getAppserverURLByKey("appserver.app.refresh2"));
								logs.updateLog(logs);
							}
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);		return resp;
						} catch (Exception ex) {
							logger.error(Helper.GetException(ex));
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							resp.setAppServer2("Failed FOR: "+getAppserverURLByKey("appserver.app.refresh2"));
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
					
						} // end of (Exception ex)
					} catch (JSONException ex) {
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}
}
