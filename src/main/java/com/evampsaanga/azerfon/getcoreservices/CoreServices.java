package com.evampsaanga.azerfon.getcoreservices;

import java.util.ArrayList;

import com.evampsaanga.azerfon.getsubscriber.CoreServicesCategoryItem;

public class CoreServices {
	private String coreServiceCategory = "";

	public CoreServices(String coreServiceCategory) {
		super();
		this.coreServiceCategory = coreServiceCategory;
	}

	public CoreServices(String coreServiceCategory, ArrayList<CoreServicesCategoryItem> coreServicesList) {
		super();
		this.coreServiceCategory = coreServiceCategory;
		this.coreServicesList = coreServicesList;
	}

	private ArrayList<CoreServicesCategoryItem> coreServicesList = new ArrayList<CoreServicesCategoryItem>();

	/**
	 * @return the coreServiceCategory
	 */
	public String getCoreServiceCategory() {
		return coreServiceCategory;
	}

	/**
	 * @param coreServiceCategory
	 *            the coreServiceCategory to set
	 */
	public void setCoreServiceCategory(String coreServiceCategory) {
		this.coreServiceCategory = coreServiceCategory;
	}

	public ArrayList<CoreServicesCategoryItem> getCoreServicesList() {
		return coreServicesList;
	}

	public void setCoreServicesList(ArrayList<CoreServicesCategoryItem> coreServicesList) {
		this.coreServicesList = coreServicesList;
	}


	
}
