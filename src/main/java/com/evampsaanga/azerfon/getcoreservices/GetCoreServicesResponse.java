package com.evampsaanga.azerfon.getcoreservices;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetCoreServicesResponse extends BaseResponse {
	private Data data = new Data();

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}
}
