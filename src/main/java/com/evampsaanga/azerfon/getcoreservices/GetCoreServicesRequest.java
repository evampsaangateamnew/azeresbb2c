package com.evampsaanga.azerfon.getcoreservices;

import javax.xml.bind.annotation.XmlElement;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetCoreServicesRequest extends BaseRequest {
	
	@XmlElement(name = "userType", required = true)
	private String userType;
	@XmlElement(name = "brand", required = true)
	private String brand;
	@XmlElement(name = "accountType")
	private String accountType;
	@XmlElement(name = "groupType")
	private String groupType;
	private String isFrom="";
	
	
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getIsFrom() {
		return isFrom;
	}
	public void setIsFrom(String isFrom) {
		this.isFrom = isFrom;
	}
	
	
	
}
