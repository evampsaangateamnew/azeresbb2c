package com.evampsaanga.azerfon.nartv.subscribe;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.azerfon.validator.RequestValidator;
import com.evampsaanga.azerfon.validator.ResponseValidator;
import com.evampsaanga.azerfon.validator.ValidatorService;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Helper;

@Path("/azerfon/")
public class SubscribeController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/subscribe")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SubscribeResponse Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			// ------------------------- Setting Transaction Name ---------------------- //

			SubscribeResponse getManagerTokenResponse = new SubscribeResponse();
			SubscribeRequest cclient = null;

			String transactionName = Transactions.NAR_TV_GET_SUBSCRIBE;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text --------------------------- //

			cclient = Helper.JsonToObject(requestBody, SubscribeRequest.class);

			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				SubscribeRequest getManagerTokenRequest = new SubscribeRequest();

				requestBody = Helper.addParamsToJSONObject(requestBody, "grantType", "password");
				requestBody = Helper.addParamsToJSONObject(requestBody, "clientId", "nar@master");
				requestBody = Helper.addParamsToJSONObject(requestBody, "username", "nar_it@sotal.tv");
				requestBody = Helper.addParamsToJSONObject(requestBody, "password", "vxGNUuqH");

				getManagerTokenRequest = Helper.JsonToObject(requestBody, SubscribeRequest.class);

				getManagerTokenResponse = AzerfonThirdPartyCalls.narTvSubscribe(token, transactionName,
						getManagerTokenRequest, getManagerTokenResponse);

			} else {

				getManagerTokenResponse.setReturnCode(responseValidator.getResponseCode());
				getManagerTokenResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(getManagerTokenResponse.getReturnCode());
				logs.setResponseDescription(getManagerTokenResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(token + "-Response Returned From-" + transactionName + "-"
					+ Helper.ObjectToJson(getManagerTokenResponse));

			return getManagerTokenResponse;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			SubscribeResponse resp = new SubscribeResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			try {
				Constants.logger.info(
						"" + "-Response Returned From-" + "NAR TV GET SUBSCRIPTIONS" + "-" + Helper.ObjectToJson(resp));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
			}
			return resp;
		}
	}
}
