package com.evampsaanga.azerfon.crmsubscriber;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class CrmSubscriberResponse extends BaseResponse {
	protected String AccountId = "";
	protected String pin1 = "";
	protected String pin2 = "";
	protected String puk1 = "";
	protected String puk2 = "";
	protected String subscriberType = "";
	protected String brandId = "";
	protected String status = "";
	protected String writtenLanguage = "";

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return AccountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(String accountId) {
		AccountId = accountId;
	}

	/**
	 * @return the pin1
	 */
	public String getPin1() {
		return pin1;
	}

	/**
	 * @param pin1
	 *            the pin1 to set
	 */
	public void setPin1(String pin1) {
		this.pin1 = pin1;
	}

	/**
	 * @return the pin2
	 */
	public String getPin2() {
		return pin2;
	}

	/**
	 * @param pin2
	 *            the pin2 to set
	 */
	public void setPin2(String pin2) {
		this.pin2 = pin2;
	}

	/**
	 * @return the puk1
	 */
	public String getPuk1() {
		return puk1;
	}

	/**
	 * @param puk1
	 *            the puk1 to set
	 */
	public void setPuk1(String puk1) {
		this.puk1 = puk1;
	}

	/**
	 * @return the puk2
	 */
	public String getPuk2() {
		return puk2;
	}

	/**
	 * @param puk2
	 *            the puk2 to set
	 */
	public void setPuk2(String puk2) {
		this.puk2 = puk2;
	}

	/**
	 * @return the subscriberType
	 */
	public String getSubscriberType() {
		return subscriberType;
	}

	/**
	 * @param subscriberType
	 *            the subscriberType to set
	 */
	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	/**
	 * @return the brandId
	 */
	public String getBrandId() {
		return brandId;
	}

	/**
	 * @param brandId
	 *            the brandId to set
	 */
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the writtenLanguage
	 */
	public String getWrittenLanguage() {
		return writtenLanguage;
	}

	/**
	 * @param writtenLanguage
	 *            the writtenLanguage to set
	 */
	public void setWrittenLanguage(String writtenLanguage) {
		this.writtenLanguage = writtenLanguage;
	}

	public CrmSubscriberResponse(String pin1, String pin2, String puk1, String puk2, String writtenLanguage) {
		super();
		this.pin1 = pin1;
		this.pin2 = pin2;
		this.puk1 = puk1;
		this.puk2 = puk2;
		this.writtenLanguage = writtenLanguage;
	}

	public CrmSubscriberResponse() {
		super();
	}
}
