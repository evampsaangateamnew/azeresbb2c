package com.evampsaanga.azerfon.ussdgwqueryfreeresource;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.gethomepage.FreeResource;
import com.evampsaanga.gethomepage.FreeResources;
import com.evampsaanga.gethomepage.FreeResourcesRoaming;
import com.evampsaanga.gethomepage.FreeResourcesTypes;
import com.evampsaanga.services.USSDService;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResultMsg;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWQueryFreeResourceReqMsg;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWQueryFreeResourceReqMsg.QueryObj;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWQueryFreeResourceRspMsg;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWQueryFreeResourceRspMsg.FreeUnitItem;

public class FreeResourcesUSSDservices {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	public static final List<Long> times = Arrays.asList(TimeUnit.DAYS.toMillis(365), TimeUnit.DAYS.toMillis(30),
			TimeUnit.DAYS.toMillis(1), TimeUnit.HOURS.toMillis(1), TimeUnit.MINUTES.toMillis(1),
			TimeUnit.SECONDS.toMillis(1));
	public static final List<String> timesString = Arrays.asList("year", "month", "day", "hour", "min", "sec");

	public static String toDuration(long duration) {
		duration = duration * 1000;
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < times.size(); i++) {
			Long current = times.get(i);
			long temp = duration / current;
			if (temp > 0) {
				res.append(temp).append(' ').append(timesString.get(i)).append(temp != 1 ? "s" : "");
				break;
			}
		}
		if ("".equals(res.toString()))
			return "0 seconds";
		else
			return res.toString();
	}

	private FreeResources processUnityTypes(USSDGWQueryFreeResourceRspMsg res, String tariffType)
			throws IOException, Exception {
		List<USSDGWQueryFreeResourceRspMsg.FreeUnitItem> items = res.getFreeUnitItem();
		HashMap<String, FreeResourcesTypes> resourceMapped = new HashMap<>();
		HashMap<String, FreeResourcesRoaming> resourceMappedR = new HashMap<>();
		for (Iterator<FreeUnitItem> iterator = items.iterator(); iterator.hasNext();) {
			FreeUnitItem freeUnitItem = iterator.next();
			try {
				if (freeUnitItem.getFreeUnitType() != null)
					if (!ConfigurationManager.getContainsValueByKey("free.resource.type."+freeUnitItem.getFreeUnitType()))
						logger.info("A KEY of Query Resources doesnt exists:" + freeUnitItem.getFreeUnitType());
					else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType()).equals(Constants.VOICE)) {
						logger.info("Home Page Circle: "+freeUnitItem.getFreeUnitType());
						if (resourceMapped.containsKey(Constants.VOICE)) {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = Long.parseLong(freeUnitItem.getTotalInitialAmount());
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
							try {
								remainingAmount = Long.parseLong(freeUnitItem.getTotalUnusedAmount());
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
							try {
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
							FreeResourcesTypes unit = resourceMapped.get(Constants.VOICE);
							if(freeUnitItem.getMeasureUnit().equalsIgnoreCase("1004"))
							{
								logger.info("Its minute unit");
								initialAmount = initialAmount * 60;
								remainingAmount = remainingAmount * 60;
								unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
								unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							}
							else
							{
								logger.info("Seconds unit");
								unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
								unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							}
							resourceMapped.put(Constants.VOICE, unit);
						} else {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = Long.parseLong(freeUnitItem.getTotalInitialAmount());
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
							try {
								remainingAmount = Long.parseLong(freeUnitItem.getTotalUnusedAmount());
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
							if(freeUnitItem.getMeasureUnit().equalsIgnoreCase("1004"))
							{
								logger.info("Its minute unit");
								initialAmount = initialAmount * 60;
								remainingAmount = remainingAmount * 60;
								resourceMapped.put(Constants.VOICE, new FreeResourcesTypes(Constants.VOICE, initialAmount,
										remainingAmount, "Seconds"));
							}
							else
							{
								logger.info("Its seconds unit");
								resourceMapped.put(Constants.VOICE, new FreeResourcesTypes(Constants.VOICE, initialAmount,
									remainingAmount, freeUnitItem.getMeasureUnitName()));
							}
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType()).equals(Constants.DATA)) {
						long initialAmount = 0L;
						long remainingAmount = 0L;
						try {
							initialAmount = Long.parseLong(freeUnitItem.getTotalInitialAmount());
						} catch (Exception ex) {
						}
						try {
							remainingAmount = Long.parseLong(freeUnitItem.getTotalUnusedAmount());
						} catch (Exception ex) {
						}
						if (resourceMapped.containsKey(Constants.DATA)) {
							FreeResourcesTypes unit = resourceMapped.get(Constants.DATA);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMapped.put(Constants.DATA, unit);
						} else {
							resourceMapped.put(Constants.DATA, new FreeResourcesTypes(Constants.DATA, initialAmount,
									remainingAmount, freeUnitItem.getMeasureUnitName()));
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType()).equals(Constants.SMS)) {
						long initialAmount = 0L;
						long remainingAmount = 0L;
						try {
							initialAmount = Long.parseLong(freeUnitItem.getTotalInitialAmount());
						} catch (Exception ex) {
						}
						try {
							remainingAmount = Long.parseLong(freeUnitItem.getTotalUnusedAmount());
						} catch (Exception ex) {
						}
						if (resourceMapped.containsKey(Constants.SMS)) {
							FreeResourcesTypes unit = resourceMapped.get(Constants.SMS);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMapped.put(Constants.SMS, unit);
						} else {
							resourceMapped.put(Constants.SMS, new FreeResourcesTypes(Constants.SMS, initialAmount,
									remainingAmount, freeUnitItem.getMeasureUnitName()));
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType()).equals(Constants.ROAMING_SMS)) {
						long initialAmount = 0L;
						long remainingAmount = 0L;
						try {
							initialAmount = Long.parseLong(freeUnitItem.getTotalInitialAmount());
						} catch (Exception ex) {
						}
						try {
							remainingAmount = Long.parseLong(freeUnitItem.getTotalUnusedAmount());
						} catch (Exception ex) {
						}
						if (resourceMappedR.containsKey(Constants.ROAMING_SMS)) {
							FreeResourcesRoaming unit = resourceMappedR.get(Constants.ROAMING_SMS);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMappedR.put(Constants.ROAMING_SMS, unit);
						} else {
							resourceMappedR.put(Constants.ROAMING_SMS, new FreeResourcesRoaming(Constants.SMS,
									initialAmount, remainingAmount, freeUnitItem.getMeasureUnitName(), ""));
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType())
							.equals(Constants.ROAMING_DATA)) {
						long initialAmount = 0L;
						long remainingAmount = 0L;
						try {
							initialAmount = Long.parseLong(freeUnitItem.getTotalInitialAmount());
						} catch (Exception ex) {
						}
						try {
							remainingAmount = Long.parseLong(freeUnitItem.getTotalUnusedAmount());
						} catch (Exception ex) {
						}
						if (resourceMappedR.containsKey(Constants.ROAMING_DATA)) {
							FreeResourcesRoaming unit = resourceMappedR.get(Constants.ROAMING_DATA);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMappedR.put(Constants.ROAMING_DATA, unit);
						} else {
							resourceMappedR.put(Constants.ROAMING_DATA, new FreeResourcesRoaming(Constants.DATA,
									initialAmount, remainingAmount, freeUnitItem.getMeasureUnitName(), ""));
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType())
							.equals(Constants.ROAMING_VOICE)) {
						if (resourceMappedR.containsKey(Constants.ROAMING_VOICE)) {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = Long.parseLong(freeUnitItem.getTotalInitialAmount());
							} catch (Exception ex) {
							}
							try {
								remainingAmount = Long.parseLong(freeUnitItem.getTotalUnusedAmount());
							} catch (Exception ex) {
							}
							FreeResourcesRoaming unit = resourceMappedR.get(Constants.ROAMING_VOICE);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMappedR.put(Constants.ROAMING_VOICE, unit);
						} else {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = Long.parseLong(freeUnitItem.getTotalInitialAmount());
							} catch (Exception ex) {
							}
							try {
								remainingAmount = Long.parseLong(freeUnitItem.getTotalUnusedAmount());
							} catch (Exception ex) {
							}
							resourceMappedR.put(Constants.ROAMING_VOICE, new FreeResourcesRoaming(Constants.VOICE,
									initialAmount, remainingAmount, freeUnitItem.getMeasureUnitName(), ""));
						}
					}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		ArrayList<FreeResourcesTypes> list = new ArrayList<>();
		if (resourceMapped.get(Constants.DATA) != null) {
			FreeResourcesTypes datafreeresources = resourceMapped.get(Constants.DATA);
			String formatter = readableFileSize(datafreeresources.getResourceRemainingUnits());
			datafreeresources.setResourceUnitName(formatter.split(" ")[1]);
			datafreeresources.setRemainingFormatted(formatter.split(" ")[0]);
			list.add(datafreeresources);
		}
		if (resourceMapped.get(Constants.SMS) != null) {
			FreeResourcesTypes sms = resourceMapped.get(Constants.SMS);
			sms.setRemainingFormatted(sms.getResourceRemainingUnits() + "");
			sms.setResourceUnitName("SMS");
			list.add(sms);
		}
		if (resourceMapped.get(Constants.VOICE) != null) {
			FreeResourcesTypes voicefreeResources = resourceMapped.get(Constants.VOICE);
			logger.info(voicefreeResources.getRemainingFormatted());
			voicefreeResources.setRemainingFormatted((voicefreeResources.getResourceRemainingUnits() / 60) + "");
			voicefreeResources.setResourceUnitName("Min");
			//below code is commented to fix problem raised by Arif Babyev that circle is not showing call data if less than or equal to 60sec
			/*
			if (voicefreeResources.getResourceRemainingUnits() > 60) {
				voicefreeResources.setRemainingFormatted((voicefreeResources.getResourceRemainingUnits() / 60) + "");
				voicefreeResources.setResourceUnitName("Mins");
			} else {
				voicefreeResources.setResourceUnitName("Secs");
			}*/
			list.add(voicefreeResources);
		}
		FreeResources freeResources = new FreeResources();
		for (FreeResourcesTypes freeResourcesTypes : list) {
			/*freeResources.getFreeResources().add(new FreeResource(freeResourcesTypes.getRemainingFormatted(), "",
					freeResourcesTypes.getResourceType(), freeResourcesTypes.getResourceInitialUnits() + "",
					freeResourcesTypes.getResourceRemainingUnits() + "", freeResourcesTypes.getResourceUnitName(), ""));*/
		}
		ArrayList<FreeResourcesRoaming> listR = new ArrayList<>();
		if (resourceMappedR.get(Constants.ROAMING_DATA) != null) {
			FreeResourcesRoaming data = resourceMappedR.get(Constants.ROAMING_DATA);
			String formatter = readableFileSize(data.getResourceRemainingUnits());
			data.setResourceUnitName(formatter.split(" ")[1]);
			data.setRemainingFormatted(formatter.split(" ")[0]);
			listR.add(new FreeResourcesRoaming(data.getResourceType(), data.getResourceInitialUnits(),
					data.getResourceRemainingUnits(), data.getResourceUnitName(), data.getRemainingFormatted()));
		}
		if (resourceMappedR.get(Constants.ROAMING_SMS) != null) {
			FreeResourcesRoaming rSms = resourceMappedR.get(Constants.ROAMING_SMS);
			rSms.setRemainingFormatted(rSms.getResourceRemainingUnits() + "");
			listR.add(rSms);
		}
		if (resourceMappedR.get(Constants.ROAMING_VOICE) != null) {
			FreeResourcesRoaming data = resourceMappedR.get(Constants.ROAMING_VOICE);
			if (data.getResourceRemainingUnits() > 60) {
				data.setRemainingFormatted((data.getResourceRemainingUnits() / 60) + "");
				data.setResourceUnitName("Min");
			} else {
				data.setRemainingFormatted(data.getResourceRemainingUnits() + "");
				data.setResourceUnitName("Secs");
			}
			listR.add(data);
		}
		for (FreeResourcesRoaming freeResourcesTypes : listR) {
			freeResources.getFreeResourcesRoaming().add(new FreeResourcesRoaming(freeResourcesTypes.getResourceType(),
					freeResourcesTypes.getResourceInitialUnits(), freeResourcesTypes.getResourceRemainingUnits(),
					freeResourcesTypes.getResourceUnitName(), freeResourcesTypes.getRemainingFormatted()));
		}
		return freeResources;
	}

	public static FreeResources processUnityTypesHome(QueryFreeUnitResultMsg res, String tariffType,String token)
			throws IOException, Exception {
		List<QueryFreeUnitResult.FreeUnitItem> items = res.getQueryFreeUnitResult().getFreeUnitItem();
		HashMap<String, FreeResourcesTypes> resourceMapped = new HashMap<>();
		HashMap<String, FreeResourcesRoaming> resourceMappedR = new HashMap<>();
		logger.info(token+"FreeResourcesFrom Azerfon"+Helper.ObjectToJson(items));
		for (Iterator<com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult.FreeUnitItem> iterator = items.iterator(); iterator.hasNext();) {
			com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult.FreeUnitItem freeUnitItem = iterator.next();
			try {
				if (freeUnitItem.getFreeUnitType() != null)
					
					logger.info(token+"UNIT TYPE CHECK :"+freeUnitItem.getFreeUnitType());
					if (!ConfigurationManager.getContainsValueByKey("free.resource.type."+freeUnitItem.getFreeUnitType()))
						logger.info(token+"A KEY of Query Resources doesnt exists:" + freeUnitItem.getFreeUnitType());
					else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType()).equals(Constants.VOICE)) {
						logger.info(token+"Home Page Circle Found in Cache: "+freeUnitItem.getFreeUnitType());
						if (resourceMapped.containsKey(Constants.VOICE)) {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = freeUnitItem.getTotalInitialAmount();
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							try {
								remainingAmount = freeUnitItem.getTotalUnusedAmount();
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							try {
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							FreeResourcesTypes unit = resourceMapped.get(Constants.VOICE);
							if(freeUnitItem.getMeasureUnit().equalsIgnoreCase("1004"))
							{
								logger.info(token+"Its minute unit");
								initialAmount = initialAmount * 60;
								remainingAmount = remainingAmount * 60;
								unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
								unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							}
							else
							{
								logger.info(token+"Seconds unit");
								unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
								unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							}
							resourceMapped.put(Constants.VOICE, unit);
						} else {
							logger.info(token+"FIRST TIME UNIT TYPE CHECK :"+freeUnitItem.getFreeUnitType());
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = freeUnitItem.getTotalInitialAmount();
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							try {
								remainingAmount =freeUnitItem.getTotalUnusedAmount();
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							if(freeUnitItem.getMeasureUnit().equalsIgnoreCase("1004"))
							{
								logger.info(token+"Its minute unit");
								initialAmount = initialAmount * 60;
								remainingAmount = remainingAmount * 60;
								resourceMapped.put(Constants.VOICE, new FreeResourcesTypes(Constants.VOICE, initialAmount,
										remainingAmount, "Seconds"));
							}
							else
							{
								logger.info(token+"Its seconds unit");
								resourceMapped.put(Constants.VOICE, new FreeResourcesTypes(Constants.VOICE, initialAmount,
									remainingAmount, freeUnitItem.getMeasureUnitName()));
							}
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType()).equals(Constants.DATA)) {
						long initialAmount = 0L;
						long remainingAmount = 0L;
						try {
							initialAmount = freeUnitItem.getTotalInitialAmount();
						} catch (Exception ex) {
							logger.info(token+Helper.GetException(ex));
						}
						try {
							remainingAmount = freeUnitItem.getTotalUnusedAmount();
						} catch (Exception ex) {
							logger.info(token+Helper.GetException(ex));
						}
						if (resourceMapped.containsKey(Constants.DATA)) {
							FreeResourcesTypes unit = resourceMapped.get(Constants.DATA);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMapped.put(Constants.DATA, unit);
						} else {
							resourceMapped.put(Constants.DATA, new FreeResourcesTypes(Constants.DATA, initialAmount,
									remainingAmount, freeUnitItem.getMeasureUnitName()));
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType()).equals(Constants.SMS)) {
						long initialAmount = 0L;
						long remainingAmount = 0L;
						try {
							initialAmount =freeUnitItem.getTotalInitialAmount();
						} catch (Exception ex) {
						}
						try {
							remainingAmount = freeUnitItem.getTotalUnusedAmount();
						} catch (Exception ex) {
						}
						if (resourceMapped.containsKey(Constants.SMS)) {
							FreeResourcesTypes unit = resourceMapped.get(Constants.SMS);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMapped.put(Constants.SMS, unit);
						} else {
							resourceMapped.put(Constants.SMS, new FreeResourcesTypes(Constants.SMS, initialAmount,
									remainingAmount, freeUnitItem.getMeasureUnitName()));
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType()).equals(Constants.ROAMING_SMS)) {
						long initialAmount = 0L;
						long remainingAmount = 0L;
						try {
							initialAmount = freeUnitItem.getTotalInitialAmount();
						} catch (Exception ex) {
							logger.info(token+Helper.GetException(ex));
						}
						try {
							remainingAmount = freeUnitItem.getTotalUnusedAmount();
						} catch (Exception ex) {
							logger.info(token+Helper.GetException(ex));
						}
						if (resourceMappedR.containsKey(Constants.ROAMING_SMS)) {
							FreeResourcesRoaming unit = resourceMappedR.get(Constants.ROAMING_SMS);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMappedR.put(Constants.ROAMING_SMS, unit);
						} else {
							resourceMappedR.put(Constants.ROAMING_SMS, new FreeResourcesRoaming(Constants.SMS,
									initialAmount, remainingAmount, freeUnitItem.getMeasureUnitName(), ""));
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType())
							.equals(Constants.ROAMING_DATA)) {
						long initialAmount = 0L;
						long remainingAmount = 0L;
						try {
							initialAmount = freeUnitItem.getTotalInitialAmount();
						} catch (Exception ex) {
							logger.info(token+Helper.GetException(ex));
						}
						try {
							remainingAmount = freeUnitItem.getTotalUnusedAmount();
						} catch (Exception ex) {
							logger.info(token+Helper.GetException(ex));
						}
						if (resourceMappedR.containsKey(Constants.ROAMING_DATA)) {
							FreeResourcesRoaming unit = resourceMappedR.get(Constants.ROAMING_DATA);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMappedR.put(Constants.ROAMING_DATA, unit);
						} else {
							resourceMappedR.put(Constants.ROAMING_DATA, new FreeResourcesRoaming(Constants.DATA,
									initialAmount, remainingAmount, freeUnitItem.getMeasureUnitName(), ""));
						}
					} else if (ConfigurationManager.getConfigurationFromCache("free.resource.type."+freeUnitItem.getFreeUnitType())
							.equals(Constants.ROAMING_VOICE)) {
						if (resourceMappedR.containsKey(Constants.ROAMING_VOICE)) {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount =freeUnitItem.getTotalInitialAmount();
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							try {
								remainingAmount = freeUnitItem.getTotalUnusedAmount();
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							
							FreeResourcesRoaming unit = resourceMappedR.get(Constants.ROAMING_VOICE);
							unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
							unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
							resourceMappedR.put(Constants.ROAMING_VOICE, unit);
						} else {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = freeUnitItem.getTotalInitialAmount();
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							try {
								remainingAmount = freeUnitItem.getTotalUnusedAmount();
							} catch (Exception ex) {
								logger.info(token+Helper.GetException(ex));
							}
							resourceMappedR.put(Constants.ROAMING_VOICE, new FreeResourcesRoaming(Constants.VOICE,
									initialAmount, remainingAmount, freeUnitItem.getMeasureUnitName(), ""));
						}
					}
			} catch (Exception ex) {
				logger.info(token+Helper.GetException(ex));
			}
		}
		ArrayList<FreeResourcesTypes> list = new ArrayList<>();
		if (resourceMapped.get(Constants.VOICE) != null) {
			FreeResourcesTypes voicefreeResources = resourceMapped.get(Constants.VOICE);
			logger.info(token+voicefreeResources.getRemainingFormatted());
			voicefreeResources.setRemainingFormatted((voicefreeResources.getResourceRemainingUnits() / 60) + "");
			voicefreeResources.setResourceUnitName("Min");
			//below code is commented to fix problem raised by Arif Babyev that circle is not showing call data if less than or equal to 60sec
			/*
			if (voicefreeResources.getResourceRemainingUnits() > 60) {
				voicefreeResources.setRemainingFormatted((voicefreeResources.getResourceRemainingUnits() / 60) + "");
				voicefreeResources.setResourceUnitName("Mins");
			} else {
				voicefreeResources.setResourceUnitName("Secs");
			}*/
			list.add(voicefreeResources);
		}
		else
		{
			FreeResourcesTypes voicefreeResources = new FreeResourcesTypes();//resourceMapped.get(Constants.VOICE);
//			logger.info(token+voicefreeResources.getRemainingFormatted());
			voicefreeResources.setResourceInitialUnits(0L);
			voicefreeResources.setResourceRemainingUnits(0L);
			voicefreeResources.setRemainingFormatted("0");
			voicefreeResources.setResourceUnitName("Min");
			voicefreeResources.setResourceType("VOICE");
			//below code is commented to fix problem raised by Arif Babyev that circle is not showing call data if less than or equal to 60sec
			/*
			if (voicefreeResources.getResourceRemainingUnits() > 60) {
				voicefreeResources.setRemainingFormatted((voicefreeResources.getResourceRemainingUnits() / 60) + "");
				voicefreeResources.setResourceUnitName("Mins");
			} else {
				voicefreeResources.setResourceUnitName("Secs");
			}*/
			list.add(voicefreeResources);
		}
		if (resourceMapped.get(Constants.DATA) != null) {
			FreeResourcesTypes datafreeresources = resourceMapped.get(Constants.DATA);
			String formatter = readableFileSize(datafreeresources.getResourceRemainingUnits());
			datafreeresources.setResourceUnitName(formatter.split(" ")[1]);
			datafreeresources.setRemainingFormatted(formatter.split(" ")[0]);
			list.add(datafreeresources);
		}
		else
		{
			FreeResourcesTypes datafreeresources = new FreeResourcesTypes();
//			String formatter = readableFileSize(datafreeresources.getResourceRemainingUnits());
			datafreeresources.setResourceInitialUnits(0L);
			datafreeresources.setResourceRemainingUnits(0L);
			datafreeresources.setResourceUnitName("MB");
			datafreeresources.setRemainingFormatted("0");
			datafreeresources.setResourceType("DATA");
			list.add(datafreeresources);
		}
		if (resourceMapped.get(Constants.SMS) != null) {
			FreeResourcesTypes sms = resourceMapped.get(Constants.SMS);
			sms.setRemainingFormatted(sms.getResourceRemainingUnits() + "");
			sms.setResourceUnitName("SMS");
			list.add(sms);
		}
		else
		{
			FreeResourcesTypes sms = new FreeResourcesTypes();
			sms.setRemainingFormatted("0");
			sms.setResourceUnitName("SMS");
			sms.setResourceInitialUnits(0L);
			sms.setResourceRemainingUnits(0L);
			sms.setResourceType("SMS");
			list.add(sms);
		}
		
		FreeResources freeResources = new FreeResources();
		for (FreeResourcesTypes freeResourcesTypes : list) 
		{
			
				freeResources.getFreeResources().add(new FreeResource(freeResourcesTypes.getRemainingFormatted(), "",
					freeResourcesTypes.getResourceType(), freeResourcesTypes.getResourceInitialUnits() + "",
					freeResourcesTypes.getResourceRemainingUnits() + "", freeResourcesTypes.getResourceUnitName(), ""));
			
		}
		ArrayList<FreeResourcesRoaming> listR = new ArrayList<>();
		
		
		if (resourceMappedR.get(Constants.ROAMING_VOICE) != null) {
			FreeResourcesRoaming data = resourceMappedR.get(Constants.ROAMING_VOICE);
			if (data.getResourceRemainingUnits() > 60) {
				data.setRemainingFormatted((data.getResourceRemainingUnits() / 60) + "");
				data.setResourceUnitName("Min");
			} else {
				data.setRemainingFormatted(data.getResourceRemainingUnits() + "");
				data.setResourceUnitName("Secs");
			}
			listR.add(data);
		}
		else
		{
			FreeResourcesRoaming data = new FreeResourcesRoaming();
			data.setRemainingFormatted("0");
			data.setResourceInitialUnits(0L);
			data.setResourceRemainingUnits(0L);
			data.setResourceType("VOICE");
			data.setResourceUnitName("Min");
			listR.add(data);
		}
		if (resourceMappedR.get(Constants.ROAMING_DATA) != null) {
			FreeResourcesRoaming data = resourceMappedR.get(Constants.ROAMING_DATA);
			String formatter = readableFileSize(data.getResourceRemainingUnits());
			data.setResourceUnitName(formatter.split(" ")[1]);
			data.setRemainingFormatted(formatter.split(" ")[0]);
			listR.add(new FreeResourcesRoaming(data.getResourceType(), data.getResourceInitialUnits(),
					data.getResourceRemainingUnits(), data.getResourceUnitName(), data.getRemainingFormatted()));
		}
		else
		{
			FreeResourcesRoaming data = new FreeResourcesRoaming();
//			String formatter = readableFileSize(data.getResourceRemainingUnits());
			data.setResourceUnitName("MB");
			data.setRemainingFormatted("0");
			data.setResourceInitialUnits(0L);
			data.setResourceRemainingUnits(0L);
			data.setResourceType("DATA");
			listR.add(new FreeResourcesRoaming(data.getResourceType(), data.getResourceInitialUnits(),
					data.getResourceRemainingUnits(), data.getResourceUnitName(), data.getRemainingFormatted()));
		}
		if (resourceMappedR.get(Constants.ROAMING_SMS) != null) {
			FreeResourcesRoaming rSms = resourceMappedR.get(Constants.ROAMING_SMS);
			rSms.setRemainingFormatted(rSms.getResourceRemainingUnits() + "");
			listR.add(rSms);
		}
		else
		{
			FreeResourcesRoaming rSms = new FreeResourcesRoaming();
			rSms.setRemainingFormatted("0");
			rSms.setResourceInitialUnits(0L);
			rSms.setResourceRemainingUnits(0L);
			rSms.setResourceType("SMS");
			rSms.setResourceUnitName("SMS");
			
			listR.add(rSms);
		}
		for (FreeResourcesRoaming freeResourcesTypes : listR) 
		{
			
				freeResources.getFreeResourcesRoaming().add(new FreeResourcesRoaming(freeResourcesTypes.getResourceType(),
					freeResourcesTypes.getResourceInitialUnits(), freeResourcesTypes.getResourceRemainingUnits(),
					freeResourcesTypes.getResourceUnitName(), freeResourcesTypes.getRemainingFormatted()));
			
		}
		return freeResources;
	}
	
	
	
	
	

	public static FreeResources processUnityTypesHomeInCaseOfNull(QueryFreeUnitResultMsg res, String tariffType,String token)
			throws IOException, Exception 
	{
		
		ArrayList<FreeResourcesTypes> list = new ArrayList<>();

		FreeResourcesTypes voicefreeResources = new FreeResourcesTypes();
		voicefreeResources.setResourceInitialUnits(0L);
		voicefreeResources.setResourceRemainingUnits(0L);
		voicefreeResources.setRemainingFormatted("0");
		voicefreeResources.setResourceUnitName("Min");
		voicefreeResources.setResourceType("VOICE");
		//below code is commented to fix problem raised by Arif Babyev that circle is not showing call data if less than or equal to 60sec
		
		list.add(voicefreeResources);

		FreeResourcesTypes datafreeresources = new FreeResourcesTypes();
		datafreeresources.setResourceInitialUnits(0L);
		datafreeresources.setResourceRemainingUnits(0L);
		datafreeresources.setResourceUnitName("MB");
		datafreeresources.setRemainingFormatted("0");
		datafreeresources.setResourceType("DATA");
		list.add(datafreeresources);

		FreeResourcesTypes sms = new FreeResourcesTypes();
		sms.setRemainingFormatted("0");
		sms.setResourceUnitName("SMS");
		sms.setResourceInitialUnits(0L);
		sms.setResourceRemainingUnits(0L);
		sms.setResourceType("SMS");
		list.add(sms);
		
		FreeResources freeResources = new FreeResources();
		for (FreeResourcesTypes freeResourcesTypes : list) 
		{
			
				freeResources.getFreeResources().add(new FreeResource(freeResourcesTypes.getRemainingFormatted(), "",
					freeResourcesTypes.getResourceType(), freeResourcesTypes.getResourceInitialUnits() + "",
					freeResourcesTypes.getResourceRemainingUnits() + "", freeResourcesTypes.getResourceUnitName(), ""));
			
		}
		ArrayList<FreeResourcesRoaming> listR = new ArrayList<>();
		
		

		FreeResourcesRoaming data = new FreeResourcesRoaming();
		data.setRemainingFormatted("0");
		data.setResourceInitialUnits(0L);
		data.setResourceRemainingUnits(0L);
		data.setResourceType("VOICE");
		data.setResourceUnitName("Min");
		listR.add(data);


		FreeResourcesRoaming dataNet = new FreeResourcesRoaming();
		dataNet.setResourceUnitName("MB");
		dataNet.setRemainingFormatted("0");
		dataNet.setResourceInitialUnits(0L);
		dataNet.setResourceRemainingUnits(0L);
		dataNet.setResourceType("DATA");
		listR.add(new FreeResourcesRoaming(dataNet.getResourceType(), dataNet.getResourceInitialUnits(),
				dataNet.getResourceRemainingUnits(), dataNet.getResourceUnitName(), dataNet.getRemainingFormatted()));

		FreeResourcesRoaming rSms = new FreeResourcesRoaming();
		rSms.setRemainingFormatted("0");
		rSms.setResourceInitialUnits(0L);
		rSms.setResourceRemainingUnits(0L);
		rSms.setResourceType("SMS");
		rSms.setResourceUnitName("SMS");
		
		listR.add(rSms);
		for (FreeResourcesRoaming freeResourcesTypes : listR) 
		{
			
				freeResources.getFreeResourcesRoaming().add(new FreeResourcesRoaming(freeResourcesTypes.getResourceType(),
					freeResourcesTypes.getResourceInitialUnits(), freeResourcesTypes.getResourceRemainingUnits(),
					freeResourcesTypes.getResourceUnitName(), freeResourcesTypes.getRemainingFormatted()));
			
		}
		return freeResources;
	}
	
	
	public static String readableFileSize(long size) {
		
		//As Arif's request, we are changing it, 19-08-2019 10:27 AM
		/*Logic for calculation of GB on dashboard circles:
			for now for getting GB in calculation is using formula XXX/1024
			on AZF we need to change only calculation of GB
			via API you are getting data in KB
			for change to MB you are using existing KB/1024
			this step will not be changed
			but next step - change MB to GB - should be MB/1000
			for example subscriber will have 10000MB
			on current existing formula it will be 9.77GB
			on new calculation it should reflect 10GB
			we have transformation to GB only on dashboard */
		if (size <= 0)
			return "0 MB";
		/*final String[] units = new String[] { "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size) / Math.log10(1000));
		return new DecimalFormat("###0.##").format(size / Math.pow(1000, digitGroups)) + " " + units[digitGroups];*/
		String formattedValue = "";
		if(size / 1024 > 1000)
		{
		formattedValue = new DecimalFormat("###0.##").format((size / 1024.0)/1000.0)+" GB";
		}else{
		formattedValue = new DecimalFormat("###0.##").format((size / 1024.0))+" MB";
		}
		return formattedValue;
	}

	public FreeResources getFreeResources(String msisdn, String tariffType) throws IOException, Exception {
		USSDGWQueryFreeResourceReqMsg req = new USSDGWQueryFreeResourceReqMsg();
		req.setReqHeader(getRequestHeaderForFreeResources());
		QueryObj quObj = new QueryObj();
		com.huawei.bss.soaif._interface.ussdgateway.USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode subAccessCode = new com.huawei.bss.soaif._interface.ussdgateway.USSDGWQueryFreeResourceReqMsg.QueryObj.SubAccessCode();
		subAccessCode.setPrimaryIdentity(msisdn);
		quObj.setSubAccessCode(subAccessCode);
		req.setQueryObj(quObj);
		USSDGWQueryFreeResourceRspMsg res = USSDService.getInstance().ussdGatewayQueryFreeResource(req);
		if (res.getRspHeader().getReturnCode().equals("0000")) {
			FreeResources finalresources = processUnityTypes(res, tariffType);
			finalresources.setReturnCode("0000");
			finalresources.setReturnMsg(res.getRspHeader().getReturnMsg());
			return finalresources;
		} else {
			FreeResources finalresourcenew = new FreeResources();
			finalresourcenew.setReturnCode(res.getRspHeader().getReturnCode());
			finalresourcenew.setReturnMsg(res.getRspHeader().getReturnMsg());
			return finalresourcenew;
		}
	}

	private ReqHeader getRequestHeaderForFreeResources() {
		ReqHeader reqh = null;
		reqh = new ReqHeader();
		reqh.setAccessUser(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessUser").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("ussd.reqh.ChannelId").trim());
		reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessPwd").trim());
		return reqh;
	}

}
