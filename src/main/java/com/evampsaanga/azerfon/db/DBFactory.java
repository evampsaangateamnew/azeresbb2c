package com.evampsaanga.azerfon.db;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;

public class DBFactory {
	private DBFactory() {
	}

	public static final Logger logger = Logger.getLogger("azerfon-esb");
	private static Connection crConnection = null;
	private static Connection EsbdbConnection = null;
	private static Connection smsConnection = null;
	private static Connection connectionApp = null;
	private static Connection connectionMagentoDB = null;
	
	private static DataSource dataSourceMagento = null;

	public static Connection getDbConnection() {
		try {
			if (crConnection == null || crConnection.isClosed() || !crConnection.isValid(0)) {
				DriverManager.registerDriver(new com.mysql.jdbc.Driver());
				String dburl = ConfigurationManager.getDBProperties("logs.url");
				String username = ConfigurationManager.getDBProperties("logs.user");
				String password = ConfigurationManager.getDBProperties("logs.password");
				crConnection = DriverManager.getConnection(dburl, username, password);
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		return crConnection;
	}

	/*
	 * public static Connection getDbConnectionESB() { try { if (EsbdbConnection ==
	 * null || EsbdbConnection.isClosed() || !EsbdbConnection.isValid(0)) {
	 * DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	 * Class.forName("com.mysql.jdbc.Driver"); String dburl =
	 * ConfigurationManager.getDBProperties("esb.db.url"); String username =
	 * ConfigurationManager.getDBProperties("esb.db.user"); String password =
	 * ConfigurationManager.getDBProperties("esb.db.password"); EsbdbConnection =
	 * DriverManager.getConnection(dburl, username, password); } } catch (Exception
	 * ex) { logger.error(Helper.GetException(ex)); } return EsbdbConnection; }
	 */

	public static int insertMessageIntoEsmg(String mobilenum, String message, String language, String sender,
			StringBuffer stringBuffer,String token) throws ClassNotFoundException, SQLException, UnsupportedEncodingException {

		String shortcode = ConfigurationManager.getDBProperties("bakcell.smsc.shortcode");
		String driverid = ConfigurationManager.getDBProperties("bakcell.smsc.driverid.1");
		String encoding = "UCS2Encoding";
		if (sender != null && sender.length() > 0) {
			shortcode = Constants.AZERI_COUNTRY_CODE + sender;
			driverid = ConfigurationManager.getDBProperties("bakcell.smsc.driverid.2");
			encoding = "UCS2Encoding";
		}
		if (!driverid.equalsIgnoreCase("")) {
			message = message.replace("'", "\\'");
			String insertMessageSql = "insert into esmg_sms_mt (id,driverid,mobilenum,shortcode,message,timein,encoding,status)"
					+ " values (null," + driverid + ",'" + Constants.AZERI_COUNTRY_CODE + mobilenum + "','" + shortcode
					+ "','" + message + "',NOW(),'" + encoding + "',1)";
			logger.info(token+"QUERY FOR SEND SMS" + insertMessageSql);

			int result1 = 0;
			try {
				PreparedStatement preparedStatement1 = getESMGDbConnection().prepareStatement("SET NAMES 'utf8mb4'");
				preparedStatement1.executeQuery();
				Statement prepareStatement = getESMGDbConnection().prepareStatement(insertMessageSql);
				result1 = prepareStatement.executeUpdate(insertMessageSql);
				if (stringBuffer != null) {
					stringBuffer.append(Helper.getOMlogTimeStamp() + " Execute Query " + prepareStatement.toString());

				}
				preparedStatement1.close();
				prepareStatement.close();
			} catch (Exception e) {
				String dburl = ConfigurationManager.getDBProperties("smsc.url");
				String username = ConfigurationManager.getDBProperties("smsc.user");
				String password = ConfigurationManager.getDBProperties("smsc.password");
				logger.info("ESMG , SMSC : " + dburl + ":" + username + ":" + password);
				if (stringBuffer != null)
					stringBuffer.append(Helper.getOMlogTimeStamp() + "Insertion Failed: " + e.getMessage());
				logger.info(token+Helper.GetException(e));
			}
			return result1;
		}
		return -1;
	}

	public static Connection getESMGDbConnection() throws SQLException {
		if (smsConnection == null || smsConnection.isClosed() || !smsConnection.isValid(0)) {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			  try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
			}
			String dburl = ConfigurationManager.getDBProperties("smsc.url");
			String username = ConfigurationManager.getDBProperties("smsc.user");
			String password = ConfigurationManager.getDBProperties("smsc.password");
			
			smsConnection = DriverManager.getConnection(dburl, username, password);
			logger.info("ESMG , SMSC : " + dburl + ":" + username + ":" + password);
		}
		return smsConnection;
	}

	public static Connection getAppConnection()
			throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		if (connectionApp == null || connectionApp.isClosed() || !connectionApp.isValid(2)) {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			String dburl = ConfigurationManager.getDBProperties("appdb.url");
			String username = ConfigurationManager.getDBProperties("appdb.user");
			String password = ConfigurationManager.getDBProperties("appdb.password");
			logger.info("APP SERVER DB" + dburl + ":" + username + ":" + password);

			connectionApp = DriverManager.getConnection(dburl, username, password);
		}
		return connectionApp;
	}

	public static Connection getMagentoDBConnection()
			throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
//		if (connectionMagentoDB == null || connectionMagentoDB.isClosed() || !connectionMagentoDB.isValid(2)) {
//			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
//			String dburl = ConfigurationManager.getDBProperties("magentodb.url");
//			String username = ConfigurationManager.getDBProperties("magentodb.user");
//			String password = ConfigurationManager.getDBProperties("magentodb.password");
//			logger.info("APP SERVER DB" + dburl + ":" + username + ":" + password);
//
//			connectionMagentoDB = DriverManager.getConnection(dburl, username, password);
//		}
//		return connectionMagentoDB;
		
		DataSource dataSource = getMagentoDataSource();
		Connection connection = dataSource.getConnection();
		return connection;
	}
	
	public static DataSource getMagentoDataSource() {

		if (dataSourceMagento == null) {
			DataSource eds = new DataSource();
			PoolProperties p = new PoolProperties();

			String dburl = ConfigurationManager.getDBProperties("magentodb.url");
			String username = ConfigurationManager.getDBProperties("magentodb.user");
			String password = ConfigurationManager.getDBProperties("magentodb.password");
			try {
				DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			} catch (SQLException e) {
				logger.error(Helper.GetException(e));
			}
			p.setDriverClassName("com.mysql.jdbc.Driver");
			p.setUrl(dburl);
			p.setUsername(username);
			p.setPassword(password);
			p.setMaxActive(2000);
			p.setValidationQuery("select 1");
			p.setValidationInterval(5000);

			p.setMinIdle(Integer.parseInt(ConfigurationManager.getDBProperties("db.connection.magento.min.idle")));// 200
			p.setMaxWait(Integer.parseInt(ConfigurationManager.getDBProperties("db.connection.magento.max.wait")));// 3000);
			p.setRemoveAbandoned(true);
			p.setRemoveAbandonedTimeout(
					Integer.parseInt(ConfigurationManager.getDBProperties("db.connection.magento.abandoned.timeout")));// 100
			eds.setPoolProperties(p);
			dataSourceMagento = eds;

		}

		return dataSourceMagento;
	}
	
}