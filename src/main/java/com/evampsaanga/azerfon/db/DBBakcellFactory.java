package com.evampsaanga.azerfon.db;

import java.sql.Connection;

import org.apache.log4j.Logger;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.developer.utils.Helper;

public class DBBakcellFactory {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	static {
		new oracle.jdbc.OracleDriver();
		new oracle.jdbc.driver.OracleDriver();
	}

	public static Connection getConnection() {
		String struserid = ConfigurationManager.getDBProperties("ods.user");
		String strcredentialpass = ConfigurationManager.getDBProperties("ods.password");
		String dbURL = ConfigurationManager.getDBProperties("ods.url");
		logger.info(">>>>>>> ODS DATABASE CONECTIVITY CREDENTIALS <<<<<<<");
		logger.info("ODS USER :" + struserid);
		logger.info("ODS PASSWORD :" + strcredentialpass);
		logger.info("ODS URL :" + dbURL);
		try {
			return java.sql.DriverManager.getConnection(dbURL, struserid, strcredentialpass);
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		return null;
	}
}
