package com.evampsaanga.azerfon.ordermanagementV2.details;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class OrderDetailsRequest extends BaseRequest{

	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
