package com.evampsaanga.azerfon.ordermanagementV2.details;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Helper;

@Path("/bakcell")
public class OrderDetailsRequestLand {

	public static final Logger logger = Logger.getLogger("azerfon-esb");
	
	@POST
	@Path("/getorderdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OrderDetailsResponse getOrderDetails(@Header("credentials") String credential,@Header("Content-Type") String contentType, @Body() String requestBody)
	{
		OrderDetailsResponse orderDetailsResponse = new OrderDetailsResponse();
		OrderDetailsRequest orderDetailsRequest = new OrderDetailsRequest();
		try
		{
			if(!AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential))
			{
				orderDetailsResponse.setReturnCode(ResponseCodes.ERROR_401_CODE);
				orderDetailsResponse.setReturnMsg(ResponseCodes.ERROR_401);
			}
			orderDetailsRequest = Helper.JsonToObject(requestBody, OrderDetailsRequest.class);
			orderDetailsResponse.setOrderDetailsResponse(getOrderDetailsByOrderId(orderDetailsRequest));
			orderDetailsResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			orderDetailsResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		}catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		return orderDetailsResponse;
	}
	
	private ArrayList<OrderDetailsResponseModel> getOrderDetailsByOrderId(OrderDetailsRequest detailsRequest) throws SQLException
	{
		ArrayList<OrderDetailsResponseModel> arrayList = new ArrayList<>();
		String getOrderDetailsQuery = "select msisdn,`status`,responsecode,responsedescription from order_details where order_id_fk=?";
		PreparedStatement preparedStatement = DBFactory.getDbConnection().prepareStatement(getOrderDetailsQuery);
		preparedStatement.setString(1, detailsRequest.getOrderId());
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next())
		{
			OrderDetailsResponseModel model = new OrderDetailsResponseModel();
			model.setMsisdn(resultSet.getString("msisdn"));
			model.setResultCode(resultSet.getString("responsecode"));
			if(resultSet.getString("status").equalsIgnoreCase("P"))
				model.setResultCode("order.p");
			String desc = ConfigurationManager.getConfigurationFromCache(model.getResultCode()+"."+detailsRequest.getLang());
			if(desc != null && desc.length() > 0)
				model.setResultDescription(desc);
			else
				model.setResultDescription(resultSet.getString("responsedescription"));
			arrayList.add(model);
		}
		return arrayList;
	}
	
}
