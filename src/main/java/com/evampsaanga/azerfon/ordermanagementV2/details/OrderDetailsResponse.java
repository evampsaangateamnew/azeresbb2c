package com.evampsaanga.azerfon.ordermanagementV2.details;

import java.util.ArrayList;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class OrderDetailsResponse extends BaseResponse{

	ArrayList<OrderDetailsResponseModel> orderDetailsResponse;

	public ArrayList<OrderDetailsResponseModel> getOrderDetailsResponse() {
		return orderDetailsResponse;
	}

	public void setOrderDetailsResponse(ArrayList<OrderDetailsResponseModel> orderDetailsResponse) {
		this.orderDetailsResponse = orderDetailsResponse;
	}
}
