package com.evampsaanga.azerfon.verifypassport;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class VerifyPassportRequest extends BaseRequest {
	String passPortNumber = "";

	/**
	 * @return the passPortNumber
	 */
	public String getPassPortNumber() {
		return passPortNumber;
	}

	/**
	 * @param passPortNumber
	 *            the passPortNumber to set
	 */
	public void setPassPortNumber(String passPortNumber) {
		this.passPortNumber = passPortNumber;
	}
}
