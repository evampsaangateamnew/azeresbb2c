package com.evampsaanga.azerfon.signupverifyotp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.evampsaanga.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SignUpRequest")
public class SignUpResponse extends BaseResponse {
	/*
	 * CustomerData customerData=new CustomerData(); public CustomerData
	 * getCustomerData() { return customerData; } public void
	 * setCustomerData(CustomerData customerData) { this.customerData =
	 * customerData; }
	 * 
	 * @Override public String toString() { return
	 * "SignUpResponse [customerData=" + customerData.toString() + "]"; }
	 */
}
