package com.evampsaanga.azerfon.mysubscription;

import java.util.ArrayList;

public class FreeUnitItemDetails {
	private String offeringId = "";
	private String isDaily;
	ArrayList<UnitItem> usage = new ArrayList<>();

	/**
	 * @return the usage
	 */
	public ArrayList<UnitItem> getUsage() {
		return usage;
	}

	/**
	 * @param usage
	 *            the usage to set
	 */
	public void setUsage(ArrayList<UnitItem> usage) {
		this.usage = usage;
	}

	public FreeUnitItemDetails(String offeringId,String isDaily, int value) {
		super();
		this.offeringId = offeringId;
		this.isDaily=isDaily;
	}
	public FreeUnitItemDetails(String offeringId) {
		super();
		this.offeringId = offeringId;
		
	}

	public String getIsDaily() {
		return isDaily;
	}

	public void setIsDaily(String isDaily) {
		this.isDaily = isDaily;
	}

	public FreeUnitItemDetails(String offeringId, String status) {
		super();
		this.offeringId = offeringId;
	}

	/**
	 * @return the offeringId
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * @param offeringId
	 *            the offeringId to set
	 */
	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}
}
