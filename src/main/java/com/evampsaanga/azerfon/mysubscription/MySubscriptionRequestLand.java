package com.evampsaanga.azerfon.mysubscription;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.amqimplementationsesb.Logs;
import com.evampsaanga.amqimplementationsesb.LogsType;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.configs.ThirdPartyNames;
import com.evampsaanga.configs.Transactions;
import com.evampsaanga.developer.utils.Decrypter;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.services.CBSBBService;
import com.huawei.bme.cbsinterface.bbcommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitRequest;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitRequest.QueryObj;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitRequestMsg;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult.FreeUnitItem;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult.FreeUnitItem.FreeUnitItemDetail.FreeUnitOrigin.OfferingKey;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResultMsg;
import com.huawei.crm.azerfon.bss.basetype.ExtParameterInfo;
import com.huawei.crm.azerfon.bss.basetype.GetSubOfferingInfo;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;

@Path("/azerfon/")
public class MySubscriptionRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	DecimalFormat decimalFormat = new DecimalFormat("#####.##");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Getv2(@Header("credentials") String credential, @Body() String requestBody) {
		Response resp = new Response();
		Logs logs = new Logs();
		try {
			Helper.logInfoMessageV2("Request Landed on MySubscriptionRequestLand V2:" + requestBody);

			logs.setTransactionName(Transactions.MYSUBSCRIPTION_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.MY_SUBSCRIPTION);
			logs.setTableType(LogsType.AppFaq);
			JSONObject jObject = new JSONObject(requestBody);
			if (jObject.has("individualMsisdn"))
				jObject.remove("individualMsisdn");
			Request cclient = null;
			try {
				cclient = Helper.JsonToObject(jObject.toString(), Request.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
						logs.setTransactionName(Transactions.MYSUBSCRIPTION_TRANSACTION_NAME_B2B);
					}
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			}
			catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				}
				catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				}
				else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					// code Logic here

					QueryFreeUnitRequestMsg msg = new QueryFreeUnitRequestMsg();
					QueryFreeUnitRequest qFUR = new QueryFreeUnitRequest();
					QueryObj qO = new QueryObj();
					SubAccessCode sAC = new SubAccessCode();
					sAC.setPrimaryIdentity(cclient.getmsisdn());
					qO.setSubAccessCode(sAC);
					qFUR.setQueryObj(qO);
					msg.setQueryFreeUnitRequest(qFUR);
					msg.setRequestHeader(CBSBBService.getRequestHeader());
					QueryFreeUnitResultMsg response = CBSBBService.getInstance().queryFreeUnit(msg);
					if (response.getResultHeader().getResultCode().equals("0")) {
						resp.setItems(processCBSBBv2(response, resp, cclient.getmsisdn()));
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					else {
						resp.setReturnCode(response.getResultHeader().getResultCode());
						resp.setReturnMsg(response.getResultHeader().getResultDesc());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				}
				else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		}
		catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public ArrayList<FreeUnitItemDetails> processCBSBBv2(QueryFreeUnitResultMsg response, Response responseTemp, String msisdn) {
		HashMap<String, ArrayList<FreeUnitItemDetails>> freeResourcemap = new HashMap<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat desiredDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		HashMap<String, FreeUnitItemDetails> map = new HashMap<>();
		List<com.evampsaanga.azerfon.getcustomerrequest.OfferingInfo> offeringInfos = new ArrayList<com.evampsaanga.azerfon.getcustomerrequest.OfferingInfo>();
		GetSubscriberResponse respns = new com.evampsaanga.azerfon.getsubscriber.CRMSubscriberService().GetSubscriberRequest(msisdn);
		if (respns.getResponseHeader().getRetCode().equals("0")) {
			if (respns.getGetSubscriberBody().getSupplementaryOfferingList() != null) {
				for (GetSubOfferingInfo offeringInfo : respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo()) {
					String getOfferingShortName = "";
					String networkType = "";
					String OfferingName = "";

					if (offeringInfo.getExtParamList() != null) {

						for (ExtParameterInfo paramInfo : offeringInfo.getExtParamList().getParameterInfo()) {
							if (paramInfo.getParamName().equals("OfferingShortName"))
								getOfferingShortName = paramInfo.getParamValue();
							if (paramInfo.getParamName().equals("NetworkType"))
								networkType = paramInfo.getParamValue();
							if (paramInfo.getParamName().equals("OfferingName"))
								OfferingName = paramInfo.getParamValue();
						}

					}

					try {
						offeringInfos.add(new com.evampsaanga.azerfon.getcustomerrequest.OfferingInfo(offeringInfo.getOfferingId().getOfferingId(), OfferingName, offeringInfo.getOfferingId().getOfferingCode(), getOfferingShortName,
								ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_STATUS + offeringInfo.getStatus()), networkType, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(offeringInfo.getEffectiveTime())),
								new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(offeringInfo.getExpiredTime()))));
					}
					catch (ParseException ex) {
						logger.error(Helper.GetException(ex));
					}
				}
			}
		}
		if (response != null && response.getQueryFreeUnitResult() != null) {
			for (FreeUnitItem item : response.getQueryFreeUnitResult().getFreeUnitItem()) {
				logger.info("check if resource type is defined as excluded: " + item.getFreeUnitType());
				if (!checkIfResourceTypeExcluded(item.getFreeUnitType())) {
					try {
						for (FreeUnitItemDetail itemDetails : item.getFreeUnitItemDetail()) {
							if (itemDetails.getFreeUnitOrigin() != null && itemDetails.getFreeUnitOrigin().getOfferingKey() != null) {
								try {
									OfferingKey offeringKey = itemDetails.getFreeUnitOrigin().getOfferingKey();
									String offeringId = "" + offeringKey.getOfferingID();
									if (map.containsKey(offeringId)) {
										FreeUnitItemDetails object = map.get(offeringId);
										if (item.getMeasureUnitName().equalsIgnoreCase("KB")) {
											String initialFormatted = decimalFormat.format(itemDetails.getInitialAmount() / 1024.0);
											String remainingFormatted = decimalFormat.format(itemDetails.getCurrentAmount() / 1024.0);
											logger.info(item.getFreeUnitType());
											if (checkIfFreeResource(item.getFreeUnitType())) {
												logger.info("Free resource unlimited setting usage to zero");
												initialFormatted = "FREE";
												remainingFormatted = "FREE";
											}
											object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, "MB", itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()),
													ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType()), getIconSorting(item.getFreeUnitType())));
										}
										else {
											String initialFormatted = itemDetails.getInitialAmount() + "";
											String remainingFormatted = itemDetails.getCurrentAmount() + "";
											logger.info(item.getFreeUnitType());
											if (checkIfFreeResource(item.getFreeUnitType())) {
												logger.info("Free resource unlimited settig usage to zero");
												initialFormatted = "FREE";
												remainingFormatted = "FREE";
											}
											object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, item.getMeasureUnitName(), itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()),
													ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType()), getIconSorting(item.getFreeUnitType())));
										}
										if (checkIfPTS(offeringId) && item.getMeasureUnit().equalsIgnoreCase("KB") && itemDetails.getInitialAmount() == 0)
											logger.info("PTS with zero skipping");
										else
											map.put(offeringId, object);
									}
									else {

										FreeUnitItemDetails object = null;
										if (dailyOfferingId(offeringId)) {
											object = new FreeUnitItemDetails(offeringId, "true", 1);
										}
										else {
											object = new FreeUnitItemDetails(offeringId, "false", 1);
										}

										if (item.getMeasureUnitName().equalsIgnoreCase("KB")) {
											String initialFormatted = decimalFormat.format(itemDetails.getInitialAmount() / 1024.0);
											String remainingFormatted = decimalFormat.format(itemDetails.getCurrentAmount() / 1024.0);
											logger.info(item.getFreeUnitType());
											if (checkIfFreeResource(item.getFreeUnitType())) {
												logger.info("Free resource unlimited settig usage to zero");
												initialFormatted = "FREE";
												remainingFormatted = "FREE";
											}
											object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, "MB", itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()),
													ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType()), getIconSorting(item.getFreeUnitType())));
										}
										else {
											String initialFormatted = itemDetails.getInitialAmount() + "";
											String remainingFormatted = itemDetails.getCurrentAmount() + "";
											logger.info(item.getFreeUnitType());
											if (checkIfFreeResource(item.getFreeUnitType())) {
												logger.info("Free resource unlimited settig usage to zero");
												initialFormatted = "FREE";
												remainingFormatted = "FREE";
											}
											object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, item.getMeasureUnitName(), itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()),
													ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType()), getIconSorting(item.getFreeUnitType())));
										}
										if (checkIfPTS(offeringId) && itemDetails.getCurrentAmount() == 0)
											logger.info("PTS with zero skipping");
										else
											map.put(offeringId, object);
									}
								}
								catch (Exception ex) {
									logger.error(Helper.GetException(ex));
								}
							}
							else // for cumulative items
							{
								boolean isOfferingIdMatched = false;
								logger.info("No Origin type detected, treating as cumulative offer checking offering id against free unit type..." + item.getFreeUnitType());
								String[] offeringIdArrayFromConfig = ConfigurationManager.getConfigurationFromCache("cumulative." + item.getFreeUnitType()).split(",");
								if (offeringIdArrayFromConfig != null)
									for (int ind = 0; ind < offeringIdArrayFromConfig.length && !(isOfferingIdMatched); ind++) {
										String offeringId = offeringIdArrayFromConfig[ind];
										logger.info("Cumulative offering id: " + offeringId);
										for (int j = 0; j < offeringInfos.size(); j++) {
											logger.info("Matching cumulative offering id to supplementary offerings : " + offeringInfos.get(j).getOfferingId() + " : " + offeringId);
											if (offeringInfos.get(j).getOfferingId().equalsIgnoreCase(offeringId)) {
												logger.info("Offering ID Matched, checking if exist already or not...");
												isOfferingIdMatched = true;
												if (map.containsKey(offeringId)) {
													logger.info("Offering id already exist");
													FreeUnitItemDetails object = null;
													if (dailyOfferingId(offeringId)) {
														object = new FreeUnitItemDetails(offeringId, "true", 1);
													}
													else {
														object = new FreeUnitItemDetails(offeringId, "false", 1);
													}
													if (item.getMeasureUnitName().equalsIgnoreCase("KB")) {
														String initialFormatted = decimalFormat.format(itemDetails.getInitialAmount() / 1024.0);
														String remainingFormatted = decimalFormat.format(itemDetails.getCurrentAmount() / 1024.0);
														logger.info(item.getFreeUnitType());
														if (checkIfFreeResource(item.getFreeUnitType())) {
															logger.info("Free resource unlimited settig usage to zero");
															initialFormatted = "FREE";
															remainingFormatted = "FREE";
														}
														object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, "MB", itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()),
																ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType()), getIconSorting(item.getFreeUnitType())));
													}
													else {
														String initialFormatted = itemDetails.getInitialAmount() + "";
														String remainingFormatted = itemDetails.getCurrentAmount() + "";
														if (checkIfFreeResource(item.getFreeUnitType())) {
															logger.info("Free resource unlimited settig usage to zero");
															initialFormatted = "FREE";
															remainingFormatted = "FREE";
														}
														object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, item.getMeasureUnitName(), itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()),
																ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType()), getIconSorting(item.getFreeUnitType())));
													}
													logger.info("Added cumulative : " + offeringId + " : " + object.getUsage().get(0).getTotalUnits());
													map.put(offeringId, object);
												}
												else {
													logger.info("Offering id does not exist putting new object");
													FreeUnitItemDetails object = null;
													if (dailyOfferingId(offeringId)) {
														object = new FreeUnitItemDetails(offeringId, "true", 1);
													}
													else {
														object = new FreeUnitItemDetails(offeringId, "false", 1);
													}

													if (item.getMeasureUnitName().equalsIgnoreCase("KB")) {
														String initialFormatted = decimalFormat.format(itemDetails.getInitialAmount() / 1024.0);
														String remainingFormatted = decimalFormat.format(itemDetails.getCurrentAmount() / 1024.0);
														logger.info(item.getFreeUnitType());
														if (checkIfFreeResource(item.getFreeUnitType())) {
															logger.info("Free resource unlimited settig usage to zero");
															initialFormatted = "FREE";
															remainingFormatted = "FREE";
														}
														object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, "MB", itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()),
																ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType()), getIconSorting(item.getFreeUnitType())));
													}
													else {
														String initialFormatted = itemDetails.getInitialAmount() + "";
														String remainingFormatted = itemDetails.getCurrentAmount() + "";
														if (checkIfFreeResource(item.getFreeUnitType())) {
															logger.info("Free resource unlimited settig usage to zero");
															initialFormatted = "FREE";
															remainingFormatted = "FREE";
														}
														object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), itemDetails.getInitialAmount() + "", itemDetails.getCurrentAmount() + "", item.getMeasureUnitName(), itemDetails.getEffectiveTime(), itemDetails.getExpireTime(),
																getIconMapping(item.getFreeUnitType()), ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType()), getIconSorting(item.getFreeUnitType())));
													}
													logger.info("Added cumulative : " + offeringId + " : " + object.getUsage().get(0).getTotalUnits());
													map.put(offeringId, object);
												}
												break;// Got one offering id
														// matched
														// so breaking to avoid
														// more
														// traversing over loop.
											}
										}
									}
								// Below block will check if offering id not
								// matched
								// with any cumulative one its free resource
								logger.info("Offering id matched : " + isOfferingIdMatched);
								if (!(isOfferingIdMatched)) {
									logger.info("Offering id not matched so getting it as free resource");
									String offeringId = Long.toString(System.currentTimeMillis());
									Thread.sleep(1);
									logger.info(offeringId);
									FreeUnitItemDetails object = null;
									if (dailyOfferingId(offeringId)) {
										object = new FreeUnitItemDetails(offeringId, "true", 1);
									}
									else {
										object = new FreeUnitItemDetails(offeringId, "false", 1);
									}
									String unitType = ConfigurationManager.getConfigurationFromCache("free.resource.type." + item.getFreeUnitType());
									if (unitType.equalsIgnoreCase(Constants.VOICE))
										unitType = "513";
									else if (unitType.equalsIgnoreCase(Constants.DATA))
										unitType = "511";
									else if (unitType.equalsIgnoreCase(Constants.SMS))
										unitType = "514";
									if (!freeResourcemap.containsKey(unitType)) {
										ArrayList<FreeUnitItemDetails> arrayList = new ArrayList<>();
										if (item.getMeasureUnitName().equalsIgnoreCase("KB")) {
											String initialFormatted = decimalFormat.format(itemDetails.getInitialAmount() / 1024.0);
											String remainingFormatted = decimalFormat.format(itemDetails.getCurrentAmount() / 1024.0);
											object.getUsage()
													.add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, "MB", itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()), unitType, getIconSorting(item.getFreeUnitType())));
										}
										else
											object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), itemDetails.getInitialAmount() + "", itemDetails.getCurrentAmount() + "", item.getMeasureUnitName(), itemDetails.getEffectiveTime(), itemDetails.getExpireTime(),
													getIconMapping(item.getFreeUnitType()), unitType, getIconSorting(item.getFreeUnitType())));
										logger.info("Added Free Resource : " + offeringId + " : " + object.getUsage().get(0).getTotalUnits());
										arrayList.add(object);
										freeResourcemap.put(unitType, arrayList);
									}
									else {
										if (item.getMeasureUnitName().equalsIgnoreCase("KB")) {
											String initialFormatted = decimalFormat.format(itemDetails.getInitialAmount() / 1024.0);
											String remainingFormatted = decimalFormat.format(itemDetails.getCurrentAmount() / 1024.0);
											object.getUsage()
													.add(new UnitItem(item.getFreeUnitTypeName(), initialFormatted, remainingFormatted, "MB", itemDetails.getEffectiveTime(), itemDetails.getExpireTime(), getIconMapping(item.getFreeUnitType()), unitType, getIconSorting(item.getFreeUnitType())));
										}
										else
											object.getUsage().add(new UnitItem(item.getFreeUnitTypeName(), itemDetails.getInitialAmount() + "", itemDetails.getCurrentAmount() + "", item.getMeasureUnitName(), itemDetails.getEffectiveTime(), itemDetails.getExpireTime(),
													getIconMapping(item.getFreeUnitType()), unitType, getIconSorting(item.getFreeUnitType())));
										logger.info("Added Free Resource : " + offeringId + " : " + object.getUsage().get(0).getTotalUnits());
										freeResourcemap.get(unitType).add(object);
										// freeResourcemap.put(unitType,
										// freeResourcemap.get(unitType).add(object));
									}
								}
							} // cumulative ends here
						}
					}
					catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				}
				else
					logger.info("Resource is defined as excluded : " + item.getFreeUnitType());
			}
		}
		Date dateTemp;
		Calendar calendar = Calendar.getInstance();
		Calendar calendarConstant = Calendar.getInstance();
		for (int k = 0; k < offeringInfos.size(); k++) {
			if (!map.containsKey(offeringInfos.get(k).getOfferingId())) {
				FreeUnitItemDetails object = null;
				if (dailyOfferingId(offeringInfos.get(k).getOfferingId())) {
					object = new FreeUnitItemDetails(offeringInfos.get(k).getOfferingId(), "true", 1);
				}
				else {
					object = new FreeUnitItemDetails(offeringInfos.get(k).getOfferingId(), "false", 1);
				}

				try {
					dateTemp = dateFormat.parse(offeringInfos.get(k).getExpiredTime());
					calendar.setTime(dateTemp);
					if (calendar.get(Calendar.YEAR) >= calendarConstant.get(Calendar.YEAR) + 2) {
						calendar.set(Calendar.YEAR, calendarConstant.get(Calendar.YEAR));
					}
					if (offeringInfos.get(k).getStatus().equalsIgnoreCase("Active"))
						object.getUsage()
								.add(new UnitItem(offeringInfos.get(k).getOfferingShortName(), "", "", "", desiredDateFormat.format(dateFormat.parse(offeringInfos.get(k).getEffectiveTime())), desiredDateFormat.format(calendar.getTime()),
										// desiredDateFormat.format(dateFormat.parse(offeringInfos.get(k).getExpiredTime())),
										"", "", 0));
					else
						object.getUsage()
								.add(new UnitItem(offeringInfos.get(k).getOfferingShortName(), "", "", "", "", "",
										// desiredDateFormat.format(dateFormat.parse(offeringInfos.get(k).getExpiredTime())),
										"", "", 0));
				}
				catch (ParseException e) {
					logger.error(Helper.GetException(e));
				}
				logger.info("Added supplementary : " + offeringInfos.get(k).getOfferingId());
				map.put(offeringInfos.get(k).getOfferingId(), object);
			}
		}
		ArrayList<FreeUnitItemDetails> respon = new ArrayList<FreeUnitItemDetails>();
		Calendar calendar2 = Calendar.getInstance();
		Date date = new Date();
		Calendar calendar3 = Calendar.getInstance();
		Date date2 = new Date();
		ArrayList<UnitItem> indexToRemove = new ArrayList<>();
		for (Entry<String, FreeUnitItemDetails> entries : map.entrySet()) {
			FreeUnitItemDetails details = entries.getValue();
			for (int i = 0; i < details.getUsage().size(); i++) {
				for (int j = i + 1; j < details.getUsage().size(); j++) {
					if (details.getUsage().get(i).getExpiryDate() != null && details.getUsage().get(j).getExpiryDate() != null) {
						try {
							date = new SimpleDateFormat("yyyyMMddHHmmss").parse(details.getUsage().get(i).getExpiryDate());
							date2 = new SimpleDateFormat("yyyyMMddHHmmss").parse(details.getUsage().get(j).getExpiryDate());
							logger.info("---------------------------------------------------" + details.getUsage().get(i).getOfferName());
							logger.info("TEST: Offername : " + details.getUsage().get(i).getOfferName());
							logger.info("TEST: initial Date : " + details.getUsage().get(i).getInitialDate());
							logger.info("TEST: initial Date : " + details.getUsage().get(i).getExpiryDate());
							logger.info("---------------------------------------------------");
							logger.info("---------------------------------------------------" + details.getUsage().get(i).getOfferName());
							logger.info("TEST: Offername : " + details.getUsage().get(j).getOfferName());
							logger.info("TEST: initial Date : " + details.getUsage().get(j).getInitialDate());
							logger.info("TEST: initial Date : " + details.getUsage().get(j).getExpiryDate());
							logger.info("---------------------------------------------------");

							calendar2.setTime(date);
							calendar3.setTime(date2);
							calendar2.set(Calendar.HOUR_OF_DAY, 0);
							calendar2.set(Calendar.MINUTE, 0);
							calendar2.set(Calendar.SECOND, 0);
							calendar3.set(Calendar.HOUR_OF_DAY, 0);
							calendar3.set(Calendar.MINUTE, 0);
							calendar3.set(Calendar.SECOND, 0);
						}
						catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (new SimpleDateFormat("yyyyMMddHHmmss").format(calendar2.getTime()).equalsIgnoreCase(new SimpleDateFormat("yyyyMMddHHmmss").format(calendar3.getTime()))) {
							logger.info("------------------ Setting Expiry and initial  null---------------------------------");
							details.getUsage().get(j).setExpiryDate(null);
							details.getUsage().get(j).setInitialDate(null);
						}
						if (details.getUsage().get(i).getTotalUnits().equalsIgnoreCase("0")) {
							details.getUsage().get(j).setTotalUnits(null);
							details.getUsage().get(j).setRemainingUnits(null);
						}
						if (details.getUsage().get(i).getExpiryDate() == null && details.getUsage().get(i).getTotalUnits() == null) {
							indexToRemove.add(details.getUsage().get(j));
						}
					}
				}
			}
			for (int k = 0; k < indexToRemove.size(); k++)
				details.getUsage().remove(indexToRemove.get(k));
			respon.add(details);
			indexToRemove = new ArrayList<>();
		}
		if (!freeResourcemap.containsKey("511"))
			freeResourcemap.put("511", new ArrayList<FreeUnitItemDetails>());
		if (!freeResourcemap.containsKey("513"))
			freeResourcemap.put("513", new ArrayList<FreeUnitItemDetails>());
		if (!freeResourcemap.containsKey("514"))
			freeResourcemap.put("514", new ArrayList<FreeUnitItemDetails>());

		for (Entry<String, ArrayList<FreeUnitItemDetails>> entries : freeResourcemap.entrySet()) {
			for (int i = 0; i < entries.getValue().size(); i++)
				for (int j = 0; j < entries.getValue().get(i).getUsage().size(); j++)
					for (int k = 0; k < entries.getValue().get(i).getUsage().size(); k++)
						if (entries.getValue().get(i).getUsage().get(j).getSorting() < entries.getValue().get(i).getUsage().get(k).getSorting()) {
							UnitItem unitItem = entries.getValue().get(i).getUsage().get(j);
							entries.getValue().get(i).getUsage().set(j, respon.get(i).getUsage().get(k));
							entries.getValue().get(i).getUsage().set(k, unitItem);
						}
		}

		for (int i = 0; i < respon.size(); i++)
			for (int j = 0; j < respon.get(i).getUsage().size(); j++)
				for (int k = 0; k < respon.get(i).getUsage().size(); k++)
					if (respon.get(i).getUsage().get(j).getSorting() < respon.get(i).getUsage().get(k).getSorting()) {
						UnitItem unitItem = respon.get(i).getUsage().get(j);
						respon.get(i).getUsage().set(j, respon.get(i).getUsage().get(k));
						respon.get(i).getUsage().set(k, unitItem);
					}

		responseTemp.setFreeResources(freeResourcemap);
		// respon = makeRenewOnTop(respon);

		return respon;
	}

	// private ArrayList<FreeUnitItemDetails>
	// makeRenewOnTop(ArrayList<FreeUnitItemDetails> respon12)
	// {
	//
	//
	//
	//
	// ArrayList<FreeUnitItemDetails> respon = new
	// ArrayList<FreeUnitItemDetails>();
	// Calendar calendar2 = Calendar.getInstance();
	// Date date = new Date();
	// Calendar calendar3 = Calendar.getInstance();
	// Date date2 = new Date();
	// ArrayList<UnitItem> indexToRemove = new ArrayList<>();
	// for (Entry<String, FreeUnitItemDetails> entries : map.entrySet())
	// {
	// FreeUnitItemDetails details = entries.getValue();
	// for (int i = 0; i < details.getUsage().size(); i++)
	// {
	// for (int j = i + 1; j < details.getUsage().size(); j++)
	// {
	// if (details.getUsage().get(i).getExpiryDate() != null &&
	// details.getUsage().get(j).getExpiryDate() != null)
	// {
	// try
	// {
	// date = new SimpleDateFormat("yyyyMMddHHmmss")
	// .parse(details.getUsage().get(i).getExpiryDate());
	// date2 = new SimpleDateFormat("yyyyMMddHHmmss")
	// .parse(details.getUsage().get(j).getExpiryDate());
	// logger.info("---------------------------------------------------" +
	// details.getUsage().get(i).getOfferName());
	// logger.info("TEST: Offername : " +
	// details.getUsage().get(i).getOfferName());
	// logger.info("TEST: initial Date : "
	// +details.getUsage().get(i).getInitialDate() );
	// logger.info("TEST: initial Date : "
	// +details.getUsage().get(i).getExpiryDate() );
	// logger.info("---------------------------------------------------");
	//
	// calendar2.setTime(date);
	// calendar3.setTime(date2);
	// calendar2.set(Calendar.HOUR_OF_DAY, 0);
	// calendar2.set(Calendar.MINUTE, 0);
	// calendar2.set(Calendar.SECOND, 0);
	// calendar3.set(Calendar.HOUR_OF_DAY, 0);
	// calendar3.set(Calendar.MINUTE, 0);
	// calendar3.set(Calendar.SECOND, 0);
	// } catch (ParseException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// if (new SimpleDateFormat("yyyyMMddHHmmss").format(calendar2.getTime())
	// .equalsIgnoreCase(new
	// SimpleDateFormat("yyyyMMddHHmmss").format(calendar3.getTime())))
	// {
	// logger.info("------------------ Setting Expiry and initial
	// null---------------------------------");
	// details.getUsage().get(j).setExpiryDate(null);
	// details.getUsage().get(j).setInitialDate(null);
	// }
	// if (details.getUsage().get(i).getTotalUnits().equalsIgnoreCase("0")) {
	// details.getUsage().get(j).setTotalUnits(null);
	// details.getUsage().get(j).setRemainingUnits(null);
	// }
	// if (details.getUsage().get(i).getExpiryDate() == null
	// && details.getUsage().get(i).getTotalUnits() == null) {
	// indexToRemove.add(details.getUsage().get(j));
	// }
	// }
	// }
	// }
	// for (int k = 0; k < indexToRemove.size(); k++)
	// details.getUsage().remove(indexToRemove.get(k));
	// respon.add(details);
	// indexToRemove = new ArrayList<>();
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	// for(int i=0;i<respon.size();i++)
	// {
	// List<UnitItem> value = respon.get(i).getUsage();
	// List<UnitItem> unitItemsLinkedList = convertALtoLL(value);
	// for(int j=0;j<unitItemsLinkedList.size();j++)
	// {
	// if(unitItemsLinkedList.get(j).getExpiryDate()==null)
	// unitItemsLinkedList.get(j).
	// }
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	// ArrayList<FreeUnitItemDetails> freeUnitItemDetailsReponse = new
	// ArrayList<FreeUnitItemDetails>();
	// ArrayList<UnitItem> usageDetailsResponse = new ArrayList<UnitItem>();
	// for(int i=0;i<respon.size();i++)
	// {
	// FreeUnitItemDetails freeUnitItemDetails = new
	// FreeUnitItemDetails(respon.get(i).getOfferingId(),respon.get(i).getIsDaily());
	// ArrayList<UnitItem> usageDetails = new ArrayList<UnitItem>();
	// usageDetails=respon.get(i).getUsage();
	// logger.info("usageDetail Size:"+usageDetails.size());
	// for(int j=0;j<usageDetails.size();j++)
	// {
	// logger.info("usageDetail->j Size:"+j);
	// if(usageDetails!=null && usageDetails.get(j).getInitialDate()!=null &&
	// !usageDetails.get(j).getExpiryDate().equals(""))
	// {
	//
	// UnitItem unitItem = new UnitItem(usageDetails.get(j).getOfferName()
	// , usageDetails.get(j).getTotalUnits(),
	// usageDetails.get(j).getRemainingUnits(),
	// usageDetails.get(j).getUnitName(),
	// usageDetails.get(j).getInitialDate(),
	// usageDetails.get(j).getExpiryDate(),
	// usageDetails.get(j).getIcon(),
	// usageDetails.get(j).getType(),
	// usageDetails.get(j).getSorting()
	// );
	// usageDetailsResponse.add(unitItem);
	// }
	// }
	//
	// freeUnitItemDetails.setUsage(usageDetailsResponse);
	// freeUnitItemDetailsReponse.add(freeUnitItemDetails);
	// }
	//
	// return freeUnitItemDetailsReponse;
	//
	//
	//
	//
	//
	//
	//
	//// for(int i=0;i<respon.size();i++)
	//// {
	//// ArrayList<UnitItem> usageDetails = respon.get(i).getUsage();
	//// for(int j=0;j<usageDetails.size();j++)
	//// if(usageDetails.get(j).getInitialDate()==null ||
	// usageDetails.get(j).getInitialDate().equals("") )
	//// {
	//// usageDetailsResponse.add(usageDetails.get(j));
	//// }
	////
	////
	//// }
	//
	//
	//
	//
	//
	//// ArrayList<UnitItem> usage = new ArrayList<>();
	//// ArrayList<FreeUnitItemDetails> freeUnitItemDetails = new
	// ArrayList<FreeUnitItemDetails>();
	//// FreeUnitItemDetails value = new FreeUnitItemDetails(offeringId);
	//// freeUnitItemDetails.add(value )
	////
	////
	//// for(int i=0;i<respon.size();i++)
	//// {
	//// for (int j = 0; j < respon.get(i).getUsage().size(); j++)
	//// {
	//// if(respon.get(i).getUsage().get(j).getExpiryDate()!=null)
	//// {
	//// UnitItem unitItem = respon.get(i).getUsage().get(j);
	//// usage.add(unitItem);
	////
	//// }
	//// }
	//// }
	//// for(int i=0;i<respon.size();i++)
	//// {
	//// for (int j = 0; j < respon.get(i).getUsage().size(); j++)
	//// {
	//// if(respon.get(i).getUsage().get(j).getExpiryDate()==null)
	//// {
	//// UnitItem unitItem = respon.get(i).getUsage().get(j);
	//// usage.add(unitItem);
	////
	//// }
	//// }
	//// }
	//
	////
	////
	//// return respon;
	// }
	// }

	public String getIconMapping(String key) {
		String value = ConfigurationManager.getConfigurationFromCache("mysub.icon.sorting." + key);
		if (!value.isEmpty())
			return value.split(",")[0];
		return "";
	}

	public int getIconSorting(String key) {
		String value = ConfigurationManager.getConfigurationFromCache("mysub.icon.sorting." + key);
		if (!value.isEmpty())
			return Integer.parseInt(value.split(",")[1]);
		return 0;
	}

	public boolean checkIfPTS(String key) {
		String[] ptsOffering = ConfigurationManager.getConfigurationFromCache("pts.offering").split(",");
		for (int i = 0; i < ptsOffering.length; i++)
			if (ptsOffering[i].equalsIgnoreCase(key)) {
				logger.info("PTS detected on " + key);
				return true;
			}
		logger.info("PTS not detected on " + key);
		return false;
	}

	public static boolean checkIfFreeResource(String key) {
		String type = ConfigurationManager.getConfigurationFromCache(key);
		if (type != null && type.equalsIgnoreCase("FREE"))
			return true;
		return false;
	}

	public static boolean checkIfResourceTypeExcluded(String key) {
		String type = ConfigurationManager.getConfigurationFromCache("mysub.excluded.resource.type");
		if (type != null && type.contains(key))
			return true;
		return false;
	}

	public boolean dailyOfferingId(String offeringId) {
		List<String> dailyOfferingIdsFromConfig = Arrays.asList((ConfigurationManager.getConfigurationFromCache("mySubscriptions.daily.offers")).split(","));
		if (dailyOfferingIdsFromConfig.contains(offeringId)) {
			return true;
		}
		else {
			return false;
		}
	}

	public static void main(String[] args) {
		String ids = "123,456,234";
		String check = "4567";
		List<String> items = Arrays.asList(ids.split(","));
		if (items.contains(check)) {
			System.out.println("YES FOUND");
		}
		else {
			System.out.println("SORY");
		}
		String msisdn = "\r\nAustralia";

		if (msisdn.contains("\r\n")) {
			System.out.println("YES === CONTAINS");
			msisdn.replaceAll("\r\n", "");
			System.out.println("REUSLT : " + msisdn);
		}
	}

	public List<UnitItem> convertALtoLL(List<UnitItem> aL) {

		// Create the LinkedList by passing the ArrayList
		// as parameter in the constructor
		List<UnitItem> lL = new LinkedList<UnitItem>(aL);

		// Return the converted LinkedList
		return lL;
	}
}
