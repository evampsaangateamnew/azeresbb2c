package com.evampsaanga.azerfon.changepaymentrelation;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class ChangePaymentRelationResponse extends BaseResponse {
	
	private String minimumLimit="";
	private String maximumLimi="";
	
	
	public String getMinimumLimit() {
		return minimumLimit;
	}
	public void setMinimumLimit(String minimumLimit) {
		this.minimumLimit = minimumLimit;
	}
	public String getMaximumLimi() {
		return maximumLimi;
	}
	public void setMaximumLimi(String maximumLimi) {
		this.maximumLimi = maximumLimi;
	}

}
