package com.evampsaanga.azerfon.signupsendotp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.evampsaanga.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SignUpResponse")
public class SignUpResponse extends BaseResponse {
	private String pinMsg = "";

	/**
	 * @return the pinMsg
	 */
	public String getPinMsg() {
		return pinMsg;
	}

	/**
	 * @param pinMsg
	 *            the pinMsg to set
	 */
	public void setPinMsg(String pinMsg) {
		this.pinMsg = pinMsg;
	}
}
