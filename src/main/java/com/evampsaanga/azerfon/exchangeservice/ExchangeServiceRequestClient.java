package com.evampsaanga.azerfon.exchangeservice;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class ExchangeServiceRequestClient extends BaseRequest {
	
	private String isDataToVoiceConversion;
	private String dataValue;
	private String voiceValue;
	private String offeringId;
	private String supplementaryOfferingIds;
	
	public String getIsDataToVoiceConversion() {
		return isDataToVoiceConversion;
	}
	public void setIsDataToVoiceConversion(String isDataToVoiceConversion) {
		this.isDataToVoiceConversion = isDataToVoiceConversion;
	}
	public String getDataValue() {
		return dataValue;
	}
	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}
	public String getVoiceValue() {
		return voiceValue;
	}
	public void setVoiceValue(String voiceValue) {
		this.voiceValue = voiceValue;
	}
	public String getOfferingId() {
		return offeringId;
	}
	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}
	public String getSupplementaryOfferingIds() {
		return supplementaryOfferingIds;
	}
	public void setSupplementaryOfferingIds(String supplementaryOfferingIds) {
		this.supplementaryOfferingIds = supplementaryOfferingIds;
	}

	

}
