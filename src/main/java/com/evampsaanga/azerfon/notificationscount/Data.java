package com.evampsaanga.azerfon.notificationscount;

public class Data {
	private String count = "";
	private String notificationstatus = "";

	public String getNotificationstatus() {
		return notificationstatus;
	}

	public void setNotificationstatus(String notificationstatus) {
		this.notificationstatus = notificationstatus;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}
}
