package com.evampsaanga.azerfon.callforwarding;
/**
 * Request Data Container For Call Forward
 * @author Aqeel Abbas
 *
 */
public class CallForwardRequestDataV2 {
	private String msisdn;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
}
