package com.evampsaanga.azerfon.callforwarding;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;
/**
 * Process Core Services Request Container For Phase 2
 * @author Aqeel Abbas
 *
 */
public class ProcessCoreServicesRequestV2 extends BaseRequest {
	private String offeringId;
	private String actionType;
	private String id;
	private String number;
	
	
	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	

}
