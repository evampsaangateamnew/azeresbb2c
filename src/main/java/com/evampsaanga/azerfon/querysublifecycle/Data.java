package com.evampsaanga.azerfon.querysublifecycle;

import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleResult;

public class Data {
	QuerySubLifeCycleResult result = new QuerySubLifeCycleResult();

	public QuerySubLifeCycleResult getResult() {
		return result;
	}

	public void setResult(QuerySubLifeCycleResult result) {
		this.result = result;
	}
}
