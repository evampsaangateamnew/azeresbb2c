package com.evampsaanga.validator.rules;

public class IPNotEmpty implements ValidationRules {

	@Override
	public ValidationResult validateObject(Object object) {
		if(object == null || object.toString().isEmpty() || object.toString().length() == 0)
			return new ValidationResult(false,ValidationCode.IP_EMPTY_VALIDATION_CODE);
		return new ValidationResult(true,ValidationCode.SUCCESS_VALIDATION_CODE);	
	}

}
