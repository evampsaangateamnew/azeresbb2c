package com.evampsaanga.cache;

/**
 * @author Aqeel Abbas
 * Pic user data and group data caching 
 */
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.evampsaanga.azerfon.db.DBBakcellFactory;
import com.evampsaanga.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.azerfon.getUsersV2.UsersGroupRequest;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.developer.utils.Helper;
import com.sun.istack.logging.Logger;

public class UserGroupDataCache {
	public static HashMap<String, List<UsersGroupData>> users = new HashMap<>();
	public static HashMap<String, String> usersCount = new HashMap<>();

	public static final Logger logger = Logger.getLogger("azerfone-esb", UserGroupDataCache.class);

	public List<UsersGroupData> checkUsersInCache(String request) throws IOException, Exception {
		// printdb();
		UsersGroupRequest usersGroupRequest = Helper.JsonToObject(request, UsersGroupRequest.class);
		String customerID = usersGroupRequest.getCustomerID();
		if (users.containsKey(customerID + "." + usersGroupRequest.getLang())) {
			Helper.logInfoMessageV2("User with msisdn " + customerID + " is found in Cache");
			return users.get(customerID + "." + usersGroupRequest.getLang());
		} else {
			Helper.logInfoMessageV2("Populating Cache with CustomerID: " + customerID);
			List<UsersGroupData> usersGroupData = getUsersGroupDataDB(customerID, usersGroupRequest.getLang());
			users.put(customerID + "." + usersGroupRequest.getLang(), usersGroupData);
			return usersGroupData;
		}

	}

	private List<UsersGroupData> getUsersGroupDataDB(String customerID, String lang) {
		List<UsersGroupData> usersData = new ArrayList<UsersGroupData>();
		// MagentoServices services = new MagentoServices();
		// printing DB Data
		// services.printDB();

		java.util.Date date = new java.util.Date();
		try {
			String sqlQuery = "select PRI_IDENTITY,PROFILE_ID,BRAND_ID,CORP_ID,GROUP_SUBS_ID,GROUP_CUST_NAME,GROUP_ID,"
					+ "CORP_CRM_CUST_ID,PIC_NAME,CORP_ACCT_CODE,CORP_CRM_ACCT_ID,CUST_FST_NAME,CUST_LAST_NAME from V_E_CARE_CORP_INFO where CORP_CRM_CUST_ID = "
					+ customerID + "order by PRI_IDENTITY";
			Helper.logInfoMessageV2(date + " --   SQL Query @@GetusersGroupData is :" + sqlQuery);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(sqlQuery).executeQuery();
			/*
			 * ResultSetMetaData metadata = rs.getMetaData(); int columnCount =
			 * metadata.getColumnCount(); for (int i = 1; i <= columnCount; i++)
			 * { Helper.logInfoMessage(metadata.getColumnName(i) + ", "); }
			 */
			String row = "";
			while (rs.next()) {
				Helper.logInfoMessageV2(" SQL Query Result :" + row);
				UsersGroupData users = new UsersGroupData();
				users.setMsisdn(rs.getString("PRI_IDENTITY"));
				users.setTarif_id(rs.getString("PROFILE_ID"));
				users.setBrand_id(rs.getString("BRAND_ID"));
				users.setBrand_name(ConfigurationManager
						.getConfigurationFromCache(ConfigurationManager.MAPPING_HLR_BRANDID + users.getBrand_id()));
				users.setCorp_id(rs.getString("CORP_ID"));
				users.setGroup_code(rs.getString("GROUP_SUBS_ID"));
				/*users.setGroup_name(ConfigurationManager
						.getConfigurationFromCache(ConfigurationManager.MAPPING_HLR_GROUPID + users.getGroup_code()));*/
				users.setGroup_name(ConfigurationManager
						.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPID + "3." + rs.getString("GROUP_ID")));
				users.setGroup_cust_name(users.getGroup_name());
				users.setGroup_id( rs.getString("GROUP_ID"));
				users.setCorp_crm_cust_name(rs.getString("CORP_CRM_CUST_ID"));
				users.setPic_name(rs.getString("PIC_NAME"));
				users.setCorp_acct_code(rs.getString("CORP_ACCT_CODE"));
				users.setCorp_crm_acct_id(rs.getString("CORP_CRM_ACCT_ID"));
				users.setCust_fst_name(rs.getString("CUST_FST_NAME"));
				users.setCust_last_name(rs.getString("CUST_LAST_NAME"));
				String disp_name = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPIDTRANS + lang + "." + rs.getString("GROUP_ID"));
				logger.info("Disp Name" + disp_name);
				users.setGroup_name_disp(disp_name);
				// logger.info("@@@@@@@@@@@@@@@@ users data :" + users);
				usersData.add(users);
			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.info("SQLException", e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Exception", e);
		}
		return usersData;
	}

	public String countUsers(String customerID) {

		if (usersCount.containsKey(customerID)) {
			Helper.logInfoMessageV2("User with msisdn" + customerID + " is found in Cache");
			return usersCount.get(customerID);
		} else {
			Helper.logInfoMessageV2("Populating Cache with CustomerID: " + customerID);
			String userCount = getUsersCount(customerID);
			usersCount.put(customerID, userCount);
			return userCount;
		}

	}

	private String getUsersCount(String customerID) {
		String userCount = "0";
		String query = "select count(*) from V_E_CARE_CORP_INFO where CORP_CRM_CUST_ID = " + customerID;
		try {
			Helper.logInfoMessageV2("Query to count users: " + query);
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			while (rs.next()) {
				userCount = rs.getString(1);
			}
		} catch (SQLException e) {
			logger.info("ERROR: ", e);
		}
		Helper.logInfoMessageV2("User Count is: " + userCount);
		return userCount;
	}

	public void printdb() {
		String query = "select distinct(GROUP_CODE) from V_E_CARE_CORP_INFO ";
		try {
			ResultSet rs = DBBakcellFactory.getConnection().prepareStatement(query).executeQuery();
			ResultSetMetaData metadata = rs.getMetaData();
			int columnCount = metadata.getColumnCount();
			Helper.logInfoMessageV2("<<<<<<<<< E&S Group Data >>>>>>>>>");
			for (int i = 1; i <= columnCount; i++) {
				Helper.logInfoMessage("E&S-- " + metadata.getColumnName(i) + ", ");
			}
			// String row = "";
			for (int i = 0; i < rs.getFetchSize(); i++) {
				Helper.logInfoMessageV2("E&S-- " + rs.getString(i));
			}
			/*
			 * while (rs.next() || rs.getFetchSize()) { System.out.println(row);
			 * System.out.print(rs.get); }
			 */

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.info("ERROR: ", e);
		}

	}

}
