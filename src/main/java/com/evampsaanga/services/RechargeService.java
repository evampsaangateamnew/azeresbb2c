package com.evampsaanga.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bss.soaif.chargesubscriber._interface.common.ReqHeader;
import com.huawei.bss.soaif.chargesubscriber._interface.rechargeservice.RechargeInterfaces;
import com.huawei.bss.soaif.chargesubscriber._interface.rechargeservice.RechargeServices;



public class RechargeService {
	private static RechargeInterfaces portType = null;

	private RechargeService() {
	}

	public static synchronized RechargeInterfaces getInstance() {
		if (portType == null) 
		{
			portType = new RechargeServices().getRechargeServicePort();
			SoapHandlerService.configureBinding(portType);
		}
		return portType;
	}

	public static ReqHeader getRechargeRequestHeader() {
		ReqHeader reqH = new ReqHeader();
		reqH.setVersion("1");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).toString());
		reqH.setChannel("NarPlus");
		reqH.setReqTime(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).toString());
		reqH.setAccessPassword("PlusNar12#$");//NarPlus12#$ ->Staging (PlusNar12#$->PROD)
		reqH.setAccessUser("NarPlus");

		return reqH;
	}

}
