package com.evampsaanga.services;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGateway;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGatewayInterfaces;

public class USSDService {
	private static USSDGatewayInterfaces portType = null;

	private USSDService() {
	}

	public static synchronized USSDGatewayInterfaces getInstance() {
		if (portType == null) {
			portType = new USSDGateway().getUSSDGatewayPort();
			SoapHandlerService.configureBinding(portType);
		}
		return portType;
	}
}
