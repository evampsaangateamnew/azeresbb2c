package com.evampsaanga.services;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bss.soaif._interface.mnpservice.MNPPortType;
import com.huawei.bss.soaif._interface.mnpservice.MNPServices;

public class MNPPortService {
	private static MNPPortType PortType = null;

	private MNPPortService() {
	}

	public static synchronized MNPPortType getInstance() {
		if (PortType == null) {
			PortType = new MNPServices().getMNPServicePort();
			SoapHandlerService.configureBinding(PortType);
		}
		return PortType;
	}
}