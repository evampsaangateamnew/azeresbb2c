package com.evampsaanga.services;

import java.net.MalformedURLException;
import java.net.URL;
import com.evampsaanga.configs.Config;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bme.cbsinterface.bbservices.BbServices;
import com.huawei.bme.cbsinterface.bbservices.BbServices_Service;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;

public class CBSBBService {
	private static BbServices CBSARPortType = null;

	private CBSBBService() {
	}

	public static synchronized BbServices getInstance() throws MalformedURLException {
		if (CBSARPortType == null) {
			CBSARPortType = new BbServices_Service(new URL(Config.CBS_BB_WSDL_PATH)).getBbServicesPort();
			SoapHandlerService.configureBinding(CBSARPortType);
		}
		return CBSARPortType;
	}
	public static synchronized BbServices getInstance(String token) throws MalformedURLException {
		if (CBSARPortType == null) {
			CBSARPortType = new BbServices_Service(new URL(Config.CBS_BB_WSDL_PATH)).getBbServicesPort();
			SoapHandlerService.configureBinding(CBSARPortType);
		}
		return CBSARPortType;
	}

	public static RequestHeader getRequestHeader() {
		RequestHeader reqH = new RequestHeader();
		reqH.setVersion(Constants.QF_VERSION);
		OwnershipInfo ownerShitInfo = new OwnershipInfo();
		ownerShitInfo.setBEID(Constants.QF_BEID);
		ownerShitInfo.setBRID(Constants.QF_BRID);
		reqH.setOwnershipInfo(ownerShitInfo);
		reqH.setMessageSeq(Helper.generateTransactionID());
		SecurityInfo accessSecurityInfo = new SecurityInfo();
		accessSecurityInfo.setLoginSystemCode(ConfigurationManager.getConfigurationFromCache("cbsbb.username"));
		accessSecurityInfo.setPassword(ConfigurationManager.getConfigurationFromCache("cbsbb.password"));
		reqH.setAccessSecurity(accessSecurityInfo);
		return reqH;
	}
}
