package com.evampsaanga.services;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.bme.cbsinterface.arservices.ArServices;
import com.huawei.bme.cbsinterface.arservices.ArServices_Service;

public class BMEService {
	private static ArServices portType = null;

	private BMEService() {
	}

	public static synchronized ArServices getInstance() {
		if (portType == null) {
			portType = new ArServices_Service().getArServicesPort();
			SoapHandlerService.configureBinding(portType);
		}
		return portType;
	}
}
