package com.evampsaanga.services;

import javax.xml.ws.BindingProvider;

import com.evampsaanga.azerfon.plasticcard.topupclient.RepeatPaymentWebService;
import com.evampsaanga.azerfon.plasticcard.topupclient.RepeatPaymentWebServiceImpService;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.developer.utils.SoapHandlerService;

public class GoldenPayService {
	private static RepeatPaymentWebService portTypeGoldenPay = null;

	private GoldenPayService() {
	}

	public static synchronized RepeatPaymentWebService getInstance() {
		if (portTypeGoldenPay == null) {
			portTypeGoldenPay = new RepeatPaymentWebServiceImpService().getRepeatPaymentWebServiceImpPort();
			SoapHandlerService.configureBinding(portTypeGoldenPay);

			BindingProvider prov = (BindingProvider) portTypeGoldenPay;
			prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY,
					ConfigurationManager.getConfigurationFromCache("golden.pay.username").trim());
			prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY,
					ConfigurationManager.getConfigurationFromCache("golden.pay.password").trim());
		}
		return portTypeGoldenPay;
	}
}