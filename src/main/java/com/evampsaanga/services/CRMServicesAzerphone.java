package com.evampsaanga.services;

import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.crm.azerfon.bss.service.HuaweiCRMPortType;
import com.huawei.crm.azerfon.bss.service.OrderQueryAZCtz;


public class CRMServicesAzerphone {
	private static HuaweiCRMPortType crmPortType = null;
	
	private CRMServicesAzerphone() {
		
	}
	
	public static synchronized HuaweiCRMPortType getInstance() {
		if (crmPortType == null) {
			crmPortType = new OrderQueryAZCtz().getHuaweiCRMPort();
			SoapHandlerService.configureBinding(crmPortType);
		}
		return crmPortType;
	}

}
