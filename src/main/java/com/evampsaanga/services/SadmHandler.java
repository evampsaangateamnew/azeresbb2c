package com.evampsaanga.services;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.developer.utils.Helper;

public class SadmHandler implements SOAPHandler<SOAPMessageContext> {
	private static String username = "";
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	private String token;
	private static String credentialpass = "";
	static {
		username = ConfigurationManager.getDBProperties("sadm.username");
		credentialpass = ConfigurationManager.getDBProperties("sadm.password");
	}
	public SadmHandler()
	{
		
	}
	public SadmHandler(String token)
	{
		this.token= token;
	}
	private void logToSystemOut(SOAPMessageContext smc) {
		Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (outboundProperty.booleanValue()) {
			logger.info(new SimpleDateFormat("yyyy-MM-dd HH:mm:sss").format(new Date()) + "\nOutbound message:");
		} else {
			logger.info(new SimpleDateFormat("yyyy-MM-dd HH:mm:sss").format(new Date()) + "\nInbound message:");
		}
		SOAPMessage message = smc.getMessage();
		try {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			message.writeTo(stream);
			String msg = new String(stream.toByteArray(), "utf-8");
			logger.info(toPrettyString(msg));
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
	}

	public String toPrettyString(String xml) {
		try {
			final InputSource src = new InputSource(new StringReader(xml));
			final Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src)
					.getDocumentElement();
			final Boolean keepDeclaration = Boolean.valueOf(xml.startsWith("<?xml"));
			final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
			final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
			final LSSerializer writer = impl.createLSSerializer();
			writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
			writer.getDomConfig().setParameter("xml-declaration", keepDeclaration);
			return writer.writeToString(document);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		try {
			SOAPMessage message = context.getMessage();
			SOAPHeader header = message.getSOAPHeader();
			SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
			if (header == null) {
				header = envelope.addHeader();
			}
			QName qNameUsername = new QName("com", "UserName");
			SOAPHeaderElement username = header.addHeaderElement(qNameUsername);
			username.addTextNode(SadmHandler.username);
			QName qNamePassword = new QName("com", "Password");
			SOAPHeaderElement password = header.addHeaderElement(qNamePassword);
			password.addTextNode(SadmHandler.credentialpass);
			header.addChildElement(username);
			header.addChildElement(password);
			message.saveChanges();
		} catch (SOAPException e) {
			logger.error("Error occurred while adding credentials to SOAP header." + Helper.GetException(e));
		}
		logToSystemOut(context);
		return true;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		logger.info("handleFault has been invoked.");
		return true;
	}

	@Override
	public void close(MessageContext context) {
		logger.info("close has been invoked.");
	}

	@Override
	public Set<QName> getHeaders() {
		logger.info("getHeaders has been invoked.");
		return null;
	}
}