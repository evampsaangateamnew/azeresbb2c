package com.evampsaanga.services;

import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.evampsaanga.developer.utils.Helper;
import com.evampsaanga.developer.utils.SoapHandlerService;
import com.huawei.crm.service.ens.HuaweiCRMPortType;
import com.huawei.crm.service.ens.OrderHandle;

public class OrderHandleService {
	private static com.huawei.crm.service.ens.HuaweiCRMPortType crmPortType = null;

	private OrderHandleService() {
	}

	public static synchronized HuaweiCRMPortType getInstance() {
		if (crmPortType == null) {
			crmPortType = new OrderHandle().getHuaweiCRMPort();
			SoapHandlerService.configureBinding(crmPortType);
		}
		return crmPortType;
	}

	public static com.huawei.crm.basetype.ens.RequestHeader getReqHeaderForCallForwarding() {
		com.huawei.crm.basetype.ens.RequestHeader reqH = new com.huawei.crm.basetype.ens.RequestHeader();
		try {
			reqH.setChannelId(ConfigurationManager.getConfigurationFromCache("order.handle.channelid"));
			reqH.setTechnicalChannelId(Constants.TECHNICAL_CHANEL_ID);
			reqH.setAccessUser(ConfigurationManager.getConfigurationFromCache("order.handle.user"));
			reqH.setAccessPwd(ConfigurationManager.getConfigurationFromCache("order.handle.password"));
			reqH.setTransactionId(Helper.generateTransactionID());
			reqH.setTenantId(Constants.TENANT_ID);
		} catch (Exception e) {
			e.getStackTrace();
		}
		return reqH;
	}

	public static com.huawei.crm.basetype.ens.RequestHeader getReqHeaderForChangeLanguage() {
		com.huawei.crm.basetype.ens.RequestHeader reqH = new com.huawei.crm.basetype.ens.RequestHeader();
		reqH.setVersion(Constants.VERSION);
		reqH.setChannelId(ConfigurationManager.getConfigurationFromCache("order.handle.channelid"));
		reqH.setTechnicalChannelId(Constants.TECHNICAL_CHANEL_ID);
		reqH.setAccessUser(ConfigurationManager.getConfigurationFromCache("order.handle.user"));
		reqH.setAccessPwd(ConfigurationManager.getConfigurationFromCache("order.handle.password"));
		reqH.setTransactionId(Helper.generateTransactionID());
		reqH.setTenantId(Constants.TENANT_ID);
		return reqH;
	}

	public static com.huawei.crm.basetype.RequestHeader getReqHeaderForChangeSupplementary() {
		com.huawei.crm.basetype.RequestHeader reqH = new com.huawei.crm.basetype.RequestHeader();
		reqH.setVersion(Constants.VERSION);
		reqH.setChannelId(ConfigurationManager.getConfigurationFromCache("order.handle.channelid"));
		reqH.setTechnicalChannelId(Constants.TECHNICAL_CHANEL_ID);
		reqH.setAccessUser(ConfigurationManager.getConfigurationFromCache("order.handle.user"));
		reqH.setAccessPwd(ConfigurationManager.getConfigurationFromCache("order.handle.password"));
		reqH.setTransactionId(Helper.generateTransactionID());
		reqH.setTenantId(Constants.TENANT_ID);
		return reqH;
	}

	public static com.huawei.crm.basetype.ens.RequestHeader getReqHeaderForChangeTariff() {
		com.huawei.crm.basetype.ens.RequestHeader reqH = new com.huawei.crm.basetype.ens.RequestHeader();
		reqH.setVersion(Constants.VERSION);
		reqH.setChannelId(ConfigurationManager.getConfigurationFromCache("order.handle.channelid"));
		reqH.setTechnicalChannelId(Constants.TECHNICAL_CHANEL_ID);
		reqH.setAccessUser(ConfigurationManager.getConfigurationFromCache("order.handle.user"));
		reqH.setAccessPwd(ConfigurationManager.getConfigurationFromCache("order.handle.password"));
		reqH.setTransactionId(Helper.generateTransactionID());
		reqH.setTenantId(Constants.TENANT_ID);
		return reqH;
	}

	public static com.huawei.crm.basetype.ens.RequestHeader getReqHeaderForManipulateFnF() {
		com.huawei.crm.basetype.ens.RequestHeader reqH = new com.huawei.crm.basetype.ens.RequestHeader();
		reqH.setVersion(Constants.VERSION);
		reqH.setChannelId(ConfigurationManager.getConfigurationFromCache("order.handle.channelid"));
		reqH.setTechnicalChannelId(Constants.TECHNICAL_CHANEL_ID);
		reqH.setAccessUser(ConfigurationManager.getConfigurationFromCache("order.handle.user"));
		reqH.setAccessPwd(ConfigurationManager.getConfigurationFromCache("order.handle.password"));
		reqH.setTransactionId(Helper.generateTransactionID());
		reqH.setTenantId(Constants.TENANT_ID);
		return reqH;
	}

	public static com.huawei.crm.basetype.ens.RequestHeader getReqHeaderForReportLost() {
		com.huawei.crm.basetype.ens.RequestHeader reqH = new com.huawei.crm.basetype.ens.RequestHeader();
		reqH.setVersion(Constants.VERSION);
		reqH.setChannelId(ConfigurationManager.getConfigurationFromCache("order.handle.channelid"));
		reqH.setTechnicalChannelId(Constants.TECHNICAL_CHANEL_ID);
		reqH.setAccessUser(ConfigurationManager.getConfigurationFromCache("order.handle.user"));
		reqH.setAccessPwd(ConfigurationManager.getConfigurationFromCache("order.handle.password"));
		reqH.setTransactionId(Helper.generateTransactionID());
		reqH.setTenantId(Constants.TENANT_ID);
		return reqH;
	}

	// TODO
	public static com.huawei.crm.basetype.ens.RequestHeader getReqHeaderForChangeNetworkSetting() {
		com.huawei.crm.basetype.ens.RequestHeader reqH = new com.huawei.crm.basetype.ens.RequestHeader();
		reqH.setVersion(Constants.VERSION);
		reqH.setChannelId(ConfigurationManager.getConfigurationFromCache("order.handle.channelid"));
		reqH.setTechnicalChannelId(Constants.TECHNICAL_CHANEL_ID);
		reqH.setAccessUser(ConfigurationManager.getConfigurationFromCache("order.handle.user"));
		reqH.setAccessPwd(ConfigurationManager.getConfigurationFromCache("order.handle.password"));
		reqH.setTransactionId(Helper.generateTransactionID());
		return reqH;
	}
}
