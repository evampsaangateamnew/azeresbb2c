package com.evampsaanga.magentoservices;

import java.util.List;

import com.evampsaanga.azerfon.magento.tariffdetails.Data;
import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class TariffGroupsResponse extends BaseResponse{
	 public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	private List<Data> data = null;

}
