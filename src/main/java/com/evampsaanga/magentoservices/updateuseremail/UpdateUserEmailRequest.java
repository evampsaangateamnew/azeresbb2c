package com.evampsaanga.magentoservices.updateuseremail;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class UpdateUserEmailRequest extends BaseRequest {
	private String email = "";

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
}
