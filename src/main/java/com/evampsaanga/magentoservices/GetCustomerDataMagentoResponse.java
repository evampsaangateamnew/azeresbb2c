package com.evampsaanga.magentoservices;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetCustomerDataMagentoResponse extends BaseResponse {
	public GetCustomerDataMagentoResponse() {
		super();
	}

	com.evampsaanga.magentoservices.CustomerDataMagento customerData = new com.evampsaanga.magentoservices.CustomerDataMagento();

	/**
	 * @return the customerData
	 */
	public com.evampsaanga.magentoservices.CustomerDataMagento getCustomerData() {
		return customerData;
	}

	/**
	 * @param customerData
	 *            the customerData to set
	 */
	public void setCustomerData(com.evampsaanga.magentoservices.CustomerDataMagento customerData) {
		this.customerData = customerData;
	}
}
