package com.evampsaanga.magentoservices.getcdrsbydateotp;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;

public class GetCDRsByDateOTPRequest extends BaseRequest {
	private String accountId = "";
	private String customerId = "";
	private String passportNumber = "";

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
}
