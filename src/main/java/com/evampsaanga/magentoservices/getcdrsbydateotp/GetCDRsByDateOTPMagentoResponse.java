package com.evampsaanga.magentoservices.getcdrsbydateotp;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "resultCode", "msg", "pin", "data", "execTime" })
public class GetCDRsByDateOTPMagentoResponse {
	@JsonProperty("resultCode")
	private String resultCode;
	@JsonProperty("msg")
	private String msg;
	@JsonProperty("pin")
	private Integer pin;
	@JsonProperty("data")
	private Object data;
	@JsonProperty("execTime")
	private Double execTime;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("resultCode")
	public String getResultCode() {
		return resultCode;
	}

	@JsonProperty("resultCode")
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	@JsonProperty("msg")
	public String getMsg() {
		return msg;
	}

	@JsonProperty("msg")
	public void setMsg(String msg) {
		this.msg = msg;
	}

	@JsonProperty("pin")
	public Integer getPin() {
		return pin;
	}

	@JsonProperty("pin")
	public void setPin(Integer pin) {
		this.pin = pin;
	}

	@JsonProperty("data")
	public Object getData() {
		return data;
	}

	@JsonProperty("data")
	public void setData(Object data) {
		this.data = data;
	}

	@JsonProperty("execTime")
	public Double getExecTime() {
		return execTime;
	}

	@JsonProperty("execTime")
	public void setExecTime(Double execTime) {
		this.execTime = execTime;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
