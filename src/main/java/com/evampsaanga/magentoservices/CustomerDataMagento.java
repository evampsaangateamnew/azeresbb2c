package com.evampsaanga.magentoservices;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import com.evampsaanga.configs.ConfigurationManager;
import com.huawei.bss.soaif._interface.hlrwebservice.GetSubscriberResponse;
import com.huawei.bss.soaif._interface.hlrwebservice.OfferingInfo;
import com.huawei.crm.query.GetCustomerResponse;

public class CustomerDataMagento {
	private String title;
	private String brandName = "";
	private String firstName = "";
	private String middleName = "";
	private String lastName = "";
	private String customerType = "";
	private String gender = "";
	private String dateOfBirth = "";
	protected Long accountId = 0L;
	protected String language = "";
	protected String effectiveDate = "";
	protected String expireDate = "";
	protected String subscriberType = "";
	protected String status = "";
	protected String statusDetail = "";
	protected String brandId = "";
	protected String loyaltySegment = "";
	protected String offering_id = "";
	protected List<com.evampsaanga.azerfon.getcustomerrequest.OfferingInfo> supplementaryOfferingList = new ArrayList<com.evampsaanga.azerfon.getcustomerrequest.OfferingInfo>();
	protected String msisdn;

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName
	 *            the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the supplementaryOfferingList
	 */
	public List<com.evampsaanga.azerfon.getcustomerrequest.OfferingInfo> getSupplementaryOfferingList() {
		return supplementaryOfferingList;
	}

	/**
	 * @param supplementaryOfferingList
	 *            the supplementaryOfferingList to set
	 */
	public void setSupplementaryOfferingList(
			List<com.evampsaanga.azerfon.getcustomerrequest.OfferingInfo> supplementaryOfferingList) {
		this.supplementaryOfferingList = supplementaryOfferingList;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn
	 *            the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the offeringName
	 */
	public String getOfferingName() {
		return offeringName;
	}

	/**
	 * @param offeringName
	 *            the offeringName to set
	 */
	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	@XmlElement(name = "PrimaryOffering")
	protected OfferingInfo primaryOffering;

	/**
	 * @return the primaryOffering
	 */
	public OfferingInfo getPrimaryOffering() {
		return primaryOffering;
	}

	/**
	 * @param primaryOffering
	 *            the primaryOffering to set
	 */
	public void setPrimaryOffering(OfferingInfo primaryOffering) {
		this.primaryOffering = primaryOffering;
	}

	@XmlElement(name = "offeringName")
	protected String offeringName = "";

	public void setResponseBody(GetCustomerResponse responseBody, GetSubscriberResponse hrlSub) {
		if (responseBody.getGetCustomerBody() != null) {
			// tobeSent
			if (responseBody.getGetCustomerBody().getTitle() != null) {
				this.title = ConfigurationManager.getConfigurationFromCache(
						ConfigurationManager.MAPPING_CRM_TITLE + responseBody.getGetCustomerBody().getTitle());
			}
			if (hrlSub.getGetSubscriberBody().getPrimaryOffering() != null)
				this.primaryOffering = hrlSub.getGetSubscriberBody().getPrimaryOffering();
			if (hrlSub.getGetSubscriberBody().getServiceNumber() != null)
				msisdn = hrlSub.getGetSubscriberBody().getServiceNumber();
			if (responseBody.getGetCustomerBody().getFirstName() != null)
				this.firstName = responseBody.getGetCustomerBody().getFirstName();
			else
				this.firstName = "";
			if (responseBody.getGetCustomerBody().getMiddleName() != null)
				this.middleName = responseBody.getGetCustomerBody().getMiddleName();
			else
				this.middleName = "";
			if (responseBody.getGetCustomerBody().getLastName() != null)
				this.lastName = responseBody.getGetCustomerBody().getLastName();
			else
				this.lastName = "";
			if (responseBody.getGetCustomerBody().getCustomerType() != null) {
				this.customerType = ConfigurationManager
						.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE
								+ responseBody.getGetCustomerBody().getCustomerType());
			}
			if (responseBody.getGetCustomerBody().getGender() != null) {
				this.gender = ConfigurationManager.getConfigurationFromCache(
						ConfigurationManager.MAPPING_CRM_GENDER + responseBody.getGetCustomerBody().getGender());
			}
			if (hrlSub.getGetSubscriberBody().getBrandId() != null) {
				this.brandName = ConfigurationManager.getConfigurationFromCache(
						ConfigurationManager.MAPPING_HLR_BRANDID + hrlSub.getGetSubscriberBody().getBrandId());
			}
			if (responseBody.getGetCustomerBody().getDateOfBirth() != null) {
				try {
					this.dateOfBirth = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
							new SimpleDateFormat("yyyyMMdd").parse(responseBody.getGetCustomerBody().getDateOfBirth()));
				} catch (ParseException ex) {
					// TODO Auto-generated catch block
				}
			}
			// offering_id @XmlElement(name = "AccountId")
			if (hrlSub.getGetSubscriberBody().getEffectiveDate() != null)
				this.effectiveDate = hrlSub.getGetSubscriberBody().getEffectiveDate();
			if (hrlSub.getGetSubscriberBody().getExpireDate() != null)
				this.expireDate = hrlSub.getGetSubscriberBody().getExpireDate();
			if (hrlSub.getGetSubscriberBody().getSubscriberType() != null)
				this.subscriberType = ConfigurationManager
						.getConfigurationFromCache(ConfigurationManager.MAPPING_HLR_SUBSCRIBER_TYPE
								+ hrlSub.getGetSubscriberBody().getSubscriberType());
			if (hrlSub.getGetSubscriberBody().getStatus() != null) {
				this.status = ConfigurationManager.getConfigurationFromCache(
						ConfigurationManager.MAPPING_HLR_STATUS + hrlSub.getGetSubscriberBody().getStatus()
				// "B01"
				);
			}
			if (hrlSub.getGetSubscriberBody().getStatusDetail() != null)
				this.statusDetail = hrlSub.getGetSubscriberBody().getStatusDetail();
			if (hrlSub.getGetSubscriberBody().getBrandId() != null)
				this.brandId = hrlSub.getGetSubscriberBody().getBrandId();
			if (hrlSub.getGetSubscriberBody().getLoyaltySegment() != null)
				this.loyaltySegment = hrlSub.getGetSubscriberBody().getLoyaltySegment();
			if (hrlSub.getGetSubscriberBody().getAccountId() != null)
				this.accountId = hrlSub.getGetSubscriberBody().getAccountId();
			if (hrlSub.getGetSubscriberBody().getLanguage() != null) {
				this.language = ConfigurationManager.getConfigurationFromCache(
						ConfigurationManager.MAPPING_HLR_LANGUAGE + hrlSub.getGetSubscriberBody().getLanguage());
			}
			if (hrlSub.getGetSubscriberBody().getSupplementaryOfferingList() != null) {// com.huawei.bss.soaif._interface.hlrwebservice
				for (OfferingInfo offeringInfo : hrlSub.getGetSubscriberBody().getSupplementaryOfferingList()) {
					if (offeringInfo != null)
						this.supplementaryOfferingList.add(new com.evampsaanga.azerfon.getcustomerrequest.OfferingInfo(
								offeringInfo.getOfferingId(), offeringInfo.getOfferingName(),
								offeringInfo.getOfferingCode(), offeringInfo.getOfferingShortName(),
								offeringInfo.getStatus(), offeringInfo.getNetworkType(),
								offeringInfo.getEffectiveTime(), offeringInfo.getExpiredTime()));
				}
				// this.supplementaryOfferingList =
				// hrlSub.getGetSubscriberBody().getSupplementaryOfferingList();
			}
			if (hrlSub.getGetSubscriberBody().getPrimaryOffering() != null) {
				offering_id = hrlSub.getGetSubscriberBody().getPrimaryOffering().getOfferingId();
			}
			if (hrlSub.getGetSubscriberBody().getPrimaryOffering().getOfferingName() != null) {
				offeringName = hrlSub.getGetSubscriberBody().getPrimaryOffering().getOfferingName();
			}
		}
	}

	/**
	 * @return the supplementaryOfferingList
	 */
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the accountId
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the expireDate
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * @param expireDate
	 *            the expireDate to set
	 */
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	/**
	 * @return the subscriberType
	 */
	public String getSubscriberType() {
		return subscriberType;
	}

	/**
	 * @param subscriberType
	 *            the subscriberType to set
	 */
	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusDetail
	 */
	public String getStatusDetail() {
		return statusDetail;
	}

	/**
	 * @param statusDetail
	 *            the statusDetail to set
	 */
	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}

	/**
	 * @return the brandId
	 */
	public String getBrandId() {
		return brandId;
	}

	/**
	 * @param brandId
	 *            the brandId to set
	 */
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	/**
	 * @return the loyaltySegment
	 */
	public String getLoyaltySegment() {
		return loyaltySegment;
	}

	/**
	 * @param loyaltySegment
	 *            the loyaltySegment to set
	 */
	public void setLoyaltySegment(String loyaltySegment) {
		this.loyaltySegment = loyaltySegment;
	}

	/**
	 * @return the offering_id
	 */
	public String getOffering_id() {
		return offering_id;
	}

	/**
	 * @param offering_id
	 *            the offering_id to set
	 */
	public void setOffering_id(String offering_id) {
		this.offering_id = offering_id;
	}
}
