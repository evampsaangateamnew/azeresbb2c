package com.evampsaanga.developer.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;

public class SampleClass {

	public static void main(String[] args) throws ParseException

	{

//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//		SimpleDateFormat desiredDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//		String expireDate = dateFormat.format(desiredDateFormat.parse("20200813000000")) + " 00:00:00";
//		System.out.println(expireDate);
		
	
		//System.out.println(addOneDay("2020-10-19 00:00:00"));
		
//		String dt = "2020-10-19 00:00:00";  // Start date
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Calendar c = Calendar.getInstance();
//		c.setTime(sdf.parse(dt));
//		c.add(Calendar.DATE, 1);  // number of days to add
//		dt = sdf.format(c.getTime());  // dt is now the new date
//		System.out.println(dt);

		// Date today = new Date();
		// Calendar cal = Calendar.getInstance();
		// cal.setTime(today);
		// cal.add(Calendar.DAY_OF_MONTH, -30);
		// Date today30 = cal.getTime();
		// System.out.println("1-" + today30);
		// cal.add(Calendar.DAY_OF_MONTH, -60);
		// Date today60 = cal.getTime();
		// System.out.println("2-" + today60);
		// cal.add(Calendar.DAY_OF_MONTH, -90);
		// Date today90 = cal.getTime();
		// System.out.println("3-" + today90);
		// long size = 1126400L;
		//
		// if (size <= 0)
		// System.out.println("0 MB");
		// final String[] units = new String[] { "kB", "MB", "GB", "TB" };
		// int digitGroups = (int) (Math.log10(size) / Math.log10(1000));
		// System.out.println(digitGroups);
		// System.out.println(
		// new DecimalFormat("###0.#").format(size / Math.pow(1000,
		// digitGroups)) + " " + units[digitGroups]);
		//
		// System.out.println((((1126400L / 1024.0) / 1024.0)));

	}
	
	static public String addOneDay(String date) {
		return LocalDate.parse(date).plusDays(1).toString();
		}
	
}
