package com.evampsaanga.developer.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.azerfon.appserver.refreshappservercache.CustomerModelCache;
import com.evampsaanga.azerfon.db.DBFactory;
import com.evampsaanga.azerfon.otp.MessageTemplateResponse;
import com.evampsaanga.configs.ConfigurationManager;
import com.evampsaanga.configs.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Helper {

	public static final Logger logger = Logger.getLogger("azerfon-esb");
	public static final Logger loggerV2 = Logger.getLogger("azerfon-esb");

	public static Date getDateFromString(String str, String dateFormat) throws ParseException {
		if (!str.isEmpty())
			return new SimpleDateFormat(dateFormat).parse(str);
		return null;

	}

	public static String ifNullSetEmpty(String message) {
		if (message.equals("null") || message == null)
			return "";
		return message;
	}

	public static String retrieveToken(String transactionName, String msisdn) {

		Random random = new Random();
		return "[" + transactionName + "]" + "[" + msisdn + "]" + "[" + getCurrentTimeStamp() + "-"
				+ String.format("%04d", random.nextInt(10000)) + "]";
	}

	public static String addParamsToJSONObject(String data, String key, String value) throws JSONException {
		JSONObject dataJSON = new JSONObject(data);
		dataJSON.put(key, value);
		return dataJSON.toString();
	}

	public static String getValueFromJSON(String data, String key) throws JSONException {
		JSONObject dataJSON = new JSONObject(data);
		return dataJSON.getString(key);
	}

	public static String removeParamsFromJSONObject(String data, String key) throws JSONException {
		JSONObject dataJSON = new JSONObject(data);
		dataJSON.remove(key);
		return dataJSON.toString();
	}

	public static String getBakcellMoney(long amount) {
		Double amountD = (double) amount;
		NumberFormat formatter = new DecimalFormat("#0.00");
		formatter.setRoundingMode(RoundingMode.FLOOR);
		return "" + formatter.format(Double.parseDouble((amountD / Constants.MONEY_DIVIDEND) + ""));
	}

	public static String getBakcellMoneyDouble(Double amountD) {
		// Double amountD = (double) amount;
		NumberFormat formatter = new DecimalFormat("#0.00");
		formatter.setRoundingMode(RoundingMode.FLOOR);
		return "" + formatter.format(amountD);
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public static String getBakcellMoneyCeilingMode(long amount) {
		NumberFormat formatter = new DecimalFormat("#0.000");
		formatter.setRoundingMode(RoundingMode.FLOOR);
		NumberFormat formatter1 = new DecimalFormat("#0.00");
		double divide = ((double) amount) / Constants.MONEY_DIVIDEND;
		divide = divide + 0.00001;
		String val = formatter.format(divide);
		double finalVal = Double.parseDouble(val);
		return formatter1.format(finalVal);
	}

	public static void main(String[] args) {
		System.out.println(getBakcellMoneyCeilingMode(4004494));
	}

	public static String dateFormattorFullDate(String date) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format(new SimpleDateFormat("yyyyMMddHHmmss").parse(date));
		} catch (ParseException e) {
			logger.error(Helper.GetException(e));
		}
		return "";
	}

	public static String parseDateDynamically(String token, String dateTime, String formatWithTime) {

		Date daa = DateUtil.getDate(dateTime);// stringToDate

		String pattern = formatWithTime;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(daa);
		// logger.info(token + "-Parsed Date-" + date);

		return date;
	}

	public static String dateFormattorOnlyDate(String date, String token) {

		logger.info(token + "data To Parsed in dateFormattorOnlyDate Method: " + date);
		SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {
			DateFormat targetFormat = new SimpleDateFormat("yyyyMMdd");
			Date datetoBeformateed = originalFormat.parse(date);
			String formattedDate = targetFormat.format(datetoBeformateed); // 20120821

			logger.info(token + "Date To Return from dateFormattorOnlyDate" + formattedDate);
			return formattedDate;

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.info(token + Helper.GetException(e));
		}

		return "";
	}

	public static String getBakcellMoneyMRC(long amount) {
		if (amount == 0)
			return "FREE";
		Double amountD = (double) amount;
		NumberFormat formatter = new DecimalFormat("#0.00");
		formatter.setRoundingMode(RoundingMode.CEILING);
		return "" + formatter.format(Double.parseDouble((amountD / Constants.MONEY_DIVIDEND) + ""));
	}

	public static boolean isValidFormat(String format, String value) {
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
		}
		return date != null;
	}

	public static <T> T JsonToObject(String json, Class<T> type) throws Exception, IOException {
		return new ObjectMapper().readValue(json, type);
	}

	public static <T> T JsonToObjectFile(File json, Class<T> type) throws Exception, IOException {
		logger.debug(json);
		return new ObjectMapper().readValue(json, type);
	}

	public static <T> String ObjectToJson(T type) throws Exception, IOException {
		return new ObjectMapper().writeValueAsString(type);
	}

	public static String GenerateDateTimeToMsAccuracy() {
		return new SimpleDateFormat(Constants.SQL_DATE_FORMAT).format(new Date());
	}

	public static String generateTransactionID() {
		String tID = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).toString()
				+ RandomUtils.nextFloat() * 1000;
		return tID.substring(0, tID.indexOf('.'));
	}

	@SuppressWarnings("unused")
	private static String getValidationRegExByKey(String key) {
		return ConfigurationManager.getConfigurationFromCache(key);
	}

	@SuppressWarnings("unused")
	private static String getValidationErrorByKey(String key) {
		return ConfigurationManager.getConfigurationFromCache(key + ".error");
	}

	public static <T> String validateRequest(T type) {
		// Field[] fields = type.getClass().getSuperclass().getDeclaredFields();
		// for (int i = 0; i < fields.length; i++) {
		// fields[i].setAccessible(true);
		// try {
		// if
		// (!fields[i].get(type).toString().matches((getValidationRegExByKey(fields[i].getName()))))
		// {
		// return getValidationErrorByKey(fields[i].getName());
		// }
		// } catch (Exception ex) {
		// logger.error(Helper.GetException(ex));
		// }
		// }
		return "";
	}

	public static void logInfoMessage(String... input) {
		String logMessage = "";
		for (int i = 0; i < input.length; i++)
			logMessage = input[i] + "-";
		logger.info(logMessage);
	}

	public static void logInfoMessageV2(String... input) {
		String logMessage = "";
		for (int i = 0; i < input.length; i++)
			logMessage = input[i] + "-";
		loggerV2.info(logMessage);
	}

	public static void logDebugMessage(String... input) {
		String logMessage = "";
		for (int i = 0; i < input.length; i++)
			logMessage = input[i] + "-";
		logger.debug(logMessage);
	}

	public static String GetException(Exception ex) {
		return ExceptionUtils.getFullStackTrace(ex);
	}

	public static String GetException(Throwable ex) {
		return ExceptionUtils.getFullStackTrace(ex);
	}

	public static String getOMlogTimeStamp() {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss:SSSS");
		String timestamp = format.format(new Date());
		return timestamp + " :";
	}

	public static String round(int n) {
		// Smaller multiple
		int a = (n / 10) * 10;

		// Larger multiple
		int b = a + 10;

		// Return of closest of two
		return String.valueOf(b);
		// return (n - a > b - n)? b : a;
	}

	public static boolean checkDate(String date) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date dateNew = simpleDateFormat.parse(date);
		Date d1 = new Date();
		return d1.before(dateNew);

	}

	public static String currentMachineIPAddress() {

		String IP = "";
		try {
			IP = InetAddress.getLocalHost().getHostAddress();
			loggerV2.info("---------CURRENT IP------- " + IP);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return IP;
	}

	public static String getCurrentTimeStamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
	}

	public static String returnEmptyStringOnNull(String textCheck) {
		// TODO Auto-generated method stub
		if (textCheck != null)
			return textCheck;
		else
			return "";
	}

	public static String addOneDay(String date) throws ParseException {
		loggerV2.info("Date Before Adding One Day is:" + date);
		String newDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(date));
		c.add(Calendar.DATE, 1); // number of days to add
		newDate = sdf.format(c.getTime()); // dt is now the new date
		loggerV2.info("Date After Adding One Day is:" + newDate);
		return newDate;
	}

	public static String checkSHApass(String input, String msisdn) {

		try {
			loggerV2.info("LOGIN:" + msisdn + " " + input);
			String prefix = "xxxxxxxx";
			String postfix = ":xxxxxxxx:1";
			input = prefix + input;
			// Static getInstance method is called with hashing SHA
			MessageDigest md = MessageDigest.getInstance("SHA-256");

			// digest() method called
			// to calculate message digest of an input
			// and return array of byte
			byte[] messageDigest = md.digest(input.getBytes());

			// Convert byte array into signum representation
			BigInteger no = new BigInteger(1, messageDigest);

			// Convert message digest into hex value
			String hashtext = no.toString(16);

			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			loggerV2.info("LOGIN:" + msisdn + " SHA256 converted " + hashtext + postfix);
			return hashtext + postfix;
		}

		// For specifying wrong message digest algorithms
		catch (NoSuchAlgorithmException e) {
			System.out.println("Exception thrown" + " for incorrect algorithm: " + e);
			loggerV2.info(Helper.GetException(e));

			return null;
		}
	}

	// public static String checkSHApass(String password,String msisdn)
	// {
	//
	//
	//
	// loggerV2.info("LOGIN:"+msisdn+"checking hash in db");
	// String data = "";
	// PreparedStatement preparedStatement = null;
	// ResultSet resultSet = null;
	// try{
	// String getAllConfiguration =
	// "select CONCAT(SHA2('xxxxxxxx"+password+"', 256), ':xxxxxxxx:1') pass";
	// preparedStatement =
	// DBFactory.getDbConnection().prepareStatement(getAllConfiguration);
	// loggerV2.info("LOGIN:"+msisdn+"query "+preparedStatement.toString());
	// resultSet = preparedStatement.executeQuery();
	// loggerV2.info("Converting SHA 256 :"+preparedStatement.toString());
	// while(resultSet.next())
	// {
	// loggerV2.info("LOGIN:"+msisdn+" "+resultSet.getString("pass"));
	// // loggerV2.info("Email FROM view : :"+resultSet.getString("email"));
	// data = resultSet.getString("pass");
	//
	// }
	// // propertiesCacheHashMap.put(resultSet.getString("key"),
	// resultSet.getString("value"));
	// }catch (Exception e) {
	// logger.error(Helper.GetException(e));
	//
	// }
	// finally {
	// try{
	// if(resultSet != null)
	// resultSet.close();
	// if(preparedStatement != null)
	// preparedStatement.close();
	// return data;
	// }catch (Exception e) {
	// logger.error(Helper.GetException(e));
	// }
	// }
	// return data;
	// }

	public static boolean checkMsisdnExistInDB(String msisdn) {

		String query = "select * from ( SELECT entity_id, password_hash, email, sum(msisdn) msisdn, sum(customerid) customerid FROM ( SELECT customer_entity.entity_id, customer_entity.password_hash, customer_entity.email, ( CASE WHEN ( `customer_entity_varchar`.`attribute_id` = '212' ) THEN `customer_entity_varchar`.`value` END ) AS `msisdn`, ( CASE WHEN ( `customer_entity_varchar`.`attribute_id` = '582' ) THEN `customer_entity_varchar`.`value` END ) AS `customerid` FROM customer_entity, customer_entity_varchar WHERE customer_entity.entity_id = customer_entity_varchar.entity_id AND customer_entity_varchar.attribute_id IN (212, 582) AND customer_entity.group_id = 1 ) AS customer GROUP BY entity_id, password_hash,email ) as a WHERE msisdn="
				+ msisdn + "";
		String data = "";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection con = null;
		try {
			con = DBFactory.getMagentoDBConnection();
			preparedStatement = con.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			loggerV2.info("QeuryToGetUserData:" + preparedStatement.toString());
			if (resultSet.next()) {
				CustomerModelCache customerModelCache = new CustomerModelCache();
				customerModelCache.setEmail(resultSet.getString("email"));
				customerModelCache.setEntity_id(resultSet.getString("entity_id"));
				customerModelCache.setMsisdn(resultSet.getString("msisdn"));
				customerModelCache.setPassword_hash(resultSet.getString("password_hash"));
				customerModelCache.setCustomerId(resultSet.getString("customerid"));
				customerModelCache.setIsFromDB("true");
				BuildCacheRequestLand.customerCache.put(resultSet.getString("msisdn"), customerModelCache);
				return true;

			} else {
				return false;
			}

		} catch (Exception e) {
			logger.error(Helper.GetException(e));
			return false;
		} finally {
			logger.info("Reloading Cache End...");
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStatement != null)
					preparedStatement.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}

	}

	public static boolean checkAndUpdateCache(String msisdn) {
		try {
			if (BuildCacheRequestLand.customerCache == null)
				BuildCacheRequestLand.initHazelcast();
			if (BuildCacheRequestLand.customerCache.containsKey(msisdn)) {
				return true;
			} else {
				// return checkMsisdnExistInDB(msisdn);
				return false;

			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(Helper.GetException(e));
			return false;
		}
	}

	public static String getMessage(String section, String lang, String code, String token) {
		MessageTemplateResponse messageTemplateResponse = ConfigurationManager
				.getMessageTemplateFromCache(Constants.GET_MESSAGE_TEMPLATE_KEY, token);

		String message = "";
		if (lang.equalsIgnoreCase("2")) {
			if (section.equalsIgnoreCase("signup"))
				message = messageTemplateResponse.getData().getSmsSignUpRu();
			else if (section.equalsIgnoreCase("forgot"))
				message = messageTemplateResponse.getData().getSmsForgotPasswordRu();
			else if (section.equalsIgnoreCase("usagehistory"))
				message = messageTemplateResponse.getData().getSmsUsageRu();
			else if (section.equalsIgnoreCase("moneytransfer"))
				message = messageTemplateResponse.getData().getSmsMoneyTransferRU();
		} else if (lang.equalsIgnoreCase("4")) {
			if (section.equalsIgnoreCase("signup"))
				message = messageTemplateResponse.getData().getSmsSignUpAz();
			else if (section.equalsIgnoreCase("forgot"))
				message = messageTemplateResponse.getData().getSmsForgotPasswordAz();
			else if (section.equalsIgnoreCase("usagehistory"))
				message = messageTemplateResponse.getData().getSmsUsageAz();
			else if (section.equalsIgnoreCase("moneytransfer"))
				message = messageTemplateResponse.getData().getSmsMoneyTransferAZ();
		} else {
			if (section.equalsIgnoreCase("signup"))
				message = messageTemplateResponse.getData().getSmsSignUpEn();
			else if (section.equalsIgnoreCase("forgot"))
				message = messageTemplateResponse.getData().getSmsForgotPasswordEn();
			else if (section.equalsIgnoreCase("usagehistory"))
				message = messageTemplateResponse.getData().getSmsUsageEn();
			else if (section.equalsIgnoreCase("moneytransfer"))
				message = messageTemplateResponse.getData().getSmsMoneyTransferEn();
		}
		if (message != null && message.length() > 0)
			message = message.replace("@code@", code);
		logger.info(token + "response getMessage" + message);
		return message;
	}

	public static String generateFourDigitNumber() {
		Random random = new Random();
		return String.format("%04d", random.nextInt(10000));
	}

	public static String generatingDateTimeBeforeFourHours(String inputDateTime, String token) {
		logger.info(token + "Request Reached in Helper Method with inout Data :" + inputDateTime);
		SimpleDateFormat dtfrmt = new SimpleDateFormat("yyyyMMddHHmmss");
		Date inputDate;
		try {
			inputDate = dtfrmt.parse(inputDateTime);

			logger.info(token + "DAte After PArsing :" + inputDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(inputDate);
			calendar.add(Calendar.HOUR_OF_DAY, -4);
			Date datecleneder = calendar.getTime();

			System.out.println("time final :" + calendar.getTime());
			String finalformatingForThirpart = dtfrmt.format(datecleneder);
			System.out.println("time After Formating: " + finalformatingForThirpart);
			return finalformatingForThirpart;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.info(token + Helper.GetException(e));
			return "";
		}

	}

	public static String getLang(String lang) {
		String language = "";
		if (lang != null && lang.equals(Constants.LANGUAGE_AZERI_MAPPING)) {
			language = Constants.LANGUAGE_AZERI_MAPPING_DESC;
		} else if (lang != null && lang.equals(Constants.LANGUAGE_ENGLISH_MAPPING)) {
			language = Constants.LANGUAGE_ENGLISH_MAPPING_DESC;
		} else if (lang != null && lang.equals(Constants.LANGUAGE_RUSSIAN_MAPPING)) {
			language = Constants.LANGUAGE_RUSSIAN_MAPPING_DESC;
		} else {
			language = Constants.LANGUAGE_ENGLISH_MAPPING_DESC;
		}
		return language;
	}

	public static String addOneSecondForNGBSSExpiryDate(String inputDate) throws ParseException {

		Date oldDate = new SimpleDateFormat("yyyyMMddHHmmss").parse(inputDate);
		Calendar gcal = new GregorianCalendar();
		gcal.setTime(oldDate);
		gcal.add(Calendar.SECOND, 1);
		Date newDate = gcal.getTime();
		// System.out.println(newDate);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String strDate = formatter.format(newDate);
		// System.out.println("Date Format with yyyyMMddHHmmss : "+strDate);
		return strDate;

	}

	public static long getMiliSecondsFromDate(String myDate) {
		long miliSec = 0;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = null;
			try {
				date = sdf.parse(myDate);
			} catch (ParseException e) {
				logger.info("Exception while parsing1" + e.getMessage());
				return 0;
			}
			miliSec = date.getTime();
		} catch (Exception e) {
			logger.info("Exception while parsing2" + e.getMessage());
		}
		return miliSec;
	}

	public static long getOneDayMinus(long startMili) {
		if (startMili > 0) {
			startMili = (long) (startMili - 8.64e+7);
		}
		return startMili;
	}

	public static long getCurrentMiliSeconds() {
		return System.currentTimeMillis();
	}

	public static int getDaysDifferenceBetweenMiliseconds(long startDate, long endDate) {

		long duration = endDate - startDate;

		long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);

		int daysRemain = (int) diffInDays;

		return daysRemain;
	}

}
