package com.evampsaanga.developer;

import java.io.IOException;
import org.apache.log4j.Logger;
import com.evampsaanga.azerfon.sms.SendSMSRequest;
import com.evampsaanga.configs.ResponseCodes;
import com.evampsaanga.developer.utils.Helper;

public class SendSMCService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public boolean sendSMS(String msisdn, String msg, String channel, String lang, String ip, String sender)
			throws IOException, Exception {
		com.evampsaanga.azerfon.sms.SendSMSRequestLand land = new com.evampsaanga.azerfon.sms.SendSMSRequestLand();
		SendSMSRequest request = new SendSMSRequest();
		request.setChannel(channel);
		request.setiP(ip);
		request.setLang(lang);
		request.setMsisdn(msisdn);
		request.setTextmsg(msg);
		request.setSender(sender);
		com.evampsaanga.azerfon.sms.SendSMSResponse response = land
				.Get(com.evampsaanga.configs.Constants.CREDENTIALSUNCODED, Helper.ObjectToJson(request));
		return response.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200);
	}
}
