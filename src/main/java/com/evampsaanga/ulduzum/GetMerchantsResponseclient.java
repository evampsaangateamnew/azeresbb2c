package com.evampsaanga.ulduzum;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetMerchantsResponseclient extends BaseResponse {
	
	
	List<com.evampsaanga.ulduzum.getmerchants.Datum> data=new ArrayList<>();
	List<com.evampsaanga.ulduzum.getcategories.Datum> categoryNamelist=null;

	public List<com.evampsaanga.ulduzum.getmerchants.Datum> getData() {
		return data;
	}

	public void setData(List<com.evampsaanga.ulduzum.getmerchants.Datum> data) {
		this.data = data;
	}

	public List<com.evampsaanga.ulduzum.getcategories.Datum> getCategoryNamelist() {
		return categoryNamelist;
	}

	public void setCategoryNamelist(List<com.evampsaanga.ulduzum.getcategories.Datum> categoryNamelist) {
		this.categoryNamelist = categoryNamelist;
	}






}
