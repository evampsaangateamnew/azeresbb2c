package com.evampsaanga.ulduzum;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;

public class GetUsageHistoryResponseclient extends BaseResponse {
	
	List<com.evampsaanga.ulduzum.getusagehistory.Datum> data=new ArrayList<>();

	public List<com.evampsaanga.ulduzum.getusagehistory.Datum> getData() {
		return data;
	}

	public void setData(List<com.evampsaanga.ulduzum.getusagehistory.Datum> data) {
		this.data = data;
	}

}
