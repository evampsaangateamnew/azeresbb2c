package com.evampsaanga.ulduzum;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.ulduzum.getunusedcodes.Datum;

public class GetUnusedCodesResponseclient extends BaseResponse {
	
	List<Datum> data=new ArrayList<>();

	public List<Datum> getData() {
		return data;
	}

	public void setData(List<Datum> data) {
		this.data = data;
	}

}
