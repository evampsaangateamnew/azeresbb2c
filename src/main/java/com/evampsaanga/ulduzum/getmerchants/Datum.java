
package com.evampsaanga.ulduzum.getmerchants;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "parent_id",
    "name",
    "category_id",
    "category_name",
    "short_code",
    "contact_person",
    "telephone",
    "mobile",
    "min_discount_amount",
    "max_discount_amount",
    "segment_a_discount",
    "segment_b_discount",
    "segment_c_discount",
    "coor_lat",
    "coor_lng",
    "address",
    "description",
    "app_sharename",
    "app_redmsharename",
    "logo_small"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Datum {

    @JsonProperty("id")
    private String id;
    @JsonProperty("parent_id")
    private String parentId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("category_id")
    private String categoryId;
    @JsonProperty("category_name")
    private String categoryName;
    @JsonProperty("short_code")
    private String shortCode;
    @JsonProperty("contact_person")
    private String contactPerson;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("min_discount_amount")
    private String minDiscountAmount;
    @JsonProperty("max_discount_amount")
    private String maxDiscountAmount;
    @JsonProperty("segment_a_discount")
    private String segmentADiscount;
    @JsonProperty("segment_b_discount")
    private String segmentBDiscount;
    @JsonProperty("segment_c_discount")
    private String segmentCDiscount;
    
    @JsonProperty("coor_lat")
    private String coorLat;
    @JsonProperty("coor_lng")
    private String coorLng;
    @JsonProperty("address")
    private String address;
    @JsonProperty("description")
    private String description;
    @JsonProperty("app_sharename")
    private String appSharename;
    @JsonProperty("app_redmsharename")
    private String appRedmsharename;
    @JsonProperty("logo_small")
    private LogoSmall logoSmall;
    
    private String loyality_segment="";
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("parent_id")
    public String getParentId() {
        return parentId;
    }

    @JsonProperty("parent_id")
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("category_id")
    public String getCategoryId() {
        return categoryId;
    }

    @JsonProperty("category_id")
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty("category_name")
    public String getCategoryName() {
        return categoryName;
    }

    @JsonProperty("category_name")
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonProperty("short_code")
    public String getShortCode() {
        return shortCode;
    }

    @JsonProperty("short_code")
    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    @JsonProperty("contact_person")
    public String getContactPerson() {
        return contactPerson;
    }

    @JsonProperty("contact_person")
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty("min_discount_amount")
    public String getMinDiscountAmount() {
        return minDiscountAmount;
    }

    @JsonProperty("min_discount_amount")
    public void setMinDiscountAmount(String minDiscountAmount) {
        this.minDiscountAmount = minDiscountAmount;
    }

    @JsonProperty("max_discount_amount")
    public String getMaxDiscountAmount() {
        return maxDiscountAmount;
    }

    @JsonProperty("max_discount_amount")
    public void setMaxDiscountAmount(String maxDiscountAmount) {
        this.maxDiscountAmount = maxDiscountAmount;
    }

    @JsonProperty("segment_a_discount")
    public String getSegmentADiscount() {
        return segmentADiscount;
    }

    @JsonProperty("segment_a_discount")
    public void setSegmentADiscount(String segmentADiscount) {
        this.segmentADiscount = segmentADiscount;
    }

    @JsonProperty("segment_b_discount")
    public String getSegmentBDiscount() {
        return segmentBDiscount;
    }

    @JsonProperty("segment_b_discount")
    public void setSegmentBDiscount(String segmentBDiscount) {
        this.segmentBDiscount = segmentBDiscount;
    }

    @JsonProperty("segment_c_discount")
    public String getSegmentCDiscount() {
        return segmentCDiscount;
    }

    @JsonProperty("segment_c_discount")
    public void setSegmentCDiscount(String segmentCDiscount) {
        this.segmentCDiscount = segmentCDiscount;
    }

    @JsonProperty("coor_lat")
    public String getCoorLat() {
        return coorLat;
    }

    @JsonProperty("coor_lat")
    public void setCoorLat(String coorLat) {
        this.coorLat = coorLat;
    }

    @JsonProperty("coor_lng")
    public String getCoorLng() {
        return coorLng;
    }

    @JsonProperty("coor_lng")
    public void setCoorLng(String coorLng) {
        this.coorLng = coorLng;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("app_sharename")
    public String getAppSharename() {
        return appSharename;
    }

    @JsonProperty("app_sharename")
    public void setAppSharename(String appSharename) {
        this.appSharename = appSharename;
    }

    @JsonProperty("app_redmsharename")
    public String getAppRedmsharename() {
        return appRedmsharename;
    }

    @JsonProperty("app_redmsharename")
    public void setAppRedmsharename(String appRedmsharename) {
        this.appRedmsharename = appRedmsharename;
    }

    @JsonProperty("logo_small")
    public LogoSmall getLogoSmall() {
        return logoSmall;
    }

    @JsonProperty("logo_small")
    public void setLogoSmall(LogoSmall logoSmall) {
        this.logoSmall = logoSmall;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	public String getLoyality_segment() {
		return loyality_segment;
	}

	public void setLoyality_segment(String loyality_segment) {
		this.loyality_segment = loyality_segment;
	}

}
