package com.evampsaanga.ulduzum;

import com.evampsaanga.azerfon.requestheaders.BaseRequest;
import com.evampsaanga.ulduzum.codegenerate.GetCodegenerateData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UlduzumRequestClient extends BaseRequest {

	
	String loyaltySegment="";

	public String getLoyaltySegment() {
		return loyaltySegment;
	}

	public void setLoyaltySegment(String loyaltySegment) {
		this.loyaltySegment = loyaltySegment;
	}
	
	
	

}
