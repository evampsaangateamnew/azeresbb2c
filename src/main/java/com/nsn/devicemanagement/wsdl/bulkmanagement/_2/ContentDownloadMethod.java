package com.nsn.devicemanagement.wsdl.bulkmanagement._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ContentDownloadMethod.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="ContentDownloadMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OMA_SCOMO"/>
 *     &lt;enumeration value="WAP_PUSH"/>
 *     &lt;enumeration value="TEXT_SMS"/>
 *     &lt;enumeration value="TARM_CLIENT"/>
 *     &lt;enumeration value="SADM_CLIENT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContentDownloadMethod", namespace = "http://nsn.com/DeviceManagement/wsdl/BulkManagement/2.0")
@XmlEnum
public enum ContentDownloadMethod {
	OMA_SCOMO, WAP_PUSH, TEXT_SMS, TARM_CLIENT, SADM_CLIENT;
	public String value() {
		return name();
	}

	public static ContentDownloadMethod fromValue(String v) {
		return valueOf(v);
	}
}
