package com.nsn.devicemanagement.wsdl.devicemanagement._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for linkParameter complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="linkParameter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wsisetting" type="{http://nsn.com/DeviceManagement/wsdl/DeviceManagement/2.0}setting" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "linkParameter", propOrder = { "wsisetting" })
public class LinkParameter {
	protected Setting wsisetting;

	/**
	 * Gets the value of the wsisetting property.
	 * 
	 * @return possible object is {@link Setting }
	 * 
	 */
	public Setting getWsisetting() {
		return wsisetting;
	}

	/**
	 * Sets the value of the wsisetting property.
	 * 
	 * @param value
	 *            allowed object is {@link Setting }
	 * 
	 */
	public void setWsisetting(Setting value) {
		this.wsisetting = value;
	}
}
