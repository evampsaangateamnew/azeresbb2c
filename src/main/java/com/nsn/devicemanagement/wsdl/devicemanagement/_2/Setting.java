package com.nsn.devicemanagement.wsdl.devicemanagement._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for setting complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="setting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="displayableName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="external" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="linkedParameters" type="{http://nsn.com/DeviceManagement/wsdl/DeviceManagement/2.0}linkParameter" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameterGroups" type="{http://nsn.com/DeviceManagement/wsdl/DeviceManagement/2.0}parameterGroup" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setting", propOrder = { "displayableName", "external", "linkedParameters", "name", "parameterGroups" })
public class Setting {
	protected String displayableName;
	protected String external;
	@XmlElement(nillable = true)
	protected List<LinkParameter> linkedParameters;
	protected String name;
	@XmlElement(nillable = true)
	protected List<ParameterGroup> parameterGroups;

	/**
	 * Gets the value of the displayableName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDisplayableName() {
		return displayableName;
	}

	/**
	 * Sets the value of the displayableName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDisplayableName(String value) {
		this.displayableName = value;
	}

	/**
	 * Gets the value of the external property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getExternal() {
		return external;
	}

	/**
	 * Sets the value of the external property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setExternal(String value) {
		this.external = value;
	}

	/**
	 * Gets the value of the linkedParameters property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the linkedParameters property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getLinkedParameters().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link LinkParameter }
	 * 
	 * 
	 */
	public List<LinkParameter> getLinkedParameters() {
		if (linkedParameters == null) {
			linkedParameters = new ArrayList<LinkParameter>();
		}
		return this.linkedParameters;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the parameterGroups property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the parameterGroups property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getParameterGroups().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ParameterGroup }
	 * 
	 * 
	 */
	public List<ParameterGroup> getParameterGroups() {
		if (parameterGroups == null) {
			parameterGroups = new ArrayList<ParameterGroup>();
		}
		return this.parameterGroups;
	}
}
