package com.nsn.devicemanagement.wsdl.devicesecurity._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for autoLockSettings complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="autoLockSettings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="autoLockOn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "autoLockSettings", propOrder = { "autoLockOn" })
public class AutoLockSettings {
	protected Integer autoLockOn;

	/**
	 * Gets the value of the autoLockOn property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getAutoLockOn() {
		return autoLockOn;
	}

	/**
	 * Sets the value of the autoLockOn property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 * 
	 */
	public void setAutoLockOn(Integer value) {
		this.autoLockOn = value;
	}
}
