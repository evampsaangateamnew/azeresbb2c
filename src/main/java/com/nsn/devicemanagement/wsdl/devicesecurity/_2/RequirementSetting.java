package com.nsn.devicemanagement.wsdl.devicesecurity._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for requirementSetting.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="requirementSetting">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NOT_REQUIRED"/>
 *     &lt;enumeration value="REQUIRED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "requirementSetting")
@XmlEnum
public enum RequirementSetting {
	NOT_REQUIRED, REQUIRED;
	public String value() {
		return name();
	}

	public static RequirementSetting fromValue(String v) {
		return valueOf(v);
	}
}
