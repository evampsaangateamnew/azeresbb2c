package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for rcBootstrapScenario complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="rcBootstrapScenario">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Scenario">
 *       &lt;sequence>
 *         &lt;element name="notification" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message" minOccurs="0"/>
 *         &lt;element name="operatorName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proxyExternalAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proxyExternalPort" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="proxyInternalAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proxyInternalPort" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rcBootstrapScenario", propOrder = { "notification", "operatorName", "proxyExternalAddress",
		"proxyExternalPort", "proxyInternalAddress", "proxyInternalPort" })
public class RcBootstrapScenario extends Scenario {
	protected Message notification;
	protected String operatorName;
	protected String proxyExternalAddress;
	protected int proxyExternalPort;
	protected String proxyInternalAddress;
	protected int proxyInternalPort;

	/**
	 * Gets the value of the notification property.
	 * 
	 * @return possible object is {@link Message }
	 * 
	 */
	public Message getNotification() {
		return notification;
	}

	/**
	 * Sets the value of the notification property.
	 * 
	 * @param value
	 *            allowed object is {@link Message }
	 * 
	 */
	public void setNotification(Message value) {
		this.notification = value;
	}

	/**
	 * Gets the value of the operatorName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOperatorName() {
		return operatorName;
	}

	/**
	 * Sets the value of the operatorName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOperatorName(String value) {
		this.operatorName = value;
	}

	/**
	 * Gets the value of the proxyExternalAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProxyExternalAddress() {
		return proxyExternalAddress;
	}

	/**
	 * Sets the value of the proxyExternalAddress property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProxyExternalAddress(String value) {
		this.proxyExternalAddress = value;
	}

	/**
	 * Gets the value of the proxyExternalPort property.
	 * 
	 */
	public int getProxyExternalPort() {
		return proxyExternalPort;
	}

	/**
	 * Sets the value of the proxyExternalPort property.
	 * 
	 */
	public void setProxyExternalPort(int value) {
		this.proxyExternalPort = value;
	}

	/**
	 * Gets the value of the proxyInternalAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProxyInternalAddress() {
		return proxyInternalAddress;
	}

	/**
	 * Sets the value of the proxyInternalAddress property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProxyInternalAddress(String value) {
		this.proxyInternalAddress = value;
	}

	/**
	 * Gets the value of the proxyInternalPort property.
	 * 
	 */
	public int getProxyInternalPort() {
		return proxyInternalPort;
	}

	/**
	 * Sets the value of the proxyInternalPort property.
	 * 
	 */
	public void setProxyInternalPort(int value) {
		this.proxyInternalPort = value;
	}
}
