package com.nsn.devicemanagement.wsdl.common._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SettingScanAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SettingScanAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="settingTypes" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SettingScanAction", propOrder = { "settingTypes" })
public class SettingScanAction extends Action {
	@XmlElement(nillable = true)
	protected List<String> settingTypes;

	/**
	 * Gets the value of the settingTypes property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the settingTypes property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getSettingTypes().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 * 
	 * 
	 */
	public List<String> getSettingTypes() {
		if (settingTypes == null) {
			settingTypes = new ArrayList<String>();
		}
		return this.settingTypes;
	}
}
