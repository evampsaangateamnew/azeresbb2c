package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ScenarioInvocationAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ScenarioInvocationAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="scenarioName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScenarioInvocationAction", propOrder = { "scenarioName" })
public class ScenarioInvocationAction extends Action {
	protected String scenarioName;

	/**
	 * Gets the value of the scenarioName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getScenarioName() {
		return scenarioName;
	}

	/**
	 * Sets the value of the scenarioName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setScenarioName(String value) {
		this.scenarioName = value;
	}
}
