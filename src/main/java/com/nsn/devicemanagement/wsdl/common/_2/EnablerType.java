package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for enablerType.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="enablerType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OMA_CP"/>
 *     &lt;enumeration value="NOKIA_ERICSSON_OTA"/>
 *     &lt;enumeration value="NOKIA_SMARTMESSAGE"/>
 *     &lt;enumeration value="OPENWAVE_OTA"/>
 *     &lt;enumeration value="MICROSOFT_OTA"/>
 *     &lt;enumeration value="MANUAL_TEXT_OTA"/>
 *     &lt;enumeration value="CUSTOM_OTA_ENCODING_1"/>
 *     &lt;enumeration value="WAP_PUSH"/>
 *     &lt;enumeration value="TEXT_SMS"/>
 *     &lt;enumeration value="OMA_DM"/>
 *     &lt;enumeration value="OMA_DL"/>
 *     &lt;enumeration value="OMA_FUMO"/>
 *     &lt;enumeration value="OMA_SCOMO"/>
 *     &lt;enumeration value="SADM_DM_CLIENT"/>
 *     &lt;enumeration value="NOKIA_TARM_CLIENT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enablerType")
@XmlEnum
public enum EnablerType {
	OMA_CP, NOKIA_ERICSSON_OTA, NOKIA_SMARTMESSAGE, OPENWAVE_OTA, MICROSOFT_OTA, MANUAL_TEXT_OTA, CUSTOM_OTA_ENCODING_1, WAP_PUSH, TEXT_SMS, OMA_DM, OMA_DL, OMA_FUMO, OMA_SCOMO, SADM_DM_CLIENT, NOKIA_TARM_CLIENT;
	public String value() {
		return name();
	}

	public static EnablerType fromValue(String v) {
		return valueOf(v);
	}
}
