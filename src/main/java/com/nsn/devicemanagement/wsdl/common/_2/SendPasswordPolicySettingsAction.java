package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.devicesecurity._2.PasswordPolicySettings;

/**
 * <p>
 * Java class for SendPasswordPolicySettingsAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SendPasswordPolicySettingsAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="settings" type="{http://nsn.com/DeviceManagement/wsdl/DeviceSecurity/2.0}passwordPolicySettings" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendPasswordPolicySettingsAction", propOrder = { "settings" })
public class SendPasswordPolicySettingsAction extends Action {
	protected PasswordPolicySettings settings;

	/**
	 * Gets the value of the settings property.
	 * 
	 * @return possible object is {@link PasswordPolicySettings }
	 * 
	 */
	public PasswordPolicySettings getSettings() {
		return settings;
	}

	/**
	 * Sets the value of the settings property.
	 * 
	 * @param value
	 *            allowed object is {@link PasswordPolicySettings }
	 * 
	 */
	public void setSettings(PasswordPolicySettings value) {
		this.settings = value;
	}
}
