package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for applicationManagementMethod complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="applicationManagementMethod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="type" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}enablerType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "applicationManagementMethod", propOrder = { "type" })
public class ApplicationManagementMethod {
	@XmlSchemaType(name = "string")
	protected EnablerType type;

	/**
	 * Gets the value of the type property.
	 * 
	 * @return possible object is {@link EnablerType }
	 * 
	 */
	public EnablerType getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param value
	 *            allowed object is {@link EnablerType }
	 * 
	 */
	public void setType(EnablerType value) {
		this.type = value;
	}
}
