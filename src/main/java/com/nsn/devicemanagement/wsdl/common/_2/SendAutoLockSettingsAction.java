package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.devicesecurity._2.AutoLockSettings;

/**
 * <p>
 * Java class for SendAutoLockSettingsAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SendAutoLockSettingsAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="settings" type="{http://nsn.com/DeviceManagement/wsdl/DeviceSecurity/2.0}autoLockSettings" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendAutoLockSettingsAction", propOrder = { "settings" })
public class SendAutoLockSettingsAction extends Action {
	protected AutoLockSettings settings;

	/**
	 * Gets the value of the settings property.
	 * 
	 * @return possible object is {@link AutoLockSettings }
	 * 
	 */
	public AutoLockSettings getSettings() {
		return settings;
	}

	/**
	 * Sets the value of the settings property.
	 * 
	 * @param value
	 *            allowed object is {@link AutoLockSettings }
	 * 
	 */
	public void setSettings(AutoLockSettings value) {
		this.settings = value;
	}
}
