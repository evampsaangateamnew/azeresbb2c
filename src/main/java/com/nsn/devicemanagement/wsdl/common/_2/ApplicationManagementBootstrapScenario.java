package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for applicationManagementBootstrapScenario complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="applicationManagementBootstrapScenario">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Scenario">
 *       &lt;sequence>
 *         &lt;element name="applicationManagementMethod" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}applicationManagementMethod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "applicationManagementBootstrapScenario", propOrder = { "applicationManagementMethod" })
public class ApplicationManagementBootstrapScenario extends Scenario {
	protected ApplicationManagementMethod applicationManagementMethod;

	/**
	 * Gets the value of the applicationManagementMethod property.
	 * 
	 * @return possible object is {@link ApplicationManagementMethod }
	 * 
	 */
	public ApplicationManagementMethod getApplicationManagementMethod() {
		return applicationManagementMethod;
	}

	/**
	 * Sets the value of the applicationManagementMethod property.
	 * 
	 * @param value
	 *            allowed object is {@link ApplicationManagementMethod }
	 * 
	 */
	public void setApplicationManagementMethod(ApplicationManagementMethod value) {
		this.applicationManagementMethod = value;
	}
}
