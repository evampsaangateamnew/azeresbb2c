package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DeviceEnableSecurityAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="DeviceEnableSecurityAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="method" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}deviceSecurityMethod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceEnableSecurityAction", propOrder = { "method" })
public class DeviceEnableSecurityAction extends Action {
	protected DeviceSecurityMethod method;

	/**
	 * Gets the value of the method property.
	 * 
	 * @return possible object is {@link DeviceSecurityMethod }
	 * 
	 */
	public DeviceSecurityMethod getMethod() {
		return method;
	}

	/**
	 * Sets the value of the method property.
	 * 
	 * @param value
	 *            allowed object is {@link DeviceSecurityMethod }
	 * 
	 */
	public void setMethod(DeviceSecurityMethod value) {
		this.method = value;
	}
}
