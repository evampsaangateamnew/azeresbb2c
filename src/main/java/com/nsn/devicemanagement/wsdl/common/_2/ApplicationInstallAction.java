package com.nsn.devicemanagement.wsdl.common._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.nsn.devicemanagement.wsdl.bulkmanagement._2.ContentDownloadMethod;

/**
 * <p>
 * Java class for ApplicationInstallAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationInstallAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="packID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="methods" type="{http://nsn.com/DeviceManagement/wsdl/BulkManagement/2.0}ContentDownloadMethod" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="indicationMessage" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message" minOccurs="0"/>
 *         &lt;element name="interactionLevel" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}InteractionLevel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationInstallAction", propOrder = { "packID", "methods", "indicationMessage",
		"interactionLevel" })
@XmlSeeAlso({ WapPush.class })
public class ApplicationInstallAction extends Action {
	protected long packID;
	@XmlSchemaType(name = "string")
	protected List<ContentDownloadMethod> methods;
	protected Message indicationMessage;
	@XmlSchemaType(name = "string")
	protected InteractionLevel interactionLevel;

	/**
	 * Gets the value of the packID property.
	 * 
	 */
	public long getPackID() {
		return packID;
	}

	/**
	 * Sets the value of the packID property.
	 * 
	 */
	public void setPackID(long value) {
		this.packID = value;
	}

	/**
	 * Gets the value of the methods property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the methods property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getMethods().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ContentDownloadMethod }
	 * 
	 * 
	 */
	public List<ContentDownloadMethod> getMethods() {
		if (methods == null) {
			methods = new ArrayList<ContentDownloadMethod>();
		}
		return this.methods;
	}

	/**
	 * Gets the value of the indicationMessage property.
	 * 
	 * @return possible object is {@link Message }
	 * 
	 */
	public Message getIndicationMessage() {
		return indicationMessage;
	}

	/**
	 * Sets the value of the indicationMessage property.
	 * 
	 * @param value
	 *            allowed object is {@link Message }
	 * 
	 */
	public void setIndicationMessage(Message value) {
		this.indicationMessage = value;
	}

	/**
	 * Gets the value of the interactionLevel property.
	 * 
	 * @return possible object is {@link InteractionLevel }
	 * 
	 */
	public InteractionLevel getInteractionLevel() {
		return interactionLevel;
	}

	/**
	 * Sets the value of the interactionLevel property.
	 * 
	 * @param value
	 *            allowed object is {@link InteractionLevel }
	 * 
	 */
	public void setInteractionLevel(InteractionLevel value) {
		this.interactionLevel = value;
	}
}
