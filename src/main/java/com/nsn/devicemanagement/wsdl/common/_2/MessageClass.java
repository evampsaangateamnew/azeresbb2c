package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MessageClass.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="MessageClass">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PERSONAL"/>
 *     &lt;enumeration value="INFORMATIONAL"/>
 *     &lt;enumeration value="ADVERTISEMENT"/>
 *     &lt;enumeration value="AUTO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MessageClass")
@XmlEnum
public enum MessageClass {
	PERSONAL, INFORMATIONAL, ADVERTISEMENT, AUTO;
	public String value() {
		return name();
	}

	public static MessageClass fromValue(String v) {
		return valueOf(v);
	}
}
