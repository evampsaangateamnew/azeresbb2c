package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SettingProvisioningScenario complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SettingProvisioningScenario">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Scenario">
 *       &lt;sequence>
 *         &lt;element name="provisioning" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}SettingProvisioning" minOccurs="0"/>
 *         &lt;element name="subscriptionAware" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SettingProvisioningScenario", propOrder = { "provisioning", "subscriptionAware" })
public class SettingProvisioningScenario extends Scenario {
	protected SettingProvisioning provisioning;
	protected boolean subscriptionAware;

	/**
	 * Gets the value of the provisioning property.
	 * 
	 * @return possible object is {@link SettingProvisioning }
	 * 
	 */
	public SettingProvisioning getProvisioning() {
		return provisioning;
	}

	/**
	 * Sets the value of the provisioning property.
	 * 
	 * @param value
	 *            allowed object is {@link SettingProvisioning }
	 * 
	 */
	public void setProvisioning(SettingProvisioning value) {
		this.provisioning = value;
	}

	/**
	 * Gets the value of the subscriptionAware property.
	 * 
	 */
	public boolean isSubscriptionAware() {
		return subscriptionAware;
	}

	/**
	 * Sets the value of the subscriptionAware property.
	 * 
	 */
	public void setSubscriptionAware(boolean value) {
		this.subscriptionAware = value;
	}
}
