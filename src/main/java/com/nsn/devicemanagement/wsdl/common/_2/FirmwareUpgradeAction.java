package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for FirmwareUpgradeAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="FirmwareUpgradeAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="firmwareID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FirmwareUpgradeAction", propOrder = { "firmwareID" })
public class FirmwareUpgradeAction extends Action {
	protected String firmwareID;

	/**
	 * Gets the value of the firmwareID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFirmwareID() {
		return firmwareID;
	}

	/**
	 * Sets the value of the firmwareID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFirmwareID(String value) {
		this.firmwareID = value;
	}
}
