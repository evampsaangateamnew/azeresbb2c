package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SendMmsAction complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SendMmsAction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Action">
 *       &lt;sequence>
 *         &lt;element name="mmsContent" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}MmsContent" minOccurs="0"/>
 *         &lt;element name="mmsSpecFeatures" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}MmsSpecFeatures" minOccurs="0"/>
 *         &lt;element name="mmsStdFeatures" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}MmsStdFeatures" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendMmsAction", propOrder = { "mmsContent", "mmsSpecFeatures", "mmsStdFeatures" })
public class SendMmsAction extends Action {
	protected MmsContent mmsContent;
	protected MmsSpecFeatures mmsSpecFeatures;
	protected MmsStdFeatures mmsStdFeatures;

	/**
	 * Gets the value of the mmsContent property.
	 * 
	 * @return possible object is {@link MmsContent }
	 * 
	 */
	public MmsContent getMmsContent() {
		return mmsContent;
	}

	/**
	 * Sets the value of the mmsContent property.
	 * 
	 * @param value
	 *            allowed object is {@link MmsContent }
	 * 
	 */
	public void setMmsContent(MmsContent value) {
		this.mmsContent = value;
	}

	/**
	 * Gets the value of the mmsSpecFeatures property.
	 * 
	 * @return possible object is {@link MmsSpecFeatures }
	 * 
	 */
	public MmsSpecFeatures getMmsSpecFeatures() {
		return mmsSpecFeatures;
	}

	/**
	 * Sets the value of the mmsSpecFeatures property.
	 * 
	 * @param value
	 *            allowed object is {@link MmsSpecFeatures }
	 * 
	 */
	public void setMmsSpecFeatures(MmsSpecFeatures value) {
		this.mmsSpecFeatures = value;
	}

	/**
	 * Gets the value of the mmsStdFeatures property.
	 * 
	 * @return possible object is {@link MmsStdFeatures }
	 * 
	 */
	public MmsStdFeatures getMmsStdFeatures() {
		return mmsStdFeatures;
	}

	/**
	 * Sets the value of the mmsStdFeatures property.
	 * 
	 * @param value
	 *            allowed object is {@link MmsStdFeatures }
	 * 
	 */
	public void setMmsStdFeatures(MmsStdFeatures value) {
		this.mmsStdFeatures = value;
	}
}
