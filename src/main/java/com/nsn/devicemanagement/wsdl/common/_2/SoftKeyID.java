package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SoftKeyID.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="SoftKeyID">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SOFTKEY_LEFT"/>
 *     &lt;enumeration value="SOFTKEY_RIGHT"/>
 *     &lt;enumeration value="SOFTKEY_IDLE1"/>
 *     &lt;enumeration value="SOFTKEY_IDLE2"/>
 *     &lt;enumeration value="SOFTKEY_IDLE3"/>
 *     &lt;enumeration value="SOFTKEY_IDLE4"/>
 *     &lt;enumeration value="SOFTKEY_IDLE5"/>
 *     &lt;enumeration value="SOFTKEY_IDLE6"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SoftKeyID")
@XmlEnum
public enum SoftKeyID {
	SOFTKEY_LEFT("SOFTKEY_LEFT"), SOFTKEY_RIGHT("SOFTKEY_RIGHT"), @XmlEnumValue("SOFTKEY_IDLE1")
	SOFTKEY_IDLE_1("SOFTKEY_IDLE1"), @XmlEnumValue("SOFTKEY_IDLE2")
	SOFTKEY_IDLE_2("SOFTKEY_IDLE2"), @XmlEnumValue("SOFTKEY_IDLE3")
	SOFTKEY_IDLE_3("SOFTKEY_IDLE3"), @XmlEnumValue("SOFTKEY_IDLE4")
	SOFTKEY_IDLE_4("SOFTKEY_IDLE4"), @XmlEnumValue("SOFTKEY_IDLE5")
	SOFTKEY_IDLE_5("SOFTKEY_IDLE5"), @XmlEnumValue("SOFTKEY_IDLE6")
	SOFTKEY_IDLE_6("SOFTKEY_IDLE6");
	private final String value;

	SoftKeyID(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static SoftKeyID fromValue(String v) {
		for (SoftKeyID c : SoftKeyID.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
