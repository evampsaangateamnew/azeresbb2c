package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for invitationMessage complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="invitationMessage">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message">
 *       &lt;sequence>
 *         &lt;element name="standalone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "invitationMessage", propOrder = { "standalone" })
public class InvitationMessage extends Message {
	protected boolean standalone;

	/**
	 * Gets the value of the standalone property.
	 * 
	 */
	public boolean isStandalone() {
		return standalone;
	}

	/**
	 * Sets the value of the standalone property.
	 * 
	 */
	public void setStandalone(boolean value) {
		this.standalone = value;
	}
}
