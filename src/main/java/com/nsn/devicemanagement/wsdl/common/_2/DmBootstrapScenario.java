package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for dmBootstrapScenario complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="dmBootstrapScenario">
 *   &lt;complexContent>
 *     &lt;extension base="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}Scenario">
 *       &lt;sequence>
 *         &lt;element name="connectivity" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}setting" minOccurs="0"/>
 *         &lt;element name="displayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serverURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serverPort" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="autoNotif" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="notification" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}message" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dmBootstrapScenario", propOrder = { "connectivity", "displayName", "serverURL", "serverPort",
		"autoNotif", "notification" })
public class DmBootstrapScenario extends Scenario {
	protected Setting connectivity;
	protected String displayName;
	protected String serverURL;
	protected int serverPort;
	protected boolean autoNotif;
	protected Message notification;

	/**
	 * Gets the value of the connectivity property.
	 * 
	 * @return possible object is {@link Setting }
	 * 
	 */
	public Setting getConnectivity() {
		return connectivity;
	}

	/**
	 * Sets the value of the connectivity property.
	 * 
	 * @param value
	 *            allowed object is {@link Setting }
	 * 
	 */
	public void setConnectivity(Setting value) {
		this.connectivity = value;
	}

	/**
	 * Gets the value of the displayName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Sets the value of the displayName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDisplayName(String value) {
		this.displayName = value;
	}

	/**
	 * Gets the value of the serverURL property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServerURL() {
		return serverURL;
	}

	/**
	 * Sets the value of the serverURL property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServerURL(String value) {
		this.serverURL = value;
	}

	/**
	 * Gets the value of the serverPort property.
	 * 
	 */
	public int getServerPort() {
		return serverPort;
	}

	/**
	 * Sets the value of the serverPort property.
	 * 
	 */
	public void setServerPort(int value) {
		this.serverPort = value;
	}

	/**
	 * Gets the value of the autoNotif property.
	 * 
	 */
	public boolean isAutoNotif() {
		return autoNotif;
	}

	/**
	 * Sets the value of the autoNotif property.
	 * 
	 */
	public void setAutoNotif(boolean value) {
		this.autoNotif = value;
	}

	/**
	 * Gets the value of the notification property.
	 * 
	 * @return possible object is {@link Message }
	 * 
	 */
	public Message getNotification() {
		return notification;
	}

	/**
	 * Sets the value of the notification property.
	 * 
	 * @param value
	 *            allowed object is {@link Message }
	 * 
	 */
	public void setNotification(Message value) {
		this.notification = value;
	}
}
