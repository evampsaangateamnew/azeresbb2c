package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Device complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Device">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="imei" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deviceType" type="{http://nsn.com/DeviceManagement/wsdl/Common/2.0}gsmPhoneType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Device", propOrder = { "imei", "deviceType" })
public class Device {
	protected String imei;
	protected GsmPhoneType deviceType;

	/**
	 * Gets the value of the imei property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getImei() {
		return imei;
	}

	/**
	 * Sets the value of the imei property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setImei(String value) {
		this.imei = value;
	}

	/**
	 * Gets the value of the deviceType property.
	 * 
	 * @return possible object is {@link GsmPhoneType }
	 * 
	 */
	public GsmPhoneType getDeviceType() {
		return deviceType;
	}

	/**
	 * Sets the value of the deviceType property.
	 * 
	 * @param value
	 *            allowed object is {@link GsmPhoneType }
	 * 
	 */
	public void setDeviceType(GsmPhoneType value) {
		this.deviceType = value;
	}
}
