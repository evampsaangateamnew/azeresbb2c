package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MmsSpecFeatures complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="MmsSpecFeatures">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="standAlone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MmsSpecFeatures", propOrder = { "standAlone" })
public class MmsSpecFeatures {
	protected boolean standAlone;

	/**
	 * Gets the value of the standAlone property.
	 * 
	 */
	public boolean isStandAlone() {
		return standAlone;
	}

	/**
	 * Sets the value of the standAlone property.
	 * 
	 */
	public void setStandAlone(boolean value) {
		this.standAlone = value;
	}
}
