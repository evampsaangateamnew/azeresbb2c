package com.nsn.devicemanagement.wsdl.common._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.nsn.devicemanagement.wsdl.common._2
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	private final static QName _DMException_QNAME = new QName("http://nsn.com/DeviceManagement/wsdl/Common/2.0",
			"DMException");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.nsn.devicemanagement.wsdl.common._2
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link GeneralPersonalParams }
	 * 
	 */
	public GeneralPersonalParams createGeneralPersonalParams() {
		return new GeneralPersonalParams();
	}

	/**
	 * Create an instance of {@link GeneralPersonalParams.Parameter }
	 * 
	 */
	public GeneralPersonalParams.Parameter createGeneralPersonalParamsParameter() {
		return new GeneralPersonalParams.Parameter();
	}

	/**
	 * Create an instance of {@link DmMap }
	 * 
	 */
	public DmMap createDmMap() {
		return new DmMap();
	}

	/**
	 * Create an instance of {@link DmMap.HashTable }
	 * 
	 */
	public DmMap.HashTable createDmMapHashTable() {
		return new DmMap.HashTable();
	}

	/**
	 * Create an instance of {@link DMException }
	 * 
	 */
	public DMException createDMException() {
		return new DMException();
	}

	/**
	 * Create an instance of {@link APNProvisioningAction }
	 * 
	 */
	public APNProvisioningAction createAPNProvisioningAction() {
		return new APNProvisioningAction();
	}

	/**
	 * Create an instance of {@link ApplicationManagementBootstrapScenario }
	 * 
	 */
	public ApplicationManagementBootstrapScenario createApplicationManagementBootstrapScenario() {
		return new ApplicationManagementBootstrapScenario();
	}

	/**
	 * Create an instance of {@link ApplicationStartAction }
	 * 
	 */
	public ApplicationStartAction createApplicationStartAction() {
		return new ApplicationStartAction();
	}

	/**
	 * Create an instance of {@link MmsSpecFeatures }
	 * 
	 */
	public MmsSpecFeatures createMmsSpecFeatures() {
		return new MmsSpecFeatures();
	}

	/**
	 * Create an instance of {@link SoftKeyAssignmentAction }
	 * 
	 */
	public SoftKeyAssignmentAction createSoftKeyAssignmentAction() {
		return new SoftKeyAssignmentAction();
	}

	/**
	 * Create an instance of {@link NcsScenario }
	 * 
	 */
	public NcsScenario createNcsScenario() {
		return new NcsScenario();
	}

	/**
	 * Create an instance of {@link SendAutoLockSettingsAction }
	 * 
	 */
	public SendAutoLockSettingsAction createSendAutoLockSettingsAction() {
		return new SendAutoLockSettingsAction();
	}

	/**
	 * Create an instance of {@link Setting }
	 * 
	 */
	public Setting createSetting() {
		return new Setting();
	}

	/**
	 * Create an instance of {@link DmList }
	 * 
	 */
	public DmList createDmList() {
		return new DmList();
	}

	/**
	 * Create an instance of {@link SettingProvisioningScenario }
	 * 
	 */
	public SettingProvisioningScenario createSettingProvisioningScenario() {
		return new SettingProvisioningScenario();
	}

	/**
	 * Create an instance of {@link ApplicationUninstallAction }
	 * 
	 */
	public ApplicationUninstallAction createApplicationUninstallAction() {
		return new ApplicationUninstallAction();
	}

	/**
	 * Create an instance of {@link WapPush }
	 * 
	 */
	public WapPush createWapPush() {
		return new WapPush();
	}

	/**
	 * Create an instance of {@link ApplicationStopAction }
	 * 
	 */
	public ApplicationStopAction createApplicationStopAction() {
		return new ApplicationStopAction();
	}

	/**
	 * Create an instance of {@link SendAdminPasswordAction }
	 * 
	 */
	public SendAdminPasswordAction createSendAdminPasswordAction() {
		return new SendAdminPasswordAction();
	}

	/**
	 * Create an instance of {@link SyncRestoreAction }
	 * 
	 */
	public SyncRestoreAction createSyncRestoreAction() {
		return new SyncRestoreAction();
	}

	/**
	 * Create an instance of {@link MmsContent }
	 * 
	 */
	public MmsContent createMmsContent() {
		return new MmsContent();
	}

	/**
	 * Create an instance of {@link SettingVerificationAction }
	 * 
	 */
	public SettingVerificationAction createSettingVerificationAction() {
		return new SettingVerificationAction();
	}

	/**
	 * Create an instance of {@link Subscription }
	 * 
	 */
	public Subscription createSubscription() {
		return new Subscription();
	}

	/**
	 * Create an instance of {@link ApplicationInstallAction }
	 * 
	 */
	public ApplicationInstallAction createApplicationInstallAction() {
		return new ApplicationInstallAction();
	}

	/**
	 * Create an instance of {@link ConnectivityBootstrapScenario }
	 * 
	 */
	public ConnectivityBootstrapScenario createConnectivityBootstrapScenario() {
		return new ConnectivityBootstrapScenario();
	}

	/**
	 * Create an instance of {@link DeviceEnableSecurityAction }
	 * 
	 */
	public DeviceEnableSecurityAction createDeviceEnableSecurityAction() {
		return new DeviceEnableSecurityAction();
	}

	/**
	 * Create an instance of {@link FileDownloadAction }
	 * 
	 */
	public FileDownloadAction createFileDownloadAction() {
		return new FileDownloadAction();
	}

	/**
	 * Create an instance of {@link Device }
	 * 
	 */
	public Device createDevice() {
		return new Device();
	}

	/**
	 * Create an instance of {@link SettingDeleteAction }
	 * 
	 */
	public SettingDeleteAction createSettingDeleteAction() {
		return new SettingDeleteAction();
	}

	/**
	 * Create an instance of {@link AccessPointNamesAction }
	 * 
	 */
	public AccessPointNamesAction createAccessPointNamesAction() {
		return new AccessPointNamesAction();
	}

	/**
	 * Create an instance of {@link ScenarioInvocationAction }
	 * 
	 */
	public ScenarioInvocationAction createScenarioInvocationAction() {
		return new ScenarioInvocationAction();
	}

	/**
	 * Create an instance of {@link DefaultScenario }
	 * 
	 */
	public DefaultScenario createDefaultScenario() {
		return new DefaultScenario();
	}

	/**
	 * Create an instance of {@link MmsStdFeatures }
	 * 
	 */
	public MmsStdFeatures createMmsStdFeatures() {
		return new MmsStdFeatures();
	}

	/**
	 * Create an instance of {@link SequenceScenario }
	 * 
	 */
	public SequenceScenario createSequenceScenario() {
		return new SequenceScenario();
	}

	/**
	 * Create an instance of {@link PoCPersonalParams }
	 * 
	 */
	public PoCPersonalParams createPoCPersonalParams() {
		return new PoCPersonalParams();
	}

	/**
	 * Create an instance of {@link FirmwareUpgradeAction }
	 * 
	 */
	public FirmwareUpgradeAction createFirmwareUpgradeAction() {
		return new FirmwareUpgradeAction();
	}

	/**
	 * Create an instance of {@link NamedScenario }
	 * 
	 */
	public NamedScenario createNamedScenario() {
		return new NamedScenario();
	}

	/**
	 * Create an instance of {@link DeviceSecurityBootstrapScenario }
	 * 
	 */
	public DeviceSecurityBootstrapScenario createDeviceSecurityBootstrapScenario() {
		return new DeviceSecurityBootstrapScenario();
	}

	/**
	 * Create an instance of {@link File }
	 * 
	 */
	public File createFile() {
		return new File();
	}

	/**
	 * Create an instance of {@link LocaleWrapper }
	 * 
	 */
	public LocaleWrapper createLocaleWrapper() {
		return new LocaleWrapper();
	}

	/**
	 * Create an instance of {@link DeviceLockAction }
	 * 
	 */
	public DeviceLockAction createDeviceLockAction() {
		return new DeviceLockAction();
	}

	/**
	 * Create an instance of {@link DeviceSecurityMethod }
	 * 
	 */
	public DeviceSecurityMethod createDeviceSecurityMethod() {
		return new DeviceSecurityMethod();
	}

	/**
	 * Create an instance of {@link ApplicationManagementMethod }
	 * 
	 */
	public ApplicationManagementMethod createApplicationManagementMethod() {
		return new ApplicationManagementMethod();
	}

	/**
	 * Create an instance of {@link ContentMeta }
	 * 
	 */
	public ContentMeta createContentMeta() {
		return new ContentMeta();
	}

	/**
	 * Create an instance of {@link SecuritySettingsScanAction }
	 * 
	 */
	public SecuritySettingsScanAction createSecuritySettingsScanAction() {
		return new SecuritySettingsScanAction();
	}

	/**
	 * Create an instance of {@link GsmPhoneType }
	 * 
	 */
	public GsmPhoneType createGsmPhoneType() {
		return new GsmPhoneType();
	}

	/**
	 * Create an instance of {@link FirmwareScanAction }
	 * 
	 */
	public FirmwareScanAction createFirmwareScanAction() {
		return new FirmwareScanAction();
	}

	/**
	 * Create an instance of {@link EnablePolicyControlAction }
	 * 
	 */
	public EnablePolicyControlAction createEnablePolicyControlAction() {
		return new EnablePolicyControlAction();
	}

	/**
	 * Create an instance of {@link InvitationMessage }
	 * 
	 */
	public InvitationMessage createInvitationMessage() {
		return new InvitationMessage();
	}

	/**
	 * Create an instance of {@link DevDetailScanAction }
	 * 
	 */
	public DevDetailScanAction createDevDetailScanAction() {
		return new DevDetailScanAction();
	}

	/**
	 * Create an instance of {@link SendMmsAction }
	 * 
	 */
	public SendMmsAction createSendMmsAction() {
		return new SendMmsAction();
	}

	/**
	 * Create an instance of {@link SettingProvisioning }
	 * 
	 */
	public SettingProvisioning createSettingProvisioning() {
		return new SettingProvisioning();
	}

	/**
	 * Create an instance of {@link SettingScanAction }
	 * 
	 */
	public SettingScanAction createSettingScanAction() {
		return new SettingScanAction();
	}

	/**
	 * Create an instance of {@link RcBootstrapScenario }
	 * 
	 */
	public RcBootstrapScenario createRcBootstrapScenario() {
		return new RcBootstrapScenario();
	}

	/**
	 * Create an instance of {@link SendPasswordPolicySettingsAction }
	 * 
	 */
	public SendPasswordPolicySettingsAction createSendPasswordPolicySettingsAction() {
		return new SendPasswordPolicySettingsAction();
	}

	/**
	 * Create an instance of {@link Message }
	 * 
	 */
	public Message createMessage() {
		return new Message();
	}

	/**
	 * Create an instance of {@link InteractionMessageAction }
	 * 
	 */
	public InteractionMessageAction createInteractionMessageAction() {
		return new InteractionMessageAction();
	}

	/**
	 * Create an instance of {@link SyncBackupActionAction }
	 * 
	 */
	public SyncBackupActionAction createSyncBackupActionAction() {
		return new SyncBackupActionAction();
	}

	/**
	 * Create an instance of {@link DeviceEnableOTAAction }
	 * 
	 */
	public DeviceEnableOTAAction createDeviceEnableOTAAction() {
		return new DeviceEnableOTAAction();
	}

	/**
	 * Create an instance of {@link ApplicationScanAction }
	 * 
	 */
	public ApplicationScanAction createApplicationScanAction() {
		return new ApplicationScanAction();
	}

	/**
	 * Create an instance of {@link SettingDeleteScannedAction }
	 * 
	 */
	public SettingDeleteScannedAction createSettingDeleteScannedAction() {
		return new SettingDeleteScannedAction();
	}

	/**
	 * Create an instance of {@link ContentId }
	 * 
	 */
	public ContentId createContentId() {
		return new ContentId();
	}

	/**
	 * Create an instance of {@link DmBootstrapScenario }
	 * 
	 */
	public DmBootstrapScenario createDmBootstrapScenario() {
		return new DmBootstrapScenario();
	}

	/**
	 * Create an instance of {@link BlacklistedScenario }
	 * 
	 */
	public BlacklistedScenario createBlacklistedScenario() {
		return new BlacklistedScenario();
	}

	/**
	 * Create an instance of {@link DeviceDataWipeAction }
	 * 
	 */
	public DeviceDataWipeAction createDeviceDataWipeAction() {
		return new DeviceDataWipeAction();
	}

	/**
	 * Create an instance of {@link GeneralPersonalParams.Parameter.Entry }
	 * 
	 */
	public GeneralPersonalParams.Parameter.Entry createGeneralPersonalParamsParameterEntry() {
		return new GeneralPersonalParams.Parameter.Entry();
	}

	/**
	 * Create an instance of {@link DmMap.HashTable.Entry }
	 * 
	 */
	public DmMap.HashTable.Entry createDmMapHashTableEntry() {
		return new DmMap.HashTable.Entry();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DMException
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Common/2.0", name = "DMException")
	public JAXBElement<DMException> createDMException(DMException value) {
		return new JAXBElement<DMException>(_DMException_QNAME, DMException.class, null, value);
	}
}
