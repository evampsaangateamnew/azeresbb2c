package com.nsn.devicemanagement.wsdl.provisioning._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.nsn.devicemanagement.wsdl.common._2.DMException;
import com.nsn.devicemanagement.wsdl.common._2.SettingDeleteScannedAction;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * com.nsn.devicemanagement.wsdl.provisioning._2 package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	private final static QName _SubmitNetworkTriggerResponse_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "submitNetworkTriggerResponse");
	private final static QName _SubmitProvisionRequestResponse_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "submitProvisionRequestResponse");
	private final static QName _SubmitProvisionReqResponse_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "submitProvisionReqResponse");
	private final static QName _GetNCSProvisioningDocument_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "getNCSProvisioningDocument");
	private final static QName _GetNCSProvisioningDocumentResponse_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "getNCSProvisioningDocumentResponse");
	private final static QName _SubmitProvisioningRequest_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "submitProvisioningRequest");
	private final static QName _GetScenario_QNAME = new QName("http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0",
			"getScenario");
	private final static QName _DMException_QNAME = new QName("http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0",
			"DMException");
	private final static QName _GetScenarioNames_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "getScenarioNames");
	private final static QName _SettingDeleteScanned_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "settingDeleteScanned");
	private final static QName _GetScenarioNamesResponse_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "getScenarioNamesResponse");
	private final static QName _SubmitProvisionRequest_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "submitProvisionRequest");
	private final static QName _GetCustomProvisionedServices_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "getCustomProvisionedServices");
	private final static QName _SubmitProvisionReq_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "submitProvisionReq");
	private final static QName _SubmitProvisioningRequestResponse_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "submitProvisioningRequestResponse");
	private final static QName _GetCustomProvisionedServicesResponse_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "getCustomProvisionedServicesResponse");
	private final static QName _SubmitNetworkTrigger_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "submitNetworkTrigger");
	private final static QName _GetScenarioResponse_QNAME = new QName(
			"http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", "getScenarioResponse");
	private final static QName _GetNCSProvisioningDocumentResponseReturn_QNAME = new QName("", "return");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.nsn.devicemanagement.wsdl.provisioning._2
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link GetCustomProvisionedServices }
	 * 
	 */
	public GetCustomProvisionedServices createGetCustomProvisionedServices() {
		return new GetCustomProvisionedServices();
	}

	/**
	 * Create an instance of {@link SubmitProvisionRequest }
	 * 
	 */
	public SubmitProvisionRequest createSubmitProvisionRequest() {
		return new SubmitProvisionRequest();
	}

	/**
	 * Create an instance of {@link SubmitProvisionReq }
	 * 
	 */
	public SubmitProvisionReq createSubmitProvisionReq() {
		return new SubmitProvisionReq();
	}

	/**
	 * Create an instance of {@link SubmitProvisioningRequestResponse }
	 * 
	 */
	public SubmitProvisioningRequestResponse createSubmitProvisioningRequestResponse() {
		return new SubmitProvisioningRequestResponse();
	}

	/**
	 * Create an instance of {@link GetScenarioResponse }
	 * 
	 */
	public GetScenarioResponse createGetScenarioResponse() {
		return new GetScenarioResponse();
	}

	/**
	 * Create an instance of {@link GetCustomProvisionedServicesResponse }
	 * 
	 */
	public GetCustomProvisionedServicesResponse createGetCustomProvisionedServicesResponse() {
		return new GetCustomProvisionedServicesResponse();
	}

	/**
	 * Create an instance of {@link SubmitNetworkTrigger }
	 * 
	 */
	public SubmitNetworkTrigger createSubmitNetworkTrigger() {
		return new SubmitNetworkTrigger();
	}

	/**
	 * Create an instance of {@link SubmitProvisionRequestResponse }
	 * 
	 */
	public SubmitProvisionRequestResponse createSubmitProvisionRequestResponse() {
		return new SubmitProvisionRequestResponse();
	}

	/**
	 * Create an instance of {@link SubmitNetworkTriggerResponse }
	 * 
	 */
	public SubmitNetworkTriggerResponse createSubmitNetworkTriggerResponse() {
		return new SubmitNetworkTriggerResponse();
	}

	/**
	 * Create an instance of {@link GetNCSProvisioningDocument }
	 * 
	 */
	public GetNCSProvisioningDocument createGetNCSProvisioningDocument() {
		return new GetNCSProvisioningDocument();
	}

	/**
	 * Create an instance of {@link GetNCSProvisioningDocumentResponse }
	 * 
	 */
	public GetNCSProvisioningDocumentResponse createGetNCSProvisioningDocumentResponse() {
		return new GetNCSProvisioningDocumentResponse();
	}

	/**
	 * Create an instance of {@link SubmitProvisionReqResponse }
	 * 
	 */
	public SubmitProvisionReqResponse createSubmitProvisionReqResponse() {
		return new SubmitProvisionReqResponse();
	}

	/**
	 * Create an instance of {@link GetScenario }
	 * 
	 */
	public GetScenario createGetScenario() {
		return new GetScenario();
	}

	/**
	 * Create an instance of {@link SubmitProvisioningRequest }
	 * 
	 */
	public SubmitProvisioningRequest createSubmitProvisioningRequest() {
		return new SubmitProvisioningRequest();
	}

	/**
	 * Create an instance of {@link GetScenarioNamesResponse }
	 * 
	 */
	public GetScenarioNamesResponse createGetScenarioNamesResponse() {
		return new GetScenarioNamesResponse();
	}

	/**
	 * Create an instance of {@link GetScenarioNames }
	 * 
	 */
	public GetScenarioNames createGetScenarioNames() {
		return new GetScenarioNames();
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SubmitNetworkTriggerResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "submitNetworkTriggerResponse")
	public JAXBElement<SubmitNetworkTriggerResponse> createSubmitNetworkTriggerResponse(
			SubmitNetworkTriggerResponse value) {
		return new JAXBElement<SubmitNetworkTriggerResponse>(_SubmitNetworkTriggerResponse_QNAME,
				SubmitNetworkTriggerResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SubmitProvisionRequestResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "submitProvisionRequestResponse")
	public JAXBElement<SubmitProvisionRequestResponse> createSubmitProvisionRequestResponse(
			SubmitProvisionRequestResponse value) {
		return new JAXBElement<SubmitProvisionRequestResponse>(_SubmitProvisionRequestResponse_QNAME,
				SubmitProvisionRequestResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SubmitProvisionReqResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "submitProvisionReqResponse")
	public JAXBElement<SubmitProvisionReqResponse> createSubmitProvisionReqResponse(SubmitProvisionReqResponse value) {
		return new JAXBElement<SubmitProvisionReqResponse>(_SubmitProvisionReqResponse_QNAME,
				SubmitProvisionReqResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link GetNCSProvisioningDocument }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "getNCSProvisioningDocument")
	public JAXBElement<GetNCSProvisioningDocument> createGetNCSProvisioningDocument(GetNCSProvisioningDocument value) {
		return new JAXBElement<GetNCSProvisioningDocument>(_GetNCSProvisioningDocument_QNAME,
				GetNCSProvisioningDocument.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link GetNCSProvisioningDocumentResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "getNCSProvisioningDocumentResponse")
	public JAXBElement<GetNCSProvisioningDocumentResponse> createGetNCSProvisioningDocumentResponse(
			GetNCSProvisioningDocumentResponse value) {
		return new JAXBElement<GetNCSProvisioningDocumentResponse>(_GetNCSProvisioningDocumentResponse_QNAME,
				GetNCSProvisioningDocumentResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SubmitProvisioningRequest }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "submitProvisioningRequest")
	public JAXBElement<SubmitProvisioningRequest> createSubmitProvisioningRequest(SubmitProvisioningRequest value) {
		return new JAXBElement<SubmitProvisioningRequest>(_SubmitProvisioningRequest_QNAME,
				SubmitProvisioningRequest.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetScenario
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "getScenario")
	public JAXBElement<GetScenario> createGetScenario(GetScenario value) {
		return new JAXBElement<GetScenario>(_GetScenario_QNAME, GetScenario.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DMException
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "DMException")
	public JAXBElement<DMException> createDMException(DMException value) {
		return new JAXBElement<DMException>(_DMException_QNAME, DMException.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link GetScenarioNames }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "getScenarioNames")
	public JAXBElement<GetScenarioNames> createGetScenarioNames(GetScenarioNames value) {
		return new JAXBElement<GetScenarioNames>(_GetScenarioNames_QNAME, GetScenarioNames.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SettingDeleteScannedAction }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "settingDeleteScanned")
	public JAXBElement<SettingDeleteScannedAction> createSettingDeleteScanned(SettingDeleteScannedAction value) {
		return new JAXBElement<SettingDeleteScannedAction>(_SettingDeleteScanned_QNAME,
				SettingDeleteScannedAction.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link GetScenarioNamesResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "getScenarioNamesResponse")
	public JAXBElement<GetScenarioNamesResponse> createGetScenarioNamesResponse(GetScenarioNamesResponse value) {
		return new JAXBElement<GetScenarioNamesResponse>(_GetScenarioNamesResponse_QNAME,
				GetScenarioNamesResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SubmitProvisionRequest }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "submitProvisionRequest")
	public JAXBElement<SubmitProvisionRequest> createSubmitProvisionRequest(SubmitProvisionRequest value) {
		return new JAXBElement<SubmitProvisionRequest>(_SubmitProvisionRequest_QNAME, SubmitProvisionRequest.class,
				null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link GetCustomProvisionedServices }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "getCustomProvisionedServices")
	public JAXBElement<GetCustomProvisionedServices> createGetCustomProvisionedServices(
			GetCustomProvisionedServices value) {
		return new JAXBElement<GetCustomProvisionedServices>(_GetCustomProvisionedServices_QNAME,
				GetCustomProvisionedServices.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SubmitProvisionReq }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "submitProvisionReq")
	public JAXBElement<SubmitProvisionReq> createSubmitProvisionReq(SubmitProvisionReq value) {
		return new JAXBElement<SubmitProvisionReq>(_SubmitProvisionReq_QNAME, SubmitProvisionReq.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SubmitProvisioningRequestResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "submitProvisioningRequestResponse")
	public JAXBElement<SubmitProvisioningRequestResponse> createSubmitProvisioningRequestResponse(
			SubmitProvisioningRequestResponse value) {
		return new JAXBElement<SubmitProvisioningRequestResponse>(_SubmitProvisioningRequestResponse_QNAME,
				SubmitProvisioningRequestResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link GetCustomProvisionedServicesResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "getCustomProvisionedServicesResponse")
	public JAXBElement<GetCustomProvisionedServicesResponse> createGetCustomProvisionedServicesResponse(
			GetCustomProvisionedServicesResponse value) {
		return new JAXBElement<GetCustomProvisionedServicesResponse>(_GetCustomProvisionedServicesResponse_QNAME,
				GetCustomProvisionedServicesResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link SubmitNetworkTrigger }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "submitNetworkTrigger")
	public JAXBElement<SubmitNetworkTrigger> createSubmitNetworkTrigger(SubmitNetworkTrigger value) {
		return new JAXBElement<SubmitNetworkTrigger>(_SubmitNetworkTrigger_QNAME, SubmitNetworkTrigger.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link GetScenarioResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://nsn.com/DeviceManagement/wsdl/Provisioning/2.0", name = "getScenarioResponse")
	public JAXBElement<GetScenarioResponse> createGetScenarioResponse(GetScenarioResponse value) {
		return new JAXBElement<GetScenarioResponse>(_GetScenarioResponse_QNAME, GetScenarioResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link byte[]}{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "return", scope = GetNCSProvisioningDocumentResponse.class)
	public JAXBElement<byte[]> createGetNCSProvisioningDocumentResponseReturn(byte[] value) {
		return new JAXBElement<byte[]>(_GetNCSProvisioningDocumentResponseReturn_QNAME, byte[].class,
				GetNCSProvisioningDocumentResponse.class, (value));
	}
}
