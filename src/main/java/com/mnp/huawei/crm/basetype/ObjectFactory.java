
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.crm.basetype package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CorpInfoIDType_QNAME = new QName("http://crm.huawei.com/basetype/", "IDType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.crm.basetype
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HistoryList }
     * 
     */
    public HistoryList createHistoryList() {
        return new HistoryList();
    }

    /**
     * Create an instance of {@link ResponseHeader }
     * 
     */
    public ResponseHeader createResponseHeader() {
        return new ResponseHeader();
    }

    /**
     * Create an instance of {@link RequestHeader }
     * 
     */
    public RequestHeader createRequestHeader() {
        return new RequestHeader();
    }

    /**
     * Create an instance of {@link OfferingInformation }
     * 
     */
    public OfferingInformation createOfferingInformation() {
        return new OfferingInformation();
    }

    /**
     * Create an instance of {@link DealerOrder }
     * 
     */
    public DealerOrder createDealerOrder() {
        return new DealerOrder();
    }

    /**
     * Create an instance of {@link CustomerOrderInfo }
     * 
     */
    public CustomerOrderInfo createCustomerOrderInfo() {
        return new CustomerOrderInfo();
    }

    /**
     * Create an instance of {@link OrderItemResponse }
     * 
     */
    public OrderItemResponse createOrderItemResponse() {
        return new OrderItemResponse();
    }

    /**
     * Create an instance of {@link ContractInfoList }
     * 
     */
    public ContractInfoList createContractInfoList() {
        return new ContractInfoList();
    }

    /**
     * Create an instance of {@link DealerInfo }
     * 
     */
    public DealerInfo createDealerInfo() {
        return new DealerInfo();
    }

    /**
     * Create an instance of {@link ExtProductList }
     * 
     */
    public ExtProductList createExtProductList() {
        return new ExtProductList();
    }

    /**
     * Create an instance of {@link HistoryParameterInfo }
     * 
     */
    public HistoryParameterInfo createHistoryParameterInfo() {
        return new HistoryParameterInfo();
    }

    /**
     * Create an instance of {@link ManagerInfo }
     * 
     */
    public ManagerInfo createManagerInfo() {
        return new ManagerInfo();
    }

    /**
     * Create an instance of {@link ProductInfo }
     * 
     */
    public ProductInfo createProductInfo() {
        return new ProductInfo();
    }

    /**
     * Create an instance of {@link ConsumptionLimit }
     * 
     */
    public ConsumptionLimit createConsumptionLimit() {
        return new ConsumptionLimit();
    }

    /**
     * Create an instance of {@link ManagerList }
     * 
     */
    public ManagerList createManagerList() {
        return new ManagerList();
    }

    /**
     * Create an instance of {@link HistoryParameterList }
     * 
     */
    public HistoryParameterList createHistoryParameterList() {
        return new HistoryParameterList();
    }

    /**
     * Create an instance of {@link OfferingList }
     * 
     */
    public OfferingList createOfferingList() {
        return new OfferingList();
    }

    /**
     * Create an instance of {@link ServiceInfo }
     * 
     */
    public ServiceInfo createServiceInfo() {
        return new ServiceInfo();
    }

    /**
     * Create an instance of {@link HistoryPropInfo }
     * 
     */
    public HistoryPropInfo createHistoryPropInfo() {
        return new HistoryPropInfo();
    }

    /**
     * Create an instance of {@link OfferingInfo }
     * 
     */
    public OfferingInfo createOfferingInfo() {
        return new OfferingInfo();
    }

    /**
     * Create an instance of {@link ServiceList }
     * 
     */
    public ServiceList createServiceList() {
        return new ServiceList();
    }

    /**
     * Create an instance of {@link RewardItemInfo }
     * 
     */
    public RewardItemInfo createRewardItemInfo() {
        return new RewardItemInfo();
    }

    /**
     * Create an instance of {@link AgreementInfo }
     * 
     */
    public AgreementInfo createAgreementInfo() {
        return new AgreementInfo();
    }

    /**
     * Create an instance of {@link CycleInfo }
     * 
     */
    public CycleInfo createCycleInfo() {
        return new CycleInfo();
    }

    /**
     * Create an instance of {@link EUCutOffSetting }
     * 
     */
    public EUCutOffSetting createEUCutOffSetting() {
        return new EUCutOffSetting();
    }

    /**
     * Create an instance of {@link HistoryPropList }
     * 
     */
    public HistoryPropList createHistoryPropList() {
        return new HistoryPropList();
    }

    /**
     * Create an instance of {@link AddressKeyInfo }
     * 
     */
    public AddressKeyInfo createAddressKeyInfo() {
        return new AddressKeyInfo();
    }

    /**
     * Create an instance of {@link OfferingExtParameterList }
     * 
     */
    public OfferingExtParameterList createOfferingExtParameterList() {
        return new OfferingExtParameterList();
    }

    /**
     * Create an instance of {@link GetOfferingInfo }
     * 
     */
    public GetOfferingInfo createGetOfferingInfo() {
        return new GetOfferingInfo();
    }

    /**
     * Create an instance of {@link CorpCustMember }
     * 
     */
    public CorpCustMember createCorpCustMember() {
        return new CorpCustMember();
    }

    /**
     * Create an instance of {@link TaxInfo }
     * 
     */
    public TaxInfo createTaxInfo() {
        return new TaxInfo();
    }

    /**
     * Create an instance of {@link OfferingIdList }
     * 
     */
    public OfferingIdList createOfferingIdList() {
        return new OfferingIdList();
    }

    /**
     * Create an instance of {@link TaxList }
     * 
     */
    public TaxList createTaxList() {
        return new TaxList();
    }

    /**
     * Create an instance of {@link AddressKeyList }
     * 
     */
    public AddressKeyList createAddressKeyList() {
        return new AddressKeyList();
    }

    /**
     * Create an instance of {@link ContactInfo }
     * 
     */
    public ContactInfo createContactInfo() {
        return new ContactInfo();
    }

    /**
     * Create an instance of {@link OfferingExtParameterInfo }
     * 
     */
    public OfferingExtParameterInfo createOfferingExtParameterInfo() {
        return new OfferingExtParameterInfo();
    }

    /**
     * Create an instance of {@link OrderItemStatusList }
     * 
     */
    public OrderItemStatusList createOrderItemStatusList() {
        return new OrderItemStatusList();
    }

    /**
     * Create an instance of {@link OfferingIdInfo }
     * 
     */
    public OfferingIdInfo createOfferingIdInfo() {
        return new OfferingIdInfo();
    }

    /**
     * Create an instance of {@link GoodsItems }
     * 
     */
    public GoodsItems createGoodsItems() {
        return new GoodsItems();
    }

    /**
     * Create an instance of {@link CustomerDocumentList }
     * 
     */
    public CustomerDocumentList createCustomerDocumentList() {
        return new CustomerDocumentList();
    }

    /**
     * Create an instance of {@link ModCustomerInfo }
     * 
     */
    public ModCustomerInfo createModCustomerInfo() {
        return new ModCustomerInfo();
    }

    /**
     * Create an instance of {@link ContactList }
     * 
     */
    public ContactList createContactList() {
        return new ContactList();
    }

    /**
     * Create an instance of {@link GoodsItemsList }
     * 
     */
    public GoodsItemsList createGoodsItemsList() {
        return new GoodsItemsList();
    }

    /**
     * Create an instance of {@link IndividualInfo }
     * 
     */
    public IndividualInfo createIndividualInfo() {
        return new IndividualInfo();
    }

    /**
     * Create an instance of {@link RewardItemIdList }
     * 
     */
    public RewardItemIdList createRewardItemIdList() {
        return new RewardItemIdList();
    }

    /**
     * Create an instance of {@link RegisterCustInfo }
     * 
     */
    public RegisterCustInfo createRegisterCustInfo() {
        return new RegisterCustInfo();
    }

    /**
     * Create an instance of {@link CorporateInfo }
     * 
     */
    public CorporateInfo createCorporateInfo() {
        return new CorporateInfo();
    }

    /**
     * Create an instance of {@link PasswordInfo }
     * 
     */
    public PasswordInfo createPasswordInfo() {
        return new PasswordInfo();
    }

    /**
     * Create an instance of {@link CustomerDocumentInfo }
     * 
     */
    public CustomerDocumentInfo createCustomerDocumentInfo() {
        return new CustomerDocumentInfo();
    }

    /**
     * Create an instance of {@link OrderItemList }
     * 
     */
    public OrderItemList createOrderItemList() {
        return new OrderItemList();
    }

    /**
     * Create an instance of {@link PaymentRelation }
     * 
     */
    public PaymentRelation createPaymentRelation() {
        return new PaymentRelation();
    }

    /**
     * Create an instance of {@link WorkOrderDetail }
     * 
     */
    public WorkOrderDetail createWorkOrderDetail() {
        return new WorkOrderDetail();
    }

    /**
     * Create an instance of {@link GetOrderItemInfo }
     * 
     */
    public GetOrderItemInfo createGetOrderItemInfo() {
        return new GetOrderItemInfo();
    }

    /**
     * Create an instance of {@link PromotionInfo }
     * 
     */
    public PromotionInfo createPromotionInfo() {
        return new PromotionInfo();
    }

    /**
     * Create an instance of {@link PromotionList }
     * 
     */
    public PromotionList createPromotionList() {
        return new PromotionList();
    }

    /**
     * Create an instance of {@link ConsumptionLimitList }
     * 
     */
    public ConsumptionLimitList createConsumptionLimitList() {
        return new ConsumptionLimitList();
    }

    /**
     * Create an instance of {@link FamilyMemberInfo }
     * 
     */
    public FamilyMemberInfo createFamilyMemberInfo() {
        return new FamilyMemberInfo();
    }

    /**
     * Create an instance of {@link CartItemInfo }
     * 
     */
    public CartItemInfo createCartItemInfo() {
        return new CartItemInfo();
    }

    /**
     * Create an instance of {@link CorpMemberList }
     * 
     */
    public CorpMemberList createCorpMemberList() {
        return new CorpMemberList();
    }

    /**
     * Create an instance of {@link InvoiceNoList }
     * 
     */
    public InvoiceNoList createInvoiceNoList() {
        return new InvoiceNoList();
    }

    /**
     * Create an instance of {@link CustInfo }
     * 
     */
    public CustInfo createCustInfo() {
        return new CustInfo();
    }

    /**
     * Create an instance of {@link CorpMemberInfo }
     * 
     */
    public CorpMemberInfo createCorpMemberInfo() {
        return new CorpMemberInfo();
    }

    /**
     * Create an instance of {@link InvoiceNoInfo }
     * 
     */
    public InvoiceNoInfo createInvoiceNoInfo() {
        return new InvoiceNoInfo();
    }

    /**
     * Create an instance of {@link PointRedeemCondValue }
     * 
     */
    public PointRedeemCondValue createPointRedeemCondValue() {
        return new PointRedeemCondValue();
    }

    /**
     * Create an instance of {@link CartItemList }
     * 
     */
    public CartItemList createCartItemList() {
        return new CartItemList();
    }

    /**
     * Create an instance of {@link CalcOneOffFeeInfoForQuery }
     * 
     */
    public CalcOneOffFeeInfoForQuery createCalcOneOffFeeInfoForQuery() {
        return new CalcOneOffFeeInfoForQuery();
    }

    /**
     * Create an instance of {@link CorpOrgList }
     * 
     */
    public CorpOrgList createCorpOrgList() {
        return new CorpOrgList();
    }

    /**
     * Create an instance of {@link RelationshipAccount }
     * 
     */
    public RelationshipAccount createRelationshipAccount() {
        return new RelationshipAccount();
    }

    /**
     * Create an instance of {@link PointCustCondValue }
     * 
     */
    public PointCustCondValue createPointCustCondValue() {
        return new PointCustCondValue();
    }

    /**
     * Create an instance of {@link CorpOrgInfo }
     * 
     */
    public CorpOrgInfo createCorpOrgInfo() {
        return new CorpOrgInfo();
    }

    /**
     * Create an instance of {@link CommissionDetail }
     * 
     */
    public CommissionDetail createCommissionDetail() {
        return new CommissionDetail();
    }

    /**
     * Create an instance of {@link OfferingKey }
     * 
     */
    public OfferingKey createOfferingKey() {
        return new OfferingKey();
    }

    /**
     * Create an instance of {@link ContractList }
     * 
     */
    public ContractList createContractList() {
        return new ContractList();
    }

    /**
     * Create an instance of {@link DNESettingList }
     * 
     */
    public DNESettingList createDNESettingList() {
        return new DNESettingList();
    }

    /**
     * Create an instance of {@link OrderItemStatusInfo }
     * 
     */
    public OrderItemStatusInfo createOrderItemStatusInfo() {
        return new OrderItemStatusInfo();
    }

    /**
     * Create an instance of {@link DPAInfo }
     * 
     */
    public DPAInfo createDPAInfo() {
        return new DPAInfo();
    }

    /**
     * Create an instance of {@link GetSubProductInfo }
     * 
     */
    public GetSubProductInfo createGetSubProductInfo() {
        return new GetSubProductInfo();
    }

    /**
     * Create an instance of {@link DNESettingInfo }
     * 
     */
    public DNESettingInfo createDNESettingInfo() {
        return new DNESettingInfo();
    }

    /**
     * Create an instance of {@link ActualCustInfo }
     * 
     */
    public ActualCustInfo createActualCustInfo() {
        return new ActualCustInfo();
    }

    /**
     * Create an instance of {@link CommissionSummary }
     * 
     */
    public CommissionSummary createCommissionSummary() {
        return new CommissionSummary();
    }

    /**
     * Create an instance of {@link GetSubProductList }
     * 
     */
    public GetSubProductList createGetSubProductList() {
        return new GetSubProductList();
    }

    /**
     * Create an instance of {@link CorpInfo }
     * 
     */
    public CorpInfo createCorpInfo() {
        return new CorpInfo();
    }

    /**
     * Create an instance of {@link PointRedeemSchema }
     * 
     */
    public PointRedeemSchema createPointRedeemSchema() {
        return new PointRedeemSchema();
    }

    /**
     * Create an instance of {@link DealerOrderDetail }
     * 
     */
    public DealerOrderDetail createDealerOrderDetail() {
        return new DealerOrderDetail();
    }

    /**
     * Create an instance of {@link ResourceInfo }
     * 
     */
    public ResourceInfo createResourceInfo() {
        return new ResourceInfo();
    }

    /**
     * Create an instance of {@link ProlongIdInfo }
     * 
     */
    public ProlongIdInfo createProlongIdInfo() {
        return new ProlongIdInfo();
    }

    /**
     * Create an instance of {@link FamilyCustMember }
     * 
     */
    public FamilyCustMember createFamilyCustMember() {
        return new FamilyCustMember();
    }

    /**
     * Create an instance of {@link ContractInfo }
     * 
     */
    public ContractInfo createContractInfo() {
        return new ContractInfo();
    }

    /**
     * Create an instance of {@link OrderDetail }
     * 
     */
    public OrderDetail createOrderDetail() {
        return new OrderDetail();
    }

    /**
     * Create an instance of {@link ResourceList }
     * 
     */
    public ResourceList createResourceList() {
        return new ResourceList();
    }

    /**
     * Create an instance of {@link DealerResource }
     * 
     */
    public DealerResource createDealerResource() {
        return new DealerResource();
    }

    /**
     * Create an instance of {@link ParameterList }
     * 
     */
    public ParameterList createParameterList() {
        return new ParameterList();
    }

    /**
     * Create an instance of {@link TaxDetail }
     * 
     */
    public TaxDetail createTaxDetail() {
        return new TaxDetail();
    }

    /**
     * Create an instance of {@link CorpCustOrg }
     * 
     */
    public CorpCustOrg createCorpCustOrg() {
        return new CorpCustOrg();
    }

    /**
     * Create an instance of {@link ProlongationInfo }
     * 
     */
    public ProlongationInfo createProlongationInfo() {
        return new ProlongationInfo();
    }

    /**
     * Create an instance of {@link PaymentPlanInfo }
     * 
     */
    public PaymentPlanInfo createPaymentPlanInfo() {
        return new PaymentPlanInfo();
    }

    /**
     * Create an instance of {@link ProlongationList }
     * 
     */
    public ProlongationList createProlongationList() {
        return new ProlongationList();
    }

    /**
     * Create an instance of {@link AccountInfo }
     * 
     */
    public AccountInfo createAccountInfo() {
        return new AccountInfo();
    }

    /**
     * Create an instance of {@link PaymentRelationList }
     * 
     */
    public PaymentRelationList createPaymentRelationList() {
        return new PaymentRelationList();
    }

    /**
     * Create an instance of {@link CustBaseInfo }
     * 
     */
    public CustBaseInfo createCustBaseInfo() {
        return new CustBaseInfo();
    }

    /**
     * Create an instance of {@link GetSubOfferingList }
     * 
     */
    public GetSubOfferingList createGetSubOfferingList() {
        return new GetSubOfferingList();
    }

    /**
     * Create an instance of {@link GetSubOfferingInfo }
     * 
     */
    public GetSubOfferingInfo createGetSubOfferingInfo() {
        return new GetSubOfferingInfo();
    }

    /**
     * Create an instance of {@link PointRedeemPrice }
     * 
     */
    public PointRedeemPrice createPointRedeemPrice() {
        return new PointRedeemPrice();
    }

    /**
     * Create an instance of {@link ExtParameterList }
     * 
     */
    public ExtParameterList createExtParameterList() {
        return new ExtParameterList();
    }

    /**
     * Create an instance of {@link ExtParameterInfo }
     * 
     */
    public ExtParameterInfo createExtParameterInfo() {
        return new ExtParameterInfo();
    }

    /**
     * Create an instance of {@link ParkOrderInfo }
     * 
     */
    public ParkOrderInfo createParkOrderInfo() {
        return new ParkOrderInfo();
    }

    /**
     * Create an instance of {@link AddressList }
     * 
     */
    public AddressList createAddressList() {
        return new AddressList();
    }

    /**
     * Create an instance of {@link PointSubCondValue }
     * 
     */
    public PointSubCondValue createPointSubCondValue() {
        return new PointSubCondValue();
    }

    /**
     * Create an instance of {@link FamliyMemberList }
     * 
     */
    public FamliyMemberList createFamliyMemberList() {
        return new FamliyMemberList();
    }

    /**
     * Create an instance of {@link AddressInfo }
     * 
     */
    public AddressInfo createAddressInfo() {
        return new AddressInfo();
    }

    /**
     * Create an instance of {@link HistoryList.HistoryInfo }
     * 
     */
    public HistoryList.HistoryInfo createHistoryListHistoryInfo() {
        return new HistoryList.HistoryInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://crm.huawei.com/basetype/", name = "IDType", scope = CorpInfo.class)
    public JAXBElement<String> createCorpInfoIDType(String value) {
        return new JAXBElement<String>(_CorpInfoIDType_QNAME, String.class, CorpInfo.class, value);
    }

}
