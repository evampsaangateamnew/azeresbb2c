
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PointRedeemCondValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PointRedeemCondValue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustFactor" type="{http://crm.huawei.com/basetype/}PointCustCondValue"/>
 *         &lt;element name="SubFactor" type="{http://crm.huawei.com/basetype/}PointSubCondValue"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PointRedeemCondValue", propOrder = {
    "custFactor",
    "subFactor"
})
public class PointRedeemCondValue {

    @XmlElement(name = "CustFactor", required = true)
    protected PointCustCondValue custFactor;
    @XmlElement(name = "SubFactor", required = true)
    protected PointSubCondValue subFactor;

    /**
     * Gets the value of the custFactor property.
     * 
     * @return
     *     possible object is
     *     {@link PointCustCondValue }
     *     
     */
    public PointCustCondValue getCustFactor() {
        return custFactor;
    }

    /**
     * Sets the value of the custFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointCustCondValue }
     *     
     */
    public void setCustFactor(PointCustCondValue value) {
        this.custFactor = value;
    }

    /**
     * Gets the value of the subFactor property.
     * 
     * @return
     *     possible object is
     *     {@link PointSubCondValue }
     *     
     */
    public PointSubCondValue getSubFactor() {
        return subFactor;
    }

    /**
     * Sets the value of the subFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointSubCondValue }
     *     
     */
    public void setSubFactor(PointSubCondValue value) {
        this.subFactor = value;
    }

}
