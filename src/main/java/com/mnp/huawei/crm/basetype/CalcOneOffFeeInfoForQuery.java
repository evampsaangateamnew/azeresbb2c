
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CalcOneOffFeeInfoForQuery complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CalcOneOffFeeInfoForQuery">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="FeeItemCode" type="{http://crm.huawei.com/basetype/}FeeItemCode"/>
 *         &lt;element name="FeeType" type="{http://crm.huawei.com/basetype/}FeeType"/>
 *         &lt;element name="CaculatedFee" type="{http://crm.huawei.com/basetype/}CaculatedFee"/>
 *         &lt;element name="OriginalFee" type="{http://crm.huawei.com/basetype/}OriginalFee"/>
 *         &lt;element name="DiscountFee" type="{http://crm.huawei.com/basetype/}DiscountFee" minOccurs="0"/>
 *         &lt;element name="TaxInfo" type="{http://crm.huawei.com/basetype/}TaxList" minOccurs="0"/>
 *         &lt;element name="PayType" type="{http://crm.huawei.com/basetype/}PayType" minOccurs="0"/>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingId" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalcOneOffFeeInfoForQuery", propOrder = {

})
public class CalcOneOffFeeInfoForQuery {

    @XmlElement(name = "FeeItemCode", required = true)
    protected String feeItemCode;
    @XmlElement(name = "FeeType", required = true)
    protected String feeType;
    @XmlElement(name = "CaculatedFee")
    protected long caculatedFee;
    @XmlElement(name = "OriginalFee")
    protected long originalFee;
    @XmlElement(name = "DiscountFee")
    protected Long discountFee;
    @XmlElement(name = "TaxInfo")
    protected TaxList taxInfo;
    @XmlElement(name = "PayType")
    protected String payType;
    @XmlElement(name = "OfferingId")
    protected String offeringId;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the feeItemCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeItemCode() {
        return feeItemCode;
    }

    /**
     * Sets the value of the feeItemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeItemCode(String value) {
        this.feeItemCode = value;
    }

    /**
     * Gets the value of the feeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeType() {
        return feeType;
    }

    /**
     * Sets the value of the feeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeType(String value) {
        this.feeType = value;
    }

    /**
     * Gets the value of the caculatedFee property.
     * 
     */
    public long getCaculatedFee() {
        return caculatedFee;
    }

    /**
     * Sets the value of the caculatedFee property.
     * 
     */
    public void setCaculatedFee(long value) {
        this.caculatedFee = value;
    }

    /**
     * Gets the value of the originalFee property.
     * 
     */
    public long getOriginalFee() {
        return originalFee;
    }

    /**
     * Sets the value of the originalFee property.
     * 
     */
    public void setOriginalFee(long value) {
        this.originalFee = value;
    }

    /**
     * Gets the value of the discountFee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDiscountFee() {
        return discountFee;
    }

    /**
     * Sets the value of the discountFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDiscountFee(Long value) {
        this.discountFee = value;
    }

    /**
     * Gets the value of the taxInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TaxList }
     *     
     */
    public TaxList getTaxInfo() {
        return taxInfo;
    }

    /**
     * Sets the value of the taxInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxList }
     *     
     */
    public void setTaxInfo(TaxList value) {
        this.taxInfo = value;
    }

    /**
     * Gets the value of the payType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayType() {
        return payType;
    }

    /**
     * Sets the value of the payType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayType(String value) {
        this.payType = value;
    }

    /**
     * Gets the value of the offeringId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingId() {
        return offeringId;
    }

    /**
     * Sets the value of the offeringId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingId(String value) {
        this.offeringId = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
