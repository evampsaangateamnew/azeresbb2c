
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CartItemInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CartItemInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OrderType" type="{http://crm.huawei.com/basetype/}OrderType"/>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="SubscriberId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="ItemType" type="{http://crm.huawei.com/basetype/}EffectMode"/>
 *         &lt;element name="ItemId" type="{http://crm.huawei.com/basetype/}OfferingId"/>
 *         &lt;element name="SKUID" type="{http://crm.huawei.com/basetype/}SkuId" minOccurs="0"/>
 *         &lt;element name="IdentityId" type="{http://crm.huawei.com/basetype/}ResourceCode" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://crm.huawei.com/basetype/}Quantity"/>
 *         &lt;element name="SalesPerson" type="{http://crm.huawei.com/basetype/}OperId" minOccurs="0"/>
 *         &lt;element name="OwnerType" type="{http://crm.huawei.com/basetype/}CustomerType" minOccurs="0"/>
 *         &lt;element name="OwnerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="ShippingFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *         &lt;element name="SubCartItemList" type="{http://crm.huawei.com/basetype/}CartItemList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CartItemInfo", propOrder = {

})
public class CartItemInfo {

    @XmlElement(name = "OrderType", required = true)
    protected String orderType;
    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "SubscriberId")
    protected Long subscriberId;
    @XmlElement(name = "ItemType", required = true)
    protected String itemType;
    @XmlElement(name = "ItemId", required = true)
    protected String itemId;
    @XmlElement(name = "SKUID")
    protected String skuid;
    @XmlElement(name = "IdentityId")
    protected String identityId;
    @XmlElement(name = "Quantity", required = true)
    protected String quantity;
    @XmlElement(name = "SalesPerson")
    protected String salesPerson;
    @XmlElement(name = "OwnerType")
    protected String ownerType;
    @XmlElement(name = "OwnerId")
    protected Long ownerId;
    @XmlElement(name = "ShippingFlag", required = true)
    protected String shippingFlag;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;
    @XmlElement(name = "SubCartItemList")
    protected CartItemList subCartItemList;

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderType(String value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriberId(Long value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the itemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Sets the value of the itemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemType(String value) {
        this.itemType = value;
    }

    /**
     * Gets the value of the itemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * Sets the value of the itemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemId(String value) {
        this.itemId = value;
    }

    /**
     * Gets the value of the skuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSKUID() {
        return skuid;
    }

    /**
     * Sets the value of the skuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSKUID(String value) {
        this.skuid = value;
    }

    /**
     * Gets the value of the identityId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentityId() {
        return identityId;
    }

    /**
     * Sets the value of the identityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentityId(String value) {
        this.identityId = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity(String value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the salesPerson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesPerson() {
        return salesPerson;
    }

    /**
     * Sets the value of the salesPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesPerson(String value) {
        this.salesPerson = value;
    }

    /**
     * Gets the value of the ownerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerType() {
        return ownerType;
    }

    /**
     * Sets the value of the ownerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerType(String value) {
        this.ownerType = value;
    }

    /**
     * Gets the value of the ownerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOwnerId() {
        return ownerId;
    }

    /**
     * Sets the value of the ownerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOwnerId(Long value) {
        this.ownerId = value;
    }

    /**
     * Gets the value of the shippingFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingFlag() {
        return shippingFlag;
    }

    /**
     * Sets the value of the shippingFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingFlag(String value) {
        this.shippingFlag = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

    /**
     * Gets the value of the subCartItemList property.
     * 
     * @return
     *     possible object is
     *     {@link CartItemList }
     *     
     */
    public CartItemList getSubCartItemList() {
        return subCartItemList;
    }

    /**
     * Sets the value of the subCartItemList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CartItemList }
     *     
     */
    public void setSubCartItemList(CartItemList value) {
        this.subCartItemList = value;
    }

}
