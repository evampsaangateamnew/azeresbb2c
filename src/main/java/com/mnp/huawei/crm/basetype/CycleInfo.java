
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CycleInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CycleInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Cycle" type="{http://crm.huawei.com/basetype/}Cycle" minOccurs="0"/>
 *         &lt;element name="CycleType" type="{http://crm.huawei.com/basetype/}CycleType" minOccurs="0"/>
 *         &lt;element name="BeId" type="{http://crm.huawei.com/basetype/}BeID" minOccurs="0"/>
 *         &lt;element name="StartDate" type="{http://crm.huawei.com/basetype/}Date" minOccurs="0"/>
 *         &lt;element name="EndDate" type="{http://crm.huawei.com/basetype/}Date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CycleInfo", propOrder = {
    "cycle",
    "cycleType",
    "beId",
    "startDate",
    "endDate"
})
public class CycleInfo {

    @XmlElement(name = "Cycle")
    protected String cycle;
    @XmlElement(name = "CycleType")
    protected String cycleType;
    @XmlElement(name = "BeId")
    protected String beId;
    @XmlElement(name = "StartDate")
    protected String startDate;
    @XmlElement(name = "EndDate")
    protected String endDate;

    /**
     * Gets the value of the cycle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * Sets the value of the cycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycle(String value) {
        this.cycle = value;
    }

    /**
     * Gets the value of the cycleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycleType() {
        return cycleType;
    }

    /**
     * Sets the value of the cycleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycleType(String value) {
        this.cycleType = value;
    }

    /**
     * Gets the value of the beId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeId() {
        return beId;
    }

    /**
     * Sets the value of the beId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeId(String value) {
        this.beId = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDate(String value) {
        this.endDate = value;
    }

}
