
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DealerOrderDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DealerOrderDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="DealerId" type="{http://crm.huawei.com/basetype/}DealerIdentifier" minOccurs="0"/>
 *         &lt;element name="OrderNo" type="{http://crm.huawei.com/basetype/}OrderNo" minOccurs="0"/>
 *         &lt;element name="ItemNo" type="{http://crm.huawei.com/basetype/}ItemNo" minOccurs="0"/>
 *         &lt;element name="ProductId" type="{http://crm.huawei.com/basetype/}ProductId30" minOccurs="0"/>
 *         &lt;element name="ProductName" type="{http://crm.huawei.com/basetype/}ProductName128" minOccurs="0"/>
 *         &lt;element name="OfferId" type="{http://crm.huawei.com/basetype/}OfferId" minOccurs="0"/>
 *         &lt;element name="OfferName" type="{http://crm.huawei.com/basetype/}OfferingName" minOccurs="0"/>
 *         &lt;element name="ResType" type="{http://crm.huawei.com/basetype/}ResourceType10" minOccurs="0"/>
 *         &lt;element name="ResModel" type="{http://crm.huawei.com/basetype/}ResourceModel64" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://crm.huawei.com/basetype/}UnitPrice" minOccurs="0"/>
 *         &lt;element name="UnitPrice" type="{http://crm.huawei.com/basetype/}UnitPrice" minOccurs="0"/>
 *         &lt;element name="IncludeTaxFee" type="{http://crm.huawei.com/basetype/}IncludeTaxFee" minOccurs="0"/>
 *         &lt;element name="ExcludeTaxFee" type="{http://crm.huawei.com/basetype/}ExcludeTaxFee" minOccurs="0"/>
 *         &lt;element name="Tax" type="{http://crm.huawei.com/basetype/}TaxFee" minOccurs="0"/>
 *         &lt;element name="Discount" type="{http://crm.huawei.com/basetype/}DiscountFee" minOccurs="0"/>
 *         &lt;element name="Waive" type="{http://crm.huawei.com/basetype/}Waive" minOccurs="0"/>
 *         &lt;element name="Fee" type="{http://crm.huawei.com/basetype/}TaxFee" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}DealerOrderStatus" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerOrderDetail", propOrder = {

})
public class DealerOrderDetail {

    @XmlElement(name = "DealerId")
    protected String dealerId;
    @XmlElement(name = "OrderNo")
    protected String orderNo;
    @XmlElement(name = "ItemNo")
    protected String itemNo;
    @XmlElement(name = "ProductId")
    protected String productId;
    @XmlElement(name = "ProductName")
    protected String productName;
    @XmlElement(name = "OfferId")
    protected String offerId;
    @XmlElement(name = "OfferName")
    protected String offerName;
    @XmlElement(name = "ResType")
    protected String resType;
    @XmlElement(name = "ResModel")
    protected String resModel;
    @XmlElement(name = "Quantity")
    protected Long quantity;
    @XmlElement(name = "UnitPrice")
    protected Long unitPrice;
    @XmlElement(name = "IncludeTaxFee")
    protected Long includeTaxFee;
    @XmlElement(name = "ExcludeTaxFee")
    protected Long excludeTaxFee;
    @XmlElement(name = "Tax")
    protected Long tax;
    @XmlElement(name = "Discount")
    protected Long discount;
    @XmlElement(name = "Waive")
    protected Long waive;
    @XmlElement(name = "Fee")
    protected Long fee;
    @XmlElement(name = "Status")
    protected String status;

    /**
     * Gets the value of the dealerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * Sets the value of the dealerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerId(String value) {
        this.dealerId = value;
    }

    /**
     * Gets the value of the orderNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * Sets the value of the orderNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNo(String value) {
        this.orderNo = value;
    }

    /**
     * Gets the value of the itemNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemNo() {
        return itemNo;
    }

    /**
     * Sets the value of the itemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemNo(String value) {
        this.itemNo = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductName(String value) {
        this.productName = value;
    }

    /**
     * Gets the value of the offerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     * Sets the value of the offerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferId(String value) {
        this.offerId = value;
    }

    /**
     * Gets the value of the offerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferName() {
        return offerName;
    }

    /**
     * Sets the value of the offerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferName(String value) {
        this.offerName = value;
    }

    /**
     * Gets the value of the resType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResType() {
        return resType;
    }

    /**
     * Sets the value of the resType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResType(String value) {
        this.resType = value;
    }

    /**
     * Gets the value of the resModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResModel() {
        return resModel;
    }

    /**
     * Sets the value of the resModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResModel(String value) {
        this.resModel = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setQuantity(Long value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the unitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUnitPrice() {
        return unitPrice;
    }

    /**
     * Sets the value of the unitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUnitPrice(Long value) {
        this.unitPrice = value;
    }

    /**
     * Gets the value of the includeTaxFee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIncludeTaxFee() {
        return includeTaxFee;
    }

    /**
     * Sets the value of the includeTaxFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIncludeTaxFee(Long value) {
        this.includeTaxFee = value;
    }

    /**
     * Gets the value of the excludeTaxFee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExcludeTaxFee() {
        return excludeTaxFee;
    }

    /**
     * Sets the value of the excludeTaxFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExcludeTaxFee(Long value) {
        this.excludeTaxFee = value;
    }

    /**
     * Gets the value of the tax property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTax() {
        return tax;
    }

    /**
     * Sets the value of the tax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTax(Long value) {
        this.tax = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDiscount(Long value) {
        this.discount = value;
    }

    /**
     * Gets the value of the waive property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getWaive() {
        return waive;
    }

    /**
     * Sets the value of the waive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setWaive(Long value) {
        this.waive = value;
    }

    /**
     * Gets the value of the fee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFee() {
        return fee;
    }

    /**
     * Sets the value of the fee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFee(Long value) {
        this.fee = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
