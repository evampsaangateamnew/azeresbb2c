
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetOfferingInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetOfferingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingId" type="{http://crm.huawei.com/basetype/}OfferingId"/>
 *         &lt;element name="OfferingName" type="{http://crm.huawei.com/basetype/}OfferingName"/>
 *         &lt;element name="OfferingCode" type="{http://crm.huawei.com/basetype/}OfferingCode"/>
 *         &lt;element name="EffectiveTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="ExpiredTime" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *         &lt;element name="OfferingShortName" type="{http://crm.huawei.com/basetype/}OfferingShortName" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://crm.huawei.com/basetype/}OfferingOwnerStatus" minOccurs="0"/>
 *         &lt;element name="NetworkType" type="{http://crm.huawei.com/basetype/}TeleType" minOccurs="0"/>
 *         &lt;element name="OfferingType" type="{http://crm.huawei.com/basetype/}OfferingType" minOccurs="0"/>
 *         &lt;element name="OwnerType" type="{http://crm.huawei.com/basetype/}OwnerType" minOccurs="0"/>
 *         &lt;element name="MonthlyFee" type="{http://crm.huawei.com/basetype/}MonthlyFee" minOccurs="0"/>
 *         &lt;element name="OneTimeFee" type="{http://crm.huawei.com/basetype/}OneTimeFee" minOccurs="0"/>
 *         &lt;element name="ContractInfo" type="{http://crm.huawei.com/basetype/}ContractInfoList" minOccurs="0"/>
 *         &lt;element name="SellCatalogueId" type="{http://crm.huawei.com/basetype/}SellCatalogueId" minOccurs="0"/>
 *         &lt;element name="SellCatalogueName" type="{http://crm.huawei.com/basetype/}SellCatalogueName" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetOfferingInfo", propOrder = {
    "offeringId",
    "offeringName",
    "offeringCode",
    "effectiveTime",
    "expiredTime",
    "offeringShortName",
    "status",
    "networkType",
    "offeringType",
    "ownerType",
    "monthlyFee",
    "oneTimeFee",
    "contractInfo",
    "sellCatalogueId",
    "sellCatalogueName",
    "extParamList"
})
public class GetOfferingInfo {

    @XmlElement(name = "OfferingId", required = true)
    protected String offeringId;
    @XmlElement(name = "OfferingName", required = true)
    protected String offeringName;
    @XmlElement(name = "OfferingCode", required = true)
    protected String offeringCode;
    @XmlElement(name = "EffectiveTime")
    protected String effectiveTime;
    @XmlElement(name = "ExpiredTime")
    protected String expiredTime;
    @XmlElement(name = "OfferingShortName")
    protected String offeringShortName;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "NetworkType")
    protected String networkType;
    @XmlElement(name = "OfferingType")
    protected String offeringType;
    @XmlElement(name = "OwnerType")
    protected String ownerType;
    @XmlElement(name = "MonthlyFee")
    protected String monthlyFee;
    @XmlElement(name = "OneTimeFee")
    protected String oneTimeFee;
    @XmlElement(name = "ContractInfo")
    protected ContractInfoList contractInfo;
    @XmlElement(name = "SellCatalogueId")
    protected String sellCatalogueId;
    @XmlElement(name = "SellCatalogueName")
    protected String sellCatalogueName;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the offeringId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingId() {
        return offeringId;
    }

    /**
     * Sets the value of the offeringId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingId(String value) {
        this.offeringId = value;
    }

    /**
     * Gets the value of the offeringName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingName() {
        return offeringName;
    }

    /**
     * Sets the value of the offeringName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingName(String value) {
        this.offeringName = value;
    }

    /**
     * Gets the value of the offeringCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingCode() {
        return offeringCode;
    }

    /**
     * Sets the value of the offeringCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingCode(String value) {
        this.offeringCode = value;
    }

    /**
     * Gets the value of the effectiveTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveTime() {
        return effectiveTime;
    }

    /**
     * Sets the value of the effectiveTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveTime(String value) {
        this.effectiveTime = value;
    }

    /**
     * Gets the value of the expiredTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiredTime() {
        return expiredTime;
    }

    /**
     * Sets the value of the expiredTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiredTime(String value) {
        this.expiredTime = value;
    }

    /**
     * Gets the value of the offeringShortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingShortName() {
        return offeringShortName;
    }

    /**
     * Sets the value of the offeringShortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingShortName(String value) {
        this.offeringShortName = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the networkType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkType() {
        return networkType;
    }

    /**
     * Sets the value of the networkType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkType(String value) {
        this.networkType = value;
    }

    /**
     * Gets the value of the offeringType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingType() {
        return offeringType;
    }

    /**
     * Sets the value of the offeringType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingType(String value) {
        this.offeringType = value;
    }

    /**
     * Gets the value of the ownerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerType() {
        return ownerType;
    }

    /**
     * Sets the value of the ownerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerType(String value) {
        this.ownerType = value;
    }

    /**
     * Gets the value of the monthlyFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthlyFee() {
        return monthlyFee;
    }

    /**
     * Sets the value of the monthlyFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthlyFee(String value) {
        this.monthlyFee = value;
    }

    /**
     * Gets the value of the oneTimeFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOneTimeFee() {
        return oneTimeFee;
    }

    /**
     * Sets the value of the oneTimeFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOneTimeFee(String value) {
        this.oneTimeFee = value;
    }

    /**
     * Gets the value of the contractInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ContractInfoList }
     *     
     */
    public ContractInfoList getContractInfo() {
        return contractInfo;
    }

    /**
     * Sets the value of the contractInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractInfoList }
     *     
     */
    public void setContractInfo(ContractInfoList value) {
        this.contractInfo = value;
    }

    /**
     * Gets the value of the sellCatalogueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellCatalogueId() {
        return sellCatalogueId;
    }

    /**
     * Sets the value of the sellCatalogueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellCatalogueId(String value) {
        this.sellCatalogueId = value;
    }

    /**
     * Gets the value of the sellCatalogueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellCatalogueName() {
        return sellCatalogueName;
    }

    /**
     * Sets the value of the sellCatalogueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellCatalogueName(String value) {
        this.sellCatalogueName = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
