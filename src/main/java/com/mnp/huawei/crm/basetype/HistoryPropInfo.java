
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HistoryPropInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HistoryPropInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="PropertyType" type="{http://crm.huawei.com/basetype/}PropertyType"/>
 *         &lt;element name="PropertyCode" type="{http://crm.huawei.com/basetype/}PropertyCode"/>
 *         &lt;element name="PropertyOldValue" type="{http://crm.huawei.com/basetype/}PropertyValue" minOccurs="0"/>
 *         &lt;element name="PropertyNewValue" type="{http://crm.huawei.com/basetype/}PropertyValue" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoryPropInfo", propOrder = {

})
public class HistoryPropInfo {

    @XmlElement(name = "PropertyType", required = true)
    protected String propertyType;
    @XmlElement(name = "PropertyCode", required = true)
    protected String propertyCode;
    @XmlElement(name = "PropertyOldValue")
    protected String propertyOldValue;
    @XmlElement(name = "PropertyNewValue")
    protected String propertyNewValue;

    /**
     * Gets the value of the propertyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyType() {
        return propertyType;
    }

    /**
     * Sets the value of the propertyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyType(String value) {
        this.propertyType = value;
    }

    /**
     * Gets the value of the propertyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyCode() {
        return propertyCode;
    }

    /**
     * Sets the value of the propertyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyCode(String value) {
        this.propertyCode = value;
    }

    /**
     * Gets the value of the propertyOldValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyOldValue() {
        return propertyOldValue;
    }

    /**
     * Sets the value of the propertyOldValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyOldValue(String value) {
        this.propertyOldValue = value;
    }

    /**
     * Gets the value of the propertyNewValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyNewValue() {
        return propertyNewValue;
    }

    /**
     * Sets the value of the propertyNewValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyNewValue(String value) {
        this.propertyNewValue = value;
    }

}
