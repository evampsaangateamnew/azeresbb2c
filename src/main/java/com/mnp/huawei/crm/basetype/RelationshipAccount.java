
package com.mnp.huawei.crm.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RelationshipAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationshipAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubsEnityId" type="{http://crm.huawei.com/basetype/}SubscriberId" minOccurs="0"/>
 *         &lt;element name="AccountEnityId" type="{http://crm.huawei.com/basetype/}AccountId" minOccurs="0"/>
 *         &lt;element name="PaymentDesc" type="{http://crm.huawei.com/basetype/}Remark" minOccurs="0"/>
 *         &lt;element name="ExtRelationshipParam" type="{http://crm.huawei.com/basetype/}ExtParameterList" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationshipAccount", propOrder = {
    "subsEnityId",
    "accountEnityId",
    "paymentDesc",
    "extRelationshipParam"
})
public class RelationshipAccount {

    @XmlElement(name = "SubsEnityId")
    protected Long subsEnityId;
    @XmlElement(name = "AccountEnityId")
    protected Long accountEnityId;
    @XmlElement(name = "PaymentDesc")
    protected String paymentDesc;
    @XmlElement(name = "ExtRelationshipParam")
    protected List<ExtParameterList> extRelationshipParam;

    /**
     * Gets the value of the subsEnityId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubsEnityId() {
        return subsEnityId;
    }

    /**
     * Sets the value of the subsEnityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubsEnityId(Long value) {
        this.subsEnityId = value;
    }

    /**
     * Gets the value of the accountEnityId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountEnityId() {
        return accountEnityId;
    }

    /**
     * Sets the value of the accountEnityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountEnityId(Long value) {
        this.accountEnityId = value;
    }

    /**
     * Gets the value of the paymentDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDesc() {
        return paymentDesc;
    }

    /**
     * Sets the value of the paymentDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDesc(String value) {
        this.paymentDesc = value;
    }

    /**
     * Gets the value of the extRelationshipParam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extRelationshipParam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtRelationshipParam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtParameterList }
     * 
     * 
     */
    public List<ExtParameterList> getExtRelationshipParam() {
        if (extRelationshipParam == null) {
            extRelationshipParam = new ArrayList<ExtParameterList>();
        }
        return this.extRelationshipParam;
    }

}
