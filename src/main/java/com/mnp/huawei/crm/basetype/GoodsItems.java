
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GoodsItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GoodsItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="idx">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="offerId" type="{http://crm.huawei.com/basetype/}OfferingId"/>
 *         &lt;element name="productId" type="{http://crm.huawei.com/basetype/}ProductId" minOccurs="0"/>
 *         &lt;element name="skuId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="overrideUnitPrice" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="quantity" type="{http://crm.huawei.com/basetype/}Amount"/>
 *         &lt;element name="FeeItemCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CaculatedFee" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="OriginalFee" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="DiscountFee" type="{http://crm.huawei.com/basetype/}Amount" minOccurs="0"/>
 *         &lt;element name="TaxInfo" type="{http://crm.huawei.com/basetype/}TaxDetail" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GoodsItems", propOrder = {

})
public class GoodsItems {

    @XmlElement(required = true)
    protected String idx;
    @XmlElement(required = true)
    protected String offerId;
    protected String productId;
    protected String skuId;
    protected Long overrideUnitPrice;
    protected long quantity;
    @XmlElement(name = "FeeItemCode")
    protected String feeItemCode;
    @XmlElement(name = "CaculatedFee")
    protected Long caculatedFee;
    @XmlElement(name = "OriginalFee")
    protected Long originalFee;
    @XmlElement(name = "DiscountFee")
    protected Long discountFee;
    @XmlElement(name = "TaxInfo")
    protected TaxDetail taxInfo;

    /**
     * Gets the value of the idx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdx() {
        return idx;
    }

    /**
     * Sets the value of the idx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdx(String value) {
        this.idx = value;
    }

    /**
     * Gets the value of the offerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     * Sets the value of the offerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferId(String value) {
        this.offerId = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the skuId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuId() {
        return skuId;
    }

    /**
     * Sets the value of the skuId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuId(String value) {
        this.skuId = value;
    }

    /**
     * Gets the value of the overrideUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOverrideUnitPrice() {
        return overrideUnitPrice;
    }

    /**
     * Sets the value of the overrideUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOverrideUnitPrice(Long value) {
        this.overrideUnitPrice = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     */
    public void setQuantity(long value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the feeItemCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeItemCode() {
        return feeItemCode;
    }

    /**
     * Sets the value of the feeItemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeItemCode(String value) {
        this.feeItemCode = value;
    }

    /**
     * Gets the value of the caculatedFee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCaculatedFee() {
        return caculatedFee;
    }

    /**
     * Sets the value of the caculatedFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCaculatedFee(Long value) {
        this.caculatedFee = value;
    }

    /**
     * Gets the value of the originalFee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOriginalFee() {
        return originalFee;
    }

    /**
     * Sets the value of the originalFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOriginalFee(Long value) {
        this.originalFee = value;
    }

    /**
     * Gets the value of the discountFee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDiscountFee() {
        return discountFee;
    }

    /**
     * Sets the value of the discountFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDiscountFee(Long value) {
        this.discountFee = value;
    }

    /**
     * Gets the value of the taxInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TaxDetail }
     *     
     */
    public TaxDetail getTaxInfo() {
        return taxInfo;
    }

    /**
     * Sets the value of the taxInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxDetail }
     *     
     */
    public void setTaxInfo(TaxDetail value) {
        this.taxInfo = value;
    }

}
