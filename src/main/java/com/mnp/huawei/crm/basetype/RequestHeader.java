
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="Version" type="{http://crm.huawei.com/basetype/}Version" minOccurs="0"/>
 *         &lt;element name="TransactionId" type="{http://crm.huawei.com/basetype/}TransactionId"/>
 *         &lt;element name="SessionId" type="{http://crm.huawei.com/basetype/}SessionId" minOccurs="0"/>
 *         &lt;element name="ProcessTime" type="{http://crm.huawei.com/basetype/}ProcessTime" minOccurs="0"/>
 *         &lt;element name="ContactId" type="{http://crm.huawei.com/basetype/}ContactId" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://crm.huawei.com/basetype/}Language" minOccurs="0"/>
 *         &lt;element name="ChannelId" type="{http://crm.huawei.com/basetype/}ChannelId"/>
 *         &lt;element name="TechnicalChannelId" type="{http://crm.huawei.com/basetype/}TechnicalChannelId"/>
 *         &lt;element name="TenantId" type="{http://crm.huawei.com/basetype/}TenantId" minOccurs="0"/>
 *         &lt;element name="AccessUser" type="{http://crm.huawei.com/basetype/}AccessUser"/>
 *         &lt;element name="AccessPwd" type="{http://crm.huawei.com/basetype/}AccessPwd"/>
 *         &lt;element name="AccessIP" type="{http://crm.huawei.com/basetype/}AccessIP" minOccurs="0"/>
 *         &lt;element name="OperatorId" type="{http://crm.huawei.com/basetype/}OperatorId" minOccurs="0"/>
 *         &lt;element name="OperatorPwd" type="{http://crm.huawei.com/basetype/}OperatorPwd" minOccurs="0"/>
 *         &lt;element name="TestFlag" type="{http://crm.huawei.com/basetype/}TestFlag" minOccurs="0"/>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestHeader", propOrder = {

})
public class RequestHeader {

    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "TransactionId", required = true)
    protected String transactionId;
    @XmlElement(name = "SessionId")
    protected String sessionId;
    @XmlElement(name = "ProcessTime")
    protected String processTime;
    @XmlElement(name = "ContactId")
    protected String contactId;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "ChannelId", required = true)
    protected String channelId;
    @XmlElement(name = "TechnicalChannelId", required = true)
    protected String technicalChannelId;
    @XmlElement(name = "TenantId")
    protected String tenantId;
    @XmlElement(name = "AccessUser", required = true)
    protected String accessUser;
    @XmlElement(name = "AccessPwd", required = true)
    protected String accessPwd;
    @XmlElement(name = "AccessIP")
    protected String accessIP;
    @XmlElement(name = "OperatorId")
    protected String operatorId;
    @XmlElement(name = "OperatorPwd")
    protected String operatorPwd;
    @XmlElement(name = "TestFlag")
    protected String testFlag;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the sessionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the value of the sessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionId(String value) {
        this.sessionId = value;
    }

    /**
     * Gets the value of the processTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessTime() {
        return processTime;
    }

    /**
     * Sets the value of the processTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessTime(String value) {
        this.processTime = value;
    }

    /**
     * Gets the value of the contactId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactId() {
        return contactId;
    }

    /**
     * Sets the value of the contactId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactId(String value) {
        this.contactId = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the channelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * Sets the value of the channelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelId(String value) {
        this.channelId = value;
    }

    /**
     * Gets the value of the technicalChannelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicalChannelId() {
        return technicalChannelId;
    }

    /**
     * Sets the value of the technicalChannelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicalChannelId(String value) {
        this.technicalChannelId = value;
    }

    /**
     * Gets the value of the tenantId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * Sets the value of the tenantId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTenantId(String value) {
        this.tenantId = value;
    }

    /**
     * Gets the value of the accessUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessUser() {
        return accessUser;
    }

    /**
     * Sets the value of the accessUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessUser(String value) {
        this.accessUser = value;
    }

    /**
     * Gets the value of the accessPwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessPwd() {
        return accessPwd;
    }

    /**
     * Sets the value of the accessPwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessPwd(String value) {
        this.accessPwd = value;
    }

    /**
     * Gets the value of the accessIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessIP() {
        return accessIP;
    }

    /**
     * Sets the value of the accessIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessIP(String value) {
        this.accessIP = value;
    }

    /**
     * Gets the value of the operatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorId() {
        return operatorId;
    }

    /**
     * Sets the value of the operatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorId(String value) {
        this.operatorId = value;
    }

    /**
     * Gets the value of the operatorPwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorPwd() {
        return operatorPwd;
    }

    /**
     * Sets the value of the operatorPwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorPwd(String value) {
        this.operatorPwd = value;
    }

    /**
     * Gets the value of the testFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestFlag() {
        return testFlag;
    }

    /**
     * Sets the value of the testFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestFlag(String value) {
        this.testFlag = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
