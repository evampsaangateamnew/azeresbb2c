
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DNESettingInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DNESettingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActionType" type="{http://crm.huawei.com/basetype/}ActionType" minOccurs="0"/>
 *         &lt;element name="DNEParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *         &lt;element name="DNEType" type="{http://crm.huawei.com/basetype/}DNEType"/>
 *         &lt;element name="Remark" type="{http://crm.huawei.com/basetype/}Remark" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DNESettingInfo", propOrder = {
    "actionType",
    "dneParamList",
    "dneType",
    "remark"
})
public class DNESettingInfo {

    @XmlElement(name = "ActionType")
    protected String actionType;
    @XmlElement(name = "DNEParamList")
    protected ExtParameterList dneParamList;
    @XmlElement(name = "DNEType", required = true)
    protected String dneType;
    @XmlElement(name = "Remark")
    protected String remark;

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the dneParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getDNEParamList() {
        return dneParamList;
    }

    /**
     * Sets the value of the dneParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setDNEParamList(ExtParameterList value) {
        this.dneParamList = value;
    }

    /**
     * Gets the value of the dneType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDNEType() {
        return dneType;
    }

    /**
     * Sets the value of the dneType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDNEType(String value) {
        this.dneType = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

}
