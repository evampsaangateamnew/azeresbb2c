
package com.mnp.huawei.crm.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModCustomerInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModCustomerInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerType" type="{http://crm.huawei.com/basetype/}CustomerType" minOccurs="0"/>
 *         &lt;element name="CertificateType" type="{http://crm.huawei.com/basetype/}CertificateType" minOccurs="0"/>
 *         &lt;element name="CertificateNumber" type="{http://crm.huawei.com/basetype/}CertificateNumber" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://crm.huawei.com/basetype/}Title" minOccurs="0"/>
 *         &lt;element name="Gender" type="{http://crm.huawei.com/basetype/}Gender" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://crm.huawei.com/basetype/}FirstName" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://crm.huawei.com/basetype/}MiddleName" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://crm.huawei.com/basetype/}LastName" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://crm.huawei.com/basetype/}Nationality" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://crm.huawei.com/basetype/}Language" minOccurs="0"/>
 *         &lt;element name="CustLevel" type="{http://crm.huawei.com/basetype/}CustLevel" minOccurs="0"/>
 *         &lt;element name="AddressInfo" type="{http://crm.huawei.com/basetype/}AddressInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CustContact" type="{http://crm.huawei.com/basetype/}ContactInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CustStatus" type="{http://crm.huawei.com/basetype/}CustStatus" minOccurs="0"/>
 *         &lt;element name="DateOfBirth" type="{http://crm.huawei.com/basetype/}DateOfBirth" minOccurs="0"/>
 *         &lt;element name="CustomerDNE" type="{http://crm.huawei.com/basetype/}DNESettingInfo" minOccurs="0"/>
 *         &lt;element name="ExtCustParam" type="{http://crm.huawei.com/basetype/}ExtParameterList" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModCustomerInfo", propOrder = {
    "customerType",
    "certificateType",
    "certificateNumber",
    "title",
    "gender",
    "firstName",
    "middleName",
    "lastName",
    "nationality",
    "language",
    "custLevel",
    "addressInfo",
    "custContact",
    "custStatus",
    "dateOfBirth",
    "customerDNE",
    "extCustParam"
})
public class ModCustomerInfo {

    @XmlElement(name = "CustomerType")
    protected String customerType;
    @XmlElement(name = "CertificateType")
    protected String certificateType;
    @XmlElement(name = "CertificateNumber")
    protected String certificateNumber;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "Gender")
    protected String gender;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "Nationality")
    protected String nationality;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "CustLevel")
    protected String custLevel;
    @XmlElement(name = "AddressInfo")
    protected List<AddressInfo> addressInfo;
    @XmlElement(name = "CustContact")
    protected List<ContactInfo> custContact;
    @XmlElement(name = "CustStatus")
    protected String custStatus;
    @XmlElement(name = "DateOfBirth")
    protected String dateOfBirth;
    @XmlElement(name = "CustomerDNE")
    protected DNESettingInfo customerDNE;
    @XmlElement(name = "ExtCustParam", required = true)
    protected List<ExtParameterList> extCustParam;

    /**
     * Gets the value of the customerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerType() {
        return customerType;
    }

    /**
     * Sets the value of the customerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerType(String value) {
        this.customerType = value;
    }

    /**
     * Gets the value of the certificateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateType() {
        return certificateType;
    }

    /**
     * Sets the value of the certificateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateType(String value) {
        this.certificateType = value;
    }

    /**
     * Gets the value of the certificateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateNumber() {
        return certificateNumber;
    }

    /**
     * Sets the value of the certificateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateNumber(String value) {
        this.certificateNumber = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the custLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLevel() {
        return custLevel;
    }

    /**
     * Sets the value of the custLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLevel(String value) {
        this.custLevel = value;
    }

    /**
     * Gets the value of the addressInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addressInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddressInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddressInfo }
     * 
     * 
     */
    public List<AddressInfo> getAddressInfo() {
        if (addressInfo == null) {
            addressInfo = new ArrayList<AddressInfo>();
        }
        return this.addressInfo;
    }

    /**
     * Gets the value of the custContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the custContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactInfo }
     * 
     * 
     */
    public List<ContactInfo> getCustContact() {
        if (custContact == null) {
            custContact = new ArrayList<ContactInfo>();
        }
        return this.custContact;
    }

    /**
     * Gets the value of the custStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustStatus() {
        return custStatus;
    }

    /**
     * Sets the value of the custStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustStatus(String value) {
        this.custStatus = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfBirth(String value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the customerDNE property.
     * 
     * @return
     *     possible object is
     *     {@link DNESettingInfo }
     *     
     */
    public DNESettingInfo getCustomerDNE() {
        return customerDNE;
    }

    /**
     * Sets the value of the customerDNE property.
     * 
     * @param value
     *     allowed object is
     *     {@link DNESettingInfo }
     *     
     */
    public void setCustomerDNE(DNESettingInfo value) {
        this.customerDNE = value;
    }

    /**
     * Gets the value of the extCustParam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extCustParam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtCustParam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtParameterList }
     * 
     * 
     */
    public List<ExtParameterList> getExtCustParam() {
        if (extCustParam == null) {
            extCustParam = new ArrayList<ExtParameterList>();
        }
        return this.extCustParam;
    }

}
