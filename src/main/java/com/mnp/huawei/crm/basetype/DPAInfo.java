
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DPAInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DPAInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReminderId" type="{http://crm.huawei.com/basetype/}DPAReminderId"/>
 *         &lt;element name="OtherReminder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DPAInfo", propOrder = {
    "reminderId",
    "otherReminder",
    "password"
})
public class DPAInfo {

    @XmlElement(name = "ReminderId", required = true)
    protected String reminderId;
    @XmlElement(name = "OtherReminder")
    protected String otherReminder;
    @XmlElement(name = "Password", required = true)
    protected String password;

    /**
     * Gets the value of the reminderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReminderId() {
        return reminderId;
    }

    /**
     * Sets the value of the reminderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReminderId(String value) {
        this.reminderId = value;
    }

    /**
     * Gets the value of the otherReminder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherReminder() {
        return otherReminder;
    }

    /**
     * Sets the value of the otherReminder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherReminder(String value) {
        this.otherReminder = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

}
