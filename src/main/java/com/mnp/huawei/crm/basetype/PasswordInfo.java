
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PasswordInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PasswordInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="PasswordType" type="{http://crm.huawei.com/basetype/}PasswordType"/>
 *         &lt;element name="PwdOperateType" type="{http://crm.huawei.com/basetype/}PwdOperateType"/>
 *         &lt;element name="Password" type="{http://crm.huawei.com/basetype/}Password"/>
 *         &lt;element name="OldPassword" type="{http://crm.huawei.com/basetype/}Password" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PasswordInfo", propOrder = {

})
public class PasswordInfo {

    @XmlElement(name = "PasswordType", required = true)
    protected String passwordType;
    @XmlElement(name = "PwdOperateType", required = true)
    protected String pwdOperateType;
    @XmlElement(name = "Password", required = true)
    protected String password;
    @XmlElement(name = "OldPassword")
    protected String oldPassword;

    /**
     * Gets the value of the passwordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordType() {
        return passwordType;
    }

    /**
     * Sets the value of the passwordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordType(String value) {
        this.passwordType = value;
    }

    /**
     * Gets the value of the pwdOperateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPwdOperateType() {
        return pwdOperateType;
    }

    /**
     * Sets the value of the pwdOperateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPwdOperateType(String value) {
        this.pwdOperateType = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the oldPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * Sets the value of the oldPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldPassword(String value) {
        this.oldPassword = value;
    }

}
