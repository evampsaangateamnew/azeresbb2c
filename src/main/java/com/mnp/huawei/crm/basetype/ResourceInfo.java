
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResourceInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResourceInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ResourceType" type="{http://crm.huawei.com/basetype/}ResourceType" minOccurs="0"/>
 *         &lt;element name="ResourceModel" type="{http://crm.huawei.com/basetype/}ResourceModel" minOccurs="0"/>
 *         &lt;element name="ProductId" type="{http://crm.huawei.com/basetype/}ProductId" minOccurs="0"/>
 *         &lt;element name="PriceAmount" type="{http://crm.huawei.com/basetype/}PriceAmount" minOccurs="0"/>
 *         &lt;element name="ResourceID" type="{http://crm.huawei.com/basetype/}ResourceID" minOccurs="0"/>
 *         &lt;element name="ResourceCode" type="{http://crm.huawei.com/basetype/}ResourceCode" minOccurs="0"/>
 *         &lt;element name="ShippingFlg" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://crm.huawei.com/basetype/}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResourceInfo", propOrder = {

})
public class ResourceInfo {

    @XmlElement(name = "ResourceType")
    protected String resourceType;
    @XmlElement(name = "ResourceModel")
    protected String resourceModel;
    @XmlElement(name = "ProductId")
    protected String productId;
    @XmlElement(name = "PriceAmount")
    protected Long priceAmount;
    @XmlElement(name = "ResourceID")
    protected String resourceID;
    @XmlElement(name = "ResourceCode")
    protected String resourceCode;
    @XmlElement(name = "ShippingFlg")
    protected String shippingFlg;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the resourceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * Sets the value of the resourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceType(String value) {
        this.resourceType = value;
    }

    /**
     * Gets the value of the resourceModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceModel() {
        return resourceModel;
    }

    /**
     * Sets the value of the resourceModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceModel(String value) {
        this.resourceModel = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the priceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPriceAmount() {
        return priceAmount;
    }

    /**
     * Sets the value of the priceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPriceAmount(Long value) {
        this.priceAmount = value;
    }

    /**
     * Gets the value of the resourceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceID() {
        return resourceID;
    }

    /**
     * Sets the value of the resourceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceID(String value) {
        this.resourceID = value;
    }

    /**
     * Gets the value of the resourceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceCode() {
        return resourceCode;
    }

    /**
     * Sets the value of the resourceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceCode(String value) {
        this.resourceCode = value;
    }

    /**
     * Gets the value of the shippingFlg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingFlg() {
        return shippingFlg;
    }

    /**
     * Sets the value of the shippingFlg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingFlg(String value) {
        this.shippingFlg = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
