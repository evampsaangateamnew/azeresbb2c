
package com.mnp.huawei.crm.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HistoryParameterList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HistoryParameterList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryParameterInfo" type="{http://crm.huawei.com/basetype/}HistoryParameterInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoryParameterList", propOrder = {
    "historyParameterInfo"
})
public class HistoryParameterList {

    @XmlElement(name = "HistoryParameterInfo", required = true)
    protected List<HistoryParameterInfo> historyParameterInfo;

    /**
     * Gets the value of the historyParameterInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the historyParameterInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHistoryParameterInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HistoryParameterInfo }
     * 
     * 
     */
    public List<HistoryParameterInfo> getHistoryParameterInfo() {
        if (historyParameterInfo == null) {
            historyParameterInfo = new ArrayList<HistoryParameterInfo>();
        }
        return this.historyParameterInfo;
    }

}
