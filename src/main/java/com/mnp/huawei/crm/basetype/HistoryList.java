
package com.mnp.huawei.crm.basetype;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HistoryList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HistoryList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryInfo" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OrderItemType" type="{http://crm.huawei.com/basetype/}OrderItemType"/>
 *                   &lt;element name="Timestamp" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
 *                   &lt;element name="HistoryPropList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoryList", propOrder = {
    "historyInfo"
})
public class HistoryList {

    @XmlElement(name = "HistoryInfo", required = true)
    protected List<HistoryList.HistoryInfo> historyInfo;

    /**
     * Gets the value of the historyInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the historyInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHistoryInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HistoryList.HistoryInfo }
     * 
     * 
     */
    public List<HistoryList.HistoryInfo> getHistoryInfo() {
        if (historyInfo == null) {
            historyInfo = new ArrayList<HistoryList.HistoryInfo>();
        }
        return this.historyInfo;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OrderItemType" type="{http://crm.huawei.com/basetype/}OrderItemType"/>
     *         &lt;element name="Timestamp" type="{http://crm.huawei.com/basetype/}Time" minOccurs="0"/>
     *         &lt;element name="HistoryPropList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "orderItemType",
        "timestamp",
        "historyPropList"
    })
    public static class HistoryInfo {

        @XmlElement(name = "OrderItemType", required = true)
        protected String orderItemType;
        @XmlElement(name = "Timestamp")
        protected String timestamp;
        @XmlElement(name = "HistoryPropList")
        protected ExtParameterList historyPropList;

        /**
         * Gets the value of the orderItemType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrderItemType() {
            return orderItemType;
        }

        /**
         * Sets the value of the orderItemType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrderItemType(String value) {
            this.orderItemType = value;
        }

        /**
         * Gets the value of the timestamp property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTimestamp() {
            return timestamp;
        }

        /**
         * Sets the value of the timestamp property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTimestamp(String value) {
            this.timestamp = value;
        }

        /**
         * Gets the value of the historyPropList property.
         * 
         * @return
         *     possible object is
         *     {@link ExtParameterList }
         *     
         */
        public ExtParameterList getHistoryPropList() {
            return historyPropList;
        }

        /**
         * Sets the value of the historyPropList property.
         * 
         * @param value
         *     allowed object is
         *     {@link ExtParameterList }
         *     
         */
        public void setHistoryPropList(ExtParameterList value) {
            this.historyPropList = value;
        }

    }

}
