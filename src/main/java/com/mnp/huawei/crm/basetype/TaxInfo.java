
package com.mnp.huawei.crm.basetype;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TaxCode" type="{http://crm.huawei.com/basetype/}TaxCode" minOccurs="0"/>
 *         &lt;element name="TaxName" type="{http://crm.huawei.com/basetype/}TaxName" minOccurs="0"/>
 *         &lt;element name="TaxFee" type="{http://crm.huawei.com/basetype/}TaxFee" minOccurs="0"/>
 *         &lt;element name="TaxRate" type="{http://crm.huawei.com/basetype/}TaxRate" minOccurs="0"/>
 *         &lt;element name="TaxableAmount" type="{http://crm.huawei.com/basetype/}TaxableAmount" minOccurs="0"/>
 *         &lt;element name="ExemptionType" type="{http://crm.huawei.com/basetype/}ExemptionType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxInfo", propOrder = {

})
public class TaxInfo {

    @XmlElement(name = "TaxCode")
    protected String taxCode;
    @XmlElement(name = "TaxName")
    protected String taxName;
    @XmlElement(name = "TaxFee")
    protected Long taxFee;
    @XmlElement(name = "TaxRate")
    protected String taxRate;
    @XmlElement(name = "TaxableAmount")
    protected Long taxableAmount;
    @XmlElement(name = "ExemptionType")
    protected String exemptionType;

    /**
     * Gets the value of the taxCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCode() {
        return taxCode;
    }

    /**
     * Sets the value of the taxCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCode(String value) {
        this.taxCode = value;
    }

    /**
     * Gets the value of the taxName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxName() {
        return taxName;
    }

    /**
     * Sets the value of the taxName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxName(String value) {
        this.taxName = value;
    }

    /**
     * Gets the value of the taxFee property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTaxFee() {
        return taxFee;
    }

    /**
     * Sets the value of the taxFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTaxFee(Long value) {
        this.taxFee = value;
    }

    /**
     * Gets the value of the taxRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxRate() {
        return taxRate;
    }

    /**
     * Sets the value of the taxRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxRate(String value) {
        this.taxRate = value;
    }

    /**
     * Gets the value of the taxableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTaxableAmount() {
        return taxableAmount;
    }

    /**
     * Sets the value of the taxableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTaxableAmount(Long value) {
        this.taxableAmount = value;
    }

    /**
     * Gets the value of the exemptionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExemptionType() {
        return exemptionType;
    }

    /**
     * Sets the value of the exemptionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExemptionType(String value) {
        this.exemptionType = value;
    }

}
