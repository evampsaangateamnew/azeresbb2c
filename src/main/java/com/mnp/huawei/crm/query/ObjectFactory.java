
package com.mnp.huawei.crm.query;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.crm.query package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.crm.query
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCustomerDataListResponse }
     * 
     */
    public GetCustomerDataListResponse createGetCustomerDataListResponse() {
        return new GetCustomerDataListResponse();
    }

    /**
     * Create an instance of {@link GetCustomerDataListOut }
     * 
     */
    public GetCustomerDataListOut createGetCustomerDataListOut() {
        return new GetCustomerDataListOut();
    }

    /**
     * Create an instance of {@link GetCorpCustomerDataRequest }
     * 
     */
    public GetCorpCustomerDataRequest createGetCorpCustomerDataRequest() {
        return new GetCorpCustomerDataRequest();
    }

    /**
     * Create an instance of {@link GetCorpCustomerDataIn }
     * 
     */
    public GetCorpCustomerDataIn createGetCorpCustomerDataIn() {
        return new GetCorpCustomerDataIn();
    }

    /**
     * Create an instance of {@link GetCustomerDataListRequest }
     * 
     */
    public GetCustomerDataListRequest createGetCustomerDataListRequest() {
        return new GetCustomerDataListRequest();
    }

    /**
     * Create an instance of {@link GetCustomerDataListIn }
     * 
     */
    public GetCustomerDataListIn createGetCustomerDataListIn() {
        return new GetCustomerDataListIn();
    }

    /**
     * Create an instance of {@link GetCorpCustomerDataResponse }
     * 
     */
    public GetCorpCustomerDataResponse createGetCorpCustomerDataResponse() {
        return new GetCorpCustomerDataResponse();
    }

    /**
     * Create an instance of {@link GetCorpCustomerDataOut }
     * 
     */
    public GetCorpCustomerDataOut createGetCorpCustomerDataOut() {
        return new GetCorpCustomerDataOut();
    }

    /**
     * Create an instance of {@link CustomerInfo }
     * 
     */
    public CustomerInfo createCustomerInfo() {
        return new CustomerInfo();
    }

}
