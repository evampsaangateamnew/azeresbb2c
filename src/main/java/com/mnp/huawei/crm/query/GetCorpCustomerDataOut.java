
package com.mnp.huawei.crm.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.mnp.huawei.crm.basetype.RegisterCustInfo;


/**
 * <p>Java class for GetCorpCustomerDataOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCorpCustomerDataOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCorpCustomerDataList" type="{http://crm.huawei.com/basetype/}RegisterCustInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCorpCustomerDataOut", propOrder = {
    "getCorpCustomerDataList"
})
public class GetCorpCustomerDataOut {

    @XmlElement(name = "GetCorpCustomerDataList")
    protected List<RegisterCustInfo> getCorpCustomerDataList;

    /**
     * Gets the value of the getCorpCustomerDataList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCorpCustomerDataList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCorpCustomerDataList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegisterCustInfo }
     * 
     * 
     */
    public List<RegisterCustInfo> getGetCorpCustomerDataList() {
        if (getCorpCustomerDataList == null) {
            getCorpCustomerDataList = new ArrayList<RegisterCustInfo>();
        }
        return this.getCorpCustomerDataList;
    }

}
