
package com.mnp.huawei.crm.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.mnp.huawei.crm.basetype.ExtParameterList;


/**
 * <p>Java class for GetCorpCustomerDataIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCorpCustomerDataIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CustomerId" type="{http://crm.huawei.com/basetype/}CustomerId" minOccurs="0"/>
 *         &lt;element name="GroupId" type="{http://crm.huawei.com/basetype/}GroupId" minOccurs="0"/>
 *         &lt;element name="GroupName" type="{http://crm.huawei.com/basetype/}GroupName" minOccurs="0"/>
 *         &lt;element name="GroupNo" type="{http://crm.huawei.com/basetype/}GroupNo" minOccurs="0"/>
 *         &lt;element name="CorpNo" type="{http://crm.huawei.com/basetype/}CorporateNumber" minOccurs="0"/>
 *         &lt;element name="CorporateName" type="{http://crm.huawei.com/basetype/}CustomerName" minOccurs="0"/>
 *         &lt;element name="ContactName" type="{http://crm.huawei.com/basetype/}RelaName" minOccurs="0"/>
 *         &lt;element name="ContactServiceNumber" type="{http://crm.huawei.com/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="MemberServiceNumber" type="{http://crm.huawei.com/basetype/}RelaTelephone" minOccurs="0"/>
 *         &lt;element name="TIN_NUMBER" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CertificateType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CertificateNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ExtParamList" type="{http://crm.huawei.com/basetype/}ExtParameterList" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCorpCustomerDataIn", propOrder = {

})
public class GetCorpCustomerDataIn {

    @XmlElement(name = "CustomerId")
    protected Long customerId;
    @XmlElement(name = "GroupId")
    protected Long groupId;
    @XmlElement(name = "GroupName")
    protected String groupName;
    @XmlElement(name = "GroupNo")
    protected String groupNo;
    @XmlElement(name = "CorpNo")
    protected String corpNo;
    @XmlElement(name = "CorporateName")
    protected String corporateName;
    @XmlElement(name = "ContactName")
    protected String contactName;
    @XmlElement(name = "ContactServiceNumber")
    protected String contactServiceNumber;
    @XmlElement(name = "MemberServiceNumber")
    protected String memberServiceNumber;
    @XmlElement(name = "TIN_NUMBER")
    protected String tinnumber;
    @XmlElement(name = "CertificateType")
    protected String certificateType;
    @XmlElement(name = "CertificateNumber")
    protected String certificateNumber;
    @XmlElement(name = "ExtParamList")
    protected ExtParameterList extParamList;

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the groupId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGroupId(Long value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the groupNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupNo() {
        return groupNo;
    }

    /**
     * Sets the value of the groupNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupNo(String value) {
        this.groupNo = value;
    }

    /**
     * Gets the value of the corpNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpNo() {
        return corpNo;
    }

    /**
     * Sets the value of the corpNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpNo(String value) {
        this.corpNo = value;
    }

    /**
     * Gets the value of the corporateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateName() {
        return corporateName;
    }

    /**
     * Sets the value of the corporateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateName(String value) {
        this.corporateName = value;
    }

    /**
     * Gets the value of the contactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Sets the value of the contactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactName(String value) {
        this.contactName = value;
    }

    /**
     * Gets the value of the contactServiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactServiceNumber() {
        return contactServiceNumber;
    }

    /**
     * Sets the value of the contactServiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactServiceNumber(String value) {
        this.contactServiceNumber = value;
    }

    /**
     * Gets the value of the memberServiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberServiceNumber() {
        return memberServiceNumber;
    }

    /**
     * Sets the value of the memberServiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberServiceNumber(String value) {
        this.memberServiceNumber = value;
    }

    /**
     * Gets the value of the tinnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTINNUMBER() {
        return tinnumber;
    }

    /**
     * Sets the value of the tinnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTINNUMBER(String value) {
        this.tinnumber = value;
    }

    /**
     * Gets the value of the certificateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateType() {
        return certificateType;
    }

    /**
     * Sets the value of the certificateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateType(String value) {
        this.certificateType = value;
    }

    /**
     * Gets the value of the certificateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateNumber() {
        return certificateNumber;
    }

    /**
     * Sets the value of the certificateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateNumber(String value) {
        this.certificateNumber = value;
    }

    /**
     * Gets the value of the extParamList property.
     * 
     * @return
     *     possible object is
     *     {@link ExtParameterList }
     *     
     */
    public ExtParameterList getExtParamList() {
        return extParamList;
    }

    /**
     * Sets the value of the extParamList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtParameterList }
     *     
     */
    public void setExtParamList(ExtParameterList value) {
        this.extParamList = value;
    }

}
